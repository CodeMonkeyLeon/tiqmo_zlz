package com.bottomnavigation

/**
 * 防止按钮多次点击
 */
object ButtonUtils {
    private var lastClickTime: Long = 0
    private const val DIFF: Long = 200

    //    private const val DIFF: Long = 0
    private var lastButtonId = -1

    /**
     * 判断两次点击的间隔，如果小于1000，则认为是多次无效点击
     *
     * @return
     */
    val isFastDoubleClick: Boolean
        get() = isFastDoubleClick(-1, DIFF)

    fun isFastDoubleClick(time: Long): Boolean {
        return isFastDoubleClick(-1, time)
    }

    /**
     * 判断两次点击的间隔，如果小于1000，则认为是多次无效点击
     *
     * @return
     */
    fun isFastDoubleClick(buttonId: Int): Boolean {
        return isFastDoubleClick(buttonId, DIFF)
    }

    /**
     * 判断两次点击的间隔，如果小于diff，则认为是多次无效点击
     *
     * @param diff
     * @return
     */
    fun isFastDoubleClick(buttonId: Int, diff: Long): Boolean {
        val time = System.currentTimeMillis()
        val timeD = time - lastClickTime
        //if (lastButtonId == buttonId && lastClickTime > 0 && timeD < diff) {
        if (lastClickTime > 0 && timeD < diff) {
            return true
        }
        lastClickTime = time
        lastButtonId = buttonId
        return false
    }
}