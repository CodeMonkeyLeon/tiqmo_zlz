package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SplitOderDetailEntity extends BaseEntity {

    private String orderDesc;
    private String tradeTime;
    private String totalAmount;
    private String orderCurrency;
    private String orderStatus;

    public String getOrderDesc() {
        return this.orderDesc;
    }

    public void setOrderDesc(final String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getTradeTime() {
        return this.tradeTime;
    }

    public void setTradeTime(final String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getTotalAmount() {
        return this.totalAmount;
    }

    public void setTotalAmount(final String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOrderCurrency() {
        return this.orderCurrency;
    }

    public void setOrderCurrency(final String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderStatus() {
        return this.orderStatus;
    }

    public void setOrderStatus(final String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
