package cn.swiftpass.wallet.tiqmo.module.setting.entity;


import com.gc.gcchat.GcChatSession;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryListParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;

public class EventEntity {

    public static final int EVENT_ADD_CARD = 0;
    public static final int EVENT_ADD_MONEY = 3;
    public static final int EVENT_OPEN_ACCOUNT = 1;
    public static final int EVENT_GET_CARD_LIST = 2;
    public static final int EVENT_HOME_SCAN = 4;
    public static final int EVENT_ADD_MONEY_SCAN = 5;
    public static final int KYC_TYPE_SPLIT_BILL = 6;
    public static final int START_KYC = 7;
    //更新余额
    public static final int UPDATE_BALANCE = 8;
    //imr添加收款人成功
    public static final int EVENT_IMR_ADD_BENE_SUCCESS = 9;
    //imr修改收款人成功
    public static final int EVENT_IMR_EDIT_BENE_SUCCESS = 10;
    //imr sendMoney添加收款人成功
    public static final int EVENT_IMR_SEND_ADD_BENE_SUCCESS = 11;
    //处理请求转账成功
    public static final int EVENT_APPROVE_TRANSFER_SUCCESS = 12;
    //修改支付密码成功
    public static final int EVENT_CHANGE_PAY_PD_SUCCESS = 13;
    //Iban转账添加收款人成功
    public static final int EVENT_ADD_BENEFICIARY_SUCCESS = 14;
    //BillPay 添加payment成功
    public static final int EVENT_ADD_PAYMENT_SUCCESS = 16;

    public static final int EVENT_KSA_SET_LIMIT_SUCCESS = 17;
    public static final int EVENT_KSA_SHOW_DETAIL = 18;
    public static final int EVENT_KSA_CARD_SET_CHANNEL = 188;
    public static final int EVENT_KSA_CARD_FREEZE = 189;
    public static final int EVENT_KSA_CARD_UNFREEZE = 190;
    public static final int EVENT_KSA_CARD_BLOCK = 191;
    public static final int EVENT_KSA_CARD_ACTIVATE = 192;
    public static final int EVENT_CHANGE_LOGIN_PD = 193;
    public static final int EVENT_HyperPay_ADD_MONEY = 195;
    public static final int EVENT_SEND_BENEFICIARY = 196;
    public static final int EVENT_ADD_BENEFICIARY = 197;
    public static final int EVENT_ADD_SEND_BENEFICIARY = 198;
    public static final int EVENT_ACTIVATE_BENEFICIARY = 199;
    public static final int EVENT_WITHDRAW_ADD_BENEFICIARY = 200;
    public static final int EVENT_NAFATH_FAIL = 201;
    public static final int EVENT_ACTIVATE_USER = 202;
    public static final int EVENT_CHANGE_PHONE_SUCCESS = 203;
    public static final int EVENT_CHANGE_PHONE_PAY_SUCCESS = 204;
    public static final int EVENT_BLOCK_CHAT_USER = 205;
    public static final int EVENT_GET_NEW_NOTIFICATION = 206;
    public static final int EVENT_CHAT_SEND_MONEY_SUCCESS = 207;

    public static final int EVENT_PRE_AUTH_HISTORY_REFRESH = 300;
    public static final int EVENT_HISTORY_REFRESH = 301;

    public static final int EVENT_HISTORY_DETAIL = 302;
    public static final int EVENT_PRE_AUTH_HISTORY_DETAIL = 303;


    public static final int EVENT_HISTORY_DELETE = 304;
    public static final int EVENT_PRE_AUTH_HISTORY_DELETE = 305;

    public static final int EVENT_HISTORY_INIT_DATA = 306;

    private CardNotifyEntity cardNotifyEntity;
    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail responseEntity;
    private CardDetailEntity cardDetailEntity;
    private BalanceListEntity balanceListEntity;
    private RiskControlEntity riskControlEntity;

    private GcChatSession gcChatSession;

    private String failType;

    private TransferEntity transferEntity;


    private HistoryListParamEntity historyListParam;

    private HistoryDetailEntity historyDetail;

    public HistoryDetailEntity getHistoryDetailEntity() {
        return historyDetail;
    }


    public HistoryListParamEntity getHistoryListParamEntity() {
        return historyListParam;
    }


    public TransferEntity getTransferEntity() {
        return this.transferEntity;
    }

    public EventEntity(final int eventType, final TransferEntity transferEntity) {
        this.transferEntity = transferEntity;
        this.eventType = eventType;
    }

    public String getFailType() {
        return this.failType;
    }

    public RiskControlEntity getRiskControlEntity() {
        return this.riskControlEntity;
    }

    public BalanceListEntity getBalanceListEntity() {
        return this.balanceListEntity;
    }

    public ImrBeneficiaryEntity getImrBeneficiaryEntity() {
        return imrBeneficiaryEntity;
    }

    public ImrBeneficiaryDetails.ImrBeneficiaryDetail getResponseEntity() {
        return responseEntity;
    }

    public CardNotifyEntity getCardNotifyEntity() {
        return cardNotifyEntity;
    }

    public CardDetailEntity getCardDetailEntity() {
        return this.cardDetailEntity;
    }

    public void setCardNotifyEntity(CardNotifyEntity cardNotifyEntity) {
        this.cardNotifyEntity = cardNotifyEntity;
    }

    public EventEntity(int eventType) {

        this.eventType = eventType;
    }


    public EventEntity(int eventType, HistoryListParamEntity historyListParam) {
        this.eventType = eventType;
        this.historyListParam = historyListParam;
    }

    public EventEntity(int eventType, HistoryDetailEntity detailEntity) {
        this.eventType = eventType;
        this.historyDetail = detailEntity;
    }

    public EventEntity(int eventType, BalanceListEntity balanceListEntity) {
        this.eventType = eventType;
        this.balanceListEntity = balanceListEntity;
    }

    public GcChatSession getGcChatSession() {
        return this.gcChatSession;
    }

    public void setGcChatSession(final GcChatSession gcChatSession) {
        this.gcChatSession = gcChatSession;
    }

    public EventEntity(final int eventType, final GcChatSession gcChatSession) {
        this.eventType = eventType;
        this.gcChatSession = gcChatSession;
    }

    public EventEntity(int eventType, String message) {

        this.eventType = eventType;
        this.eventMsg = message;
    }

    public EventEntity(int eventType, CardNotifyEntity cardNotifyEntity) {

        this.eventType = eventType;
        this.cardNotifyEntity = cardNotifyEntity;
    }

    public EventEntity(int eventType, CardDetailEntity cardDetailEntity) {

        this.eventType = eventType;
        this.cardDetailEntity = cardDetailEntity;
    }

    public EventEntity(int eventType, ImrBeneficiaryEntity imrBeneficiaryEntity) {
        this.eventType = eventType;
        this.imrBeneficiaryEntity = imrBeneficiaryEntity;
    }

    public EventEntity(int eventType, ImrBeneficiaryEntity imrBeneficiaryEntity, RiskControlEntity riskControlEntity) {
        this.eventType = eventType;
        this.imrBeneficiaryEntity = imrBeneficiaryEntity;
        this.riskControlEntity = riskControlEntity;
    }

    public EventEntity(int eventType, ImrBeneficiaryDetails.ImrBeneficiaryDetail responseEntity, RiskControlEntity riskControlEntity) {
        this.eventType = eventType;
        this.responseEntity = responseEntity;
        this.riskControlEntity = riskControlEntity;
    }

    public EventEntity(int eventType, String message, String errorCodeIn) {

        this.eventType = eventType;
        this.eventMsg = message;
        this.errorCode = errorCodeIn;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getEventMsg() {
        return eventMsg;
    }

    public void setEventMsg(String eventMsg) {
        this.eventMsg = eventMsg;
    }

    private int eventType;
    private String eventMsg;

    public String getErrorCode() {
        return errorCode;
    }

    private String errorCode;

}
