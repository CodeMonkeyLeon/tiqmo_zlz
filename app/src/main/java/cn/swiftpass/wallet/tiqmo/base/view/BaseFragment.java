package cn.swiftpass.wallet.tiqmo.base.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CustomerInfoActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ExecutorManager;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.OnImageBitmapSuccess;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SdkShareUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CommonNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomProgressDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ExpireIdDialog;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements Loading {

    private static final String TAG = BaseFragment.class.getSimpleName();

    @LayoutRes
    protected abstract int getLayoutID();

    @Nullable
    @Override
    public Context getContext() {
        return super.getContext();
    }

    protected abstract void initView(View parentView);
    protected abstract void initData();
    protected abstract void restart();

    protected P mPresenter;
    public Context mContext;
    public Activity mActivity;
    public View view;
    protected CustomProgressDialog mProgressDialog;
    private int mCurrentPermissionsRequestCode;
    private Unbinder unbinder;
    protected EventBus eventBus;
    private boolean isLoaded = false;

    private WindowManager windowManager;
    private Handler mHandler;
    private View tipView;

    private CustomDialog mDealDialog;

    protected CommonNoticeDialog commonNoticeDialog;

    private ChatConfirmDialog chatConfirmDialog;

    private ExpireIdDialog oneButtonBottomDialog;

    protected boolean isChatConnected;

    public boolean firstRejectPermission = true;

    public void setContext(FragmentActivity fragmentActivity) {
        mContext = fragmentActivity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void getUserDisableAppFuncCodeList(){
        UserInfoEntity userInfoEntity = getUserInfoEntity();
        AppClient.getInstance().getUserManager().getUserDisableAppFuncCodeList(userInfoEntity.callingCode, userInfoEntity.phone, new ResultCallback<UserUnableUseEntity>() {
            @Override
            public void onResult(UserUnableUseEntity userUnableUseEntity) {
                SpUtils.getInstance().setUserUnableUseEntity(userUnableUseEntity);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }
        });
    }

    public void getBalance() {
        AppClient.getInstance().getBalance(new ResultCallback<BalanceListEntity>() {
            @Override
            public void onResult(BalanceListEntity balanceListEntity) {
                if (balanceListEntity != null) {
                    if(balanceListEntity.balanceInfos.size()>0) {
                        BalanceEntity balanceEntity = balanceListEntity.balanceInfos.get(0);
                        if (balanceEntity != null) {
                            String balance = balanceEntity.balance;
                            if (!TextUtils.isEmpty(balance)) {
                                UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
                                if (userInfoEntity != null) {
                                    userInfoEntity.setBalance(balance);
                                    AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);

                                    eventBus.post(new EventEntity(EventEntity.UPDATE_BALANCE, balanceListEntity));
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }
        });
    }

    public void setAlpha(Context context, float alpha) {
        WindowManager.LayoutParams params = ((Activity) context).getWindow().getAttributes();
        params.alpha = alpha;
        ((Activity) context).getWindow().setAttributes(params);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
    }

    public void showTipDialog(String msg) {
        removeView();
        tipView = LayoutInflater.from(mActivity).inflate(R.layout.layout_tips, null);
        TextView tvMsg = tipView.findViewById(R.id.tv_msg);
        tvMsg.setText(msg);
        windowManager = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |    //不拦截页面点击事件
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            layoutParams.format = PixelFormat.TRANSLUCENT;
            layoutParams.gravity = Gravity.TOP;
//            layoutParams.height = (int) getResources().getDimension(R.dimen.dimen_42);
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            layoutParams.windowAnimations = R.style.popwindowAnimStyle;    //自定义动画
            windowManager.addView(tipView, layoutParams);

            if (mHandler == null) {
                mHandler = new Handler();
            }
            mHandler.postDelayed(timerRunnable, 4000);
        }
    }

    private final Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            removeView();
        }
    };

    public void removeView() {
        if (tipView != null && tipView.getParent() != null) {
            if (windowManager != null) {
                windowManager.removeViewImmediate(tipView);
                windowManager = null;
                mHandler.removeCallbacks(timerRunnable);
            }
        }
    }

    public UserInfoEntity getUserInfoEntity() {
        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        return userInfoEntity;
    }

    public String getChannelCode(){
        return SpUtils.getInstance().getChannelCode();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isLoaded) {
            initData();
            isLoaded = true;
        } else {
            restart();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getLayoutID() == 0) {
            return null;
        }
        View view = inflater.inflate(getLayoutID(), container, false);
        unbinder = ButterKnife.bind(this, view);
        eventBus = EventBus.getDefault();
        initPresenter();
        this.view = view;
        initView(view);
        return view;
    }

    public void dissmissOneButtonDialog(){
        if(oneButtonBottomDialog != null){
            oneButtonBottomDialog.dismiss();
            oneButtonBottomDialog = null;
        }
    }

    protected void showCommonOneButtonDialog(String title,int imgId,String content,String confirmText,ExpireIdDialog.RenewIdListener renewIdListener){
        oneButtonBottomDialog = new ExpireIdDialog(mContext);
        oneButtonBottomDialog.setTitle(title);
        oneButtonBottomDialog.setImage(imgId);
        oneButtonBottomDialog.setContent(content);
        oneButtonBottomDialog.setConfirmText(confirmText);
        oneButtonBottomDialog.setCloseListener(renewIdListener);
        oneButtonBottomDialog.showWithBottomAnim();
    }

    protected void showExpireIdDialog() {
        ExpireIdDialog expireIdDialog = new ExpireIdDialog(mContext);
        expireIdDialog.setCloseListener(new ExpireIdDialog.RenewIdListener() {
            @Override
            public void clickRenew() {
                ActivitySkipUtil.startAnotherActivity((Activity) mContext, CustomerInfoActivity.class,
                        ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                expireIdDialog.dismiss();
            }
        });
        expireIdDialog.showWithBottomAnim();
    }

    //用户id未过期
    protected boolean isNotExpireId(){
        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            if (userInfoEntity.isUserIdCardExpried()) {
                showExpireIdDialog();
                return false;
            }
        }
        return true;
    }

    protected boolean isKyc(int kycType) {
        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            if (userInfoEntity.isKyc()) {
                if (userInfoEntity.isUserIdCardExpried()) {
                    showExpireIdDialog();
                    return false;
                }
                return true;
            } else {
                if (userInfoEntity.isKycChecking()) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                } else if (userInfoEntity.isKycHigh()) {
                    showLogoutDialog();
                } else {
                    UserInfoManager.getInstance().setKycType(kycType);
                    ReferralCodeDialogUtils.showReferralCodeDialog(this, mActivity);
                }
            }
        }

        return false;
    }

    protected void showLogoutDialog() {
        BottomDialog bottomDialog = new BottomDialog(mActivity);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        View contentView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_kyc_logout, null);
        TextView tvLogOut = contentView.findViewById(R.id.tv_log_out);

        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, RegisterActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    finish();
                    ProjectApp.removeAllTaskExcludeLoginStack();
                }
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
//            layoutParams.height = AndroidUtils.dip2px(mActivity, 400);
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    public void finish() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    protected void showLackOfPermissionDialog(String title, String message) {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.common_9), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mContext);
            }
        });
        builder.setNegativeButton(getString(R.string.common_10), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_1da1f1).setRightColor(R.color.color_1da1f1);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    public void showCustomDialog(Context mContext, int msgId, int posStr, int navId, DialogInterface.OnClickListener posClick) {
        CustomDialog mDealDialog = null;
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle("           ");
        builder.setMessage(msgId);
        builder.setNegativeButton(getString(navId), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        builder.setPositiveButton(getString(posStr), posClick);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    //--------------------------- -------------------------------------- 显示错误dialog -------------------------------------- --------------------------------------//

    public void showCustomDialog(Context mContext, int titleId, int msgId, int posStr, int navId, DialogInterface.OnClickListener posClick, DialogInterface.OnClickListener navClick) {
        CustomDialog mDealDialog = null;
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(titleId);
        builder.setMessage(msgId);
        builder.setNegativeButton(getString(navId), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(dialog != null){
                    dialog.dismiss();
                }
            }
        });
        builder.setPositiveButton(getString(posStr), posClick);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    /**
     * 检查是否有permission权限  返回true则表示有
     */
    public boolean isGranted_(String permission) {
        if (getActivity() == null) {
            return false;
        }
        int checkSelfPermission = ActivityCompat.checkSelfPermission(mActivity, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    public void showErrorPwdDialog(Context mContext, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.wtw_11), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.wtw_11), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String title, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(mContext.getString(R.string.wtw_11), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String title, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(mContext.getString(R.string.wtw_11), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.wtw_11), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.setCanceledOnTouchOutside(true);
//            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    //--------------------------- -------------------------------------- 显示错误dialog -------------------------------------- --------------------------------------//

    @CallSuper
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        resetView();
    }

    /**
     * 重置资源
     */
    protected void resetView() {
        if (mPresenter != null) {
            mPresenter.detachView();
            mPresenter = null;
        }

        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    public final void requestPermissionsInCompatMode(@NonNull String[] permissions, int requestCode) {
        List<String> permissionList = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(permission);
            }
        }

        int size = permissionList.size();
        if (size != 0) {
            mCurrentPermissionsRequestCode = requestCode;
            requestPermissions(permissionList.toArray(new String[size]), requestCode);
        } else {
            onRequestPermissionsResult(requestCode, true, null);
        }
    }

    @Override
    public final void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != mCurrentPermissionsRequestCode) {
            return;
        }
        mCurrentPermissionsRequestCode = -1;
        boolean isNeedShowMissingPermissionDialog = false;
        boolean result = true;
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (int i = 0, length = permissions.length; i < length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                result = false;
                if (!shouldShowRequestPermissionRationale(permissions[i])) {
                    isNeedShowMissingPermissionDialog = true;
                }
                deniedPermissions.add(permissions[i]);
            }
        }
        final String[] deniedArr = deniedPermissions.toArray(new String[0]);
        if (result) {
            onRequestPermissionsResult(requestCode, true, null);
        } else if (isNeedShowMissingPermissionDialog) {
        } else {
            onRequestPermissionsResult(requestCode, false, deniedArr);
        }
    }

    protected void onRequestPermissionsResult(int requestCode, boolean isSuccess, String[] deniedPermissions) {

    }

    public void showToast(String content) {
        showToast(content, Toast.LENGTH_SHORT);
    }

    public void showLongToast(String content) {
        showToast(content, Toast.LENGTH_LONG);
    }

    public void showToast(String content, int duration) {
        Toast.makeText(mContext, content, duration).show();
    }

    @Override
    public void showProgress(boolean iShow) {
        showProgress(iShow, false, true);
    }

    @Override
    public void showProgress(boolean iShow, boolean isDim) {
        showProgress(iShow, false, isDim);
    }

    public void showProgressCancelable(boolean iShow, boolean cancelable) {
        showProgress(iShow, cancelable, true);
    }

    public void showProgress(boolean iShow, final boolean cancelable, boolean isDim) {
        Activity activity = (Activity) this.getActivity();
        if(activity == null)return;
        if (mProgressDialog == null) {
            try {
                if (!activity.isFinishing()) {
                    if(isDim){
                        mProgressDialog = CustomProgressDialog.createDialog(activity, activity.getString(R.string.common_34),R.style.CustomProgressDialog);
                    }else {
                        mProgressDialog = CustomProgressDialog.createDialog(activity, activity.getString(R.string.common_34),R.style.NoDimCustomProgressDialog);
                    }
                    mProgressDialog.setCancel(false);
                }
            } catch (Exception e) {
                LogUtils.d(TAG, "---"+e+"---");
            }
        }
        if (iShow) {
            mProgressDialog.setCancelable(cancelable);
            if (!mProgressDialog.isShowing()) {
                if (!activity.isFinishing()) {
                    mProgressDialog.show();
                }
            }
        } else {
            mProgressDialog.setOnCancelListener(null);
            mProgressDialog.dismiss();
        }
    }

    public void showErrorDialog(String error) {
        showErrorMsgDialog(getContext(), error);
    }

    public void showErrorDialog(String error, OnMsgClickCallBack listener) {
        showErrorMsgDialog(getContext(), error, listener);
    }

    public boolean isAttachedToPresenter() {
        return mPresenter != null;
    }

    @SuppressWarnings("unchecked")
    protected final void initPresenter() {
        if (this instanceof BaseView) {
            BaseView view = (BaseView) this;
            mPresenter = (P) view.getPresenter();
            if (mPresenter == null) {
                return;
            }
            Class aClass = this.getClass();
            while (aClass != null) {
                Type type = aClass.getGenericSuperclass();
                if (type instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type;
                    Type genericType = parameterizedType.getActualTypeArguments()[0];
                    Class<?>[] interfaces = mPresenter.getClass().getInterfaces();
                    for (Class c : interfaces) {
                        if (c == genericType) {
                            mPresenter.attachView(view);
                            return;
                        }
                    }
                } else {
                    aClass = aClass.getSuperclass();
                }
            }
            mPresenter = null;
        }
    }

    //根据状态来显示需要显示的view
    protected void showStatusView(StatusView statusView, int status) {
        switch (status) {
            case StatusView.CONTENT_VIEW:
                statusView.dismissLoading();
                break;
            case StatusView.NET_ERROR_VIEW:
                statusView.showNetError();
                break;
            case StatusView.NO_CONTENT_VIEW:
                statusView.showEmpty();
                break;
            case StatusView.LOADING_VIEW:
                statusView.showLoading();
                break;
            default:
                statusView.dismissLoading();
                break;
        }
    }

    public void noticeThemeChange() {
    }

    /**
     * 截图保存图片到本地
     */
    protected void saveImage(LinearLayout linearLayout) {
        if (!isGranted_(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                firstRejectPermission = false;
            } else {
                firstRejectPermission = true;
            }
            PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    saveToAlbum(linearLayout);
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        //拒绝并勾选不再询问
                        if (firstRejectPermission) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_32));
                        } else {
                            firstRejectPermission = false;
                        }
                    }
                }
            }, PermissionInstance.getPermissons(2));
        } else {
            saveToAlbum(linearLayout);
        }
    }

    private void saveToAlbum(LinearLayout linearLayout) {
        showProgress(true);
        linearLayout.post(new Runnable() {
            @Override
            public void run() {
                ExecutorManager.getInstance()
                        .execute(new Runnable() {
                            @Override
                            public void run() {
                                saveImageToGalley(linearLayout);
                                linearLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        showTipDialog(getString(R.string.common_28));
                                        showProgress(false);
                                    }
                                });
                            }
                        });
            }
        });
    }

    private void saveImageToGalley(LinearLayout linearLayout) {
        // 把一个View转换成图片
        Bitmap cacheBmp = AndroidUtils.getViewGroupBitmap(linearLayout);
        AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cacheBmp);
    }

    /**
     * 截图并分享
     */
    protected void downShareImage(LinearLayout linearLayout, String title) {
        initShareView(linearLayout, new OnImageBitmapSuccess() {
            @Override
            public void onImageBitmapCallBack(Uri uri) {
                showProgress(false);
//                AndroidUtils.shareImage(mContext, title, uri.getPath());
                AndroidUtils.shareImage(mContext, title, uri);
            }
        });
    }

    private void initShareView(LinearLayout linearLayout, OnImageBitmapSuccess onImageBitmapSuccess) {
        Bitmap cachebmp = AndroidUtils.getViewGroupBitmap(linearLayout);

        if (!isGranted_(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                firstRejectPermission = false;
            } else {
                firstRejectPermission = true;
            }
            PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    shareImageTemp(cachebmp, onImageBitmapSuccess);
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        //拒绝并勾选不再询问
                        if (!firstRejectPermission) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_32));
                        } else {
                            firstRejectPermission = false;
                        }
                    }
                }
            }, PermissionInstance.getPermissons(2));
        } else {
            shareImageTemp(cachebmp, onImageBitmapSuccess);
        }
    }

    private void shareImageTemp(Bitmap cachebmp, OnImageBitmapSuccess onImageBitmapSuccess) {
//        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + mContext.getString(R.string.account_pay_detail);
        String filePath = "transfer_" + AndroidUtils.localImageStr(System.currentTimeMillis()) + ".jpg";
//        String filePath = mContext.getExternalFilesDir(Environment.DIRECTORY_SCREENSHOTS).getAbsolutePath() + File.separator + mContext.getString(R.string.account_pay_detail);
        ExecutorManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, filePath);
                if (uri != null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (onImageBitmapSuccess != null) {
                                onImageBitmapSuccess.onImageBitmapCallBack(uri);
                            }
                        }
                    });
                }
            }
        });
    }

    public void showNafathErrorDialog(String failType){
        ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
        String errorTitle = "";
        String errorContent = "";
        int errorImgId = -1;
        if("1".equals(failType)){
            //未成年
            errorTitle = getString(R.string.sprint11_97);
            errorContent = getString(R.string.sprint11_98);
            errorImgId = R.drawable.l_error_reg_id;
        }else if("2".equals(failType)){
            //id过期
            errorTitle = getString(R.string.analytics_32);
            errorContent = getString(R.string.RENEW_2);
            errorImgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_expired_id);
        }else if("3".equals(failType)){
            //id不匹配
            errorTitle = getString(R.string.sprint11_99);
            errorContent = getString(R.string.sprint11_100);
            errorImgId = R.drawable.l_error_reg_id;
        }else if("080121".equals(failType)){
            //电话和id不匹配
            errorTitle = getString(R.string.sprint11_95);
            errorContent = getString(R.string.sprint11_96);
            errorImgId = R.drawable.l_error_mobile_number;
        }else if("4".equals(failType)){
            //修改手机号码成功
            errorTitle = getString(R.string.sprint20_38);
            errorContent = getString(R.string.sprint20_39);
            errorImgId = R.drawable.l_change_phone_success;
        }
        errorBottomDialog.setErrorMsg(errorTitle,errorContent,errorImgId);
        errorBottomDialog.showWithBottomAnim();
    }

    protected void contactHelp(){
        if (!isGranted_(Manifest.permission.CALL_PHONE)) {
            PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    SdkShareUtil.callPhoneNumber(mContext, Constants.call_phone);
                }

                @Override
                public void rejectPermission() {

                }
            }, Manifest.permission.CALL_PHONE);
        } else {
            SdkShareUtil.callPhoneNumber(mContext, Constants.call_phone);
        }
    }

    public void showChatConfirmDialog(String title,String left, String right, String message, ChatConfirmDialog.ConfirmListener confirmListener){

        chatConfirmDialog = new ChatConfirmDialog(mContext);
        chatConfirmDialog.setLeft(left);
        chatConfirmDialog.setRight(right);
        chatConfirmDialog.setMessage(message);
        chatConfirmDialog.setTitle(title);
        chatConfirmDialog.setConfirmListener(confirmListener);
        chatConfirmDialog.showMatchParentWidthAndGravityCenter(AndroidUtils.dip2px(mContext,280));
    }

    public void dissmissChatConfirmDialog(){
        if(chatConfirmDialog != null){
            chatConfirmDialog.dismiss();
            chatConfirmDialog = null;
        }
    }

    protected void showActivateSuccessDialog(Context mContext,String lockAcc,CommonNoticeDialog.CommonConfirmListener commonConfirmListener){
        commonNoticeDialog = new CommonNoticeDialog(mContext);
        String title = "",content = "",confirmContent = "";
        int imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);;
        switch (lockAcc){
            //非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
            case "S":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_112);
                confirmContent = mContext.getString(R.string.sprint11_113);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_reactivated);
                break;
            case "D":
            case "U":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_119);
                confirmContent = mContext.getString(R.string.sprint11_120);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_ivr_call);
                break;
            case "E":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_126);
                confirmContent = mContext.getString(R.string.sprint11_113);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_has_renew);
                break;
            case "W":
                //Id更新失败或者处理中
                title = mContext.getString(R.string.sprint11_127);
                content = mContext.getString(R.string.sprint11_128);
                confirmContent = "";
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_not_renew);
                break;
            default:break;
        }
        commonNoticeDialog.setCommonText(title,content,confirmContent,imgId);
        commonNoticeDialog.setCommonConfirmListener(commonConfirmListener);
        commonNoticeDialog.showWithBottomAnim();
    }

    protected void showAccountStatusDialog(Context mContext,String lockAcc,CommonNoticeDialog.CommonConfirmListener commonConfirmListener){
        commonNoticeDialog = new CommonNoticeDialog(mContext);
        String title = "",content = "",confirmContent = "";
        int imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);;
        switch (lockAcc){
            //非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
            case "S":
                title = mContext.getString(R.string.sprint11_108);
                content = mContext.getString(R.string.sprint11_109);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);
                break;
            case "D":
                title = mContext.getString(R.string.sprint11_117);
                content = mContext.getString(R.string.sprint11_118);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_dormant);
                break;
            case "C":
                title = mContext.getString(R.string.sprint11_114);
                content = mContext.getString(R.string.sprint11_115);
                confirmContent = mContext.getString(R.string.sprint11_116);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_closed);
                break;
            case "U":
                title = mContext.getString(R.string.sprint11_121);
                content = mContext.getString(R.string.sprint11_122);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);
                break;
            case "E":
                /**
                 * {\"codeType\":0,\"partnerNo\":\"96601\",\"requestCounts\":0,\"lockAcc\":\"E\"}"
                 */
                title = mContext.getString(R.string.sprint11_123);
                content = mContext.getString(R.string.sprint11_124);
                confirmContent = mContext.getString(R.string.sprint11_125);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_expired);
                break;
            default:break;
        }
        commonNoticeDialog.setCommonText(title,content,confirmContent,imgId);
        commonNoticeDialog.setCommonConfirmListener(commonConfirmListener);
        commonNoticeDialog.showWithBottomAnim();
    }

    protected void dismissNoticeDialog(){
        if(commonNoticeDialog != null && commonNoticeDialog.isShowing()){
            commonNoticeDialog.dismiss();
        }
    }
}
