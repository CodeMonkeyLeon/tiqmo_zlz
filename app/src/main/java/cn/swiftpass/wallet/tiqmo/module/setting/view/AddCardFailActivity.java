package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;

public class AddCardFailActivity extends BaseCompatActivity {
    @BindView(R.id.tv_ok)
    TextView tvOk;
    @BindView(R.id.ll_content)
    LinearLayout llContent;

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_add_card_fail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @OnClick(R.id.tv_ok)
    public void onViewClicked() {
        finish();
    }
}
