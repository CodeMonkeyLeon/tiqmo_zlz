package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;

public class ImrDeclarationDialog extends BaseDialogFragmentN {

    TextView confirmButton;
    OnAgreeClickListener listener;
    OnDialogCancelListener cancelListener;

    public void setOnAgreeClickListener(OnAgreeClickListener listener) {
        this.listener = listener;
    }

    public void setCancelListener(OnDialogCancelListener cancelListener) {
        this.cancelListener = cancelListener;
    }

    private volatile static ImrDeclarationDialog singleton;
    private ImrDeclarationDialog (){}
    public static ImrDeclarationDialog getSingleton() {
        if (singleton == null) {
            synchronized (ImrDeclarationDialog.class) {
                if (singleton == null) {
                    singleton = new ImrDeclarationDialog();
                }
            }
        }
        return singleton;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_imr_declaration;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
        initData();
    }

    private void initData() {
    }

    private void initView(View parentView) {
        confirmButton = parentView.findViewById(R.id.tv_declaration_agree);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onAgreeClick();
                }
                ImrDeclarationDialog.this.dismiss();
            }
        });
    }

    public interface OnAgreeClickListener {
        void onAgreeClick();
    }

    public interface OnDialogCancelListener {
        void onCancel();
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        if (cancelListener!= null) {
            cancelListener.onCancel();
        }
    }
}
