package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.ConfirmAddMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.ActivateCardActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.BlockCardConfirmActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.CardManageKsaFragment;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetCardPwdOneActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetCardPwdTwoActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetNewCardSuccessActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetNewCardSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardLimitActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardResultActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardSettingActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.SetPinConfirmActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CharitySummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.ReceiveSplitFragment;
import cn.swiftpass.wallet.tiqmo.module.home.view.RequestCenterActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryFiveActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiarySixActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryTwoActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrSendMoneySummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.view.PayBillSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegSetPwdOneActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegSetPwdTwoActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.NumberGridAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.NumberEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferPwdTimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferSurePayPresenter;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherPayReviewActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.SystemInitManager;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BiometricInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyGridView;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomDialog;

public class TransferPwdActivity extends BaseCompatActivity<TransferContract.TransferSurePayPresenter> implements TransferContract.TransferSurePayView {

    @BindView(R.id.tv_forget_pwd)
    TextView tvForgetPwd;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_show_time_expire)
    TextView tvShowTimeExpire;
    @BindView(R.id.tv_otp_time)
    TextView tvOtpTime;
    @BindView(R.id.ll_otp_time)
    LinearLayout llOtpTime;
    @BindView(R.id.iv_splash)
    ImageView ivSplash;
    @BindView(R.id.iv_user_avatar)
    RoundedImageView ivUserAvatar;
    @BindView(R.id.tv_user_name)
    MyTextView tvUserName;
    @BindView(R.id.cb_reg_one)
    CheckBox cbRegOne;
    @BindView(R.id.cb_reg_two)
    CheckBox cbRegTwo;
    @BindView(R.id.cb_reg_three)
    CheckBox cbRegThree;
    @BindView(R.id.cb_reg_four)
    CheckBox cbRegFour;
    @BindView(R.id.cb_reg_five)
    CheckBox cbRegFive;
    @BindView(R.id.cb_reg_six)
    CheckBox cbRegSix;
    @BindView(R.id.ll_check)
    LinearLayout llCheck;
    @BindView(R.id.tv_error)
    MyTextView tvError;
    @BindView(R.id.ll_face_login)
    LinearLayout llFaceLogin;
    @BindView(R.id.grid_number)
    MyGridView gridNumber;
    @BindView(R.id.con_avatar)
    ConstraintLayout conAvatar;
    @BindView(R.id.tv_name_first)
    TextView tvNameFirst;
    @BindView(R.id.tv_current_title)
    TextView tvCurrentTitle;

    private String password;

    private CustomDialog customDialog;

    private TransferEntity transferEntity;
    private String sceneType, pd_sceneType, orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode, transFees, vat;
    private boolean isHidePwd = true;
    private KycContactEntity kycContactEntity;
    private boolean openQrCode;
    private String lockTime;
    private int lockTimeNumber;
    private boolean isLock;
    private String lockMsg;
    private String payerName;
    private CheckOutEntity checkOutEntity;
    private String paymentMethodNo;
    private String orderType = "P";

    private boolean isFromSDK;
    private boolean isOpen = true;

    private String checkPdType;
    private String reason;

    private boolean isHidePwdClick;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    boolean isFromEditBeneficiary = false;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;

    private String receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
            nickName, relationshipCode, imrCallingCode, phone,
            transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
            birthDate, sex, cityName, districtName,
            poBox, buildingNo, street, idNo, idExpiry,
            bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
            cityId, beneCurrency, channelPayeeId, channelCode, branchId;

    /**
     * 付费开卡
     */
    private KsaCardSummaryEntity ksaCardSummaryEntity;
    /**
     * 免费开卡
     */
    private OpenCardReqEntity openCardReqEntity;
    private KsaCardEntity ksaCardEntity;

    private KsaLimitChannelEntity.CardLimitConfigEntity mCardLimitConfigEntity;

    private String proxyCardNo, blockCardReason, blockCardReasonStatus;
    private RiskControlEntity riskControlEntity;
    private String otpType;
    private BottomDialog bottomDialog;

    private NumberGridAdapter numberGridAdapter;
    private StringBuilder stringBuilder;

    private ChangePhoneFeeEntity mChangePhoneFeeEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_fast_login_new;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        ivBack.setVisibility(View.VISIBLE);
        tvForgetPwd.setVisibility(View.GONE);
        tvCurrentTitle.setVisibility(View.VISIBLE);
        long currentTime = System.currentTimeMillis();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }

        int lastTime = SpUtils.getInstance().getLockTime();
        if (lastTime > 0) {
            lockTimeNumber = SpUtils.getInstance().getLockTime() - (int) (currentTime / 1000);
            lockMsg = SpUtils.getInstance().getLockTimeMsg();
            if (lockTimeNumber > 0) {
                isLock = true;
                tvForgetPwd.setVisibility(View.VISIBLE);
                llOtpTime.setVisibility(View.VISIBLE);
                setErrorMsg(lockMsg);
                setResetTime(true, "", lockTimeNumber);
            } else {
                SpUtils.getInstance().setLockTime(0);
                SpUtils.getInstance().setLockTimeMsg("");
                lockMsg = "";
            }
        }
        if (getIntent() != null && getIntent().getExtras() != null) {
            otpType = getIntent().getExtras().getString(Constants.DATA_OTP_TYPE);
            riskControlEntity = (RiskControlEntity) getIntent().getExtras().getSerializable(Constants.OTP_riskControlEntity);
            ksaCardSummaryEntity = (KsaCardSummaryEntity) getIntent().getExtras().getSerializable(Constants.ksaCardSummaryEntity);
            mCardLimitConfigEntity = (KsaLimitChannelEntity.CardLimitConfigEntity) getIntent().getExtras().getSerializable(Constants.ksaCardLimitConfigEntity);
            openCardReqEntity = (OpenCardReqEntity) getIntent().getExtras().getSerializable(Constants.openCardReqEntity);
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            transferEntity = (TransferEntity) getIntent().getExtras().getSerializable(Constants.TRANSFER_ENTITY);
            kycContactEntity = (KycContactEntity) getIntent().getExtras().getSerializable(Constants.CONTACT_ENTITY);
            checkOutEntity = (CheckOutEntity) getIntent().getExtras().getSerializable(Constants.CHECK_OUT_ENTITY);
            mChangePhoneFeeEntity = (ChangePhoneFeeEntity) getIntent().getExtras().getSerializable(Constants.mChangePhoneFeeEntity);
            proxyCardNo = getIntent().getExtras().getString(Constants.proxyCardNo);
            blockCardReason = getIntent().getExtras().getString(Constants.blockCardReason);
            blockCardReasonStatus = getIntent().getExtras().getString(Constants.blockCardReasonStatus);
            sceneType = getIntent().getExtras().getString(Constants.sceneType);
            checkPdType = getIntent().getExtras().getString(Constants.checkPdType);
            reason = getIntent().getExtras().getString(Constants.checkPdType_reason);
            pd_sceneType = getIntent().getExtras().getString(Constants.pd_sceneType);
            openQrCode = getIntent().getExtras().getBoolean("openQrCode");
            isFromSDK = getIntent().getExtras().getBoolean(Constants.IS_FROM_SDK);
            isOpen = getIntent().getExtras().getBoolean(Constants.isOpen, true);
            isFromEditBeneficiary = getIntent().getExtras().getBoolean(Constants.IS_FROM_EDIT_BENEFICIARY, false);
            if (kycContactEntity != null) {
                payerName = kycContactEntity.getContactsName();
            }
            if (transferEntity != null) {
                orderNo = transferEntity.orderNo;
                payerName = transferEntity.payerName;
                payMethod = transferEntity.payMethod;
                transAmount = transferEntity.orderAmount;
                exchangeRate = transferEntity.exchangeRate;
                transCurrencyCode = transferEntity.orderCurrencyCode;
                transFees = transferEntity.transFees;
                vat = transferEntity.vat;
            } else if (checkOutEntity != null) {
                orderNo = checkOutEntity.orderNo;
                if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
                    paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
                }
                orderType = "P";
            }
        }

        imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
        if (imrBeneficiaryEntity != null) {
            receiptMethod = imrBeneficiaryEntity.receiptMethod;
            payeeFullName = imrBeneficiaryEntity.payeeFullName;
            nickName = imrBeneficiaryEntity.nickName;
            relationshipCode = imrBeneficiaryEntity.relationshipCode;
            imrCallingCode = imrBeneficiaryEntity.callingCode;
            phone = imrBeneficiaryEntity.phone;
            transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;
            payeeInfoCountryCode = imrBeneficiaryEntity.payeeInfoCountryCode;
            birthPlace = imrBeneficiaryEntity.birthPlace;
            birthDate = imrBeneficiaryEntity.birthDate;
            sex = imrBeneficiaryEntity.sex;
            cityName = imrBeneficiaryEntity.cityName;
            districtName = imrBeneficiaryEntity.districtName;
            poBox = imrBeneficiaryEntity.poBox;
            buildingNo = imrBeneficiaryEntity.buildingNo;
            street = imrBeneficiaryEntity.street;
            idNo = imrBeneficiaryEntity.idNo;
            idExpiry = imrBeneficiaryEntity.idExpiry;
            bankAccountType = imrBeneficiaryEntity.bankAccountType;
            ibanNo = imrBeneficiaryEntity.ibanNo;
            bankAccountNo = imrBeneficiaryEntity.bankAccountNo;
            receiptOrgCode = imrBeneficiaryEntity.receiptOrgCode;
            receiptOrgBranchCode = imrBeneficiaryEntity.receiptOrgBranchCode;
            saveFlag = imrBeneficiaryEntity.saveFlag;
            beneCurrency = imrBeneficiaryEntity.beneCurrency;
            cityId = imrBeneficiaryEntity.cityId;
            receiptOrgName = imrBeneficiaryEntity.receiptOrgName;
            receiptOrgBranchName = imrBeneficiaryEntity.receiptOrgBranchName;
            channelPayeeId = imrBeneficiaryEntity.channelPayeeId;
            channelCode = imrBeneficiaryEntity.channelCode;
            branchId = imrBeneficiaryEntity.branchId;
        }

        stringBuilder = new StringBuilder();
        numberGridAdapter = new NumberGridAdapter(mContext);
        gridNumber.setAdapter(numberGridAdapter);
        numberGridAdapter.setOnItemClickListener(new NumberGridAdapter.OnItemClick() {
            @Override
            public void onItemClick(NumberEntity numberEntity, int position) {
                if(numberEntity != null){
                    int type = numberEntity.type;
                    int length = stringBuilder.length();
                    if(type == 1){
                        if(length == 6)return;
                        String number = numberEntity.number;
                        if(!TextUtils.isEmpty(number)) {
                            stringBuilder.append(number);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length);
                            if(checkBox != null) {
                                checkBox.setChecked(true);
                            }
                        }
                    }else if(type == 2){
                        if(stringBuilder.length()>0) {
                            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length-1);
                            if(checkBox != null) {
                                checkBox.setChecked(false);
                            }
                        }
                    }
                    if(stringBuilder.length() == 6){
                        password = stringBuilder.toString();

                        if (!isLock) {
                            if (Constants.TYPE_TRANSFER_QR.equals(pd_sceneType)) {
                                mPresenter.checkPayPassword(Constants.TYPE_TRANSFER, pd_sceneType, password, orderNo);
                            } else {
                                if (Constants.TYPE_SPLIT.equals(sceneType)) {
                                    mPresenter.checkPayPassword(Constants.TYPE_TRANSFER, Constants.TYPE_TRANSFER_RT, password, orderNo);
                                } else {
                                    mPresenter.checkPayPassword(Constants.TYPE_TRANSFER, sceneType, password, orderNo);
                                }
                            }
                        }
                    }
                }
            }
        });
        numberGridAdapter.changeData(AndroidUtils.getRegNumberList(mContext));

        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkUser(userInfoEntity);
    }

    private void setErrorMsg(String errorMsg){
        if(!TextUtils.isEmpty(errorMsg)) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(errorMsg);
        }else{
            tvError.setVisibility(View.GONE);
        }
        for(int i=0;i<6;i++) {
            CheckBox checkBox = (CheckBox) llCheck.getChildAt(i);
            checkBox.setChecked(false);
        }
        stringBuilder = new StringBuilder();
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.getAvatar();
            String gender = userInfoEntity.gender;
            if (!TextUtils.isEmpty(userInfoEntity.userName)) {
                if (userInfoEntity.userName.contains(" ")) {
                    String[] fastName = userInfoEntity.userName.split(" ");
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + fastName[0] + "!");
                } else {
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + userInfoEntity.userName + "!");
                }
                tvNameFirst.setText(userInfoEntity.userName.substring(0,1).toUpperCase(Locale.ENGLISH));
            }
            if (!TextUtils.isEmpty(avatar)) {
                conAvatar.setVisibility(View.GONE);
                ivUserAvatar.setVisibility(View.VISIBLE);
                Glide.with(this).clear(ivUserAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .placeholder(R.drawable.l_default_avatar)
                        .into(ivUserAvatar);
            } else {
                conAvatar.setVisibility(View.VISIBLE);
                ivUserAvatar.setVisibility(View.GONE);
            }
            phone = userInfoEntity.phone;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity eventEntity) {
        int eventType = eventEntity.getEventType();
        if (eventType == EventEntity.EVENT_CHANGE_PAY_PD_SUCCESS) {
            setErrorMsg("");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        UserInfoManager.getInstance().setOpenFace(false);
        if (ProjectApp.isPhoneNotSupportedFace()) {
//            ivFace.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.payment_password_logo));
//            ivFace.setEnabled(false);
            llFaceLogin.setVisibility(View.INVISIBLE);
        } else if (AndroidUtils.isOpenBiometricPay() && !Constants.TYPE_CHANGE_LOGIN_PD.equals(sceneType)) {
//            ivFace.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.fingerprint_logo));
            llFaceLogin.setVisibility(View.VISIBLE);
            showBiometric();
        } else {
            llFaceLogin.setVisibility(View.INVISIBLE);
//            ivFace.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.payment_password_logo));
        }
    }

    private void showTouchIdDialog() {
        if (ProjectApp.isBiometricSupported()) {
            customDialog = showCustomDialog(mContext, getString(R.string.FaceID_SU_0006_D_2), getString(R.string.FaceID_SU_0006_D_4), R.string.FaceID_SU_0006_D_6, R.string.FaceID_SU_0006_D_5,
                    false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (customDialog != null && customDialog.isShowing()) {
                                customDialog.dismiss();
                            }

                            HashMap<String, Object> mHashMap = new HashMap<>(16);
                            mHashMap.put(Constants.checkPdType, Constants.checkPdType_face_id);
                            mHashMap.put(Constants.isOpen, true);
                            ActivitySkipUtil.startAnotherActivity(TransferPwdActivity.this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                            BiometricInstance.getInstance().registerBiometric(TransferPwdActivity.this, false, isOpen,
//                                    new BiometricInstance.BiometricListener() {
//                                        @Override
//                                        public void onBiometricFail(String errMsg) {
//                                            showTipDialog(errMsg);
//                                        }
//
//                                        @Override
//                                        public void onBiometricSuccess() {
//                                            checkTouchSuccess();
//                                        }
//
//                                        @Override
//                                        public void onBiometricError() {
//
//                                        }
//                                    });
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (customDialog != null && customDialog.isShowing()) {
                                customDialog.dismiss();
                            }
                        }
                    });
            customDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                }
            });
        } else if (!ProjectApp.isPhoneNotSupportedFace()) {
            showErrorDialog(getString(R.string.common_22));
        }
    }

    private boolean isNeedIvr() {
        if (TextUtils.isEmpty(otpType)) return false;
        switch (otpType) {
            case Constants.OTP_FIRST_CHARITY:
                if (riskControlEntity.isNeedIvr()) {
                    CharitySummaryActivity.getCharitySummaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_WITHDRAW_ADD_IMR:
                if (riskControlEntity.isNeedIvr()) {
                    AddBeneficiaryActivity.getAddBeneficiaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_ADD_SEND_BENE:
                if (riskControlEntity.isNeedIvr()) {
                    if (AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity() != null) {
                        AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity().jumpToPwd(true);
                    } else if (AddBeneficiarySixActivity.getAddBeneficiarySixActivity() != null) {
                        AddBeneficiarySixActivity.getAddBeneficiarySixActivity().jumpToPwd(true);
                    } else if (AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity() != null) {
                        AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity().jumpToPwd(true);
                    }
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_TRANSFER_CONTACT:
                if (riskControlEntity.isNeedIvr()) {
                    TransferMoneyActivity.getTransferMoneyActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_TRANSFER_IBANK:
                if (riskControlEntity.isNeedIvr()) {
                    TransferSummaryActivity.getTransferSummaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_TRANSFER_REQUEST:
                if (riskControlEntity.isNeedIvr()) {
                    RequestCenterActivity.getRequestCenterActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_TOP_UP:
                if (riskControlEntity.isNeedIvr()) {
                    EVoucherPayReviewActivity.geteVoucherPayReviewActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_INTER_TOP_UP:
                if (riskControlEntity.isNeedIvr()) {
                    RechargeSummaryActivity.getRechargeSummaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_PAY_BILL:
                if (riskControlEntity.isNeedIvr()) {
                    PayBillSummaryActivity.getPayBillSummaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_KSA_CARD_BLOCK:
                if (riskControlEntity.isNeedIvr()) {
                    BlockCardConfirmActivity.getBlockCardConfirmActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_KSA_CARD_SET_PIN:
                if (riskControlEntity.isNeedIvr()) {
                    SetPinConfirmActivity.getSetPinConfirmActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_IMR:
                if (riskControlEntity.isNeedIvr()) {
                    ImrSendMoneySummaryActivity.getImrSendMoneySummaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_PAY_CARD:
                if (riskControlEntity.isNeedIvr()) {
                    GetNewCardSummaryActivity.getGetNewCardSummaryActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_FREEZE_CARD:
            case Constants.OTP_FIRST_CARD_DETAIL:
            case Constants.OTP_FIRST_UNFREEZE_CARD:
                if (riskControlEntity.isNeedIvr()) {
                    CardManageKsaFragment.cardManageKsaFragment.jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_PAY_CARD_NO_FEE:
                if (riskControlEntity.isNeedIvr()) {
                    GetCardPwdTwoActivity.getGetCardPwdTwoActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_SET_KSA_CARD_LIMIT:
                if (riskControlEntity.isNeedIvr()) {
                    KsaCardLimitActivity.getKsaCardLimitActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_RECEIVE_PAY:
                if (riskControlEntity.isNeedIvr()) {
                    ReceiveSplitFragment.getInstance().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_KSA_CARD_SET_CHANNEL:
                if (riskControlEntity.isNeedIvr()) {
                    KsaCardSettingActivity.getKsaCardSettingActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_PAY_CARD_ACTIVATE:
                if (riskControlEntity.isNeedIvr()) {
                    ActivateCardActivity.getActivateCardActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_REQUEST_TRANSFER:
                if (riskControlEntity.isNeedIvr()) {
                    RequestTransferActivity.getRequestTransferActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_SPLIT_BILL:
                if (riskControlEntity.isNeedIvr()) {
                    CreateSplitBillActivity.getCreateSplitBillActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_CHANGE_LOGIN_PD:
                if (riskControlEntity.isNeedIvr()) {
                    RegSetPwdTwoActivity.getRegSetPwdTwoActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            case Constants.OTP_FIRST_HyperPay_ADD_MONEY:
                if (riskControlEntity.isNeedIvr()) {
                    ConfirmAddMoneyActivity.getConfirmAddMoneyActivity().jumpToPwd(true);
                    finish();
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.none, R.anim.none);
    }


    private void showBiometric() {
        BiometricInstance.getInstance().clickWithFio(TransferPwdActivity.this, false, isOpen,
                new BiometricInstance.BiometricListener() {
                    @Override
                    public void onBiometricFail(String errMsg) {
                        showTipDialog(errMsg);
                    }

                    @Override
                    public void onBiometricSuccess() {
                        checkTouchSuccess();
                    }

                    @Override
                    public void onBiometricError() {

                    }
                });
    }

    @OnClick({R.id.ll_face_login, R.id.tv_forget_pwd, R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        switch (view.getId()) {
            case R.id.tv_forget_pwd:
                if(AndroidUtils.isCloseUse(this,Constants.profile_forgotLoginPd))return;
                if (Constants.checkPdType_face_id.equals(checkPdType)) {
                    UserInfoManager.getInstance().setOpenFace(true);
                }
                mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_FORGET_LOGIN_PD);
                ActivitySkipUtil.startAnotherActivity(this, RegisterActivity.class,
                        mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.ll_face_login:
                if (AndroidUtils.isOpenBiometricPay()) {
                    showBiometric();
                } else if (ProjectApp.isBiometricSupported()) {
                    if (Constants.checkPdType_face_id.equals(checkPdType)) {

                    } else {
                        showTouchIdDialog();
                    }
                } else if (!ProjectApp.isPhoneNotSupportedFace()) {
                    showErrorDialog(getString(R.string.common_22));
                }
                break;
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,sceneType,transferEntity,true);
    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void switchTouchIDPayment(boolean isOpen) {
        if (isOpen) {
            showToast(getString(R.string.Hint_FingerprintOpenSuccessful));
        } else {
            showTipDialog(getString(R.string.common_35));
        }
    }

    private void checkTouchSuccess() {
        if (isNeedIvr()) return;
        if (openQrCode) {
            mPresenter.checkOut("", "");
        } else {
            if (Constants.checkPdType_face_id.equals(checkPdType)) {
                finish();
            } else {
                verifyPwdSuccess();
            }
        }
    }

    /**
     * 验密成功后的处理
     */
    private void verifyPwdSuccess() {
        if(Constants.TYPE_CHANGE_PHONE_FEE.equals(sceneType)){
            if(mChangePhoneFeeEntity != null) {
                mPresenter.payChangePhoneFee(mChangePhoneFeeEntity.totalAmount,mChangePhoneFeeEntity.discountVat);
            }
        }else if (Constants.TYPE_HyperPay_ADD_MONEY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_HyperPay_ADD_MONEY));
            finish();
        } else if (Constants.TYPE_CHARITY.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_ADD_SEND_BENE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ADD_SEND_BENEFICIARY));
            finish();
        } else if (Constants.TYPE_CHANGE_LOGIN_PD.equals(sceneType)) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            mHashMap.put(Constants.isChangeLoginPd, true);
            CheckPhoneEntity checkPhoneEntity = new CheckPhoneEntity();
            checkPhoneEntity.oldPassword = password;
            checkPhoneEntity.checkIdNumber = userInfoEntity.idNumber;
            checkPhoneEntity.phone = userInfoEntity.phone;
            UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
            ActivitySkipUtil.startAnotherActivity(this, RegSetPwdOneActivity.class, mHashMap, RIGHT_IN);
            finish();
        } else if (Constants.TYPE_WITHDRAW_ADD_BENEFICIARY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_WITHDRAW_ADD_BENEFICIARY));
            finish();
        } else if (Constants.TYPE_KSA_CARD_FREEZE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_FREEZE));
            finish();
        } else if (Constants.TYPE_KSA_CARD_ACTIVATE.equals(sceneType)) {
            mPresenter.activateCard(ksaCardEntity.proxyCardNo, ksaCardEntity.cardNumber, ksaCardEntity.expiryTime);
        } else if (Constants.TYPE_KSA_CARD_SET_CHANNEL.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_SET_CHANNEL));
            finish();
        } else if (Constants.TYPE_KSA_CARD_DETAIL.equals(sceneType)) {
            mPresenter.getKsaCardDetails(ksaCardEntity.proxyCardNo);
        } else if (Constants.TYPE_KSA_CARD_SET_PIN.equals(sceneType)) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            mHashMap.put(Constants.proxyCardNo, proxyCardNo);
            ActivitySkipUtil.startAnotherActivity(TransferPwdActivity.this, GetCardPwdOneActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.TYPE_KSA_CARD_BLOCK.equals(sceneType)) {
            mPresenter.setKsaCardStatus(Constants.TYPE_CARD_BLOCK, proxyCardNo, blockCardReason, blockCardReasonStatus);
        } else if (Constants.TYPE_KSA_CARD_SET_LIMIT.equals(sceneType)) {
            String txnType = mCardLimitConfigEntity.txnType;
            String limitMoney = mCardLimitConfigEntity.isDaily ? mCardLimitConfigEntity.dailyLimitValue : mCardLimitConfigEntity.monthlyLimitValue;
            mPresenter.setKsaCardLimit(txnType,mCardLimitConfigEntity.isDaily ? AndroidUtils.getReqTransferMoney(limitMoney) : "",
                    mCardLimitConfigEntity.isDaily ? "" : AndroidUtils.getReqTransferMoney(limitMoney), mCardLimitConfigEntity.proxyCardNo, mCardLimitConfigEntity.up);
        } else if (Constants.TYPE_KSA_CARD_UNFREEZE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_FREEZE));
            finish();
        } else if (Constants.TYPE_PAY_CARD_NO_FEE.equals(sceneType)) {
            mPresenter.getKsaCardPayResult("", "", openCardReqEntity);
        } else if (Constants.TYPE_PAY_CARD.equals(sceneType)) {
            mPresenter.getKsaCardPayResult(ksaCardSummaryEntity.vat, ksaCardSummaryEntity.totalAmount, ksaCardSummaryEntity.openCardReqEntity);
        } else if (Constants.TYPE_IMR_PAY.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_PAY_BILL.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_INTER_TOP_UP.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_IMR_ADD_BENEFICIARY.equals(sceneType)) {
            if (isFromEditBeneficiary) {
                beneficiaryInfo = (ImrBeneficiaryDetails.ImrBeneficiaryDetail) getIntent().getSerializableExtra("BeneficiaryInfo");
                mPresenter.saveImrBeneficiaryInfo(beneficiaryInfo);
            } else {
                mPresenter.imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                        nickName, relationshipCode, imrCallingCode, phone,
                        transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                        birthDate, sex, cityName, districtName,
                        poBox, buildingNo, street, idNo, idExpiry,
                        bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
                        cityId, beneCurrency, channelPayeeId, channelCode, branchId);
            }
        } else if (Constants.TYPE_SPLIT.equals(sceneType)) {
            if (transferEntity != null) {
                mPresenter.splitBill(transferEntity.orderNo, AndroidUtils.getReqTransferMoney(transferEntity.orderAmount),
                        transferEntity.orderCurrencyCode, transferEntity.remark,
                        transferEntity.receiverInfoList);
            }
        } else if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
            if (kycContactEntity != null) {
                mPresenter.transferToContact(kycContactEntity.getUserId(), SpUtils.getInstance().getCallingCode(), kycContactEntity.getRequestPhoneNumber(), userInfoEntity.userId,
                        userInfoEntity.getPhone(), AndroidUtils.getReqTransferMoney(kycContactEntity.payMoney), UserInfoManager.getInstance().getCurrencyCode(),
                        kycContactEntity.remark, sceneType, "", "1", "", "", "", payerName);
            }
        } else {
            if (Constants.TYPE_TRANSFER_RC.equals(sceneType)) {
                mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            } else if (Constants.TYPE_RECEIVE_SPLIT_BILL.equals(sceneType)) {
                mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            } else if (Constants.TYPE_SCAN_MERCHANT.equals(sceneType)) {
                mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
            } else {
                mPresenter.transferSurePay(sceneType, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            }
        }
    }

    @Override
    public void checkPayPwdSuccess(TransferPwdTimeEntity transferPwdTimeEntity) {
        if (transferPwdTimeEntity != null) {
            lockTime = transferPwdTimeEntity.lockTime;
            int time = Integer.parseInt(lockTime);
            SpUtils.getInstance().setLockTime(time + (int) (System.currentTimeMillis() / 1000));
            String errMsg = transferPwdTimeEntity.errMsg;
            SpUtils.getInstance().setLockTimeMsg(errMsg);
            if (!TextUtils.isEmpty(lockTime)) {
                llOtpTime.setVisibility(View.VISIBLE);
                setResetTime(true, "", time);
                setErrorMsg(errMsg);
            }
        } else {
            if (isNeedIvr()) return;
            if (openQrCode) {
                mPresenter.checkOut("", "");
            } else {
                if (Constants.checkPdType_face_id.equals(checkPdType)) {
                    if (isOpen) {
                        BiometricInstance.getInstance().registerBiometric(TransferPwdActivity.this, false, isOpen,
                                new BiometricInstance.BiometricListener() {
                                    @Override
                                    public void onBiometricFail(String errMsg) {
                                        showTipDialog(errMsg);
                                    }

                                    @Override
                                    public void onBiometricSuccess() {
                                        checkTouchSuccess();
                                    }

                                    @Override
                                    public void onBiometricError() {

                                    }
                                });
                    } else {
                        AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(isOpen);
                        checkTouchSuccess();
                    }
                } else {
                    verifyPwdSuccess();
                }
            }
        }
    }

    @Override
    public void checkPayPwdFail(String errorCode, String errorMsg) {
        setErrorMsg(errorMsg);
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        transferToContactSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void transferToContactFail(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        } else {
            showTipDialog(errMsg);
        }
    }

    @Override
    public void confirmPaySuccess(TransferEntity transferEntity) {
        confirmPaySuccess(this,sceneType,checkOutEntity,isFromSDK,transferEntity);
    }

    @Override
    public void confirmPayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        if (checkOutEntity != null) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("checkOutEntity", checkOutEntity);
            setResult(RESULT_OK, resultIntent);
            finish();
        }
//        if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
//            paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
//            SpUtils.getInstance().setPayMethodCode(paymentMethodNo);
//        }
//        ProjectApp.openQrCode = true;

    }

    @Override
    public void checkOutFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {
        splitBillSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {
//        etPassword.setError(errorMsg);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(errorMsg);
    }

    @Override
    public void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity) {
        if (imrAddBeneResultEntity != null) {
            imrBeneficiaryEntity.payeeInfoId = imrAddBeneResultEntity.payeeInfoId;
        }
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_ADD_BENE_SUCCESS, imrBeneficiaryEntity, riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrAddBeneficiaryFail(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        } else {
            showTipDialog(errorMsg);
        }
    }


    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }


    @Override
    public void imrEditBeneficiarySuccess(ResponseEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_EDIT_BENE_SUCCESS, beneficiaryInfo,riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrEditBeneficiaryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {
        if (Constants.TYPE_KSA_CARD_BLOCK.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_BLOCK));
        }
        ProjectApp.removeAllTaskExcludeMainStack();
        finish();
    }

    public void setResetTime(boolean isReset, String phoneNo, int time) {
        if (isReset) {
            AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, time, new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (isFinishing()) {
                        return;
                    }
                    isLock = true;
                    if (millisUntilFinished < 60 && millisUntilFinished >= 0) {
                        tvShowTimeExpire.setText(getString(R.string.wtw_35));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        if (LocaleUtils.isRTL(mContext)) {
                            tvOtpTime.setText("s " + millisUntilFinished);
                        } else {
                            tvOtpTime.setText(millisUntilFinished + " s");
                        }
                    } else {
                        tvShowTimeExpire.setText(getString(R.string.wtw_35));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        int second = millisUntilFinished % 60;
                        if (second < 10) {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":0" + second);
                        } else {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":" + second);
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (isFinishing()) {
                        return;
                    }
                    isLock = false;
                    tvShowTimeExpire.setText(getString(R.string.wtw_35));
                    tvOtpTime.setVisibility(View.VISIBLE);
                    tvOtpTime.setText(0 + "s");
                    lockMsg = "";
                    setErrorMsg(lockMsg);
                    SpUtils.getInstance().setLockTime(0);
                    SpUtils.getInstance().setLockTimeMsg("");
                }
            });
        } else {
            AdvancedCountdownTimer.getInstance().startCountDown(phoneNo, SystemInitManager.getInstance().getSystemInitEntity().getOtpIntervalTime(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (isFinishing()) {
                        return;
                    }
                    isLock = true;
                    if (millisUntilFinished < 60 && millisUntilFinished >= 0) {
                        tvShowTimeExpire.setText(getString(R.string.wtw_35));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        tvOtpTime.setText(millisUntilFinished + "s");
                    } else {
                        tvShowTimeExpire.setText(getString(R.string.wtw_35));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        int second = millisUntilFinished % 60;
                        if (second < 10) {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":0" + second);
                        } else {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":" + second);
                        }
                    }

                }

                @Override
                public void onFinish() {
                    if (isFinishing()) {
                        return;
                    }
                    isLock = false;
                    tvShowTimeExpire.setText(getString(R.string.wtw_35));
                    tvOtpTime.setVisibility(View.VISIBLE);
                    tvOtpTime.setText(0 + "s");
                    lockMsg = "";
                    setErrorMsg(lockMsg);
                    SpUtils.getInstance().setLockTime(0);
                    SpUtils.getInstance().setLockTimeMsg("");
                }
            });
        }
    }

    @Override
    public TransferContract.TransferSurePayPresenter getPresenter() {
        return new TransferSurePayPresenter();
    }

    @Override
    public void showErrorMsg(final String errorCode, final String errorMsg) {
        if ("030194".equals(errorCode)) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.cardResultType, KsaCardResultActivity.cardResultType_Open_Card_fail);
            mHashMap.put(Constants.cardResultErrorMsg, errorMsg);
            ActivitySkipUtil.startAnotherActivity(this, KsaCardResultActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void getKsaCardPayResultSuccess(KsaPayResultEntity ksaPayResultEntity) {
        if (ksaPayResultEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksa_cardType, ksaPayResultEntity.cardType);
            if (ksaCardSummaryEntity != null) {
                mHashMap.put(Constants.ksa_deliveryTime, ksaCardSummaryEntity.deliveryTime);
            }
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void activateCardSuccess(Void result) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.ksaCardEntity, ksaCardEntity);
        mHashMap.put(Constants.cardSuccessType, 3);
        ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_SET_LIMIT_SUCCESS));
        finish();
    }

    @Override
    public void getKsaCardDetailsSuccess(CardDetailEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_SHOW_DETAIL, result));
        finish();
    }

    @Override
    public void payChangePhoneFeeSuccess(ChangePhoneResultEntity changePhoneResultEntity) {
        if(changePhoneResultEntity != null){
            String orderNo = changePhoneResultEntity.orderNo;
            //订单状态 S成功 E失败
            String orderStatus = changePhoneResultEntity.orderStatus;
            if(changePhoneResultEntity.isOrderSuccess()){
                HashMap<String, Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_CHANGE_PHONE);
                mHashMap.put(Constants.mChangePhoneOrderNo, orderNo);
                ActivitySkipUtil.startAnotherActivity(this, RegisterActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_CHANGE_PHONE_PAY_SUCCESS));
                finish();
            }
        }
    }
}
