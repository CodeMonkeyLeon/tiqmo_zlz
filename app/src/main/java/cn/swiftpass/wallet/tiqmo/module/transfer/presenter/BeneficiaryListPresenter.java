package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class BeneficiaryListPresenter implements TransferContract.BeneficiaryListPresenter {

    TransferContract.BeneficiaryListView beneficiaryListView;

    @Override
    public void getTime(String ibanNo) {
        AppClient.getInstance().getTransferTime(ibanNo, new LifecycleMVPResultCallback<TimeEntity>(beneficiaryListView, false) {
            @Override
            protected void onSuccess(TimeEntity result) {
                beneficiaryListView.getTimeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                beneficiaryListView.getTimeFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(beneficiaryListView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                beneficiaryListView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                beneficiaryListView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getBeneficiaryList() {
        AppClient.getInstance().getBeneficiaryList(new LifecycleMVPResultCallback<BeneficiaryListEntity>(beneficiaryListView, true) {
            @Override
            protected void onSuccess(BeneficiaryListEntity result) {
                beneficiaryListView.getBeneficiaryListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                beneficiaryListView.getBeneficiaryListFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void deleteBeneficiary(String id) {
        AppClient.getInstance().deleteBeneficiary(id, new LifecycleMVPResultCallback<Void>(beneficiaryListView, true) {
            @Override
            protected void onSuccess(Void result) {
                beneficiaryListView.deleteBeneficiarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                beneficiaryListView.deleteBeneficiaryFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(TransferContract.BeneficiaryListView beneficiaryListView) {
        this.beneficiaryListView = beneficiaryListView;
    }

    @Override
    public void detachView() {
        this.beneficiaryListView = null;
    }
}
