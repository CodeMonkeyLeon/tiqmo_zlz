package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CardDetailEntity extends BaseEntity {
    /**
     * {\"cvv2\":\"25A\",\"partnerNo\":\"96601\",\"expireTimes\":\"60\",\"expireDate\":\"27/05\",\"cardNo\":\"5249400004160646\"}}"
     */
    public String cardNo;

    public String expireDate;

    public String cvv2;

    public String expireTimes;

    public String maskedCardNo;

    public String cardHolderName;
}
