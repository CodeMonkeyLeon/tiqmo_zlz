package cn.swiftpass.wallet.tiqmo.module.home.view;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.TransferHistoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryListParamEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.home.interfaces.HistoryFragmentImp;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.WrapContentLinearLayoutManager;
import cn.swiftpass.wallet.tiqmo.widget.adapter.ItemTouchHelperListener;
import cn.swiftpass.wallet.tiqmo.widget.adapter.SimpleItemTouchHelperCallback;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class BaseHistoryFragment extends BaseFragment implements ItemTouchHelperListener, HistoryFragmentImp {

    public static final int FILTER_REQUEST = 1001;

    protected TransferHistoryMainEntity mTempPreAuthHistory;

    protected boolean mIsPreAuth = false;

    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;

    @BindView(R.id.ll_filter)
    LinearLayout llFilter;

    @BindView(R.id.sw_history)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ry_transaction_history)
    RecyclerView ryTransactionHistory;

    @BindView(R.id.view_bottom)
    View viewBottom;


    private int pageSize = 30;
    protected int currentPage = 1;

    private TransferHistoryAdapter transferHistoryAdapter;
    public List<TransferHistoryEntity> historyEntityList = new ArrayList<>();

    public List<String> typeList = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();

    private String startDate = "", endDate = "";

    private StatusView historyStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private FilterEntity filterEntity;
    private HashMap<Integer, String> selectedArray = new HashMap<>();
    private String direction, orderNo;

    private SimpleItemTouchHelperCallback itemTouchHelperCallback;
    ItemTouchHelper historyTouchHelper;

    private int historyPosition;
    private int clickPosition;

    private FilterListEntity filterListEntity;

    private WrapContentLinearLayoutManager wrapContentLinearLayoutManager;

    protected String isFrom;

    protected boolean isClick;


    @Override
    protected int getLayoutID() {
        return R.layout.fragment_transation_history;
    }


    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);

        if (noHistoryView != null) {
            helper.setImageResourceByAttr(noHistoryView.findViewById(R.id.iv_no_content), R.attr.no_transactions);
        }

        if (transferHistoryAdapter != null) {
            transferHistoryAdapter.changeTheme();
        }
    }


    @Override
    public void isClick(boolean isClick) {
        this.isClick = isClick;
    }

    public boolean getClickStatus() {
        return isClick;
    }


    @Override
    public void showFilterHistoryDialog(@NonNull FilterListEntity listFilter) {
        showFilterDialog(listFilter);
    }


    public void setRefreshing(boolean isRefreshing) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(isRefreshing);
        }
    }


    private void updatePreAuthHistory() {
        if (mIsPreAuth) {
            EventBus.getDefault().postSticky(
                    new EventEntity(
                            EventEntity.EVENT_PRE_AUTH_HISTORY_REFRESH,
                            "preAuthHis"
                    )
            );
        }
    }


    private void postHistory() {
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_HISTORY_REFRESH,
                        new HistoryListParamEntity(
                                "",
                                "",
                                typeList,
                                tradeType,
                                startDate,
                                endDate,
                                pageSize,
                                currentPage,
                                direction,
                                orderNo
                        )
                )
        );
    }


    private void postPreAuthHistory() {
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_PRE_AUTH_HISTORY_REFRESH,
                        new HistoryListParamEntity(
                                "",
                                null,
                                null,
                                null,
                                startDate,
                                endDate,
                                pageSize,
                                currentPage,
                                direction,
                                orderNo
                        )
                )
        );
    }


    private void pullToRefresh() {
        currentPage = 1;
        orderNo = "";
        direction = "";
        if (mIsPreAuth) {
            //预授权
            postPreAuthHistory();
        } else {
            //交易历史
            postHistory();
            postPreAuthHistory();
        }
    }


    @Override
    protected void initView(View parentView) {
        swipeRefreshLayout.setColorSchemeResources(R.color.color_B00931);
        filterEntity = new FilterEntity();
        if (getArguments() != null) {
            isFrom = getArguments().getString(Constants.isFrom);
            mIsPreAuth = getArguments().getBoolean(Constants.HISTORY_TYPE, false);
            if (Constants.from_evoucher.equals(isFrom)) {
                viewBottom.setVisibility(View.GONE);
            } else if (Constants.from_notification_center.equals(isFrom)) {
                viewBottom.setVisibility(View.GONE);
            }
        }

        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        historyStatusView = new StatusView.Builder(mContext, ryTransactionHistory).setNoContentMsg(getString(R.string.History_filter_13))
                .setNoContentView(noHistoryView).build();

        wrapContentLinearLayoutManager = new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        ryTransactionHistory.setLayoutManager(wrapContentLinearLayoutManager);
        transferHistoryAdapter = new TransferHistoryAdapter(historyEntityList, mActivity);

        itemTouchHelperCallback = new SimpleItemTouchHelperCallback(this, 1);
        historyTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        //优化需求Transaction history历史记录不能删除
//        historyTouchHelper.attachToRecyclerView(ryTransactionHistory);

        transferHistoryAdapter.bindToRecyclerView(ryTransactionHistory);
        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
        LocaleUtils.viewRotationY(mContext, scFilter, llFilter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh();
            }
        });

        transferHistoryAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                currentPage += 1;
                direction = "down";
                if (mIsPreAuth) {
                    //预授权
                    postPreAuthHistory();
                } else {
                    //交易历史
                    postHistory();
                }
            }
        }, ryTransactionHistory);

        transferHistoryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                clickPosition = position;
                TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
                if (transferHistoryEntity != null) {
                    HistoryDetailEntity detail = new HistoryDetailEntity(
                            transferHistoryEntity.orderNo,
                            transferHistoryEntity.orderType,
                            ""
                    );
                    if (mIsPreAuth) {
                        //预授权
                        postPreAuthHistoryDetail(detail);
                    } else {
                        //交易历史
                        postHistoryDetail(detail);
                    }
                }
            }
        });
    }


    private void postHistoryDetail(HistoryDetailEntity detailEntity) {
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_HISTORY_DETAIL,
                        detailEntity
                )
        );
    }

    private void postPreAuthHistoryDetail(HistoryDetailEntity detailEntity) {
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_PRE_AUTH_HISTORY_DETAIL,
                        detailEntity
                )
        );
    }


    @Override
    protected void initData() {

    }


    protected void showFilterDialog(FilterListEntity filterListEntity) {
        FilterHistoryDialogFragment fragment = FilterHistoryDialogFragment.newHistoryInstance(
                filterEntity,
                filterListEntity,
                mIsPreAuth);
        fragment.setTargetFragment(BaseHistoryFragment.this, FILTER_REQUEST);
        if (getFragmentManager() != null) {
            fragment.show(getFragmentManager(), "FilterHistory");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTER_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    filterEntity = (FilterEntity) data.getSerializableExtra("filterEntity");
                    refreshList(filterEntity);
                }
            }
        }
    }


    @Override
    public HistoryListParamEntity updateGetListStatus() {
        setRefreshing(true);
        if (llFilter != null) {
            llFilter.removeAllViews();
        }
        filterEntity = new FilterEntity();
        typeList = new ArrayList<>();
        tradeType = new ArrayList<>();
        startDate = "";
        endDate = "";
        currentPage = 1;
        orderNo = "";
        direction = "";
        return new HistoryListParamEntity(
                "",
                "",
                typeList,
                tradeType,
                startDate,
                endDate,
                pageSize,
                currentPage,
                direction,
                orderNo
        );
    }


    protected void refreshList(FilterEntity filterEntity) {
        if (filterEntity != null) {
            String showStartDate = "", showEndDate = "";
            startDate = filterEntity.startDate;
            showStartDate = filterEntity.showStartDate;
            endDate = filterEntity.endDate;
            showEndDate = filterEntity.showEndDate;
            typeList = filterEntity.paymentType;
            tradeType = filterEntity.tradeType;
            selectedArray = filterEntity.selectedArray;
            categoryList = filterEntity.categoryList;
            List<FilterCodeEntity> filterList = new ArrayList<>();

            if (TextUtils.isEmpty(startDate) && !TextUtils.isEmpty(endDate)) {
                showStartDate = "...";
            }

            if (!TextUtils.isEmpty(startDate) && TextUtils.isEmpty(endDate)) {
                showEndDate = getString(R.string.History_filter_14);
            }

            if (!TextUtils.isEmpty(startDate) || !TextUtils.isEmpty(endDate)) {
                FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                filterCodeEntity.type = 3;
                filterCodeEntity.title = showStartDate + "-" + showEndDate;
                filterList.add(filterCodeEntity);
            }

            int typeSize = typeList.size();
            for (int i = 0; i < typeSize; i++) {
                FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                filterCodeEntity.type = 2;
                String type = typeList.get(i);
                filterCodeEntity.value = type;
                if ("P".equals(type)) {
                    filterCodeEntity.title = getString(R.string.History_filter_5);
                } else if ("I".equals(type)) {
                    filterCodeEntity.title = getString(R.string.History_filter_6);
                }
                filterList.add(filterCodeEntity);
            }

            int categorySize = categoryList.size();
            for (int i = 0; i < categorySize; i++) {
                FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                filterCodeEntity.type = 1;
                filterCodeEntity.title = categoryList.get(i);
                filterCodeEntity.value = tradeType.get(i);
                filterList.add(filterCodeEntity);
            }

            initFilter(filterList);
            currentPage = 1;
            orderNo = "";
            direction = "";
            showProgress(true);

            if (mIsPreAuth) {
                //预授权
                postPreAuthHistory();
            } else {
                //交易历史
                postHistory();
            }
        }
    }


    private void initFilter(List<FilterCodeEntity> filterList) {
        if (filterList != null && filterList.size() > 0) {
            scFilter.setVisibility(View.VISIBLE);
            llFilter.removeAllViews();
            int size = filterList.size();
            for (int i = 0; i < size; i++) {
                final FilterCodeEntity filterCodeEntity = filterList.get(i);
                if (filterCodeEntity != null) {
                    String title = filterCodeEntity.title;
                    final View view = LayoutInflater.from(mContext).inflate(R.layout.item_top_filter, null);
                    TextView tvTopFilter = view.findViewById(R.id.tv_top_filter);
                    ImageView ivDelete = view.findViewById(R.id.iv_filter_delete);
                    if (!TextUtils.isEmpty(title)) {
                        tvTopFilter.setText(title);
                    }
                    ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int type = filterCodeEntity.type;
                            switch (type) {
                                case 1:
                                    tradeType.remove(filterCodeEntity.value);
                                    categoryList.remove(filterCodeEntity.title);
                                    // 键和值
                                    Integer key = null;
                                    String value = null;
                                    // 获取键值对的迭代器
                                    Iterator it = selectedArray.entrySet().iterator();
                                    while (it.hasNext()) {
                                        HashMap.Entry entry = (HashMap.Entry) it.next();
                                        key = (Integer) entry.getKey();
                                        value = (String) entry.getValue();
                                        if (filterCodeEntity.value.equals(value)) {
                                            it.remove();
                                        }
                                        System.out.println("key:" + key + "---" + "value:" + value);
                                    }
                                    break;
                                case 2:
                                    typeList.remove(filterCodeEntity.value);
                                    break;
                                case 3:
                                    startDate = "";
                                    endDate = "";
                                    filterEntity.showStartDate = "";
                                    filterEntity.showEndDate = "";
                                    filterEntity.startDate = "";
                                    filterEntity.endDate = "";
                                    break;
                                default:
                                    break;
                            }
                            llFilter.removeView(view);

                            currentPage = 1;
                            orderNo = "";
                            direction = "";
                            showProgress(true);

                            if (mIsPreAuth) {
                                //预授权
                                postPreAuthHistory();
                            } else {
                                //交易历史
                                postHistory();
                            }
                        }
                    });
                    llFilter.addView(view);
                }
            }
        } else {
            scFilter.setVisibility(View.GONE);
        }
    }


    @Override
    protected void restart() {

    }


    @Override
    public void getHistoryListSuccess(TransferHistoryMainEntity transferHistoryMainEntity) {
        showProgress(false);
        List<TransferHistoryEntity> tempHistoryList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(false);
        if (transferHistoryMainEntity != null) {
            List<TransferHistoryListEntity> historyListEntityList = transferHistoryMainEntity.transactionHistorys;
            int size = historyListEntityList.size();
            for (int i = 0; i < size; i++) {
                TransferHistoryListEntity historyListEntity = historyListEntityList.get(i);
                if (historyListEntity != null) {
                    tempHistoryList.addAll(historyListEntity.transactionListInfos);
                }
            }
        }

        if (currentPage == 1) {
            ryTransactionHistory.scrollToPosition(0);
            historyEntityList.clear();
            if (tempHistoryList.size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                transferHistoryAdapter.setDataList(tempHistoryList);
            }
        } else {
            if (tempHistoryList.size() == 0) {
                transferHistoryAdapter.loadMoreEnd(false);
            } else {
                transferHistoryAdapter.addData(tempHistoryList);
                if (tempHistoryList.size() < pageSize) {
                    transferHistoryAdapter.loadMoreEnd(false);
                } else {
                    transferHistoryAdapter.loadMoreComplete();
                }
            }
        }
        historyEntityList.addAll(tempHistoryList);
        if (historyEntityList.size() > 0) {
            TransferHistoryEntity lastEntity = historyEntityList.get(historyEntityList.size() - 1);
            if (lastEntity != null) {
                orderNo = lastEntity.orderNo;
            }
        }
    }

    @Override
    public void getHistoryListFail(String errorCode, String errorMsg) {
        swipeRefreshLayout.setRefreshing(false);
        showProgress(false);
        showTipDialog(errorMsg);
        if (currentPage == 1) {
            showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
        }
    }


    @Override
    public void getHistoryDetailSuccess(TransferHistoryDetailEntity transferHistoryDetailEntity) {
//        wrapContentLinearLayoutManager.scrollToPositionWithOffset(clickPosition,0);
        showProgress(false);
        AndroidUtils.jumpToTransferDetail(mActivity, transferHistoryDetailEntity, "", true);
    }

    @Override
    public void getHistoryDetailFail(String errorCode, String errorMsg) {
        showProgress(false);
        showTipDialog(errorMsg);
    }

    @Override
    public void deleteTransferHistorySuccess(Void result) {
        showProgress(false);
        if (transferHistoryAdapter != null) {
            //移除数据
            transferHistoryAdapter.getDataList().remove(historyPosition);
            transferHistoryAdapter.notifyDataSetChanged();
            if (transferHistoryAdapter.getDataList().size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void deleteTransferHistoryFail(String errorCode, String errorMsg) {
        showProgress(false);
        showTipDialog(errorMsg);
    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDissmiss(int position, int type) {
        historyPosition = position;
        TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
        if (transferHistoryEntity != null) {
            if (mIsPreAuth) {
                //预授权
                postPreAuthHistoryDelete(transferHistoryEntity.orderNo);
            } else {
                //交易历史
                postHistoryDelete(transferHistoryEntity.orderNo);
            }
        }
    }


    private void postHistoryDelete(String orderNo) {
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_HISTORY_DELETE,
                        orderNo
                )
        );
    }


    private void postPreAuthHistoryDelete(String orderNo) {
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_PRE_AUTH_HISTORY_DELETE,
                        orderNo
                )
        );
    }


    @Override
    public void getFilterListSuccess(@Nullable FilterListEntity result) {
        showProgress(false);
        filterListEntity = result;
        if (Constants.from_evoucher.equals(isFrom)) {
            List<FilterCodeEntity> filterOrderConditions = filterListEntity.filterOrderConditions;
            int size = filterOrderConditions.size();
            for (int i = 0; i < size; i++) {
                FilterCodeEntity filterCodeEntity = filterOrderConditions.get(i);
                if ("15".equals(filterCodeEntity.value)) {
                    tradeType.add(filterCodeEntity.value);
                    categoryList.add(filterCodeEntity.title);
                    selectedArray.put(i, filterCodeEntity.value);
                    filterEntity.tradeType = tradeType;
                    filterEntity.categoryList = categoryList;
                    filterEntity.selectedArray = selectedArray;
                    refreshList(filterEntity);
                }
            }
        }
        if (isClick) {
            showFilterDialog(result);
        }
    }

    @Override
    public void getFilterListFailed(@Nullable String errorCode, @Nullable String errorMsg) {
        showProgress(false);
    }


    @Override
    public void hideProcess() {
        if (mProgressDialog != null) {
            showProgress(false);
        }
    }
}
