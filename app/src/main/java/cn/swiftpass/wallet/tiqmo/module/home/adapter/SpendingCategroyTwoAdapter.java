package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingCategoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SpendingCategroyTwoAdapter extends BaseRecyclerAdapter<SpendingCategoryDetailEntity> {
    public SpendingCategroyTwoAdapter(@Nullable List<SpendingCategoryDetailEntity> data) {
        super(R.layout.item_spending_category_two, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SpendingCategoryDetailEntity spendingCategoryDetailEntity, int position) {
        try {
            if (position == mDataList.size() - 1) {
                baseViewHolder.setVisible(R.id.tv_line, false);
            } else {
                baseViewHolder.setVisible(R.id.tv_line, true);
            }
            RoundedImageView ivCategory = baseViewHolder.getView(R.id.iv_spending_category);
            int sceneType = spendingCategoryDetailEntity.sceneType;
            int subSceneType = spendingCategoryDetailEntity.subSceneType;
            int drawableId = AndroidUtils.getHistoryDrawable(mContext, sceneType);
            if(sceneType == 24){
                drawableId = AndroidUtils.getLoopHistoryDrawable(mContext,subSceneType);
            }
            String logo = spendingCategoryDetailEntity.logo;
            if (drawableId == -1) {
                ivCategory.setOval(true);
                if (sceneType == 15) {
                    ivCategory.setOval(false);
                }
                Glide.with(mContext)
                        .load(logo)
                        .dontAnimate()
                        .placeholder((sceneType != 15 && sceneType != 5) ? ThemeSourceUtils.getDefAvatar(mContext, spendingCategoryDetailEntity.sex) : -1)
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivCategory);
            } else {
                ivCategory.setOval(false);
                ivCategory.setImageResource(drawableId);
            }
            baseViewHolder.setText(R.id.tv_spending_title, spendingCategoryDetailEntity.orderDesc);
            baseViewHolder.setText(R.id.tv_spending_content, spendingCategoryDetailEntity.createTime);
            baseViewHolder.setText(R.id.tv_spending_money, AndroidUtils.getTransferStringMoney(spendingCategoryDetailEntity.orderAmount) + " " + LocaleUtils.getCurrencyCode(""));
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
