package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class KsaCardSummaryEntity extends BaseEntity {

    /**
     * {\"totalAmount\":1560.000000000000,\"partnerNo\":\"96601\",\"deliveryTime\":\"1 Week\",\"vat\":60.000000000000,\"physicalCardAmount\":1500.000000}}
     */

    //实体卡卡费（金额：单位最小币种）。当请求参数cardType为空的时候，表示获取获取卡费接口，此刻必返回 ，当请求参数cardType必传的时候，表示summary接口,该值依据类型是否返回。
    public String physicalCardAmount;
    //	虚拟卡卡费（金额：单位最小币种）。当请求参数cardType为空的时候，表示获取获取卡费接口，此刻必返回 ，当请求参数cardType必传的时候，表示summary接口,该值依据类型是否返回。
    public String virtualCardAmount;
    //表示summary接口的时候必返回 vat(金额：单位最小币种)
    public String vat;
    //表示summary接口的时候必返回 总交易金额(金额：单位最小币种)
    public String totalAmount;
    //	表示summary接口的时候且为实体卡必返回 实体卡投递周期
    public String deliveryTime;
    //白金卡卡费（金额：单位最小币种）。 当获取标准卡费 和 白金卡费的时候会返回，如果没返回表示该卡免费
    public String platinumCardAmount;

    public OpenCardReqEntity openCardReqEntity;

    public RiskControlEntity riskControlInfo;
}
