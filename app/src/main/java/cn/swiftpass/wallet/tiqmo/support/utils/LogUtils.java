package cn.swiftpass.wallet.tiqmo.support.utils;

import android.util.Log;

public class LogUtils {

    private LogUtils() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static final boolean isLogDebug = true;
    public static final boolean isWriteLog = true;

    public static void d(String tag, String msg) {
        if (isLogDebug) {
            Log.d(tag, "centerbank---" + msg + "---");
        }
    }

    public static void d(Object object, String msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }

    public static void d(Object object, Object msg){
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }

    public static void d(String object, Object msg){
        if (isLogDebug) {
            Log.d(object, "centerbank---" + msg + "---");
        }
    }

    public static void i(String tag, String msg) {
        if (isLogDebug) {
            Log.i(tag, "centerbank---" + msg + "---");
        }
    }

    public static void i(Object object, String msg) {
        if (isLogDebug) {
            Log.i(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }

    public static void i(Object object, Object msg){
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }

    public static void w(String tag, String msg) {
        if (isLogDebug) {
            Log.w(tag, "centerbank---" + msg + "---");
        }
    }

    public static void w(Object object, String msg) {
        if (isLogDebug) {
            Log.w(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }

    public static void w(Object object, Object msg){
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }

    public static void e(String tag, String msg) {
        if (isLogDebug) {
            Log.e(tag, "centerbank---" + msg + "---");
        }
    }

    public static void e(Object object, String msg) {
        if (isLogDebug) {
            Log.e(object.getClass().getSimpleName(), "centerbank---" + msg + "---");
        }
    }



}
