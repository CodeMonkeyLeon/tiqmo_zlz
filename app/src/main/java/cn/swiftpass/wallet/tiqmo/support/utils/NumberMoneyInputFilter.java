package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

import java.util.regex.Pattern;

public class NumberMoneyInputFilter implements InputFilter {
    public static final int MONEY = 9166;

    private int digits = 2;

    private final String STR_NUMBER = "0-9";

    private Pattern mPattern;

    public NumberMoneyInputFilter() {
        mPattern = Pattern.compile("[" + STR_NUMBER  + "]*");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        CharSequence out = source;
        String result = dest.toString();
        result = result.replace(",", "");

//        // if changed, replace the source
        if (out != null) {
            source = out;
            start = 0;
            end = out.length();
        }


        int len = end - start;
        if (len == 0) {
            return source;
        }

        if (source != null &&"0".equals(source.toString())&&result.length()>0&&dstart==0) {
            return "";
        }

        //如果起始位置为0,且第二位跟的不是".",则无法后续输入
        if ("0".equals(source.toString())&&result.startsWith("0")) {
            return "";
        }

        //如果起始位置为0,且第二位跟的不是".",则无法后续输入
        if (result.startsWith("0") && dstart!=0) {
            return "";
        }

        // if the dot is after the inserted part,
        // nothing can break
        return new SpannableStringBuilder(source, start, end);
    }
}
