package cn.swiftpass.wallet.tiqmo.module.register.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;

public class RegisterPhoneCheckContract {

    public interface View extends BaseView<Presenter> {
        void checkPhoneRegSuccess(CheckPhoneEntity result);
        void checkPhoneError(String errorCode, String errorMsg);
    }


    public interface Presenter extends BasePresenter<View> {
        void checkPhoneReg(String callingCode,String phone,double longitude,double latitude);
        void checkChangePhone(String callingCode,String phone,double longitude,double latitude,String type, String checkIdNumber, String birthday,String needCheckIdNumber);
        void checkForgetPd(String callingCode,String phone,String type);
    }

    public interface RegisterNafathView extends BaseView<RegisterNafathPresenter> {
        void getNafathUrlSuccess(NafathUrlEntity result);
        void getNafathUrlFail(String errorCode, String errorMsg);
        void checkNewDeviceSuccess(CheckPhoneEntity result);
        void checkPhoneRegSuccess(CheckPhoneEntity result);
        void checkPhoneRegError(String errorCode, String errorMsg);
        void checkNewDeviceError(String errorCode, String errorMsg);
    }


    public interface RegisterNafathPresenter extends BasePresenter<RegisterNafathView> {
        void getNafathUrl(String callingCode, String phone, String recipientId,String sceneType);
        void checkNewDevice(String callingCode,String phone, String checkIdNumber, String birthday,String sceneType,String userId);
        void checkPhoneReg(String callingCode,String phone,double longitude,double latitude,String type, String checkIdNumber, String birthday,String needCheckIdNumber);
    }

    public interface RegisterMoreView extends BaseView<RegisterMorePresenter> {
        void checkNewDeviceSuccess(CheckPhoneEntity result);
        void checkNewDeviceError(String errorCode, String errorMsg);
    }


    public interface RegisterMorePresenter extends BasePresenter<RegisterMoreView> {
        void checkNewDevice(String callingCode, String phone, String recipientId,String birthday,String sceneType,String userId);
    }
}