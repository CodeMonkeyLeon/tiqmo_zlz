package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillIOListPresenter implements PayBillContract.BillIOListPresenter {

    PayBillContract.BillIOListView billIOListView;

    @Override
    public void getNewPayBillIOList(String billerId, String sku, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillServiceIOList(billerId, sku, channelCode, new LifecycleMVPResultCallback<PayBillIOListEntity>(billIOListView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                if (billIOListView != null) {
                    billIOListView.getNewPayBillIOListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (billIOListView != null) {
                    billIOListView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getPayBillIOList(String billerId, String sku) {
        AppClient.getInstance().getTransferManager().getPayBillIOList(billerId, sku, new LifecycleMVPResultCallback<PayBillIOListEntity>(billIOListView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                billIOListView.getPayBillIOListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billIOListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

//    @Override
//    public void getPayBillDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data) {
//        AppClient.getInstance().getTransferManager().getPayBillServiceDetail(aliasName, billerId, sku, channelCode, data, new LifecycleMVPResultCallback<PayBillDetailEntity>(billIOListView, true) {
//            @Override
//            protected void onSuccess(PayBillDetailEntity result) {
//                billIOListView.getPayBillDetailSuccess(result);
//            }
//
//            @Override
//            protected void onFail(String errorCode, String errorMsg) {
//                billIOListView.getPayBillDetailFail(errorCode, errorMsg);
//            }
//        });
//    }

    @Override
    public void getPayBillServiceDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data,String paymentType) {
        AppClient.getInstance().getTransferManager().getPayBillServiceDetail(aliasName, billerId, sku, channelCode, data,paymentType, new LifecycleMVPResultCallback<PayBillDetailEntity>(billIOListView, true) {
            @Override
            protected void onSuccess(PayBillDetailEntity result) {
                billIOListView.getPayBillDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billIOListView.getPayBillDetailFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(billIOListView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                billIOListView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billIOListView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillIOListView billIOListView) {
        this.billIOListView = billIOListView;
    }

    @Override
    public void detachView() {
        this.billIOListView = null;
    }
}
