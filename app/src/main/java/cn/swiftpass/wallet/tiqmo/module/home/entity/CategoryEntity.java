package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 各种本地资源分类选择实例
 */
public class CategoryEntity extends BaseEntity {

    public int imgId;
    public int textId;

    public CategoryEntity(int imgId, int textId) {
        this.imgId = imgId;
        this.textId = textId;
    }
}
