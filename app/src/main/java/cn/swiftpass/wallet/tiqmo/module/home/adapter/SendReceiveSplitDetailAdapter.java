package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SendReceiveSplitDetailAdapter extends BaseRecyclerAdapter<SendReceiveSplitDetailEntity.SplitPayerDtos> {

    public SendReceiveSplitDetailAdapter(@Nullable List<SendReceiveSplitDetailEntity.SplitPayerDtos> data) {
        super(R.layout.item_send_receive_split_detail, data);
    }


    @Override
    protected void bindData(@NonNull BaseViewHolder holder, SendReceiveSplitDetailEntity.SplitPayerDtos item, int position) {

        holder.setText(R.id.tv_name, item.getPayerName());
        ImageView image = holder.getView(R.id.iv_direction);
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        switch (item.getPaymentStatus()){
            case "C":
                helper.setBackgroundResourceByAttr(image,R.attr.split_process);
                holder.setText(R.id.tv_amount, Spans.builder()
                        .text(AndroidUtils.getTransferMoney(item.getAmount()))
                        .color(mContext.getColor(R.color.color_f1a13c))
                        .size(18)
                        .text(" " + LocaleUtils.getCurrencyCode(item.getPayerName()))
                        .color(mContext.getColor(R.color.color_9c9da1))
                        .size(16)
                        .build());
                break;
            case "S":
                helper.setBackgroundResourceByAttr(image,R.attr.split_success);
                holder.setText(R.id.tv_amount, Spans.builder()
                        .text(AndroidUtils.getTransferMoney(item.getAmount()))
                        .color(mContext.getColor(R.color.green2))
                        .size(18)
                        .text(" " + LocaleUtils.getCurrencyCode(item.getPayerName()))
                        .color(mContext.getColor(R.color.color_9c9da1))
                        .size(16)
                        .build());
                break;
            case "V":
                helper.setBackgroundResourceByAttr(image,R.attr.split_reject);
                holder.setText(R.id.tv_amount, Spans.builder()
                        .text(AndroidUtils.getTransferMoney(item.getAmount()))
                        .color(mContext.getColor(R.color.red2))
                        .size(18)
                        .text(" " + LocaleUtils.getCurrencyCode(item.getPayerName()))
                        .color(mContext.getColor(R.color.color_9c9da1))
                        .size(16)
                        .build());
                break;
        }
        ImageView ivAvatar = holder.getView(R.id.iv_avatar);
        String avatarUrl = item.getIconUrl();
            if (!TextUtils.isEmpty(avatarUrl)) {
                Glide.with(mContext).clear(ivAvatar);
                Glide.with(mContext)
                        .asBitmap()
                        .load(avatarUrl)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .dontAnimate()
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(new BitmapImageViewTarget(ivAvatar) {
                            @Override
                            public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                                ivAvatar.setImageBitmap(bitmap);
                            }
                        });
            } else {
                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivAvatar);
            }
        }


}
