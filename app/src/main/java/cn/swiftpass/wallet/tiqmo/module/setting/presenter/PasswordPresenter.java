package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.PasswordContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PasswordPresenter implements PasswordContract.Presenter {

    private PasswordContract.View baseView;

    @Override
    public void checkLoginPwd(String pdType, String oldPassword) {
        AppClient.getInstance().getUserManager().checkLoginPassword(pdType, oldPassword, new LifecycleMVPResultCallback<Void>(baseView,true) {
            @Override
            protected void onSuccess(Void result) {
                baseView.checkLoginPwdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.checkLoginPwdFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(PasswordContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
