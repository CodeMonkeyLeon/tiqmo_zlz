package cn.swiftpass.wallet.tiqmo.module.home.presenter

import cn.swiftpass.wallet.tiqmo.module.home.contract.HistoryContract
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity
import cn.swiftpass.wallet.tiqmo.sdk.AppClient
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback

class HistoryPresenter : HistoryContract.Presenter {


    override fun getFilterList(
        isClick: Boolean,
        isPreAuth: Boolean
    ) {
        mView?.run {
            if (!isClick) {
                showProgress(true)
            }
            if (isPreAuth) {
                //预授权历史
                AppClient.getInstance().getFilterList(
                    object : ResultCallback<FilterListEntity> {
                        override fun onResult(response: FilterListEntity?) {
                            showProgress(false)
                            getFilterListSuccess(
                                response,
                                isClick,
                                true
                            )
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            showProgress(false)
                            getFilterListFailed(
                                errorCode,
                                errorMsg,
                                isClick,
                                true
                            )
                        }
                    }
                )
            } else {
                //交易历史
                AppClient.getInstance().getFilterList(
                    object : ResultCallback<FilterListEntity> {
                        override fun onResult(response: FilterListEntity?) {
                            showProgress(false)
                            getFilterListSuccess(
                                response,
                                isClick,
                                false
                            )
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            showProgress(false)
                            getFilterListFailed(
                                errorCode,
                                errorMsg,
                                isClick,
                                false
                            )
                        }
                    }
                )
            }
        }
    }

    override fun getHistoryList(
        protocolNo: String?,
        accountNo: String?,
        paymentType: List<String?>?,
        tradeType: List<String?>?,
        startDate: String?,
        endDate: String?,
        pageSize: Int,
        pageNum: Int,
        direction: String?,
        orderNo: String?,
        isPreAuth: Boolean
    ) {
        mView?.run {
            if (isPreAuth) {
                //预授权历史
                AppClient.getInstance().getPreAuthTransferMainHistory(
                    protocolNo,
                    startDate,
                    endDate,
                    pageSize,
                    pageNum,
                    direction,
                    orderNo,
                    object : ResultCallback<TransferHistoryMainEntity> {
                        override fun onResult(response: TransferHistoryMainEntity?) {
                            showProgress(false)
                            getHistoryListSuccess(response, pageNum, true)
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            showProgress(false)
                            getHistoryListFail(errorCode, errorMsg, pageNum, true)
                        }
                    }
                )
            } else {
                //交易历史
                AppClient.getInstance().getTransferMainHistory(
                    protocolNo,
                    accountNo,
                    paymentType,
                    tradeType,
                    startDate,
                    endDate,
                    pageSize,
                    pageNum,
                    direction,
                    orderNo,
                    object : ResultCallback<TransferHistoryMainEntity> {


                        override fun onResult(response: TransferHistoryMainEntity?) {
                            showProgress(false)
                            getHistoryListSuccess(response, pageNum, false)
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            showProgress(false)
                            getHistoryListFail(errorCode, errorMsg, pageNum, false)
                        }
                    }
                )
            }
        }
    }

    override fun getHistoryDetail(
        orderNo: String?,
        orderType: String?,
        queryType: String?,
        isPreAuth: Boolean
    ) {
        mView?.run {
            if (isPreAuth) {
                //预授权历史详情
                AppClient.getInstance().getPreAuthTransferHistoryDetail(
                    orderNo,
                    orderType,
                    object : ResultCallback<TransferHistoryDetailEntity> {

                        override fun onResult(response: TransferHistoryDetailEntity?) {
                            getHistoryDetailSuccess(response, true)
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            getHistoryDetailFail(errorCode, errorMsg, true)
                        }
                    }
                )
            } else {
                //交易历史详情
                AppClient.getInstance().getTransferHistoryDetail(
                    orderNo,
                    orderType,
                    queryType,
                    object : ResultCallback<TransferHistoryDetailEntity> {
                        override fun onResult(response: TransferHistoryDetailEntity?) {
                            getHistoryDetailSuccess(response, false)
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            getHistoryDetailFail(errorCode, errorMsg, false)
                        }
                    }
                )
            }
        }
    }

    override fun deleteTransferHistory(
        orderNo: String?,
        isPreAuth: Boolean
    ) {

        mView?.run {
            showProgress(true)
            if (isPreAuth) {
                //预授权历史
            } else {
                //交易历史
                AppClient.getInstance().deleteTransferHistory(
                    orderNo,
                    object : ResultCallback<Void> {

                        override fun onResult(response: Void?) {
                            deleteTransferHistorySuccess(response, false)
                        }

                        override fun onFailure(errorCode: String?, errorMsg: String?) {
                            deleteTransferHistoryFail(errorCode, errorMsg, false)
                        }
                    }
                )
            }
        }
    }

    private var mView: HistoryContract.View? = null

    override fun attachView(v: HistoryContract.View?) {
        v?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}