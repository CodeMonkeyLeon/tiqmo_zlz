package cn.swiftpass.wallet.tiqmo.support.utils;

public class StringUtils {

    public static int countStr(String str1, String str2) {
        int count = 0;
        while (str1.contains(str2)) {
            str1 = str1.substring(str1.indexOf(str2) + 1);
            ++count;
        }
        return count;
    }
}
