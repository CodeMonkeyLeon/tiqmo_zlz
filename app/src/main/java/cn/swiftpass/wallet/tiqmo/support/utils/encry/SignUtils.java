package cn.swiftpass.wallet.tiqmo.support.utils.encry;

import android.util.Log;

import java.security.MessageDigest;
import java.security.Signature;

import cn.swiftpass.wallet.tiqmo.support.utils.Base64;


public class SignUtils {

    public static boolean verify(String sign, String data, String pubKey) {
        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(RSAHelper.getPublicKey(pubKey));
            //摘要
            MessageDigest sha256Digest = DigestUtils.getSha256Digest();
            sha256Digest.update(data.getBytes());
            signature.update(sha256Digest.digest());
            return signature.verify(Base64.decode(sign));
        } catch (Exception e) {
            Log.e("SignUtils", Log.getStackTraceString(e));
        }
        return false;

    }
}
