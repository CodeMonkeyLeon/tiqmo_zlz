package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.contract.KycVerifyContract;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.KycVerifyPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.KycPersonalInfo;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.AreaUtils;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

public class KycVerifyActivity extends BaseCompatActivity<KycVerifyContract.Presenter> implements KycVerifyContract.View {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_id_number)
    CustomizeEditText etIdNumber;
    @BindView(R.id.et_birthday)
    CustomizeEditText etBirthday;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.tv_complete)
    TextView tvComplete;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_content_title)
    TextView tvContentTitle;
    @BindView(R.id.con_content)
    ConstraintLayout conContent;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_city_choose_subtitle)
    TextView tvCityChooseSub;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.ll_kyc_info)
    LinearLayout llKycInfo;
    @BindView(R.id.ll_kyc_detail)
    LinearLayout llKycDetail;
    @BindView(R.id.et_phone)
    CustomizeEditText etPhone;
    @BindView(R.id.tv_state_subtitle)
    TextView tvStateSubtitle;
    @BindView(R.id.tv_state)
    TextView tvState;
    @BindView(R.id.ll_state_choose)
    FrameLayout llStateChoose;
    @BindView(R.id.ll_city_choose)
    FrameLayout llCityChoose;
    @BindView(R.id.et_kyc_address)
    CustomizeEditText etKycAddress;
    @BindView(R.id.et_employer_name)
    CustomizeEditText etEmployerName;
    @BindView(R.id.tv_source_of_fund_subtitle)
    TextView tvSourceOfFundSubtitle;
    @BindView(R.id.tv_source_of_fund)
    TextView tvSourceOfFund;
    @BindView(R.id.fl_source_of_fund)
    FrameLayout flSourceOfFund;
    @BindView(R.id.tv_profession_subtitle)
    TextView tvProfessionSubtitle;
    @BindView(R.id.tv_profession)
    TextView tvProfession;
    @BindView(R.id.fl_profession)
    FrameLayout flProfession;
    @BindView(R.id.tv_salary_range_subtitle)
    TextView tvSalaryRangeSubtitle;
    @BindView(R.id.tv_salary_range)
    TextView tvSalaryRange;
    @BindView(R.id.fl_salary_range)
    FrameLayout flSalaryRange;
    @BindView(R.id.tv_birthday_subtitle)
    TextView tvBirthdaySubtitle;
    @BindView(R.id.tv_birthday)
    TextView tvBirthday;
    @BindView(R.id.fl_birthday)
    FrameLayout flBirthday;
    @BindView(R.id.ll_kyc_number)
    LinearLayout llKycNumber;
    @BindView(R.id.ll_agree_content)
    LinearLayout llAgreeContent;
    @BindView(R.id.tv_employment_subtitle)
    TextView tvEmploymentSubtitle;
    @BindView(R.id.tv_employment)
    TextView tvEmployment;
    @BindView(R.id.fl_employment)
    FrameLayout flEmployment;
    @BindView(R.id.rg_agree_one)
    RadioGroup rgAgreeOne;
    @BindView(R.id.rg_agree_two)
    RadioGroup rgAgreeTwo;

    private Date mSelectedDate = null;
    // 设置日历的显示的地区（根据自己的需要写）

    private GregorianCalendar chooseCal;

    private String kycType;
    private String referralCode;
    private String shareLink;
    private String validIDNotices = "";
    private boolean isOnlyGeo = false;
    private int maxIdLength;

    private KycPersonalInfo info;
    private String employmentCode;

    private String beneficialOwner = "";
    private String relativesExposedPerson = "";


    /**
     * 是否选择公历
     */
    private boolean isChooseGeo = true;

    private void checkCompleteEnable() {
        tvComplete.setEnabled(Constants.KYC_REG.equals(kycType) ? info.isRegFull() :
                (Constants.KYC_FORGET_PAY_PD.equals(kycType) && info.isForgetPPFull(maxIdLength)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (etIdNumber != null) {
            etIdNumber.setError("");
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_kyc_verify;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        fixedSoftKeyboardSliding();
        info = new KycPersonalInfo();
        if (getIntent() == null || getIntent().getExtras() == null) {
            return;
        }
        kycType = getIntent().getExtras().getString(Constants.KYC_TYPE);
        referralCode = SpUtils.getInstance().getReferralCode();
        shareLink = SpUtils.getInstance().getShareLink();
        if (Constants.KYC_FORGET_PAY_PD.equals(kycType)) {
            info.sceneType = Constants.OTP_KYC_PAY_PD;
            tvTitle.setText(R.string.Profile_CLP_0010_2_5_L_13);
            tvContentTitle.setText(R.string.forget_ppw_2);
            llKycInfo.setVisibility(View.GONE);
            llKycDetail.setVisibility(View.GONE);
            llKycNumber.setVisibility(View.VISIBLE);
            llAgreeContent.setVisibility(View.GONE);
        } else if (Constants.KYC_REG.equals(kycType)) {
            info.sceneType = Constants.OTP_KYC_REG_KSA;
            tvTitle.setText(R.string.kyc_13);
            tvContentTitle.setText(R.string.kyc_8);
            ivKycStep.setVisibility(View.GONE);
            ivKycStep.setImageResource(R.drawable.step_progress_2_1);
            llKycInfo.setVisibility(View.VISIBLE);
            llKycNumber.setVisibility(View.GONE);
        }

        rgAgreeOne.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_yes_one:
                        beneficialOwner = "1";
                        info.beneficialOwner = "1";
                        break;
                    case R.id.rb_no_one:
                        beneficialOwner = "0";
                        info.beneficialOwner = "0";
                        break;
                    default:
                        break;
                }
                checkCompleteEnable();
            }
        });


        rgAgreeTwo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_yes_two:
                        relativesExposedPerson = "1";
                        info.relativesExposedPerson = "1";
                        break;
                    case R.id.rb_no_two:
                        relativesExposedPerson = "0";
                        info.relativesExposedPerson = "0";
                        break;
                    default:
                        break;
                }
                checkCompleteEnable();
            }
        });

        tvSalaryRangeSubtitle.setText(R.string.newhome_36);

        etBirthday.setEnabled(false);
        maxIdLength = 10;
        etIdNumber.setHint(getString(R.string.forget_ppw_5));
        validIDNotices = getString(R.string.kyc_37);
        isOnlyGeo = false;
        etIdNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxIdLength),
                new NormalInputFilter(NormalInputFilter.NUMBER)});

        etBirthday.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    tvComplete.setEnabled(false);
                } else {
                    checkCompleteEnable();
                }
            }
        });
        etBirthday.setOnUnableClickListener(new CustomizeEditText.OnUnableClickListener() {
            @Override
            public void onUnableViewClick() {
                if (TextUtils.isEmpty(info.idNum) || info.idNum.length() != maxIdLength) {
                    return;
                }
                selectDatetime(isOnlyGeo || !info.idNum.startsWith("1"));
            }
        });

        etKycAddress.setFilters(new InputFilter[]{new InputFilter.LengthFilter(64),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPACE)});
        etEmployerName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(64),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPACE)});
        etKycAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                info.address = etKycAddress.getText().toString().trim();
                if (!hasFocus) {
                    checkCompleteEnable();
                }
            }
        });
        etKycAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (info == null) return;
                info.address = s.toString().trim();
                checkCompleteEnable();
            }
        });
        etEmployerName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                info.employerName = etEmployerName.getText().toString().trim();
                if (!hasFocus) {
                    checkCompleteEnable();
                }
            }
        });
        etEmployerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (info == null) return;
                info.employerName = s.toString().trim();
                checkCompleteEnable();
            }
        });
        etIdNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                info.idNum = s.toString();
                if (TextUtils.isEmpty(info.idNum)) {
                    etBirthday.setText("");
                    clearBirthday();
                    info.dataOfBirth = "";
                    tvComplete.setEnabled(false);
                    etIdNumber.setError("");
                } else {
                    if (AreaUtils.getCurrentArea() == AreaUtils.AREA_SAUDI) {
                        if (!info.idNum.startsWith("1") && !info.idNum.startsWith("2")) {
                            etBirthday.setText("");
                            clearBirthday();
                            info.dataOfBirth = "";
                            etIdNumber.setError(validIDNotices);
                        } else {
                            if (info.idNum.startsWith("1") && isChooseGeo) {
                                etBirthday.setText("");
                                clearBirthday();
                                info.dataOfBirth = "";
                            }

                            if (info.idNum.startsWith("2") && !isChooseGeo) {
                                etBirthday.setText("");
                                clearBirthday();
                                info.dataOfBirth = "";
                            }
                        }
                    }
                    checkCompleteEnable();
                }
            }
        });

        etIdNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (AreaUtils.getCurrentArea() == AreaUtils.AREA_SAUDI) {
                    info.idNum = etIdNumber.getText().toString().trim();
                } else {
                    info.idNum = "784" + etIdNumber.getText().toString().trim();
                    info.idNum = info.idNum.replace("-", "");
                }
                if (!hasFocus && !TextUtils.isEmpty(info.idNum)) {
                    if (AreaUtils.getCurrentArea() == AreaUtils.AREA_SAUDI) {
                        if (info.idNum.length() < maxIdLength || (!info.idNum.startsWith("1") &&
                                !info.idNum.startsWith("2"))) {
                            etIdNumber.setError(validIDNotices);
                        } else {
                            etIdNumber.setError("");
                        }
                    }
                }
            }
        });
    }

    private void clearBirthday() {
        try {
            tvBirthday.setText("");
            tvBirthday.setVisibility(View.GONE);
            ViewAnim.resetViewParams(mContext, tvBirthdaySubtitle);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    protected void idNumAddLine(final CustomizeEditText mEditText) {
        mEditText.addTextChangedListener(new TextWatcher() {
            int beforeTextLength = 0;
            int onTextLength = 0;
            boolean isChanged = false;

            int location = 0;//记录光标的位置
            private char[] tempChar;
            private StringBuffer buffer = new StringBuffer();
            int lineNumberB = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeTextLength = s.length();
                if (buffer.length() > 0) {
                    buffer.delete(0, buffer.length());
                }
                lineNumberB = 0;
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == '-') {
                        lineNumberB++;
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onTextLength = s.length();
                buffer.append(s.toString());
                if (onTextLength == beforeTextLength || onTextLength <= 3 || isChanged) {
                    isChanged = false;
                    return;
                }
                isChanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isChanged) {
                    location = mEditText.getSelectionEnd();
                    int index = 0;
                    while (index < buffer.length()) {
                        if (buffer.charAt(index) == '-') {
                            buffer.deleteCharAt(index);
                        } else {
                            index++;
                        }
                    }

                    index = 0;
                    int lineNumberC = 0;
                    while (index < buffer.length()) {
                        //银行卡号的话需要改这里
                        if ((index == 4 || index == 12)) {
                            buffer.insert(index, '-');
                            lineNumberC++;
                        }
                        index++;
                    }

                    if (lineNumberC > lineNumberB) {
                        location += (lineNumberC - lineNumberB);
                    }

                    tempChar = new char[buffer.length()];
                    buffer.getChars(0, buffer.length(), tempChar, 0);
                    String str = buffer.toString();
                    if (location > str.length()) {
                        location = str.length();
                    } else if (location < 0) {
                        location = 0;
                    }

                    mEditText.setText(str);
                    Editable etable = mEditText.getText();
                    Selection.setSelection(etable, location);
                    isChanged = false;
                }
            }
        });
    }

    public void selectDatetime(boolean isGeo) {
        if (isGeo) {
            GregorianCalendar startDate = new GregorianCalendar();
            GregorianCalendar endDate = new GregorianCalendar();
            startDate.set(Calendar.YEAR, startDate.get(Calendar.YEAR) - 90);
            endDate.set(Calendar.YEAR, endDate.get(Calendar.YEAR) - 16);

            GregorianCalendar selectedDate = new GregorianCalendar();
            if (mSelectedDate == null) {
                mSelectedDate = endDate.getTime();
                selectedDate.setTime(endDate.getTime());
            } else {
                selectedDate.setTime(mSelectedDate);
            }

            TimePickerDialogUtils.showTimePicker(mContext, selectedDate, startDate, endDate, new OnTimeSelectListener() {
                @Override
                public void onTimeSelect(Date date, View v) {
                    isChooseGeo = true;
                    mSelectedDate = date;

                    SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
                    info.dataOfBirth = df.format(date);
                    ;
                    etBirthday.setText(info.dataOfBirth);
                    etIdNumber.setError("");
                    refreshItemView(info.dataOfBirth, tvBirthday, tvBirthdaySubtitle, false);
                }
            });
        } else {
            UmmalquraCalendar startHijDate = new UmmalquraCalendar();
            UmmalquraCalendar endHijDate = new UmmalquraCalendar();

            GregorianCalendar calendar = new GregorianCalendar();
            GregorianCalendar startCal = new GregorianCalendar(calendar.get(Calendar.YEAR) - 80,
                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            startHijDate.setTime(startCal.getTime());
            GregorianCalendar endCal = new GregorianCalendar(calendar.get(Calendar.YEAR) - 16,
                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            endHijDate.setTime(endCal.getTime());
            UmmalquraCalendar selectedHijDate = new UmmalquraCalendar();
            if (chooseCal != null) {
                selectedHijDate.setTime(chooseCal.getTime());
            } else {
                selectedHijDate.setTime(endHijDate.getTime());
            }
            TimePickerDialogUtils.showTimePicker(mContext, selectedHijDate, startHijDate, endHijDate, new OnTimeSelectListener() {
                @Override
                public void onTimeSelect(Date date, View v) {
                    isChooseGeo = false;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
                    ummalquraCalendar.set(UmmalquraCalendar.YEAR, year);
                    ummalquraCalendar.set(UmmalquraCalendar.MONTH, month);
                    ummalquraCalendar.set(UmmalquraCalendar.DAY_OF_MONTH, day);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("", Locale.ENGLISH);
                    dateFormat.setCalendar(ummalquraCalendar);
                    dateFormat.applyPattern("MMMM d, y");
                    String bir = dateFormat.format(ummalquraCalendar.getTime());

                    GregorianCalendar gCal = new GregorianCalendar();
                    gCal.setTime(ummalquraCalendar.getTime());
                    chooseCal = new GregorianCalendar(gCal.get(Calendar.YEAR), gCal.get(Calendar.MONTH),
                            gCal.get(Calendar.DAY_OF_MONTH));
                    GregorianCalendar serverCal = new GregorianCalendar(gCal.get(Calendar.YEAR),
                            gCal.get(Calendar.MONTH), gCal.get(Calendar.DAY_OF_MONTH));

                    info.dataOfBirth = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(serverCal.getTime());

                    etBirthday.setText(bir);
                    refreshItemView(bir, tvBirthday, tvBirthdaySubtitle, false);
                    etIdNumber.setError("");
                }
            });
        }
    }

    @OnClick({R.id.iv_back, R.id.fl_birthday, R.id.tv_complete, R.id.ll_city_choose, R.id.ll_state_choose, R.id.fl_source_of_fund, R.id.fl_profession, R.id.fl_salary_range,
    R.id.fl_employment})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.fl_birthday:
                if (TextUtils.isEmpty(info.idNum) || info.idNum.length() != maxIdLength) {
                    return;
                }
                selectDatetime(isOnlyGeo || !info.idNum.startsWith("1"));
                break;
            case R.id.ll_state_choose:
                mPresenter.getStateList();
                break;
            case R.id.fl_employment:
                mPresenter.getEmploymentSector("EMPLOYMENT_SECTOR","");
                break;
            case R.id.fl_source_of_fund:
                mPresenter.getSourceOfFund();
                break;
            case R.id.fl_profession:
                if(!TextUtils.isEmpty(employmentCode)) {
                    mPresenter.getProfession("PROFESSION", employmentCode);
                }
                break;
            case R.id.fl_salary_range:
                mPresenter.getSalaryRange();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_complete:
                if (Constants.KYC_FORGET_PAY_PD.equals(kycType)) {
                    mPresenter.kycVerify(SpUtils.getInstance().getCallingCode(), ProjectApp.getPhone(), info.idNum,
                            info.dataOfBirth, info.sceneType, "", "", "",
                            "","", "", "", "", beneficialOwner, relativesExposedPerson,"","");
                } else if (Constants.KYC_REG.equals(kycType)) {
                    mPresenter.kycVerify(SpUtils.getInstance().getCallingCode(), ProjectApp.getPhone(), info.idNum,
                            info.dataOfBirth, info.sceneType, info.stateEntity.statesId, info.city.citiesId,
                            info.address,info.employmentCode, info.professionCode,
                            info.sourceOfFundCode, info.salaryRangeCode, info.employerName, beneficialOwner, relativesExposedPerson,referralCode,shareLink);
                }
                showKycProgress(true);
                break;
            case R.id.ll_city_choose:
                if (info.stateEntity == null) return;
                mPresenter.getCityList(info.stateEntity.statesId);
                break;
            default:
                break;
        }
    }

    @Override
    public void kycVerifySuccess(UserInfoEntity userInfoEntity) {
        String codeLength = "4";
        int expiredTime = 180;
        if (userInfoEntity != null) {
            if (userInfoEntity.isKycChecking() || userInfoEntity.isKycHigh()) {
                AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
            }
        }
        if (userInfoEntity != null) {
            codeLength = userInfoEntity.codeLength;
            expiredTime = userInfoEntity.expiredTime;
        }
        ProjectApp.getInstance().getMainHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showKycProgress(false);
            }
        }, 3000);

        if (userInfoEntity != null) {
            if (userInfoEntity.isKyc()) {
                ProjectApp.kycSuccess = true;
                if (TextUtils.isEmpty(referralCode)) {
                    AppsFlyerHelper.getInstance().sendKycNoCodeEvent(getUserInfoEntity());
                } else {
                    AppsFlyerHelper.getInstance().sendKycWithCodeEvent(getUserInfoEntity());
                }
                //kyc确认成功后再清除Kyc
                SpUtils.getInstance().setReferralCode("");
            }else if (userInfoEntity.isKycLow()) {
                ProjectApp.kycSuccess = false;
            } else if (userInfoEntity.isKycHigh()) {
                ProjectApp.removeKycTask();
                return;
            } else if (userInfoEntity.isKycChecking()) {
                SpUtils.getInstance().setKycChecking(true);
                ProjectApp.removeKycTask();
                return;
            }
        }
        if (Constants.KYC_REG.equals(kycType)) {
//            HashMap<String, Object> mHashMaps = new HashMap<>(16);
//            mHashMaps.put(Constants.KYC_TYPE, Constants.KYC_REG);
//            mHashMaps.put(Constants.DATA_USER, userInfoEntity);
//            mHashMaps.put(Constants.REFERRAL_CODE, othersRefCode);
//            ActivitySkipUtil.startAnotherActivity(this, ForgetPayPwdActivity.class, mHashMaps, RIGHT_IN);
            finish();
        } else {
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            mHashMaps.put(Constants.DATA_OTP_TYPE, kycType);
            mHashMaps.put(Constants.DATA_OTP_LENGTH, codeLength);
            mHashMaps.put(Constants.DATA_OTP_TIME, expiredTime);
            mHashMaps.put(Constants.DATA_USER, userInfoEntity);
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, ProjectApp.getPhone());
            ActivitySkipUtil.startAnotherActivity(KycVerifyActivity.this, RegisterOtpActivity.class,
                    mHashMaps, RIGHT_IN);
        }
    }

    @Override
    public void kycVerifyFail(String errorCode, String errorMsg) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showKycProgress(false);
            }
        }, 3000);
//        etIdNumber.setError(errorMsg);
        showTipDialog(errorMsg);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {

    }

    @Override
    public void showCityListDia(CityEntity result) {
        ArrayList<SelectInfoEntity> cityNameList = new ArrayList<>();
        for (int i = 0; i < result.citiesInfoList.size(); i++) {
            cityNameList.add(new SelectInfoEntity(result.citiesInfoList.get(i).citiesName));
        }
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(cityNameList, getString(R.string.IMR_72), true, true);
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (info.city == null) {
                    info.city = result.citiesInfoList.get(position);
                } else {
                    if (!info.city.citiesId.equals(result.citiesInfoList.get(position).citiesId)) {
                        info.city = result.citiesInfoList.get(position);
//                        info.regionInfo = null;
                    }
                }
                if (tvCity.getVisibility() == View.GONE) {
                    ViewAnim.startViewUp(mContext, tvCityChooseSub, new ViewAnim.AnimStateListener() {
                        @Override
                        public void onAnimEnd() {
                            tvCity.setText(info.city.citiesName);
                            tvCity.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    tvCity.setText(info.city.citiesName);
                }
                checkCompleteEnable();
            }
        });
        dialog.showNow(getSupportFragmentManager(), "city");
    }

    @Override
    public void getStateListSuccess(StateListEntity stateListEntity) {
        ArrayList<SelectInfoEntity> stateNameList = new ArrayList<>();
        for (int i = 0; i < stateListEntity.statesInfoList.size(); i++) {
            stateNameList.add(new SelectInfoEntity(stateListEntity.statesInfoList.get(i).statesName));
        }
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(stateNameList, getString(R.string.IMR_new_4), true, true);
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (info.stateEntity == null) {
                    info.stateEntity = stateListEntity.statesInfoList.get(position);
                } else {
                    if (!info.stateEntity.statesId.equals(stateListEntity.statesInfoList.get(position).statesId)) {
                        tvCity.setText("");
                        tvCity.setVisibility(View.GONE);
                        ViewAnim.resetViewParams(mContext, tvCityChooseSub);
                        info.stateEntity = stateListEntity.statesInfoList.get(position);
//                        info.regionInfo = null;
                    }
                }
                if (tvState.getVisibility() == View.GONE) {
                    ViewAnim.startViewUp(mContext, tvStateSubtitle, new ViewAnim.AnimStateListener() {
                        @Override
                        public void onAnimEnd() {
                            tvState.setText(info.stateEntity.statesName);
                            tvState.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    tvState.setText(info.stateEntity.statesName);
                }
                checkCompleteEnable();
            }
        });
        dialog.showNow(getSupportFragmentManager(), "state");
    }

    @Override
    public void showSourceOfFund(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_new_6));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(info.sourceOfFundCode)) {
                    return;
                }
                info.sourceOfFundDesc = data.get(position).businessParamValue;
                info.sourceOfFundCode = data.get(position).businessParamKey;
                refreshItemView(info.sourceOfFundDesc, tvSourceOfFund, tvSourceOfFundSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showSourceOfFund");
    }

    @Override
    public void showProfession(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_7));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(info.professionCode)) {
                    return;
                }
                info.professionDesc = data.get(position).businessParamValue;
                info.professionCode = data.get(position).businessParamKey;
                refreshItemView(info.professionDesc, tvProfession, tvProfessionSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showProfession");
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkCompleteEnable();
                    }
                }
            });
        } else {
            content.setText(value);
            checkCompleteEnable();

        }
    }

    @Override
    public void showSalaryRange(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.newhome_36));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(info.businessTypeCode)) {
                    return;
                }
                info.salaryRangeDesc = data.get(position).businessParamValue;
                info.salaryRangeCode = data.get(position).businessParamKey;
                refreshItemView(info.salaryRangeDesc, tvSalaryRange, tvSalaryRangeSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showSalaryRange");
    }

    @Override
    public void getEmploymentSectorSuccess(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.sprint19_2));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(info.businessTypeCode)) {
                    return;
                }
                if(!data.get(position).businessParamKey.equals(employmentCode)) {
                    tvProfession.setText("");
                    tvProfession.setVisibility(View.GONE);
                    info.professionCode = "";
                    ViewAnim.resetViewParams(mContext, tvProfessionSubtitle);
                }
                info.employmentDesc = data.get(position).businessParamValue;
                info.employmentCode = data.get(position).businessParamKey;
                employmentCode = data.get(position).businessParamKey;
                refreshItemView(info.employmentDesc, tvEmployment, tvEmploymentSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "employmentSector");
    }

    @Override
    public KycVerifyContract.Presenter getPresenter() {
        return new KycVerifyPresenter();
    }

}
