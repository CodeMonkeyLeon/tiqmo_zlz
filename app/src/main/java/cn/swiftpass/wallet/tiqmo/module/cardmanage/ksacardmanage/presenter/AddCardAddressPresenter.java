package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.MapResultEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AddCardAddressPresenter implements CardKsaContract.AddCardAddressPresenter {

    private CardKsaContract.AddCardAddressView mAddCardAddressView;

    @Override
    public void getCityList(String statesId) {
        AppClient.getInstance().requestCityList(statesId, new LifecycleMVPResultCallback<CityEntity>(mAddCardAddressView, true) {
            @Override
            protected void onSuccess(CityEntity result) {
                if (mAddCardAddressView == null || result == null || result.citiesInfoList == null
                        || result.citiesInfoList.size() <= 0) {
                    return;
                }
                mAddCardAddressView.showCityListDia(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mAddCardAddressView == null) {
                    return;
                }
                mAddCardAddressView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getStateList() {
        AppClient.getInstance().getUserManager().getStateList(new LifecycleMVPResultCallback<StateListEntity>(mAddCardAddressView, true) {
            @Override
            protected void onSuccess(StateListEntity result) {
                if (mAddCardAddressView == null) {
                    return;
                }
                mAddCardAddressView.getStateListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mAddCardAddressView == null) {
                    return;
                }
                mAddCardAddressView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void mapSearch(String address, String key) {
        AppClient.getInstance().mapSearch(address, key, new LifecycleMVPResultCallback<MapResultEntity>(mAddCardAddressView,false) {
            @Override
            protected void onSuccess(MapResultEntity mapResultEntity) {
                mAddCardAddressView.mapSearchSuccess(mapResultEntity);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddCardAddressView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardKsaContract.AddCardAddressView addCardAddressView) {
        this.mAddCardAddressView = addCardAddressView;
    }

    @Override
    public void detachView() {
        this.mAddCardAddressView = null;
    }
}
