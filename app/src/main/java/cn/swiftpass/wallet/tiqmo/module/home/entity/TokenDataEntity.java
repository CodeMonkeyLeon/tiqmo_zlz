package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TokenDataEntity extends BaseEntity {

    public String version;
    public String data;
    public String signature;
}
