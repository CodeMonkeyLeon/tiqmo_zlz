package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.AddMoneyDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.AnimationTransformer;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.CardAnimationHelper;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.StackCardView;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.ZIndexTransformer;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.adapter.StackCardAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.entry.CardItem;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultCommonTransformer;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.CardManageContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.CardManagePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.eventbus.CardEvent;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

public class CardManageActivity extends BaseCompatActivity<CardManageContract.Presenter> implements CardManageContract.View {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_add_new)
    TextView tvAddNew;
    @BindView(R.id.stackview)
    StackCardView stackview;
    @BindView(R.id.common_head)
    RelativeLayout commonHead;
    @BindView(R.id.iv_no_card)
    ImageView ivNoCard;
    @BindView(R.id.iv_default_card)
    ImageView ivDefaultCard;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.con_default)
    ConstraintLayout conDefault;
    @BindView(R.id.rl_card_list)
    RelativeLayout rlCardList;
    List<CardEntity> newCardList = new ArrayList<>();
    List<CardEntity> allCardList = new ArrayList<>();
    CardBind3DSEntity cardBind3DSEntity;
    @BindView(R.id.tv_no_card_text)
    TextView tvNoCardText;
    @BindView(R.id.iv_default_visa)
    ImageView iv_default_visa;
    @BindView(R.id.iv_default_mastercard)
    ImageView iv_default_mastercard;
    @BindView(R.id.tv_default_card_num)
    TextView tvDefaultCardNum;

    private BottomDialog bottomDialog, addNewDialog;

    private String addMoney;

    private long mFadeoutAnimationDuration = 500L;
    private long mFadeinAnimationDuration = 500L;


    private Date mSelectedDate = new Date();
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedDate = Calendar.getInstance();
    Calendar startDate = Calendar.getInstance();
    Calendar endDate = Calendar.getInstance();
    private SimpleDateFormat mShowDateFormat;

    private StackCardAdapter stackCardAdapter;

    private String expire_data;
    private boolean isVerifyCardSuccess = false;
    private String cardFrom;

    HashMap<String, Object> mHashMaps = new HashMap<>();
    HashMap<String, Object> mHashMaps_back = new HashMap<>();
    HashMap<String, Object> mHashMaps_mini = new HashMap<>();
    HashMap<String, Object> mHashMaps_card_bar = new HashMap<>();
    HashMap<Integer, Object> mHashMaps_tension = new HashMap<>();

//    private int tension;
    private CardEntity defaultCardEntity;

    private ImageView iv_visa, iv_mastercard;
    //脱敏显示的卡号
    private String showCardNo;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_card_manage;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    private void setStyle(int size) {
        if (size > 5) {
            size = 5;
        }
        stackview.setClickable(true);
        stackview.setAnimType(StackCardView.ANIM_TYPE_FRONT_TO_LAST);
        stackview.setAnimInterpolator(new OvershootInterpolator((int) mHashMaps_tension.get(size)));
        stackview.setTransformerToFront(new DefaultCommonTransformer());
        stackview.setCardClickListener(new CardAnimationHelper.CardClickListener() {
            @Override
            public void cardClick(CardEntity cardEntity) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (Constants.CARD_FROM_ADD_MONEY.equals(cardFrom)) {
                    mHashMaps.put(Constants.ADD_MONEY, addMoney);
                    mHashMaps.put(Constants.CARD_ENTITY, cardEntity);
                    ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, AddMoneyDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    mHashMaps.put(Constants.CARDDETAILS, cardEntity);
                    mHashMaps.put(Constants.ONLYCARD, newCardList.size() == 1);
                    ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, CardDetailsAcitivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
        stackview.setTransformerToBack(new AnimationTransformer() {
            @Override
            public void transformAnimation(View view, float fraction, int cardWidth, int cardHeight, int fromPosition, int toPosition) {
                int positionCount = fromPosition - toPosition;
                float scale = (0.8f - 0.1f * fromPosition) + (0.1f * fraction * positionCount);
                if (fraction < 0.5) {
                    view.setRotationX(180 * fraction);
                    view.setScaleX(scale + (0.2f - fraction));
                    view.setScaleY(scale + (0.2f - fraction));
                } else {
                    view.setRotationX(180 * (1 - fraction));
                    view.setScaleX(scale + (0.2f - (1 - fraction)));
                    view.setScaleY(scale + 0.2f);
                }
            }

            @Override
            public void transformInterpolatedAnimation(View view, float fraction, int cardWidth, int cardHeight, int fromPosition, int toPosition) {
                int positionCount = fromPosition - toPosition;
                float scale = (0.8f - 0.1f * fromPosition) + (0.1f * fraction * positionCount);
                view.setTranslationY(-cardHeight * (0.8f - scale) * 0.5f - cardWidth * (0.06f *
                        fromPosition - 0.06f * fraction * positionCount));
            }
        });
        stackview.setZIndexTransformerToBack(new ZIndexTransformer() {
            @Override
            public void transformAnimation(CardItem card, float fraction, int cardWidth, int cardHeight, int fromPosition, int toPosition) {
                if (fraction < 0.5f) {
                    card.zIndex = 1f + 0.01f * fromPosition;
                } else {
                    card.zIndex = 1f + 0.01f * toPosition;
                }
            }

            @Override
            public void transformInterpolatedAnimation(CardItem card, float fraction, int cardWidth, int cardHeight, int fromPosition, int toPosition) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        userInfoEntity = getUserInfoEntity();
        if (userInfoEntity.isKycHigh()) {
            showLogoutDialog();
            return;
        }
        if (userInfoEntity.isKycChecking()) {
            ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        }
        if (ProjectApp.kycSuccess) {
            ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, KycSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.kycSuccess = false;
        }
        if (ProjectApp.addCardSuccess) {
            cardListRequest();
//            if ("000000".equals(ProjectApp.getCardNotifyEntity().respCode)) {
//                ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, AddCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
//            } else {
//                ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, AddCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
//            }
            ProjectApp.addCardSuccess = false;
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.card_list_empty_1);
        if (getIntent() != null && getIntent().getExtras() != null) {
            cardFrom = getIntent().getExtras().getString(Constants.CARD_FROM);
            if (Constants.CARD_FROM_ADD_MONEY.equals(cardFrom)) {
                tvTitle.setText(R.string.add_money_14_1);
                addMoney = getIntent().getExtras().getString(Constants.ADD_MONEY);
            }
        }
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
        startDate.set(Calendar.YEAR, selectedDate.get(Calendar.YEAR));
        endDate.set(Calendar.YEAR, selectedDate.get(Calendar.YEAR) + 10);

        mHashMaps.put("424242", R.drawable.card_front_new);
        mHashMaps.put("454347", R.drawable.card_front_new);
        mHashMaps.put("543603", R.drawable.card_front_new);
        mHashMaps.put("222300", R.drawable.card_front_new);
        mHashMaps.put("519999", R.drawable.card_front_new);
        mHashMaps.put("500018", R.drawable.card_front_new);
        mHashMaps.put("465858", R.drawable.card_front_new);

        mHashMaps_back.put("424242", R.drawable.card_back_new);
        mHashMaps_back.put("454347", R.drawable.card_back_new);
        mHashMaps_back.put("543603", R.drawable.card_back_new);
        mHashMaps_back.put("222300", R.drawable.card_back_new);
        mHashMaps_back.put("519999", R.drawable.card_back_new);
        mHashMaps_back.put("500018", R.drawable.card_back_new);
        mHashMaps_back.put("465858", R.drawable.card_back_new);

        mHashMaps_mini.put("424242", R.drawable.card_middle_new);
        mHashMaps_mini.put("454347", R.drawable.card_middle_new);
        mHashMaps_mini.put("543603", R.drawable.card_middle_new);
        mHashMaps_mini.put("222300", R.drawable.card_middle_new);
        mHashMaps_mini.put("519999", R.drawable.card_middle_new);
        mHashMaps_mini.put("500018", R.drawable.card_middle_new);
        mHashMaps_mini.put("465858", R.drawable.card_middle_new);

        mHashMaps_card_bar.put("424242", R.drawable.card_small_new);
        mHashMaps_card_bar.put("454347", R.drawable.card_small_new);
        mHashMaps_card_bar.put("543603", R.drawable.card_small_new);
        mHashMaps_card_bar.put("222300", R.drawable.card_small_new);
        mHashMaps_card_bar.put("519999", R.drawable.card_small_new);
        mHashMaps_card_bar.put("500018", R.drawable.card_small_new);
        mHashMaps_card_bar.put("465858", R.drawable.card_small_new);

        if (stackCardAdapter == null) {
            stackCardAdapter = new StackCardAdapter(mContext, newCardList);
            stackCardAdapter.setmHashMaps(mHashMaps);
        }
        mHashMaps_tension.put(1, -40);
        mHashMaps_tension.put(2, -30);
        mHashMaps_tension.put(3, -25);
        mHashMaps_tension.put(4, -20);
        mHashMaps_tension.put(5, -15);
        mHashMaps_tension.put(6, -11);
        setStyle(1);

        cardListRequest();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void cardListRequest() {
        mPresenter.getBindCardList();
    }

    private void cardBindRequest(String cardHolderName, String cardNo, String expire_data, String cvv) {
        if (Constants.CARD_FROM_ADD_MONEY.equals(cardFrom)) {

            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.ADD_MONEY, addMoney);
            CardEntity cardEntity = new CardEntity();
            cardEntity.setCustName(cardHolderName);
            cardEntity.setCardNo(cardNo);
            cardEntity.setExpire(expire_data);
            cardEntity.setCvv(cvv);
            cardEntity.showCardNo = showCardNo;
            mHashMaps.put(Constants.CARD_ENTITY, cardEntity);
            mHashMaps.put(Constants.ADD_MONEY_BIND_NEW_CARD, true);
            ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, AddMoneyDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            mPresenter.bindNewCard(cardHolderName,
                    cardNo,
                    expire_data,
                    cvv,
                    "B");
        }
    }


    @OnClick({R.id.iv_back, R.id.tv_title, R.id.tv_add_new, R.id.iv_default_card})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMaps = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_default_card:
                if (Constants.CARD_FROM_ADD_MONEY.equals(cardFrom)) {
                    mHashMaps.put(Constants.ADD_MONEY, addMoney);
                    mHashMaps.put(Constants.CARD_ENTITY, defaultCardEntity);
                    ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, AddMoneyDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    mHashMaps.put(Constants.CARDDETAILS, defaultCardEntity);
                    mHashMaps.put(Constants.ONLYCARD, newCardList.size() == 1);
                    ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, CardDetailsAcitivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_add_new:
                userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
                if (userInfoEntity != null) {
                    if (userInfoEntity.isKyc()) {
                        clickAddCard();
                    } else {
                        if (userInfoEntity.isKycChecking()) {
                            ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                        } else {
                            UserInfoManager.getInstance().setKycType(Constants.KYC_TYPE_ADD_CARD);
                            ReferralCodeDialogUtils.showReferralCodeDialog(this,CardManageActivity.this);
                        }
                    }
                }
            default:
                break;
        }
    }

    private void clickAddCard() {
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            String bingCardNum = configEntity.bingCardNum;
            if (TextUtils.isEmpty(bingCardNum)) {
                bingCardNum = "15";
            }
            try {
                int maxCard = Integer.parseInt(bingCardNum);
                if (allCardList.size() >= maxCard) {
                    showAddCardDialog(mContext.getString(R.string.add_new_card_24),
                            R.attr.icon_max_cards_uploaded,
                            mContext.getString(R.string.add_new_card_25).replace("15", maxCard + "") + "\n\n" + mContext.getString(R.string.add_new_card_26),
                            mContext.getString(R.string.confirm_default_2));
                } else {
                    showAddCardDialog(mContext.getString(R.string.add_new_card_21),
                            R.attr.icon_initiation_refund,
                            mContext.getString(R.string.add_new_card_22).replace("XXX", LocaleUtils.getCurrencyCode("")),
                            mContext.getString(R.string.add_new_card_23));
                }
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ADD_CARD == event.getEventType()) {
            clickAddCard();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEvent cardEvent) {
        if (CardEvent.remove_card_success == cardEvent.getEventType()) {
            cardListRequest();
        }
    }

    public void showAddCardDialog(String title, int resImageId, String messageText, String buttonText) {
//        Bitmap bmp = FastBlurUtility.getBlurBackgroundDrawer(this);
        addNewDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_add_card_dialog, null);

        TextView tv_title = contentView.findViewById(R.id.tv_dialog_title);
        ImageView iv_image = contentView.findViewById(R.id.iv_image_center);
        TextView tv_content = contentView.findViewById(R.id.tv_dialog_message);
        TextView tv_dialog_button = contentView.findViewById(R.id.tv_dialog_button);

        tv_title.setText(title);
        iv_image.setImageResource(ThemeSourceUtils.getSourceID(mContext, resImageId));
        tv_content.setText(messageText);

        if(!TextUtils.isEmpty(buttonText)){
            tv_dialog_button.setText(buttonText);
        }

        addNewDialog.setContentView(contentView);

        addNewDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (addNewDialog != null) {
                    addNewDialog.dismiss();
                    addNewDialog = null;
                }
            }
        });

        tv_dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addNewDialog != null && addNewDialog.isShowing()) {
                    addNewDialog.dismiss();
                }

                String bingCardNum = AppClient.getInstance().getUserManager().getConfig().bingCardNum;
                int maxCard = Integer.parseInt(bingCardNum);
                if (allCardList.size() < maxCard) {
                    //弹绑卡的弹框
                    if (bottomDialog == null) {
                        showBindCardDialog();
                    }
                }
            }
        });


        Window dialogWindow = addNewDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialogWindow.setAttributes(layoutParams);
        }
        addNewDialog.showWithBottomAnim();
    }

    public void showBindCardDialog() {
        bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.widget_popupwindow, null);

        initView(contentView);

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                bottomDialog.dismiss();
                bottomDialog = null;
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(contentView);

        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialogWindow.setAttributes(layoutParams);
            dialogWindow.setDimAmount(0f);
        }
        setAlpha(mContext, 0.7f);
        bottomDialog.showWithBottomAnim();
    }


    FrameLayout fl_front_card;
    FrameLayout fl_back_card;
    EditTextWithDel et_card_num, et_full_name, et_valid_time, et_cvv_text;

    ImageView iv_front_card_image,iv_back_card_image;
    TextView tv_next_step;

    public void initView(View view) {
        ImageView iv_back;
        TextView tv_popup_title, tv_popup_scan;
        ImageView imageView_scan;
        LinearLayout ll_date;

        TextView tv_bank_name;

        ImageView iv_date;

        iv_visa = view.findViewById(R.id.iv_visa);
        iv_mastercard = view.findViewById(R.id.iv_mastercard);

        iv_back = view.findViewById(R.id.iv_back);
        iv_back.setVisibility(View.GONE);
        LocaleUtils.viewRotationY(mContext, iv_back);

        tv_popup_title = view.findViewById(R.id.tv_popup_title);
        imageView_scan = view.findViewById(R.id.iv_scan);
        tv_popup_scan = view.findViewById(R.id.tv_popup_scan);

        imageView_scan.setVisibility(View.INVISIBLE);
        tv_popup_scan.setVisibility(View.INVISIBLE);

        fl_front_card = view.findViewById(R.id.fl_front_card);
        fl_front_card.setAlpha(1f);
        fl_front_card.setVisibility(View.VISIBLE);

        iv_front_card_image = view.findViewById(R.id.iv_front_card_image);

        fl_back_card = view.findViewById(R.id.fl_back_card);
        fl_back_card.setAlpha(0f);

        iv_back_card_image = view.findViewById(R.id.iv_back_card_image);

        tv_bank_name = view.findViewById(R.id.tv_bank_name);

        et_card_num = view.findViewById(R.id.et_card_num);
        et_full_name = view.findViewById(R.id.et_full_name);
        et_valid_time = view.findViewById(R.id.et_valid_time);
        et_valid_time.getEditText().setText(getText(R.string.add_new_card_6));
        et_valid_time.setEnabled(false);
        et_valid_time.setFocusable(false);
        et_valid_time.setFocusableInTouchMode(false);
        if (et_valid_time.getTlEdit() != null) {
            et_valid_time.getTlEdit().setHintAnimationEnabled(false);
        }

        iv_date = view.findViewById(R.id.iv_date);
        et_cvv_text = view.findViewById(R.id.et_cvv_text);
        ll_date = view.findViewById(R.id.ll_date);

        tv_next_step = view.findViewById(R.id.tv_next_step);
        tv_next_step.setEnabled(false);

        LocaleUtils.viewRotationY(this, iv_back_card_image);

        et_card_num.addTextChangedListener(new TextWatcher() {
            char space = ' ';
            //改变之前text长度
            int beforeTextLength = 0;
            //改变之前的文字
//            private CharSequence beforeChar;
            //改变之后text长度
            int onTextLength = 0;
            //是否改变空格或光标
            boolean isChanged = false;
            // 记录光标的位置
            int location = 0;
            private char[] tempChar;
            private StringBuffer buffer = new StringBuffer();
            //已有空格数量
            int spaceNumberB = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeTextLength = s.length();
                if (buffer.length() > 0) {
                    buffer.delete(0, buffer.length());
                }
                spaceNumberB = 0;
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == ' ') {
                        spaceNumberB++;
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onTextLength = s.length();
                buffer.append(s.toString());
                if (onTextLength == beforeTextLength || onTextLength <= 3
                        || isChanged) {
                    isChanged = false;
                    return;
                }
                isChanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                tv_next_step.setEnabled(false);
                if (isChanged) {
                    location = et_card_num.getEditText().getSelectionEnd();
                    int index = 0;
                    while (index < buffer.length()) {
                        if (buffer.charAt(index) == space) {
                            buffer.deleteCharAt(index);
                        } else {
                            index++;
                        }
                    }
                    index = 0;
                    int spaceNumberC = 0;
                    while (index < buffer.length()) {
                        if ((index == 4 || index == 9 || index == 14 || index == 19)) {
                            buffer.insert(index, space);
                            spaceNumberC++;
                        }
                        index++;
                    }

                    if (spaceNumberC > spaceNumberB) {
                        location += (spaceNumberC - spaceNumberB);
                    }

                    tempChar = new char[buffer.length()];
                    buffer.getChars(0, buffer.length(), tempChar, 0);
                    String str = buffer.toString();
                    if (location > str.length()) {
                        location = str.length();
                    } else if (location < 0) {
                        location = 0;
                    }

                    et_card_num.getEditText().setText(str);
                    Editable etable = et_card_num.getEditText().getText();
                    Selection.setSelection(etable, location);
                    isChanged = false;
                }
                String cardNum = et_card_num.getText().replace(" ", "").trim().toString();
                AndroidUtils.setCardLogoVisible(cardNum, iv_visa, iv_mastercard);
//                if (isVerifyCardSuccess
//                        && !TextUtils.isEmpty(et_card_num.getText().toString())
//                        && cardNum.length() == 16
//                        && !TextUtils.isEmpty(et_full_name.getText().toString())
//                        && !TextUtils.isEmpty(et_valid_time.getText().toString())
//                        && !et_valid_time.getText().toString().equalsIgnoreCase("MM/YY")) {
//
//                    tv_next_step.setEnabled(true);
//                } else {
//                    tv_next_step.setEnabled(false);
//                }
            }
        });

        et_card_num.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    //卡号输入框失去焦点则调用卡号校验接口
                    String number = et_card_num.getEditText().getText().toString().trim();
                    if (number.contains(" ")) {
                        String cardNum = number.replace(" ", "").trim().toString();
                        if (!TextUtils.isEmpty(cardNum) && cardNum.length() > 6) {
                            //本地有图先取本地的图
                            String resNum = cardNum.substring(0, 6);
                            if (mHashMaps.containsKey(resNum)) {
                                int resId = (int) mHashMaps.get(resNum);
                                if (resId != 0 && resId != -1) {
                                    iv_front_card_image.setImageResource(resId);
                                }
                            }
                            mPresenter.verifyCard(cardNum);
                        }
                    } else {
                        et_card_num.showErrorViewWithMsg("");
                    }
                }
            }
        });

        et_full_name.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(25), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_SPACE)});
        et_full_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
//                String cardNum = et_card_num.getText().replace(" ", "").trim().toString();
                if(isVerifyCardSuccess
                        && !TextUtils.isEmpty(et_card_num.getText().toString())
                        && et_card_num.getText().length() == 19
                        && !TextUtils.isEmpty(et_full_name.getText().toString())
                        && !TextUtils.isEmpty(et_valid_time.getText().toString())
                        && !et_valid_time.getText().toString().equalsIgnoreCase("MM/YY")){

                    tv_next_step.setEnabled(true);
                }else{
                    tv_next_step.setEnabled(false);
                }

            }
        });

        et_valid_time.getEditText().setCursorVisible(false);
        et_valid_time.getEditText().setInputType(InputType.TYPE_NULL);
        et_valid_time.getEditText().setFocusable(false);
        et_valid_time.getEditText().clearFocus();
        et_valid_time.getEditText().setKeyListener(null);
        et_valid_time.getEditText().setMovementMethod(null);


        et_valid_time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(isVerifyCardSuccess
                        && !TextUtils.isEmpty(et_card_num.getText().toString())
                        && et_card_num.getText().length() == 19
                        && !TextUtils.isEmpty(et_full_name.getText().toString())
                        && !TextUtils.isEmpty(et_valid_time.getText().toString())
                        && !et_valid_time.getText().toString().equalsIgnoreCase("MM/YY")){

                    tv_next_step.setEnabled(true);
                }else{
                    tv_next_step.setEnabled(false);
                }
            }
        });

        mShowDateFormat = new SimpleDateFormat("MM/yy", Locale.ENGLISH);

        et_cvv_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString()) && s.length() == 3) {
                    tv_next_step.setEnabled(true);
                } else {
                    tv_next_step.setEnabled(false);
                }
            }
        });


        tv_next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (tv_next_step.getText().toString().equalsIgnoreCase(mContext.getString(R.string.add_new_card_7))) {
                    et_cvv_text.getEditText().setText("");
                    fl_front_card.animate()
                            .alpha(0f)
                            .setDuration(mFadeoutAnimationDuration)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    fl_back_card.setVisibility(View.VISIBLE);
                                    fl_front_card.setVisibility(View.GONE);

                                    fl_back_card.animate()
                                            .alpha(1f)
                                            .setDuration(mFadeinAnimationDuration)
                                            .setListener(new AnimatorListenerAdapter() {
                                                @Override
                                                public void onAnimationEnd(Animator animation) {

                                                    iv_back.setVisibility(View.VISIBLE);

                                                    imageView_scan.setVisibility(View.GONE);
                                                    tv_popup_scan.setVisibility(View.GONE);

                                                    tv_next_step.setText(mContext.getString(R.string.add_new_card_9));

                                                    if (!TextUtils.isEmpty(et_cvv_text.getText().toString())
                                                            && et_cvv_text.getText().toString().trim().length() == 3) {
                                                        tv_next_step.setEnabled(true);
                                                    } else {
                                                        tv_next_step.setEnabled(false);
                                                    }
                                                }
                                            });
                                }
                            });


                } else if (tv_next_step.getText().toString().equalsIgnoreCase(mContext.getString(R.string.add_new_card_9))) {
                    //调用绑卡接口
                    String cardNum = et_card_num.getText().replace(" ", "").trim().toString();
                    cardBindRequest(et_full_name.getText().toString().trim(),
                            cardNum,
                            et_valid_time.getText().replace("/", "").toString().trim(),
                            et_cvv_text.getText().trim());
                }
            }
        });

        imageView_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //扫描卡调用第三方控件的逻辑
            }
        });

        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_card_num.getEditText().clearFocus();
                et_full_name.getEditText().clearFocus();

                selectDatetime(et_valid_time);
            }
        });

//        iv_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                et_card_num.getEditText().clearFocus();
//                et_full_name.getEditText().clearFocus();
//
//                selectDatetime(et_valid_time);
//            }
//        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fl_back_card.animate()
                        .alpha(0f)
                        .setDuration(mFadeoutAnimationDuration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                fl_front_card.setVisibility(View.VISIBLE);
                                fl_back_card.setVisibility(View.GONE);

                                fl_front_card.animate()
                                        .alpha(1f)
                                        .setDuration(mFadeinAnimationDuration)
                                        .setListener(new AnimatorListenerAdapter() {
                                            @Override
                                            public void onAnimationEnd(Animator animation) {

                                                iv_back.setVisibility(View.GONE);

                                                tv_next_step.setText(mContext.getString(R.string.add_new_card_7));

                                                String cardNum = et_card_num.getText().replace(" ","").trim().toString();
                                                if(isVerifyCardSuccess
                                                        &&  !TextUtils.isEmpty(et_card_num.getText().toString())
                                                        &&  cardNum.length() == 16
                                                        && !TextUtils.isEmpty(et_full_name.getText().toString())
                                                        && !TextUtils.isEmpty(et_valid_time.getText().toString())
                                                        && !et_valid_time.getText().toString().equalsIgnoreCase("MM/YY")){

                                                    tv_next_step.setEnabled(true);
                                                }else{
                                                    tv_next_step.setEnabled(false);
                                                }
                                            }
                                        });
                            }
                        });


            }
        });
    }

    public void selectDatetime(EditTextWithDel et_valid_time) {
        selectedDate.setTime(mSelectedDate);
        TimePickerDialogUtils.showTimePickerWithoutDay(mContext, selectedDate, startDate, endDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedDate = date;
                expire_data = mShowDateFormat.format(date);
                et_valid_time.getEditText().setText(expire_data);
            }
        });
    }


    @Override
    public void getBindCardListSuccess(List<CardEntity> result) {
        newCardList = result;
        allCardList.clear();
        allCardList.addAll(result);
        int cardNum = newCardList.size();
        if (cardNum > 0) {
            setStyle(cardNum);
            ivNoCard.setVisibility(View.GONE);
            tvNoCardText.setVisibility(View.GONE);
            if (cardNum == 1) {
                conDefault.setVisibility(View.GONE);
            } else {
                conDefault.setVisibility(View.VISIBLE);
                for (int i = 0; i < cardNum; i++) {
                    CardEntity cardEntity = newCardList.get(i);
                    if ("1".equals(cardEntity.getDefaultCardInd())) {
                        defaultCardEntity = cardEntity;
                        setDefaultCard(cardEntity);
                        newCardList.remove(cardEntity);
                        cardNum = cardNum - 1;
                        tvDefaultCardNum.setText(AndroidUtils.convertBankCardNoWithBlank(cardEntity.getCardNo()));
                    }
                }
            }
            stackview.setAdapter(stackCardAdapter, mContext, newCardList);

//            addCard(cardNum, rlCardList);

        } else {
            ivNoCard.setVisibility(View.VISIBLE);
            tvNoCardText.setVisibility(View.VISIBLE);
            conDefault.setVisibility(View.GONE);
        }
    }

    private void setDefaultCard(CardEntity cardEntity) {
        //本地有图先取本地的图
        int resIdBar = (int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6));
        AndroidUtils.setCardLogoVisible(cardEntity.getCardNo(), iv_default_visa, iv_default_mastercard);
        if (resIdBar != 0 && resIdBar != -1) {
            ivDefaultCard.setImageResource((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)));
        } else {
            if (cardEntity.getCardFacebar() != null && !TextUtils.isEmpty(cardEntity.getCardFacebar())) {
                String card_url = (String) mHashMaps_card_bar.get(cardEntity.getCardFacebar());
                Glide.with(CardManageActivity.this)
                        .load(card_url)
                        .placeholder((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)))
                        .error((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)))
                        .into(ivDefaultCard);
            } else {
                ivDefaultCard.setImageResource((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)));
            }
        }
    }

    @Override
    public void getBindCardListFailed(String errorCode, String errorMsg) {
        //获取卡列表失败，则直接展示无数据页面
        rlCardList.setVisibility(View.GONE);
        stackview.setVisibility(View.GONE);
        conDefault.setVisibility(View.GONE);
        ivNoCard.setVisibility(View.VISIBLE);
        tvNoCardText.setVisibility(View.VISIBLE);

        showTipDialog(errorMsg);

    }

    private void dismissDialog() {
        if (bottomDialog != null && bottomDialog.isShowing()) {
            bottomDialog.dismiss();
        }
    }

    @Override
    public void bindNewCardSuccess(CardBind3DSEntity result) {
        cardBind3DSEntity = result;
        if (cardBind3DSEntity.getRedirectUrl() != null && !TextUtils.isEmpty(cardBind3DSEntity.getRedirectUrl())) {

            dismissDialog();
            //跳转到3ds验证页面
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.BIND_CARD_3DS, cardBind3DSEntity.getRedirectUrl());

            ActivitySkipUtil.startAnotherActivity(CardManageActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        }
    }

    @Override
    public void bindNewCardFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void verifyCardSuccess(verifyCardEntity result) {
        try {
            String cardNum = et_card_num.getText().replace(" ", "").trim().toString();
            //如果卡号校验成功,则更换卡面
            if (result != null) {
                showCardNo = result.cardNo;
                isVerifyCardSuccess = true;
                int resId = 0;
                if (mHashMaps.containsKey(cardNum.substring(0, 6))) {
                    resId = (int) mHashMaps.get(cardNum.substring(0, 6));
                }
                if (resId != 0 && resId != -1) {
                    iv_front_card_image.setImageResource((int) mHashMaps.get(cardNum.substring(0, 6)));
                } else {
                    if (result.getCardFace() != null && !TextUtils.isEmpty(result.getCardFace())) {
                        Glide.with(this)
                                .load(result.getCardFace())
                                .placeholder((int) mHashMaps.get(cardNum.substring(0, 6)))
                                .error((int) mHashMaps.get(cardNum.substring(0, 6)))
                                .into(iv_front_card_image);
                    } else {
                        iv_front_card_image.setImageResource((int) mHashMaps.get(cardNum.substring(0, 6)));
                    }
                }


                int resId_back = (int) mHashMaps_back.get(cardNum.substring(0, 6));
                if (resId_back != 0 && resId_back != -1) {
                    iv_back_card_image.setImageResource((int) mHashMaps_back.get(cardNum.substring(0, 6)));
                } else {
                    if (result.getCardFaceBack() != null && !TextUtils.isEmpty(result.getCardFaceBack())) {
                        Glide.with(this)
                                .load(result.getCardFaceBack())
                                .placeholder((int) mHashMaps_back.get(cardNum.substring(0, 6)))
                                .error((int) mHashMaps_back.get(cardNum.substring(0, 6)))
                                .into(iv_back_card_image);
                    } else {
                        if (mHashMaps_back.containsKey(cardNum.substring(0, 6))) {
                            iv_back_card_image.setImageResource((int) mHashMaps_back.get(cardNum.substring(0, 6)));
                        }
                    }
                }

                if (isVerifyCardSuccess
                        && !TextUtils.isEmpty(et_card_num.getText().toString())
                        && cardNum.length() == 16
                        && !TextUtils.isEmpty(et_full_name.getText().toString())
                        && !TextUtils.isEmpty(et_valid_time.getText().toString())
                        && !et_valid_time.getText().toString().equalsIgnoreCase("MM/YY")) {

                    tv_next_step.setEnabled(true);
                } else {
                    tv_next_step.setEnabled(false);
                }
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void verifyCardFailed(String errorCode, String errorMsg) {
        try {
            //如果卡号校验失败，则前端展示errorMsg
            isVerifyCardSuccess = false;
            et_card_num.showErrorViewWithMsg(errorMsg);
            et_card_num.setErrorColor(R.color.color_80white);
            iv_front_card_image.setImageResource(R.drawable.default_card_front);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public CardManageContract.Presenter getPresenter() {
        return new CardManagePresenter();
    }


}
