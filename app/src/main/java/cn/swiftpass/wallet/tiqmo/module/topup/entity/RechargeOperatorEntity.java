package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeOperatorEntity extends BaseEntity {
    public String id;
    //是否默认属于运营商 true是 false否
    public boolean identified;
    //运营商名称
    public String operatorName;
    //国家ISO代码
    public String countryIsoCode;
    public String countryCode;
    //国家名称
    public String countryName;

    public String mobileNumber;
    public String shortName;
    public String operatorImgUrl;
}
