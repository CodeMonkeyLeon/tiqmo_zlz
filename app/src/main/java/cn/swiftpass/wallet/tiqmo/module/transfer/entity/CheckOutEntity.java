package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CheckOutEntity extends BaseEntity {

    public String exchangeRate;
    public String merchantId;
    public String merchantLogo;
    public String merchantName;
    public boolean needAddNewPayChannel;
    public String openId;
    public String orderAmount  = "0";
    public String orderCurrencyCode;
    public String orderNo;
    public String monthLimitAmt;
    public String orderType;
    public String partnerNo;
    public String partnerUserId;
    public String payerAmount  = "0";
    public String payerCurrencyCode;
    public List<PaymentMethodEntity> paymentMethodList = new ArrayList<>();
    public String productDesc;
    //手续费
    public String serviceFee;
    public String serviceFeeCurrencyCode;

    //原增值税金额
    public String vat;
    //优惠增值税金额
    public String discountVat  = "0";
    //原手续费金额
    public String transFees;
    //优惠手续费金额
    public String discountTransFees  = "0";
    public String voucherDarkIconUrl;
    public String voucherLightIconUrl;
    public String voucherAmount  = "0";

    public String cardType;
    public String cardNo;
    public String couponType;
    //优惠金额
    public String couponAmount  = "0";
    public String couponCurrencyCode;
    public String couponAmountModule;
    //品牌产品合同条款
    public String voucherTermsConditions;
    //品牌产品隐私条款
    public String voucherPrivateNotice;
    //接收人名称
    public String receiptFullName;
    //接收人性别
    public String receiptSex;
    //目标金额
    public String destinationAmount  = "0";
    //目标币种
    public String destinationCurrency;
    public String destinationCountry;
    public String purpose;
    //汇款类型 AT代理，BT银行
    public String receiptMethod;
    public String receiptMethodDesc;
    //收款人机构名称
    public String receiptOrgName;
    //收款人账户
    public String receiptAccountNo;

    public String brandName;
}
