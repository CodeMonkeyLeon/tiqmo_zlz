package cn.swiftpass.wallet.tiqmo.sdk.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class AgreeEntity extends BaseEntity {

    /**
     * type=1为user_agreement，type=2为payment_cards_t_c，type=3为charity_terms_condition，type=4为privacy_policy
     */
    public String type;
    public String url;
}
