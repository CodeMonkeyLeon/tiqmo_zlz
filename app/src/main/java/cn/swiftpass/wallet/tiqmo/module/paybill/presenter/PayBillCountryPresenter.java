package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListNewEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillCountryPresenter implements PayBillContract.BillCountryPresenter {

    PayBillContract.BillCountryView billCountryView;


    @Override
    public void getPayBillCategory(final String countryCode, final String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillCategory(countryCode, channelCode, new LifecycleMVPResultCallback<PayBillTypeListNewEntity>(billCountryView, true) {
            @Override
            protected void onSuccess(PayBillTypeListNewEntity result) {
                if (billCountryView != null) {
                    billCountryView.getPayBillCategorySuccess(result, countryCode, channelCode);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (billCountryView != null) {
                    billCountryView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getPayBillCountry() {
        AppClient.getInstance().getTransferManager().getPayBillCountry(new LifecycleMVPResultCallback<PayBillCountryListEntity>(billCountryView, true) {
            @Override
            protected void onSuccess(PayBillCountryListEntity result) {
                billCountryView.getPayBillCountrySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billCountryView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillCountryView billCountryView) {
        this.billCountryView = billCountryView;
    }

    @Override
    public void detachView() {
        this.billCountryView = null;
    }
}
