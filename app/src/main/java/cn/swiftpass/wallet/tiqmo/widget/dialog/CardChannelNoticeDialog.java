package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import cn.swiftpass.wallet.tiqmo.R;

public class CardChannelNoticeDialog extends BottomDialog {

    private Context mContext;

    public CardChannelNoticeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(context);
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_setting_channel, null);

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
