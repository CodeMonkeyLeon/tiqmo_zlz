package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Executor;

import javax.crypto.Cipher;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.TouchIDContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.TouchIDPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.manager.BiometricManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.FingerPrintHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class KycSuccessActivity extends BaseCompatActivity<TouchIDContract.Presenter> implements TouchIDContract.View {

    @BindView(R.id.tv_face_id)
    TextView tvFaceId;
    @BindView(R.id.tv_thanks)
    TextView tvThanks;
    @BindView(R.id.ll_content)
    LinearLayout llContent;

    private FingerPrintHelper mFingerPrintHelper;
    private Handler handler = new Handler();

    private Executor executor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };

    @Override
    protected int getLayoutID() {
        return R.layout.activity_kyc_success;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
        setFinishOnTouchOutside(false);
        mFingerPrintHelper = FingerPrintHelper.newInstance(ProjectApp.getContext());

        if (ProjectApp.isPhoneNotSupportedFace()) {
            tvFaceId.setVisibility(View.GONE);
        } else {
            tvFaceId.setVisibility(View.VISIBLE);
        }

        tvFaceId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ProjectApp.isPhoneNotSupportedFace()) {
                    if (!ProjectApp.isBiometricSupported()) {
                        showErrorDialog(getString(R.string.common_22));
                    }else{
                        tryOpenTouchIDPay();
                    }
                }
            }
        });
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
        switch (UserInfoManager.getInstance().getKycType()) {
            case Constants.KYC_TYPE_ACCOUNT:
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_OPEN_ACCOUNT));
                break;
            case Constants.KYC_TYPE_ADD_CARD:
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ADD_CARD));
                break;
            case Constants.KYC_TYPE_HOME_SCAN:
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_HOME_SCAN));
                break;
            case Constants.KYC_TYPE_ADD_MONEY_SCAN:
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ADD_MONEY_SCAN));
                break;
            case Constants.KYC_TYPE_SPLIT_BILL:
                EventBus.getDefault().post(new EventEntity(EventEntity.KYC_TYPE_SPLIT_BILL));
                break;
            case Constants.KYC_TYPE_IMR:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_IMR));
                break;
            case Constants.KYC_TYPE_E_VOUCHER:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_E_VOUCHER));
                break;
            case Constants.KYC_TYPE_TOP_UP:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_TOP_UP));
                break;
            case Constants.KYC_TYPE_KSA_ADD_CARD:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_KSA_ADD_CARD));
                break;
            case Constants.KYC_TYPE_REFERRAL_CODE:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_REFERRAL_CODE));
                break;
            case Constants.KYC_TYPE_CUSTOMER_INFO:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_CUSTOMER_INFO));
                break;
            case Constants.KYC_TYPE_ACCOUNT_DETAIL:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_ACCOUNT_DETAIL));
                break;
            case Constants.KYC_TYPE_PAY_BILL:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_PAY_BILL));
                break;
            case Constants.KYC_TYPE_HOME_ADD_MONEY:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_HOME_ADD_MONEY));
                break;
            case Constants.KYC_TYPE_HOME_SEND_MONEY:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_TYPE_HOME_SEND_MONEY));
                break;
            case Constants.KYC_CHANGE_PHONE_NUMBER:
                EventBus.getDefault().post(new EventEntity(Constants.KYC_CHANGE_PHONE_NUMBER));
                break;
            default:
                break;
        }
    }

    @OnClick({R.id.tv_thanks})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_thanks:
                showProgress(true);
                AppClient.getInstance().notUseFinger(Constants.NO_USE_FINGER_PAY,new ResultCallback<Void>() {
                    @Override
                    public void onResult(Void response) {
                        showProgress(false);
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                        showTipDialog(errorMsg);
                    }
                });
                finish();
                break;
            default:
                break;
        }
    }

    //生物认证的setting
    private void showBiometricPrompt() {
        BiometricPrompt.PromptInfo promptInfo =
                new BiometricPrompt.PromptInfo.Builder()
                        .setTitle(getString(R.string.common_40)) //设置大标题
                        .setSubtitle(getString(R.string.common_41)) // 设置标题下的提示
                        .setNegativeButtonText(getString(R.string.common_6)) //设置取消按钮
                        .build();
        //需要提供的参数callback
        BiometricPrompt biometricPrompt = new BiometricPrompt(KycSuccessActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            //各种异常的回调
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(ProjectApp.mContext,
                                getString(R.string.common_42) + ": " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            //认证成功的回调
            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                BiometricPrompt.CryptoObject authenticatedCryptoObject =
                        result.getCryptoObject();
                mPresenter.setEnableTouchIDPayment(true);
            }

            //认证失败的回调
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(ProjectApp.mContext, getString(R.string.common_44),
                                Toast.LENGTH_SHORT)
                        .show();
            }
        });
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            FingerPrintHelper mFingerPrintHelper = FingerPrintHelper.newInstance(mContext);
            if (mFingerPrintHelper == null) {
                return;
            }
            String mSecretKeyName = AppClient.getInstance().getUserID() + BiometricManager.FINGERPRINT_SECRET_KEY_PAYMENT;
            Cipher cipher = mFingerPrintHelper.getCipherIfFingerPrintNoChange(mSecretKeyName);
            if (cipher == null) {
                cipher = mFingerPrintHelper.generateNewSecretKey(mSecretKeyName);
                if (cipher == null) {
                    LogUtil.e("cipher == null");
                    return;
                }
            }
            BiometricPrompt.CryptoObject crypto = new BiometricPrompt.CryptoObject(cipher);
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo, crypto);
        } else {
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo);
        }
    }

    private void tryOpenTouchIDPay() {
        if (!mFingerPrintHelper.hasEnrolledFingerprints()) {
//            showErrorDialog(getString(R.string.Hint_PleaseRegisterFingerprint));
            return;
        }
        showBiometricPrompt();
    }

    @Override
    public void switchTouchIDPayment(boolean isOpen) {
        if (isOpen) {
            showToast(getString(R.string.Hint_FingerprintOpenSuccessful));
            finish();
        } else {
            showTipDialog(getString(R.string.common_35));
        }
    }

    @Override
    public void switchTouchIDLogin(boolean isOpen) {

    }

    @Override
    public TouchIDContract.Presenter getPresenter() {
        return new TouchIDPresenter();
    }
}
