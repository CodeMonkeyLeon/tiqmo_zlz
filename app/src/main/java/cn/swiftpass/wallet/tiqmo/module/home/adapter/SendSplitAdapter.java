package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Typeface;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zrq.spanbuilder.Spans;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.CircularProgressView1;
import cn.swiftpass.wallet.tiqmo.widget.ColorItem;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SendSplitAdapter extends BaseRecyclerAdapter<SplitListEntity.SplitDetail> {
    public SendSplitAdapter(@Nullable List<SplitListEntity.SplitDetail> data) {
        super(R.layout.item_send_split, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, SplitListEntity.SplitDetail item, int position) {
        holder.setText(R.id.tv_amount,Spans.builder()
                .text(AndroidUtils.getTransferMoney(item.getTotalAmount()))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_title_text_color)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(28)
                .text(" " + LocaleUtils.getCurrencyCode(item.getCurrency()))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_60white_603a3b44)))
                .size(20)
                .build());

        holder.setText(R.id.tv_title,item.getSplitTitel());
        holder.setText(R.id.tv_time,item.getSplitTime());

        holder.setText(R.id.tv_paid,Spans.builder()
                .text(item.getPaidProportion())
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_title_text_color)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(20)
                .text("%")
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_title_text_color)))
                .size(18)
                .build());

        CircularProgressView1 progress  = holder.getView(R.id.progress_received);
        int paid = Integer.parseInt(item.getPaidProportion());
        int unPaid = Integer.parseInt(item.getUnpaidProportion());
        int rejected = Integer.parseInt(item.getRejectedProportion());
        setActivityPie(progress,paid,unPaid,rejected);
    }

    private void setActivityPie(CircularProgressView1 progress, int paid, int unPaid, int rejected) {
        try {
            progress.setVisibility(View.VISIBLE);
            List<ColorItem> items = new ArrayList<>();
            progress.setBackColor(R.color.transparent);

            if (paid != 0) {
                items.add(new ColorItem(paid, mContext.getColor(R.color.green2), "0"));
            }
            if (unPaid != 0) {
                items.add(new ColorItem(unPaid, mContext.getColor(R.color.color_f1a13c), "0"));
            }
            if (rejected != 0) {
                items.add(new ColorItem(rejected, mContext.getColor(R.color.red2), "0"));
            }

            if (paid == 100 || unPaid == 100 || rejected == 100) {
                progress.setSpaceProgressAngle(0);
            }else {
                progress.setSpaceProgressAngle(360*3/100);
            }
            progress.setItems(items);

        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }
}
