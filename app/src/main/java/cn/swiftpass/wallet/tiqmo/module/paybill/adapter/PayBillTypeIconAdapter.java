package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.CommonAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeIconEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;

public class PayBillTypeIconAdapter extends CommonAdapter {

    private Context context;

    private LayoutInflater inflater;

    private OnItemClick onItemClickListener;

    public void setOnItemClickListener(OnItemClick itemClickListener) {
        this.onItemClickListener = itemClickListener;
    }

    public interface OnItemClick {
        void onItemClick(PayBillTypeIconEntity payBillTypeIconEntity, int position);
    }

    public PayBillTypeIconAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_paybill_icon_type, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final PayBillTypeIconEntity payBillTypeIconEntity = (PayBillTypeIconEntity) getItem(position);
        if (payBillTypeIconEntity != null) {
            String typeLogo = payBillTypeIconEntity.imgUrl;
            Glide.with(context)
                    .load(typeLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(context,R.attr.utilities))
                    .into(holder.iv_icon);
            holder.tv_icon.setText(payBillTypeIconEntity.billerDescriptionShow);
            holder.tv_icon.setTextColor(ResourceHelper.getInstance(context).getColorByAttr(R.attr.home_item_text));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ButtonUtils.isFastDoubleClick()) {
                        return;
                    }
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(payBillTypeIconEntity, position);
                    }
                }
            });
        }
        return convertView;
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView iv_icon;
        @BindView(R.id.tv_icon)
        TextView tv_icon;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
