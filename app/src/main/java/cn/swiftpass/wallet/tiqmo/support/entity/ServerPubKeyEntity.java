package cn.swiftpass.wallet.tiqmo.support.entity;

/**
 * 服务端RSA公钥对象
 */

public class ServerPubKeyEntity extends BaseEntity {
    private String serverPubKey;

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String serverPubKey) {
        this.serverPubKey = serverPubKey;
    }
}
