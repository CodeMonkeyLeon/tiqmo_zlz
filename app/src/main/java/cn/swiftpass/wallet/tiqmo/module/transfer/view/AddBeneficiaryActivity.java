package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.TransferRelationAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.AddBeneficiaryPresenter;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.SaveBeneDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class AddBeneficiaryActivity extends BaseCompatActivity<TransferContract.AddBeneficiaryPresenter> implements TransferContract.AddBeneficiaryView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_beneficiary_name)
    EditTextWithDel etBeneficiaryName;
    @BindView(R.id.et_ibank_number)
    CustomizeEditText etIbankNumber;
    @BindView(R.id.rl_relation)
    RecyclerView rlRelation;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.ll_save_bene)
    LinearLayout llSaveBene;
    @BindView(R.id.cb_save_bene)
    CheckBox cbSaveBene;
    @BindView(R.id.ll_relationship)
    LinearLayout llRelationship;

    private TransferRelationAdapter transferRelationAdapter;
    private LinearLayoutManager mLayoutManager;
    public List<KycContactEntity> contactList = new ArrayList<>();

    private String beneficiaryName, beneficiaryIbanNo, chooseRelationship = "1";

    private TimeEntity timeEntity;
    private BeneficiaryEntity beneficiaryEntity;

    private String saveFlag = "0";

    private SaveBeneDialog saveBeneDialog;

    private RiskControlEntity riskControlEntity;

    private boolean isFromHome;

    private static AddBeneficiaryActivity addBeneficiaryActivity;
    private BottomDialog bottomDialog;

    public static AddBeneficiaryActivity getAddBeneficiaryActivity() {
        return addBeneficiaryActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_ibank;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        addBeneficiaryActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        if(getIntent() != null && getIntent().getExtras() != null){
            isFromHome = getIntent().getExtras().getBoolean(Constants.isFromHome);
        }
        if(isFromHome){
            saveFlag = "1";
            tvTitle.setText(R.string.wtba_56);
            tvContent.setText(R.string.DigitalCard_110);
        }else {
            tvTitle.setText(R.string.DigitalCard_106);
        }
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rlRelation.setLayoutManager(mLayoutManager);
        contactList.add(new KycContactEntity(getString(R.string.newhome_37), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_my_favourite)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_6), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_my_account)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_7), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_family)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_8), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_friends)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_9), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_service)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_9_1), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_other_relation)));
        transferRelationAdapter = new TransferRelationAdapter(contactList);
        transferRelationAdapter.bindToRecyclerView(rlRelation);

        transferRelationAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.con_choose:
                        transferRelationAdapter.setPosition(position);
                        chooseRelationship = String.valueOf(position);
                        break;
                    default:
                        break;
                }
                checkContinueStatus();
            }
        });
        cbSaveBene.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    saveFlag = "0";
                    chooseRelationship = "";
                    transferRelationAdapter.setPosition(-1);
                    llRelationship.setVisibility(View.VISIBLE);
                } else {
                    saveFlag = "1";
                    chooseRelationship = "1";
                    llRelationship.setVisibility(View.GONE);
                }
                checkContinueStatus();
            }
        });
        etIbankNumber.setInputNumber(true);
        etIbankNumber.setLeftText(getString(R.string.wtba_11) + " ");
        etIbankNumber.setClickListener(new CustomizeEditText.OnViewClickListener() {
            @Override
            public void onRightViewClick() {
                etIbankNumber.setInputNumber(true);

            }
        });
        etBeneficiaryName.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(60), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_SPACE)});
        etIbankNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(22)});
        etBeneficiaryName.getTlEdit().setHint(getString(R.string.wtba_3));
        if(isFromHome) {
            beneficiaryName = getUserInfoEntity().userName;
            etBeneficiaryName.setContentText(beneficiaryName);
            etBeneficiaryName.setEditTextEnable(false);
        }else{
            llSaveBene.setVisibility(View.VISIBLE);
            cbSaveBene.setChecked(true);
        }
        etBeneficiaryName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                beneficiaryName = s.toString();
                if (TextUtils.isEmpty(beneficiaryName)) {
                    tvContinue.setEnabled(false);
                } else {
                    checkContinueStatus();
                }
            }
        });

        etIbankNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    beneficiaryIbanNo = etIbankNumber.getText().toString().trim();
                    if (!TextUtils.isEmpty(beneficiaryIbanNo)) {
                        if (beneficiaryIbanNo.length() != 22) {
                            etIbankNumber.setError(getString(R.string.wtba_4_1));
                        }
                    }
                }
            }
        });


        etIbankNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etIbankNumber.setError("");
                beneficiaryIbanNo = s.toString();
                if (TextUtils.isEmpty(beneficiaryIbanNo)) {
                    tvContinue.setEnabled(false);
                } else {
                    checkContinueStatus();
                }
            }
        });
    }

    private void checkContinueStatus() {
        if (TextUtils.isEmpty(beneficiaryName) || TextUtils.isEmpty(beneficiaryIbanNo) || beneficiaryIbanNo.length() < 22 || TextUtils.isEmpty(chooseRelationship)) {
            tvContinue.setEnabled(false);
        } else {
            tvContinue.setEnabled(true);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_continue})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_continue:
                beneficiaryName = etBeneficiaryName.getEditText().getText().toString().trim();
                beneficiaryIbanNo = etIbankNumber.getText().toString().trim();
                beneficiaryIbanNo = "SA" + beneficiaryIbanNo;

                mPresenter.addBeneficiary(beneficiaryName, beneficiaryIbanNo, chooseRelationship, saveFlag);
//                List<String> additionalData = new ArrayList<>();
//                mPresenter.getOtpType("ActionType", "IMRP", additionalData);
//                showSaveDialog();
                break;
            default:
                break;
        }
    }

//    private void showSaveDialog() {
//        saveBeneDialog = new SaveBeneDialog(mContext);
//        saveBeneDialog.setBottomButtonListener(new SaveBeneDialog.BottomButtonListener() {
//            @Override
//            public void clickSaveButton() {
//                if (saveBeneDialog != null) {
//                    saveBeneDialog.dismiss();
//                }
//                saveFlag = "0";
//                mPresenter.addBeneficiary(beneficiaryName, beneficiaryIbanNo, chooseRelationship, saveFlag);
//            }
//
//            @Override
//            public void clickCancelButton() {
//                if (saveBeneDialog != null) {
//                    saveBeneDialog.dismiss();
//                }
//                saveFlag = "1";
//                mPresenter.addBeneficiary(beneficiaryName, beneficiaryIbanNo, chooseRelationship, saveFlag);
//            }
//        });
//        saveBeneDialog.showWithBottomAnim();
//    }

    @Override
    public void AddBeneficiaryFailed(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else if ("010170".equals(errorCode)) {
            showTipDialog(errorMsg);
        } else {
            etIbankNumber.setError(errorMsg);
        }
    }

    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void AddBeneficiarySuccess(BeneficiaryEntity beneficiaryEntity) {
        if (beneficiaryEntity != null) {
            this.beneficiaryEntity = beneficiaryEntity;
            riskControlEntity = beneficiaryEntity.riskControlInfo;

            AppsFlyerHelper.getInstance().sendAddBeneEvent();

            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestAddBene();
                }
            } else {
                requestAddBene();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (saveBeneDialog != null) {
            saveBeneDialog.dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        addBeneficiaryActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_WITHDRAW_ADD_BENEFICIARY == event.getEventType()) {
            requestAddBene();
        }
    }

    private void requestAddBene(){
        eventBus.post(new EventEntity(EventEntity.EVENT_ADD_BENEFICIARY_SUCCESS));
        mPresenter.getTime(beneficiaryEntity.beneficiaryIbanNo);
    }

    @Override
    public void getTimeFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getTimeSuccess(TimeEntity timeEntity) {
        this.timeEntity = timeEntity;
        mPresenter.getLimit(Constants.LIMIT_TRANSFER);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        if (saveBeneDialog != null) {
            saveBeneDialog.dismiss();
        }

        HashMap<String, Object> limitMap = new HashMap<>();
        limitMap.put(Constants.isFromHome,isFromHome);
        limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        limitMap.put(Constants.TimeEntity, timeEntity);
        limitMap.put(Constants.BeneficiaryEntity, beneficiaryEntity);
        ActivitySkipUtil.startAnotherActivity(AddBeneficiaryActivity.this, BeneficiaryMoneyActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        if ("0".equals(saveFlag)) {
            finish();
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public TransferContract.AddBeneficiaryPresenter getPresenter() {
        return new AddBeneficiaryPresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestAddBene();
                }
            } else {
                requestAddBene();
            }
        } else {
            requestAddBene();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_WITHDRAW_ADD_IMR);
        if(isFromHome) {
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtba_56));
        }else{
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_106));
        }
        mHashMap.put(Constants.sceneType, Constants.TYPE_WITHDRAW_ADD_BENEFICIARY);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
