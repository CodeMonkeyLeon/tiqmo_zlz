package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.TouchIDContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.PreVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class TouchIDPresenter implements TouchIDContract.Presenter {

    private TouchIDContract.View mTouchIDView;

    @Override
    public void attachView(TouchIDContract.View view) {
        mTouchIDView = view;
    }

    @Override
    public void detachView() {
        mTouchIDView = null;
    }

    @Override
    public String getUserID() {
        return AppClient.getInstance().getUserID();
    }

    @Override
    public void checkTouchIDPaymentStateFromServer(String type) {
        AppClient.getInstance().getBiometricManager().preVerify(type,new LifecycleMVPResultCallback<PreVerifyEntity>(mTouchIDView) {
            @Override
            protected void onSuccess(PreVerifyEntity result) {
                mTouchIDView.switchTouchIDPayment(true);
            }

            @Override
            protected void onFail(String code, String error) {
                if (ResponseCallbackWrapper.RESPONSE_CODE_BIOMETRIC_NOT_MATCH.equals(code)) {
                    AppClient.getInstance().getBiometricManager().unregisterFingerprintPayment();
                    mTouchIDView.switchTouchIDPayment(false);
                } else {
                    mTouchIDView.switchTouchIDPayment(true);
                }
            }
        });
    }

    @Override
    public void setEnableTouchIDLogin(boolean isEnable) {
        if (isEnable) {
            AppClient.getInstance().getBiometricManager().registerFingerprintPayment(new LifecycleMVPResultCallback<Void>(mTouchIDView) {
                @Override
                protected void onSuccess(Void result) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(true);
                    mTouchIDView.switchTouchIDLogin(true);
                }

                @Override
                protected void onFail(String code, String error) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(false);
                    mTouchIDView.switchTouchIDLogin(false);
                }
            });
        } else {
            if (AndroidUtils.isOpenBiometricLogin() && !AndroidUtils.isOpenBiometricPay()) {
                AppClient.getInstance().getBiometricManager().unregisterFingerprintPayment();
            }
            AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(false);
        }
    }

    @Override
    public void setEnableTouchIDPayment(boolean isEnable) {
        if (isEnable) {
            AppClient.getInstance().getBiometricManager().registerFingerprintPayment(new LifecycleMVPResultCallback<Void>(mTouchIDView) {
                @Override
                protected void onSuccess(Void result) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(true);
                    mTouchIDView.switchTouchIDPayment(true);
                }

                @Override
                protected void onFail(String code, String error) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(false);
                    mTouchIDView.switchTouchIDPayment(false);
                }
            });
        } else {
            if (!AndroidUtils.isOpenBiometricLogin() && AndroidUtils.isOpenBiometricPay()) {
                AppClient.getInstance().getBiometricManager().unregisterFingerprintPayment();
            }
            AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(false);
        }
    }
}
