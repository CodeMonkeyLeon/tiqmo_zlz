package cn.swiftpass.wallet.tiqmo.widget.wheelview.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
