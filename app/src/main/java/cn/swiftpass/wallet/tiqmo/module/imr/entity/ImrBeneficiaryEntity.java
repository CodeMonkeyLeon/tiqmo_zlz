package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrBeneficiaryEntity extends BaseEntity {
    //收款方式，AT：代理，BT：银行
    public String receiptMethod;
    //收款机构代码
    public String receiptOrgCode;
    //收款机构支行代码
    public String receiptOrgBranchCode;
    //收款人名称全称
    public String payeeFullName;
    //收款人昵称
    public String nickName;
    //关系名称代码
    public String relationshipCode;
    //手机冠码
    public String callingCode;
    //收款人手机号
    public String phone;
    //转账目的地国家代码
    public String transferDestinationCountryCode;
    //收款人国家代码
    public String payeeInfoCountryCode;
    //出生地
    public String birthPlace;
    //生日日期
    public String birthDate;
    //性别 0:男 1:女
    public String sex;
    //所属城市
    public String cityName;
    //所属地区
    public String districtName;
    //邮政信箱
    public String poBox;
    //楼层编号
    public String buildingNo;
    //街道地址
    public String street;
    //idNo
    public String idNo;
    //证件过期日期
    public String idExpiry;
    //银行账户类型 IBAN:IBAN SWIFT:SWIFT
    public String bankAccountType;
    //iban号码
    public String ibanNo;
    //银行账号
    public String bankAccountNo;

    public String destinationName;
    public String countryLogo;
    public String nationality;
    //前端页面展示地址&ID的类型，* 0：都不展示（跳过此页面），1：只展示Address页面，2：只展示ID页面，3：展示Address&Id页面
    public String bankAddrIdType;
    //	前端页面展示地址&ID的类型，* 0：都不展示（跳过此页面），1：只展示Address页面，2：只展示ID页面，3：展示Address&Id页面
    public String agentAddrIdType;
    //Iban前缀
    public String ibanPreCode;
    //Iban长度
    public String ibanLength;
    //保存标识 0 — 保存 1 — 不保存
    public String saveFlag;
    //是否展示保存到list
    public boolean showSaveList = false;
    public String payeeInfoId;

    //	是否支持IBAN，Y：支持，N：不支持
    public String ibanSupport;
    //是否支持ACCOUNT，Y：支持，N：不支持
    public String accountSupport;

    //银行模式
    public String bankMode;
    //银行模式支持币种
    public List<String> bankSupportCurrencyCodes = new ArrayList<>();
    //机构模式
    public String agentMode;
    //机构模式支持币种
    public List<String> agentSupportCurrencyCodes = new ArrayList<>();

    public String beneCurrency;
    public String cityId;
    public String receiptOrgName;
    public String receiptOrgBranchName;
    public String channelPayeeId;
    public String channelCode;
    public String branchId;
    //付款机构信息
    public List<ImrPaymentEntity> paymentMethodList = new ArrayList<>();
    public ImrPaymentEntity imrPaymentEntity;
}
