package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class VouSupCountryEntity extends BaseEntity {

    public String voucherServiceProviderCode;
    public String voucherBrandCode;
    public String brandName;
    public ArrayList<VouSupCountry> voucherSupportCountry;

    public static class VouSupCountry extends BaseEntity {
        public String countryName;
        public String countryCode;
        public String countryLogo;
    }
}
