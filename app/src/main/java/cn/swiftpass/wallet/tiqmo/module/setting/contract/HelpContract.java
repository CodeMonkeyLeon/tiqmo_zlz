package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;

public class HelpContract {
    public interface HelpListView extends BaseView<HelpContract.HelpListPresenter> {
        void getHelpContentListSuccess(HelpListEntity helpListEntity);
        void submitHelpQuestionSuccess(Void result);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface HelpListPresenter extends BasePresenter<HelpContract.HelpListView> {
        void getHelpContentList(String content);
        void submitHelpQuestion(String id,String comment,String thumb);
    }
}
