package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class MyTextView extends TextView {
    private int fontStyle = 1;
    private String fontPath = "fonts/SFProDisplay-Regular.ttf";

    private Context mContext;

    public MyTextView(Context context) {
        super(context);
        init(context, null);
    }


    public void setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
        AssetManager mgr = mContext.getAssets();
        if (LocaleUtils.isRTL(mContext)) {
            switch (fontStyle) {
                case 1:
                    fontPath = "fonts/Alilato-Regular.ttf";
                    break;
                case 2:
                    fontPath = "fonts/Alilato-Semi-Bold.ttf";
                    break;
                case 3:
                    fontPath = "fonts/Alilato-Bold.ttf";
                    break;
                case 4:
                    fontPath = "fonts/Alilato-Light.ttf";
                    break;
            }
            Typeface tf = Typeface.createFromAsset(mgr, fontPath);
            setTypeface(tf);
            setLineSpacing(AndroidUtils.dip2px(mContext, 4), 1);
        } else {
            switch (fontStyle) {
                case 1:
                    fontPath = "fonts/SFProDisplay-Regular.ttf";
                    break;
                case 2:
                    fontPath = "fonts/SFProDisplay-Semibold.ttf";
                    break;
                case 3:
                    fontPath = "fonts/SFProDisplay-Bold.ttf";
                    break;
                case 4:
                    fontPath = "fonts/SFProDisplay-Light.ttf";
                    break;
            }
            Typeface tf = Typeface.createFromAsset(mgr, fontPath);
            setTypeface(tf);
        }
    }


    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        try {
            if (attrs != null) {
                TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyTextView, 0, 0);
                fontStyle = ta.getInteger(R.styleable.MyTextView_fontType, 1);
            }
            setFontStyle(fontStyle);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }
}
