package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Typeface;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class BudgetListAdapter extends BaseRecyclerAdapter<SpendingBudgetEntity> {

    public BudgetListAdapter(@Nullable List<SpendingBudgetEntity> data) {
        super(R.layout.item_budget_category_list, data);
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SpendingBudgetEntity spendingBudgetEntity, int position) {
        try {
            int categoryCode = spendingBudgetEntity.categoryCode;
            //分类名称
            String categoryName = spendingBudgetEntity.categoryName;
            //预算金额 单位：最小币种单位
            String budgetLimitAmount = spendingBudgetEntity.budgetLimitAmount;
            //已花预算金额 单位：最小币种单位
            String alreadySpendingAmount = spendingBudgetEntity.alreadySpendingAmount;

            String spendingCategoryRatio = spendingBudgetEntity.spendingCategoryRatio;

            ProgressBar progressBudget = baseViewHolder.getView(R.id.progress_budget);

            baseViewHolder.setBackgroundRes(R.id.con_content,ThemeSourceUtils.getSourceID(mContext,R.attr.bg_030b28_white));
            baseViewHolder.setText(R.id.tv_category_name, categoryName);
            baseViewHolder.setTextColor(R.id.tv_category_name,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_3a3b44)));
            baseViewHolder.setTextColor(R.id.tv_category_money,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_3a3b44)));
            baseViewHolder.setTextColor(R.id.tv_category_rate,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_3a3b44)));
            baseViewHolder.setTextColor(R.id.tv_category_second_money,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_50white_503a3b44)));
            baseViewHolder.setText(R.id.tv_category_money,
                    Spans.builder()
                            .text(AndroidUtils.getTransferStringMoney(alreadySpendingAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                            .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf")).size(15)
                            .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                            .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf")).size(10)
                            .build());
            baseViewHolder.setText(R.id.tv_category_second_money, mContext.getString(R.string.analytics_21) + " " + AndroidUtils.getTransferIntNumber(budgetLimitAmount) + " " + LocaleUtils.getCurrencyCode(""));
            baseViewHolder.setImageResource(R.id.iv_budget_category, AndroidUtils.getBudgetCategoryDrawable(mContext, categoryCode));
            int progress = Integer.parseInt(spendingCategoryRatio);
            if (progress < 100) {
                progressBudget.setProgressDrawable(mContext.getDrawable(R.drawable.progressbar_budget_1cfcff));
            } else {
                progressBudget.setProgressDrawable(mContext.getDrawable(R.drawable.progressbar_budget_fd1d2d));
            }
            progressBudget.setProgress(progress);

            baseViewHolder.setText(R.id.tv_category_rate, progress + "%");
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
