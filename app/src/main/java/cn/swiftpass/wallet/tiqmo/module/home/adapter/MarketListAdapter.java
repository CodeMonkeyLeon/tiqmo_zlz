package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity.MarketInfo;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class MarketListAdapter extends BaseRecyclerAdapter<MarketInfo> {

    public MarketListAdapter(@Nullable List<MarketInfo> data) {
        super(R.layout.all_market, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, MarketInfo item, int position) {
        if (item == null){
            return;
        }
        RoundedImageView market = baseViewHolder.getView(R.id.iv_market_show);

        if (!TextUtils.isEmpty(item.activityPicUrl)) {
            String picUrl = item.activityPicUrl;
            Glide.with(mContext).clear(market);
            Glide.with(mContext)
                    .load(picUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(market);
        }
    }

    @Override
    public OnItemClickListener getOnItemClickListener() {
        return super.getOnItemClickListener();
    }
}
