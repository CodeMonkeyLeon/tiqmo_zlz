package cn.swiftpass.wallet.tiqmo.module.imr.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.HorizontalSlidingItemView;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class ImrBeneficiaryItemAdapter extends BaseRecyclerAdapter<ImrBeneficiaryListEntity.ImrBeneficiaryBean> {

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ImrBeneficiaryItemAdapter(@Nullable List<ImrBeneficiaryListEntity.ImrBeneficiaryBean> data) {
        super(R.layout.item_imr_beneficiary_item, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, ImrBeneficiaryListEntity.ImrBeneficiaryBean item, int position) {
        if (item == null) {
            return;
        }
        ImageView ivGenderAvatar = baseViewHolder.getView(R.id.iv_gender_avatar);
        ImageView ivBeneficiaryFlag = baseViewHolder.getView(R.id.iv_beneficiary_flag);
        ImageView ivImrCountry = baseViewHolder.getView(R.id.iv_imr_country);
        TextView tvBeneficiaryCountry = baseViewHolder.getView(R.id.tv_beneficiary_country);
        TextView tvBeneficiaryName = baseViewHolder.getView(R.id.tv_beneficiary_name);
        TextView tvImrCountry = baseViewHolder.getView(R.id.tv_imr_country);
        String activeState = item.activeState;
        if(!TextUtils.isEmpty(item.countrySupportedCurrencyName)) {
            tvImrCountry.setText(item.countrySupportedCurrencyName);
            ivImrCountry.setVisibility(View.VISIBLE);
            tvImrCountry.setVisibility(View.VISIBLE);
        }else{
            ivImrCountry.setVisibility(View.GONE);
            tvImrCountry.setVisibility(View.GONE);
        }
//        TextView tvBeneficiaryCurrency = baseViewHolder.getView(R.id.tv_beneficiary_currency);
        HorizontalSlidingItemView beneficiaryItem = baseViewHolder.getView(R.id.hv_imr_beneficiary_item);
        if("1".equals(activeState)){
            beneficiaryItem.setIvBeneficiarySend(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_imr_send_money));
            beneficiaryItem.setTvBeneficiarySend(ThemeSourceUtils.getSourceID(mContext,R.attr.color_80white_090a15),mContext.getString(R.string.IMR_27_3));
        }else{
            beneficiaryItem.setIvBeneficiarySend(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_ivr_bene_call));
            beneficiaryItem.setTvBeneficiarySend(R.color.color_fc4f00,mContext.getString(R.string.sprint11_88));
        }
        beneficiaryItem.setOnClickListener(new HorizontalSlidingItemView.OnClickListener() {
            @Override
            public void onDeleteClick() {
                if (listener != null) {
                    listener.onItemDeleteClick(position);
                }
            }

            @Override
            public void onEditClick() {
                if (listener != null) {
                    listener.onItemEditClick(position);
                }
            }

            @Override
            public void onSendClick() {
                if (listener != null) {
                    listener.onItemSendClick(position);
                }
            }

            @Override
            public void onDeleteShow(boolean isShow) {
                if (listener != null) {
                    listener.onItemDeleteShow(position, isShow);
                }
            }

            @Override
            public void onBenOPShow(boolean isShow) {
                if (listener != null) {
                    listener.onItemBenOPShow(position, isShow);
                }
            }
        });
//        if (!TextUtils.isEmpty(item.countrySupportedCurrencyName)) {
//            tvBeneficiaryCurrency.setText(item.countrySupportedCurrencyName);
//        }
        if (!TextUtils.isEmpty(item.transferDestinationCountryName)) {
            tvBeneficiaryCountry.setText(item.transferDestinationCountryName);
        }
        if (!TextUtils.isEmpty(item.payeeFullName)) {
            tvBeneficiaryName.setText(item.payeeFullName);
        }
        ivGenderAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, item.sex));
        if (!TextUtils.isEmpty(item.countryLogoUrl)) {
            Glide.with(mContext).clear(ivBeneficiaryFlag);
            Glide.with(mContext)
                    .load(item.countryLogoUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivBeneficiaryFlag);
        }
        if (!item.isShowRemoveIcon()) {
            beneficiaryItem.resetRemoveIcon();
        } else {
            beneficiaryItem.showRemoveIcon();
        }
        beneficiaryItem.showBeneficiaryOp(item.isShowBeneficiaryOP);
    }

    interface OnItemClickListener {
        void onItemDeleteClick(int position);
        void onItemEditClick(int position);
        void onItemSendClick(int position);
        void onItemDeleteShow(int position, boolean isShow);
        void onItemBenOPShow(int position, boolean isShow);
    }
}

