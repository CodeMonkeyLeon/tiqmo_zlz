package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MultipleItemAdapter;

public class MessageAdapter extends MultipleItemAdapter<MessageEntity> {
    public MessageLongPressListener messageLongPressListener;
    public static final int LeftSimpleMessage = 1;
    public static final int RightSimpleMessage = 2;
    public static final int LeftSingleImage = 3;
    public static final int RightSingleImage = 4;
    public static final int LeftFileImage = 5;
    public static final int RightFileImage = 6;
    public static final int DateFloater = 7;
    public static final int LeftTransferMessage = 8;
    public static final int RightTransferMessage = 9;

    public MessageAdapter(@Nullable List<MessageEntity> data) {
        super(data);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder baseViewHolder, final int position) {
        super.onBindViewHolder(baseViewHolder, position);
        bindViewClickListener(baseViewHolder);
    }

    @Override
    protected int getViewType(MessageEntity messageEntity) {
        int type = 0;
        if(messageEntity.getMessageType() != null) {
            switch (messageEntity.getMessageType()) {
                case LeftSimpleMessage: {
                    type = LeftSimpleMessage;
                    break;
                }
                case RightSimpleMessage: {
                    type = RightSimpleMessage;
                    break;
                }
                case LeftSingleImage: {
                    type = LeftSingleImage;
                    break;
                }
                case RightSingleImage: {
                    type = RightSingleImage;
                    break;
                }
                case LeftFileImage: {
                    type = LeftFileImage;
                    break;
                }
                case RightFileImage: {
                    type = RightFileImage;
                    break;
                }
                case DateFloater: {
                    type = DateFloater;
                    break;
                }
                case LeftTransferMessage: {
                    type = LeftTransferMessage;
                    break;
                }
                case RightTransferMessage: {
                    type = RightTransferMessage;
                    break;
                }
            }
        }else {
            type = LeftSimpleMessage;
        }
        if (type == 0) {
            throw new RuntimeException("Set Message Type ( Message Type is Null )");
        } else {
            return type;
        }
    }

    @Override
    public void addItemDataBinder() {
        addDataBinder(new LeftSimpleMessageBinder());
        addDataBinder(new RightSimpleMessageBinder());
        addDataBinder(new LeftSingleImageBinder());
        addDataBinder(new RightSingleImageBinder());
        addDataBinder(new LeftFileImageBinder());
        addDataBinder(new RightFileImageBinder());
        addDataBinder(new DateFloaterBinder());
        addDataBinder(new RightTransferMessageBinder());
        addDataBinder(new LeftTransferMessageBinder());
    }
}
