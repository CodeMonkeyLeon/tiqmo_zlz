package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatUser;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.UserUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class GroupChatMemberAdapter extends BaseRecyclerAdapter<GcChatParticipant> {

    private GcChatUser createUser;

    public void setCreateUser(final GcChatUser createUser) {
        this.createUser = createUser;
    }

    public GroupChatMemberAdapter(@Nullable List<GcChatParticipant> data) {
        super(R.layout.item_group_member,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, GcChatParticipant gcChatParticipant, int position) {
        baseViewHolder.setVisible(R.id.iv_delete_member,true);

        RoundedImageView ivUser = baseViewHolder.getView(R.id.iv_user);
        if(gcChatParticipant != null && gcChatParticipant.getUser() != null) {
            baseViewHolder.setText(R.id.tv_name, gcChatParticipant.getUser().getName());
            if(createUser != null){
                String id = createUser.getExternalId();
                if(!TextUtils.isEmpty(id)){
                    if(id.equals(gcChatParticipant.getUser().getExternalId())){
                        baseViewHolder.setVisible(R.id.iv_delete_member,false);
                        baseViewHolder.setText(R.id.tv_name, gcChatParticipant.getUser().getName()+" ("+mContext.getString(R.string.sprint20_57)+")");
                    }

                    if(!id.equals(UserUtils.loggedInUserId)){
                        baseViewHolder.setVisible(R.id.iv_delete_member,false);
                    }
                }
            }

            baseViewHolder.setText(R.id.tv_phone, AndroidUtils.getPhone(gcChatParticipant.getUser().getPhoneNumber()));
            String displayPic = gcChatParticipant.getUser().getFileId();
            if (displayPic != null && !displayPic.isEmpty()) {
                Glide.with(mContext).load(ChatHelper.getInstance().getChatUserAvatar(displayPic)).centerCrop().autoClone().into(ivUser);
            } else {
                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivUser);
            }
        }

        baseViewHolder.addOnClickListener(R.id.iv_delete_member);
    }
}
