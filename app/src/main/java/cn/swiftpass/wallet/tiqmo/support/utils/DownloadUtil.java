package cn.swiftpass.wallet.tiqmo.support.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.core.app.NotificationCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.fcm.MyFirebaseMessagingService;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DownloadUtil{
    private static int NOTIFICATION_ID = 100;
    private final String CHANNEL_ID = ProjectApp.getContext().getPackageName();
    private final String CHANNEL_NAME = ProjectApp.getContext().getPackageName() + "_Notification";

    private static DownloadUtil downloadUtil;
    private OkHttpClient okHttpClient;

    public static DownloadUtil get() {
        if (downloadUtil == null) {
            downloadUtil = new DownloadUtil();
        }
        return downloadUtil;
    }

    private DownloadUtil() {
        try {
            okHttpClient = getBuilder(true)
                    .connectTimeout(90, TimeUnit.SECONDS)
                    .readTimeout(90, TimeUnit.SECONDS)
                    .writeTimeout(90, TimeUnit.SECONDS)
                    .build();
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");

        }
    }

    /**
     * 添加自定义拦截器  拦截器处理request顺序按依赖关系排序
     *
     * @return
     */
    public OkHttpClient.Builder getBuilder(boolean isAddInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        try {
            if (getInputStream() == null) {
                if (BuildConfig.DOWNLOAD_CER_SERVER) {
//                    builder = setCertificates(builder, ProjectApp.getContext().getAssets().open("tiqmo_sit.cer"));
                    builder = ApiHelper.setCertificates(builder, ProjectApp.getContext().getAssets().open("tiqmo_k8s.cer"));
                }
            } else {
                builder = ApiHelper.setCertificates(builder, getInputStream());
            }
        } catch (Exception e) {
        }
        return builder;
    }

    public InputStream getInputStream() {
        String filePath = ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File file = new File(filePath);
        File[] filenameList = file.listFiles();
        InputStream inputStream = null;
        for (int i = 0; i < filenameList.length; i++) {
            String filename = filenameList[i].getName();
            if (!filename.endsWith(".crt") && !filename.endsWith(".cer")) {
                continue;
            }
            try {
                inputStream = new FileInputStream(filenameList[i]);
            } catch (FileNotFoundException e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        return inputStream;
    }

    /**
     * @param url          下载连接
     * @param destFileDir  下载的文件储存目录
     * @param destFileName 下载文件名称
     * @param listener     下载监听
     */
    public void download(final String url, final String destFileDir, final String destFileName, final OnDownloadListener listener) {
        Request request = new Request.Builder().url(url).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 下载失败监听回调
                listener.onDownloadFailed(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                // 储存下载文件的目录
                File dir = new File(destFileDir);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File file = new File(dir, destFileName);
                try {
                    is = response.body().byteStream();
                    long total = response.body().contentLength();
                    fos = new FileOutputStream(file);
                    long sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;
                        int progress = (int) (sum * 1.0f / total * 100);
                        // 下载中更新进度条
                        listener.onDownloading(progress);
                    }
                    fos.flush();
                    // 下载完成
                    CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
                    cardNotifyEntity.pdfFilePath = file.getAbsolutePath();
                    cardNotifyEntity.pdfFileName = destFileName;
                    sendNotifacation(ProjectApp.getContext(),cardNotifyEntity,-10);
                    listener.onDownloadSuccess(file);
                } catch (Exception e) {
                    listener.onDownloadFailed(e);
                } finally {
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                    }
                }
            }
        });
    }

    private void sendNotifacation(Context mContext,CardNotifyEntity cardNotifyEntity, int msgType) {
        Intent intent = new Intent(mContext, MyFirebaseMessagingService.NotifyClickReceiver.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.MSGTYPE, msgType);
        intent.putExtra(Constants.NOTIFY_ENTITY, cardNotifyEntity);
        PendingIntent pendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getBroadcast(mContext, new SecureRandom().nextInt(100), intent, PendingIntent.FLAG_IMMUTABLE);
        } else {
            pendingIntent = PendingIntent.getBroadcast(mContext, new SecureRandom().nextInt(100), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        Bitmap iconBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.tiqmo_logo);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        mBuilder.setSmallIcon(R.mipmap.tiqmo_logo_small).
                setContentTitle(cardNotifyEntity.pdfFileName.contains("receipt")?mContext.getString(R.string.sprint11_70):mContext.getString(R.string.DigitalCard_92)).
                setContentText(cardNotifyEntity.pdfFileName).
                setAutoCancel(true).
                setLargeIcon(iconBitmap).
                setSound(defaultSoundUri).
                setContentIntent(pendingIntent);
        //创建大文本样式
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText("pdf");
        mBuilder.setStyle(bigTextStyle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            mBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(NOTIFICATION_ID++, mBuilder.build());
    }

    public interface OnDownloadListener {
        /**
         * @param file 下载成功后的文件
         */
        void onDownloadSuccess(File file);

        /**
         * @param progress 下载进度
         */
        void onDownloading(int progress);

        /**
         * @param e 下载异常信息
         */
        void onDownloadFailed(Exception e);
    }
}