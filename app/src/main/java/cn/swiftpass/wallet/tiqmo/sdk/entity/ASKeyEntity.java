package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by YZX on 2018年12月04日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class ASKeyEntity {
    private String askey;

    public String getAskey() {
        return askey;
    }

    public void setAskey(String askey) {
        this.askey = askey;
    }
}
