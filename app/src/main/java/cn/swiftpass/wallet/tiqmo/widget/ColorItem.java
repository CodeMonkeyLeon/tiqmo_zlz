package cn.swiftpass.wallet.tiqmo.widget;

import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;

public class ColorItem {
    //1被选中 0未被选中
    public int isSelect = 0;

    public String spendingCategoryId;

    /**
     * 所占值
     */
    private int value;
    /**
     * 颜色
     */
    private int color;

    private Region region;

    public void setRegion(Path path) {
        Region re = new Region();
        RectF rectF = new RectF();
        path.computeBounds(rectF, true);
        re.setPath(path, new Region((int) (rectF.left), (int) (rectF.top), (int) (rectF.right), (int) (rectF.bottom)));
        this.region = re;
    }
    public boolean isInRegion(float x, float y) {
        return region != null && region.contains((int)x, (int)y);
    }

    public ColorItem(int value, int color,String spendingCategoryId) {
        this.value = value;
        this.color = color;
        this.spendingCategoryId = spendingCategoryId;
    }

    public ColorItem(int value, int color,String spendingCategoryId,int isSelect) {
        this.value = value;
        this.color = color;
        this.spendingCategoryId = spendingCategoryId;
        this.isSelect = isSelect;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
