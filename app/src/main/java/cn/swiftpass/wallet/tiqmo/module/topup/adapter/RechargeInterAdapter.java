package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInterOrderEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class RechargeInterAdapter extends BaseRecyclerAdapter<RechargeInterOrderEntity> {
    public RechargeInterAdapter(@Nullable List<RechargeInterOrderEntity> data) {
        super(R.layout.item_recharge_inter,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, RechargeInterOrderEntity rechargeInterOrderEntity, int position) {
        RoundedImageView ivOperator = baseViewHolder.getView(R.id.iv_operator);
        ImageView ivCountry = baseViewHolder.getView(R.id.iv_country);
        if (rechargeInterOrderEntity != null) {
            String operatorImgUrl = rechargeInterOrderEntity.operatorImgUrl;
            String countryImgUrl = rechargeInterOrderEntity.countryImgUrl;
            Glide.with(mContext)
                    .load(countryImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivCountry);
            Glide.with(mContext)
                    .load(operatorImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.mobile_topup))
                    .into(ivOperator);

            baseViewHolder.setText(R.id.tv_user_name, rechargeInterOrderEntity.userShortName);
            baseViewHolder.setText(R.id.tv_operator_name, rechargeInterOrderEntity.operatorName);
            baseViewHolder.setText(R.id.tv_country_name, rechargeInterOrderEntity.countryName);
            baseViewHolder.addOnClickListener(R.id.tv_pay);
        }
    }
}
