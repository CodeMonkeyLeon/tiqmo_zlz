package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.OpenKsaCardPresenter;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.KsaCardTermsDialog;

public class GetNewCardFourActivity extends BaseCompatActivity<CardKsaContract.OpenCardPresenter> implements CardKsaContract.OpenCardView {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_introduce_image)
    ImageView ivIntroduceImage;
    @BindView(R.id.et_card_name)
    EditTextWithDel etCardName;
    @BindView(R.id.cb_digital)
    CheckBox cbDigital;
    @BindView(R.id.tv_digital_price)
    TextView tvDigitalPrice;
    @BindView(R.id.ll_digital_card)
    LinearLayout llDigitalCard;
    @BindView(R.id.cb_physical)
    CheckBox cbPhysical;
    @BindView(R.id.tv_physical_money)
    TextView tvPhysicalMoney;
    @BindView(R.id.ll_physical_card)
    LinearLayout llPhysicalCard;
    @BindView(R.id.ll_check)
    LinearLayout llCheck;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    private OpenCardReqEntity openCardReqEntity;

    private String cardType = Constants.ksa_card_VIRTUAL;

    private KsaCardSummaryEntity ksaCardSummaryEntity;
    private String physicalCardAmount,virtualCardAmount;
    private KsaCardTermsDialog ksaCardTermsDialog;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_new_card_four;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.DigitalCard_0);
        etCardName.getTlEdit().setHint(getString(R.string.DigitalCard_24));
        setDarkBar();
        fixedSoftKeyboardSliding();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        if(getIntent() != null && getIntent().getExtras()!=null){
            ksaCardSummaryEntity = (KsaCardSummaryEntity) getIntent().getExtras().getSerializable(Constants.ksaCardSummaryEntity);
            if(ksaCardSummaryEntity != null){
                physicalCardAmount = ksaCardSummaryEntity.physicalCardAmount;
                virtualCardAmount = ksaCardSummaryEntity.virtualCardAmount;
//                virtualCardAmount = "0";
//                physicalCardAmount = "0";
                if(!TextUtils.isEmpty(virtualCardAmount) && BigDecimalFormatUtils.compareBig(virtualCardAmount,"0")){
                    tvDigitalPrice.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(virtualCardAmount)+" ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                            .build());
                    openCardReqEntity.isVirtualFree = false;
                    tvConfirm.setText(getString(R.string.scan_pay_7) + " " + AndroidUtils.getTransferMoney(virtualCardAmount) + " " + LocaleUtils.getCurrencyCode(""));
                }else{
                    tvDigitalPrice.setText(getString(R.string.DigitalCard_26));
                    openCardReqEntity.isVirtualFree = true;
                    tvConfirm.setText(R.string.kyc_5);
                }
                if(!TextUtils.isEmpty(physicalCardAmount) && BigDecimalFormatUtils.compareBig(physicalCardAmount,"0")) {
                    tvPhysicalMoney.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(physicalCardAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                            .build());
                    openCardReqEntity.isPhysicalFree = false;
                }else{
                    tvPhysicalMoney.setText(getString(R.string.DigitalCard_26));
                    openCardReqEntity.isPhysicalFree = true;
                }
            }
        }

        ivIntroduceImage.setImageResource(AndroidUtils.getCardFaceDrawable(openCardReqEntity.cardFaceId,true));

        etCardName.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(26), new NormalInputFilter(NormalInputFilter.AR_ENGLISH_SPACE)});

        cbDigital.setEnabled(false);
        cbPhysical.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    cardType = Constants.ksa_card_PHYSICAL;
                    if(openCardReqEntity.isPhysicalFree){
                        tvConfirm.setText(R.string.kyc_5);
                    }else {
                        tvConfirm.setText(getString(R.string.scan_pay_7) + " " + AndroidUtils.getTransferMoney(physicalCardAmount) + " " + LocaleUtils.getCurrencyCode(""));
                    }
                }else{
                    cardType = Constants.ksa_card_VIRTUAL;
                    if(openCardReqEntity.isVirtualFree){
                        tvConfirm.setText(R.string.kyc_5);
                    }else {
                        tvConfirm.setText(getString(R.string.scan_pay_7) + " " + AndroidUtils.getTransferMoney(virtualCardAmount) + " " + LocaleUtils.getCurrencyCode(""));
                    }
                }
            }
        });

        etCardName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                openCardReqEntity.cardOfName = s.toString();
                checkButtonStatus();
            }
        });
    }

    private void checkButtonStatus(){
        tvConfirm.setEnabled(!TextUtils.isEmpty(openCardReqEntity.cardOfName.trim()) && openCardReqEntity.cardOfName.length()>1);
    }

    @OnClick({R.id.iv_back, R.id.tv_confirm})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                showCardTermsDialog();
                break;
            default:break;
        }
    }

    private void showCardTermsDialog(){
        ksaCardTermsDialog = new KsaCardTermsDialog(mContext);
        ksaCardTermsDialog.setApproveListener(new KsaCardTermsDialog.ApproveListener() {
            @Override
            public void clickApprove() {
                mPresenter.openKsaCardAccount(openCardReqEntity.cardProductType);
            }
        });
        ksaCardTermsDialog.showWithBottomAnim();
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void openKsaCardAccountSuccess(Void result) {
        if(ksaCardTermsDialog != null){
            ksaCardTermsDialog.dismiss();
        }
        openCardReqEntity.cardType = cardType;
        UserInfoManager.getInstance().setOpenCardReqEntity(openCardReqEntity);
        if(Constants.ksa_card_VIRTUAL.equals(cardType)){
            ActivitySkipUtil.startAnotherActivity(GetNewCardFourActivity.this, GetCardPwdOneActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(GetNewCardFourActivity.this, GetNewCardFiveActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getKsaCardSummarySuccess(KsaCardSummaryEntity ksaCardSummaryEntity) {
    }

    @Override
    public CardKsaContract.OpenCardPresenter getPresenter() {
        return new OpenKsaCardPresenter();
    }
}
