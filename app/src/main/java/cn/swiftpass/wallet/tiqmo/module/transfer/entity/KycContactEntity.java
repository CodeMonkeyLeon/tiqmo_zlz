package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import android.text.TextUtils;

import com.github.promeg.pinyinhelper.Pinyin;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class KycContactEntity extends BaseEntity {
    private static final String ATOZSTR = "[A-Z]";
    private static final String SPECIAL_CHAR_ONE = "#";
    private static final String SPECIAL_CHAR_TWO = "@";

    public String getSortLetter() {
        return sortLetter;
    }

    public void setSortLetter(String sortLetter) {
        this.sortLetter = sortLetter;
    }

    public void setSortLetterName(String userName) {
        // 汉字转换成拼音
        if (!TextUtils.isEmpty(userName)) {
//            String pinyin = converterToFirstSpell(userName);
            String pinyin = Pinyin.toPinyin(userName, "");
//            LogUtils.i(TAG, "userName：--->" + userName + " pinyin:" + pinyin);
            if (!TextUtils.isEmpty(pinyin)) {
                String sortString = pinyin.substring(0, 1).toUpperCase();
                // 正则表达式，判断首字母是否是英文字母
                if (sortString.matches(ATOZSTR)) {
                    this.sortLetter = sortString.toUpperCase();
                } else {
                    this.sortLetter = SPECIAL_CHAR_ONE;
                }
            } else {
                this.sortLetter = SPECIAL_CHAR_ONE;
            }
        } else {
            this.sortLetter = SPECIAL_CHAR_ONE;
        }
    }

    /**
     * 模糊搜索 去除空格
     */
    public String getPhoneNoMapping() {
        if (TextUtils.isEmpty(phone)) {
            return "";
        }
        return phone.replace(" ", "").replace("-", "");
    }

    public String getNameNoMapping() {
        if (TextUtils.isEmpty(contactsName)) {
            return "";
        }
        return contactsName.replace(" ", "");
    }

    private String sortLetter;

    private String phone;
    private String phoneNumber;
    private String requestPhoneNumber;
    public int headResId;
    public boolean isChoose;

    private String callingCode;
    private String contactsName;
    private String headIcon;
    private String userId;
    private String sex;
    public String flag;

    public String remark;
    public String payMoney;

    public String splitMoney;
    public boolean isChecked;

    public String purposeCode;

    public String company;
    // -- 未注册 - unregistered  已注册-registered
    private String userStatus = "";
    private String inviteStatus;

    public String getUserStatus() {
        return this.userStatus;
    }

    public boolean isRegistered(){
        return "registered".equals(userStatus);
    }

    public void setUserStatus(final String userStatus) {
        this.userStatus = userStatus;
    }

    public String getInviteStatus() {
        return this.inviteStatus;
    }

    public void setInviteStatus(final String inviteStatus) {
        this.inviteStatus = inviteStatus;
    }

    public String getRequestPhoneNumber() {
        if (!TextUtils.isEmpty(phone) && phone.startsWith("+966") && phone.length() > 5) {
            return phone.substring(5);
        }
        return phone;
    }

    public void setRequestPhoneNumber(String requestPhoneNumber) {
        this.requestPhoneNumber = requestPhoneNumber;
    }

    public int getHeadResId() {
        return headResId;
    }

    public void setHeadResId(int headResId) {
        this.headResId = headResId;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }

    public String getCallingCode() {
        if (TextUtils.isEmpty(callingCode)) {
            return SpUtils.getInstance().getCallingCode();
        }
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getContactsName() {
//        if(!TextUtils.isEmpty(company)){
//            contactsName = contactsName+" "+company;
//        }
        return contactsName;
    }

    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    public String getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(String headIcon) {
        this.headIcon = headIcon;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public KycContactEntity() {
    }

    public KycContactEntity(String name, int headResId) {
        this.contactsName = name;
        this.headResId = headResId;
    }

    public KycContactEntity(String purposeCode, String name, int headResId) {
        this.purposeCode = purposeCode;
        this.contactsName = name;
        this.headResId = headResId;
    }

    public String getPhone() {
        if (!TextUtils.isEmpty(phone) && !phone.startsWith("+966")) {
            return "+966 " + phone;
        }
        return phone;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
