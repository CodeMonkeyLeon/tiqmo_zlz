package cn.swiftpass.wallet.tiqmo.module.home.interfaces

import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryListParamEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity

interface HistoryFragmentImp {


    fun isClick(isClick: Boolean)

    fun showFilterHistoryDialog(listFilter: FilterListEntity)

    fun getFilterListSuccess(result: FilterListEntity?)

    fun getFilterListFailed(
        errorCode: String?,
        errorMsg: String?
    )


    fun updateGetListStatus(): HistoryListParamEntity?


    fun getHistoryListSuccess(transferHistoryMainEntity: TransferHistoryMainEntity?)

    fun getHistoryListFail(
        errorCode: String?,
        errorMsg: String?
    )


    fun getHistoryDetailSuccess(
        result: TransferHistoryDetailEntity?
    )

    fun getHistoryDetailFail(
        errorCode: String?,
        errorMsg: String?
    )


    fun deleteTransferHistorySuccess(
        result: Void
    )

    fun deleteTransferHistoryFail(
        errorCode: String?,
        errorMsg: String?
    )


    fun hideProcess()
}