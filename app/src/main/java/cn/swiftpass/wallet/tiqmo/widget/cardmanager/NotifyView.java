package cn.swiftpass.wallet.tiqmo.widget.cardmanager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.LinkedList;

import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class NotifyView<T> extends FrameLayout {

    /**
     * 显示可见的卡片数量
     */
    public static final int DEFAULT_SHOW_ITEM = 4;
    /**
     * 实际有数据的view的数量
     */
    public static final int DEFAULT_DATA_ITEM = 2;
    /**
     * 默认缩放的比例
     */
    public static final float DEFAULT_SCALE = 0.08f;
    /**
     * 叠加卡片Y轴偏移量
     */
    public static final int DEFAULT_TRANSLATE_Y = 50;
    /**
     * 卡片Y轴空余高度
     */
    public static final int SPARE_TRANSLATE_Y = 40;
    /**
     * 卡片Y轴最大移动高度
     */
    public static final int MAX_TRANSLATE_Y = 410;
    /**
     * 卡片移动动画最大时间
     */
    public static final int MAX_ANIMATION_DURATION = 600;
    /**
     * 卡片移动是否翻页判断
     */
    public static final int TRANSLATE_Y_CRITICAL = 40;

    private final String TAG = "NotifyView";
    private float oldY; // 计算Y轴偏移量时按下的初始位置
    private float oldX; // 计算Y轴偏移量时按下的初始位置
    private float slidingDistance = 0f; // Y轴偏移量
    private boolean isTouchable = true; // 在上滑的动画执行时，不可再次滑动下一个view
    private boolean isChildClickable = true; // 在上滑的动画执行时，不可再次滑动下一个view
    private int currentPosition = 0; // 在上滑的动画执行时，不可再次滑动下一个view
    private boolean isSliding = false; // 是否随手势滑动

    private Context context;
//    private ArrayList<View> childViews; //需要显示的view
    private LinkedList<T> data = new LinkedList<>(); //需要显示的数据
    private T cacheData; // 删除数据时的缓存
    private int layoutRes; // 填充的layout resource
    private ViewPropertyAnimator animate;
    private BindView<T> bindView;
    private FontItemMoveListener onFontItemMoveListener;

    public void setOnFontItemMoveListener(FontItemMoveListener onFontItemMoveListener) {
        this.onFontItemMoveListener = onFontItemMoveListener;
    }

    public boolean isChildClickable() {
        return isChildClickable;
    }

    public NotifyView(@NonNull Context context) {
        this(context, null);
    }

    public NotifyView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotifyView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    public void initChildViews(LinkedList<T> data, @NonNull int layoutRes, BindView<T> bindView) {
        this.data.clear();
        this.data.addAll(data);
        this.bindView = bindView;
        this.layoutRes = layoutRes;
//        Collections.reverse(this.data);
        initView();
    }

    private void initView() {
        int startIndex = data.size() > DEFAULT_SHOW_ITEM ? DEFAULT_SHOW_ITEM - 1 : data.size() - 1;
        for (int i = startIndex; i >= 0; i--) {
            View view = LayoutInflater.from(context).inflate(layoutRes, null);
            if (bindView != null && i <= DEFAULT_DATA_ITEM) {
                bindView.bindView(data.get(i), view);
            }
            if (view != null) {
                view.setTranslationY(Math.min(i, startIndex) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y);
                view.setScaleX(1 - Math.min(i, startIndex) * DEFAULT_SCALE);
                view.setScaleY(1 - Math.min(i, startIndex) * DEFAULT_SCALE);
                addView(view);
            }
        }


        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i) != null) {
                getChildAt(i).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e(TAG, "childViews " + "i:  " + currentPosition);
                        if (onFontItemMoveListener != null) {
                            onFontItemMoveListener.onItemClick(v);
                        }
                    }
                });
            }
        }
    }

    // 获取显示在最前的view
    private View getFrontView() {
        return getChildAt(getChildCount() - 1);
    }

    // 获取显示在最后的view
    private View getLastView() {
        return getChildAt(0);
    }

    // 获取需要刷新数据的view
    private View getChangeDataView() {
        return getChildAt(Math.min(data.size(), DEFAULT_SHOW_ITEM) - DEFAULT_DATA_ITEM);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!isTouchable || data == null || data.size() <= 1) {
            return false;
        }

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                oldY = ev.getY();
                oldX = ev.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                if (Math.abs(ev.getX() - oldX) > TRANSLATE_Y_CRITICAL || Math.abs(ev.getY() - oldY) > TRANSLATE_Y_CRITICAL) {
                    return true;
                }
            case MotionEvent.ACTION_UP:
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!isTouchable || data == null || data.size() <= 1) {
            return false;
        }
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.e(TAG, "onTouchEvent: " + "ACTION_DOWN " + "ev.getY(): " + ev.getY());
                oldY = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                if (oldY == 0) {
                    oldY = ev.getY();
                }
                slidingDistance = Math.abs(ev.getY() - oldY < 0 ? (ev.getY() - oldY >= -MAX_TRANSLATE_Y ? Math.round(ev.getY() - oldY) : -MAX_TRANSLATE_Y) : 0);
                if (isSliding) {
                    getFrontView().setTranslationY(SPARE_TRANSLATE_Y - slidingDistance);
                    getFrontView().setScaleX((float) (1f - (DEFAULT_SHOW_ITEM + 1) * DEFAULT_SCALE * (slidingDistance / MAX_TRANSLATE_Y)));
                    getFrontView().setScaleY((float) (1f - (DEFAULT_SHOW_ITEM + 1) * DEFAULT_SCALE * (slidingDistance / MAX_TRANSLATE_Y)));

                    int index = getChildCount() > DEFAULT_SHOW_ITEM ? getChildCount() - DEFAULT_SHOW_ITEM : 0;
                    for (int position = index; position < getChildCount() - 1; position++) {
                        View view = getChildAt(position);
                        if (view != null) {
                            view.setTranslationY(Math.round((getChildCount() - 1 - position) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y - DEFAULT_TRANSLATE_Y * (slidingDistance / MAX_TRANSLATE_Y)));
                            view.setScaleX((float) (1f - (getChildCount() - 1 - position) * DEFAULT_SCALE + DEFAULT_SCALE * slidingDistance / MAX_TRANSLATE_Y));
                            view.setScaleY((float) (1f - (getChildCount() - 1 - position) * DEFAULT_SCALE + DEFAULT_SCALE * slidingDistance / MAX_TRANSLATE_Y));
                        }
                    }
                }

                break;
            case MotionEvent.ACTION_UP:
                Log.e(TAG, "onTouchEvent: " + "ACTION_UP" + slidingDistance);
                isTouchable = false;
                isChildClickable = false;
                oldY = 0;
                oldX = 0;
                animate = getFrontView().animate();
                animate.setListener(null);
                int startIndex = getChildCount() > DEFAULT_SHOW_ITEM ? getChildCount() - DEFAULT_SHOW_ITEM : 0;
                if (slidingDistance > TRANSLATE_Y_CRITICAL) {
                    animate.translationY(SPARE_TRANSLATE_Y - MAX_TRANSLATE_Y)
                            .scaleX(1 - (DEFAULT_SHOW_ITEM + 1) * DEFAULT_SCALE)
                            .scaleY(1 - (DEFAULT_SHOW_ITEM + 1) * DEFAULT_SCALE)
                            .setDuration(isSliding ? (long) Math.round((MAX_ANIMATION_DURATION * (1.0 - slidingDistance / MAX_TRANSLATE_Y))) : MAX_ANIMATION_DURATION)
                            .setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    animate.setListener(null);

                                    moveFontToLast();
                                    addPosition();
                                    Log.e(TAG, "onTouchEvent: " + "currentPosition: " + currentPosition);
                                    if (onFontItemMoveListener != null) {
                                        onFontItemMoveListener.afterTurnPage(currentPosition);
                                    }

                                    animate.translationY(SPARE_TRANSLATE_Y + Math.min(getChildCount() - 1, DEFAULT_SHOW_ITEM - 1) * DEFAULT_TRANSLATE_Y)
                                            .setDuration(MAX_ANIMATION_DURATION - 200)
                                            .scaleX(1 - (Math.min(getChildCount() - 1, DEFAULT_SHOW_ITEM - 1)) * DEFAULT_SCALE)
                                            .scaleY(1 - (Math.min(getChildCount() - 1, DEFAULT_SHOW_ITEM - 1)) * DEFAULT_SCALE)
                                            .setListener(new Animator.AnimatorListener() {
                                                @Override
                                                public void onAnimationStart(Animator animation) {
                                                }

                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    animate.setListener(null);
                                                    Log.e(TAG, "onTouchEvent: " + "onAnimationEnd: 2");
                                                    if (data.size() <= DEFAULT_DATA_ITEM) {
                                                        isTouchable = true;
                                                        isChildClickable = true;
                                                        return;
                                                    }
                                                    try {
                                                        if (bindView != null) {
                                                            int add = (currentPosition + DEFAULT_DATA_ITEM - 1) % data.size();
                                                            T t = data.get(add);
                                                            View view = getChangeDataView();
                                                            bindView.bindView(t, view);
                                                        }
                                                    } catch (Exception e) {
                                                        LogUtils.d("errorMsg", "---"+e+"---");
                                                    } finally {
                                                        isTouchable = true;
                                                        isChildClickable = true;
                                                        animate = null;
                                                    }
                                                }

                                                @Override
                                                public void onAnimationCancel(Animator animation) {

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animator animation) {

                                                }
                                            })
                                            .start();


                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {
                                }
                            }).start();

                    for (int position = startIndex; position < getChildCount() - 1; position++) {
                        View view = getChildAt(position);
                        ViewPropertyAnimator animate1 = view.animate();
                        animate1.translationY((getChildCount() - 1 - position - 1) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y)
                                .scaleX(1f - (getChildCount() - 1 - position - 1) * DEFAULT_SCALE)
                                .scaleY(1f - (getChildCount() - 1 - position - 1) * DEFAULT_SCALE)
                                .setDuration(isSliding ? (long) Math.round((MAX_ANIMATION_DURATION * (1.1 - slidingDistance / MAX_TRANSLATE_Y))) : MAX_ANIMATION_DURATION)
                                .start();
                    }
                } else {
                    if (isSliding) {
                        animate.translationY(SPARE_TRANSLATE_Y).scaleX(1).scaleY(1)
                                .setDuration(MAX_ANIMATION_DURATION)
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        isTouchable = true;
                                        isChildClickable = true;
                                        animate.setListener(null);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                }).start();
                        for (int position = startIndex; position < getChildCount() - 1; position++) {
                            View view = getChildAt(position);
                            ViewPropertyAnimator animate1 = view.animate();
                            animate1.translationY((getChildCount() - 1 - position) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y)
                                    .scaleX(1f - (getChildCount() - 1 - position) * DEFAULT_SCALE)
                                    .scaleY(1f - (getChildCount() - 1 - position) * DEFAULT_SCALE)
                                    .setDuration((long) Math.round((MAX_ANIMATION_DURATION * (1.1 - slidingDistance / MAX_TRANSLATE_Y))))
                                    .start();
                        }
                    } else {
                        isTouchable = true;
                        isChildClickable = true;
                    }
                }

                break;
        }
        return super.onTouchEvent(ev);
    }

    // 点击reject后移除显示的第一个view
    public void removeFontItem() {
        isTouchable = false;
        isChildClickable = false;
        ViewPropertyAnimator animate = getFrontView().animate();
        animate.translationX(-AndroidUtils.getScreenWidth(context)).setDuration(250)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        animate.setListener(null);
                        moveFontToLast();
                        View view = getLastView();
                        if (view != null && data.size() > 3) {
                            view.setScaleX(1f - (getChildCount() - 1) * DEFAULT_SCALE);
                            view.setScaleY(1f - (getChildCount() - 1) * DEFAULT_SCALE);
                            view.setTranslationX(0);
                            view.setTranslationY((getChildCount() - 1) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y);
                        }
                        moveViewUp();

                        addPosition();
                        cacheData = data.get(currentPosition == 0 ? data.size() - 1 : currentPosition - 1);

                        if (data.size() > DEFAULT_DATA_ITEM) {
                            int add = (currentPosition + DEFAULT_DATA_ITEM - 1) % data.size();
                            T t = data.get(add);
                            bindView.bindView(t, getChangeDataView());
                        }

                        if (onFontItemMoveListener != null) {
                            onFontItemMoveListener.onFontItemRemove(currentPosition);
                        }
                        isChildClickable = true;
                    }
                }).start();
    }

    // 取消reject时，恢复缓存的第一个view
    public void backFontItem() {
        moveLastToFont();
        View view = getFrontView();
        if (view != null) {
            if (cacheData != null) {
                bindView.bindView(cacheData, view);
            }

            view.setScaleX(1);
            view.setScaleY(1);
            view.setTranslationX(-AndroidUtils.getScreenWidth(context));
            view.setTranslationY(SPARE_TRANSLATE_Y);
            ViewPropertyAnimator animate = view.animate();
            animate.scaleY(1).scaleX(1).translationY(SPARE_TRANSLATE_Y).translationX(0).setDuration(250)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            moveViewDown();
                            if (currentPosition >= 1) {
                                currentPosition = currentPosition - 1;
                            } else if (data.size() > 1) {
                                currentPosition = data.size() - 1;
                            }
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animate.setListener(null);
                            if (onFontItemMoveListener != null) {
                                onFontItemMoveListener.onFontItemBack(currentPosition);
                            }
                            isTouchable = true;
                        }
                    }).start();
        }

    }

    // 移动第一个view的时候其他view的动画
    public void moveViewUp() {
        for (int position = 1; position < getChildCount(); position++) {
            View view = getChildAt(position);
            ViewPropertyAnimator animate1 = view.animate();
            animate1.translationY((getChildCount() - 1 - position) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y)
                    .translationX(0)
                    .scaleX(1f - (getChildCount() - 1 - position) * DEFAULT_SCALE)
                    .scaleY(1f - (getChildCount() - 1 - position) * DEFAULT_SCALE)
                    .setDuration(250)
                    .start();
        }
    }

    // 移动第一个view的时候其他view的动画
    public void moveViewDown() {
        int startIndex = getChildCount() > DEFAULT_SHOW_ITEM ? getChildCount() - DEFAULT_SHOW_ITEM : 0;
        for (int position = startIndex; position < getChildCount() - 1; position++) {
            View view = getChildAt(position);
            ViewPropertyAnimator animate1 = view.animate();
            animate1.translationY((getChildCount() - 1 - position) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y)
                    .scaleX(1f - (getChildCount() - 1 - position) * DEFAULT_SCALE)
                    .scaleY(1f - (getChildCount() - 1 - position) * DEFAULT_SCALE)
                    .setDuration(250)
                    .start();
        }
    }

    // 移除第一个view切换到最后
    private void moveFontToLast() {
        View v = getFrontView();
        if (v != null) {
            removeView(v);
            addView(v, 0);
        }
    }

    // 移除最后一个view显示到最前
    private void moveLastToFont() {
        View v = getLastView();
        if (v != null) {
            removeView(v);
            addView(v);
        }
    }

    private void addPosition() {
        currentPosition++;
        currentPosition = currentPosition >= data.size() ? 0 : currentPosition;
    }

    private void minusPosition() {
        if (currentPosition >= 1) {
            currentPosition = currentPosition - 1;
        }
    }

    // 移除缓存的数据
    public int removeCache(boolean isTouchable) {
        if (cacheData != null) {
            data.remove(cacheData);
            cacheData = null;
            minusPosition();
            if (data.size() < DEFAULT_SHOW_ITEM) {
                removeView(getLastView());
            }
        }
        this.isTouchable = isTouchable;
        return currentPosition;
    }

    public int clearFontItem() {
        if (data.size() > currentPosition) {
            data.remove(currentPosition);
        }
        moveFontToLast();
        View view = getLastView();
        if (view != null && data.size() > 3) {
            view.setScaleX(1f - (getChildCount() - 1) * DEFAULT_SCALE);
            view.setScaleY(1f - (getChildCount() - 1) * DEFAULT_SCALE);
            view.setTranslationX(0);
            view.setTranslationY((getChildCount() - 1) * DEFAULT_TRANSLATE_Y + SPARE_TRANSLATE_Y);
        }
        moveViewUp();
        addPosition();
        if (data.size() >= DEFAULT_SHOW_ITEM) {
            if (bindView != null) {
                int add = (currentPosition + DEFAULT_DATA_ITEM - 1) % data.size();
                T t = data.get(add);
                bindView.bindView(t, getChangeDataView());
            }
        } else {
            removeView(getLastView());
        }
        minusPosition();
        return currentPosition;
    }

    public interface BindView<T> {
        void bindView(T data, View view);
    }

    public interface FontItemMoveListener {
        void onItemClick(View view);

        void onFontItemRemove(int position);

        void onFontItemBack(int position);

        void afterTurnPage(int position);
    }
}
