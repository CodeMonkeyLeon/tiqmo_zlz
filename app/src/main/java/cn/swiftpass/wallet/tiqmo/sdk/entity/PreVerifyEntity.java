package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by YZX on 2018年12月04日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class PreVerifyEntity {

    private String preAuthId;

    public String getPreAuthId() {
        return preAuthId;
    }

    public void setPreAuthId(String preAuthId) {
        this.preAuthId = preAuthId;
    }
}
