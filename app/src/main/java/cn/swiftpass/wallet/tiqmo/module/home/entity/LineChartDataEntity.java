package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class LineChartDataEntity extends BaseEntity {
    //点对应的数据
    public List<List<String>> data = new ArrayList<>();
    //X轴刻度值
    public List<String> x = new ArrayList<>();
    //Y轴刻度
    public List<String> y = new ArrayList<>();

    public HashMap<Double,Double> getDataMap(List<String> data){
        HashMap<Double,Double> map=new HashMap<>();
        for(int i=0;i<data.size();i++) {
            double money = AndroidUtils.getTransferMoneyNumber(data.get(i));
            map.put((double)i,money);
        }
        return map;
    }
}
