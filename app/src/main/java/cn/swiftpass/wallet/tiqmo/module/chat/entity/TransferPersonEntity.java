package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferPersonEntity extends BaseEntity {
    /**
     * \"recentTransferPersons\":{\"payeeCallingCode\":\"966\",\"transferTime\":1686294257899,
     * \"payeeFirstName\":\"FN533456789\",\"org\":\"96601\",\"payerUserName\":\"Winer\",
     * \"payerMobileNo\":\"532345670\",\"payerCallingCode\":\"966\",\"payerFirstName\":\"Winer\",
     * \"recentlyListType\":\"0\",\"payeeUserName\":\"FN533456789 MN LN ON\",
     * \"payeeUserId\":\"96601000031682066007535\",\"payerUserId\":\"96601000861595319169217\",
     * \"payeeMobileNo\":\"533456789\",\"payeeCustomerNo\":\"110000368005\",\"customerNo\":\"111000152883\"}
     */
    public String payerUserId;
    public String payeeUserId;
    public String payerUserName;
    public String payeeMobileNo;
    public String payerMobileNo;
}
