package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class OpenCardReqEntity extends BaseEntity {
    //即将要开卡的 卡产品类型 - Standard表示标准卡,Platinum表示白金卡
    public String cardProductType;
    //即将要开卡的 卡类型 - VIRTUAL:虚拟卡 PHYSICAL:物理卡
    public String cardType;
    //卡片颜色 : 标准卡的01表示浅色 ,02表示深色；白金卡的06浅色,07深色
    public String cardFaceId;
    //地址信息之州名
    public String province;
    //地址信息之城市名
    public String city;
    //地址信息之详细地址
    public String address;
    //持卡人姓名
    public String cardOfName;
    //set pin 的请求对象
    public CardPinEntity pinBean;

    public String passwordOne,passwordTwo;

    public boolean isVirtualFree,isPhysicalFree;
}
