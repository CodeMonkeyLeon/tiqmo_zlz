package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillTypeEntity extends BaseEntity {

    public String billerType;
    //展示用
    public String billerTypeShow;

    //当渠道是=Sadad的时候返回。父分类名称子描述
    public String billerTypeSub;
    //当渠道是=Sadad的时候返回。父分类名称子描述-展示用
    public String billerTypeSubDescriptionShow;

    public String imgUrl;

    public List<PayBillTypeIconEntity> billerDescriptionList = new ArrayList<>();

}
