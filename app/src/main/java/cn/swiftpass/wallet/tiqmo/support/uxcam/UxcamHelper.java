package cn.swiftpass.wallet.tiqmo.support.uxcam;

import com.uxcam.UXCam;

import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

/**
 * UXCAM 埋点统计
 */
public class UxcamHelper {

    private static final String app_key = "6jyae8o798h8qbf";

    private UxcamHelper() {
    }

    private static class UxcamHolder {
        private static UxcamHelper uxcamHelper = new UxcamHelper();
    }

    public static UxcamHelper getInstance() {
        return UxcamHelper.UxcamHolder.uxcamHelper;
    }

    //启动埋点
    public void startUxcam() {
        UXCam.startWithKey(app_key);
        //隐藏屏幕中输入框内容
        UXCam.occludeAllTextFields(true);
    }

    //隐藏屏幕敏感信息
    public void hideScreen(boolean hideScreen) {
        UXCam.occludeSensitiveScreen(hideScreen);
    }

    public void sendUxcamUserInfo(UserInfoEntity userInfoEntity) {
        UXCam.setUserIdentity(userInfoEntity.phone);
        String birthday = userInfoEntity.birthday;
        UXCam.setUserProperty("Age", AndroidUtils.getAge(birthday));
        UXCam.setUserProperty("Gender", "0".equals(userInfoEntity.gender) ? "Male" : "Female");
        UXCam.setUserProperty("Nationality", userInfoEntity.countryCode);
        UXCam.setUserProperty("AppLanguage", "en_US".equals(AppClient.getLanguage()) ? "en" : AppClient.getLanguage());
    }
}
