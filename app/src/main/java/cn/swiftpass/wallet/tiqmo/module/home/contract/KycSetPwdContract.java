package cn.swiftpass.wallet.tiqmo.module.home.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public class KycSetPwdContract {
    public interface View extends BaseView<Presenter> {
        void checkPayPwdSuccess(Void result);

        void checkPayPwdFail(String errorCode, String errorMsg);
    }

    public interface Presenter extends BasePresenter<View> {
        void checkPayPwd(String pdType, String oldPassword);
    }
}
