package cn.swiftpass.wallet.tiqmo.sdk;

import java.util.Calendar;

public class AppConfig {

    //服务器的公钥
//    public static final String SERVER_PUBLIC_KEY = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERNDl9+/Hl06QNf3e8kG4MSw8fwGRVVs3+0pbsBRqTbIv8yo+pTyYWjbY3mJjUTcREn7RBEKKrgk8OD0P4cg3UA==";//Server PublicKey
    public static String SERVER_PUBLIC_KEY;//Server PublicKey
    public static final String PD_HASH_SALT = "Y29tLnN3aWZ0cGx1cy53d3c="; //密码sha256盐值

    public static final int VERIFICATION_CODE_SEND_INTERVAL = 90 * 1000;//验证码倒计时时间
    public static final int PAYMENT_CODE_CODE_UPDATE_INTERVAL = 60 * 1000;//付款码自动刷新间隔
    public static final int PAYMENT_CODE_CHECK_STATUS_INTERVAL = 3 * 1000;//付款码轮询间隔
    public static final int COLLECT_CODE_CHECK_STATUS_INTERVAL = 1000;//收款码轮询间隔
    public static final int APP_UNLOCK_INTERVAL = 600 * 1000; //应用解锁时间
    public static final int QR_CODE_COUNT = 1;//本地最多的付款码数量
    public static final int QR_CODE_LOAD_MIN_CONDITION = 0; //最少本地QR付款码数量，少于这个数就从服务器拿新的
    public static final int PAYMENT_PD_LENGTH = 6; //支付密码长度
    public static final int PROMPT_USER_UPDATE_INTERVAL = 7; //提示用户更新应用的间隔(单位天)

    /**
     * H5请求中，参数名字，标识，功能
     */
    public static final String H5_REQUEST_FLAG = "swallet";//标识
    public static final String H5_PARAM_DATA = "data";//参数
    public static final String H5_PARAM_CALLBACK = "callback";//参数
    public static final String H5_FUNCTION_PAY = "pay";//功能:支付
    public static final String H5_FUNCTION_CLOSE = "close";//功能:关闭
    public static final String H5_RESPONSE_CODE_SUCCESSFUL = "200";//成功
    public static final String H5_RESPONSE_CODE_FAILURE = "400";//失败

    /**
     * 带参数的上传中，普通参数的part名字
     */
    public static final String MULTIPART_CONTENT_NAME = "content";


    /**
     * 后台返回的布尔类型
     */
    public static final String TRUE = "Y";
    public static final String FALSE = "N";

    /**
     * 性别
     */
    public static final String SEX_MALE = "1"; //男
    public static final String SEX_FEMALE = "2"; //女

    /**
     * 支付方法类型
     */
    public static final String PAYMENT_METHOD_CP = "CP";     //卡类型
    public static final String PAYMENT_METHOD_BAL = "BAL";   //余额类型

    /**
     * 扫一扫支持的类型
     */
    public static final String CARD_TYPE_NOT = "N"; //不支持任何卡类型
    public static final String CARD_TYPE_CREDIT = "C"; //信用卡
    public static final String CARD_TYPE_DEBIT = "D";  //银行卡
    public static final String CARD_TYPE_PREPAID = "P0";  //预付费卡
    public static final String CARD_TYPE_MAXICARE = "P1";  //医疗卡

    /**
     * 交易类型常量
     */
    public static final String ORDER_TYPE_TRANSFER = "T";    //转账
    public static final String ORDER_TYPE_TOP_UP = "R";    //充值
    public static final String ORDER_TYPE_WITHDRAW = "W";    //提现
    public static final String ORDER_TYPE_COLLECT = "C";    //收款
    public static final String ORDER_TYPE_REFUND = "D";    //退款
    public static final String ORDER_TYPE_CONSUME = "P"; //消费

    /**
     * 订单状态
     */
    public static final String ORDER_STATUS_SUBMITTED = "C";//已提交
    public static final String ORDER_STATUS_PAYING = "W";//支付中
    public static final String ORDER_STATUS_CERTIFYING = "R";//认证中
    public static final String ORDER_STATUS_SUCCESSFUL = "S";//成功
    public static final String ORDER_STATUS_INVALID = "V";//已关闭
    public static final String ORDER_STATUS_FAILURE = "E";//失败
    public static final String ORDER_STATUS_REFUNDING = "X";//转入退款
    public static final String ORDER_STATUS_ALL_REFUND = "Y";//已退款
    public static final String ORDER_STATUS_RECALL = "B";//撤销

    /**
     * 订单交易方向
     */
    public static final String TRADING_DIRECTION_PAYMENT = "P";    //支付
    public static final String TRADING_DIRECTION_INCOME = "I";    //收入

    /**
     * 支付类型
     */
    public static final String PAY_TYPE_PROACTIVE = "P";//主扫支付
    public static final String PAY_TYPE_PASSIVE = "B";//被扫支付
    public static final String PAY_TYPE_H5 = "H";//H5支付
    public static final String PAY_TYPE_QUICK_PAY = "N";//免密支付

    /**
     * 扫一扫支持的类型
     */
    public static final String SCAN_CONTENT_TYPE_HTML = "h";    //HTML
    public static final String SCAN_CONTENT_TYPE_ORDER = "o";    //订单类型
    public static final String SCAN_CONTENT_TYPE_MALL = "m";    //商城H5
    public static final String SCAN_CONTENT_TYPE_STRING = "s";    //字符串
    public static final String SCAN_CONTENT_TYPE_ILLEGAL = "i";    //非法内容

    public static final Calendar MONTH_FILTER_MIN_DATE; //帐单列表，月份过滤允许的最小时间
    public static final Calendar DATE_INTERVAL_FILTER_MIN_DATE;//帐单列表，时间间隔过滤允许的最小时间


    static {
        MONTH_FILTER_MIN_DATE = Calendar.getInstance();
        DATE_INTERVAL_FILTER_MIN_DATE = Calendar.getInstance();

        DATE_INTERVAL_FILTER_MIN_DATE.set(2019, 8, 1, 0, 0, 0);
        MONTH_FILTER_MIN_DATE.set(2019, 8, 1, 0, 0, 0);
    }


}
