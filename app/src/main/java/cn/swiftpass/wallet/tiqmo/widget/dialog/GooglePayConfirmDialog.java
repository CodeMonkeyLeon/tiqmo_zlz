package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class GooglePayConfirmDialog extends BottomDialog {

    private Context mContext;

    private PayMoneyListener payMoneyListener;


    private String totalAmount;

    public GooglePayConfirmDialog(Context context, TransFeeEntity transFeeEntity) {
        super(context);
        this.mContext = context;
        initViews(context, transFeeEntity);
    }

    public interface PayMoneyListener {
        void payMoney(String money);
    }

    public void setPayMoneyListener(final PayMoneyListener payMoneyListener) {
        this.payMoneyListener = payMoneyListener;
    }

    private void initViews(Context context, TransFeeEntity transFeeEntity) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_google_pay_confirm, null);
        ImageView ivGooglePay = mView.findViewById(R.id.iv_google_pay);
        TextView tvTopupAmount = mView.findViewById(R.id.tv_topup_amount);
        TextView tvWrongFees = mView.findViewById(R.id.tv_wrong_fees);
        TextView tvFees = mView.findViewById(R.id.tv_fees);
        ImageView ivBack = mView.findViewById(R.id.iv_back);
        TextView tvWrongVatMoney = mView.findViewById(R.id.tv_wrong_vat_money);
        TextView tvVatMoney = mView.findViewById(R.id.tv_vat_money);
        TextView tvTotalAmount = mView.findViewById(R.id.tv_total_amount);
        TextView tvCancel = mView.findViewById(R.id.tv_cancel);
        TextView tvConfirm = mView.findViewById(R.id.tv_confirm);

        tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        ivBack.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });

        totalAmount = "0.00";
        if (transFeeEntity != null) {
            String topupAmount = transFeeEntity.getOrderAmount();
            String transferFee = transFeeEntity.getDiscountTransFees();
            String wrongTransferFee = transFeeEntity.getTransFees();
            totalAmount = transFeeEntity.getTotalAmount();
            String wrongVat = transFeeEntity.getVat();
            String vat = transFeeEntity.getDiscountVat();
            String orderCurrencyCode = transFeeEntity.orderCurrencyCode;
            if (!TextUtils.isEmpty(topupAmount)) {
                tvTopupAmount.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(topupAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(transferFee)) {
                tvFees.setText(Spans.builder()
                        .text(transferFee + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(vat)) {
                tvVatMoney.setText(Spans.builder()
                        .text(vat + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(totalAmount)) {
                tvTotalAmount.setText(Spans.builder()
                        .text(totalAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(wrongTransferFee)) {
                tvWrongFees.setText(Spans.builder()
                        .text(wrongTransferFee + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(wrongVat)) {
                tvWrongVatMoney.setText(Spans.builder()
                        .text(wrongVat + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).size(10)
                        .build());
            }
        }

        tvCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });

        tvConfirm.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (payMoneyListener != null) {
                    payMoneyListener.payMoney(totalAmount);
                }
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
