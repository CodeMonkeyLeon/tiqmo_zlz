package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;


public class SideBar extends View {
    // 触摸事件
    private OnTouchingLetterChangedListener onTouchingLetterChangedListener;
    public static String[] b = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"};
    private int choose = -1;
    private Paint paint = new Paint();

    private TextView mTextDialog;

    private Bitmap bitmap;

    private int oldHeight;
    private Context mContext;
    //是否隐藏#符号
    private boolean isHideSpecial;

    public void setHideSpecial(final boolean hideSpecial) {
        this.isHideSpecial = hideSpecial;
    }

    /**
     * 为SideBar设置显示字母的TextView
     *
     * @param textDialog
     */
    public void setTextView(TextView textDialog) {
        this.mTextDialog = textDialog;
    }


    public SideBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public SideBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SideBar(Context context) {
        this(context, null);
    }

    public void init(Context context) {
        mContext = context;
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.d_select_alphabet);
        Matrix m = new Matrix();
        m.setRotate(LocaleUtils.isRTL(mContext) ? 180 : 0, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //虚拟导航栏高度 解决
        int currentHeight = (int) (getHeight());
        if (oldHeight != 0 && currentHeight < oldHeight) {
            currentHeight = oldHeight;
        }
        int length = b.length;
        int width = getWidth();
        int singleHeight = currentHeight / length;// 获取每一个字母的高度

        for (int i = 0; i < length; i++) {
//            paint.setColor(Color.parseColor("#11A0F8"));
            paint.setColor(getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_30white_9c9da1)));
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setAntiAlias(true);
            paint.setTextSize(24);
            if (i == choose) {// 选中的状态
                paint.setColor(getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)));
                paint.setFakeBoldText(true);

            }
            // x坐标等于中间-字符串宽度的一半
            float xPos = LocaleUtils.isRTL(mContext) ? AndroidUtils.dip2px(mContext, 14)
                    : width * 5 / 6 - paint.measureText(b[i]) / 2;
            float yPos = singleHeight * i + singleHeight;
            if (i == length - 1 && isHideSpecial) {
            }else {
                canvas.drawText(b[i], xPos, yPos, paint);
            }

            if (i == choose) {
                canvas.drawBitmap(bitmap, LocaleUtils.isRTL(mContext) ? xPos - 18 : xPos - 38,
                        LocaleUtils.isRTL(mContext) ? yPos - 35 : yPos - 34, paint);
                paint.setColor(getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)));
                paint.setTypeface(Typeface.DEFAULT);
                paint.setAntiAlias(true);
                paint.setTextSize(56);
                if (i == length - 1 && isHideSpecial) {
                }else {
                    canvas.drawText(b[i], LocaleUtils.isRTL(mContext) ? xPos + 100 : xPos - 100, yPos + 10, paint);
                }
            }

            paint.reset();
        }
        oldHeight = currentHeight;

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        System.out.print("heightMeasureSpec:" + heightMeasureSpec);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float y = event.getY();
        final int oldChoose = choose;
        final OnTouchingLetterChangedListener listener = onTouchingLetterChangedListener;
        final int c = (int) (y / getHeight() * b.length);// 点击y坐标所占总高度的比例*b数组的长度就等于点击b中的个数

        switch (action) {
            case MotionEvent.ACTION_UP:
                //setBackground(new ColorDrawable(0x00000000));
                choose = -1;//
                invalidate();
                if (mTextDialog != null) {
                    mTextDialog.setVisibility(View.INVISIBLE);
                }
                break;

            default:
//                setBackgroundResource(R.drawable.sidebar_background);
                if (oldChoose != c) {
                    if (c >= 0 && c < b.length) {
                        if (listener != null) {
                            listener.onTouchingLetterChanged(b[c]);
                        }
                        if (mTextDialog != null) {
                            mTextDialog.setText(b[c]);
                            mTextDialog.setVisibility(View.INVISIBLE);
                        }

                        choose = c;
                        invalidate();
                    }
                }

                break;
        }
        return true;
    }

    /**
     * 触摸事件
     *
     * @param onTouchingLetterChangedListener
     */
    public void setOnTouchingLetterChangedListener(OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
        this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
    }

    /**
     * @author coder
     */
    public interface OnTouchingLetterChangedListener {
        void onTouchingLetterChanged(String s);
    }
}
