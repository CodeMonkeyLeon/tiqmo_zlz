package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;

public class ComingSoonFragment extends BaseFragment {

    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_back_to_home)
    TextView tvBackToHome;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_coming_soon)
    TextView tvComingSoon;
    @BindView(R.id.iv_comming_soon)
    ImageView ivComingSoon;

    private boolean isActivity;


    public static ComingSoonFragment getInstance(String title) {
        ComingSoonFragment comingSoonFragment = new ComingSoonFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        comingSoonFragment.setArguments(bundle);
        return comingSoonFragment;
    }
    public static ComingSoonFragment getInstance(String title, boolean isActivity) {
        ComingSoonFragment comingSoonFragment = new ComingSoonFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putBoolean("isActivity", isActivity);
        comingSoonFragment.setArguments(bundle);
        return comingSoonFragment;
    }


    @Override
    protected int getLayoutID() {
        return R.layout.fragment_coming_soon;
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, ivBack);
        if (getArguments() != null) {
            tvTitle.setText(mContext.getString(R.string.newhome_23));
            isActivity = getArguments().getBoolean("isActivity", true);
        }
        if (!isActivity) {
            ivBack.setVisibility(View.GONE);

        }
    }

    @Override
    protected void initData() {
        noticeThemeChange();
    }

    @Override
    protected void restart() {

    }

    @OnClick({R.id.iv_back, R.id.tv_back_to_home})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_back_to_home:
                if (isActivity) {
                    ActivitySkipUtil.startAnotherActivity(mActivity,MainActivity.class);
                } else {
                    if (mActivity != null && mActivity instanceof MainActivity) {
                        ((MainActivity) mActivity).showBottom(3);
                    }
                }
                break;
            default:
                break;
        }
    }
    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(bgMain, R.attr.splash_bg_color);
        helper.setImageBgResourceByAttr(headCircle, R.attr.icon_circle);
        helper.setTextColorByAttr(tvTitle, R.attr.wallet_setting_text_color);
        helper.setImageBgResourceByAttr(headCircle, R.attr.icon_circle);
        helper.setTextColorByAttr(tvComingSoon, R.attr.wallet_setting_text_color);
        helper.setImageBgResourceByAttr(ivComingSoon, R.attr.icon_coming_soon);
        helper.setBackgroundResourceByAttr(tvBackToHome, R.attr.bg_btn_next_page_card);

    }

}
