package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.CategoryEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SpendingDateAdapter extends BaseRecyclerAdapter<CategoryEntity> {

    private int choosePosition;

    public void setChoosePosition(final int choosePosition) {
        this.choosePosition = choosePosition;
    }

    public SpendingDateAdapter(@Nullable List<CategoryEntity> data) {
        super(R.layout.item_spending_date, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, CategoryEntity categoryEntity, int position) {
        if (choosePosition == position) {
            baseViewHolder.setBackgroundRes(R.id.con_spending_date, ThemeSourceUtils.getSourceID(mContext, R.attr.color_20white_d8e1ec));
            baseViewHolder.setImageResource(R.id.iv_spending_date, R.drawable.icon_date_check);
        } else {
            baseViewHolder.setBackgroundRes(R.id.con_spending_date, R.color.transparent);
            baseViewHolder.setImageResource(R.id.iv_spending_date, categoryEntity.imgId);
        }

        baseViewHolder.setText(R.id.tv_spending_date, categoryEntity.textId);
        baseViewHolder.setTextColor(R.id.tv_spending_date, ResourceHelper.getInstance(mContext).getColorByAttr(R.attr.color_white_3a3b44));
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }
}
