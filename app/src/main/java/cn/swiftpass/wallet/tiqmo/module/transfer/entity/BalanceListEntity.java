package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class BalanceListEntity extends BaseEntity {

    public List<BalanceEntity> balanceInfos = new ArrayList<>();

    /**
     * 不可用余额（最小币种）
     */
    public String totalHoldAmount;
}
