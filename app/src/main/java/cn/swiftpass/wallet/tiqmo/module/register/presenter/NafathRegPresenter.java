package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import cn.swiftpass.wallet.tiqmo.module.register.contract.NafathContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class NafathRegPresenter implements NafathContract.Presenter {

    NafathContract.View nafathView;

    @Override
    public void getNafathUrl(String callingCode, String phone, String recipientId,String sceneType) {
        AppClient.getInstance().getUserManager().getNafathUrl(callingCode, phone, recipientId,sceneType, new LifecycleMVPResultCallback<NafathUrlEntity>(nafathView,true) {
            @Override
            protected void onSuccess(NafathUrlEntity result) {
                nafathView.getNafathUrlSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                nafathView.getNafathUrlFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getNafathResult(String callingCode, String phone,String hashedState, String recipientId,String random,String sceneType) {
        AppClient.getInstance().getUserManager().getNafathResult(callingCode,phone,hashedState, recipientId,random,sceneType, new LifecycleMVPResultCallback<NafathResultEntity>(nafathView,true) {
            @Override
            protected void onSuccess(NafathResultEntity result) {
                nafathView.getNafathResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                nafathView.getNafathResultFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(NafathContract.View view) {
        this.nafathView = view;
    }

    @Override
    public void detachView() {
        this.nafathView = null;
    }
}
