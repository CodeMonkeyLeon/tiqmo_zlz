package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class HomeBannerAdapter extends BannerAdapter<AllMarketInfoEntity.MarketInfo, HomeBannerAdapter.BannerViewHolder> {

    private Context mContext;

    public HomeBannerAdapter(Context mContext,List<AllMarketInfoEntity.MarketInfo> mDatas) {
        //设置数据，也可以调用banner提供的方法,或者自己在adapter中实现
        super(mDatas);
        this.mContext = mContext;
    }

    //创建ViewHolder，可以用viewType这个字段来区分不同的ViewHolder
    @Override
    public BannerViewHolder onCreateHolder(ViewGroup parent, int viewType) {
//        ImageView imageView = new ImageView(parent.getContext());
//        //注意，必须设置为match_parent，这个是viewpager2强制要求的
//        imageView.setLayoutParams(new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT));
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        return new BannerViewHolder(imageView);

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_home_banner, parent, false);
        return new BannerViewHolder(itemView);
    }

    @Override
    public void onBindView(BannerViewHolder holder, AllMarketInfoEntity.MarketInfo data, int position, int size) {
        try {
            if (!TextUtils.isEmpty(data.activityPicUrl)) {
                String picUrl = data.activityPicUrl;
//            Glide.with(ProjectApp.getContext()).clear(holder.imageView);
                Glide.with(ProjectApp.getContext())
                        .load(picUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(holder.imageView.getDrawable())
                        .into(holder.imageView);
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    class BannerViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.iv_banner);
        }
    }
}
