package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class EnterCardCvvDialog extends BottomDialog{
    private Context mContext;

    private ContinueListener continueListener;

    private  String cvv;

    public void setContinueListener(ContinueListener chooseTypeListener) {
        this.continueListener = chooseTypeListener;
    }

    public EnterCardCvvDialog(Context context, CardEntity cardEntity) {
        super(context,R.style.DialogBottomInTransfer);
        this.mContext = context;
        initViews(mContext,cardEntity);
    }

    public interface ContinueListener {
        void clickContinue(String cvv);
    }

    private void initViews(Context mContext,CardEntity cardEntity) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_enter_card_cvv, null);
        TextView tvSelectTitle = view.findViewById(R.id.tv_select_title);
        TextView tvCardNumber = view.findViewById(R.id.tv_card_number);
        TextView tvCardExpire = view.findViewById(R.id.tv_card_expire);
        TextView tvContinue = view.findViewById(R.id.tv_continue);
        EditTextWithDel etCardCvv = view.findViewById(R.id.et_card_cvv);
        etCardCvv.getTlEdit().setHint(mContext.getString(R.string.sprint11_8));

        if(cardEntity != null){
            String cardNo = cardEntity.getCardNo();
            String custName = cardEntity.getCustName();
            String cardOrganization = cardEntity.getCardOrganization();
            String expire = cardEntity.getExpire();
            StringBuffer sb = new StringBuffer();
            sb.append(expire).insert(2,"/");
            tvCardExpire.setText(mContext.getString(R.string.sprint11_38)+" "+sb.toString());
            if(!TextUtils.isEmpty(custName)){
                tvCardNumber.setText(custName);
            }else {
                if(!TextUtils.isEmpty(cardNo)) {
                    tvCardNumber.setText(cardOrganization + " **** " + cardNo.substring(12, 16));
                }
            }
        }

        etCardCvv.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cvv = s.toString();
                tvContinue.setEnabled(!TextUtils.isEmpty(cvv) && cvv.length() == 3);
            }
        });
        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(continueListener != null){
                    continueListener.clickContinue(cvv);
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

}
