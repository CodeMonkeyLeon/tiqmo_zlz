package cn.swiftpass.wallet.tiqmo.support.eventbus;

public class CardEvent extends BaseEvent {
    public static final int remove_card_success = 1001;

    private int eventType;

    public int getEventType() {
        return this.eventType;
    }

    public CardEvent(int eventType) {
        this.eventType = eventType;
    }
}
