package cn.swiftpass.wallet.tiqmo.support.theme;

import java.util.ArrayList;
import java.util.List;

public class ThemeChangeObserverStack {

    private List<ThemeChangeObserver> mThemeChangeObserverStack; //  主题切换监听栈

    private ThemeChangeObserverStack() {
        if (mThemeChangeObserverStack == null) {
            mThemeChangeObserverStack = new ArrayList<>();
        }
    }

    private static ThemeChangeObserverStack instance = new ThemeChangeObserverStack();

    public static ThemeChangeObserverStack getInstance() {
        return instance;
    }

    /**
     * 向堆栈中添加observer
     */
    public void registerObserver(ThemeChangeObserver observer) {
        if (observer == null || mThemeChangeObserverStack.contains(observer)) return;
        mThemeChangeObserverStack.add(observer);
    }

    /**
     * 从堆栈中移除observer
     */
    public void unregisterObserver(ThemeChangeObserver observer) {
        if (observer == null || !(mThemeChangeObserverStack.contains(observer))) return;
        mThemeChangeObserverStack.remove(observer);
    }

    /**
     * 向堆栈中所有对象发送更新UI的指令
     */
    public void notifyByThemeChanged() {
        List<ThemeChangeObserver> observers = mThemeChangeObserverStack;
        for (ThemeChangeObserver observer : observers) {
            observer.notifyByThemeChanged();
        }
    }
}