package cn.swiftpass.wallet.tiqmo.module.transfer.adapter;

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class TransferRelationDetailAdapter extends BaseRecyclerAdapter<BeneficiaryEntity> {

    public TransferRelationDetailAdapter(List<BeneficiaryEntity> list) {
        super(R.layout.item_transfer_account, list);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, BeneficiaryEntity beneficiaryEntity, int position) {
        baseViewHolder.setText(R.id.tv_name, beneficiaryEntity.beneficiaryName);
        baseViewHolder.setText(R.id.tv_bank_name_title, mContext.getString(R.string.wtba_15));
        baseViewHolder.setBackgroundRes(R.id.ll_content, ThemeSourceUtils.getSourceID(mContext, R.attr.bg_051446_061F6F));
        ImageView iv_transfer = baseViewHolder.getView(R.id.iv_transfer);
        Glide.with(mContext)
                .load(beneficiaryEntity.bankLogo)
                .dontAnimate()
                .format(DecodeFormat.PREFER_RGB_565)
                .into(iv_transfer);
        baseViewHolder.setText(R.id.tv_bank_name, beneficiaryEntity.bankName);
        String ibanNum = beneficiaryEntity.beneficiaryIbanNo;
        if (!TextUtils.isEmpty(ibanNum)) {
            baseViewHolder.setText(R.id.tv_bank_number, AndroidUtils.changeBanNum(ibanNum));
        }
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }
}
