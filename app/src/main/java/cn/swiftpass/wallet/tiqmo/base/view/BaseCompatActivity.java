package cn.swiftpass.wallet.tiqmo.base.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.view.ExpireSessionActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.MainActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.util.NetworkUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerEntity;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidBug5497Workaround;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ExecutorManager;
import cn.swiftpass.wallet.tiqmo.support.utils.GPSUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.OnImageBitmapSuccess;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SdkShareUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CommonNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.GpsPermissionDialog;

/**
 * MVP基类，只处MVP相关逻辑
 */
public abstract class BaseCompatActivity<P extends BasePresenter> extends BaseHintCompatActivity {
    //用于常用的传递数据
    public static final String EXTRA_DATA = "extra_data";

    public static final int SYSTEM_UI_MODE_NONE = 0;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS = 1;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS = 2;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION = 3;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION = 4;
    public static final int SYSTEM_UI_MODE_FULLSCREEN = 5;
    public static final int SYSTEM_UI_MODE_LIGHT_BAR = 6;

    protected static final int PERMISSON_REQUESTCODE = 0;

    protected int screenWidth;

    protected CommonNoticeDialog commonNoticeDialog;

    public boolean firstRejectPermission = true;

    protected boolean permission;

    @IntDef({SYSTEM_UI_MODE_NONE, SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS, SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS, SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION, SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION, SYSTEM_UI_MODE_FULLSCREEN, SYSTEM_UI_MODE_LIGHT_BAR})
    @Retention(RetentionPolicy.SOURCE)
    private @interface SystemUiMode {
    }

    protected P mPresenter;

    private static final String TAG = "BaseCompatActivity";
    private Handler mHandler;
    protected long longTimeNotUse;
    private String countTime;

    private long updateAddressTime = 15 * 60 * 1000;
    private Address address;

    private boolean isFirstConfig = true;

    public boolean dispatchTouchEvent(MotionEvent event) {
        try {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    removeCallbacks();
                    break;

                case MotionEvent.ACTION_UP:
                    startAD();
                    break;

            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return super.dispatchTouchEvent(event);
    }

    private void removeCallbacks(){
        if (mHandler == null) {
            mHandler = ProjectApp.getInstance().getMainHandler();
        }
        mHandler.removeCallbacksAndMessages(null);
    }

    private void showExpireSessionDialog(String countTime){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("countTime",countTime);
        ActivitySkipUtil.startAnotherActivity(this, ExpireSessionActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.NONE);
    }

    protected void getConfig(){
        if(AppClient.getInstance().getUserManager() != null) {
            AppClient.getInstance().getUserManager().getConfig(null);
        }
    }
    protected void checkLanguageUpdate(final ResultCallback<LanguageListEntity> callback){
        if(AppClient.getInstance().getUserManager() != null) {
            AppClient.getInstance().getUserManager().checkLanguageUpdate(callback);
        }
    }

    protected void logout(){
        AppClient.getInstance().logout(new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {

            }

            @Override
            public void onFailure(String code, String error) {

            }
        });
        removeCallbacks();
        AppClient.getInstance().clearLastSessionInfo();
        UserInfoEntity user = AppClient.getInstance().getUserManager().getUserInfo();
        if (user != null) {
            LoginFastNewActivity.startActivityOfNewTaskType(mContext, user, null);
        } else {
            RegisterActivity.startActivityOfNewTaskType(mContext);
        }
    }

    /**
     * 长时间未操作app
     */
    private Runnable longTimeRunnable = new Runnable() {
        @Override
        public void run() {
            if(!TextUtils.isEmpty(countTime)) {
                showExpireSessionDialog(countTime);
            }
        }

    };

    public void startAD()   {
        try {
            if(longTimeNotUse>0) {
                if (AppClient.getInstance() != null && !TextUtils.isEmpty(AppClient.getInstance().getSessionID())) {
                    removeCallbacks();
                    mHandler.postDelayed(longTimeRunnable, longTimeNotUse);
                }
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        screenWidth = AndroidUtils.getScreenWidth(mContext);
        try {
            if (getConfigEntity() != null && isFirstConfig) {
                String modifyTime = getConfigEntity().modifyTime;
                countTime = getConfigEntity().countTime;
                if(!TextUtils.isEmpty(modifyTime)) {
                    longTimeNotUse = Long.parseLong(BigDecimalFormatUtils.mul(BigDecimalFormatUtils.sub(modifyTime,countTime,0), "1000", 0));
                    isFirstConfig = false;
                }
//                longTimeNotUse = 30*1000;
//                countTime = "15";
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        initPresenter();
        super.onInit(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            //http://wallet-saas-dev.wallyt.net/tiqmolink/referralCode=6O1DKP
                            if(deepLink != null && deepLink.toString().contains("=")) {
                                String referralCode = deepLink.toString().split("=")[1];
                                SpUtils.getInstance().setReferralCode(referralCode);
                                SpUtils.getInstance().setShareLink(deepLink.toString());
                            }
                            LogUtils.d(TAG, "getDynamicLink:deepLink"+ deepLink);
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        LogUtils.d(TAG, "getDynamicLink:onFailure"+ e);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void showLogoutDialog() {
        BottomDialog bottomDialog = new BottomDialog(mContext);
        bottomDialog.setCanceledOnTouchOutside(false);
        bottomDialog.setCancelable(false);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_kyc_logout, null);
        TextView tvLogOut = contentView.findViewById(R.id.tv_log_out);

        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    ActivitySkipUtil.startAnotherActivity((Activity) mContext, RegisterActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    finish();
                    ProjectApp.removeAllTaskExcludeLoginStack();
                }
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
//            layoutParams.height = AndroidUtils.dip2px(mActivity, 400);
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mPresenter != null) {
            mPresenter.detachView();
            mPresenter = null;
        }

        if(this instanceof MainActivity){
            removeCallbacks();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public boolean isConnected() {
        return NetworkUtil.isNetworkConnected(mContext);
    }


    @SuppressWarnings("unchecked")
    private void initPresenter() {
        if (this instanceof BaseView) {
            BaseView view = (BaseView) this;
            mPresenter = (P) view.getPresenter();
            if (mPresenter == null) {
                return;
            }
            Class aClass = this.getClass();
            while (aClass != null) {
                Type type = aClass.getGenericSuperclass();
                if (type instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type;
                    Type genericType = parameterizedType.getActualTypeArguments()[0];
                    Class<?>[] interfaces = mPresenter.getClass().getInterfaces();
                    for (Class c : interfaces) {
                        if (c == genericType) {
                            mPresenter.attachView(view);
                            return;
                        }
                    }

                } else {
                    aClass = aClass.getSuperclass();
                }
            }
            mPresenter = null;
        }
    }

//    public void setSystemUiMode(@SystemUiMode int mode) {
//        Window window = getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        switch (mode) {
//            case SYSTEM_UI_MODE_NONE:
//                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    TypedValue value = new TypedValue();
//                    getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
//                    window.setStatusBarColor(value.data);
//                }
//                break;
//            case SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS:
//            case SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS:
//                if (mode == SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//                } else {
//                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                }
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                    window.setStatusBarColor(Color.TRANSPARENT);
//                }
//                break;
//            case SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION:
//            case SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION:
//                if (mode == SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
//                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//                    } else {
//                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                    }
//                } else {
//                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                }
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                    window.setNavigationBarColor(Color.TRANSPARENT);
//                    window.setStatusBarColor(Color.TRANSPARENT);
//                }
//                break;
//            case SYSTEM_UI_MODE_FULLSCREEN:
//                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//                break;
//            case SYSTEM_UI_MODE_LIGHT_BAR:
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//                }
//                break;
//            default:
//                break;
//        }
//    }

    public boolean isAttachedToPresenter() {
        return mPresenter != null;
    }

    /**
     * 检查是否有permission权限  返回true则表示有
     */
    public boolean isGranted_(String permission) {
        if (mContext == null) {
            return false;
        }
        int checkSelfPermission = ActivityCompat.checkSelfPermission(mContext, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    protected void fixedSoftKeyboardSliding() {
        AndroidBug5497Workaround.assistActivity(this);
    }

    /**
     * 每次启动后先去获取定位信息  保证本地经纬度刷新
     * @return
     */
    public boolean getPermission(){
        if (!isGranted_(Manifest.permission.ACCESS_FINE_LOCATION) || !isGranted_(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            //检查是否有定位权限  如果没有  则提示开启权限
            PermissionInstance.getInstance().getPermission(this, new PermissionInstance.PermissionCallback() {
                        @Override
                        public void acceptPermission() {
                            getLocation();
                            permission = true;
                        }

                        @Override
                        public void rejectPermission() {
                            showPermissionDialog(false);
                            permission = false;

                        }
                    }, Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            getLocation();
//            permission = true;
        }

        return permission;
    }

    private void getAddress(double latitude,double longitude){
        //反编译耗时较长,建议放子线程进行
        new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder gc = new Geocoder(mContext, Locale.getDefault());
                try {
                    List<Address> result = gc.getFromLocation(latitude,
                            longitude, 1);
                    Address address = result.get(0);
                    updateAddress(latitude,longitude,address);
//                                LogUtils.e("地址信息",result.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    protected void getLocation() {
        GPSUtils.getInstance(mContext).getLngAndLat(new GPSUtils.OnLocationResultListener() {
            @Override
            public void onLocationResult(Location location,int type) {
                if (location != null) {
                    SpUtils.getInstance().setLongitude(String.valueOf(location.getLongitude()));
                    SpUtils.getInstance().setLatitude(String.valueOf(location.getLatitude()));
                    permission =true;
                    //不为空,显示地理位置经纬度
                    getAddress(location.getLatitude(),location.getLongitude());
                } else {
                    String longitude = SpUtils.getInstance().getLongitude();
                    String latitude = SpUtils.getInstance().getLatitude();
                    if(!TextUtils.isEmpty(longitude) && type == 1){
                        //如果历史经纬度不为空 但当前经纬度没有获取到  则用历史经纬度
                        permission = true;
                        getAddress(Double.parseDouble(latitude),Double.parseDouble(longitude));
                    }else {
                        showPermissionDialog(true);
                        permission = false;
                    }
                }
            }

            @Override
            public void OnLocationChange(Location location) {
                if (location != null) {
                    //不为空,显示地理位置经纬度
                    getAddress(location.getLatitude(),location.getLongitude());
                }
            }
        });
    }

    protected void showPermissionDialog(boolean isGPS) {
        GpsPermissionDialog dialog = new GpsPermissionDialog(mContext);
        dialog.setGotoPermissionListener(new GpsPermissionDialog.GotoPermissionListener() {
            @Override
            public void gotoPermission() {
                if (isGPS) {
                    Intent i = new Intent();
                    i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(i);
                }else {
                    Intent intent = new Intent();
                    intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + "cn.swiftpass.wallet.tiqmo"));
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });
        dialog.setDismissListener(new GpsPermissionDialog.DismissListener() {
            @Override
            public void dismiss() {
                dialog.dismiss();
//                ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
            }
        });
        dialog.showMatchParentWidthAndGravityCenter(AndroidUtils.getScreenWidth(mContext));
    }

    protected  void updateAddress(double latitude,double longitude, Address address){
        ProjectApp.setLongitude(longitude);
        ProjectApp.setLatitude(latitude);
        if(address != null && !TextUtils.isEmpty(address.getCountryCode())) {
            AppClient.getInstance().updateAddress(address.getCountryCode().length() == 2?address.getCountryCode():"", address.getCountryCode().length() == 3?address.getCountryCode():"",
                    address.getCountryName(), longitude, latitude, null);
        }else{
            AppClient.getInstance().updateAddress("", "",
                    "", longitude, latitude, null);
        }

        this.address = address;
        removeCallbacks();
        mHandler.postDelayed(updateAddressRunnable, updateAddressTime);
    };

    /**
     * 定时更新地址信息
     */
    private Runnable updateAddressRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                if(address != null && !TextUtils.isEmpty(address.getCountryCode())) {
                    AppClient.getInstance().updateAddress(address.getCountryCode().length() == 2?address.getCountryCode():"", address.getCountryCode().length() == 3?address.getCountryCode():"",
                            address.getCountryName(), ProjectApp.getLongitude(), ProjectApp.getLatitude(), null);
                }else{
                    AppClient.getInstance().updateAddress("", "",
                            "", ProjectApp.getLongitude(), ProjectApp.getLatitude(), null);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    /**
     * 截图保存图片到本地
     */
    protected void saveImage(LinearLayout linearLayout) {
        if (!isGranted_(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                firstRejectPermission = false;
            } else {
                firstRejectPermission = true;
            }
            PermissionInstance.getInstance().getPermission(this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    saveToAlbum(linearLayout);
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        //拒绝并勾选不再询问
                        if (firstRejectPermission) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_32));
                        } else {
                            firstRejectPermission = false;
                        }
                    }
                }
            }, PermissionInstance.getPermissons(2));
        } else {
            saveToAlbum(linearLayout);
        }
    }

    private void saveToAlbum(LinearLayout linearLayout) {
        showProgress(true);
        linearLayout.post(new Runnable() {
            @Override
            public void run() {
                ExecutorManager.getInstance()
                        .execute(new Runnable() {
                            @Override
                            public void run() {
//                                if ((Activity) mContext instanceof ReferralCodeActivity) {
//                                    saveImageToGalley((Activity) mContext);
//                                } else {
//                                    saveImageToGalley(linearLayout);
//                                }
                                saveImageToGalley(linearLayout);
                                linearLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        showTipDialog(getString(R.string.common_28));
                                        showProgress(false);
                                    }
                                });
                            }
                        });
            }
        });
    }

    private void saveImageToGalley(LinearLayout linearLayout) {
        // 把一个View转换成图片
        Bitmap cacheBmp = AndroidUtils.getViewGroupBitmap(linearLayout);
        AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cacheBmp);
    }

//    private void saveImageToGalley(Activity activity) {
//        // 把一个View转换成图片
//        Bitmap cacheBmp = FastBlurUtility.takeScreenShot(activity);
//        AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cacheBmp);
//    }

    /**
     * 截图并分享
     */
    protected void downShareImage(LinearLayout linearLayout, String title) {
        Bitmap cachebmp = null;
//        if ((Activity) mContext instanceof ReferralCodeActivity) {
//            cachebmp = FastBlurUtility.takeScreenShot((Activity) mContext);
//        } else {
//            cachebmp = AndroidUtils.getViewGroupBitmap(linearLayout);
//        }
        cachebmp = AndroidUtils.getViewGroupBitmap(linearLayout);
        initShareView(cachebmp, linearLayout, new OnImageBitmapSuccess() {
            @Override
            public void onImageBitmapCallBack(Uri uri) {
                showProgress(false);
//                AndroidUtils.shareImage(mContext, title, uri.getPath());
                AndroidUtils.shareImage(mContext, title, uri);
            }
        });
    }

    private void initShareView(Bitmap cachebmp, LinearLayout linearLayout, OnImageBitmapSuccess onImageBitmapSuccess) {

        if (!isGranted_(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                firstRejectPermission = false;
            } else {
                firstRejectPermission = true;
            }
            PermissionInstance.getInstance().getPermission(this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    shareImageTemp(cachebmp, onImageBitmapSuccess);
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        //拒绝并勾选不再询问
                        if (!firstRejectPermission) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_32));
                        } else {
                            firstRejectPermission = false;
                        }
                    }
                }
            }, PermissionInstance.getPermissons(2));
        } else {
            shareImageTemp(cachebmp, onImageBitmapSuccess);
        }
    }

    private void shareImageTemp(Bitmap cachebmp, OnImageBitmapSuccess onImageBitmapSuccess) {
//        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + mContext.getString(R.string.account_pay_detail);
        String filePath = "transfer_" + AndroidUtils.localImageStr(System.currentTimeMillis()) + ".jpg";
//        String filePath = mContext.getExternalFilesDir(Environment.DIRECTORY_SCREENSHOTS).getAbsolutePath() + File.separator + mContext.getString(R.string.account_pay_detail);
        ExecutorManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, filePath);
                if (uri != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (onImageBitmapSuccess != null) {
                                onImageBitmapSuccess.onImageBitmapCallBack(uri);
                            }
                        }
                    });
                }
            }
        });
    }

    protected void showActivateSuccessDialog(Context mContext,String lockAcc,CommonNoticeDialog.CommonConfirmListener commonConfirmListener){
        commonNoticeDialog = new CommonNoticeDialog(mContext);
        String title = "",content = "",confirmContent = "";
        int imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);;
        switch (lockAcc){
            //非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
            case "S":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_112);
                confirmContent = mContext.getString(R.string.sprint11_113);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_reactivated);
                break;
            case "D":
            case "U":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_119);
                confirmContent = mContext.getString(R.string.sprint11_120);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_ivr_call);
                break;
            case "E":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_126);
                confirmContent = mContext.getString(R.string.sprint11_113);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_has_renew);
                break;
            case "W":
                //Id更新失败或者处理中
                title = mContext.getString(R.string.sprint11_127);
                content = mContext.getString(R.string.sprint11_128);
                confirmContent = "";
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_not_renew);
                break;
            default:break;
        }
        commonNoticeDialog.setCommonText(title,content,confirmContent,imgId);
        commonNoticeDialog.setCommonConfirmListener(commonConfirmListener);
        commonNoticeDialog.showWithBottomAnim();
    }

    protected void showAccountStatusDialog(Context mContext,String lockAcc,CommonNoticeDialog.CommonConfirmListener commonConfirmListener){
        commonNoticeDialog = new CommonNoticeDialog(mContext);
        String title = "",content = "",confirmContent = "";
        int imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);;
        switch (lockAcc){
            //非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
            case "S":
                title = mContext.getString(R.string.sprint11_108);
                content = mContext.getString(R.string.sprint11_109);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);
                break;
            case "D":
                title = mContext.getString(R.string.sprint11_117);
                content = mContext.getString(R.string.sprint11_118);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_dormant);
                break;
            case "C":
                title = mContext.getString(R.string.sprint11_114);
                content = mContext.getString(R.string.sprint11_115);
                confirmContent = mContext.getString(R.string.sprint11_116);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_closed);
                break;
            case "U":
                title = mContext.getString(R.string.sprint11_121);
                content = mContext.getString(R.string.sprint11_122);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);
                break;
            case "E":
                /**
                 * {\"codeType\":0,\"partnerNo\":\"96601\",\"requestCounts\":0,\"lockAcc\":\"E\"}"
                 */
                title = mContext.getString(R.string.sprint11_123);
                content = mContext.getString(R.string.sprint11_124);
                confirmContent = mContext.getString(R.string.sprint11_125);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_expired);
                break;
            default:break;
        }
        commonNoticeDialog.setCommonText(title,content,confirmContent,imgId);
        commonNoticeDialog.setCommonConfirmListener(commonConfirmListener);
        commonNoticeDialog.showWithBottomAnim();
    }

    protected void dismissNoticeDialog(){
        if(commonNoticeDialog != null && commonNoticeDialog.isShowing()){
            commonNoticeDialog.dismiss();
        }
    }

    protected void transferToContactSuccess(Activity activity,String transferType,TransferEntity transferEntity){
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
        cardNotifyEntity.orderAmount = transferEntity.orderAmount;
        cardNotifyEntity.paymentMethodDesc = transferEntity.payMethodDesc;
        cardNotifyEntity.orderNo = transferEntity.orderNo;
        cardNotifyEntity.orderDirection = transferEntity.orderDirection;
        cardNotifyEntity.respMsg = transferEntity.respMsg;
        cardNotifyEntity.sceneType = transferEntity.sceneType;
        cardNotifyEntity.orderStatus = transferEntity.orderStatus;
        cardNotifyEntity.subSceneType = transferEntity.subSceneType;
        cardNotifyEntity.startTime = transferEntity.startTime;
        cardNotifyEntity.subject = Constants.SUBJECT_TRANSFER_WW;
        cardNotifyEntity.transferName = transferEntity.payeeName;
        cardNotifyEntity.payerName = transferEntity.payerName;
        cardNotifyEntity.orderDesc = transferEntity.orderDesc;
        cardNotifyEntity.extendProperties = transferEntity.extendProperties;
        cardNotifyEntity.basicExtendProperties = transferEntity.basicExtendProperties;
        mHashMaps.put(Constants.sceneType, transferType);
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        if(cardNotifyEntity.sceneType == 8){
            //请求转账
            AppsFlyerHelper.getInstance().sendRequestTransferEvent();
        }
        ProjectApp.removeTransferTask();
        ActivitySkipUtil.startAnotherActivity(activity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        finish();
    }

    protected void transferSurePaySuccess(Activity activity,String transferType,TransferEntity transferEntity,boolean isFinish) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
        cardNotifyEntity.orderAmount = transferEntity.orderAmount;
        cardNotifyEntity.paymentMethodDesc = transferEntity.payMethodDesc;
        cardNotifyEntity.extendProperties = transferEntity.extendProperties;
        cardNotifyEntity.basicExtendProperties = transferEntity.basicExtendProperties;
        cardNotifyEntity.orderNo = transferEntity.orderNo;
        cardNotifyEntity.logo = transferEntity.logo;
        cardNotifyEntity.startTime = transferEntity.startTime;
        cardNotifyEntity.orderDesc = transferEntity.orderDesc;
        cardNotifyEntity.respMsg = transferEntity.respMsg;
        cardNotifyEntity.directionDesc = transferEntity.directionDesc;
        cardNotifyEntity.orderType = transferEntity.orderType;
        cardNotifyEntity.sceneType = transferEntity.sceneType;
        cardNotifyEntity.subSceneType = transferEntity.subSceneType;
        cardNotifyEntity.transferName = transferEntity.payeeName;
        cardNotifyEntity.orderStatus = transferEntity.orderStatus;
        cardNotifyEntity.orderDirection = transferEntity.orderDirection;
        cardNotifyEntity.orderCurrencyCode = transferEntity.orderCurrencyCode;
        cardNotifyEntity.subject = Constants.SUBJECT_TRANSFER_WW;
        mHashMaps.put(Constants.sceneType, transferType);
        AppsFlyerEntity appsFlyerEntity = UserInfoManager.getInstance().getAppsFlyerEntity();
        String purpose = appsFlyerEntity.purpose;
        if(cardNotifyEntity.sceneType == 8){
            //请求转账
            AppsFlyerHelper.getInstance().sendRequestTransferEvent();
        }
        if(Constants.TYPE_TRANSFER_BT.equals(transferType)){
            if(TextUtils.isEmpty(purpose)){
                //提现
                AppsFlyerHelper.getInstance().sendWithdrawEvent();
            }else{
                //银行转账
                AppsFlyerHelper.getInstance().sendTransferToLocalBank(purpose);
            }
        }else if(Constants.TYPE_TRANSFER_WW.equals(transferType)){
            //钱包转账
            if(!TextUtils.isEmpty(purpose)) {
                AppsFlyerHelper.getInstance().sendP2PEvent("True", purpose);
            }
        }
        String chatId = transferEntity.chatId;
        if(!TextUtils.isEmpty(chatId)){
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_CHAT_SEND_MONEY_SUCCESS,transferEntity));
        }
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        ProjectApp.removeTransferTask();
        ActivitySkipUtil.startAnotherActivity(activity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        if(isFinish) {
            finish();
        }
    }

    protected void confirmPaySuccess(Activity activity, String transferType, CheckOutEntity checkOutEntity,boolean isFromSDK,TransferEntity transferEntity) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
        cardNotifyEntity.orderAmount = transferEntity.orderAmount;
        cardNotifyEntity.payerAmount = transferEntity.payerAmount;
        cardNotifyEntity.paymentMethodDesc = transferEntity.payMethodDesc;
        cardNotifyEntity.orderNo = transferEntity.orderNo;
        cardNotifyEntity.orderDesc = transferEntity.orderDesc;
        cardNotifyEntity.logo = transferEntity.logo;
        cardNotifyEntity.popupInfo = transferEntity.popupInfo;
        cardNotifyEntity.respMsg = transferEntity.respMsg;
        cardNotifyEntity.extendProperties = transferEntity.extendProperties;
        cardNotifyEntity.basicExtendProperties = transferEntity.basicExtendProperties;
        cardNotifyEntity.startTime = transferEntity.startTimeStr;
        cardNotifyEntity.merchantId = transferEntity.merchantId;
        cardNotifyEntity.outTradeNo = transferEntity.outTradeNo;
        cardNotifyEntity.orderStatus = transferEntity.orderStatus;
        cardNotifyEntity.orderDirection = transferEntity.orderDirection;
        cardNotifyEntity.orderType = transferEntity.orderType;
        cardNotifyEntity.merchantName = transferEntity.merchantName;
        cardNotifyEntity.sceneType = transferEntity.sceneType;
        cardNotifyEntity.subSceneType = transferEntity.subSceneType;
        cardNotifyEntity.shareDesc = transferEntity.shareDesc;
        cardNotifyEntity.remark = transferEntity.remark;
        cardNotifyEntity.discountType = transferEntity.discountType;
        cardNotifyEntity.voucherPinCode = transferEntity.voucherPinCode;
        cardNotifyEntity.bankName = checkOutEntity.brandName;
        cardNotifyEntity.discountTransFees = checkOutEntity.discountTransFees;
        cardNotifyEntity.destinationCountry = checkOutEntity.destinationCountry;
        cardNotifyEntity.downloadFile = transferEntity.downloadFile;
        cardNotifyEntity.isShowEInvoice = transferEntity.isShowEInvoice;
        mHashMaps.put(Constants.sceneType, transferType);
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        AppsFlyerEntity appsFlyerEntity = UserInfoManager.getInstance().getAppsFlyerEntity();
        switch (cardNotifyEntity.sceneType){
            case 15:
                AppsFlyerHelper.getInstance().sendVoucherEvent(appsFlyerEntity.voucherBrandName);
                break;
            case 5:
                AppsFlyerHelper.getInstance().sendMobileTopupEvent(appsFlyerEntity.voucherBrandName);
                break;
            case 23:
                AppsFlyerHelper.getInstance().sendIntMobileTopupEvent(appsFlyerEntity.merchantName,appsFlyerEntity.desitinationCode);
                break;
            case 11:
                AppsFlyerHelper.getInstance().sendImrTransferEvent(appsFlyerEntity.purpose,appsFlyerEntity.desitinationCode);
                break;
        }
        if (isFromSDK) {
            mHashMaps.put(Constants.IS_FROM_SDK, true);
            finish();
        } else {
            if (cardNotifyEntity.sceneType == 15 || cardNotifyEntity.sceneType == 5 || cardNotifyEntity.sceneType == 23) {
                ProjectApp.removeAllTaskExcludeMainStack();
            } else if (cardNotifyEntity.sceneType == 11) {
                ProjectApp.removeImrSendMoneyTask();
                ProjectApp.removeImrAddBeneTask();
            } else if (cardNotifyEntity.sceneType == 6 || cardNotifyEntity.sceneType == 27 || cardNotifyEntity.sceneType == 28 || cardNotifyEntity.sceneType == 29) {
                mHashMaps.put(Constants.sceneType, Constants.history_detail);
                ProjectApp.removeAllTaskExcludeMainStack();
            } else {
                ProjectApp.removeScanTask();
            }
        }
        ActivitySkipUtil.startAnotherActivity(activity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);

        finish();
    }

    public void splitBillSuccess(Activity activity, String transferType,TransferEntity transferEntity) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
        cardNotifyEntity.orderAmount = transferEntity.tradeAmount;
        cardNotifyEntity.orderDesc = transferEntity.orderDesc;
        cardNotifyEntity.paymentMethodDesc = transferEntity.payMethodDesc;
        cardNotifyEntity.orderNo = transferEntity.orderNo;
        cardNotifyEntity.respMsg = transferEntity.respMsg;
        cardNotifyEntity.startTime = transferEntity.createTime;
        cardNotifyEntity.extendProperties = transferEntity.extendProperties;
        cardNotifyEntity.basicExtendProperties = transferEntity.basicExtendProperties;
        cardNotifyEntity.orderCurrencyCode = transferEntity.currencyCode;
        cardNotifyEntity.orderStatus = transferEntity.orderStatus;
        cardNotifyEntity.orderDirection = transferEntity.orderDirection;
        cardNotifyEntity.sceneType = transferEntity.sceneType;
        cardNotifyEntity.subSceneType = transferEntity.subSceneType;
        cardNotifyEntity.orderType = transferEntity.orderType;
        mHashMaps.put(Constants.sceneType, transferType);
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        AppsFlyerHelper.getInstance().sendBillSplitEvent("True","");
        finish();
        ProjectApp.removeSplitTask();
        ActivitySkipUtil.startAnotherActivity(activity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
    }



    public void showCommonErrorDialog(String errorMsg) {
        BottomDialog bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    public void showNafathErrorDialog(String failType){
        ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
        String errorTitle = "";
        String errorContent = "";
        int errorImgId = -1;
        if("1".equals(failType)){
            //未成年
            errorTitle = getString(R.string.sprint11_97);
            errorContent = getString(R.string.sprint11_98);
            errorImgId = R.drawable.l_error_reg_id;
        }else if("2".equals(failType)){
            //id过期
            errorTitle = getString(R.string.analytics_32);
            errorContent = getString(R.string.RENEW_2);
            errorImgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_expired_id);
        }else if("3".equals(failType)){
            //id不匹配
            errorTitle = getString(R.string.sprint11_99);
            errorContent = getString(R.string.sprint11_100);
            errorImgId = R.drawable.l_error_reg_id;
        }else if("080121".equals(failType)){
            //电话和id不匹配
            errorTitle = getString(R.string.sprint11_95);
            errorContent = getString(R.string.sprint11_96);
            errorImgId = R.drawable.l_error_mobile_number;
        }else if("4".equals(failType)){
            //修改手机号码成功
            errorTitle = getString(R.string.sprint20_38);
            errorContent = getString(R.string.sprint20_39);
            errorImgId = R.drawable.l_change_phone_success;
        }
        errorBottomDialog.setErrorMsg(errorTitle,errorContent,errorImgId);
        errorBottomDialog.showWithBottomAnim();
    }

    public void contactHelp(){
        if (!isGranted_(Manifest.permission.CALL_PHONE)) {
            PermissionInstance.getInstance().getPermission(this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    SdkShareUtil.callPhoneNumber(mContext, Constants.call_phone);
                }

                @Override
                public void rejectPermission() {

                }
            }, Manifest.permission.CALL_PHONE);
        } else {
            SdkShareUtil.callPhoneNumber(mContext, Constants.call_phone);
        }
    }
}
