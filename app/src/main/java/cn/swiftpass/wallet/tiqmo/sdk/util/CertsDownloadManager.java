package cn.swiftpass.wallet.tiqmo.sdk.util;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomProgressDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 证书下载
 * 1.7月版证书下载功能 只涉及到Idv/FRP/其他webview证书下载的证书
 * 2.Pa注册/绑卡/忘记密码 需要涉及到idv/frp证书
 * 3.webview页面需要一些域名检测的证书列表(银联互通 最新消息里头的一下外部重定向网页校验)
 * 4.需要用到下载证书的地方调用 有两个方法 1）异步返回 2）同步返回
 * 5.tnc 参与证书校验 不参与到网络异常的问题
 * 6.证书md5校验
 */
public class CertsDownloadManager {
    private static CertsDownloadManager instance;
    private static final Object INSTANCE_LOCK = new Object();
    private static final String TAG = "CertsDownloadManager";
    private Certificate[] mCertificates;
    private CustomProgressDialog progressDialog;
    /**
     * 下载压缩证书的连接
     */
//    private String DOWNLOADCERTURL = "http://wallet-saas-dev.wepayez.com/tiqmo/cms/cert/download?partnerNo=96601&domainInfo=wallet-saas-dev.wepayez.com";
    private String DOWNLOADCERTURL = BuildConfig.ServerUrl + "cms/cert/download?partnerNo=" + BuildConfig.partnerNo + "&domainInfo=" + BuildConfig.hostUrl;

//    public static CertsDownloadManager getInstance() {
//        if (instance == null) {
//            synchronized (INSTANCE_LOCK) {
//                if (instance == null) {
//                    instance = new CertsDownloadManager();
//                }
//            }
//        }
//        return instance;
//    }

    private CertsDownloadManager() {
    }

    private static class CertsDownloadHolder {
        public static CertsDownloadManager certsDownloadInstance = new CertsDownloadManager();
    }

    public static CertsDownloadManager getInstance() {
        return CertsDownloadManager.CertsDownloadHolder.certsDownloadInstance;
    }

    /**
     * 只有当证书下载成功了 才可以直接调用这个方法  主要是为了解决activity传参无法序列化问题 需要内存存放
     *
     * @return
     */
    public Certificate[] getDownloadCerts() {

        return mCertificates;
    }


    /**
     * 证书初始化，提前下载证书配置
     */
    public void initCertificate(Context mcontext) {
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        String certZipName = SpUtils.getInstance().getCertZipName();
        if (TextUtils.isEmpty(certZipName)) {
            loadCertificates(mcontext, false, null);
            return;
        }
        if (configEntity != null) {
            //如果证书压缩包名称更新  则重新下载证书压缩包
            if (!TextUtils.isEmpty(certZipName)) {
                if (!certZipName.equals(configEntity.certZipName)) {
                    loadCertificates(mcontext, false, null);
                }
            }
        }

    }

    /**
     * 异步拉取证书
     *
     * @param mContext
     * @param showDialog             是否显示dialog阻塞
     * @param onCertDownLoadListener
     */
    public void loadCertificates(Context mContext, boolean showDialog, OnCertDownLoadListener onCertDownLoadListener) {

        if (mCertificates != null && mCertificates.length > 0) {
            LogUtils.i(TAG, "Certificates ALREADY EXIT SUCCESS BACK");
            onCertDownLoadListener.onCertsDownLoaFinish(true, mCertificates);
        } else {
            if (!NetworkUtil.isNetworkConnected(mContext)) {
                LogUtils.i(TAG, "Certificates NOT EXIT NETWORK ERROR");
                if (onCertDownLoadListener != null) {
                    onCertDownLoadListener.onCertsDownLoaFinish(false, null);
                }
                return;
            }
            LogUtils.i(TAG, "Certificates NOT EXIT START DOWN");
            new DownLoadCertsTask(onCertDownLoadListener, mContext, showDialog).execute();
        }
    }

    /**
     * 耗时任务读取通讯录
     */
    private class DownLoadCertsTask extends AsyncTask<Void, Void, Certificate[]> {
        private OnCertDownLoadListener onCertDownLoadListener;

        private boolean showDialog;
        private Context mContext;

        public DownLoadCertsTask(OnCertDownLoadListener onCertDownLoadListener, Context mContext, boolean showDialogIn) {
            this.onCertDownLoadListener = onCertDownLoadListener;
            this.showDialog = showDialogIn;
            this.mContext = mContext;
        }

        @Override
        protected Certificate[] doInBackground(Void... voids) {
            LogUtils.i(TAG, " down load doInBackground ->" + Thread.currentThread().getName());
            return downLoadCertificates();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtils.i(TAG, "down load onPreExecute->");
            //看需不需要加载框显示
            if (showDialog && mContext != null) {
//                showDialogNotCancel(mContext);
            }

        }

        @Override
        protected void onPostExecute(Certificate[] contactEntities) {
            super.onPostExecute(contactEntities);
            if (contactEntities != null && contactEntities.length > 0) {
                LogUtils.i(TAG, "down load onPostExecute->");
            }
            if (onCertDownLoadListener != null) {
                //证书下载成功 缓存到内存中
                mCertificates = contactEntities;
                onCertDownLoadListener.onCertsDownLoaFinish(contactEntities != null, contactEntities);
                if (showDialog && mContext != null) {
                    dismissDialog();
                }
            }
        }
    }

    /**
     * 加锁 阻塞 防止并发请求
     *
     * @return
     */
    private synchronized Certificate[] downLoadCertificates() {
        if (mCertificates != null && mCertificates.length > 0) {
            return mCertificates;
        }
        LogUtils.i(TAG, "downLoadCertificates start ->"+DOWNLOADCERTURL);

        //1.通过证书下载地址进行证书下载
        OkHttpClient okHttpClient = getHttpsClient();
        Request request = new Request.Builder()
                .url(DOWNLOADCERTURL)
                .addHeader("Connection", "close")
                .build();
        List<Certificate> certificates = new ArrayList<>();
        ZipInputStream zin = null;
        FileOutputStream out = null;
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful() && response.body() != null) {
                zin = new ZipInputStream(response.body().byteStream());
                ZipEntry entry;
                //如果entry不为空，并不在同一个目录下
                while (zin != null && ((entry = zin.getNextEntry()) != null)) {
                    //解压出的文件路径
                    if (!entry.isDirectory()) {
//                        String fileName = entry.getName();

                        String outFileName =
                                AndroidUtils.validateFilenameInDir(entry.getName(), ".");
                        //byte[] fileContent = entry.getExtra();
                        LogUtils.i(TAG, "fileName: " + outFileName);

                        File file = new File(ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + outFileName);
                        file.createNewFile();
                        out = new FileOutputStream(file);
                        int len;
                        byte[] buffer = new byte[1024];
                        while ((len = zin.read(buffer)) != -1) {
                            out.write(buffer, 0, len);
                            out.flush();
                        }
//                        out.close();
//                        try {
//                            // 获取当前条目的字节数组
//                            byte[] data = getByte(zin);
//                            //这里要做异常捕获 防止多张证书 其中有些证书格式不对 直接异常 期待的是 格式正确的证书都应该读取到内存中
//                            InputStream is = new ByteArrayInputStream(data);
//                            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
////                            X509Certificate certificate = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(is);
//                            Certificate certificate = certificateFactory.generateCertificate(is);
//                            certificates.add(certificate);
//                        } catch (CertificateException e) {
//                            if (BuildConfig.isLogDebug) {
//                                LogUtils.d("errorMsg", "---"+e+"---");
//                            }
//                        }
                        zin.closeEntry();
                        LogUtils.i(TAG, "downLoadCertificates success->");
                    }
                }
//                zin.close();
            } else {
//                //网络或者解压缩问题失败
                LogUtils.i(TAG, "downLoadCertificates failed->");
            }
        } catch (Exception e) {
            try {
                if (out != null) {
                    out.close();
                }
                if (zin != null) {
                    zin.close();
                }
            } catch (Exception ex) {
                if (BuildConfig.DEBUG) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (zin != null) {
                    zin.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        mCertificates = certificates.toArray(new Certificate[certificates.size()]);
        return mCertificates;
    }

    private OkHttpClient getHttpsClient() {
        OkHttpClient.Builder okhttpClient = new OkHttpClient().newBuilder();
        okhttpClient.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession sslSession) {
                if(BuildConfig.ServerUrl.contains(hostname)) {
                    return true;
                }
                return false;
            }
        });
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] x509Certificates,
                    String s) throws java.security.cert.CertificateException {
            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] x509Certificates,
                    String s) throws java.security.cert.CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
//为OkHttpClient设置sslSocketFactory
            okhttpClient.sslSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return okhttpClient.build();
    }


//    /**
//     * 需要上传的header和body参数字段
//     *
//     * @param mBodyParams
//     * @return
//     */
//    private JSONObject makePostData(JSONObject mBodyParams) {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            //摘要，签名处理  body加密放在加密拦截器处理
//            jsonObject.put("header", HeaderUtils.getHeaders());
//            jsonObject.put("body", mBodyParams);
//            LogUtils.i(TAG, "requestparams: " + jsonObject.toString());
//        } catch (Exception e) {
//            LogUtils.e(TAG, Log.getStackTraceString(e));
//        }
//        return jsonObject;
//    }


    /**
     * 获取条目byte[]字节
     *
     * @param zis
     * @return
     */
    public byte[] getByte(InflaterInputStream zis) {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] temp = new byte[1024];
            byte[] buf = null;
            int length = 0;
            while ((length = zis.read(temp, 0, 1024)) != -1) {
                bout.write(temp, 0, length);
            }
            buf = bout.toByteArray();
            bout.close();
            return buf;
        } catch (IOException e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
            return null;
        }
    }


    public void dismissDialog() {
        if (progressDialog == null) {
            return;
        }
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
    }


    public void showDialogNotCancel(Context context) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.common_34),R.style.CustomProgressDialog);
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        } else {
            if (progressDialog != null) {
                try {
                    Activity activity = (Activity) context;
                    if (!activity.isFinishing()) {
                        progressDialog.setCancel(false);
                        progressDialog.show();
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, e.getMessage());
                }

            }
        }

    }


    public interface OnCertDownLoadListener {
        void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates);
    }


//    public static class UrlData extends BaseEntity {
//        /**
//         * value : http://mbasit1.ftcwifi.com/json/certs.zip
//         */
//        private String value;
//
//        public void setValue(String value) {
//            this.value = value;
//        }
//
//        public String getValue() {
//            return value;
//        }
//    }

}
