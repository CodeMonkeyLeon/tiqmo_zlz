package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardSetPinPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class SetPinConfirmActivity extends BaseCompatActivity<CardKsaContract.SetCardPinPresener> implements CardKsaContract.SetCardPinView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    private String proxyCardNo;
    private KsaCardEntity ksaCardEntity;

    private RiskControlEntity riskControlEntity;
    private static SetPinConfirmActivity setPinConfirmActivity;

    public static SetPinConfirmActivity getSetPinConfirmActivity() {
        return setPinConfirmActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setPinConfirmActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_set_pin_confirm;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setPinConfirmActivity = this;
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        ivBack.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_card_close));
        if(getIntent() != null && getIntent().getExtras() != null) {
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            if(ksaCardEntity != null){
                proxyCardNo = ksaCardEntity.proxyCardNo;
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_confirm})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                List<String> additionalData = new ArrayList<>();
                if(ksaCardEntity != null){
                    additionalData.add(ksaCardEntity.maskedCardNo);
                }
                mPresenter.getOtpType("ActionType", "TCA08", additionalData);
                break;
            default:break;
        }
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(SetPinConfirmActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_SET_PIN);
        mHashMap.put(Constants.proxyCardNo, proxyCardNo);
        mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_KSA_CARD_SET_PIN);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    private void requestTransfer(){
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        ActivitySkipUtil.startAnotherActivity(SetPinConfirmActivity.this, GetCardPwdOneActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void setKsaCardPinSuccess(Void result) {

    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {

    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {

    }

    @Override
    public void setKsaCardChannelSuccess(Void result) {

    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public CardKsaContract.SetCardPinPresener getPresenter() {
        return new CardSetPinPresenter();
    }
}
