package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.TransferHistoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.FilterHistoryDialogFragment;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.AccountInfoContract;
import cn.swiftpass.wallet.tiqmo.module.setting.dialog.DeActNoticeDialog;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.AccountInfoPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AccountInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.AsteriskPasswordTransformationMethod;
import cn.swiftpass.wallet.tiqmo.widget.adapter.ItemTouchHelperListener;
import cn.swiftpass.wallet.tiqmo.widget.adapter.SimpleItemTouchHelperCallback;
import cn.swiftpass.wallet.tiqmo.widget.dialog.DeactivationLoginDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class AccountDetailActivity extends BaseCompatActivity<AccountInfoContract.Presenter> implements AccountInfoContract.View, ItemTouchHelperListener {

    public static final int filter_request = 1001;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.tv_wallet_account)
    TextView tvWalletAccount;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_bank_number)
    TextView tvBankNumber;
    @BindView(R.id.iv_copy_bank)
    ImageView ivCopyBank;
    @BindView(R.id.iv_money_see)
    ImageView ivMoneySee;
    @BindView(R.id.ry_transaction_history)
    RecyclerView ryTransactionHistory;
    @BindView(R.id.sw_account_detail)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_stop_user)
    TextView tvStopUser;

    private boolean isHideMoney = false;

    private TransferHistoryAdapter transferHistoryAdapter;
    public List<TransferHistoryEntity> historyEntityList = new ArrayList<>();
    public List<TransferHistoryListEntity> historyListEntityList = new ArrayList<>();

    private int pageSize = 30;
    private int currentPage = 1;
    private FilterEntity filterEntity;
    public List<String> typeList = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();
    private HashMap<Integer, String> selectedArray = new HashMap<>();

    private String startDateTime = "", endDateTime = "";
    private String accountNo = "";
    private StatusView historyStatusView;
    private String direction, orderNo;

    private FilterListEntity filterListEntity;

    private int historyPosition;
    private SimpleItemTouchHelperCallback itemTouchHelperCallback;
    ItemTouchHelper historyTouchHelper;

    private double balanceMoney;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_account_detail;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.getAccountInfo(Constants.ACCOUNT_TYPE_BASE);
    }

    private void initFilter(List<FilterCodeEntity> filterList) {
        if (filterList != null && filterList.size() > 0) {
            scFilter.setVisibility(View.VISIBLE);
            llFilter.removeAllViews();
            int size = filterList.size();
            for (int i = 0; i < size; i++) {
                final FilterCodeEntity filterCodeEntity = filterList.get(i);
                if (filterCodeEntity != null) {
                    String title = filterCodeEntity.title;
                    final View view = LayoutInflater.from(mContext).inflate(R.layout.item_top_filter, null);
                    TextView tvTopFilter = view.findViewById(R.id.tv_top_filter);
                    ImageView ivDelete = view.findViewById(R.id.iv_filter_delete);
                    if (!TextUtils.isEmpty(title)) {
                        tvTopFilter.setText(title);
                    }
                    ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int type = filterCodeEntity.type;
                            switch (type) {
                                case 1:
                                    tradeType.remove(filterCodeEntity.value);
                                    categoryList.remove(filterCodeEntity.title);
                                    // 键和值
                                    Integer key = null;
                                    String value = null;
                                    // 获取键值对的迭代器
                                    Iterator it = selectedArray.entrySet().iterator();
                                    while (it.hasNext()) {
                                        HashMap.Entry entry = (HashMap.Entry) it.next();
                                        key = (Integer) entry.getKey();
                                        value = (String) entry.getValue();
                                        if (filterCodeEntity.value.equals(value)) {
                                            it.remove();
                                        }
                                        System.out.println("key:" + key + "---" + "value:" + value);
                                    }
                                    break;
                                case 2:
                                    typeList.remove(filterCodeEntity.value);
                                    break;
                                case 3:
                                    startDateTime = "";
                                    endDateTime = "";
                                    filterEntity.showStartDate = "";
                                    filterEntity.showEndDate = "";
                                    filterEntity.startDate = "";
                                    filterEntity.endDate = "";
                                    break;
                                default:
                                    break;
                            }
                            llFilter.removeView(view);

                            currentPage = 1;
                            orderNo = "";
                            direction = "";
                            showProgress(true);
                            mPresenter.getHistoryList("", accountNo, typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
                        }
                    });
                    llFilter.addView(view);
                }
            }
        } else {
            scFilter.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvFilter.setEnabled(true);
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.Profile_BAD_0015_L_1);

        isHideMoney = SpUtils.getInstance().getHidePwd();
        if (isHideMoney) {
            ivMoneySee.setImageResource(R.drawable.d_money_hide);
            tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        } else {
            ivMoneySee.setImageResource(R.drawable.d_money_show);
            tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }

        userInfoEntity = getUserInfoEntity();
        balanceMoney = userInfoEntity.getBalanceSumMoney();
        tvStopUser.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG ); //下划线
        tvStopUser.getPaint().setAntiAlias(true);//抗锯齿
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                mPresenter.getAccountInfo(Constants.ACCOUNT_TYPE_BASE);
//            }
//        });
        filterEntity = new FilterEntity();
        swipeRefreshLayout.setColorSchemeResources(R.color.color_B00931);

        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
        LocaleUtils.viewRotationY(mContext, headCircle, scFilter, llFilter, ivBack);

        View noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        historyStatusView = new StatusView.Builder(mContext, ryTransactionHistory).setNoContentMsg(getString(R.string.History_filter_13))
                .setNoContentView(noHistoryView).build();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        ryTransactionHistory.setLayoutManager(mLayoutManager);
        transferHistoryAdapter = new TransferHistoryAdapter(historyEntityList, AccountDetailActivity.this);
        itemTouchHelperCallback = new SimpleItemTouchHelperCallback(this, 1);
        historyTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        historyTouchHelper.attachToRecyclerView(ryTransactionHistory);
        transferHistoryAdapter.bindToRecyclerView(ryTransactionHistory);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAccountInfo(Constants.ACCOUNT_TYPE_BASE);
            }
        });

        transferHistoryAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                currentPage += 1;
                direction = "down";
                mPresenter.getHistoryList("", accountNo, typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
            }
        }, ryTransactionHistory);

        transferHistoryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
                if (transferHistoryEntity != null) {
                    mPresenter.getHistoryDetail(transferHistoryEntity.orderNo, transferHistoryEntity.orderType,"");
                }
            }
        });
    }

    private void showDeactivationLoginDialog(){
        DeactivationLoginDialog deactivationLoginDialog = new DeactivationLoginDialog(mContext);
        deactivationLoginDialog.showWithBottomAnim();
    }

    @OnClick({R.id.iv_back, R.id.iv_copy_bank, R.id.tv_filter, R.id.iv_money_see, R.id.tv_add_money,R.id.tv_stop_user})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_stop_user:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (balanceMoney > 0) {
                    DeActNoticeDialog dialog = DeActNoticeDialog.getInstance();
                    dialog.setBackToHomeClickListener(new DeActNoticeDialog.OnBackToHomeClickListener() {
                        @Override
                        public void onBTHClick() {
                            ActivitySkipUtil.clearTaskToMainActivity(AccountDetailActivity.this);
                        }
                    });
                    dialog.showNow(getSupportFragmentManager(), "DeActNotice");
                } else {
                    showDeactivationLoginDialog();
                }
                break;
            case R.id.tv_add_money:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            case R.id.iv_back:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
                break;
            case R.id.iv_money_see:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (isHideMoney) {
                    //如果已经隐藏了金额  则显示
                    isHideMoney = false;
                    ivMoneySee.setImageResource(R.drawable.d_money_show);
                    tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    isHideMoney = true;
                    ivMoneySee.setImageResource(R.drawable.d_money_hide);
                    tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
                }
                SpUtils.getInstance().setHidePwd(isHideMoney);
                break;
            case R.id.tv_filter:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }

                if (filterListEntity != null) {
                    showFilterDialog(filterListEntity);
                } else {
                    AppClient.getInstance().getFilterList(new ResultCallback<FilterListEntity>() {
                        @Override
                        public void onResult(FilterListEntity mFilterListEntity) {
                            filterListEntity = mFilterListEntity;
                            showFilterDialog(filterListEntity);
                        }

                        @Override
                        public void onFailure(String errorCode, String errorMsg) {

                        }
                    });
                }

                break;
            case R.id.iv_copy_bank:
                if (ButtonUtils.isFastDoubleClick(-1, 2500)) {
                    return;
                }
                String bankNumber = tvBankNumber.getText().toString().trim();
                AndroidUtils.setClipboardText(mContext, bankNumber, true);
                showTipDialog(getString(R.string.iban_copy));
                break;
            default:
                break;
        }
    }

    private void showFilterDialog(FilterListEntity filterListEntity) {
        FilterHistoryDialogFragment fragment = FilterHistoryDialogFragment.newInstance(filterEntity, filterListEntity);
        fragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void getAccountInfoSuccess(AccountInfoEntity accountInfoEntity) {
        swipeRefreshLayout.setRefreshing(false);
        tvBankName.setText(accountInfoEntity.accountName);
        String ibanNum = userInfoEntity.ibanNum;
        if (!TextUtils.isEmpty(ibanNum)) {
            tvBankNumber.setText(AndroidUtils.changeBanNum(ibanNum));
        }
        tvAmount.setText(accountInfoEntity.getBalance());
        tvCurrency.setText(LocaleUtils.getCurrencyCode(accountInfoEntity.currencyCode));
        accountNo = accountInfoEntity.accountNo;

//        currentPage = 1;
//        orderNo = "";
//        direction = "";
//        mPresenter.getHistoryList("", accountNo, typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
    }

    @Override
    public void getAccountInfoFail(String errorCode, String errorMsg) {
        swipeRefreshLayout.setRefreshing(false);
        showTipDialog(errorMsg);
//        showErrorDialog(errorMsg);
    }

    @Override
    public void getHistoryListSuccess(TransferHistoryMainEntity transferHistoryMainEntity) {
        showProgress(false);
        List<TransferHistoryEntity> tempHistoryList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(false);
        if (transferHistoryMainEntity != null) {
            List<TransferHistoryListEntity> historyListEntityList = transferHistoryMainEntity.transactionHistorys;
            int size = historyListEntityList.size();
            for (int i = 0; i < size; i++) {
                TransferHistoryListEntity historyListEntity = historyListEntityList.get(i);
                if (historyListEntity != null) {
                    tempHistoryList.addAll(historyListEntity.transactionListInfos);
                }
            }
        }

        if (currentPage == 1) {
            ryTransactionHistory.scrollToPosition(0);
            historyEntityList.clear();
            if (tempHistoryList.size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                transferHistoryAdapter.setDataList(tempHistoryList);
            }
        } else {
            if (tempHistoryList.size() == 0) {
                transferHistoryAdapter.loadMoreEnd(false);
            } else {
                transferHistoryAdapter.addData(tempHistoryList);
                if (tempHistoryList.size() < pageSize) {
                    transferHistoryAdapter.loadMoreEnd(false);
                } else {
                    transferHistoryAdapter.loadMoreComplete();
                }
            }
        }
        historyEntityList.addAll(tempHistoryList);
        if (historyEntityList.size() > 0) {
            TransferHistoryEntity lastEntity = historyEntityList.get(historyEntityList.size() - 1);
            if (lastEntity != null) {
                orderNo = lastEntity.orderNo;
            }
        }
    }

    @Override
    public void getHistoryListFail(String errorCode, String errorMsg) {
        showProgress(false);
        swipeRefreshLayout.setRefreshing(false);
        showTipDialog(errorMsg);
        if (currentPage == 1) {
            showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
        }
    }

    @Override
    public void getHistoryDetailSuccess(TransferHistoryDetailEntity transferHistoryDetailEntity) {
        AndroidUtils.jumpToTransferDetail(AccountDetailActivity.this, transferHistoryDetailEntity, "", true);
    }

    @Override
    public void getHistoryDetailFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AccountInfoContract.Presenter getPresenter() {
        return new AccountInfoPresenter();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (tvFilter != null) {
            tvFilter.setEnabled(true);
        }
        if (requestCode == filter_request) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    filterEntity = (FilterEntity) data.getSerializableExtra("filterEntity");
                    if (filterEntity != null) {
                        String showStartDate = "", showEndDate = "";
                        startDateTime = filterEntity.startDate;
                        showStartDate = filterEntity.showStartDate;
                        endDateTime = filterEntity.endDate;
                        showEndDate = filterEntity.showEndDate;
                        typeList = filterEntity.paymentType;
                        tradeType = filterEntity.tradeType;
                        categoryList = filterEntity.categoryList;
                        selectedArray = filterEntity.selectedArray;
                        List<FilterCodeEntity> filterList = new ArrayList<>();

                        if (TextUtils.isEmpty(startDateTime) && !TextUtils.isEmpty(endDateTime)) {
                            showStartDate = "...";
                        }

                        if (!TextUtils.isEmpty(startDateTime) && TextUtils.isEmpty(endDateTime)) {
                            showEndDate = getString(R.string.History_filter_14);
                        }

                        if (!TextUtils.isEmpty(startDateTime) || !TextUtils.isEmpty(endDateTime)) {
                            FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                            filterCodeEntity.type = 3;
                            filterCodeEntity.title = showStartDate + "-" + showEndDate;
                            filterList.add(filterCodeEntity);
                        }

                        int typeSize = typeList.size();
                        for (int i = 0; i < typeSize; i++) {
                            FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                            filterCodeEntity.type = 2;
                            String type = typeList.get(i);
                            filterCodeEntity.value = type;
                            if ("P".equals(type)) {
                                filterCodeEntity.title = getString(R.string.History_filter_5);
                            } else if ("I".equals(type)) {
                                filterCodeEntity.title = getString(R.string.History_filter_6);
                            }
                            filterList.add(filterCodeEntity);
                        }

                        int categorySize = categoryList.size();
                        for (int i = 0; i < categorySize; i++) {
                            FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                            filterCodeEntity.type = 1;
                            filterCodeEntity.title = categoryList.get(i);
                            filterCodeEntity.value = tradeType.get(i);
                            filterList.add(filterCodeEntity);
                        }

                        initFilter(filterList);
                        currentPage = 1;
                        orderNo = "";
                        direction = "";
                        showProgress(true);
                        mPresenter.getHistoryList("", accountNo, typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
                    }
                }
            }
        }
    }

    @Override
    public void deleteTransferHistorySuccess(Void result) {
        if (transferHistoryAdapter != null) {
            //移除数据
            transferHistoryAdapter.getDataList().remove(historyPosition);
            transferHistoryAdapter.notifyDataSetChanged();
            if (transferHistoryAdapter.getDataList().size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void deleteTransferHistoryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDissmiss(int position, int type) {
        historyPosition = position;
        TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
        if (transferHistoryEntity != null) {
            mPresenter.deleteTransferHistory(transferHistoryEntity.orderNo);
        }
    }
}
