package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.gc.gcchat.GcChatGetSessionsRequest;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.gc.gcchat.callback.GcChatSimpleCallback;
import com.gc.gcchat.callback.GetSessionListCallback;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.CardIntroductionActivity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.NotificationCenterAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.RequestCenterContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgBizParamEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgContentEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.RequestCenterListPresenter;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.GcChatErrorUtils;
import cn.swiftpass.wallet.tiqmo.support.chat.UserUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.adapter.ItemTouchHelperListener;
import cn.swiftpass.wallet.tiqmo.widget.adapter.SimpleItemTouchHelperCallback;
import cn.swiftpass.wallet.tiqmo.widget.cardmanager.NotifyView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CardExpireNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.NoBalanceDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class RequestCenterActivity extends BaseCompatActivity<RequestCenterContract.RequestCenterListPresenter> implements RequestCenterContract.RequestCenterListView, ItemTouchHelperListener {
    protected static final float FLIP_DISTANCE = 50;

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.ll_split)
    LinearLayout llSplit;
    @BindView(R.id.tv_page)
    TextView tvPage;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_recyclerview)
    LinearLayout llRecyclerview;
    @BindView(R.id.rl_request_center)
    NotifyView rlRequest;
    @BindView(R.id.rl_notification)
    RecyclerView rlNotification;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;

    private int currentPosition = 0;

    private List<MsgEntity> msgList = new ArrayList<>();

    private NotificationCenterAdapter notificationCenterAdapter;

    private String sendType;

    private StatusView statusView;
    private StatusView notifyStatusView;
    private RequestCenterEntity requestCenterEntity;
    private boolean removeItem;

    private SimpleItemTouchHelperCallback itemTouchHelperCallback;
    ItemTouchHelper requestTouchHelper;
    private int notifyPosition;

    private int pageSize = 30;
    private int currentPage = 1;
    private LinearLayoutManager linearLayoutManager;
    private String payerName;

    private Handler mHandler;
    private TextView tvError;
    private BottomDialog requestDialog;

//    private GestureDetector mDetector;

    private NoBalanceDialog noBalanceDialog;

    private String orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode, transFees, vat;

    private LinkedList<RequestCenterEntity> requestList = new LinkedList<>();
    private LinkedHashMap<String, RequestCenterEntity> requestMap = new LinkedHashMap<>();

    private RiskControlEntity riskControlEntity;

    private static RequestCenterActivity requestCenterActivity;
    private BottomDialog bottomDialog;


    public static RequestCenterActivity getRequestCenterActivity() {
        if (requestCenterActivity == null) {
            requestCenterActivity = new RequestCenterActivity();
        }
        return requestCenterActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_notification_center;
    }

    private void setSize() {
        if (tvPage == null) {
            return;
        }
        if (requestList.size() == 0) {
            tvPage.setVisibility(View.GONE);
        } else {
            tvPage.setVisibility(View.VISIBLE);
            tvPage.setText(Spans.builder()
                    .text(currentPosition + 1 + "").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf")).size(14)
                    .text("/" + requestList.size()).color(mContext.getColor(R.color.color_a3a3a3)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf")).size(8)
                    .build());
        }
    }

    private void showCardExpiredDialog(String expireContent, MsgBizParamEntity msgBizParamEntity) {
        String proxyCardNo = msgBizParamEntity.orderNo;
        CardExpireNoticeDialog cardExpireNoticeDialog = new CardExpireNoticeDialog(mContext);
        cardExpireNoticeDialog.setProxyCardNo(proxyCardNo, expireContent);
        cardExpireNoticeDialog.setCardHideNumber(msgBizParamEntity.maskedCardNo, msgBizParamEntity.cardHolderName);
        cardExpireNoticeDialog.setButtonListener(new CardExpireNoticeDialog.ButtonListener() {
            @Override
            public void copyCardNo(String cardNo) {
                AndroidUtils.setClipboardText(mContext, cardNo, true);
                showTipDialog(getString(R.string.DigitalCard_87));
            }

            @Override
            public void renewCard() {
                ActivitySkipUtil.startAnotherActivity(RequestCenterActivity.this, CardIntroductionActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void keepVirtual() {
                ActivitySkipUtil.startAnotherActivity(RequestCenterActivity.this, CardIntroductionActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                cardExpireNoticeDialog.dismiss();
            }

            @Override
            public void showError(String errorMsg) {
                showTipDialog(errorMsg);
            }
        });
        cardExpireNoticeDialog.showWithBottomAnim();
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        requestCenterActivity = this;
        showProgress(true);
        setDarkBar();
        tvTitle.setText(R.string.request_transfer_9);

        View noContentView = LayoutInflater.from(mContext).inflate(R.layout.view_no_request_list, null);
        View noNotifyView = LayoutInflater.from(mContext).inflate(R.layout.view_no_notification_list, null);
        statusView = new StatusView.Builder(mContext, rlRequest)
                .setNoContentView(noContentView).build();
        notifyStatusView = new StatusView.Builder(mContext, swipeRefreshLayout)
                .setNoContentView(noNotifyView).build();

        swipeRefreshLayout.setColorSchemeResources(R.color.color_B00931);
        swipeRefreshLayout.setRefreshing(false);

        initRecy();

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 12f);
        rlNotification.addItemDecoration(MyItemDecoration.createVertical(getColor(R.color.transparent), ryLineSpace));
        rlNotification.setLayoutManager(linearLayoutManager);
        notificationCenterAdapter = new NotificationCenterAdapter(msgList);

        itemTouchHelperCallback = new SimpleItemTouchHelperCallback(this, 1);
        requestTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        requestTouchHelper.attachToRecyclerView(rlNotification);

        notificationCenterAdapter.bindToRecyclerView(rlNotification);

        notificationCenterAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (notificationCenterAdapter.getDataList().size() > position) {
                    MsgEntity msgEntity = notificationCenterAdapter.getDataList().get(position);
                    if (msgEntity != null) {
                        String msgSubject = msgEntity.msgSubject;
                        String msgCode = msgEntity.msgCode;
                        String msgId = msgEntity.msgId;
                        MsgContentEntity msgContentEntity = msgEntity.msgContent;
                        MsgBizParamEntity msgBizParamEntity = msgEntity.msgBizParam;

                        if (Constants.NOTIFY_SUBJECT_NI_CARD_EXPIRED.equals(msgSubject)) {
                            String orderNo = msgBizParamEntity.orderNo;
                            String maskedCardNo = msgBizParamEntity.maskedCardNo;
                            if (!TextUtils.isEmpty(maskedCardNo)) {
                                showCardExpiredDialog(msgEntity.inAppTitle, msgBizParamEntity);
                            } else {
                                showProgress(true);
                                AppClient.getInstance().getCardManager().getKsaCardDetails(orderNo, new ResultCallback<CardDetailEntity>() {
                                    @Override
                                    public void onResult(CardDetailEntity cardDetailEntity) {
                                        showProgress(false);
                                        if (cardDetailEntity != null) {
                                            msgBizParamEntity.maskedCardNo = cardDetailEntity.maskedCardNo;
                                            msgBizParamEntity.cardHolderName = cardDetailEntity.cardHolderName;
                                            showCardExpiredDialog(msgEntity.inAppTitle, msgBizParamEntity);
                                        }
                                    }

                                    @Override
                                    public void onFailure(String errorCode, String errorMsg) {
                                        showProgress(false);
                                        showTipDialog(errorMsg);
                                    }
                                });
                            }
                            return;
                        }

                        if (Constants.NOTIFY_SUBJECT_USER_KYC_STATUS_UNPD.equals(msgSubject)) {

                        }

                        if (Constants.NOTIFY_SUBJECT_PORTAL_PUSH.equals(msgSubject)) {
                            if (TextUtils.isEmpty(msgEntity.clickTime)) {
                                AppClient.getInstance().updateClickTimeService(msgId);
                            }

                            if (msgBizParamEntity != null) {
                                String targetURL = msgBizParamEntity.targetURL;
                                String targetPage = msgBizParamEntity.targetPage;
                                if (!TextUtils.isEmpty(targetURL)) {
                                    HashMap<String, Object> mHashMaps = new HashMap<>();
                                    mHashMaps.put(Constants.BIND_CARD_3DS, targetURL);
                                    ActivitySkipUtil.startAnotherActivity(RequestCenterActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                } else if ("splitBill".equals(targetPage)) {
                                    if (isKyc(Constants.KYC_TYPE_SPLIT_BILL)) {
                                        mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                                    }
                                }
                            }
                        }

//                        showCardExpiredDialog("52494000107066459830");
//                        mPresenter.updateClickTimeService(msgId,userInfoEntity.userId,System.currentTimeMillis(),SpUtils.getInstance().getCallingCode());

                        if (!"000000".equals(msgCode)) {

                        } else {
                            if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(msgSubject)) {
                                if (msgContentEntity != null) {
                                    String activityNo = msgContentEntity.activityNo;
                                    String activityType = msgContentEntity.activityType;
                                    showMarketActivityDetail(activityNo, activityType);
                                }
                            } else if (msgBizParamEntity != null && !TextUtils.isEmpty(msgBizParamEntity.orderNo)) {
                                showProgress(true);
                                AppClient.getInstance().getTransferManager().getTransferDetail(msgBizParamEntity.orderNo, "", "", new ResultCallback<TransferHistoryDetailEntity>() {
                                    @Override
                                    public void onResult(TransferHistoryDetailEntity transferHistoryDetailEntity) {
                                        showProgress(false);
                                        AndroidUtils.jumpToTransferDetail(RequestCenterActivity.this, transferHistoryDetailEntity, "", true);
                                    }

                                    @Override
                                    public void onFailure(String errorCode, String errorMsg) {
                                        showProgress(false);
                                        showTipDialog(errorMsg);
                                    }
                                });
                            } else {
                                AndroidUtils.jumpToNotifyActivity(RequestCenterActivity.this, msgSubject);
                            }
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                mPresenter.getMsgList(currentPage, pageSize);
            }
        });

        notificationCenterAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                currentPage += 1;
                if (mPresenter != null) {
                    mPresenter.getMsgList(currentPage, pageSize);
                }
            }
        }, rlNotification);

        LocaleUtils.viewRotationY(this, ivBack, headCircle);
    }

    private void showNoBalanceDialog() {
        noBalanceDialog = new NoBalanceDialog(mContext);
        noBalanceDialog.showWithBottomAnim();
    }

    private final Runnable rejectRunnable = new Runnable() {

        @Override
        public void run() {
            if (removeItem) {
                if (requestCenterEntity != null && requestMap.containsKey(requestCenterEntity.requestId)) {
                    sendRejectNotify(true);
                    setSize();
                    removeItem = false;
                }
            }
        }
    };

    private void initRecy() {
        rlRequest.removeAllViews();
    }

    public void getList() {
        getNotifyList();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPresenter.getMsgList(currentPage, pageSize);
            }
        }, 300);
    }

    public void getNotifyList() {
        mPresenter.getRequestCenterList();
    }


    @OnClick({R.id.iv_back, R.id.ll_split})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_split:
                break;
            default:
                break;
        }
    }

    private void setRequestContent(LinkedList<RequestCenterEntity> requestList) {
        if (requestList.size() > 0) {
            showStatusView(statusView, StatusView.CONTENT_VIEW);
            tvPage.setVisibility(View.VISIBLE);
            tvPage.setText(Spans.builder()
                    .text("1").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf")).size(14)
                    .text("/" + requestList.size()).color(mContext.getColor(R.color.color_a3a3a3)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf")).size(8)
                    .build());
        } else {
            tvPage.setVisibility(View.GONE);
            showStatusView(statusView, StatusView.NO_CONTENT_VIEW);
        }

        if (requestList.size() > 0) {
            rlRequest.initChildViews(requestList, R.layout.item_request_center, new NotifyView.BindView<RequestCenterEntity>() {
                @Override
                public void bindView(RequestCenterEntity data, View view) {
                    createNotifyChild(data, view);
                }
            });
            rlRequest.setOnFontItemMoveListener(new NotifyView.FontItemMoveListener() {

                @Override
                public void afterTurnPage(int position) {
                    currentPosition = position;
                    setSize();
                }

                @Override
                public void onItemClick(View view) {
                }

                @Override
                public void onFontItemBack(int position) {
                    currentPosition = position;
                    setSize();
                    removeItem = false;
                }

                @Override
                public void onFontItemRemove(int position) {
                    removeItem = true;
                    currentPosition = position;
                    setSize();

                    if (mHandler == null) {
                        mHandler = new Handler();
                    }
                    mHandler.postDelayed(rejectRunnable, 5000);
                    String msg = getString(R.string.request_transfer_15_1);
                    if (Constants.SUBJECT_SPLIT_BILL.equals(requestCenterEntity.requestSubject)) {
                        msg = getString(R.string.ref_code_06);
                    }
                    showTipCenterDialog(msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeView();
                            removeItem = false;
                            rlRequest.backFontItem();
                            if (requestCenterEntity != null && requestMap.containsKey(requestCenterEntity.requestId)) {
                                requestMap.remove(requestCenterEntity.requestId);
                                mHandler.removeCallbacks(rejectRunnable);
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void getRequestCenterListSuccess(RequestCenterListEntity requestCenterListEntity) {
        currentPosition = 0;
        requestList.clear();
        if (!removeItem) {
            requestMap.clear();
        }
        rlRequest.removeAllViews();
        if (requestCenterListEntity != null) {
            requestList.addAll(requestCenterListEntity.requestList);
            if (userInfoEntity != null) {
                userInfoEntity.requestCounts = requestList.size();
                AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
            }

            setRequestContent(requestList);
        }
//        getChatSessions();
        showProgress(false);
    }


    public String getPhone(String phone) {
        if (!TextUtils.isEmpty(phone) && !phone.startsWith("+966")) {
            return "+966 " + phone;
        }
        return phone;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
        SpUtils.getInstance().setReqCounts(0);
        getList();

        if (ProjectApp.showMarketActivity) {
            CardNotifyEntity cardNotifyEntity = ProjectApp.getCardNotifyEntity();
            String activityNo = cardNotifyEntity.activityNo;
            String activityType = cardNotifyEntity.activityType;
            showMarketActivityDetail(activityNo, activityType);
            ProjectApp.showMarketActivity = false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
        if (requestDialog != null) {
            requestDialog.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_APPROVE_TRANSFER_SUCCESS == event.getEventType()) {
            if (requestCenterEntity != null) {
                requestList.remove(requestCenterEntity);
            }
            if (requestList.size() == 0) {
                removeView();
                mPresenter.getRequestCenterList();
            }
            currentPosition = rlRequest.clearFontItem();
            setSize();
            userInfoEntity.requestCounts = userInfoEntity.requestCounts - 1;
//            SpUtils.getInstance().setReqCounts(userInfoEntity.requestCounts);
            AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
        }
    }

    @Override
    public void getRequestCenterListFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
        tvPage.setVisibility(View.GONE);
        showStatusView(statusView, StatusView.NO_CONTENT_VIEW);
        showProgress(false);
    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity payBillOrderInfoEntity) {
        if ("REJECT".equals(sendType)) {
            if (requestList.size() == 0) {
                removeView();
                mPresenter.getRequestCenterList();
            }
            userInfoEntity.requestCounts = userInfoEntity.requestCounts - 1;
//            SpUtils.getInstance().setReqCounts(userInfoEntity.requestCounts);
            AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
        } else {
            if (payBillOrderInfoEntity == null) return;
            riskControlEntity = payBillOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(RequestCenterActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                } else if (riskControlEntity.isNeedIvr()) {
                    jumpToPwd(true);
                } else {
                    requestTransfer();
                }
            } else {
                requestTransfer();
//                jumpToPwd();
            }
        }
    }

    private void requestTransfer() {
        if (requestCenterEntity != null) {
            orderNo = requestCenterEntity.orderNo;
            payMethod = requestCenterEntity.payMethod;
            transAmount = requestCenterEntity.transferAmount;
            exchangeRate = requestCenterEntity.exchangeRate;
            transCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
        }
        mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat);
    }

    private void setTransferMap(HashMap<String, Object> mHashMap) {
        TransferEntity transferEntity = new TransferEntity();
        transferEntity.orderNo = requestCenterEntity.orderNo;
        transferEntity.orderAmount = requestCenterEntity.transferAmount;
        transferEntity.remark = requestCenterEntity.transferRemark;
        transferEntity.payMethod = requestCenterEntity.payMethod;
        transferEntity.orderCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferEntity.exchangeRate = requestCenterEntity.exchangeRate;
        transferEntity.payerName = payerName;
        mHashMap.put(Constants.TRANSFER_ENTITY, transferEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_TRANSFER_RC);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_TRANSFER_REQUEST);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
        if (Constants.SUBJECT_SPLIT_BILL.equals(requestCenterEntity.requestSubject)) {
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.splitbill_13));
        } else {
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.request_transfer_1));
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if (isNeedIvr) {
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        if (requestDialog != null) {
            requestDialog.dismiss();
        }
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        if (requestDialog != null) {
            requestDialog.dismiss();
        }
        transferSurePaySuccess(this, Constants.TYPE_TRANSFER_RC, transferEntity, false);
    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestCenterActivity = null;

    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {
        if ("REJECT".equals(sendType)) {
            if (requestList.size() == 0) {
                removeView();
                mPresenter.getRequestCenterList();
            }
        } else {
//            showTipDialog(errorMsg);
            if ("010107".equals(errorCode)) {
                //余额不足
                showNoBalanceDialog();
                if (requestDialog != null) {
                    requestDialog.dismiss();
                }
            } else if ("030204".equals(errorCode) || "030207".equals(errorCode)) {
                if (requestDialog != null) {
                    requestDialog.dismiss();
                }
                showExcessBeneficiaryDialog(errorMsg);
            } else {
                tvError.setText(errorMsg);
                tvError.setVisibility(View.VISIBLE);
            }
        }
    }

    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void getMsgListSuccess(MsgListEntity msgListEntity) {
        swipeRefreshLayout.setRefreshing(false);
        if (msgListEntity != null) {
            msgList = msgListEntity.msgList;
            if (currentPage == 1) {
                if (msgList.size() == 0) {
                    showStatusView(notifyStatusView, StatusView.NO_CONTENT_VIEW);
                } else {
                    showStatusView(notifyStatusView, StatusView.CONTENT_VIEW);
                    notificationCenterAdapter.setDataList(msgList);
                }
            } else {
                if (msgList.size() == 0) {
                    notificationCenterAdapter.loadMoreEnd(false);
                } else {
                    notificationCenterAdapter.addData(msgList);
                    if (msgList.size() < pageSize) {
                        notificationCenterAdapter.loadMoreEnd(false);
                    } else {
                        notificationCenterAdapter.loadMoreComplete();
                    }
                }
            }
        }
    }

    @Override
    public void getMsgListFail(String errorCode, String errorMsg) {
        swipeRefreshLayout.setRefreshing(false);
        showTipDialog(errorMsg);
        if (currentPage == 1) {
            showStatusView(notifyStatusView, StatusView.NO_CONTENT_VIEW);
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> limitMap = new HashMap<>();
        limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        ActivitySkipUtil.startAnotherActivity(RequestCenterActivity.this, CreateSplitBillActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void removeRequestSuccess() {
        if (notificationCenterAdapter != null) {
            //移除数据
            notificationCenterAdapter.getDataList().remove(notifyPosition);
            notificationCenterAdapter.notifyItemRemoved(notifyPosition);

            if (notificationCenterAdapter.getDataList().size() == 0) {
                showStatusView(notifyStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void removeRequestFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
        if (notificationCenterAdapter != null) {
            notificationCenterAdapter.notifyItemChanged(notifyPosition);
        }
    }

    @Override
    public RequestCenterContract.RequestCenterListPresenter getPresenter() {
        return new RequestCenterListPresenter();
    }

    private void showRequestDetailDialog(RequestCenterEntity requestCenterEntity) {
        requestDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirm_transaction, null);
        TextView tvConfirm = contentView.findViewById(R.id.tv_confirm);
        TextView tvCancel = contentView.findViewById(R.id.tv_cancel);
        TextView tvMoney = contentView.findViewById(R.id.tv_money);
        TextView tvNote = contentView.findViewById(R.id.tv_note);
        TextView tvName = contentView.findViewById(R.id.tv_name);
        TextView tvPhone = contentView.findViewById(R.id.tv_phone);
        LottieAnimationView ivSend = contentView.findViewById(R.id.iv_send);
        tvError = contentView.findViewById(R.id.tv_error);
        ConstraintLayout con_note = contentView.findViewById(R.id.con_note);
        RoundedImageView ivFromAvatar = contentView.findViewById(R.id.iv_from_avatar);
        RoundedImageView ivToAvatar = contentView.findViewById(R.id.iv_to_avatar);
        LocaleUtils.viewRotationY(mContext, ivSend);

        String requestIcon = requestCenterEntity.requestIcon;
        String receiveIcon = requestCenterEntity.receiveIcon;
        String requestPhone = requestCenterEntity.requestPhone;
        String receivePhone = requestCenterEntity.receivePhone;
        String requestUserName = requestCenterEntity.requestUserName;
        String receiveUserName = requestCenterEntity.receiveUserName;
        String requestSex = requestCenterEntity.requestSex;
        String receiveSex = requestCenterEntity.receiveSex;
        String transferAmount = requestCenterEntity.transferAmount;
        String note = requestCenterEntity.transferRemark;
        String currencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferAmount = AndroidUtils.getTransferMoney(transferAmount);
        if (!TextUtils.isEmpty(transferAmount)) {
            tvMoney.setText(Spans.builder()
                    .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                    .build());
        }

        tvName.setText(AndroidUtils.getContactName(requestUserName));
        tvPhone.setText(AndroidUtils.getPhone(requestPhone));
        if (!TextUtils.isEmpty(note)) {
            con_note.setVisibility(View.VISIBLE);
            tvNote.setText(note);
        } else {
            con_note.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(receiveIcon)) {
            Glide.with(mContext).clear(ivFromAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(receiveIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, receiveSex))
                    .into(ivFromAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, receiveSex)).into(ivFromAvatar);
        }

        if (!TextUtils.isEmpty(requestIcon)) {
            Glide.with(mContext).clear(ivToAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(requestIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, requestSex))
                    .into(ivToAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, requestSex)).into(ivToAvatar);
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bottomDialog != null) {
//                    bottomDialog.dismiss();
//                }
                sendType = "TRANSFER";
                NotifyEntity notifyEntity = new NotifyEntity();
                notifyEntity.msgSubject = "TRANSFER";
                notifyEntity.msgResult = "CONFIRM";
                notifyEntity.orderNo = requestCenterEntity.orderNo;
                List<NotifyEntity> receivers = new ArrayList<>();
                receivers.add(notifyEntity);
                mPresenter.sendNotify(receivers);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestDialog != null) {
                    requestDialog.dismiss();
                }
            }
        });

        requestDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        requestDialog.setContentView(contentView);
        Window dialogWindow = requestDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        requestDialog.showWithBottomAnim();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDissmiss(int position, int type) {
        notifyPosition = position;
        MsgEntity msgEntity = notificationCenterAdapter.getDataList().get(position);
        mPresenter.removeRequest(msgEntity.msgId);
    }

    private void getChatSessions() {
        GcChatGetSessionsRequest request = new GcChatGetSessionsRequest().setTake(100).setSkip(0);
        GcChatSDK.getSessionList(request, new GetSessionListCallback() {
            @Override
            public void onSuccessful(List<GcChatSession> list, int totalCount) {
                swipeRefreshLayout.setRefreshing(false);
                if (list != null) {
                    int size = list.size();

                    for (int i = 0; i < size; i++) {
                        GcChatSession gcChatSession = list.get(i);
                        if (gcChatSession.isGroupChat()) {
                            for (GcChatParticipant chatParticipant : gcChatSession.getParticipantUsers()) {
                                boolean isMeParticipant = UserUtils.loggedInUserId.equals(chatParticipant.getUser().getExternalId());
                                if (isMeParticipant && chatParticipant.isInvitationPending()) {
                                    RequestCenterEntity entity = new RequestCenterEntity();
                                    entity.requestSubject = "invite_group";
                                    entity.chatUserId = chatParticipant.getId();
                                    entity.chatPhone = chatParticipant.getUser().getExternalId();
                                    entity.groupName = gcChatSession.getTitle();
                                    entity.groupInviter = gcChatSession.getCreatedByUser().getName();
                                    entity.chatUserAvatar = gcChatSession.getCreatedByUser().getFileId();
                                    requestList.add(entity);
                                }
                            }
                        }
                    }
                    setRequestContent(requestList);
                }
            }

            @Override
            public void onFailure(int errorCode) {
                swipeRefreshLayout.setRefreshing(false);
                showTipDialog(GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }

    private void acceptInvitation(String myParticipantId) {
        showProgressCancelable(true, true);
        GcChatSDK.acceptChatInvitation(myParticipantId, new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                showProgress(false);
                showToast("success");
            }

            @Override
            public void onFailure(int i) {
                showTipDialog("Error in accepting invitation - " + GcChatErrorUtils.getErrorMessage(i));
            }
        });
    }

    private void rejectInvitation(String myParticipantId) {
        showProgressCancelable(true, true);
        GcChatSDK.rejectChatInvitation(myParticipantId, new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                showProgress(false);
                showToast("success");
            }

            @Override
            public void onFailure(int i) {
                showTipDialog("Error in accepting invitation - " + GcChatErrorUtils.getErrorMessage(i));
            }
        });
    }

    private void createNotifyChild(RequestCenterEntity entity, View view) {
        LinearLayout llRequestTransfer = view.findViewById(R.id.ll_request_transfer);
        ConstraintLayout llInviteChat = view.findViewById(R.id.ll_invite_chat);
        RoundedImageView ivFromAvatar = view.findViewById(R.id.iv_from_avatar);
        RoundedImageView ivToAvatar = view.findViewById(R.id.iv_to_avatar);
        TextView tvMoney = view.findViewById(R.id.tv_money);
        TextView tvDesc = view.findViewById(R.id.tv_desc);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvPhone = view.findViewById(R.id.tv_phone);
        TextView tvReject = view.findViewById(R.id.tv_reject);
        TextView tvApprove = view.findViewById(R.id.tv_approve);
        TextView tvInviteYes = view.findViewById(R.id.tv_invite_yes);
        TextView tvInviteNo = view.findViewById(R.id.tv_invite_no);
        TextView tvChatId = view.findViewById(R.id.tv_chat_id);
        TextView tvInviteMessage = view.findViewById(R.id.tv_invite_message);
        ImageView ivInviteAvatar = view.findViewById(R.id.iv_invite_avatar);
        LottieAnimationView ivSend = view.findViewById(R.id.iv_send);

        String chatUserId = entity.chatUserId;
        if (!TextUtils.isEmpty(chatUserId)) {
            String displayPic = entity.chatUserAvatar;
            llRequestTransfer.setVisibility(View.GONE);
            llInviteChat.setVisibility(View.VISIBLE);
            tvChatId.setText(entity.chatPhone);
            if (displayPic != null && !displayPic.isEmpty()) {
                Glide.with(mContext).load(ChatHelper.getInstance().getChatUserAvatar(displayPic)).centerCrop().autoClone().into(ivInviteAvatar);
            } else {
                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivInviteAvatar);
            }
            tvInviteMessage.setText(Spans.builder()
                    .text(entity.groupInviter + " " + getString(R.string.sprint20_92) + " ").color(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Regular.ttf" : "fonts/SFProDisplay-Regular.ttf")).size(14)
                    .text(entity.groupName + " " + getString(R.string.sprint20_93)).color(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                    .build());
        } else {
            llRequestTransfer.setVisibility(View.VISIBLE);
            llInviteChat.setVisibility(View.GONE);
        }
        tvInviteYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(chatUserId)) {
                    acceptInvitation(chatUserId);
                }
            }
        });

        tvInviteNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(chatUserId)) {
                    rejectInvitation(chatUserId);
                }
            }
        });

        tvReject.getParent().requestDisallowInterceptTouchEvent(true);
        tvReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.e("tvReject", " " + view.getId());
                if (!isKyc(Constants.KYC_TYPE_NULL) || requestList.size() < currentPosition + 1
                        || requestList.get(currentPosition) == null
                        || !rlRequest.isChildClickable()) {
                    return;
                }
                removeView();
                if (mHandler != null) {
                    mHandler.removeCallbacks(rejectRunnable);
                }
                if (requestCenterEntity != null && requestMap.containsKey(requestCenterEntity.requestId)) {
                    sendRejectNotify(false);
                }
                requestCenterEntity = requestList.get(currentPosition);
                requestMap.put(requestCenterEntity.requestId, requestCenterEntity);

                rlRequest.removeFontItem();
            }
        });
        tvApprove.getParent().requestDisallowInterceptTouchEvent(true);
        tvApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.SUBJECT_SPLIT_BILL.equals(entity.requestSubject)) {
                    if (AndroidUtils.isCloseUse(RequestCenterActivity.this, Constants.confirm_spilit_request))
                        return;
                } else {
                    if (AndroidUtils.isCloseUse(RequestCenterActivity.this, Constants.confirm_request))
                        return;
                }
                LogUtils.e("tvApprove", " ");
                if (!isKyc(Constants.KYC_TYPE_NULL) || ButtonUtils.isFastDoubleClick() ||
                        (requestDialog != null && requestDialog.isShowing())) {
                    return;
                }
                removeItem = false;
                if (requestList.size() > currentPosition) {
                    requestCenterEntity = requestList.get(currentPosition);
                    if (requestCenterEntity != null) {
                        showRequestDetailDialog(requestCenterEntity);
                    }
                }

            }
        });
        LocaleUtils.viewRotationY(mContext, ivSend);

        if (ThemeUtils.isCurrentDark(mContext)) {
            ivSend.setAnimation("lottie/arrow_right_transfer.json");
        } else {
            ivSend.setAnimation("lottie/l_request_arrow.json");
        }

        String requestIcon = entity.requestIcon;
        String receiveIcon = entity.receiveIcon;
        String requestSubject = entity.requestSubject;
        String requestPhone = entity.requestPhone;
        String receivePhone = entity.receivePhone;
        String requestUserName = entity.requestUserName;
        String receiveUserName = entity.receiveUserName;
        String requestSex = entity.requestSex;
        String receiveSex = entity.receiveSex;
        String transferAmount = entity.transferAmount;
        String currencyCode = UserInfoManager.getInstance().getCurrencyCode();

        if (Constants.SUBJECT_SPLIT_BILL.equals(requestSubject)) {
            tvDesc.setText(mContext.getString(R.string.request_transfer_28));
        } else {
            tvDesc.setText(mContext.getString(R.string.request_transfer_12));
        }

        transferAmount = AndroidUtils.getTransferMoney(transferAmount);
        if (!TextUtils.isEmpty(transferAmount)) {
            if (transferAmount.length() > 8) {
                tvMoney.setText(Spans.builder()
                        .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(16)
                        .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(12)
                        .build());
            } else {
                tvMoney.setText(Spans.builder()
                        .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(20)
                        .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(12)
                        .build());
            }
        }

        tvName.setText(AndroidUtils.getContactName(requestUserName));
        tvPhone.setText(getPhone(requestPhone));

        if (!TextUtils.isEmpty(receiveIcon)) {
            Glide.with(mContext).clear(ivFromAvatar);
            Glide.with(mContext)
                    .load(receiveIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, receiveSex))
                    .into(ivFromAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, receiveSex)).into(ivFromAvatar);
        }

        if (!TextUtils.isEmpty(requestIcon)) {
            Glide.with(mContext).clear(ivToAvatar);
            Glide.with(mContext)
                    .load(requestIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, requestSex))
                    .into(ivToAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, requestSex)).into(ivToAvatar);
        }
    }

    private void sendRejectNotify(boolean isTouch) {
        if (rlRequest == null) {
            return;
        }
        requestList.remove(requestCenterEntity);
        requestMap.remove(requestCenterEntity.requestId);
        currentPosition = rlRequest.removeCache(isTouch);

        NotifyEntity notifyEntity = new NotifyEntity();
        notifyEntity.msgSubject = "TRANSFER";
        sendType = "REJECT";
        notifyEntity.msgResult = "REJECT";
        notifyEntity.orderNo = requestCenterEntity.orderNo;
        List<NotifyEntity> receivers = new ArrayList<>();
        receivers.add(notifyEntity);
        if (mPresenter != null) {
            mPresenter.sendNotify(receivers);
        }
    }
}
