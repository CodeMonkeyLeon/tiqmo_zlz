package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardSummaryPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class GetNewCardSummaryActivity extends BaseCompatActivity<CardKsaContract.GetCardSummaryPresenter> implements CardKsaContract.GetCardSummaryView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_available_balance)
    TextView tvAvailableBalance;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.ll_error)
    LinearLayout llError;
    @BindView(R.id.con_topView)
    ConstraintLayout conTopView;
    @BindView(R.id.tv_card_price_title)
    TextView tvCardPriceTitle;
    @BindView(R.id.tv_card_price)
    TextView tvCardPrice;
    @BindView(R.id.tv_vat_title)
    TextView tvVatTitle;
    @BindView(R.id.tv_vat)
    TextView tvVat;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.tv_total_title)
    TextView tvTotalTitle;
    @BindView(R.id.tv_total_money)
    TextView tvTotalMoney;
    @BindView(R.id.tv_time_title)
    TextView tvTimeTitle;
    @BindView(R.id.tv_delivery_time)
    TextView tvDeliveryTime;
    @BindView(R.id.cl_order_detail)
    ConstraintLayout clOrderDetail;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    @BindView(R.id.id_cl_tips)
    ConstraintLayout mClTips;

    private double monthLimitMoney, valiableMoney, money;
//    private double  totalMoney, balanceMoney;

    private TransferLimitEntity transferLimitEntity;

    private KsaCardSummaryEntity ksaCardSummaryEntity;

    //实体卡卡费（金额：单位最小币种）。当请求参数cardType为空的时候，表示获取获取卡费接口，此刻必返回 ，当请求参数cardType必传的时候，表示summary接口,该值依据类型是否返回。
    public String physicalCardAmount;
    //	虚拟卡卡费（金额：单位最小币种）。当请求参数cardType为空的时候，表示获取获取卡费接口，此刻必返回 ，当请求参数cardType必传的时候，表示summary接口,该值依据类型是否返回。
    public String virtualCardAmount;
    //表示summary接口的时候必返回 vat(金额：单位最小币种)
    public String vat;
    //表示summary接口的时候必返回 总交易金额(金额：单位最小币种)
    public String totalAmount;
    //	表示summary接口的时候且为实体卡必返回 实体卡投递周期
    public String deliveryTime;

    private OpenCardReqEntity openCardReqEntity;

    private RiskControlEntity riskControlEntity;

    private static GetNewCardSummaryActivity getNewCardSummaryActivity;

    public static GetNewCardSummaryActivity getGetNewCardSummaryActivity() {
        return getNewCardSummaryActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getNewCardSummaryActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_get_card_summary;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        getNewCardSummaryActivity = this;
        setDarkBar();
        tvTitle.setText(R.string.DigitalCard_0);
        LocaleUtils.viewRotationY(this, ivHeadCircle, ivBack);
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        if (getIntent() != null && getIntent().getExtras() != null) {
            ksaCardSummaryEntity = (KsaCardSummaryEntity) getIntent().getExtras().getSerializable(Constants.ksaCardSummaryEntity);
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            if (transferLimitEntity != null) {
                String monthLimitAmt = transferLimitEntity.monthLimitAmt;
                try {
                    monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
            }

            if (ksaCardSummaryEntity != null) {
                physicalCardAmount = ksaCardSummaryEntity.physicalCardAmount;
                virtualCardAmount = ksaCardSummaryEntity.virtualCardAmount;
                vat = ksaCardSummaryEntity.vat;
                totalAmount = ksaCardSummaryEntity.totalAmount;
                deliveryTime = ksaCardSummaryEntity.deliveryTime;
                if (!TextUtils.isEmpty(physicalCardAmount)) {
                    tvCardPriceTitle.setText(getString(R.string.DigitalCard_40));
                    mClTips.setVisibility(View.VISIBLE);
                    tvCardPrice.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(physicalCardAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                            .build());
                } else {
                    tvCardPriceTitle.setText(getString(R.string.DigitalCard_103));
                    mClTips.setVisibility(View.GONE);
                    tvCardPrice.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(virtualCardAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                            .build());
                }

                tvTotalMoney.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(totalAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                        .build());
                if (!TextUtils.isEmpty(vat) && BigDecimalFormatUtils.compareBig(vat, "0")) {
                    tvVat.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(vat) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                            .build());
                } else {
                    tvVat.setVisibility(View.GONE);
                    tvVatTitle.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(deliveryTime)) {
                    tvDeliveryTime.setText(deliveryTime);
                    tvDeliveryTime.setVisibility(View.VISIBLE);
                    tvTimeTitle.setVisibility(View.VISIBLE);
                } else {
                    tvDeliveryTime.setVisibility(View.GONE);
                    tvTimeTitle.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }

        getBalance();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUser();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                tvBalance.setText(userInfoEntity.getBalance());
            }
        }
    }

    private void checkMoney(String amount) {
        money = AndroidUtils.getTransferMoneyNumber(amount);
        if (money <= valiableMoney) {
            if (money > 0) {
                tvConfirm.setEnabled(true);
            }
            tvAddMoney.setEnabled(false);
            llError.setVisibility(View.GONE);
        } else {
            tvAddMoney.setEnabled(true);
            tvConfirm.setEnabled(false);
            llError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.wtw_25));
        }
        if (valiableMoney == 0) {
            tvAddMoney.setEnabled(true);
        }
    }

    private void checkUser() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            String balance = userInfoEntity.getBalance();
            String balanceNumber = userInfoEntity.getBalanceNumber();
            if (!TextUtils.isEmpty(balanceNumber)) {
                try {
                    balanceNumber = balanceNumber.replace(",", "");
                    valiableMoney = Double.parseDouble(balanceNumber);
                    checkMoney(totalAmount);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
            }
            tvBalance.setText(balance);
            tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_add_money, R.id.tv_cancel, R.id.tv_confirm})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            case R.id.tv_cancel:
                ProjectApp.removeGetNewCardTask();
                finish();
                break;
            case R.id.tv_confirm:
                List<String> additionalData = new ArrayList<>();
                mPresenter.getOtpType("ActionType", "TCA01", additionalData);
                break;
            default:
                break;
        }
    }

    private void requestTransfer() {
        mPresenter.getKsaCardPayResult(vat, totalAmount, openCardReqEntity);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if ("030194".equals(errorCode)) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.cardResultType, KsaCardResultActivity.cardResultType_Open_Card_fail);
            mHashMap.put(Constants.cardResultErrorMsg, errorMsg);
            ActivitySkipUtil.startAnotherActivity(this, KsaCardResultActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void getKsaCardSummarySuccess(KsaCardSummaryEntity result) {

    }

//    @Override
//    public void openKsaCardNoFeeSuccess(KsaPayResultEntity result) {
//
//    }

    @Override
    public void getKsaCardPayResultSuccess(KsaPayResultEntity ksaPayResultEntity) {
        if (ksaPayResultEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksa_cardType, ksaPayResultEntity.cardType);
            mHashMap.put(Constants.ksa_deliveryTime, deliveryTime);
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void setKsaCardPinSuccess(Void result) {

    }

    @Override
    public CardKsaContract.GetCardSummaryPresenter getPresenter() {
        return new CardSummaryPresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(GetNewCardSummaryActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                } else if (riskControlEntity.isNeedIvr()) {
                    jumpToPwd(true);
                } else {
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if (isNeedIvr) {
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap) {
        ksaCardSummaryEntity.openCardReqEntity = openCardReqEntity;
        mHashMap.put(Constants.sceneType, Constants.TYPE_PAY_CARD);
        mHashMap.put(Constants.ksaCardSummaryEntity, ksaCardSummaryEntity);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_PAY_CARD);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
