package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;

public class CardHomeFragment extends BaseFragment {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;
    @BindView(R.id.tv_coming_soon)
    TextView tvComingSoon;
    @BindView(R.id.tv_back_to_home)
    TextView tvBackToHome;
    @BindView(R.id.iv_comming_soon)
    ImageView ivCommingSoon;

    public static CardHomeFragment getInstance() {
        CardHomeFragment cardHomeFragment = new CardHomeFragment();
        return cardHomeFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_coming_soon;
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(bgMain, R.attr.splash_bg_color);
        helper.setBackgroundColorByAttr(commonHead, R.attr.color_061f6f_white);
        helper.setTextColorByAttr(tvComingSoon, R.attr.color_white_3a3b44);
        helper.setImageResourceByAttr(ivBack, R.attr.back_arrow);
        helper.setImageResourceByAttr(ivCommingSoon, R.attr.icon_coming_soon);
        helper.setTextColorByAttr(tvTitle, R.attr.color_white_3a3b44);
        helper.setBackgroundResourceByAttr(tvBackToHome, R.attr.bg_btn_next_page_card);
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, ivBack);
        ivBack.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.virtualCard_0));
        tvBackToHome.setVisibility(View.VISIBLE);


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity != null && mActivity instanceof MainActivity) {
                    ((MainActivity) mActivity).showBottom(3);
                }
            }
        });

        tvBackToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity != null && mActivity instanceof MainActivity) {
                    ((MainActivity) mActivity).showBottom(3);
                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }

    @OnClick({R.id.iv_back, R.id.tv_back_to_home})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_back_to_home:
                finish();
                break;
            default:
                break;
        }
    }
}
