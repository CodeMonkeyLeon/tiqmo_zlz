package cn.swiftpass.wallet.tiqmo.module.voucher;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.adapter.VoucherCategoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherContract;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.EVoucherMoneyDialog;

public class EVoucherSelectActivity extends BaseCompatActivity<EVoucherContract.Presenter>
        implements EVoucherContract.View {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.rv_evoucher_category)
    RecyclerView rvEVoucherCategory;
    @BindView(R.id.sw_evoucher_category)
    SwipeRefreshLayout swEVoucherCategory;

    private EVoucherMoneyDialog eVoucherMoneyDialog;
    private String brandName;
    private String moneyVoucherCode;

    private EVoucherOrderInfoEntity mEVoucherOrderInfoEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activtiy_evoucher_select;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
        ReferralCodeDialogUtils.resetKeyDialog();
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                if (!EVoucherSelectActivity.this.isFinishing()) {
                    finish();
                }
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.START_KYC == event.getEventType()) {
            if (userInfoEntity.isKycChecking()) {
                ActivitySkipUtil.startAnotherActivity(EVoucherSelectActivity.this,
                        KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                ReferralCodeDialogUtils.showReferralCodeDialog(this, mContext);
            }
        }
    }

    private void initData() {
        String providerCode = getIntent().getStringExtra("providerCode");
        String brandCode = getIntent().getStringExtra("brandCode");
        brandName = getIntent().getStringExtra("brandName");
        String countryCode = getIntent().getStringExtra("countryCode");
        tvTitle.setText(brandName);
        mPresenter.requestVoucherInfo(providerCode, brandCode, countryCode, Constants.sceneType_EVoucher);
        swEVoucherCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.requestVoucherInfo(providerCode, brandCode, countryCode, Constants.sceneType_EVoucher);
            }
        });
    }

    private void initView() {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvEVoucherCategory.setLayoutManager(manager);

        swEVoucherCategory.setColorSchemeResources(R.color.color_B00931);
        swEVoucherCategory.setRefreshing(false);

        eVoucherMoneyDialog = EVoucherMoneyDialog.getInstance(mContext);
        eVoucherMoneyDialog.setEVoucherMoneyPayListener(new EVoucherMoneyDialog.EVoucherMoneyPayListener() {
            @Override
            public void eVoucherMoneyPay(String eVoucherMoney) {
                if (!TextUtils.isEmpty(eVoucherMoney)) {
                    mPresenter.eVoucherOrder(moneyVoucherCode, AndroidUtils.getReqTransferMoney(eVoucherMoney));
                }
            }
        });
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity, CheckOutEntity checkOutEntity) {
        try {
            HashMap<String, Object> limitMap = new HashMap<>();
            limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
            limitMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
            limitMap.put("brandName", brandName);
            limitMap.put(Constants.mEVoucherOrderInfoEntity, mEVoucherOrderInfoEntity);
            limitMap.put(Constants.sceneType, Constants.sceneType_EVoucher);
            ActivitySkipUtil.startAnotherActivity(EVoucherSelectActivity.this,
                    EVoucherPayReviewActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        if("030216".equals(errCode)){
            showCommonErrorDialog(errMsg);
        }else{
            showTipDialog(errMsg);
        }
    }

    @Override
    public void eVoucherOrderSuccess(EVoucherOrderInfoEntity eVoucherOrderInfoEntity) {
        try {
            if (eVoucherOrderInfoEntity != null) {
                this.mEVoucherOrderInfoEntity = eVoucherOrderInfoEntity;
                mPresenter.checkOut("", eVoucherOrderInfoEntity.orderInfo);
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        try {
            mPresenter.getLimit(Constants.LIMIT_TRANSFER, checkOutEntity);
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void showVoucherList(VouTypeAndInfoEntity result) {
        swEVoucherCategory.setRefreshing(false);
        VoucherCategoryAdapter adapter = new VoucherCategoryAdapter(EVoucherSelectActivity.this,
                result.voucherTypeAndInfo);
        adapter.setVoucherClickListener(new VoucherCategoryAdapter.OnVoucherClickListener() {
            @Override
            public void onVoucherClick(int voucherCategory, int voucherPosition) {
                try {
                    if (result.voucherTypeAndInfo != null && result.voucherTypeAndInfo.size() > voucherCategory
                            && result.voucherTypeAndInfo.get(voucherCategory) != null
                            && result.voucherTypeAndInfo.get(voucherCategory).voucherInfos != null
                            && result.voucherTypeAndInfo.get(voucherCategory).voucherInfos.size() > voucherPosition) {
                        mPresenter.eVoucherOrder(result.voucherTypeAndInfo.get(voucherCategory)
                                .voucherInfos.get(voucherPosition).voucherCode, "");
                    }
                }catch (Throwable e){
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        });

        adapter.setOnVoucherMoneyClickListener(new VoucherCategoryAdapter.OnVoucherMoneyClickListener() {
            @Override
            public void onVoucherMoneyClick(VouTypeAndInfoEntity.VouTypeAndInfo vouTypeAndInfo) {
                if (vouTypeAndInfo != null) {
                    moneyVoucherCode = vouTypeAndInfo.voucherCode;
                    if (eVoucherMoneyDialog != null && !eVoucherMoneyDialog.isShowing()) {
                        eVoucherMoneyDialog.setVouTypeAndInfo(vouTypeAndInfo, brandName);
                        eVoucherMoneyDialog.showWithBottomAnim();
                    }
                }
            }
        });
        rvEVoucherCategory.setAdapter(adapter);
    }

    @Override
    public EVoucherContract.Presenter getPresenter() {
        return new EVoucherPresenter();
    }
}
