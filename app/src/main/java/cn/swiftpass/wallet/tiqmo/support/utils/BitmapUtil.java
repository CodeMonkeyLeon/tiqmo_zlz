package cn.swiftpass.wallet.tiqmo.support.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BitmapUtil {

    private static final String TAG = BitmapUtil.class.getName();

    public static Uri saveBitmapCompat(Context context, Bitmap bmp, @Nullable String savePath, String fileName, boolean isPNG) {
        ContentResolver resolver = context.getContentResolver();
        ContentValues contentValues = new ContentValues();
        OutputStream outputStream = null;
        FileOutputStream fos = null;
        try {
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //设置保存参数到ContentValues中
                //设置文件名
                contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
                //设置文件类型
                contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jepg");
                //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
                //RELATIVE_PATH是相对路径不是绝对路径
                //DCIM是系统文件夹，关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM);
                //因为insert 如果uri存在的话，会返回null 所以需要先delete
                resolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Images.Media.DISPLAY_NAME + "=?",
                        new String[]{fileName});
                //执行insert操作，向系统文件夹中添加文件
                //EXTERNAL_CONTENT_URI代表外部存储器，该值不变
                Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                if (uri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    outputStream = resolver.openOutputStream(uri);
                    if (outputStream != null) {
                        boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                        outputStream.flush();
                        outputStream.close();
                    }
                    return uri;
                }
            } else {
                long now = System.currentTimeMillis();
                contentValues.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, fileName);
                contentValues.put(MediaStore.Images.ImageColumns.TITLE, fileName);
                contentValues.put(MediaStore.Images.ImageColumns.MIME_TYPE, "image/jpg");
                contentValues.put(MediaStore.Images.ImageColumns.DATE_ADDED, now / 1000);
                contentValues.put(MediaStore.Images.ImageColumns.DATE_MODIFIED, now / 1000);
                contentValues.put(MediaStore.Images.ImageColumns.DATE_TAKEN, now);
//        values.put(MediaStore.Images.ImageColumns.IS_PENDING, 1);
                if (!TextUtils.isEmpty(savePath)) {
                    contentValues.put(MediaStore.Images.ImageColumns.DATA, savePath + File.separator + fileName);
                }
                Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                if (uri == null) {
                    return null;
                }
                outputStream = resolver.openOutputStream(uri);
                if (outputStream == null) {
                    return null;
                }
                if (bmp.compress(isPNG ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, isPNG ? 100 : 70, outputStream)) {
                    outputStream.flush();
//                values.put(MediaStore.Images.ImageColumns.IS_PENDING, 0);
                    resolver.update(uri, contentValues, null, null);
                    return uri;
                }
            }
        } catch (IOException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            FileUtils.closeIO(outputStream);
            FileUtils.closeIO(fos);
        }

        return null;
    }

    public static Bitmap nv21ToBitmap(byte[] nv21, int width, int height) {
        Bitmap bitmap = null;
        ByteArrayOutputStream stream = null;
        try {
            YuvImage image = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
            stream = new ByteArrayOutputStream();
            image.compressToJpeg(new Rect(0, 0, width, height), 80, stream);
            bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
            stream.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                }
            }
        }
        return bitmap;
    }


}
