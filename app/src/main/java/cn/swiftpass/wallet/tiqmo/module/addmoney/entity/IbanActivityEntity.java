package cn.swiftpass.wallet.tiqmo.module.addmoney.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class IbanActivityEntity extends BaseEntity {

    //优惠模式 1:固定，2:折扣
    public String discountModel;

    //优惠值 可能是折扣值也可能是固定优惠金额， 具体值意义 参考优惠模式【如果是折扣类型，
    // 此值表示对应的折扣是万分比单位为万分之一 示例1：8888 表示 折扣88.88%】如果是固定金额返回的是最小币种单位)
    public String discountValue;

    public String currencyCode;

}
