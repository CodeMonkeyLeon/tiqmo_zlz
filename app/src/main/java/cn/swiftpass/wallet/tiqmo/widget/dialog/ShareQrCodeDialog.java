package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;

public class ShareQrCodeDialog extends BottomDialog{

    private Context mContext;

    private ConfirmListener confirmListener;

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public interface ConfirmListener {
        void clickShare();
        void clickSave();
    }

    public ShareQrCodeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_share_qr_code, null);
        TextView tvShareQrCode = view.findViewById(R.id.tv_qr_share_code);
        TextView tvSaveQrCode = view.findViewById(R.id.tv_qr_save_code);

        tvShareQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickShare();
                }
            }
        });
        
        tvSaveQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickSave();
                }
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
