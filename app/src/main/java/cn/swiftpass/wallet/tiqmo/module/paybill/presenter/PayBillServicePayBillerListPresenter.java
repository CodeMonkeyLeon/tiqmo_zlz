package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillServicePayBillerListPresenter implements PayBillContract.BillServiceBillerListPresenter {


    PayBillContract.BillServiceBillerListView mBillServiceBillerListView;

    @Override
    public void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillerList(countryCode, billerType, billerDescription, channelCode, new LifecycleMVPResultCallback<PayBillerListEntity>(mBillServiceBillerListView, true) {
            @Override
            protected void onSuccess(PayBillerListEntity result) {
                if (mBillServiceBillerListView != null) {
                    mBillServiceBillerListView.getPayBillerListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBillServiceBillerListView != null) {
                    mBillServiceBillerListView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getBillServiceIOList(String billerId, String sku, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillServiceIOList(billerId, sku, channelCode, new LifecycleMVPResultCallback<PayBillIOListEntity>(mBillServiceBillerListView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                if (mBillServiceBillerListView != null) {
                    mBillServiceBillerListView.getBillServiceIOListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBillServiceBillerListView != null) {
                    mBillServiceBillerListView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }


    @Override
    public void attachView(PayBillContract.BillServiceBillerListView billServiceBillerListView) {
        mBillServiceBillerListView = billServiceBillerListView;
    }

    @Override
    public void detachView() {
        mBillServiceBillerListView = null;
    }
}
