package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;

/**
 * Created by aijingya on 2020/7/10.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.contract
 * @Description:
 * @date 2020/7/10.15:16.
 */
public class CardManageContract {

    public interface View extends BaseView<CardManageContract.Presenter> {
        void getBindCardListSuccess(List<CardEntity> result);
        void getBindCardListFailed(String errorCode,String errorMsg);

        void bindNewCardSuccess(CardBind3DSEntity result);
        void bindNewCardFailed(String errorCode,String errorMsg);

        void verifyCardSuccess(verifyCardEntity result);
        void verifyCardFailed(String errorCode,String errorMsg);
    }


    public interface Presenter extends BasePresenter<CardManageContract.View>{
        void getBindCardList();
        void bindNewCard(String cardHolderName,String cardNum, String expireDate,String CVV,String type) ;
        void verifyCard(String cardNo);
    }
}
