/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.swiftpass.wallet.tiqmo.support.zxing.Decoding;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.util.Collection;
import java.util.Map;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.zxing.Camera.CameraManager;
import cn.swiftpass.wallet.tiqmo.support.zxing.ViewfinderResultPointCallback;


/**
 * This class handles all the messaging which comprises the state machine for capture.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class CaptureActivityHandler extends Handler {

    private static final String TAG = CaptureActivityHandler.class.getSimpleName();

    private final QrcodeScanListener activity;
    private final DecodeThread decodeThread;
    private State state;
    private final CameraManager cameraManager;

    private enum State {
        PREVIEW, SUCCESS, DONE
    }

    public CaptureActivityHandler(QrcodeScanListener activity, Collection<BarcodeFormat> decodeFormats, String characterSet, Map<DecodeHintType, ?> baseHints, Handler mHandler, CameraManager cameraManager) {
        this.activity = activity;
        decodeThread = new DecodeThread(activity, decodeFormats, baseHints, characterSet, new ViewfinderResultPointCallback(activity.getViewfinderView()));
        decodeThread.start();
        state = State.SUCCESS;
        this.cameraManager = cameraManager;
        // Start ourselves capturing previews and decoding
        cameraManager.startPreview();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //https://blog.csdn.net/pbm863521/article/details/78965425
                //TODO  at android.hardware.Camera.native_autoFocus(Native Method) Camera ->open ->preview->autofocus
                restartPreviewAndDecode();
            }
        }, 1000);
    }

    /**
     * 重新自动对焦
     */
    public void startPreview() {
        //cameraManager.stopPreview();
        cameraManager.startPreview();
    }

    @Override
    public void handleMessage(Message message) {
//        Log.d(TAG, "handleMessage"+message.what);
        switch (message.what) {

            case R.id.restart_preview:
                restartPreviewAndDecode();
                break;
            case R.id.decode_succeeded:
                Log.d(TAG, "Got decode succeeded message");
                state = CaptureActivityHandler.State.SUCCESS;
                Bundle bundle = message.getData();
                Bitmap barcode = bundle == null ? null : (Bitmap) bundle.getParcelable(DecodeThread.BARCODE_BITMAP);
                activity.handleDecode((Result) message.obj, barcode);

                break;
            case R.id.decode_failed:
                // We're decoding as fast as possible, so when one decode fails, start another.
                state = State.PREVIEW;
                cameraManager.requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
                break;
            case R.id.return_scan_result:
                break;
            default:
                break;
        }
    }

    public void stopPreviewAndDecode() {
        if (cameraManager != null) {
            cameraManager.stopPreview();
        }
    }

    public void startPreviewAndDecode() {
        if (cameraManager != null) {
            cameraManager.startPreview();
        }
    }


    public void quitSynchronously() {
        state = State.DONE;
        cameraManager.stopPreview();
        Message quit = Message.obtain(decodeThread.getHandler(), R.id.quit);
        quit.sendToTarget();
        try {
            decodeThread.join(500L);
        } catch (InterruptedException e) {
            // continue
        }

        // Be absolutely sure we don't send any queued up messages
        removeMessages(R.id.decode_succeeded);
        //removeMessages(R.id.return_scan_result);
        removeMessages(R.id.decode_failed);
    }

    public void restartPreviewAndDecode() {
        if (state == State.SUCCESS) {
            state = State.PREVIEW;
            cameraManager.requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
            activity.drawViewfinder();
        }
    }

    public void restartPreview() {
        cameraManager.startPreview();
        if (state == State.SUCCESS) {
            state = State.PREVIEW;
            cameraManager.requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
            activity.drawViewfinder();
        } else if (state == State.PREVIEW) {
            cameraManager.requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
            activity.drawViewfinder();
        }
    }

}
