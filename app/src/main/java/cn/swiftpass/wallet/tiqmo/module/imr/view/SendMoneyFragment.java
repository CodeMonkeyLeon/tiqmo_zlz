package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrModeAdapter;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPaymentEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.AddBeneOnePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class SendMoneyFragment extends BaseFragment<AddBeneficiaryContract.AddOnePresenter> implements AddBeneficiaryContract.AddOneView {

    @BindView(R.id.et_destination)
    CustomizeEditText etDestination;
    @BindView(R.id.iv_bank_check)
    ImageView ivBankCheck;
    @BindView(R.id.ll_bank_account)
    LinearLayout llBankAccount;
    @BindView(R.id.iv_agent_check)
    ImageView ivAgentCheck;
    @BindView(R.id.ll_agent_cashout)
    LinearLayout llAgentCashout;
    @BindView(R.id.tv_next_step)
    TextView tvNextStep;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.ry_all_mode)
    MaxHeightRecyclerView ryAllMode;
    @BindView(R.id.ll_default_mode)
    LinearLayout ll_default_mode;

    private String transferDestinationCountryCode, receiptMethod, destinationName, callingCode, countryLogo, ibanPreCode, ibanLength, ibanSupport, accountSupport;
    private ImrCountryEntity imrCountryEntity;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;

    private String bankMode;
    private String agentMode;
    public List<String> bankSupportCurrencyCodes = new ArrayList<>();
    //机构模式支持币种
    public List<String> agentSupportCurrencyCodes = new ArrayList<>();

    public List<ImrPaymentEntity> paymentMethodList = new ArrayList<>();
    private ImrModeAdapter imrModeAdapter;

    private String channelCode,channelPayeeId;
    private ImrPaymentEntity mImrPaymentEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_send_money;
    }

    public void initUI() {
        etDestination.setText("");
        llBankAccount.setAlpha(0.4f);
        llAgentCashout.setAlpha(0.4f);
        llBankAccount.setVisibility(View.VISIBLE);
        llAgentCashout.setVisibility(View.VISIBLE);
        llBankAccount.setEnabled(false);
        llAgentCashout.setEnabled(false);
        ivAgentCheck.setVisibility(View.GONE);
        ivBankCheck.setVisibility(View.GONE);
        transferDestinationCountryCode = "";
        receiptMethod = "";
        checkNextStatus();
    }

    @Override
    protected void initView(View parentView) {
        initUI();
        etDestination.setEnabled(false);
        etDestination.setOnUnableClickListener(new CustomizeEditText.OnUnableClickListener() {
            @Override
            public void onUnableViewClick() {
                mPresenter.getDestination(Constants.imr_destination,"");
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryAllMode.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryAllMode.setLayoutManager(manager);
        imrModeAdapter = new ImrModeAdapter(paymentMethodList);
        imrModeAdapter.bindToRecyclerView(ryAllMode);
        imrModeAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                ImrPaymentEntity imrPaymentEntity = imrModeAdapter.getDataList().get(position);
                if (imrPaymentEntity != null) {
                    receiptMethod = imrPaymentEntity.paymentMode;
                    mImrPaymentEntity = imrPaymentEntity;
                    channelCode = imrPaymentEntity.channelCode;
                    channelPayeeId = imrPaymentEntity.defaultPaymentOrgCode;
                    checkNextStatus();
                    imrModeAdapter.setChoosePosition(position);
                    imrModeAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {
    }

    private void checkNextStatus() {
        if (!TextUtils.isEmpty(transferDestinationCountryCode) && !TextUtils.isEmpty(receiptMethod)) {
            tvNextStep.setEnabled(true);
        } else {
            tvNextStep.setEnabled(false);
        }
    }

    @OnClick({R.id.tv_next_step, R.id.ll_bank_account, R.id.ll_agent_cashout})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.ll_bank_account:
                llBankAccount.setAlpha(1.0f);
                ivBankCheck.setVisibility(View.VISIBLE);
                llAgentCashout.setAlpha(0.4f);
                ivAgentCheck.setVisibility(View.GONE);
                receiptMethod = Constants.imr_BT;
                checkNextStatus();
                break;
            case R.id.ll_agent_cashout:
                llAgentCashout.setAlpha(1.0f);
                ivAgentCheck.setVisibility(View.VISIBLE);
                llBankAccount.setAlpha(0.4f);
                ivBankCheck.setVisibility(View.GONE);
                receiptMethod = agentMode;
                checkNextStatus();
                break;
            case R.id.tv_next_step:
                imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();

                HashMap<String, Object> hashMap = new HashMap<>(16);
                imrBeneficiaryEntity.transferDestinationCountryCode = transferDestinationCountryCode;
                imrBeneficiaryEntity.receiptMethod = receiptMethod;
                imrBeneficiaryEntity.destinationName = destinationName;
                imrBeneficiaryEntity.callingCode = callingCode;
                imrBeneficiaryEntity.countryLogo = countryLogo;
                imrBeneficiaryEntity.ibanPreCode = ibanPreCode;
                imrBeneficiaryEntity.ibanLength = ibanLength;
                imrBeneficiaryEntity.accountSupport = accountSupport;
                imrBeneficiaryEntity.ibanSupport = ibanSupport;
                imrBeneficiaryEntity.showSaveList = true;
                imrBeneficiaryEntity.agentSupportCurrencyCodes = agentSupportCurrencyCodes;
                imrBeneficiaryEntity.bankSupportCurrencyCodes = bankSupportCurrencyCodes;
                imrBeneficiaryEntity.imrPaymentEntity = mImrPaymentEntity;
                imrBeneficiaryEntity.channelPayeeId = channelPayeeId;
                imrBeneficiaryEntity.channelCode = channelCode;
                imrBeneficiaryEntity.paymentMethodList = paymentMethodList;
                AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(imrBeneficiaryEntity);
                ActivitySkipUtil.startAnotherActivityForResult(mActivity, AddBeneficiaryTwoActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, 100);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(null);
    }

    @Override
    public void getDestinationSuccess(ImrCountryListEntity imrCountryListEntity) {
        if (imrCountryListEntity != null) {
            List<ImrCountryEntity> imrCountrysList = imrCountryListEntity.imrCountrysList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = imrCountrysList.size();
            for (int i = 0; i < size; i++) {
                ImrCountryEntity imrCountryEntity = imrCountrysList.get(i);
                if (imrCountryEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.imgUrl = imrCountryEntity.countryLogo;
                    selectInfoEntity.businessParamValue = imrCountryEntity.countryName + "  " + "+" + imrCountryEntity.callingCode;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_57), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    ll_default_mode.setVisibility(View.GONE);
                    imrCountryEntity = imrCountrysList.get(position);
                    if (imrCountryEntity != null) {
                        paymentMethodList = imrCountryEntity.paymentMethodList;

                        destinationName = imrCountryEntity.countryName;
                        etDestination.setText(destinationName);
                        transferDestinationCountryCode = imrCountryEntity.countryCode;
                        callingCode = imrCountryEntity.callingCode;
                        countryLogo = imrCountryEntity.countryLogo;
                        ibanPreCode = imrCountryEntity.ibanPreCode;
                        ibanLength = imrCountryEntity.ibanLength;
                        ibanSupport = imrCountryEntity.ibanSupport;
                        accountSupport = imrCountryEntity.accountSupport;

                        receiptMethod = "";
                        if(paymentMethodList.size() == 1){
                            receiptMethod = paymentMethodList.get(0).paymentMode;
                            imrModeAdapter.setChoosePosition(0);
                        }else {
                            imrModeAdapter.setChoosePosition(-1);
                        }
                        imrModeAdapter.setDataList(paymentMethodList);
                        bankMode = imrCountryEntity.bankMode;
                        agentMode = imrCountryEntity.agentMode;
                        bankSupportCurrencyCodes = imrCountryEntity.bankSupportCurrencyCodes;
                        agentSupportCurrencyCodes = imrCountryEntity.agentSupportCurrencyCodes;

//                        llBankAccount.setEnabled(true);
//                        llAgentCashout.setEnabled(true);
//
//                        if (TextUtils.isEmpty(bankMode)) {
//                            llBankAccount.setVisibility(View.GONE);
//                            if (!TextUtils.isEmpty(agentMode)) {
//                                receiptMethod = agentMode;
//                                llAgentCashout.setAlpha(1.0f);
//                                ivAgentCheck.setVisibility(View.VISIBLE);
//                            }
//                        } else {
//                            if (!TextUtils.isEmpty(agentMode)) {
//                                ivAgentCheck.setVisibility(View.GONE);
//                                ivBankCheck.setVisibility(View.GONE);
//                            }
//                            llBankAccount.setAlpha(1.0f);
//                            llBankAccount.setVisibility(View.VISIBLE);
//                        }
//                        if (TextUtils.isEmpty(agentMode)) {
//                            llAgentCashout.setVisibility(View.GONE);
//                            if (!TextUtils.isEmpty(bankMode)) {
//                                receiptMethod = Constants.imr_BT;
//                                llBankAccount.setAlpha(1.0f);
//                                ivBankCheck.setVisibility(View.VISIBLE);
//                            }
//                        } else {
//                            if (!TextUtils.isEmpty(bankMode)) {
//                                ivAgentCheck.setVisibility(View.GONE);
//                                ivBankCheck.setVisibility(View.GONE);
//                            }
//                            llAgentCashout.setAlpha(1.0f);
//                            llAgentCashout.setVisibility(View.VISIBLE);
//                        }
                    }
                    checkNextStatus();
                }
            });
            if (getFragmentManager() != null) {
                dialog.showNow(getFragmentManager(), "getDestinationSuccess");
            }
        }
    }

    @Override
    public void getDestinationFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddBeneficiaryContract.AddOnePresenter getPresenter() {
        return new AddBeneOnePresenter();
    }

}
