package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.adapter.ArrayWheelAdapter;
import cn.swiftpass.wallet.tiqmo.widget.wheelview.listener.OnItemSelectedListener;
import cn.swiftpass.wallet.tiqmo.widget.wheelview.view.WheelView;

public class ChooseDateDialog extends BottomDialog{
    private Context mContext;

    //1 week 2 month 3 year
    private int dateType = 1;

    private ConfirmListener mConfirmListener;

    List<String> showDates = new ArrayList<>();
    private int currentPosition;

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.mConfirmListener = confirmListener;
    }

    public ChooseDateDialog(Context context, List<String> dates,int currentPosition,int dateType) {
        super(context);
        this.mContext = context;
        showDates.clear();
        showDates.addAll(dates);
        this.currentPosition = currentPosition;
        this.dateType = dateType;
        initViews(mContext);
    }


    public interface ConfirmListener {
        void clickConfrim(int position);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_choose_date, null);
        WheelView whDate = view.findViewById(R.id.wh_date);
        TextView tvCancel = view.findViewById(R.id.tv_cancel);
        TextView tvConfirm = view.findViewById(R.id.tv_confirm);

        whDate.setCyclic(false);
        whDate.setDividerColor(mContext.getColor(R.color.color_eaeef3));
        whDate.setTextColorOut(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_30gray_979797)));
        whDate.setTextColorCenter(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)));
        whDate.setTypeface(Typeface.createFromAsset(ProjectApp.getContext().getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
        whDate.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.login_welcome_bg_color)));
        if(showDates.size()>0) {
            if(dateType == 1) {
                showDates.set(showDates.size()-1, showDates.get(showDates.size()-1) + "(" + mContext.getString(R.string.sprint17_34) + ")");
            }else if(dateType == 2){
                showDates.set(showDates.size()-1, showDates.get(showDates.size()-1) + "(" + mContext.getString(R.string.sprint17_3) + ")");
            }else{
                showDates.set(showDates.size()-1, showDates.get(showDates.size()-1) + "(" + mContext.getString(R.string.sprint17_35) + ")");
            }
        }
        ArrayWheelAdapter arrayWheelAdapter = new ArrayWheelAdapter(showDates);
        whDate.setAdapter(arrayWheelAdapter);
        whDate.setCurrentItem(currentPosition);
        whDate.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                currentPosition = index;
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mConfirmListener != null){
                    mConfirmListener.clickConfrim(currentPosition);
                }
                dismiss();
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
