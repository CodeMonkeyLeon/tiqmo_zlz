package cn.swiftpass.wallet.tiqmo.module.register.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.NafathContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.NafathRegPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;

public class NafathResultActivity extends BaseCompatActivity<NafathContract.Presenter> implements NafathContract.View {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.ll_login)
    ConstraintLayout llLogin;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_content_title)
    MyTextView tvContentTitle;
    @BindView(R.id.tv_random)
    TextView tvRandom;
    @BindView(R.id.tv_show_time_expire)
    MyTextView tvShowTimeExpire;
    @BindView(R.id.tv_otp_time)
    MyTextView tvOtpTime;
    @BindView(R.id.ll_otp_time)
    LinearLayout llOtpTime;
    @BindView(R.id.tv_try_again)
    MyTextView tvTryAgain;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private String hashedState, recipientId, authUrl, redirectUrl, noStr;
    private String phoneNo;
    private NafathUrlEntity nafathUrlEntity;
    private CheckPhoneEntity checkPhoneEntity;
    private int operateType;
    private String random;
    private long countSeconds;

    /**
     * 强制停止Nafath结果查询
     */
    private boolean isForceStopQuery;
    /**
     * Nafath结果查询 倒计时查询结果
     */
    private Timer queryNafathResultTimer;
    private int retainQueryTime = 60;
    private int retainQueryTimeDelay = 15;

    private Handler queryHandler;
    private String otpType;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_nafath_result;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        ivBack.setVisibility(View.GONE);
        if (getIntent() != null && getIntent().getExtras() != null) {
            otpType = getIntent().getExtras().getString(Constants.DATA_OTP_TYPE);
            if (Constants.OTP_REG.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_12);
            } else if (Constants.OTP_FORGET_LOGIN_PD.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_31);
            }else if (Constants.OTP_CHANGE_PHONE.equals(otpType)) {
                tvTitle.setText(R.string.ChangeMobile_1);
            }
        }
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if (checkPhoneEntity != null) {
            phoneNo = checkPhoneEntity.phone;
            noStr = checkPhoneEntity.noStr;
            recipientId = checkPhoneEntity.checkIdNumber;
            authUrl = checkPhoneEntity.authUrl;
            hashedState = checkPhoneEntity.hashedState;
            redirectUrl = checkPhoneEntity.redirectUrl;
            operateType = checkPhoneEntity.operateType;
            random = checkPhoneEntity.random;
            countSeconds = checkPhoneEntity.countSeconds;
            setResetTime(true, phoneNo, (int) countSeconds);
            userInfoEntity = getUserInfoEntity();
            if (!TextUtils.isEmpty(phoneNo)) {
                userInfoEntity.phone = phoneNo;
            }
            if(!TextUtils.isEmpty(random)){
                tvRandom.setText(random);
            }
        }
        queryHandler = new Handler();

        isForceStopQuery = false;
        //开始倒计时查询结果
        queryNafathResultTimer = new Timer();
        TimerTask queryVideoResultTimerTask = new TimerTask() {
            @Override
            public void run() {
                queryHandler.post(queryVideoResultRunnable);
            }
        };
        queryNafathResultTimer.schedule(queryVideoResultTimerTask, 10000, 10000);
    }

    public void setResetTime(boolean isReset, String phoneNo, int time) {
        tvTryAgain.setClickable(false);
        tvTryAgain.setEnabled(false);
        AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, time, new AdvancedCountdownTimer.OnCountDownListener() {
            @Override
            public void onTick(int millisUntilFinished) {
                if (isFinishing() || tvTryAgain == null) {
                    return;
                }

                tvShowTimeExpire.setText(getString(R.string.sprint20_82));
                tvOtpTime.setVisibility(View.VISIBLE);
                if (LocaleUtils.isRTL(mContext)) {
                    tvOtpTime.setText("s " + millisUntilFinished);
                } else {
                    tvOtpTime.setText(millisUntilFinished + " s");
                }

                tvTryAgain.setClickable(false);
                tvTryAgain.setEnabled(false);
            }

            @Override
            public void onFinish() {
                if (isFinishing() || tvTryAgain == null) {
                    return;
                }
                tvShowTimeExpire.setText(getString(R.string.sprint20_82));
                tvOtpTime.setVisibility(View.VISIBLE);
                if (LocaleUtils.isRTL(mContext)) {
                    tvOtpTime.setText("s " + 0);
                } else {
                    tvOtpTime.setText(0 + " s");
                }
                tvTryAgain.setClickable(true);
                tvTryAgain.setEnabled(true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }

    @OnClick({R.id.tv_try_again, R.id.tv_login, R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_verify:
                mPresenter.getNafathUrl(UserInfoManager.getInstance().getCallingCode(),phoneNo,recipientId,operateType==2? Constants.SCENE_TYPE_FORGET_PD:Constants.SCENE_TYPE_REG_PHONE);
                break;
            case R.id.tv_login:
                if (getUserInfoEntity() != null) {
                    ActivitySkipUtil.startAnotherActivity(this, LoginFastNewActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    ActivitySkipUtil.startAnotherActivity(this, RegisterActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeNafathCall();
    }

    private void removeNafathCall() {
        if (queryNafathResultTimer != null) {
            queryNafathResultTimer.cancel();
        }
        if (queryHandler != null && queryVideoResultRunnable != null) {
            queryHandler.removeCallbacks(queryVideoResultRunnable);
        }
        isForceStopQuery = true;
    }

    /**
     * 轮训查询Ivr结果
     */
    private Runnable queryVideoResultRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isForceStopQuery) {
                getNafathResult();
            }
        }
    };

    private void getNafathResult() {
        mPresenter.getNafathResult(UserInfoManager.getInstance().getCallingCode(), phoneNo, hashedState, recipientId, random, operateType == 2 ? Constants.SCENE_TYPE_FORGET_PD : Constants.SCENE_TYPE_REG_PHONE);
    }

    @Override
    public void getNafathResultSuccess(NafathResultEntity nafathResultEntity) {
        if(nafathResultEntity != null){
            String authStatus = nafathResultEntity.authStatus;
            if(nafathResultEntity.isAuthSuccess()){
                removeNafathCall();
                if(operateType == 3){
                    //修改手机密码 更新用户信息
                    if (checkPhoneEntity != null) {
                        String changePhoneOrderNo = checkPhoneEntity.changePhoneOrderNo;
                        if(!TextUtils.isEmpty(changePhoneOrderNo)){
                            userInfoEntity.orderNo = changePhoneOrderNo;
                        }
                        AppClient.getInstance().getUserManager().updateUserInfo(userInfoEntity, new ResultCallback<UserInfoEntity>() {
                            @Override
                            public void onResult(UserInfoEntity userInfoEntity) {
                                showProgress(false);
                                ProjectApp.changePhoneSuccess = true;
                                ProjectApp.removeAllTaskExcludeMainStack();
                                finish();
                            }

                            @Override
                            public void onFailure(String errorCode, String errorMsg) {
                                showProgress(false);
                                showTipDialog(errorMsg);
                            }
                        });
                    }

                }else {
                    String firstName = nafathResultEntity.firstName;
                    String birthday = nafathResultEntity.birthday;
                    if (checkPhoneEntity != null) {
                        checkPhoneEntity.firstName = firstName;
                        if (!TextUtils.isEmpty(birthday)) {
                            checkPhoneEntity.birthday = birthday;
                        }
                        UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                    }
                    ActivitySkipUtil.startAnotherActivity(NafathResultActivity.this, RegSetPwdOneActivity.class, RIGHT_IN);
                    ProjectApp.removeNafathTask();
                    finish();
                }
            }else if(nafathResultEntity.isAuthFail()) {
                removeNafathCall();
                String failType = nafathResultEntity.failType;
                if (!TextUtils.isEmpty(failType)) {
                    EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_NAFATH_FAIL, failType));
                }
                ProjectApp.removeNafathTask();
                finish();
            }
        }
    }

    @Override
    public void getNafathUrlSuccess(NafathUrlEntity nafathUrlEntity) {
        if(nafathUrlEntity != null){
            String authUrl = nafathUrlEntity.authUrl;
            String hashedState = nafathUrlEntity.hashedState;
            String redirectUrl = nafathUrlEntity.redirectUrl;
            String nafathAuthFlag = nafathUrlEntity.nafathAuthFlag;
            String firstName = nafathUrlEntity.firstName;
            String random = nafathUrlEntity.random;
            long countSeconds = nafathUrlEntity.countSeconds;
            if(TextUtils.isEmpty(random)){
                tvRandom.setText(random);
                tvRandom.setBackgroundResource(R.drawable.shape_46d39a_12);
                tvRandom.setTextColor(getColor(R.color.color_46d39a));
            }
            setResetTime(true, phoneNo, (int) countSeconds);
        }
    }

    @Override
    public void getNafathUrlFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getNafathResultFail(String errorCode, String errorMsg) {
        tvRandom.setBackgroundResource(R.drawable.shape_db4d4d_12);
        tvRandom.setTextColor(getColor(R.color.color_db4d4d));
    }

    @Override
    public NafathContract.Presenter getPresenter() {
        return new NafathRegPresenter();
    }
}
