package cn.swiftpass.wallet.tiqmo.module.topup.presenter;

import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RechargeCountryPresenter implements RechargeContract.RechargeCountryPresenter{

    RechargeContract.RechargeCountryView mRechargeCountryView;

    @Override
    public void getRechargeCountry() {
        AppClient.getInstance().getTransferManager().getRechargeCountryList(new LifecycleMVPResultCallback<RechargeCountryListEntity>(mRechargeCountryView,true) {
            @Override
            protected void onSuccess(RechargeCountryListEntity result) {
                mRechargeCountryView.getRechargeCountrySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeCountryView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getRechargeOperator(String phoneNumber) {
        AppClient.getInstance().getTransferManager().getRechargeOperatorList(phoneNumber, new LifecycleMVPResultCallback<RechargeOperatorListEntity>(mRechargeCountryView,true) {
            @Override
            protected void onSuccess(RechargeOperatorListEntity result) {
                mRechargeCountryView.getRechargeOperatorSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeCountryView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(RechargeContract.RechargeCountryView rechargeCountryView) {
        this.mRechargeCountryView = rechargeCountryView;
    }

    @Override
    public void detachView() {
        this.mRechargeCountryView = null;
    }
}
