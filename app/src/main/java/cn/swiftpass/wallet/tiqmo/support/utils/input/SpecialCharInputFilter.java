package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;

/**
 * 整数数字过滤器
 * created by junhua on 2019/7/24 13:54
 */
public class SpecialCharInputFilter implements InputFilter {
    private Pattern mPattern = Pattern.compile("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]*");

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (mPattern.matcher(temp).matches()) {
            return source;
        }
        return "";
    }
}
