package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RegionEntity extends BaseEntity {

    public ArrayList<RegionInfo> neighborhoodsInfoList;

    public class RegionInfo extends BaseEntity{
        public String neighborhoodsId;
        public String neighborhoodsName;
        public String neCoordinates;
        public String swCoordinates;
    }
}
