package cn.swiftpass.wallet.tiqmo.module.transfer.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveTransferDetail;

public class SendReceiveContract {

    public interface SendReceivePresenter extends BasePresenter<SendReceiveContract.SendReceiveView> {
        void getTransferPeopleDetail(String groupId,String isGroup);
        void SendReceiveDetail(String groupId,String tradeType,String isGroup,int pageNum,int pageSize);
        void getLimit(String type);
    }

    public interface SendReceiveView extends BaseView<SendReceiveContract.SendReceivePresenter> {

        void getTransferPeopleDetailSuccess(SendReceiveTransferDetail rustlt);
        void SendReceiveDetailSuccess(SendReceiveDetailListEntity listEntity);
        void SendReceiveDetailFail(String errorCode, String errorMsg);
        void getLimitSuccess(TransferLimitEntity transferLimitEntity);
        void showError(String errorCode, String errorMsg);
    }
}
