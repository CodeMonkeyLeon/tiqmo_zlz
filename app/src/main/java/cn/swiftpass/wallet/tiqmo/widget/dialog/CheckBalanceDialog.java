package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;

public class CheckBalanceDialog extends BottomDialog {


    public CheckBalanceDialog(Context context, String balance) {
        super(context);
        initViews(context, balance);
    }

    private void initViews(Context context, String balance) {
        View mView = LayoutInflater.from(context).inflate(R.layout.dialog_check_balance, null);
        TextView tvBalance = mView.findViewById(R.id.tv_balance);

        if (!TextUtils.isEmpty(balance)) {
            tvBalance.setText(Spans.builder()
                    .text(balance + " ").color(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(context.getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                    .text(LocaleUtils.getCurrencyCode("")).color(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_50white_95979d))).size(12)
                    .build());
        }
        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
