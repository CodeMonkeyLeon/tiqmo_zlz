package cn.swiftpass.wallet.tiqmo.support.utils.constants;

public enum ErrorCode {
    /**
     * 链接超时
     */
    CONTENT_TIME_OUT("1001", -1);


    public final String code;
    public final int msgId;

    private ErrorCode(String code, int msgResId) {
        this.code = code;
        this.msgId = msgResId;
    }
}
