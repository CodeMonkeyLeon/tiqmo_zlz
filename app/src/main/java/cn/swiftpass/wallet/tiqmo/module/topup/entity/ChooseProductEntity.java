package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChooseProductEntity extends BaseEntity {
    //服务类型
    public String serviceType;
    //面额
    public String amount;
    //产品ID
    public String productId;
    //产品名称 也做过国际化
    public String productName;
    //目标币种
    public String destinationCurrencyCode;
    //运营商ID
    public String operatorId;
    //产品类型
    public String productType;
    //产品描述
    public String productDescription;
    //目标金额
    public String destinationAmount;
    //描述，展示的标题，已做国际化
    public String description;
    //运营商名称，已做国际化
    public String operatorName;
}
