package cn.swiftpass.wallet.tiqmo.sdk.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by YZX on 2017年11月17日.
 * 每一个不曾起舞的日子,都是对生命的辜负.
 */

//利用泛型封装了数据库的操作，抽象了一个dao，封装了常见的增删查改方法，子类需要实现对应的抽象方法
//具体使用方法可以查看InternationalCodeDao类使用
public abstract class AbstractDao<T> {

    protected static final String COLUMN_NAME_RowID = "ROWID";

    protected ReadWriteHelper mReadWriteHelper;

    //获取表名，子类实现
    protected abstract String getTableName();

    //获取查询条件，该条件决定唯一一条数据，子类实现,要和toWhereArgsOfKey()的顺序一致
    protected abstract String getWhereClauseOfKey();

    //返回对应查询条件的数据，从实体类中，与getWhereClauseOfKey()对应
    protected abstract String[] toWhereArgsOfKey(T entity);

    //将实体类的内容填充到ContentValues
    protected abstract void parseToContentValues(T entity, ContentValues values);

    //将Cursor的数据变成实体类
    protected abstract T toEntity(Cursor cursor);

    //这里有一个读写助手，详情见DBHelper类
    public AbstractDao(ReadWriteHelper readWriteHelper) {
        mReadWriteHelper = readWriteHelper;
    }

    //通过主键查询数据并转成实体类
    public T loadByKey(String... keyValues) {
        if (keyValues == null || keyValues.length == 0) {
            return null;
        }
        SQLiteDatabase database = mReadWriteHelper.openReadableDatabase();
        Cursor cursor = database.query(getTableName(), null, getWhereClauseOfKey(), keyValues, null, null, null);
        T entity = null;
        if (cursor.moveToFirst()) {
            entity = toEntity(cursor);
        }
        cursor.close();
        mReadWriteHelper.closeReadableDatabase();
        return entity;
    }

    //把实体类的数据插入数据库
    public boolean insert(T entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values =new ContentValues();
        parseToContentValues(entity, values);
        boolean result = mReadWriteHelper.openWritableDatabase().insert(getTableName(), null, values) > 0;
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    public boolean insertAll(Iterable<T> entityIterable) {
        if (entityIterable == null) {
            return false;
        }
        boolean result = true;
        SQLiteDatabase database = mReadWriteHelper.openWritableDatabase();
        database.beginTransactionNonExclusive();
        try {
            ContentValues values = new ContentValues();
            for (T entity : entityIterable) {
                values.clear();
                parseToContentValues(entity, values);
                if (database.replace(getTableName(), null, values) <= 0) {
                    result = false;
                    break;
                }
            }
            if (result) {
                database.setTransactionSuccessful();
            }
        } finally {
            database.endTransaction();
        }
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    //把实体类的数据替换到数据库
    public boolean replace(T entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values =new ContentValues();
        parseToContentValues(entity, values);
        boolean result = mReadWriteHelper.openWritableDatabase().replace(getTableName(), null, values) > 0;
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    public boolean replaceAll(Iterable<T> entityIterable) {
        if (entityIterable == null) {
            return false;
        }
        boolean result = true;
        SQLiteDatabase database = mReadWriteHelper.openWritableDatabase();
        database.beginTransactionNonExclusive();
        try {
            ContentValues values = new ContentValues();
            for (T entity : entityIterable) {
                values.clear();
                parseToContentValues(entity, values);
                if (database.replace(getTableName(), null, values) <= 0) {
                    result = false;
                    break;
                }
            }
            if (result) {
                database.setTransactionSuccessful();
            }
        } finally {
            database.endTransaction();
        }
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    //把实体类的数据更新到数据库
    public boolean update(T entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values =new ContentValues();
        parseToContentValues(entity, values);
        boolean result = mReadWriteHelper.openWritableDatabase().update(getTableName(), values, getWhereClauseOfKey(), toWhereArgsOfKey(entity)) > 0;
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    public boolean updateAll(Iterable<T> entityIterable) {
        if (entityIterable == null) {
            return false;
        }
        boolean result = true;
        SQLiteDatabase database = mReadWriteHelper.openWritableDatabase();
        database.beginTransactionNonExclusive();
        try {
            ContentValues values = new ContentValues();
            for (T entity : entityIterable) {
                values.clear();
                parseToContentValues(entity, values);
                if (database.update(getTableName(), values, getWhereClauseOfKey(), toWhereArgsOfKey(entity)) <= 0) {
                    result = false;
                    break;
                }
            }
            if (result) {
                database.setTransactionSuccessful();
            }
        } finally {
            database.endTransaction();
        }
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    //把实体类的数据从数据库中删除，通过实体类能找到ID值，就可以删除数据啦
    public boolean delete(T entity) {
        if (entity == null) {
            return false;
        }
        boolean result = mReadWriteHelper.openWritableDatabase().delete(getTableName(), getWhereClauseOfKey(), toWhereArgsOfKey(entity)) > 0;
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    public boolean deleteByKey(String... keyValue) {
        if (keyValue == null || keyValue.length == 0) {
            return false;
        }
        boolean result = mReadWriteHelper.openWritableDatabase().delete(getTableName(), getWhereClauseOfKey(), keyValue) > 0;
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    public boolean deleteAll(Iterable<T> entityIterable) {
        if (entityIterable == null) {
            return false;
        }
        boolean result = true;
        SQLiteDatabase database = mReadWriteHelper.openWritableDatabase();
        database.beginTransactionNonExclusive();
        try {
            for (T entity : entityIterable) {
                if (database.delete(getTableName(), getWhereClauseOfKey(), toWhereArgsOfKey(entity)) <= 0) {
                    result = false;
                    break;
                }
            }
            if (result) {
                database.setTransactionSuccessful();
            }
        } finally {
            database.endTransaction();
        }
        mReadWriteHelper.closeWritableDatabase();
        return result;
    }

    public void cleanTable() {
        SQLiteDatabase database = mReadWriteHelper.openWritableDatabase();
        database.execSQL("delete from " + getTableName());
        mReadWriteHelper.closeWritableDatabase();
    }

    //判断数据是否存在
    public boolean isExist(T entity) {
        if (entity == null) {
            return false;
        }
        SQLiteDatabase database = mReadWriteHelper.openReadableDatabase();
        Cursor cursor = database.query(getTableName(), null, getWhereClauseOfKey(), toWhereArgsOfKey(entity), null, null, null, null);
        boolean result = (cursor.getCount() > 0);
        cursor.close();
        mReadWriteHelper.closeReadableDatabase();
        return result;
    }

    public interface ReadWriteHelper {
        SQLiteDatabase openReadableDatabase();

        SQLiteDatabase openWritableDatabase();

        void closeReadableDatabase();

        void closeWritableDatabase();
    }
}
