package cn.swiftpass.wallet.tiqmo.support.theme;

import android.content.Context;
import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.R;

public class ThemeSourceUtils {

    public static int getDefAvatar(Context context, String gender){
        if (TextUtils.isEmpty(gender)){
            return ResourceHelper.getInstance(context).getIdentifierByAttrId(R.attr.avatar_male);
        }
        return "1".equals(gender) ?
                ResourceHelper.getInstance(context).getIdentifierByAttrId(R.attr.avatar_female) :
                ResourceHelper.getInstance(context).getIdentifierByAttrId(R.attr.avatar_male);
    }


    public static int getSourceID(Context context, int attr){
        return ResourceHelper.getInstance(context).getIdentifierByAttrId(attr);
    }

}
