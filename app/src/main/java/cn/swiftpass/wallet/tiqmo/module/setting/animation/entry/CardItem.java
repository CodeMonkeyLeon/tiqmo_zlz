package cn.swiftpass.wallet.tiqmo.module.setting.animation.entry;

import android.view.View;

public class CardItem {
    public View view;
    public float zIndex;
    public int adapterIndex;

    public CardItem(View view, float zIndex, int adapterIndex) {
        this.view = view;
        this.zIndex = zIndex;
        this.adapterIndex = adapterIndex;
    }

    @Override
    public int hashCode() {
        return view.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CardItem && view.equals(((CardItem) obj).view);
    }

    @Override
    public String toString() {
        return "CardItem{" +
                "view=" + view +
                ", zIndex=" + zIndex +
                ", adapterIndex=" + adapterIndex +
                '}';
    }
}
