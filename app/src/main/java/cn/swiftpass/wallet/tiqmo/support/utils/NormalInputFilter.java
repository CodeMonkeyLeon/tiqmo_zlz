package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;


public class NormalInputFilter implements InputFilter {
    public static final int CHARSEQUENCE_NUMBER = 901;
    public static final int CHINESE = 902;
    public static final int CHARSEQUENCE_CHINESE__NUMBER = 903;
    public static final int CHARSEQUENCE_CHINESE__NUMBER_SPECIAL = 904;
    public static final int CHARSEQUENCE__NUMBER_SPECIAL = 905;
    public static final int CHINESE__NUMBER_SPECIAL = 906;
    public static final int CHARSEQUENCE__NUMBER_SPACE = 907;
    public static final int CHARSEQUENCE_NUMBER_SPACE_HENGGANG = 9077;
    public static final int CHINESE__NUMBER_SPACE = 908;
    public static final int CHARSEQUENCE_CHINESE__NUMBER_SPACE = 909;
    public static final int CHARSEQUENCE_CHINESE__NUMBER_SPECIAL_SPACE = 910;
    public static final int CHARSEQUENCE__NUMBER_SPECIAL_SPACE = 911;
    public static final int CHINESE_SPECIAL_SPACE = 912;
    public static final int CHINESE_NUMBER_SPECIAL_SPACE = 913;
    public static final int NUMBER = 914;
    public static final int CHARSEQUENCE_SPACE = 915;
    public static final int CHARSEQUENCE_SPACE_HENGGANG = 916;
    public static final int AR_ENGLISH = 917;
    public static final int MONEY = 9166;
    public static final int AR_ENGLISH_SPACE = 918;
    private final String STR_CHARSEQUENCE = "A-Za-z";
    //支持阿拉伯语和英文
//    private final String STR_AR_ENGLISH = "^[\\u0621-\\u064Aa-zA-Z\\d\\-_\\s]+$";
    private final String STR_AR_ENGLISH = "^[\\u0621-\\u064Aa-zA-Z\\s]+$";
    private final String STR_AR = "\\u0621-\\u064A";

    private final String STR_SPACE = " ";
    private final String STR_HENGGANG = "-";
    private final String STR_NUMBER = "0-9";
    private final String STR_MONEY_SPECIAL = ".,";
    private final String STR_CHINESE = "\\u4E00-\\u9FFF";
    private final String STR_SPECIAL = "`~!\\-_@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？";

    private Pattern mPattern = Pattern.compile("[A-Za-z0-9\\u4E00-\\u9FFF`~!@#$%^&*()\\-_+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]*");

    public NormalInputFilter(int type) {
        switch (type) {
            case MONEY:
                mPattern = Pattern.compile("[" + STR_NUMBER + STR_MONEY_SPECIAL + "]*");
                break;
            case CHARSEQUENCE_NUMBER:
                mPattern = Pattern.compile("["+ STR_AR + STR_CHARSEQUENCE + STR_NUMBER + "]*");
                break;
            case CHARSEQUENCE_SPACE:
                mPattern = Pattern.compile("["+ STR_AR + STR_CHARSEQUENCE + STR_SPACE + "]*");
                break;
            case NUMBER:
                mPattern = Pattern.compile("[" + STR_NUMBER + "]*");
                break;
            case CHINESE:
                mPattern = Pattern.compile("[" + STR_CHINESE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_CHINESE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_CHINESE + STR_SPECIAL + "]*");
                break;
            case CHARSEQUENCE__NUMBER_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_SPECIAL + "]*");
                break;
            case CHINESE__NUMBER_SPECIAL:
                mPattern = Pattern.compile("[" + STR_SPECIAL + "]*");
                break;
            case CHARSEQUENCE__NUMBER_SPACE:
                mPattern = Pattern.compile("["+ STR_AR + STR_CHARSEQUENCE + STR_NUMBER + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_SPACE_HENGGANG:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_SPACE + STR_HENGGANG + "]*");
                break;
            case CHARSEQUENCE_NUMBER_SPACE_HENGGANG:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_SPACE + STR_HENGGANG + "]*");
                break;
            case CHINESE__NUMBER_SPACE:
                mPattern = Pattern.compile("[" + STR_CHINESE + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER_SPACE:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_CHINESE + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_CHINESE + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE__NUMBER_SPECIAL_SPACE:
                mPattern = Pattern.compile("["+ STR_AR + STR_CHARSEQUENCE + STR_NUMBER + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHINESE_NUMBER_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHINESE + STR_SPECIAL + STR_SPACE + STR_NUMBER + "]*");
                break;
            case AR_ENGLISH:
                mPattern = Pattern.compile(STR_AR_ENGLISH);
                break;
            case AR_ENGLISH_SPACE:
                mPattern = Pattern.compile("[" + STR_AR + STR_SPACE + STR_CHARSEQUENCE + "]*");
                break;
            default:
                break;
        }
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (mPattern.matcher(temp).matches()) {
            return source;
        }
        return "";
    }
}
