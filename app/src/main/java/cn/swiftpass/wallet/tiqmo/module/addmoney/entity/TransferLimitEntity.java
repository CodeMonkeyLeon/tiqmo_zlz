package cn.swiftpass.wallet.tiqmo.module.addmoney.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;

public class TransferLimitEntity extends BaseEntity {

    /**
     * 单笔最大限额，单位最小币种，为空代表不限制
     */
    public String tranLimitAmt;
    /**
     * 单日最大限额，单位最小币种，为空代表不限制
     */
    public String dayLimitAmt;
    /**
     * 单月最大限额，单位最小币种，为空代表不限制
     */
    public String monthLimitAmt;
    /**
     * 单笔最小限额，单位最小币种，为空代表不限制
     */
    public String tranMinLimitAmt;
    /**
     * 单月总限额，单位最小币种，为空代表不限制
     */
    public String totalMonthLimitAmt;
    //可用余额
    public String availableBalance;
    /**
     * 最大账户限额
     */
    public String maxLimitBalance = "1000000000";

    /**
     * 是否大于等于单笔最小限额
     * @return
     */
    public boolean isOverTranMinLimitAmt(String money){
        if(TextUtils.isEmpty(tranMinLimitAmt))return true;
        String tranMinLimitAmount = AndroidUtils.getTransferStringMoney(tranMinLimitAmt);
        return BigDecimalFormatUtils.compare(money,tranMinLimitAmount);
    }
}
