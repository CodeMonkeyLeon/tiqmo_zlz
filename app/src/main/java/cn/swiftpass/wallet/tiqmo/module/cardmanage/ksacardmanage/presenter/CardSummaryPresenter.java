package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class CardSummaryPresenter implements CardKsaContract.GetCardSummaryPresenter {

    private CardKsaContract.GetCardSummaryView mGetCardSummaryView;

    @Override
    public void getKsaCardSummary(String cardType,String cardProductType) {
        AppClient.getInstance().getCardManager().getKsaCardSummary(cardType,cardProductType, new LifecycleMVPResultCallback<KsaCardSummaryEntity>(mGetCardSummaryView,true) {
            @Override
            protected void onSuccess(KsaCardSummaryEntity result) {
                mGetCardSummaryView.getKsaCardSummarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mGetCardSummaryView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

//    @Override
//    public void openKsaCardNoFee(OpenCardReqEntity openCardReqEntity) {
//        AppClient.getInstance().getCardManager().openKsaCardNoFee(openCardReqEntity.cardProductType, openCardReqEntity.cardType, openCardReqEntity.cardFaceId,
//                openCardReqEntity.province, openCardReqEntity.city, openCardReqEntity.address, openCardReqEntity.pinBean,openCardReqEntity.cardOfName, new LifecycleMVPResultCallback<KsaPayResultEntity>(mGetCardSummaryView,true) {
//            @Override
//            protected void onSuccess(KsaPayResultEntity result) {
//                mGetCardSummaryView.openKsaCardNoFeeSuccess(result);
//            }
//
//            @Override
//            protected void onFail(String errorCode, String errorMsg) {
//                mGetCardSummaryView.showErrorMsg(errorCode,errorMsg);
//            }
//        });
//    }

    @Override
    public void getKsaCardPayResult(String vat, String totalAmount, OpenCardReqEntity appNiOpenCardReq) {
        AppClient.getInstance().getCardManager().getKsaCardPayResult(vat, totalAmount, appNiOpenCardReq, new LifecycleMVPResultCallback<KsaPayResultEntity>(mGetCardSummaryView,true) {
            @Override
            protected void onSuccess(KsaPayResultEntity result) {
                mGetCardSummaryView.getKsaCardPayResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mGetCardSummaryView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardPin(String pin, String proxyCardNo) {
        AppClient.getInstance().getCardManager().setKsaCardPin(pin, proxyCardNo, new LifecycleMVPResultCallback<Void>(mGetCardSummaryView,true) {
            @Override
            protected void onSuccess(Void result) {
                mGetCardSummaryView.setKsaCardPinSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mGetCardSummaryView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardKsaContract.GetCardSummaryView getCardSummaryView) {
        this.mGetCardSummaryView = getCardSummaryView;
    }

    @Override
    public void detachView() {
        this.mGetCardSummaryView = null;
    }

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mGetCardSummaryView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mGetCardSummaryView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mGetCardSummaryView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }
}
