package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferPwdTimeEntity extends BaseEntity {

    public String lockTime;
    public String errMsg;
}
