package cn.swiftpass.wallet.tiqmo.support.theme;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import cn.swiftpass.wallet.tiqmo.R;

public class ThemeUtils {
    private static final String KEY_CACHE_LOCAL = "cache_local";

    private static final String KEY_CACHE_THEME_TAG = "cache_theme_tag";

    // theme tag
    public static final String THEME_LIGHT = "0";
    public static final String THEME_DARK = "1";

    /**
     * 获取缓存的主题Tag
     */
    public static String getThemeTag(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences preferences = context.getSharedPreferences(KEY_CACHE_LOCAL, Context.MODE_PRIVATE);
        return preferences.getString(KEY_CACHE_THEME_TAG, THEME_DARK);
//        return THEME_LIGHT;
    }

    /**
     * 缓存当前的主题Tag
     */
    public static void setThemeTag(Context context, String tag) {
        SharedPreferences preferences = context.getSharedPreferences(KEY_CACHE_LOCAL, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(KEY_CACHE_THEME_TAG, tag);
        edit.commit();
    }

    /**
     * 主题替换
     */
    public static void switchCurrentThemeTag(Context context) {
        if (context == null || TextUtils.isEmpty(getThemeTag(context))) {
            return;
        }
        setThemeTag(context, getThemeTag(context).equals(THEME_LIGHT)? THEME_DARK : THEME_LIGHT);
        loadingCurrentTheme(context, true);
    }

    /**
     * 根据tag设置主题
     */
    public static void loadingCurrentTheme(Context context, boolean isSwitch) {
        if (isCurrentDark(context)){
            context.setTheme(R.style.MultiTheme_Dark);
        } else {
            context.setTheme(R.style.MultiTheme_Light);
        }
        // 每次switchTheme都需要提示observers更新界面
        if (isSwitch) {
            ThemeChangeObserverStack.getInstance().notifyByThemeChanged();
        }
    }

    /**
     * 判断当前是否是暗色主题
     */
    public static boolean isCurrentDark(Context context) {
        return ThemeUtils.getThemeTag(context).equals(ThemeUtils.THEME_DARK);
    }

    public static String getThemeCode(Context context) {
        return isCurrentDark(context) ? "dark" : "light";
    }

    public static String getCurrentTheme(Context context) {
        if (THEME_DARK.equals(getThemeTag(context))) {
            return "dark";
        } else {
            return "white";
        }
    }

    /**
     * 设置状态栏为透明
     *
     * @param activity
     */
    @TargetApi(19)
    public static void setTranslucentStatus(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            int uiStyle;
            if (ThemeUtils.isCurrentDark(activity)) {
                uiStyle = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_VISIBLE;
            } else {
                uiStyle = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            }
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(uiStyle);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = activity.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
