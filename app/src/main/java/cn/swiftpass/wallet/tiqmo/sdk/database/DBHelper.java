package cn.swiftpass.wallet.tiqmo.sdk.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

//这里对SQLiteDatabase封装了读写锁操作，就是为了复用一个SQLiteDatabase，而不是频繁的open和close，增加性能
//内部实现使用了读写锁
public class DBHelper {

    private AppSQLiteOpenHelper mHelper;
    private SQLiteDatabase mReadWriteDatabase;
    private final Lock mReadLock;
    private final Lock mWriteLock;

    public DBHelper(Context context, String name, int version) {
        mHelper = new AppSQLiteOpenHelper(context, name, version);
        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
        mReadWriteDatabase = mHelper.getWritableDatabase();
        mReadLock = readWriteLock.readLock();
        mWriteLock = readWriteLock.writeLock();
    }

    public AbstractDao.ReadWriteHelper getReadWriteHelper() {
        return mReadWriteHelper;
    }

    //这个方法目前是用不到，因为APP整个生命周期都会用到数据库
    public void destroy() {
        mWriteLock.lock();
        if (mReadWriteDatabase != null) {
            mReadWriteDatabase.close();
            mReadWriteDatabase = null;
        }
        if (mHelper != null) {
            mHelper.close();
        }
        mWriteLock.unlock();
    }


    //这里就抽象了一个ReadWriteHelper，通过ReadWriteHelper去获取SQLiteDatabase，内部封装了复用的实现
    private final AbstractDao.ReadWriteHelper mReadWriteHelper = new AbstractDao.ReadWriteHelper() {


        @Override
        public SQLiteDatabase openReadableDatabase() {
            if (mReadWriteDatabase == null) {
                throw new RuntimeException("Database already destroy");
            }
            mReadLock.lock();
            return mReadWriteDatabase;
        }

        @Override
        public SQLiteDatabase openWritableDatabase() {
            if (mReadWriteDatabase == null) {
                throw new RuntimeException("Database already destroy");
            }
            mWriteLock.lock();
            return mReadWriteDatabase;
        }

        @Override
        public void closeReadableDatabase() {
            if (mReadWriteDatabase == null) {
                throw new RuntimeException("Database already destroy");
            }
            mReadLock.unlock();
        }

        @Override
        public void closeWritableDatabase() {
            if (mReadWriteDatabase == null) {
                throw new RuntimeException("Database already destroy");
            }
            mWriteLock.unlock();
        }
    };

}
