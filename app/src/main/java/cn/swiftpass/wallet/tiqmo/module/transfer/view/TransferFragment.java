package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrMainActivity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.TransferContactAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.RecentContactPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.HomeBottomView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class TransferFragment extends BaseFragment<TransferContract.RecentContactPresenter> implements TransferContract.RecentContactView {
    @BindView(R.id.tv_transfer_title)
    TextView tvTransferTitle;
    @BindView(R.id.tv_transfer_desc)
    TextView tvTransferDesc;
    @BindView(R.id.iv_background)
    ImageView ivBackground;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_one_contacts)
    TextView tv_one_contacts;
    @BindView(R.id.tv_two_contacts)
    TextView tv_two_contacts;
    @BindView(R.id.homeview)
    HomeBottomView homeview;
    @BindView(R.id.ll_one_contracts)
    LinearLayout llOneContracts;
    @BindView(R.id.rl_one_contacts)
    RecyclerView rlOneContacts;
    @BindView(R.id.ll_one_all)
    LinearLayout llOneAll;
    @BindView(R.id.ll_one)
    LinearLayout llOne;
    @BindView(R.id.ll_two_contacts)
    LinearLayout llTwoContacts;
    @BindView(R.id.rl_two_contacts)
    RecyclerView rlTwoContacts;
    @BindView(R.id.ll_two_all)
    LinearLayout llTwoAll;
    @BindView(R.id.ll_two)
    LinearLayout llTwo;
    @BindView(R.id.ll_three_contacts)
    LinearLayout llThreeContacts;
    @BindView(R.id.rl_three_contacts)
    RecyclerView rlThreeContacts;
    @BindView(R.id.ll_three_all)
    LinearLayout llThreeAll;
    @BindView(R.id.ll_three)
    LinearLayout llThree;
    @BindView(R.id.ll_qr_code)
    LinearLayout llQrCode;
    @BindView(R.id.ll_transfer_contacts)
    LinearLayout llTransferContacts;
    @BindView(R.id.ll_request_transfer)
    LinearLayout llRequestTransfer;
    @BindView(R.id.ll_transfer_account)
    LinearLayout llTransferAccount;
    @BindView(R.id.iv_one_arrow)
    ImageView ivOneArrow;
    @BindView(R.id.iv_two_arrow)
    ImageView ivTwoArrow;
    @BindView(R.id.iv_three_arrow)
    ImageView ivThreeArrow;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.iv_transfer_contacts)
    ImageView ivTransferContacts;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_transfer_all_contacts)
    ImageView ivTransferAllContacts;
    @BindView(R.id.iv_transfer_all_contacts_arr)
    ImageView ivTransferAllContactsArr;
    @BindView(R.id.iv_transfer_all_contacts_two)
    ImageView ivTransferAllContactsTwo;
    @BindView(R.id.iv_transfer_all_contacts_two_arr)
    ImageView ivTransferAllContactsTwoArr;
    @BindView(R.id.iv_transfer_all_contacts_three)
    ImageView ivTransferAllContactsThree;
    @BindView(R.id.iv_transfer_all_contacts_three_arr)
    ImageView ivTransferAllContactsThreeArr;
    @BindView(R.id.iv_transfer_request)
    ImageView ivTransferRequest;
    @BindView(R.id.iv_transfer_qr_code)
    ImageView ivTransferQRC;
    @BindView(R.id.iv_transfer_acc)
    ImageView ivTransferAcc;
    @BindView(R.id.iv_four_arrow)
    ImageView ivFourArrow;
    @BindView(R.id.tv_transfer_acc_title)
    TextView tvTransferAccTitle;
    @BindView(R.id.tv_transfer_acc_sub)
    TextView tvTransferAccSubTitle;
    @BindView(R.id.tv_transfer_request)
    TextView tvTransferRequest;
    @BindView(R.id.tv_transfer_qr_code)
    TextView tvTransferQRC;
    @BindView(R.id.transition_bg)
    ConstraintLayout transitionBg;
    @BindView(R.id.tv_transfer_contacts_title)
    TextView tvTransferContactsTitle;
    @BindView(R.id.tv_transfer_all_contacts)
    TextView tvTransferAllContacts;
    @BindView(R.id.tv_transfer_all_contacts_two)
    TextView tvTransferAllContactsTwo;
    @BindView(R.id.tv_transfer_all_contacts_three)
    TextView tvTransferAllContactsThree;
    @BindView(R.id.ll_imr)
    LinearLayout llImr;
    @BindView(R.id.tv_imr)
    TextView tvImr;
    @BindView(R.id.iv_imr)
    ImageView ivImr;
    @BindView(R.id.iv_five_arrow)
    ImageView ivFiveArrow;
    @BindView(R.id.ll_withdraw)
    LinearLayout llWithDraw;
    @BindView(R.id.tv_withdraw)
    TextView tvWithDraw;
    @BindView(R.id.iv_withdraw)
    ImageView ivWithDraw;
    @BindView(R.id.iv_six_arrow)
    ImageView ivSixArrow;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    private BottomDialog bottomDialog;
    private String qrCodeUrl;
    private UserInfoEntity userInfoEntity;
    //1.提现 2.充值
    private int type = 0;


    private TransferContactAdapter transferContactAdapter1, transferContactAdapter2;
    private LinearLayoutManager mLayoutManagerOne, mLayoutManagerTwo;

    //别人给我转账的列表
    public List<KycContactEntity> payeeContactsList = new ArrayList<>();
    //我给别人转账的列表
    public List<KycContactEntity> payerContactsList = new ArrayList<>();

    public List<KycContactEntity> allContactList = new ArrayList<>();

    public static TransferFragment getInstance() {
        TransferFragment transferFragment = new TransferFragment();
        return transferFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_transter;
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(transitionBg, R.attr.splash_bg_color);
        helper.setBackgroundResourceByAttr(llTransferContacts, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llRequestTransfer, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llTransferAccount, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llQrCode, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llImr, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llOneAll, R.attr.setting_wallet_sub_item_bg);
        helper.setBackgroundResourceByAttr(llWithDraw, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llTwoAll, R.attr.setting_wallet_sub_item_bg);
        helper.setBackgroundResourceByAttr(llThreeAll, R.attr.setting_wallet_sub_item_bg);
        helper.setImageBgResourceByAttr(headCircle, R.attr.icon_circle);
        if (LocaleUtils.isRTL(mContext)) {
            helper.setImageBgResourceByAttr(ivQrCode, R.attr.icon_my_code_ar);
        } else {
            helper.setImageBgResourceByAttr(ivQrCode, R.attr.icon_my_code_en);
        }
        helper.setImageBgResourceByAttr(ivImr, R.attr.home_int_remittance);
        helper.setImageBgResourceByAttr(ivTransferContacts, R.attr.transfer_any_contact);

        if (llOne != null) {
            helper.setImageBgResourceByAttr(ivOneArrow, llOne.getVisibility() == View.VISIBLE ? R.attr.icon_down_arrow : R.attr.icon_right_arrow);
        }
        if (llTwo != null) {
            helper.setImageBgResourceByAttr(ivThreeArrow, llTwo.getVisibility() == View.VISIBLE ? R.attr.icon_down_arrow : R.attr.icon_right_arrow);
        }
        if (llThree != null) {
            helper.setImageBgResourceByAttr(ivTwoArrow, llThree.getVisibility() == View.VISIBLE ? R.attr.icon_down_arrow : R.attr.icon_right_arrow);
        }
        helper.setImageBgResourceByAttr(ivTransferAllContacts, R.attr.transfer_all_contacts);
        helper.setImageBgResourceByAttr(ivTransferAllContactsArr, R.attr.icon_right_arrow);
        helper.setImageBgResourceByAttr(ivTransferAllContactsTwo, R.attr.transfer_all_contacts);
        helper.setImageBgResourceByAttr(ivTransferAllContactsTwoArr, R.attr.icon_right_arrow);
        helper.setImageBgResourceByAttr(ivTransferAllContactsThree, R.attr.transfer_all_contacts);
        helper.setImageBgResourceByAttr(ivTransferAllContactsThreeArr, R.attr.icon_right_arrow);
        helper.setImageBgResourceByAttr(ivTransferRequest, R.attr.transfer_request_transfer);
        helper.setImageBgResourceByAttr(ivTransferAcc, R.attr.transfer_add_money_bank_account);
        helper.setImageBgResourceByAttr(ivTransferQRC, R.attr.transfer_via_qr);
        helper.setImageBgResourceByAttr(ivFourArrow, R.attr.icon_right_arrow);
        helper.setImageBgResourceByAttr(ivFiveArrow, R.attr.icon_right_arrow);
        helper.setImageBgResourceByAttr(ivSixArrow, R.attr.icon_right_arrow);
        helper.setImageBgResourceByAttr(ivWithDraw, R.attr.home_withdraw);

        helper.setTextColorByAttr(tvTransferTitle, R.attr.wallet_setting_text_color);
        helper.setTextColorByAttr(tvTransferDesc, R.attr.color_a3a3a3_a3a3a3);
        helper.setTextColorByAttr(tvTransferContactsTitle, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvTransferAllContacts, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvTransferAllContactsTwo, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvTransferAllContactsThree, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvTransferRequest, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tv_two_contacts, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tv_one_contacts, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvTransferAccTitle, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvTransferAccSubTitle, R.attr.transfer_subtext_color);
        helper.setTextColorByAttr(tvTransferQRC, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvImr, R.attr.transfer_item_text_color);
        helper.setTextColorByAttr(tvWithDraw, R.attr.transfer_item_text_color);

        if (transferContactAdapter1 != null) {
            transferContactAdapter1.changeTheme();
        }
        if (transferContactAdapter2 != null) {
            transferContactAdapter2.changeTheme();
        }
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, ivOneArrow, ivThreeArrow, ivTwoArrow, ivTransferAllContactsArr,
                ivTransferAllContactsTwoArr, ivTransferAllContactsThreeArr, ivFourArrow, ivFiveArrow,ivSixArrow, headCircle, ivBack);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        mLayoutManagerOne = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManagerTwo = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rlOneContacts.setLayoutManager(mLayoutManagerOne);
        rlTwoContacts.setLayoutManager(mLayoutManagerTwo);
        transferContactAdapter1 = new TransferContactAdapter(payerContactsList);
        transferContactAdapter2 = new TransferContactAdapter(payeeContactsList);
        transferContactAdapter1.bindToRecyclerView(rlOneContacts);
        transferContactAdapter2.bindToRecyclerView(rlTwoContacts);

        llOne.setAlpha(1f);

        transferContactAdapter1.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_contacts))return;
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                KycContactEntity kycContactEntity = payerContactsList.get(position);
                HashMap<String, Object> map = new HashMap<>(16);
                kycContactEntity.setContactsName(AndroidUtils.getContactName(kycContactEntity.getContactsName()));
                map.put(Constants.CONTACT_ENTITY, kycContactEntity);
                map.put(Constants.sceneType, Constants.TYPE_TRANSFER_WW);
                ActivitySkipUtil.startAnotherActivity(mActivity, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        transferContactAdapter2.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.home_request))return;
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                KycContactEntity kycContactEntity = payeeContactsList.get(position);
                HashMap<String, Object> map = new HashMap<>(16);
                kycContactEntity.setContactsName(AndroidUtils.getContactName(kycContactEntity.getContactsName()));
                map.put(Constants.CONTACT_ENTITY, kycContactEntity);
                map.put(Constants.sceneType, Constants.TYPE_TRANSFER_RT);
                ActivitySkipUtil.startAnotherActivity(mActivity, RequestTransferActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    @Override
    protected void initData() {
        mPresenter.getRecentContact();
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        noticeThemeChange();
    }

    @Override
    protected void restart() {
        mPresenter.getRecentContact();
    }

    private void showQrDialog(Bitmap bitmap) {
        bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_qr_code, null);
        ImageView ivQrCode = contentView.findViewById(R.id.iv_qr_code);
        ivQrCode.setImageBitmap(bitmap);

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
//            layoutParams.height = AndroidUtils.dip2px(mContext, 400);
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    private void startOpenOneAnim(View view) {
        view.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
                objectAnimator.setDuration(300);
                objectAnimator.start();
            }
        }, 200);

        ValueAnimator valueAnimator = createDropAnimator(view, AndroidUtils.dip2px(mContext, 0), AndroidUtils.dip2px(mContext, 146));
        valueAnimator.setDuration(500);
        valueAnimator.start();

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(valueAnimator);
        animatorSet.start();
    }

    private void startHideOneAnim(View view) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
                objectAnimator.setDuration(300);
                objectAnimator.start();
            }
        }, 200);

        ValueAnimator valueAnimator = createDropAnimator(view, AndroidUtils.dip2px(mContext, 146), AndroidUtils.dip2px(mContext, 0));
        valueAnimator.setDuration(500);
        valueAnimator.start();

//        valueAnimator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                view.setVisibility(View.GONE);
//            }
//        });

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(valueAnimator);
        animatorSet.start();

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }
        });
    }

    private ValueAnimator createDropAnimator(final View v, int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                int value = (int) arg0.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                layoutParams.height = value;
                v.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    @OnClick({R.id.ll_one_contracts, R.id.ll_one_all, R.id.ll_one, R.id.ll_two_contacts, R.id.ll_two_all, R.id.ll_two, R.id.ll_three_contacts, R.id.ll_three_all, R.id.ll_three,
            R.id.iv_qr_code, R.id.ll_qr_code, R.id.ll_imr,R.id.ll_withdraw})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.ll_imr:
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_intTransfer))return;
                if (!isKyc(Constants.KYC_TYPE_IMR)) {
                    return;
                }
                if (userInfoEntity != null) {
                    boolean isIMRSetup = true;
//                    boolean isIMRSetup = "Y".equals(userInfoEntity.imrSetUpFlag);
//                    if (isIMRSetup) {
                        ActivitySkipUtil.startAnotherActivity(mContext, ImrMainActivity.class);
//                    } else {
//                        if (getFragmentManager() != null) {
//                            ImrSetupNoticeDialog dialog = new ImrSetupNoticeDialog();
//                            dialog.show(getFragmentManager(), "ImrNotice");
//                        }
//                    }
                }
                break;
            case R.id.ll_one_contracts:

                if (llOne.getVisibility() == View.VISIBLE) {
                    startHideOneAnim(llOne);
//                    llOne.setVisibility(View.GONE);
                    ivOneArrow.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    if (llTwo.getVisibility() == View.VISIBLE) {
                        startHideOneAnim(llTwo);
                        ivTwoArrow.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                    }
                    startOpenOneAnim(llOne);
//                    llOne.setVisibility(View.VISIBLE);
                    llThree.setVisibility(View.GONE);
                    ivOneArrow.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                }
                break;
            case R.id.ll_one_all:
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_contacts))return;
                if (isKyc(Constants.KYC_TYPE_TRANSFER)) {
                    HashMap<String, Object> map = new HashMap<>(16);
                    map.put(Constants.sceneType, Constants.TYPE_TRANSFER_WW);
                    ActivitySkipUtil.startAnotherActivity(mActivity, TransferContactActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.ll_one:
                break;
            case R.id.ll_two_contacts:
                if (llTwo.getVisibility() == View.VISIBLE) {
                    startHideOneAnim(llTwo);
                    ivTwoArrow.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    if (llOne.getVisibility() == View.VISIBLE) {
                        startHideOneAnim(llOne);
                        ivOneArrow.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                    }
                    startOpenOneAnim(llTwo);
                    llThree.setVisibility(View.GONE);
                    ivTwoArrow.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                }
                break;
            case R.id.ll_two_all:
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_contacts))return;
                if (isKyc(Constants.KYC_TYPE_TRANSFER)) {
                    HashMap<String, Object> map = new HashMap<>(16);
                    map.put(Constants.sceneType, Constants.TYPE_TRANSFER_RT);
                    ActivitySkipUtil.startAnotherActivity(mActivity, TransferContactActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.ll_two:
                break;
            case R.id.ll_three_contacts:
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_localTransfer))return;
                if (isKyc(Constants.KYC_TYPE_TRANSFER)) {
                    type = 1;
                    mPresenter.getBeneficiaryList();
                }
                break;
            case R.id.ll_three_all:
                break;
            case R.id.ll_three:
                break;
            case R.id.iv_qr_code:
//                boolean setFirstQrCode = SpUtils.getInstance().getFirstQrCode();
//                if (setFirstQrCode) {
//                    hashMap.put("firstOpenQrCode", setFirstQrCode);
//                    ActivitySkipUtil.startAnotherActivity(mActivity, TransferPwdActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                } else {
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_myCode))return;
                qrCodeUrl = SpUtils.getInstance().getQrCode();
                if (TextUtils.isEmpty(qrCodeUrl) || qrCodeUrl.startsWith("swiftpass://")) {
                    mPresenter.getQrCode(UserInfoManager.getInstance().getCurrencyCode());
                } else {
                    hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                    ActivitySkipUtil.startAnotherActivity(mActivity,MyQrCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                    Bitmap bitmap = QRCodeUtils.createQRCode(qrCodeUrl, AndroidUtils.dip2px(mContext, 220));
//                    showQrDialog(bitmap);
                }
//                }
                break;
            case R.id.ll_qr_code:
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_scan))return;
                if (isKyc(Constants.KYC_TYPE_NULL)) {
                    hashMap.put(Constants.isFrom,"send");
                    ActivitySkipUtil.startAnotherActivity(mActivity, ScanQrActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.ll_withdraw:
                if(AndroidUtils.isCloseUse(mActivity,Constants.send_withdraw))return;
                if (userInfoEntity.isKycChecking()) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                } else if (userInfoEntity.isKycLow()) {
                    ReferralCodeDialogUtils.showReferralCodeDialog(this, getContext());
                }else if(userInfoEntity.isUserIdCardExpried()){
                    showExpireIdDialog();
                } else{
                    type = 2;
                    mPresenter.getBeneficiaryList();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void getRecentContactFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getRecentContactSuccess(TransferContactListEntity transferContactListEntity) {
        if (transferContactListEntity != null) {
            payeeContactsList = transferContactListEntity.payeeContactsList;
            payerContactsList = transferContactListEntity.payerContactsList;
            if (payerContactsList.size() > 0) {
                rlOneContacts.setVisibility(View.VISIBLE);
                tv_one_contacts.setVisibility(View.GONE);
                transferContactAdapter1.setDataList(payerContactsList);
            } else {
                rlOneContacts.setVisibility(View.GONE);
                tv_one_contacts.setVisibility(View.VISIBLE);
            }
            if (payeeContactsList.size() > 0) {
                rlTwoContacts.setVisibility(View.VISIBLE);
                tv_two_contacts.setVisibility(View.GONE);
                transferContactAdapter2.setDataList(payeeContactsList);
            } else {
                rlTwoContacts.setVisibility(View.GONE);
                tv_two_contacts.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void getQrCodeSuccess(QrCodeEntity qrCodeEntity) {
        if (qrCodeEntity != null) {
            qrCodeUrl = qrCodeEntity.qRCodeInfo;
            SpUtils.getInstance().setQrCode(qrCodeUrl);

            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
            ActivitySkipUtil.startAnotherActivity(mActivity,MyQrCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

//            Bitmap bitmap = QRCodeUtils.createQRCode(qrCodeUrl, AndroidUtils.dip2px(mContext, 220));
//            showQrDialog(bitmap);
        }
    }

    @Override
    public void getQrCodeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getBeneficiaryListFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getBeneficiaryListSuccess(BeneficiaryListEntity beneficiaryListEntity) {
        if (beneficiaryListEntity != null) {
            List<BeneficiaryEntity> beneficiaryList = beneficiaryListEntity.beneficiaryInfoList;
            int size = beneficiaryList.size();
            switch (type){
                case 1:
                    if (size > 0) {
                        ActivitySkipUtil.startAnotherActivity(mActivity, BeneficiaryListActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        ActivitySkipUtil.startAnotherActivity(mActivity, AddBeneficiaryActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    type = 0;
                    break;
                case 2:
                    if (size > 0) {
                        ActivitySkipUtil.startAnotherActivity(mActivity, WithDrawActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        HashMap<String,Object> mHashMap = new HashMap<>();
                        mHashMap.put(Constants.isFromHome,true);
                        ActivitySkipUtil.startAnotherActivity(mActivity, AddBeneficiaryActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    type = 0;
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public TransferContract.RecentContactPresenter getPresenter() {
        return new RecentContactPresenter();
    }
}
