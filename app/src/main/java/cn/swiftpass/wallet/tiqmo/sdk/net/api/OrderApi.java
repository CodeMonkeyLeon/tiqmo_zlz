//package cn.swiftpass.wallet.tiqmo.sdk.net.api;
//
//import androidx.annotation.StringDef;
//
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//
//import cn.swiftpass.wallet.sdk.entity.GetOrderDetailEntity;
//import cn.swiftpass.wallet.sdk.entity.JsonResponse;
//import cn.swiftpass.wallet.sdk.entity.OrderListEntity;
//import cn.swiftpass.wallet.sdk.entity.OrderResultEntity;
//import cn.swiftpass.wallet.sdk.net.RequestCall;
//import cn.swiftpass.wallet.sdk.net.annotation.Headers;
//import cn.swiftpass.wallet.sdk.net.annotation.Param;
//
///**
// * Created by 叶智星 on 2018年09月18日.
// * 每一个不曾起舞的日子，都是对生命的辜负。
// */
//public interface OrderApi {
//
//    //up-往前，down-往后
//    String SEARCH_TYPE_UP = "up";
//    String SEARCH_TYPE_DOWN = "down";
//
//
//    //up-往前，down-往后
//    String QUERY_TYPE_ALL = "0";
//    String QUERY_TYPE_MONTH = "1";
//    String QUERY_TYPE_INTERVAL = "2";
//
//    @Retention(RetentionPolicy.SOURCE)
//    @StringDef({SEARCH_TYPE_UP, SEARCH_TYPE_DOWN})
//    @interface SearchType {
//    }
//
//    @Retention(RetentionPolicy.SOURCE)
//    @StringDef({QUERY_TYPE_ALL, QUERY_TYPE_MONTH, QUERY_TYPE_INTERVAL})
//    @interface QueryType {
//    }
//
//
//    @Headers({"Service-Id:1052"})
//    RequestCall<JsonResponse<OrderListEntity>> getOrderList(@Param("startTime") String startTime,
//                                                            @Param("endTime") String endTime,
//                                                            @Param("count") String count,
//                                                            @Param("dealType") String dealType,
//                                                            @Param("queryType") @QueryType String queryType,
//                                                            @Param("orderNo") String orderNo,
//                                                            @Param("type") @SearchType String type);
//
//    @Headers({"Service-Id:1053"})
//    RequestCall<JsonResponse<OrderListEntity>> getRelateOrder(@Param("orderNo") String orderNo);
//
//
//    @Headers({"Service-Id:1039"})
//    RequestCall<JsonResponse<OrderResultEntity>> getOrderStatus(@Param("orderNo") String orderNo,
//                                                                @Param("codeFlowNo") String codeFlowNo);
//
//
//    @Headers({"Service-Id:1054"})
//    RequestCall<JsonResponse<GetOrderDetailEntity>> getOrderDetail(@Param("orderNo") String orderNo);
//
//
//}
