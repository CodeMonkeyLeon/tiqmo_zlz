package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SpendingCategoryDetailEntity extends BaseEntity {
    //场景类型
    public int sceneType;
    public String orderDesc;
    public String logo;
    //交易金额
    public String orderAmount;
    //交易币种
    public String currencyType;
    //交易时间
    public String createTime;
    public String sex;
    //子场景类型
    public int subSceneType;
}
