package cn.swiftpass.wallet.tiqmo.module.guide

interface OnNextClickListener {
    fun onNextClick(index: Int)
}