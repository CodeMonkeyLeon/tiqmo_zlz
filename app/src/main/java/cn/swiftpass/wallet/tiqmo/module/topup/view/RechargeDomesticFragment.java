package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.topup.adapter.RechargeDomeOrderAdapter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeDomeOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeContract;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeOrderListPresenter;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class RechargeDomesticFragment extends BaseFragment<RechargeContract.RechargeOrderListPresenter> implements RechargeContract.RechargeOrderListView {
    @BindView(R.id.tv_recharge_title)
    TextView tvRechargeTitle;
    @BindView(R.id.ll_top)
    LinearLayout llTop;
    @BindView(R.id.ry_recharge_operators)
    RecyclerView ryRechargeOperators;
    @BindView(R.id.sw_recharge_operators)
    SwipeRefreshLayout swRechargeOperators;
    @BindView(R.id.view_bottom)
    View viewBottom;

    private List<String> countryCodeList = new ArrayList<>();

    private List<RechargeDomeOrderEntity> domesticList = new ArrayList<>();

    private RechargeOrderListEntity rechargeOrderListEntity;

    private StatusView historyStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private RechargeDomeOrderEntity rechargeDomeOrderEntity;
    private RechargeDomeOrderAdapter rechargeDomeOrderAdapter;
    private boolean hideTop;

    public static RechargeDomesticFragment getInstance(RechargeOrderListEntity rechargeOrderListEntity,boolean hideTop) {
        RechargeDomesticFragment rechargeDomesticFragment = new RechargeDomesticFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.rechargeOrderListEntity, rechargeOrderListEntity);
        bundle.putBoolean("hideTop",hideTop);
        rechargeDomesticFragment.setArguments(bundle);
        return rechargeDomesticFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_recharge_domestic;
    }

    @Override
    protected void initView(View parentView) {
        swRechargeOperators.setColorSchemeResources(R.color.color_B00931);

        swRechargeOperators.setVisibility(View.VISIBLE);
        initRecyclerView();
    }

    private void initRecyclerView() {
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_31));
        historyStatusView = new StatusView.Builder(mContext, ryRechargeOperators).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();

        swRechargeOperators.setEnabled(false);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryRechargeOperators.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryRechargeOperators.setLayoutManager(manager);
        rechargeDomeOrderAdapter = new RechargeDomeOrderAdapter(domesticList);
        rechargeDomeOrderAdapter.bindToRecyclerView(ryRechargeOperators);

        if (getArguments() != null) {
            rechargeOrderListEntity = (RechargeOrderListEntity) getArguments().getSerializable(Constants.rechargeOrderListEntity);
            hideTop = getArguments().getBoolean("hideTop");
            if(hideTop){
                llTop.setVisibility(View.GONE);
            }else{
                llTop.setVisibility(View.VISIBLE);
            }
            setOrderList(rechargeOrderListEntity);
        }

        rechargeDomeOrderAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                RechargeDomeOrderEntity orderEntity = domesticList.get(position);
                if (orderEntity != null) {
                    rechargeDomeOrderEntity = orderEntity;
                    ActivitySkipUtil.startAnotherActivity(mActivity, RechargeListActivity.class, "TopUpInfoList", rechargeDomeOrderEntity);
                }
            }
        });
    }

    private void setOrderList(RechargeOrderListEntity rechargeOrderListEntity) {
        domesticList.clear();
        if (rechargeOrderListEntity != null) {
            List<RechargeDomeOrderEntity> list = rechargeOrderListEntity.domesticList;
            int size = list.size();
            if (size == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                domesticList.addAll(list);
                rechargeDomeOrderAdapter.setDataList(domesticList);
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }

    @Override
    public void getRechargeOrderListSuccess(RechargeOrderListEntity RechargeOrderListEntity) {

    }

    @Override
    public void deleteRechargeOrderSuccess(Void result) {

    }

    @Override
    public void getRechargeOrderDetailSuccess(RechargeOrderDetailEntity rechargeOrderDetailEntity) {
        
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {

    }

    @Override
    public void getRechargeOrderListFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getRechargeProductListSuccess(RechargeProductListEntity rechargeProductListEntity) {

    }

    @Override
    public RechargeContract.RechargeOrderListPresenter getPresenter() {
        return new RechargeOrderListPresenter();
    }
}
