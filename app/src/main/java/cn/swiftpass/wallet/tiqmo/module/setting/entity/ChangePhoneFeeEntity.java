package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChangePhoneFeeEntity extends BaseEntity {
    //是否需要收费 Y/N Y需要
    public String chargeFee;
    //提示的词条
    public String comment;
    //需要支付的金额
    public String totalAmount;

    public String transFees;
    public String discountTransFees;
    public String vat;
    public String discountVat;

    public String orderNo;

    public boolean needChargeFee(){
        return "Y".equals(chargeFee);
    }
}
