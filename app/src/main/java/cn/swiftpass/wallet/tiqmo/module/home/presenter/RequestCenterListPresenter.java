package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.contract.RequestCenterContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RequestCenterListPresenter implements RequestCenterContract.RequestCenterListPresenter {

    RequestCenterContract.RequestCenterListView requestCenterListView;

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(requestCenterListView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        requestCenterListView.transferSurePaySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        requestCenterListView.transferSurePayFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void getMsgList(int pageNum, int pageSize) {
        AppClient.getInstance().getMsgList(pageNum, pageSize, new LifecycleMVPResultCallback<MsgListEntity>(requestCenterListView, false) {
            @Override
            protected void onSuccess(MsgListEntity result) {
                requestCenterListView.getMsgListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                requestCenterListView.getMsgListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void removeRequest(String infoId) {
        AppClient.getInstance().removeRequest(infoId, new LifecycleMVPResultCallback<Void>(requestCenterListView, true) {
            @Override
            protected void onSuccess(Void result) {
                requestCenterListView.removeRequestSuccess();
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                requestCenterListView.removeRequestFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(requestCenterListView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (requestCenterListView == null) {
                    return;
                }
                requestCenterListView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (requestCenterListView == null) {
                    return;
                }
                requestCenterListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void updateClickTimeService(String messageId) {
        AppClient.getInstance().updateClickTimeService(messageId, new LifecycleMVPResultCallback<Void>(requestCenterListView, false) {
            @Override
            protected void onSuccess(Void result) {

            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {

            }
        });
    }

    @Override
    public void attachView(RequestCenterContract.RequestCenterListView requestCenterListView) {
        this.requestCenterListView = requestCenterListView;
    }

    @Override
    public void detachView() {
        this.requestCenterListView = null;
    }

    @Override
    public void getRequestCenterList() {
        AppClient.getInstance().getRequestCenterList(new LifecycleMVPResultCallback<RequestCenterListEntity>(requestCenterListView, false) {
            @Override
            protected void onSuccess(RequestCenterListEntity result) {
                if (requestCenterListView != null) {
                    requestCenterListView.getRequestCenterListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                requestCenterListView.getRequestCenterListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void sendNotify(List<NotifyEntity> receivers) {
        AppClient.getInstance().sendNotify(receivers, new LifecycleMVPResultCallback<PayBillOrderInfoEntity>(requestCenterListView, true) {
            @Override
            protected void onSuccess(PayBillOrderInfoEntity result) {
                requestCenterListView.sendNotifySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                requestCenterListView.sendNotifyFail(errorCode, errorMsg);
            }
        });
    }
}
