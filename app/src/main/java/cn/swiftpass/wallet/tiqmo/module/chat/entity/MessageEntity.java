package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MessageEntity extends BaseEntity {
    private String id;
    private MessageType messageType;
    private String body;
    private String time;
    private String status;
    private int indexPosition;
    private String pdfName;
    private String pdfSize;
    private String strUrl;
    private boolean isLoading;
    private boolean isError;
    // for secure chat only
    private String name;
    private String content;
    private long timestamp;
    private String type;
    private UserChat user;
    private CustomGlideUrl glideUrl;
    private String sessionType;
    private String action;
    private MessageEntity quickRepliedMessage;
    private DocumentType document;
    private String downloadLink;
    private String fileName;
    private String linkCode;
    private String fileType;
    private HashMap<String, String> metaInfo = new HashMap<>();
    private boolean isGroupChat;
    private ChatTransferEntity chatTransferEntity;

    public ChatTransferEntity getChatTransferEntity() {
        return this.chatTransferEntity;
    }

    public void setChatTransferEntity(final ChatTransferEntity chatTransferEntity) {
        this.chatTransferEntity = chatTransferEntity;
    }

    public boolean shouldHighlightView() {
        return shouldHighlightView;
    }

    public void setShouldHighlightView(boolean shouldHighlightView) {
        this.shouldHighlightView = shouldHighlightView;
    }

    private boolean shouldHighlightView;

    public String getReplyName(){
        if(metaInfo == null) return "";
        return metaInfo.get("replyName");
    }
    public String getReplyMessage(){
        if(metaInfo == null) return "";
        return metaInfo.get("replyMessage");
    }

    public String getReplyMessageId(){
        if(metaInfo == null) return "";
        return metaInfo.get("replyMessageId");
    }

    public String getReplyImageUrl(){
        if(metaInfo == null) return "";
        return metaInfo.get("replyImageUrl");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public enum MessageType {
        LeftSimpleMessage,
        RightSimpleMessage,
        LeftSingleImage,
        RightSingleImage,
        DateFloater,
        RightFileImage,
        LeftFileImage,
        RightTransferMessage,
        LeftTransferMessage,
    }

    public MessageEntity() {
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public void setIndexPosition(int indexPosition) {
        this.indexPosition = indexPosition;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

    public String getPdfSize() {
        return pdfSize;
    }

    public void setPdfSize(String pdfSize) {
        this.pdfSize = pdfSize;
    }

    public String getStrUrl() {
        return strUrl;
    }

    public void setStrUrl(String strUrl) {
        this.strUrl = strUrl;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserChat getUser() {
        return user;
    }

    public void setUser(UserChat user) {
        this.user = user;
    }

    public GlideUrl getGlideUrl() {
        if(downloadLink != null && !downloadLink.isEmpty()){
            return new GlideUrl(downloadLink);
        }
        return null;
    }

    public void setGlideUrl(CustomGlideUrl glideUrl) {
        this.glideUrl = glideUrl;
    }

    public String getSessionType() {
        return sessionType == null ? "" : sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public MessageEntity getQuickRepliedMessage() {
        return quickRepliedMessage;
    }

    public void setQuickRepliedMessage(MessageEntity quickRepliedMessage) {
        this.quickRepliedMessage = quickRepliedMessage;
    }

    public DocumentType getDocument() {
        return document;
    }

    public void setDocument(DocumentType document) {
        this.document = document;
    }

    public static class DocumentType implements Serializable {

        public DocumentType(String type) {
            this.type = type;
        }

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public CharSequence getSimpleMessageToDisplay(String searchTerm) {
        CharSequence messageToDisplay = getSimpleMessage();

        if (!searchTerm.isEmpty()) {
            int startPos = getSearchTermStartIndex(searchTerm);
            int endPos = startPos + searchTerm.length();

            if (startPos != -1) {
                Spannable spannable = new SpannableString(messageToDisplay.toString());
                spannable.setSpan(new BackgroundColorSpan(0xFFFFFF00), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                messageToDisplay = spannable;
            }
        }
        return messageToDisplay;
    }

    public int getSearchTermStartIndex(String searchTerm) {
        return getSimpleMessage().toString().toLowerCase(Locale.US).indexOf(searchTerm.toLowerCase(Locale.US));
    }

    public CharSequence getSimpleMessage() {
        String simpleMessage = getContent();
        return simpleMessage == null ? "" : simpleMessage;
    }

    public void setMetaInfo(HashMap<String, String> metaInfo) {
        this.metaInfo = metaInfo;
    }

    public boolean isGroupChat() {
        return isGroupChat;
    }

    public void setGroupChat(boolean groupChat) {
        isGroupChat = groupChat;
    }

    @NonNull
    public HashMap<String, String> getMetaInfo(){
        return metaInfo == null? new HashMap<>(): metaInfo;
    }

    public String getFileName() {
        return fileName==null?"":fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public static class CustomGlideUrl implements Serializable {
        private String token;
        private String url;

        public CustomGlideUrl(String token, String url) {
            this.token = token;
            this.url = url;
        }

        public GlideUrl getGlideUrl() {
            return new GlideUrl(url, new LazyHeaders.Builder().addHeader("token", token).build());
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "MessageEntity{" +
                "id=" + id +
                ", messageType=" + messageType +
                ", body='" + body + '\'' +
                ", time='" + time + '\'' +
                ", status='" + status + '\'' +
                ", indexPosition=" + indexPosition +
                ", pdfName='" + pdfName + '\'' +
                ", pdfSize='" + pdfSize + '\'' +
                ", strUrl='" + strUrl + '\'' +
                ", isLoading=" + isLoading +
                ", isError=" + isError +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", type='" + type + '\'' +
                ", user=" + user +
                ", downloadLink=" + downloadLink +
                ", glideUrl=" + glideUrl +
                ", sessionType='" + sessionType + '\'' +
                ", action='" + action + '\'' +
                ", quickRepliedMessage=" + quickRepliedMessage +
                '}';
    }

}
