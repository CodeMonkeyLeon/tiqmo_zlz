package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class SetPwdContract {

    public interface View extends BaseView<SetPwdContract.Presenter> {
        void resetPwdSuccess(UserInfoEntity response);
        void preCheckPdSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity);

        void resetPwdFail(String errorCode, String errorMsg);
        void preCheckPdFail(String errorCode, String errorMsg);
    }

    /**
     * pdType	M	string (4)	密码类型：L：登录密码，T：交易密码
     * password	M	String(32)	新密码
     * OldPassword	M	String(32)	旧密码
     * operatorType	M	String(4)	密码类型：S：设置密码，U：更新密码
     * sceneType 场景类型：FP：忘记支付密码场景
     */
    public interface Presenter extends BasePresenter<SetPwdContract.View> {
        void resetPwd(String pdType, String password, String oldPassword, String operatorType, String sceneType, String referralCode);

        void preCheckPd(String passwordType,
                        String operatorType,
                        String oldPassword);
    }
}
