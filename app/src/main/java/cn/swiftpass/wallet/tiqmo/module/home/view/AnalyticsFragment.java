package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.imr.UnScrollViewPager;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMainPagerAdapter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class AnalyticsFragment extends BaseFragment {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.indicator_analytics_main)
    MagicIndicator indicatorAnalyticsMain;
    @BindView(R.id.vp_analytics_main)
    UnScrollViewPager vpAnalyticsMain;
    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;

    private CommonNavigator commonNavigator;

    private SpendingBudgetFragment spendingBudgetFragment;
    private SpendingsFragment spendingsFragment;

    private int currentPosition;

    private List<String> title;

    private boolean isActivity;

    public static AnalyticsFragment getInstance(int position,boolean isActivity) {
        AnalyticsFragment analyticsFragment = new AnalyticsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putBoolean("isActivity", isActivity);
        analyticsFragment.setArguments(bundle);
        return analyticsFragment;
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundResourceByAttr(bgMain, R.attr.bg_040f33_white);
        if(indicatorAnalyticsMain != null) {
            indicatorAnalyticsMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
            if (commonNavigator != null) {
                commonNavigator.setAdapter(new CommonNavigatorAdapter() {
                    @Override
                    public int getCount() {
                        return title == null ? 0 : title.size();
                    }

                    @Override
                    public IPagerTitleView getTitleView(Context context, final int index) {
                        ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                        clipPagerTitleView.setText(title != null ? title.get(index) : "");
                        clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                        clipPagerTitleView.setTextColor(mContext.getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                        clipPagerTitleView.setClipColor(Color.WHITE);
                        clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                        clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));

                        clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (ButtonUtils.isFastDoubleClick()) {
                                    return;
                                }
                                vpAnalyticsMain.setCurrentItem(index);
                            }
                        });
                        return clipPagerTitleView;
                    }

                    @Override
                    public IPagerIndicator getIndicator(Context context) {
                        LinePagerIndicator indicator = new LinePagerIndicator(context);
                        indicator.setLineHeight(UIUtil.dip2px(context, 44));
                        indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                        indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                        return indicator;
                    }

                    @Override
                    public float getTitleWeight(Context context, int index) {
                        return 1.0f;
                    }
                });
                indicatorAnalyticsMain.setNavigator(commonNavigator);
            }

            if (spendingsFragment != null) {
                spendingsFragment.noticeThemeChange();
            }

            if (spendingBudgetFragment != null) {
                spendingBudgetFragment.noticeThemeChange();
            }
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_analytics;
    }

    private void initLayout(){
        try {
            LocaleUtils.viewRotationY(mContext, ivBack);
            tvTitle.setText(getString(R.string.analytics_0));
            if (getArguments() != null) {
                currentPosition = getArguments().getInt("position");
                isActivity = getArguments().getBoolean("isActivity");
                if(isActivity){
                    ivBack.setVisibility(View.VISIBLE);
                }else{
                    ivBack.setVisibility(View.GONE);
                }
            }
            title = new ArrayList<>(Arrays.asList(this.getString(R.string.analytics_1),
                    this.getString(R.string.analytics_2)));
            List<Fragment> fragments = new ArrayList<>();
            spendingsFragment = SpendingsFragment.getInstance();
            spendingsFragment.setSetUpListener(new SpendingsFragment.SetUpListener() {
                @Override
                public void changePage() {
                    if (vpAnalyticsMain != null) {
                        vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 0 : 1);
                    }
                }
            });
            spendingBudgetFragment = SpendingBudgetFragment.getInstance();
            fragments.add(spendingsFragment);
            fragments.add(spendingBudgetFragment);
            if (LocaleUtils.isRTL(mContext)) {
                Collections.reverse(title);
                Collections.reverse(fragments);
            }
            ImrMainPagerAdapter adapter = new ImrMainPagerAdapter(getChildFragmentManager(), fragments);
            vpAnalyticsMain.setScrollEnable(false);// 禁止滑动
            vpAnalyticsMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            vpAnalyticsMain.setAdapter(adapter);
            if (LocaleUtils.isRTL(mContext)) {
                vpAnalyticsMain.setCurrentItem(title.size() - 1);
            }
            commonNavigator = new CommonNavigator(mContext);
            commonNavigator.setAdjustMode(true);
            indicatorAnalyticsMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
            commonNavigator.setAdapter(new CommonNavigatorAdapter() {
                @Override
                public int getCount() {
                    return title == null ? 0 : title.size();
                }

                @Override
                public IPagerTitleView getTitleView(Context context, final int index) {
                    ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                    clipPagerTitleView.setText(title.get(index));
                    clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                    clipPagerTitleView.setTextColor(mContext.getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                    clipPagerTitleView.setClipColor(Color.WHITE);
                    clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                    clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));

                    clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ButtonUtils.isFastDoubleClick()) {
                                return;
                            }
                            vpAnalyticsMain.setCurrentItem(index);
                        }
                    });
                    return clipPagerTitleView;
                }

                @Override
                public IPagerIndicator getIndicator(Context context) {
                    LinePagerIndicator indicator = new LinePagerIndicator(context);
                    indicator.setLineHeight(UIUtil.dip2px(context, 44));
                    indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                    indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                    return indicator;
                }

                @Override
                public float getTitleWeight(Context context, int index) {
                    return 1.0f;
                }
            });
            indicatorAnalyticsMain.setNavigator(commonNavigator);
            commonNavigator.onPageSelected(LocaleUtils.isRTL(mContext) ? title.size() - 1 : 0);
            ViewPagerHelper.bind(indicatorAnalyticsMain, vpAnalyticsMain);

            if (currentPosition == 0) {
                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
            } else {
                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 0 : 1);
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, ivHeadCircle);
        initLayout();
    }

    @Override
    protected void initData() {
        try {
//            if (UserInfoManager.getInstance().isSetBudget()) {
//                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
//            } else {
            if(!isActivity) {
                    vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 0 : 1);
                }
//            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void restart() {
//        try {
//            if (UserInfoManager.getInstance().isSetBudget()) {
//                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
//            } else {
//                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 0 : 1);
//            }
//        }catch (Throwable e){
//            LogUtils.d("errorMsg", "---"+e+"---");
//        }
    }

    @OnClick(R.id.iv_back)
    public void onClick() {
        finish();
    }
}
