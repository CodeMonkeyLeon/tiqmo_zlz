package cn.swiftpass.wallet.tiqmo.module.home.entity;

import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class LinePointEntity extends BaseEntity {
    //1被选中 0未被选中
    public int isSelect = 0;
    private Region region;
    public String money;

    public LinePointEntity() {
    }

    public LinePointEntity(final String money) {
        this.money = money;
    }

    public void setRegion(Point point,int screenWidth) {
        Region re = new Region();
        Rect rect = new Rect(point.x-15*screenWidth/720,point.y-15*screenWidth/720,point.x+15*screenWidth/720,point.y+15*screenWidth/720);
        re.set(rect);
        this.region = re;
    }
    public boolean isInRegion(float x, float y) {
        return region != null && region.contains((int)x, (int)y);
    }
}
