package cn.swiftpass.wallet.tiqmo.module.addmoney.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class IbanBaseActivityEntity extends BaseEntity {
    /**
     * {\"partnerNo\":\"96601\",\"activityEndTime\":\"28/03/2023\",
     * \"ibanMarketActivityInfo2App\":{\"discountModel\":2,\"currencyCode\":\"SAR\",\"discountValue\":9000},\"activityNo\":\"1100062302230000053439\"}}"
     */
    //活动编号
    public String activityNo;
    //活动结束时间
    public String activityEndTime;

    //IBAN活动信息
    public IbanActivityEntity ibanMarketActivityInfo2App;
}
