package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SplitDetailAdapter extends BaseRecyclerAdapter<SplitPeopleEntity> {

    public SplitDetailAdapter(@Nullable List<SplitPeopleEntity> data) {
        super(R.layout.item_split_detail, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SplitPeopleEntity splitPeopleEntity, int position) {
        String transferStatus = splitPeopleEntity.transferStatus;
        String receiverAmount = splitPeopleEntity.receiverAmount;
        String receiverName = splitPeopleEntity.receiverName;
        String receiverPhone = splitPeopleEntity.receiverPhone;
        String receiverIcon = splitPeopleEntity.receiverIcon;
        String transferOrderNo = splitPeopleEntity.transferOrderNo;
        String orderType = splitPeopleEntity.orderType;
        String sex = splitPeopleEntity.sex;

        TextView tvUserName = baseViewHolder.getView(R.id.tv_user_name);
        TextView tvMoney = baseViewHolder.getView(R.id.tv_money);
        ImageView ivCheck = baseViewHolder.getView(R.id.iv_checked);
        if ("S".equals(transferStatus)) {
            ivCheck.setImageResource(R.drawable.d_check);
        } else if ("C".equals(transferStatus)) {
            ivCheck.setImageResource(R.drawable.under_processing);
        } else if ("V".equals(transferStatus)) {
            ivCheck.setImageResource(R.drawable.d_failed);
        }

        if (receiverAmount.length() > 8) {
            tvMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferMoney(receiverAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(12)
                    .build());
        } else {
            tvMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferMoney(receiverAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .build());
        }

        if (!TextUtils.isEmpty(receiverName)) {
            if (receiverName.contains(SpUtils.getInstance().getCallingCode())) {
                tvUserName.setTextDirection(View.TEXT_DIRECTION_LTR);
            } else {
                tvUserName.setTextDirection(View.TEXT_DIRECTION_LOCALE);
            }
            baseViewHolder.setGone(R.id.tv_user_name, false);
            baseViewHolder.setText(R.id.tv_user_name, receiverName);
        } else {
            baseViewHolder.setGone(R.id.tv_user_name, true);
        }
        baseViewHolder.setText(R.id.tv_phone, AndroidUtils.getContactPhone(receiverPhone));
        ImageView ivAvatar = baseViewHolder.getView(R.id.iv_avatar);
        if (!TextUtils.isEmpty(receiverIcon)) {
            Glide.with(mContext).clear(ivAvatar);
            Glide.with(mContext)
                    .asBitmap()
                    .load(receiverIcon)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .dontAnimate()
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, sex))
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(new BitmapImageViewTarget(ivAvatar) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                            ivAvatar.setImageBitmap(bitmap);
                        }
                    });
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, sex)).into(ivAvatar);
        }
    }
}
