package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.ChooseCardTypeContract
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity
import cn.swiftpass.wallet.tiqmo.sdk.AppClient
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback

class ChooseCardTypePresenter : ChooseCardTypeContract.Presenter {

    private var mView: ChooseCardTypeContract.View? = null


    override fun getKsaCardSummary(
        cardType: String?,
        cardProductType: String?
    ) {
        mView?.run {
            AppClient.getInstance().cardManager.getKsaCardSummary(cardType,
                cardProductType,
                object : LifecycleMVPResultCallback<KsaCardSummaryEntity>(mView, true) {
                    override fun onSuccess(result: KsaCardSummaryEntity?) {
                        getKsaCardSummarySuccess(result)
                    }

                    override fun onFail(errorCode: String?, errorMsg: String?) {
                        getKsaCardSummaryFailed(errorCode, errorMsg)
                    }
                })
        }
    }

    override fun attachView(v: ChooseCardTypeContract.View?) {
        v?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }


}