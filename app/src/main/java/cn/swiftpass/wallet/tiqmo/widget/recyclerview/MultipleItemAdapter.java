package cn.swiftpass.wallet.tiqmo.widget.recyclerview;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * 多itemType通用adapter
 * @param <T>
 * @author shican
 */
public abstract class MultipleItemAdapter<T> extends BaseRecyclerAdapter<T>{

    /**不同item数据绑定类*/
    private SparseArray<BaseItemDataBinder> mItemDataBinders;
    private static final int DEFAULT_VIEW_TYPE = -0xff;

    public MultipleItemAdapter(List<T> mDataList) {
        super(mDataList);
        mItemDataBinders = new SparseArray<>();
        addItemDataBinder();
    }

    public void addDataBinder(BaseItemDataBinder itemDataBinder){
        int viewType = itemDataBinder.getViewType();
        if (mItemDataBinders.get(viewType) == null){
            mItemDataBinders.put(viewType,itemDataBinder);
        }
    }

    /**重写父类getItemViewType方法*/
    @Override
    protected int getDefItemViewType(int position) {
        T item = getDataList().get(position);
        return item != null ? getViewType(item) : DEFAULT_VIEW_TYPE;
    }

    /**根据实体对象判断viewType*/
    protected abstract int getViewType(T t);

    /**添加不同的item数据绑定器  由子类实现 调用addDataBinder方法*/
    public abstract void addItemDataBinder();

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, T item, int position) {
        int itemViewType = baseViewHolder.getItemViewType();
        baseViewHolder.setAdapter(this);
        BaseItemDataBinder itemDataBinder = mItemDataBinders.get(itemViewType);
        itemDataBinder.bindData(baseViewHolder, item, position);
    }

    @Override
    protected void bindViewClickListener(BaseViewHolder baseViewHolder) {
        if (baseViewHolder == null) {
            return;
        }
        bindClick(baseViewHolder);
        super.bindViewClickListener(baseViewHolder);
    }

    /**item点击事件   可在子类binder中重写onItemClick方法*/
    private void bindClick(final BaseViewHolder baseViewHolder) {
        OnItemClickListener onItemClickListener = getOnItemClickListener();
        OnItemLongClickListener onItemLongClickListener = getmOnItemLongClickListener();

        if(onItemClickListener != null){
            return;
        }
        baseViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int position = baseViewHolder.getAdapterPosition();
                if (position == RecyclerView.NO_POSITION) {
                    return false;
                }
                position -= getHeaderLayoutCount();
                onItemLongClickListener.onItemLongClick(view,position);
                return false;
            }
        });
        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = baseViewHolder.getAdapterPosition();
                if (position == RecyclerView.NO_POSITION) {
                    return;
                }
                position -= getHeaderLayoutCount();
                int viewType = baseViewHolder.getItemViewType();
                BaseItemDataBinder baseItemDataBinder = mItemDataBinders.get(viewType);
                baseItemDataBinder.onItemClick(baseViewHolder,mDataList.get(position),position);
            }
        });
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseItemDataBinder itemDataBinder = mItemDataBinders.get(viewType);
        itemDataBinder.mContext = parent.getContext();
        int layoutId = itemDataBinder.getLayoutId();
        return createBaseViewHolder(parent, layoutId);
    }
}
