package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrCityListEntity extends BaseEntity {
    public List<ImrCityEntity> imrQueryCityInfoList = new ArrayList<>();
}
