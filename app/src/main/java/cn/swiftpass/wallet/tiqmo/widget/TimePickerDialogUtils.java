package cn.swiftpass.wallet.tiqmo.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Calendar;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.builder.TimePickerBuilder;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.CustomListener;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.view.TimePickerView;


public class TimePickerDialogUtils {

    private static final String[] TIME_EN = {"January", "February", "March", "April", "May", "June", "July", "Aguest",
            "September", "October", "November", "December"};

    public static void showTimePicker(Context context,
                                      Calendar selectedDate, Calendar startDate, Calendar endDate,
                                      OnTimeSelectListener listener) {
        showTimePicker(context,
                selectedDate, startDate, endDate,
                listener, null);
    }

    public static void showTimePicker(Context context,
                                      Calendar selectedDate, Calendar startDate, Calendar endDate,
                                      OnTimeSelectListener listener, DialogInterface.OnDismissListener onDismissListener) {

        TimeCustomListener timeCustomListener = new TimeCustomListener();
        TimePickerView pvCustomTime = new TimePickerBuilder(context, listener)
                .setLayoutRes(R.layout.dialog_pickerview_custom_time, timeCustomListener)
                .setOutSideCancelable(true)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .isDialog(true)
                .setLunarCalendar(false)
                .setLabel("","","","","","")
                .setContentTextSize(24).setDividerColor(context.getColor(R.color.transparent))
                .isCyclic(false).setBgColor(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.login_welcome_bg_color)))
                .setItemVisibleCount(7).isAlphaGradient(true).setTextColorCenter(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_white_090a15)))
                .setTextColorOut(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_30gray_979797)))
                .setType(new boolean[]{true, true, true, false, false, false})
                .setTextXOffset(40, 0, -40, 0, 0, 0)
                .build();
        timeCustomListener.setTimePickerView(pvCustomTime);

        Dialog mDialog = pvCustomTime.getDialog();
        if (onDismissListener != null) {
            mDialog.setOnDismissListener(onDismissListener);
        }
        if (mDialog != null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvCustomTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
                layoutParams.width = AndroidUtils.getScreenWidth(context);
                dialogWindow.setAttributes(layoutParams);

                dialogWindow.setWindowAnimations(R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
        pvCustomTime.show();
    }


    public static void showTimePickerWithoutDay(Context context,
                                      Calendar selectedDate, Calendar startDate, Calendar endDate,
                                      OnTimeSelectListener listener) {

        TimeCustomListener timeCustomListener = new TimeCustomListener();
        TimePickerView pvCustomTime = new TimePickerBuilder(context, listener)
                .setLayoutRes(R.layout.dialog_pickerview_custom_time, timeCustomListener)
                .setOutSideCancelable(true)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .isDialog(true)
                .setLunarCalendar(false)
                .setLabel("","","","","","")
                .setContentTextSize(24).setDividerColor(context.getColor(R.color.transparent))
                .isCyclic(false).setBgColor(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.login_welcome_bg_color)))
                .setItemVisibleCount(7).isAlphaGradient(true).setTextColorCenter(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_white_090a15)))
                .setTextColorOut(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_30gray_979797)))
                .setType(new boolean[]{true, true, false, false, false, false})
                .setTextXOffset(40, 0, -40, 0, 0, 0)
                .build();
        timeCustomListener.setTimePickerView(pvCustomTime);

        Dialog mDialog = pvCustomTime.getDialog();
        if (mDialog != null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvCustomTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
                layoutParams.width = AndroidUtils.getScreenWidth(context);
                dialogWindow.setAttributes(layoutParams);

                dialogWindow.setWindowAnimations(R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
        pvCustomTime.show();
    }

    public static void showTimePickerByType(Context context,
                                                Calendar selectedDate, Calendar startDate, Calendar endDate,
                                                OnTimeSelectListener listener,int type) {
        boolean showYear = false,showMonth = false,showDay = false;
        if(type == 1){
            showYear = true;
            showMonth = false;
            showDay = false;
        }

        SpendingDateListener timeCustomListener = new SpendingDateListener();
        TimePickerView pvCustomTime = new TimePickerBuilder(context, listener)
                .setLayoutRes(R.layout.dialog_choose_year, timeCustomListener)
                .setOutSideCancelable(true)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .isDialog(true)
                .setLunarCalendar(false)
                .setLabel("","","","","","")
                .setContentTextSize(24).setDividerColor(context.getColor(R.color.color_eaeef3))
                .isCyclic(false).setBgColor(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.login_welcome_bg_color)))
                .setItemVisibleCount(7).isAlphaGradient(true).setTextColorCenter(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_white_090a15)))
                .setTextColorOut(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_30gray_979797)))
                .setType(new boolean[]{showYear, showMonth, showDay, false, false, false})
                .setTextXOffset(40, 0, -40, 0, 0, 0)
                .build();
        timeCustomListener.setTimePickerView(pvCustomTime);

        Dialog mDialog = pvCustomTime.getDialog();
        if (mDialog != null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvCustomTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
                layoutParams.width = AndroidUtils.getScreenWidth(context);
                dialogWindow.setAttributes(layoutParams);

                dialogWindow.setWindowAnimations(R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
        pvCustomTime.show();
    }
    /**
     * 增加dismiss监听的月份选择
     *
     * @param context
     * @param selectedDate
     * @param startDate
     * @param endDate
     * @param listener
     */
    public static void showMonthPicker(Context context,
                                       Calendar selectedDate, Calendar startDate, Calendar endDate,
                                       OnTimeSelectListener listener) {

        TimeCustomListener timeCustomListener = new TimeCustomListener();
        TimePickerView pvCustomTime = new TimePickerBuilder(context, listener)
                .setLayoutRes(R.layout.dialog_pickerview_custom_time, timeCustomListener)
                .setOutSideCancelable(true)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .isDialog(true)
                .setLunarCalendar(false)
                .setLabel("","","","","","")
                .setType(new boolean[]{true, true, false, false, false, false})
                .setTextXOffset(100, -100, 0, 0, 0, 0)
                .build();
        timeCustomListener.setTimePickerView(pvCustomTime);

        Dialog mDialog = pvCustomTime.getDialog();
        if (mDialog != null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvCustomTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
                layoutParams.width = AndroidUtils.getScreenWidth(context);
                dialogWindow.setAttributes(layoutParams);

                dialogWindow.setWindowAnimations(R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
        pvCustomTime.show();
    }

    private static class SpendingDateListener implements CustomListener {
        TimePickerView mTimePickerView;

        void setTimePickerView(TimePickerView timePickerView) {
            mTimePickerView = timePickerView;
        }

        @Override
        public void customLayout(View v) {
            TextView tvCancel = v.findViewById(R.id.tv_cancel);
            TextView tvConfirm = v.findViewById(R.id.tv_confirm);
            tvConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTimePickerView.returnData();
                    mTimePickerView.dismiss();
                }
            });
            tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTimePickerView.dismiss();
                }
            });
        }
    }

    private static class TimeCustomListener implements CustomListener {
        TimePickerView mTimePickerView;

        void setTimePickerView(TimePickerView timePickerView) {
            mTimePickerView = timePickerView;
        }

        @Override
        public void customLayout(View v) {
            Button btnConfirm = v.findViewById(R.id.btn_confirm);
            Button btnCancel = v.findViewById(R.id.btn_cancel);
            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTimePickerView.returnData();
                    mTimePickerView.dismiss();
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTimePickerView.dismiss();
                }
            });
        }
    }
}
