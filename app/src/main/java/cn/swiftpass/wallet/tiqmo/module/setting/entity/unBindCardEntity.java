package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * Created by aijingya on 2020/7/13.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.entity
 * @Description:
 * @date 2020/7/13.20:11.
 */
public class unBindCardEntity extends BaseEntity {
    private String partnerNo;
    private String partnerUserId;
    private String openId;
    private String protocolNo;
    private String unbindStatus;


    public String getPartnerNo() {
        return partnerNo;
    }

    public void setPartnerNo(String partnerNo) {
        this.partnerNo = partnerNo;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getProtocolNo() {
        return protocolNo;
    }

    public void setProtocolNo(String protocolNo) {
        this.protocolNo = protocolNo;
    }

    public String getUnbindStatus() {
        return unbindStatus;
    }

    public void setUnbindStatus(String unbindStatus) {
        this.unbindStatus = unbindStatus;
    }
}
