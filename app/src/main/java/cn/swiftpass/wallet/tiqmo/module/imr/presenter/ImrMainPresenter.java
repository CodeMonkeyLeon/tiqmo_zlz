package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

public class ImrMainPresenter implements ImrMainContract.IImrMainPresenter {

    private ImrMainContract.ImrMainView mImrMainView;

    @Override
    public void attachView(ImrMainContract.ImrMainView imrMainView) {
        this.mImrMainView = imrMainView;
    }

    @Override
    public void detachView() {
        this.mImrMainView = null;
    }
}
