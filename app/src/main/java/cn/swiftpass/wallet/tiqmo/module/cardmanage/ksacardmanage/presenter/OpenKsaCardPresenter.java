package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class OpenKsaCardPresenter implements CardKsaContract.OpenCardPresenter {

    private CardKsaContract.OpenCardView mOpenCardView;

    @Override
    public void openKsaCardAccount(String productType) {
        AppClient.getInstance().getCardManager().openKsaCardAccount(productType, new LifecycleMVPResultCallback<Void>(mOpenCardView,true) {
            @Override
            protected void onSuccess(Void result) {
                mOpenCardView.openKsaCardAccountSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mOpenCardView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaCardSummary(String cardType, String cardProductType) {
        AppClient.getInstance().getCardManager().getKsaCardSummary(cardType,cardProductType, new LifecycleMVPResultCallback<KsaCardSummaryEntity>(mOpenCardView,true) {
            @Override
            protected void onSuccess(KsaCardSummaryEntity result) {
                mOpenCardView.getKsaCardSummarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mOpenCardView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardKsaContract.OpenCardView openCardView) {
        this.mOpenCardView = openCardView;
    }

    @Override
    public void detachView() {
        this.mOpenCardView = null;
    }
}
