package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zrq.spanbuilder.Spans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.BudgetEditAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.BudgetListAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.AnalysisContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.BudgetCategoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.SpendingBudgetPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BudgetCategoryDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class SpendingBudgetFragment extends BaseFragment<AnalysisContract.BudgetPresenter> implements AnalysisContract.BudgetView {


    @BindView(R.id.tv_top_title)
    TextView tvTopTitle;
    @BindView(R.id.tv_edit_title)
    TextView tvEditTitle;
    @BindView(R.id.tv_total_budget_money)
    TextView tvTotalBudgetMoney;
    @BindView(R.id.tv_add_category)
    TextView tvAddCategory;
    @BindView(R.id.tv_add_more)
    TextView tvAddMore;
    @BindView(R.id.rl_edit_category)
    RecyclerView rlEditCategory;
    @BindView(R.id.tv_submit)
    TextView tvSubmit;
    @BindView(R.id.ll_edit_category)
    LinearLayout llEditCategory;
    @BindView(R.id.rl_category_list)
    RecyclerView rlCategoryList;
    @BindView(R.id.ll_add_more)
    LinearLayout llAddMore;
    @BindView(R.id.ll_category_list)
    LinearLayout llCategoryList;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;
    @BindView(R.id.con_total_money)
    ConstraintLayout conTotalMoney;
    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;

    private BudgetListAdapter budgetListAdapter;
    private BudgetEditAdapter budgetEditAdapter;
    private List<SpendingBudgetEntity> spendingBudgetList = new ArrayList<>();

    //编辑保存和更新的分类列表 跟chooseBudgetList数据不能重复
    private List<SpendingBudgetEntity> budgetSaveList = new ArrayList<>();

    //删除的分类id 传给后台
    private List<String> deleteBudgetIdList = new ArrayList<>();

    //配置获取所有的category
    private List<BudgetCategoryEntity> spendingAnalysisFilterConditions = new ArrayList<>();

    //可以选择的category列表   如果界面已经选了部分列表   则在选择dialog中移除界面已存在的数据
    private List<SpendingBudgetEntity> chooseBudgetList = new ArrayList<>();
    //判断是否编辑过
    private boolean isSubmit;
    private String totalAlreadySpendingAmount;
    private String totalSpendingBudgetAmount;

    private ImageView ivNoContent;
    private View noContentView;
    private StatusView noStatusView;
    private SpendingBudgetListEntity mSpendingBudgetListEntity;
    public static SpendingBudgetFragment spendingBudgetFragment;

    public static SpendingBudgetFragment getInstance() {
        SpendingBudgetFragment  spendingBudgetFragment = new SpendingBudgetFragment();
        return spendingBudgetFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_budget;
    }

    @Override
    protected void initView(View parentView) {
        spendingBudgetFragment = this;
        initNoContentView();
        initRecyclerView();

        tvTotalBudgetMoney.setText(Spans.builder()
                .text("0 " + LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                .build());
        mPresenter.getSpendingBudgetList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        spendingBudgetFragment = null;
    }

    /**
     * 如果没有保存过就用配置接口返回的所有值  显示编辑页面
     */
    private void setDefaultSaveList() {
        budgetSaveList.clear();
        chooseBudgetList.clear();
        spendingBudgetList.clear();
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            spendingAnalysisFilterConditions = configEntity.spendingAnalysisFilterConditions;
            int size = spendingAnalysisFilterConditions.size();
            for (int i = 0; i < size; i++) {
                BudgetCategoryEntity budgetCategoryEntity = spendingAnalysisFilterConditions.get(i);
                SpendingBudgetEntity spendingBudgetEntity = new SpendingBudgetEntity();
                spendingBudgetEntity.categoryName = budgetCategoryEntity.title;
                spendingBudgetEntity.categoryCode = Integer.parseInt(budgetCategoryEntity.value);
                spendingBudgetEntity.budgetLimitAmount = "0";
                spendingBudgetEntity.id = "";
//                spendingBudgetEntity.categoryCode = i + 1;
                spendingBudgetEntity.categoryOrder = i + 1;
                chooseBudgetList.add(spendingBudgetEntity);
                budgetSaveList.add(spendingBudgetEntity);
            }
            if (budgetEditAdapter != null) {
                budgetEditAdapter.setDataList(budgetSaveList);
            }
        }
    }

    private void initNoContentView() {
        noContentView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_spending, null);
        TextView tvContent = noContentView.findViewById(R.id.tv_result_content);
        TextView tvNoContent = noContentView.findViewById(R.id.tv_no_content);
        tvContent.setVisibility(View.VISIBLE);
        ivNoContent = noContentView.findViewById(R.id.iv_no_content);
        ivNoContent.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_spending));
        tvContent.setText(getString(R.string.IMR_new_8));
        tvNoContent.setText(getString(R.string.IMR_new_9));
        noStatusView = new StatusView.Builder(mContext, scrollView).setNoContentView(noContentView).build();
    }

    private void initRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rlCategoryList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rlCategoryList.setLayoutManager(manager);
        budgetListAdapter = new BudgetListAdapter(spendingBudgetList);
        budgetListAdapter.bindToRecyclerView(rlCategoryList);

        LinearLayoutManager saveManager = new LinearLayoutManager(mContext);
        MyItemDecoration saveDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rlEditCategory.addItemDecoration(saveDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rlEditCategory.setLayoutManager(saveManager);
        budgetEditAdapter = new BudgetEditAdapter(budgetSaveList);
        budgetEditAdapter.bindToRecyclerView(rlEditCategory);

        budgetEditAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.iv_minus:
                        SpendingBudgetEntity spendingBudgetEntity = budgetSaveList.get(position);
                        budgetSaveList.remove(position);
                        chooseBudgetList.add(spendingBudgetEntity);
//                        Collections.sort(budgetSaveList, new SpendingBudgetComparator());
                        Collections.sort(chooseBudgetList, new SpendingBudgetComparator());
                        budgetEditAdapter.setDataList(budgetSaveList);
                        deleteBudgetIdList.add(spendingBudgetEntity.id);
                        if(chooseBudgetList.size()>0){
                            tvAddCategory.setVisibility(View.VISIBLE);
                        }else{
                            tvAddCategory.setVisibility(View.GONE);
                        }
                        tvSubmit.setEnabled(true);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(bgMain, R.attr.splash_bg_color);
        helper.setBackgroundResourceByAttr(conTotalMoney, R.attr.shape_051446_eaeef3_10);
        helper.setBackgroundResourceByAttr(llEditCategory, R.attr.bg_030b28_white);
        helper.setBackgroundResourceByAttr(tvSubmit, R.attr.bg_btn_next_page_card);
        helper.setBackgroundResourceByAttr(llAddMore, R.attr.shape_1da1f1_fc4f00_10);
        helper.setTextColorByAttr(tvTopTitle, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvEditTitle, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvAddCategory, R.attr.color_1da1f1_fc4f00);
        helper.setTextColorByAttr(tvAddMore, R.attr.color_1da1f1_fc4f00);
        helper.setTextColorByAttr(tvTotalBudgetMoney, R.attr.color_white_3a3b44);

        Drawable drawable_add = getResources().getDrawable(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_add_new));
        drawable_add.setBounds(0, 0, drawable_add.getMinimumWidth(), drawable_add.getMinimumHeight());
        tvAddCategory.setCompoundDrawables(drawable_add, null, null, null);
        tvAddMore.setCompoundDrawables(drawable_add, null, null, null);
        if(budgetEditAdapter != null){
            budgetEditAdapter.changeTheme();
        }
        if(budgetListAdapter != null){
            budgetListAdapter.changeTheme();
        }
    }

    @Override
    protected void restart() {
        isSubmit = false;
        mPresenter.getSpendingBudgetList();
    }

    public void saveEditData(List<SpendingBudgetEntity> spendingBudgetList) {
        if(tvSubmit != null) {
            tvSubmit.setEnabled(false);
            if (spendingBudgetList != null && spendingBudgetList.size() > 0) {
                int size = spendingBudgetList.size();
                for (int i = 0; i < size; i++) {
                    SpendingBudgetEntity spendingBudgetEntity = spendingBudgetList.get(i);
                    String budgetLimitAmount = spendingBudgetEntity.budgetLimitAmount;
                    if (!BigDecimalFormatUtils.isZero(budgetLimitAmount)) {
                        tvSubmit.setEnabled(true);
                    }
                }
            }
        }
    }

    private void showBudgetCategoryDialog() {
        BudgetCategoryDialog budgetCategoryDialog = new BudgetCategoryDialog(mContext);
        budgetCategoryDialog.setDateList(chooseBudgetList);
        budgetCategoryDialog.setBudgetCategoryItemClickListener(new BudgetCategoryDialog.BudgetCategoryItemClickListener() {
            @Override
            public void chooseBudgetCategory(SpendingBudgetEntity spendingBudgetEntity) {
                try {
                    spendingBudgetEntity.budgetLimitAmount = "0";
                    budgetSaveList.add(0, spendingBudgetEntity);
//                Collections.sort(budgetSaveList, new SpendingBudgetComparator());
                    budgetEditAdapter.setDataList(budgetSaveList);
                    chooseBudgetList.remove(spendingBudgetEntity);
                    Collections.sort(chooseBudgetList, new SpendingBudgetComparator());
                    if (chooseBudgetList.size() > 0) {
                        tvAddCategory.setVisibility(View.VISIBLE);
                    } else {
                        tvAddCategory.setVisibility(View.GONE);
                    }
                    if (deleteBudgetIdList.contains(spendingBudgetEntity.id)) {
                        deleteBudgetIdList.remove(spendingBudgetEntity.id);
                    }
                }catch (Exception e){
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        });
        budgetCategoryDialog.showWithBottomAnim();
    }

    @OnClick({R.id.tv_add_category, R.id.tv_submit, R.id.ll_add_more})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_category:
                showBudgetCategoryDialog();
                break;
            case R.id.tv_submit:
                mPresenter.saveUserSpending(budgetSaveList, deleteBudgetIdList);
                break;
            case R.id.ll_add_more:
                llCategoryList.setVisibility(View.GONE);
                llEditCategory.setVisibility(View.VISIBLE);
                tvTotalBudgetMoney.setText(AndroidUtils.getTransferStringMoney(totalSpendingBudgetAmount) + " " + LocaleUtils.getCurrencyCode(""));
                break;
            default:
                break;
        }
    }

    public class SpendingBudgetComparator implements Comparator<SpendingBudgetEntity> {

        @Override
        public int compare(SpendingBudgetEntity o1, SpendingBudgetEntity o2) {
            return o1.categoryCode - o2.categoryCode;
        }
    }

    @Override
    public void saveUserSpendingSuccess(Void result) {
        isSubmit = true;
        mPresenter.getSpendingBudgetList();
    }

    @Override
    public void getSpendingBudgetListSuccess(SpendingBudgetListEntity spendingBudgetListEntity) {
        try {
            showStatusView(noStatusView,StatusView.CONTENT_VIEW);
            setDefaultSaveList();

            deleteBudgetIdList.clear();
            if (spendingBudgetListEntity != null) {
                mSpendingBudgetListEntity = spendingBudgetListEntity;
                totalAlreadySpendingAmount = spendingBudgetListEntity.totalAlreadySpendingAmount;
                totalSpendingBudgetAmount = spendingBudgetListEntity.totalSpendingBudgetAmount;
                if (!TextUtils.isEmpty(totalSpendingBudgetAmount)) {
                    tvTotalBudgetMoney.setText(Spans.builder()
                            .text(AndroidUtils.getTransferStringMoney(totalAlreadySpendingAmount) + "/").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                            .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                            .text(AndroidUtils.getTransferIntNumber(totalSpendingBudgetAmount) + " " + LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                            .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());
                } else {
                    tvTotalBudgetMoney.setText(AndroidUtils.getTransferStringMoney(totalAlreadySpendingAmount) + " " + LocaleUtils.getCurrencyCode(""));
                }
                spendingBudgetList.addAll(spendingBudgetListEntity.userSpendingBudgetInfoList);
                if (spendingBudgetList.size() > 0) {
                    llCategoryList.setVisibility(View.VISIBLE);
                    llEditCategory.setVisibility(View.GONE);
                    budgetListAdapter.setDataList(spendingBudgetList);
                    budgetSaveList.clear();
                    budgetSaveList.addAll(spendingBudgetList);
                    budgetEditAdapter.setDataList(budgetSaveList);
                    int listSize = spendingBudgetList.size();
                    for (int i = 0; i < chooseBudgetList.size(); i++) {
                        SpendingBudgetEntity spendingBudgetEntity = chooseBudgetList.get(i);
                        for (int j = 0; j < listSize; j++) {
                            SpendingBudgetEntity spendingBudgetEntity1 = spendingBudgetList.get(j);
                            if (spendingBudgetEntity1.categoryCode == spendingBudgetEntity.categoryCode) {
                                chooseBudgetList.remove(i);
                                i--;
                            }
                        }
                    }
                } else {
                    chooseBudgetList.clear();
                    if(isSubmit){
                        llCategoryList.setVisibility(View.VISIBLE);
                        llEditCategory.setVisibility(View.GONE);
                    }else{
                        llCategoryList.setVisibility(View.GONE);
                        llEditCategory.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                tvTotalBudgetMoney.setText("0 "+LocaleUtils.getCurrencyCode(""));
                totalSpendingBudgetAmount = "0";
                chooseBudgetList.clear();
                if(isSubmit){
                    llCategoryList.setVisibility(View.VISIBLE);
                    llEditCategory.setVisibility(View.GONE);
                }else{
                    llCategoryList.setVisibility(View.GONE);
                    llEditCategory.setVisibility(View.VISIBLE);
                }
            }
            if(chooseBudgetList.size()>0){
                tvAddCategory.setVisibility(View.VISIBLE);
            }else{
                tvAddCategory.setVisibility(View.GONE);
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if(mSpendingBudgetListEntity == null) {
            showStatusView(noStatusView, StatusView.NO_CONTENT_VIEW);
        }
        showTipDialog(errorMsg);
    }

    @Override
    public AnalysisContract.BudgetPresenter getPresenter() {
        return new SpendingBudgetPresenter();
    }
}
