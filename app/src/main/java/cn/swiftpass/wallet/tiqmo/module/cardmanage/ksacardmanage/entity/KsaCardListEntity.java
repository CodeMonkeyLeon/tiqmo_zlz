package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class KsaCardListEntity extends BaseEntity {
    /**
     * {\"cardList\":[{\"proxyCardNo\":\"52494000155177960139\",\"cardType\":\"VIRTUAL\",\"cardFaceId\":\"02\",\"cardProductType\":\"Standard\",\"cardStatus\":\"ACTIVATE\"}]
     */
    public List<KsaCardEntity> cardList = new ArrayList<>();
}
