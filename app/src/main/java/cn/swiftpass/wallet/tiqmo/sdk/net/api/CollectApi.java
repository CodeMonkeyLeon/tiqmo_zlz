//package cn.swiftpass.wallet.tiqmo.sdk.net.api;
//
//import java.math.BigDecimal;
//
//public interface CollectApi {
//
//
//    @Headers({"Service-Id:1019"})
//    RequestCall<JsonResponse<GetCollectOrderListEntity>> searchGatheringOrder(@Param("qRCodeInfo") String qrCodeInfo);
//
//
//    @Headers({"Service-Id:1038"})
//    RequestCall<JsonResponse<OrderEntity>> confirmOrderAmount(@Param("orderNo") String orderNo,
//                                                              @Param("orderAmount") BigDecimal orderAmount,
//                                                              @Param("orderCurrencyCode") String orderCurrency,
//                                                              @Param("remark") String remark);
//
//
//    @Headers({"Service-Id:1009"})
//    RequestCall<JsonResponse<GetCollectQRCodeEntity>> getGatheringQRCode(@Param("amount") BigDecimal amount,
//                                                                         @Param("currencyCode") String currencyCode,
//                                                                         @Param("remark") String remark);
//
//
//    @Headers({"Service-Id:1037"})
//    RequestCall<JsonResponse<Void>> cancelCollectOrder(@Param("orderNo") String orderNo);
//}
