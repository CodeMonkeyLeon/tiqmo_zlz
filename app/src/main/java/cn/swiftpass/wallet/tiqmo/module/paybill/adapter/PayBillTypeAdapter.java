package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeIconEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.MyGridView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PayBillTypeAdapter extends BaseRecyclerAdapter<PayBillTypeEntity> {

    private PayBillTypeIconAdapter payBillTypeIconAdapter;

    private OnIconItemClick onItemClickListener;

    public void setOnIconItemClickListener(OnIconItemClick itemClickListener) {
        this.onItemClickListener = itemClickListener;
    }

    public interface OnIconItemClick {
        void onItemClick(PayBillTypeEntity payBillTypeEntity, PayBillTypeIconEntity payBillTypeIconEntity, int position);
    }

    public PayBillTypeAdapter(@Nullable List<PayBillTypeEntity> data) {
        super(R.layout.item_paybill_type, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillTypeEntity payBillTypeEntity, int position) {
        try {
            if (position == mDataList.size() - 1) {
                baseViewHolder.setGone(R.id.view_bottom, false);
            } else {
                baseViewHolder.setGone(R.id.view_bottom, true);
            }
            MyGridView gvIcon = baseViewHolder.getView(R.id.gv_type);
            baseViewHolder.setText(R.id.tv_type_title, payBillTypeEntity.billerTypeShow);

            payBillTypeIconAdapter = new PayBillTypeIconAdapter(mContext);
            gvIcon.setAdapter(payBillTypeIconAdapter);
            payBillTypeIconAdapter.changeData(payBillTypeEntity.billerDescriptionList);
            payBillTypeIconAdapter.setOnItemClickListener(new PayBillTypeIconAdapter.OnItemClick() {
                @Override
                public void onItemClick(PayBillTypeIconEntity payBillTypeIconEntity, int position) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(payBillTypeEntity, payBillTypeIconEntity, position);
                    }
                }
            });
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
