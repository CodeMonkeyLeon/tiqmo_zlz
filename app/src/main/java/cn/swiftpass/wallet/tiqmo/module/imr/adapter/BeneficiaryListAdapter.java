package cn.swiftpass.wallet.tiqmo.module.imr.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class BeneficiaryListAdapter extends BaseRecyclerAdapter<ImrBeneficiaryListEntity.ImrBeneficiaryInfo> {

    private OnViewClickListener listener;

    public void setOnViewClickListener(OnViewClickListener listener) {
        this.listener = listener;
    }

    public BeneficiaryListAdapter(@Nullable List<ImrBeneficiaryListEntity.ImrBeneficiaryInfo> data) {
        super(R.layout.item_imr_beneficiary_list, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, ImrBeneficiaryListEntity.ImrBeneficiaryInfo item, int relationship) {
        if (item == null) {
            return;
        }
        LinearLayout llRelationshipContent = baseViewHolder.getView(R.id.ll_relationship_content);
        if (item.imrPayeeDetailInfos == null || item.imrPayeeDetailInfos.size() <= 0) {
            llRelationshipContent.setVisibility(View.GONE);
            return;
        }
        LinearLayout llRelationshipItem = baseViewHolder.getView(R.id.ll_relationship_title);
        ImageView ivRelationship = baseViewHolder.getView(R.id.iv_relationship);
        ImageView ivArrow = baseViewHolder.getView(R.id.iv_arrow);
        TextView tvRelationship = baseViewHolder.getView(R.id.tv_relationship);

        RecyclerView rvRelationship = baseViewHolder.getView(R.id.rv_relationship_list);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        rvRelationship.setLayoutManager(manager);

        if (item.isShowSubView && item.imrPayeeDetailInfos.size() > 0) {
            rvRelationship.setVisibility(View.VISIBLE);
            ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
        } else {
            rvRelationship.setVisibility(View.GONE);
            ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        }


        ImrBeneficiaryItemAdapter adapter = new ImrBeneficiaryItemAdapter(item.imrPayeeDetailInfos);
        adapter.setOnItemClickListener(new ImrBeneficiaryItemAdapter.OnItemClickListener() {
            @Override
            public void onItemDeleteClick(int position) {
                if (listener != null) {
                    listener.onItemDeleteClick(relationship, position);
                }
            }

            @Override
            public void onItemEditClick(int position) {
                if (listener != null) {
                    listener.onItemEditClick(relationship, position);
                }
            }

            @Override
            public void onItemSendClick(int position) {
                if (listener != null) {
                    listener.onItemSendClick(relationship, position);
                }
            }

            @Override
            public void onItemDeleteShow(int position, boolean isShow) {
                if (listener != null) {
                    listener.onItemDeleteShow(relationship, position, isShow);
                }
            }

            @Override
            public void onItemBenOPShow(int position, boolean isShow) {
                if (listener != null) {
                    listener.onItemBenOPShow(relationship, position, isShow);
                }
            }
        });
        rvRelationship.setAdapter(adapter);


        LocaleUtils.viewRotationY(mContext, ivArrow);
        if (getRelationshipSrc(item.relationshipCode) != -1) {
            ivRelationship.setImageResource(getRelationshipSrc(item.relationshipCode));
        }

        if (!TextUtils.isEmpty(item.relationshipCode)) {
            tvRelationship.setText(getRelationshipDesc(item.relationshipCode));
        }
        llRelationshipItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.imrPayeeDetailInfos == null || item.imrPayeeDetailInfos.size() <= 0) {
                    return;
                }
                if (rvRelationship.getVisibility() == View.VISIBLE) {
                    ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                    rvRelationship.setVisibility(View.GONE);
                    item.isShowSubView = false;
                } else {
                    ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                    rvRelationship.setVisibility(View.VISIBLE);
                    item.isShowSubView = true;
                }
            }
        });
    }

    private String getRelationshipDesc(String relationshipCode) {
        String relationship = "";
        if ("RELATIONSHIP_1".equals(relationshipCode)) {
            relationship = mContext.getString(R.string.IMR_58);
        } else if ("RELATIONSHIP_2".equals(relationshipCode)) {
            relationship = mContext.getString(R.string.IMR_59);
        } else if ("RELATIONSHIP_3".equals(relationshipCode)) {
            relationship = mContext.getString(R.string.IMR_60);
        } else if ("RELATIONSHIP_4".equals(relationshipCode)) {
            relationship = mContext.getString(R.string.IMR_61);
        } else if ("RELATIONSHIP_5".equals(relationshipCode)) {
            relationship = mContext.getString(R.string.IMR_62);
        }
        return relationship;
    }


    private int getRelationshipSrc(String relationshipCode) {
        int relationship = -1;
        if ("RELATIONSHIP_1".equals(relationshipCode)) {
            relationship = ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_my_account);
        } else if ("RELATIONSHIP_2".equals(relationshipCode)) {
            relationship = ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_family);
        } else if ("RELATIONSHIP_3".equals(relationshipCode)) {
            relationship = ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_friends);
        } else if ("RELATIONSHIP_4".equals(relationshipCode)) {
            relationship = ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_service);
        } else if ("RELATIONSHIP_5".equals(relationshipCode)) {
            relationship = ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_other_relation);
        }
        return relationship;
    }

    public interface OnViewClickListener {
        void onItemDeleteClick(int relationship, int position);

        void onItemEditClick(int relationship, int position);

        void onItemSendClick(int relationship, int position);

        void onItemDeleteShow(int relationship, int position, boolean isShow);

        void onItemBenOPShow(int relationship, int position, boolean isShow);
    }
}

