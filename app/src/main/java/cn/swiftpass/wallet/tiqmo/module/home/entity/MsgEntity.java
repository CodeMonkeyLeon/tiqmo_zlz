package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MsgEntity extends BaseEntity {

    /**
     MSG_RESULT_NOTIFY("NOTIFY","通知"),
     REQUEST_SUBJECT_TRANSFER("TRANSFER","转账"),
     REQUEST_SUBJECT_PAYMENT("PAYMENT","支付"),
     REQUEST_SUBJECT_ADD_MONEY("ADD_MONEY","充值"),
     REQUEST_SUBJECT_BIND_CARD("BindCard","绑卡"),
     REQUEST_SUBJECT_REGISTERATION("REGISTERATION","注册"),
     MSG_SUBJECT_CQR_PAYMENT("CQR_PAYMENT" ,"支付二维码支付成功"),
     MSG_SUBJECT_TRANSFER_BANK("TRANSFER_BANK","转账到IBAN银行账户成功"),//转账到IBAN银行账户失败
     MSG_SUBJECT_REQUEST_TRANSFER_APPROVED("REQUEST_TRANSFER_APPROVED","同意转账请求并付款成功通知请求方"),
     MSG_SUBJECT_REQUEST_TRANSFER_REJECTED("REQUEST_TRANSFER_REJECTED","拒绝对方转账请求通知请求方"),
     MSG_SUBJECT_MOBILE_TOPUP("MOBILE_TOPUP","手机充值成功通知"),
     MSG_SUBJECT_REMITTANCE_PROCESSING("I18N_REMITTANCE_DOING","国际汇款处理中通知"),
     MSG_SUBJECT_REMTTIANCE_ACCEPTANCE("I18N_REMTTIANCE_ACCEPT" ,"国际汇款接受通知"),
     MSG_SUBJECT_REMITTANCE_REJECTED("I18N_REMITTANCE_REJECTED","国际汇款拒绝通知"),
     MSG_SUBJECT_OFFLINE_PAY_ACCEPT("OFF_PAY_ACCEPT","国线下支付接受通知"),
     MSG_SUBJECT_OFFLINE_PAY_REFUND("OFF_PAY_REFUND","国线下支付退款通知"),
     MSG_SUBJECT_UTILITY_BILLS_PAYMENT("UTILITES_BILLS_PAYMENT","utility payment success"),
     MSG_SUBJECT_TUITION_PAY("TUITION_PAYMENT","缴学费支付"),
     MSG_SUBJECT_SPLIT_BILL("SPLIT_BILL","拆分账单"),
     MSG_SUBJECT_TERMS_CONDITIONS("TERMS_CONDITIONS","协议条款"),
     MSG_SUBJECT_MARKETING_CAMPAGINS("MARKETING_CAMPAGINS","营销活动"),
     MSG_SUBJECT_CHANGE_LOGIN_PASSWORD("LOGIN_PWD_CHANGE","修改登录密码"),
     MSG_SUBJECT_RESET_LOGIN_PASSWORD("LOGIN_PWD_RESET","重置登录密码"),
     MSG_SUBJECT_CHANGE_PAYMENT_PASSWORD("PAYMENT_PWD_CHANGE","修改支付密码"),
     MSG_SUBJECT_RESET_PAYMENT_PASSWORD("PAYMENT_PWD_RESET" ,"重置支付密码")
     */

    /**
     * {\"msgSubject\":\"ADD_MONEY\",\"msgTitle\":\"Your Wallet balance has been credited with 5.00 SAR from ************9996\",\"msgFromNowTimeGap\":\"10 days\",\"msgId\":\"MSG9660103009531597645437077\",\"msgStatus\":\"MSG_STATUS_MQ_SENDED\",\"msgUpdateTime\":\"1597645442296\",\"msgCode\":\"000000\"}
     */

    public String msgId;
    public String msgTitle;
    public String inAppTitle;
    public MsgContentEntity msgContent;
    public MsgBizParamEntity msgBizParam;
    public String msgSubject;
    public String msgStatus;
    public String msgUpdateTime;
    public String msgFromNowTimeGap;
    public String msgCode;
    public String orderNo;
    public String clickTime;
}
