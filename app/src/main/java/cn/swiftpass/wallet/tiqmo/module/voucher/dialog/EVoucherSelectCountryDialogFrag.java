package cn.swiftpass.wallet.tiqmo.module.voucher.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.voucher.adapter.SelectStoreAdapter;
import cn.swiftpass.wallet.tiqmo.module.voucher.adapter.SelectTopUpAdapter;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class EVoucherSelectCountryDialogFrag extends BaseDialogFragmentN {

    private final static String E_VOUCHER = "EVoucher";
    private final static String TOP_UP = "TopUp";

    private TextView selectStoreTitle;
    private MaxHeightRecyclerView rvSelectStore;

    private VouSupCountryEntity countryEntity;
    private CountryClickListener countryClickListener;

    private ArrayList<EVoucherEntity.StoreInfo> eVoucherEntity;
    private OnOperatorClickListener listener;

    public static String TAG = "TopUpSelect";

    public void setListener(OnOperatorClickListener listener) {
        this.listener = listener;
    }

    public void setOnCountryClickListener(CountryClickListener listener) {
        this.countryClickListener = listener;
    }

    private static String sceneType;


    private volatile static EVoucherSelectCountryDialogFrag singleton;
    private EVoucherSelectCountryDialogFrag (){}
    public static EVoucherSelectCountryDialogFrag newInstance(VouSupCountryEntity countryEntity) {
        if (singleton == null) {
            synchronized (EVoucherSelectCountryDialogFrag.class) {
                if (singleton == null) {
                    singleton = new EVoucherSelectCountryDialogFrag();
                }
            }
        }
        singleton.countryEntity = countryEntity;
        sceneType = E_VOUCHER;
        return singleton;
    }

    public static EVoucherSelectCountryDialogFrag newInstance(ArrayList<EVoucherEntity.StoreInfo> eVoucherEntity) {
        if (singleton == null) {
            synchronized (EVoucherSelectCountryDialogFrag.class) {
                if (singleton == null) {
                    singleton = new EVoucherSelectCountryDialogFrag();
                }
            }
        }
        singleton.eVoucherEntity = eVoucherEntity;
        sceneType = TOP_UP;
        return singleton;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_evoucher_select;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
        initData();
    }

    private void initData() {
        if (E_VOUCHER.equals(sceneType)) {
            SelectStoreAdapter adapter = new SelectStoreAdapter(countryEntity.voucherSupportCountry);
            adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                    if (countryClickListener != null) {
                        countryClickListener.onCountryClick(countryEntity, position);
                    }
                    dismiss();
                }
            });
            rvSelectStore.setAdapter(adapter);
        } else if (TOP_UP.equals(sceneType)) {
            SelectTopUpAdapter adapter = new SelectTopUpAdapter(eVoucherEntity);
            adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                    if (listener != null) {
                        listener.onOperatorClick(position);
                    }
                    dismiss();
                }
            });
            rvSelectStore.setAdapter(adapter);
        }
    }

    private void initView(View parentView) {
        selectStoreTitle = parentView.findViewById(R.id.tv_select_store_title);
        rvSelectStore = parentView.findViewById(R.id.rv_select_store);

        if (E_VOUCHER.equals(sceneType)) {
            selectStoreTitle.setText(getString(R.string.trx_detail_8));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(AndroidUtils.dip2px(mContext, 24), AndroidUtils.dip2px(mContext, 27),
                    AndroidUtils.dip2px(mContext, 24), AndroidUtils.dip2px(mContext, 20));
            selectStoreTitle.setLayoutParams(lp);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
            rvSelectStore.setLayoutManager(gridLayoutManager);
            rvSelectStore.setMaxHeight(AndroidUtils.dip2px(mContext, 390));
        } else if (TOP_UP.equals(sceneType)) {
            selectStoreTitle.setText(getString(R.string.topup_1));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(AndroidUtils.dip2px(mContext, 41), AndroidUtils.dip2px(mContext, 27),
                    AndroidUtils.dip2px(mContext, 41), AndroidUtils.dip2px(mContext, 10));
            selectStoreTitle.setLayoutParams(lp);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
            rvSelectStore.setLayoutManager(gridLayoutManager);
            rvSelectStore.setMaxHeight(AndroidUtils.dip2px(mContext, 260));
        }
    }

    public void show(FragmentManager manager) {
        show(manager, TAG);
    }

    public interface CountryClickListener {
        void onCountryClick(VouSupCountryEntity countryEntity, int position);
    }

    public interface OnOperatorClickListener {
        void onOperatorClick(int position);
    }
}
