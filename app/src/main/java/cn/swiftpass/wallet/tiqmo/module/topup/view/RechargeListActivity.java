package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.adapter.TopUpCategoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeDomeOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherPayReviewActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherContract;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class RechargeListActivity extends BaseCompatActivity<EVoucherContract.Presenter> implements EVoucherContract.View {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.rv_evoucher_category)
    RecyclerView rvTopUpSelectCategory;
    @BindView(R.id.ll_notice_title)
    LinearLayout llNoticeTitle;
    @BindView(R.id.tv_notice)
    TextView tvNotice;
    @BindView(R.id.iv_notice)
    ImageView ivNotice;
    @BindView(R.id.sw_evoucher_category)
    SwipeRefreshLayout swEVoucherCategory;

    private RechargeDomeOrderEntity rechargeDomeOrderEntity;

    private EVoucherOrderInfoEntity mEVoucherOrderInfoEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activtiy_evoucher_select;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
        ReferralCodeDialogUtils.resetKeyDialog();
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                if (!RechargeListActivity.this.isFinishing()) {
                    finish();
                }
                break;
            default:
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.START_KYC == event.getEventType()) {
            if (userInfoEntity.isKycChecking()) {
                ActivitySkipUtil.startAnotherActivity(RechargeListActivity.this,
                        KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                ReferralCodeDialogUtils.showReferralCodeDialog(this, mContext);
            }
        }
    }

    private void initData() {
        rechargeDomeOrderEntity = (RechargeDomeOrderEntity) getIntent().getSerializableExtra("TopUpInfoList");
        if (rechargeDomeOrderEntity != null) {
            String logoUrl = ThemeUtils.isCurrentDark(mContext) ? rechargeDomeOrderEntity.operatorSmallDarkIcon :
                    rechargeDomeOrderEntity.operatorSmallLightIcon;
            if (!TextUtils.isEmpty(logoUrl)) {
                Glide.with(mContext).clear(ivNotice);
                Glide.with(mContext)
                        .load(logoUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivNotice);
            }
            mPresenter.requestVoucherInfo(rechargeDomeOrderEntity.voucherServiceProviderCode, rechargeDomeOrderEntity.voucherBrandCode,
                    "", Constants.sceneType_TopUp);
            swEVoucherCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    mPresenter.requestVoucherInfo(rechargeDomeOrderEntity.voucherServiceProviderCode,
                            rechargeDomeOrderEntity.voucherBrandCode, "", Constants.sceneType_TopUp);
                }
            });
        }
    }

    private void initView() {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        tvTitle.setText(mContext.getString(R.string.topup_2));
        swEVoucherCategory.setColorSchemeResources(R.color.color_B00931);
        swEVoucherCategory.setRefreshing(false);

        llNoticeTitle.setVisibility(View.VISIBLE);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvTopUpSelectCategory.setLayoutManager(manager);
    }

    @Override
    public EVoucherContract.Presenter getPresenter() {
        return new EVoucherPresenter();
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity, CheckOutEntity checkOutEntity) {
        HashMap<String, Object> limitMap = new HashMap<>();
        limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        if (rechargeDomeOrderEntity != null) {
            checkOutEntity.brandName = rechargeDomeOrderEntity.voucherBrandName;
            limitMap.put("brandName", rechargeDomeOrderEntity.voucherBrandName);
        }
        limitMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        limitMap.put(Constants.mEVoucherOrderInfoEntity, mEVoucherOrderInfoEntity);
        limitMap.put(Constants.sceneType, Constants.sceneType_TopUp);
        ActivitySkipUtil.startAnotherActivity(RechargeListActivity.this,
                EVoucherPayReviewActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void eVoucherOrderSuccess(EVoucherOrderInfoEntity eVoucherOrderInfoEntity) {
        if (eVoucherOrderInfoEntity != null) {
            mEVoucherOrderInfoEntity = eVoucherOrderInfoEntity;
            mPresenter.checkOut("", eVoucherOrderInfoEntity.orderInfo);
        }
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        mPresenter.getLimit(Constants.LIMIT_TRANSFER, checkOutEntity);
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        if("030216".equals(errCode)){
            showCommonErrorDialog(errMsg);
        }else{
            showTipDialog(errMsg);
        }
    }

    @Override
    public void showVoucherList(VouTypeAndInfoEntity result) {
        swEVoucherCategory.setRefreshing(false);
        TopUpCategoryAdapter adapter = new TopUpCategoryAdapter(RechargeListActivity.this, result.voucherTypeAndInfo);
        adapter.setVoucherClickListener(new TopUpCategoryAdapter.OnTopUpClickListener() {
            @Override
            public void onTopUpClick(int topUpCategory, int topUpPosition) {
                if (result.voucherTypeAndInfo.size() > 0) {
                    VouTypeAndInfoEntity.VouInfo vouInfo = result.voucherTypeAndInfo.get(topUpCategory).voucherInfos.get(topUpPosition);
                    mPresenter.eVoucherOrder(vouInfo.voucherCode, "");
                }
            }
        });
        rvTopUpSelectCategory.setAdapter(adapter);
    }
}
