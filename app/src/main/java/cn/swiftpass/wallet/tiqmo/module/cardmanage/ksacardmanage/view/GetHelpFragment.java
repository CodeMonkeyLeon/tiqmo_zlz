package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.Manifest;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.setting.view.HelpOneActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SdkShareUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class GetHelpFragment extends BaseFragment {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_help_title)
    TextView tvHelpTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.ll_how_help)
    LinearLayout llHowHelp;
    @BindView(R.id.ll_help_whatsapp)
    LinearLayout llHelpWhatsapp;
    @BindView(R.id.ll_help_phone)
    LinearLayout llHelpPhone;
    @BindView(R.id.ll_help_email)
    LinearLayout llHelpEmail;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private boolean isHideTitle, isActivity;
    private String hideTitle, topTitle;


    public static GetHelpFragment getInstance(boolean isHideTitle, String hideTitle, String topTitle, boolean isActivity) {
        GetHelpFragment getHelpFragment = new GetHelpFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isHideTitle", isHideTitle);
        bundle.putString("hideTitle", hideTitle);
        bundle.putString("topTitle", topTitle);
        bundle.putBoolean("isActivity", isActivity);
        getHelpFragment.setArguments(bundle);
        return getHelpFragment;
    }

    public static GetHelpFragment getInstance(boolean isActivity) {
        GetHelpFragment getHelpFragment = new GetHelpFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isActivity", isActivity);
        getHelpFragment.setArguments(bundle);
        return getHelpFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_get_help;
    }

    @Override
    protected void initView(View parentView) {
        tvTitle.setText(R.string.Help_support_1);
        LocaleUtils.viewRotationY(mContext, ivBack);
        if (getArguments() != null) {
            isHideTitle = getArguments().getBoolean("isHideTitle", false);
            hideTitle = getArguments().getString("hideTitle");
            topTitle = getArguments().getString("topTitle");
            isActivity = getArguments().getBoolean("isActivity", false);
            if (isActivity) {
                ivBack.setVisibility(View.VISIBLE);
            } else {
                ivBack.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(hideTitle)) {
                tvHelpTitle.setText(hideTitle);
            }
            if (!TextUtils.isEmpty(topTitle)) {
                tvTitle.setText(topTitle);
            }
            if (isHideTitle) {
                tvHelpTitle.setVisibility(View.GONE);
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.ll_help_whatsapp, R.id.ll_help_phone, R.id.ll_help_email, R.id.ll_how_help})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_how_help:
                ActivitySkipUtil.startAnotherActivity(mActivity, HelpOneActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.ll_help_whatsapp:
                //判断是否安装了whatsApp
                boolean isSuccess = SdkShareUtil.isAppInstalled(mContext, SdkShareUtil.WHATS_APP);
                if (!isSuccess) {
                } else {
                    SdkShareUtil.openWhatsApp(mContext, "", "");
                }
                break;
            case R.id.ll_help_phone:
                contactHelp();
                break;
            case R.id.ll_help_email:
                if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
                    PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                        @Override
                        public void acceptPermission() {
                            SdkShareUtil.sendEmail(mContext, Constants.call_email);
                        }

                        @Override
                        public void rejectPermission() {

                        }
                    }, Manifest.permission.READ_CONTACTS);
                } else {
                    SdkShareUtil.sendEmail(mContext, Constants.call_email);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }
}