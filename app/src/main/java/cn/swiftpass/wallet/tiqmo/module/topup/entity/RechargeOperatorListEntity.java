package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeOperatorListEntity extends BaseEntity {
    public List<RechargeOperatorEntity> operatorList = new ArrayList<>();
}
