package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AppManageEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class UpdateActivity extends BaseCompatActivity {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_update)
    TextView tvUpdate;
    @BindView(R.id.tv_log_out)
    TextView tvLogOut;

    private AppManageEntity appManageEntity;
    private String flag;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_update;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ivBack.setVisibility(View.GONE);
        LocaleUtils.viewRotationY(this, ivBack, headCircle);

        if (getIntent().getExtras() != null) {
            appManageEntity = (AppManageEntity) getIntent().getExtras().getSerializable(Constants.appManageEntity);
            if (appManageEntity != null) {
                flag = appManageEntity.flag;
            }
        }
    }

    public void startWebDownload(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri content_url = Uri.parse(url);
        intent.setData(content_url);
        context.startActivity(intent);
    }

    @OnClick({R.id.iv_back, R.id.tv_update, R.id.tv_log_out})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMaps = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_update:
                if (appManageEntity != null) {
                    startWebDownload(mContext, appManageEntity.installUrl);
                }
                break;
            case R.id.tv_log_out:
                if ("3".equals(flag)) {
                    android.os.Process.killProcess(android.os.Process.myPid());
                } else {
                    finish();
                }
                break;
            default:
                break;
        }
    }

}
