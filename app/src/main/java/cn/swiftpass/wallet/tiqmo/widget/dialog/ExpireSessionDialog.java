package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;

public class ExpireSessionDialog extends BottomDialog{

    private Context mContext;

    private String countTime;

    private ButtonListener buttonListener;


    private TextView tvMessage;
    private TextView tvTitle;

    private static TimeOverListener timeOverListener;

    public void setTimeOverListener(TimeOverListener timeOverListener) {
        this.timeOverListener = timeOverListener;
    }

    public interface TimeOverListener {
        void timeOver();
    }

    public ExpireSessionDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public void setButtonListener(final ButtonListener buttonListener) {
        this.buttonListener = buttonListener;
    }

    public void setCountTime(final String countTime) {
        this.countTime = countTime;
        String title = mContext.getString(R.string.sprint11_56)+"\n"+mContext.getString(R.string.sprint11_57);
        String content = mContext.getString(R.string.sprint11_58);
        if(tvTitle != null) {
            tvTitle.setText(title);
        }
        AdvancedCountdownTimer.getInstance().countDownEvent(!TextUtils.isEmpty(countTime)?Integer.parseInt(countTime):60, new AdvancedCountdownTimer.OnCountDownListener() {
            @Override
            public void onTick(int millisUntilFinished) {
                if(millisUntilFinished == 0){
                    if(timeOverListener != null){
                        timeOverListener.timeOver();
                    }
                }
                if(tvMessage != null) {
                    tvMessage.setText(content.replace("XXX", millisUntilFinished + ""));
                }
            }

            @Override
            public void onFinish() {
                if(timeOverListener != null){
                    timeOverListener.timeOver();
                }
            }
        });
    }

    public interface ButtonListener {
        void clickLogout();
        void clickYes();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_expire_session, null);
        TextView tvYes = view.findViewById(R.id.tv_yes);
        TextView tvLogout = view.findViewById(R.id.tv_log_out);
        tvTitle = view.findViewById(R.id.tv_title);
        tvMessage = view.findViewById(R.id.tv_message);

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(buttonListener != null){
                    buttonListener.clickLogout();
                }
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(buttonListener != null){
                    buttonListener.clickYes();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        setCanceledOnTouchOutside(false);
    }
}
