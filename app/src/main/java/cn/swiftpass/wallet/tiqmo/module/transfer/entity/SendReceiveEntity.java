package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SendReceiveEntity extends BaseEntity {

    private String groupId;
    //是否群组
    private String isGroup;
    private String firstName;
    //0:收入  1:支出 群组为空
    private String recentTraType;
    private List<String> headIcon;
    public int headResId;


    public String getGroupId() {
        return this.groupId;
    }

    public void setGroupId(final String groupId) {
        this.groupId = groupId;
    }

    public String getIsGroup() {
        return this.isGroup;
    }

    public void setIsGroup(final String isGroup) {
        this.isGroup = isGroup;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getRecentTraType() {
        return this.recentTraType;
    }

    public void setRecentTraType(final String recentTraType) {
        this.recentTraType = recentTraType;
    }

    public List<String> getHeadIcon() {
        return this.headIcon;
    }

    public void setHeadIcon(final List<String> headIcon) {
        this.headIcon = headIcon;
    }
}
