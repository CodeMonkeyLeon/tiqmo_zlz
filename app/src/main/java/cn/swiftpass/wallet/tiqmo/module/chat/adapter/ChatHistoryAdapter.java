package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.gc.gcchat.GcChatMessage;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatMessage;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.DateUtils;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.MessageUtils;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class ChatHistoryAdapter extends BaseRecyclerAdapter<GcChatSession> {

    public ChatHistoryAdapter(@Nullable List<GcChatSession> data) {
        super(R.layout.item_chat_history, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, GcChatSession gcChatSession, int position) {
        if (position == mDataList.size() - 1) {
            holder.setGone(R.id.tv_bottom, false);
        } else {
            holder.setGone(R.id.tv_bottom, true);
        }
        holder.setText(R.id.tv_name, MessageUtils.getChatSessionName(gcChatSession));
        holder.setTextColor(R.id.tv_name, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)));
        holder.setTextColor(R.id.tv_message, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_9c9da1)));
        holder.setTextColor(R.id.tv_time, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_503a3b44)));
        holder.setBackgroundColor(R.id.bg_content,mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_white)));
        GcChatMessage lastChatMessage = gcChatSession.getLastMessage();
        ChatMessage lastSampleAppChatMessage = null;
        boolean hasAttachments = false;
        if (lastChatMessage != null) {
            long time = lastChatMessage.getTimestamp();
            holder.setText(R.id.tv_time, DateUtils.getChatListTimeToDisplay(time));
            lastSampleAppChatMessage = new Gson().fromJson(new Gson().toJson(lastChatMessage.getMessage()), ChatMessage.class);
            if (lastSampleAppChatMessage != null) {
                if (lastSampleAppChatMessage.getAttachments().size() > 0) {
                    hasAttachments = true;
                }
            }
        } else {
            holder.setText(R.id.tv_time,"");
        }

        boolean isInvitationPendingAtMe = false;
//        for (GcChatParticipant chatParticipant : gcChatSession.getParticipantUsers()) {
//            boolean isMeParticipant = UserUtils.loggedInUserId.equals(chatParticipant.getUser().getExternalId());
//            if (isMeParticipant && chatParticipant.isInvitationPending()) {
//                isInvitationPendingAtMe = true;
//            }
//        }
        holder.setBackgroundRes(R.id.iv_chat_file,ThemeSourceUtils.getSourceID(mContext,R.attr.icon_chat_file));
        holder.setGone(R.id.iv_chat_file,true);
        if (isInvitationPendingAtMe) {
            holder.setText(R.id.tv_message,"Invitation Pending. Please respond to invitation!!");
        } else if (hasAttachments) {
            holder.setGone(R.id.iv_chat_file,false);
            holder.setText(R.id.tv_message,mContext.getString(R.string.sprint20_73));
        } else {
            if (lastSampleAppChatMessage != null) {
                holder.setText(R.id.tv_message,lastSampleAppChatMessage.getMessage());
            } else {
                holder.setText(R.id.tv_message,"");
            }
        }

        RoundedImageView ivUser = holder.getView(R.id.iv_user);

        String displayPic = MessageUtils.getChatSessionDisplayPic(gcChatSession);
        if(gcChatSession.isGroupChat()){
            ivUser.setImageResource(R.drawable.l_group_avatar);
        }else {
            if (displayPic != null && !displayPic.isEmpty()) {
                Glide.with(mContext).load(ChatHelper.getInstance().getChatUserAvatar(displayPic)).centerCrop().autoClone().into(ivUser);
            } else {
                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivUser);
            }
        }
    }

    public void changeTheme(){
        notifyDataSetChanged();
    }
}
