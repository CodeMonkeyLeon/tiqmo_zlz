package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.ChooseProductEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInputProductEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeContract;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargePayPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class RechargeOrderDetailActivity extends BaseCompatActivity<RechargeContract.RechargePayPresenter> implements RechargeContract.RechargePayView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_change_phone)
    TextView tvChangePhone;
    @BindView(R.id.tv_short_name)
    TextView tvShortName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.iv_operator)
    RoundedImageView ivOperator;
    @BindView(R.id.tv_operator_name)
    TextView tvOperatorName;
    @BindView(R.id.iv_operator_two)
    RoundedImageView ivOperatorTwo;
    @BindView(R.id.tv_product_name)
    TextView tvProductName;
    @BindView(R.id.tv_change_plan)
    TextView tvChangePlan;
    @BindView(R.id.tv_first_money)
    TextView tvFirstMoney;
    @BindView(R.id.tv_second_money)
    TextView tvSecondMoney;
    @BindView(R.id.con_exchange_money)
    ConstraintLayout conExchangeMoney;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_pay)
    TextView tvPay;

    private RechargeOrderInfoEntity mRechargeOrderInfoEntity;
    private RechargeOperatorEntity rechargeOperatorEntity;
    private RechargeOrderDetailEntity rechargeOrderDetailEntity;

    private TransferLimitEntity transferLimitEntity;
    private String totalMoney;
//    private double valiableMoney;
    private double monthLimitMoney;

    private RechargeInputProductEntity rechargeInputProductEntity;
    private ChooseProductEntity chooseProductEntity;

    private String productId,operatorId,countryCode,mobileNumber,amount,shortName,operatorName,operatorImgUrl,productName;
    private String destinationAmount,destinationCurrencyCode;
    private String payAmount;
    private String targetAmount;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_order_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_38);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if (getIntent() != null && getIntent().getExtras() != null) {
            rechargeInputProductEntity = (RechargeInputProductEntity) getIntent().getExtras().getSerializable(Constants.rechargeInputProductEntity);
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            chooseProductEntity = (ChooseProductEntity) getIntent().getExtras().getSerializable(Constants.rechargeChooseProductEntity);
            rechargeOperatorEntity = (RechargeOperatorEntity) getIntent().getExtras().getSerializable(Constants.rechargeOperatorEntity);
            rechargeOrderDetailEntity = (RechargeOrderDetailEntity) getIntent().getExtras().getSerializable(Constants.rechargeOrderDetailEntity);
            if (transferLimitEntity != null) {
                String monthLimitAmt = transferLimitEntity.monthLimitAmt;
                try {
                    monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
            if(rechargeOperatorEntity != null){
                mobileNumber = rechargeOperatorEntity.mobileNumber;
                countryCode = rechargeOperatorEntity.countryIsoCode;
                shortName = rechargeOperatorEntity.shortName;
                operatorImgUrl = rechargeOperatorEntity.operatorImgUrl;
            }
            if(rechargeOrderDetailEntity != null){
                tvChangePhone.setVisibility(View.GONE);
                mobileNumber = rechargeOrderDetailEntity.phone;
                countryCode = rechargeOrderDetailEntity.countryCode;
                shortName = rechargeOrderDetailEntity.userShortName;
                operatorImgUrl = rechargeOrderDetailEntity.operatorImgUrl;
            }
            if(chooseProductEntity != null){
                productId = chooseProductEntity.productId;
                operatorId = chooseProductEntity.operatorId;
                operatorName = chooseProductEntity.operatorName;
                productName = chooseProductEntity.productName;
                destinationAmount = chooseProductEntity.destinationAmount;
                destinationCurrencyCode = chooseProductEntity.destinationCurrencyCode;
                amount = chooseProductEntity.amount;
                totalMoney = chooseProductEntity.amount;
            }else if(rechargeInputProductEntity != null){
                productId = rechargeInputProductEntity.productId;
                operatorId = rechargeInputProductEntity.operatorId;
                operatorName = rechargeInputProductEntity.operatorName;
                productName = rechargeInputProductEntity.title;
                destinationAmount = rechargeInputProductEntity.destinationAmount;
                destinationCurrencyCode = rechargeInputProductEntity.destinationCurrencyCode;
                amount = rechargeInputProductEntity.amount;
                totalMoney = rechargeInputProductEntity.amount;
                operatorImgUrl = rechargeInputProductEntity.operatorImgUrl;
            }

            Glide.with(mContext)
                    .load(operatorImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.mobile_topup))
                    .into(ivOperator);

            Glide.with(mContext)
                    .load(operatorImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.mobile_topup))
                    .into(ivOperatorTwo);

            tvShortName.setText(shortName);
            tvProductName.setText(productName);
            tvPhone.setText(mobileNumber);
            tvOperatorName.setText(operatorName);
            tvFirstMoney.setText(Spans.builder()
                    .text(destinationAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(destinationCurrencyCode).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(10)
                    .build());
            tvSecondMoney.setText(Spans.builder()
                    .text(amount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(10)
                    .build());

            if (totalMoney.length() > 8) {
                tvTotalAmount.setText(Spans.builder()
                        .text(totalMoney + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(16)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(12)
                        .build());
            } else {
                tvTotalAmount.setText(Spans.builder()
                        .text(totalMoney + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(12)
                        .build());
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_get_help, R.id.tv_change_phone, R.id.tv_change_plan, R.id.tv_pay})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_help:
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_change_phone:
                ProjectApp.removeRechargeTask();
                finish();
                break;
            case R.id.tv_change_plan:
                finish();
                break;
            case R.id.tv_pay:
                payAmount = AndroidUtils.getReqTransferMoney(amount);
                targetAmount = AndroidUtils.getReqTransferMoney(destinationAmount);
                AppsFlyerEntity appsFlyerEntity = new AppsFlyerEntity();
                appsFlyerEntity.desitinationCode = countryCode;
                UserInfoManager.getInstance().setAppsFlyerEntity(appsFlyerEntity);
                mPresenter.preRechargeOrder(productId,operatorId,countryCode,mobileNumber,payAmount,targetAmount,shortName,operatorName);
                break;
            default:break;
        }
    }

    @Override
    public void preRechargeOrderSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        this.mRechargeOrderInfoEntity = rechargeOrderInfoEntity;
        if (rechargeOrderInfoEntity != null) {
            String orderInfo = rechargeOrderInfoEntity.orderInfo;
            if (!TextUtils.isEmpty(orderInfo)) {
                mPresenter.checkOut("", orderInfo);
            }
        }
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
//        checkOutEntity.voucherDarkIconUrl = billerImgUrl;
//        checkOutEntity.voucherLightIconUrl = billerImgUrl;
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.rechargeOrderInfoEntity, mRechargeOrderInfoEntity);
        mHashMap.put(Constants.productName, productName);
        ActivitySkipUtil.startAnotherActivity(this,
                RechargeSummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkOutFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if("030214".equals(errorCode) || "030215".equals(errorCode)){
            showCommonErrorDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public RechargeContract.RechargePayPresenter getPresenter() {
        return new RechargePayPresenter();
    }
}
