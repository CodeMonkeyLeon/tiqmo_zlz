package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ContactInviteEntity extends BaseEntity {

    private List<KycContactEntity> contactInviteDtos;

    public List<KycContactEntity> getContactInviteDtos() {
        return this.contactInviteDtos;
    }

    public void setContactInviteDtos(final List<KycContactEntity> contactInviteDtos) {
        this.contactInviteDtos = contactInviteDtos;
    }
}
