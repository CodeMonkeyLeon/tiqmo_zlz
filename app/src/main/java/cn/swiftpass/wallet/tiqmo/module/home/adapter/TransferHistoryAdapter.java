package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class TransferHistoryAdapter extends BaseRecyclerAdapter<TransferHistoryEntity> {

    private Activity mActivity;

    public TransferHistoryAdapter(@Nullable List<TransferHistoryEntity> data, Activity activity) {
        super(R.layout.item_pay_detail, data);
        mActivity = activity;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, TransferHistoryEntity transferHistoryEntity, int position) {
        String date = getSectionForPosition(position);
        if (TextUtils.isEmpty(date)) {
            return;
        }

        if (position == mDataList.size() - 1) {
            holder.setGone(R.id.view, true);
        } else {
            holder.setGone(R.id.view, true);
        }

        RoundedImageView ivPayMethod = holder.getView(R.id.iv_pay_method);
        int sceneType = transferHistoryEntity.sceneType;
        int subSceneType = transferHistoryEntity.subSceneType;
        String orderType = transferHistoryEntity.orderType;
        if ("CB".equals(orderType)) {
            holder.setGone(R.id.iv_bg_cashback, false);
        } else {
            holder.setGone(R.id.iv_bg_cashback, true);
        }
        int drawableId = AndroidUtils.getHistoryDrawable(mContext, sceneType);
        if(sceneType == 24){
            drawableId = AndroidUtils.getLoopHistoryDrawable(mContext,subSceneType);
        }
        String orderStatus = transferHistoryEntity.orderStatus;
        String orderDirection = transferHistoryEntity.orderDirection;
        String direction;
        if (!TextUtils.isEmpty(orderDirection)) {
            if ("I".equals(orderDirection)) {
                direction = "+" + " ";
            } else {
                direction = "-" + " ";
            }
        } else {
            direction = "";
        }

        String voucherBrandLightLogo = transferHistoryEntity.voucherBrandLightLogo;
        String logo = transferHistoryEntity.logo;
        if (sceneType == 15) {
            if (!ThemeUtils.isCurrentDark(mContext)) {
                logo = voucherBrandLightLogo;
            }
        }

        if (drawableId == -1) {
            ivPayMethod.setOval(true);
            if (sceneType == 15) {
                ivPayMethod.setOval(false);
            }
            Glide.with(mContext)
                    .load(logo)
                    .dontAnimate()
                    .placeholder((sceneType != 15 && sceneType != 5) ? ThemeSourceUtils.getDefAvatar(mContext, transferHistoryEntity.sex) : -1)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivPayMethod);
        } else {
            ivPayMethod.setOval(false);
            ivPayMethod.setImageResource(drawableId);
        }

        if ("S".equals(orderStatus)) {
            holder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_check));
        } else if ("W".equals(orderStatus)) {
            holder.setImageResource(R.id.iv_check, R.drawable.under_processing);
        } else if ("E".equals(orderStatus)) {
            holder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_failed));
        } else if ("C".equals(orderStatus)) {
            holder.setImageResource(R.id.iv_check, R.drawable.under_processing);
        } else if ("V".equals(orderStatus)) {
            holder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_failed));
        } else {
            holder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_check));
        }

        holder.setText(R.id.tv_money, Spans.builder()
                .text(direction + AndroidUtils.getTransferMoney(transferHistoryEntity.tradeAmount))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_money_color)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                .text(" " + LocaleUtils.getCurrencyCode(transferHistoryEntity.currencyType))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_money_color))).size(10)
                .build());
        holder.setText(R.id.tv_time, transferHistoryEntity.createTime);
        holder.setTextColor(R.id.tv_time, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_time_color)));
        holder.setText(R.id.tv_pay_title, transferHistoryEntity.orderDesc);
        holder.setTextColor(R.id.tv_pay_title, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_main_title)));
        holder.setBackgroundRes(R.id.ll_content, ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_bg));

        holder.setGone(R.id.ll_date, false);
        if (position == getPositionForSection(date)) {
            holder.setGone(R.id.ll_date, false);
            holder.setText(R.id.tv_date, transferHistoryEntity.createDateDesc);
            holder.setTextColor(R.id.tv_date, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_list_title_text_color)));
            holder.setBackgroundRes(R.id.tv_history_line, ThemeSourceUtils.getSourceID(mContext, R.attr.color_30gray_979797));
        } else {
            holder.setGone(R.id.ll_date, true);
        }
    }

    public void changeTheme(){
        notifyDataSetChanged();
    }

    /**
     * 根据当前位置获取时间值
     */
    public String getSectionForPosition(int position) {
        TransferHistoryEntity transferHistoryEntity = getItem(position);
        if (transferHistoryEntity != null) {
            return transferHistoryEntity.createDateDesc;
        }
        return "";
    }

    public int getPositionForSection(String date) {
        for (int i = 0; i < getItemCount(); i++) {
            String dateStr = mDataList.get(i).createDateDesc;
            if (!TextUtils.isEmpty(dateStr) && dateStr.equals(date)) {
                return i;
            }
        }
        return -1;
    }
}
