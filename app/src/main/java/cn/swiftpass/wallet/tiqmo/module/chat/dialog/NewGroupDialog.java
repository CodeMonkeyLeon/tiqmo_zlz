package cn.swiftpass.wallet.tiqmo.module.chat.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class NewGroupDialog extends BottomDialog {

    private Context mContext;

    private NewGroupListener newGroupListener;

    private String groupName;

    public void setNewGroupListener(NewGroupListener newGroupListener){
        this.newGroupListener = newGroupListener;
    }


    public NewGroupDialog(Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_chat_new_group, null);
        EditTextWithDel etGroupName = view.findViewById(R.id.et_group_name);
        TextView tvAddPeople = view.findViewById(R.id.tv_add_people);
        etGroupName.getEditText().setHint(mContext.getString(R.string.sprint20_12));
        etGroupName.getEditText().setHintTextColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_70white_9c9da1)));
        etGroupName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString().trim())) {
                    tvAddPeople.setEnabled(false);
                }else {
                    if(s.toString().length()<5){
                        tvAddPeople.setEnabled(false);
                    }else {
                        groupName = s.toString();
                        tvAddPeople.setEnabled(true);
                    }
                }
            }
        });

        tvAddPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newGroupListener.createNewGroupChat(groupName);
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    public interface NewGroupListener{
        void createNewGroupChat(String groupName);
    }

}
