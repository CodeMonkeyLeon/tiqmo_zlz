package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;

public class ImrRemoveBeneficiaryDialog extends BaseDialogFragmentN {

    private static ImrRemoveBeneficiaryDialog dialog;

    private ImrBeneficiaryListEntity.ImrBeneficiaryBean data;
    private OnDialogClickListener listener;

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        this.listener = listener;
    }

    private ImrRemoveBeneficiaryDialog() {
    }

    public static ImrRemoveBeneficiaryDialog getInstance(ImrBeneficiaryListEntity.ImrBeneficiaryBean data) {
        if (dialog == null) {
            dialog = new ImrRemoveBeneficiaryDialog();
        }
        dialog.data = data;
        return dialog;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_imr_remove_beneficiary;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        TextView name = view.findViewById(R.id.tv_remove_beneficiary_name);
        ImageView flag = view.findViewById(R.id.iv_remove_beneficiary_flag);
        TextView country = view.findViewById(R.id.tv_remove_beneficiary_country);
        ImageView genderAvatar = view.findViewById(R.id.iv_remove_gender_avatar);
        TextView removeBeneficiary = view.findViewById(R.id.tv_imr_remove_beneficiary);
//        TextView currency = view.findViewById(R.id.tv_remove_beneficiary_currency);

        if (!TextUtils.isEmpty(data.transferDestinationCountryName)) {
            country.setText(data.transferDestinationCountryName);
        }
//        if (!TextUtils.isEmpty(data.countrySupportedCurrencyName)) {
//            currency.setText(data.countrySupportedCurrencyName);
//        }
        if (!TextUtils.isEmpty(data.payeeFullName)) {
            name.setText(data.payeeFullName);
        }
        genderAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, data.sex));
        if (!TextUtils.isEmpty(data.countryLogoUrl)) {
            Glide.with(mContext).clear(flag);
            Glide.with(mContext)
                    .load(data.countryLogoUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(flag);
        }

        removeBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener !=  null) {
                    listener.onRemoveClick();
                }
                dismiss();
            }
        });
    }

    public interface OnDialogClickListener {
        void onRemoveClick();
    }
}
