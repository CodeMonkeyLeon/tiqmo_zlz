package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SpendingCategoryListEntity extends BaseEntity {
    //类别Id
    public String spendingCategoryId;
    //支出类别名称
    public String spendingCategoryName;
    //支出类别金额
    public String spendingCategoryAmount;
    //支出类别当月预算金额
    public String spendingCategoryBudgetAmount;
    //	类别支出比例(比例已乘以100) ，没有百分号
    public int spendingCategoryRatio;
    //支出类别币种
    public String spendingCategoryCurrency;
    //支出类别总笔数
    public String spendingCategoryTxnCount;
    //是否显示详情  0不显示  1显示
    public int showDetail = 0;
    //交易列表的前4笔交易列表数据
    public List<SpendingCategoryDetailEntity> spendingCategoryTxnDetail = new ArrayList<>();

}
