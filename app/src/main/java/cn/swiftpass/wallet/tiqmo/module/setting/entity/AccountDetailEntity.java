package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class AccountDetailEntity extends BaseEntity {

    public String date;

    public AccountDetailEntity(String date) {
        this.date = date;
    }
}
