package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferToContactPresenter;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class TransferSummaryActivity extends BaseCompatActivity<TransferContract.TransferToContactPresenter> implements TransferContract.TransferToContactView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_transfer)
    ImageView ivTransfer;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_bank_name_title)
    TextView tvBankNameTitle;
    @BindView(R.id.tv_bank_number)
    TextView tvBankNumber;
    @BindView(R.id.tv_purpose)
    TextView tvPurpose;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_wrong_fees)
    TextView tvWrongFees;
    @BindView(R.id.tv_fees)
    TextView tvTransFee;
    @BindView(R.id.tv_wrong_vat_money)
    TextView tvWrongVatMoney;
    @BindView(R.id.tv_vat_money)
    TextView tvVat;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.ll_transfer_purpose)
    ConstraintLayout llTransferPurpose;
    @BindView(R.id.ll_transfer_vat)
    ConstraintLayout llTransferVat;
    @BindView(R.id.ll_transfer_fee)
    ConstraintLayout llTransferFee;

    private BeneficiaryEntity beneficiaryEntity;
    private String orderAmount, transferPurpose, transferFees, vatAmount, transTimeType, transferPurposeDesc, transTimeDescOne, transTimeDescTwo,
            totalAmount, wrongTransferFee, orderCurrencyCode, wrongVat, payerName;

    private TransferEntity mTransferEntity;
    private static TransferSummaryActivity transferSummaryActivity;

    private String orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode;

    private boolean isFromHome;
    private  RiskControlEntity riskControlEntity;
    private BottomDialog bottomDialog;

    public static TransferSummaryActivity getTransferSummaryActivity() {
        return TransferSummaryActivity.transferSummaryActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        transferSummaryActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_summary;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        transferSummaryActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        tvTitle.setText(R.string.wtba_39);
        if (getIntent() != null && getIntent().getExtras() != null) {
            isFromHome = getIntent().getExtras().getBoolean(Constants.isFromHome);
            beneficiaryEntity = (BeneficiaryEntity) getIntent().getExtras().getSerializable("beneficiaryEntity");
            if (beneficiaryEntity != null) {
                transTimeType = beneficiaryEntity.transTimeType;
                orderAmount = beneficiaryEntity.orderAmount;
                transferPurpose = beneficiaryEntity.transferPurpose;
                transferPurposeDesc = beneficiaryEntity.transferPurposeDesc;
                transTimeDescOne = beneficiaryEntity.transTimeDescOne;
                transTimeDescTwo = beneficiaryEntity.transTimeDescTwo;
                tvPurpose.setText(transferPurposeDesc);
                if(TextUtils.isEmpty(transferPurposeDesc)){
                    llTransferPurpose.setVisibility(View.GONE);
                }
                tvTime.setText(Spans.builder()
                        .text(transTimeDescOne).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(" (" + transTimeDescTwo + ")").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                        .build());
                tvBankName.setText(beneficiaryEntity.bankName);
                tvBankNameTitle.setText(R.string.wtba_15);
                String ibanNum = beneficiaryEntity.beneficiaryIbanNo;
                if (!TextUtils.isEmpty(ibanNum)) {
                    tvBankNumber.setText(AndroidUtils.changeBanNum(ibanNum));
                }
                tvName.setText(beneficiaryEntity.beneficiaryName);
                if (!TextUtils.isEmpty(beneficiaryEntity.bankLogo)) {
                    ivTransfer.setVisibility(View.VISIBLE);
                }
                Glide.with(mContext)
                        .load(beneficiaryEntity.bankLogo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivTransfer);
            }
        }
        tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        mPresenter.getTransferFee(Constants.TYPE_TRANSFER, AndroidUtils.getReqTransferMoney(orderAmount), UserInfoManager.getInstance().getCurrencyCode(), transTimeType);
    }


    @OnClick({R.id.iv_back, R.id.tv_cancel, R.id.tv_confirm})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_cancel:
                ProjectApp.removeTransferTask();
                finish();
                break;
            case R.id.tv_confirm:
                mPresenter.transferToContact(userInfoEntity.userId, SpUtils.getInstance().getCallingCode(), userInfoEntity.getPhone(), "",
                        beneficiaryEntity.beneficiaryIbanNo, orderAmount, UserInfoManager.getInstance().getCurrencyCode(),
                        beneficiaryEntity.remark, isFromHome?Constants.TYPE_TRANSFER_WT:Constants.TYPE_TRANSFER_BT, transTimeType, "1", transferPurpose, AndroidUtils.getReqTransferMoney(transferFees), AndroidUtils.getReqTransferMoney(vatAmount), payerName);
                break;
            default:
                break;
        }
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        if (transferEntity == null) return;
        this.mTransferEntity = transferEntity;
        riskControlEntity = transferEntity.riskControlInfo;
        if (riskControlEntity != null) {
            if (riskControlEntity.isOtp()) {
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                setTransferMap(mHashMaps);
                ActivitySkipUtil.startAnotherActivity(TransferSummaryActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                jumpToPwd(false);
            }else if(riskControlEntity.isNeedIvr()){
                jumpToPwd(true);
            }else{
                requestTransfer();
            }
        } else {
            requestTransfer();
//            jumpToPwd();
        }
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {

    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {

    }

    private void requestTransfer(){
        if (mTransferEntity != null) {
            orderNo = mTransferEntity.orderNo;
            payerName = mTransferEntity.payerName;
            payMethod = mTransferEntity.payMethod;
            transAmount = mTransferEntity.orderAmount;
            exchangeRate = mTransferEntity.exchangeRate;
            transCurrencyCode = mTransferEntity.orderCurrencyCode;
            transferFees = mTransferEntity.transFees;
            vatAmount = mTransferEntity.vat;
        }
        mPresenter.transferSurePay(Constants.TYPE_TRANSFER_BT, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transferFees, vatAmount);
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.TRANSFER_ENTITY, mTransferEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_TRANSFER_BT);
        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_TRANSFER_IBANK);
        if(isFromHome) {
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtba_56));
        }else{
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_106));
        }
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void transferToContactFail(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        }else {
            showTipDialog(errMsg);
        }
    }

    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,Constants.TYPE_TRANSFER_BT,transferEntity,true);
    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {
        if (transFeeEntity != null) {
            transferFees = transFeeEntity.getDiscountTransFees();
            totalAmount = transFeeEntity.getTotalAmount();
            orderAmount = transFeeEntity.getOrderAmount();
            vatAmount = transFeeEntity.getDiscountVat();
            wrongVat = transFeeEntity.getVat();
            wrongTransferFee = transFeeEntity.getTransFees();
            orderCurrencyCode = transFeeEntity.orderCurrencyCode;
            if (!TextUtils.isEmpty(orderAmount)) {
                tvAmount.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(orderAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                        .build());
            }

            if (!TextUtils.isEmpty(totalAmount)) {
                tvTotalAmount.setText(Spans.builder()
                        .text(totalAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                        .build());
            }

            if (!TextUtils.isEmpty(wrongVat)) {
                ////如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                llTransferVat.setVisibility(View.VISIBLE);
                tvWrongVatMoney.setText(Spans.builder()
                        .text(wrongVat + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());

                if (!TextUtils.isEmpty(vatAmount)) {
                    tvVat.setText(Spans.builder()
                            .text(vatAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
            }else {
                //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                if (!TextUtils.isEmpty(vatAmount) && BigDecimalFormatUtils.compareBig(vatAmount, "0")) {
                    tvWrongVatMoney.setVisibility(View.GONE);
                    tvVat.setText(Spans.builder()
                            .text(vatAmount+ " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                } else {
                    llTransferVat.setVisibility(View.GONE);
                }
            }

            if (!TextUtils.isEmpty(wrongTransferFee)) {
                //如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                llTransferFee.setVisibility(View.VISIBLE);
                tvWrongFees.setText(Spans.builder()
                        .text(wrongTransferFee + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());

                if(!TextUtils.isEmpty(transferFees)) {
                    tvTransFee.setText(Spans.builder()
                            .text(transferFees + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
            }else{
                //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                if (!TextUtils.isEmpty(transferFees) && BigDecimalFormatUtils.compareBig(transferFees,"0")) {
                    tvWrongFees.setVisibility(View.GONE);
                    tvTransFee.setText(Spans.builder()
                            .text(transferFees + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }else{
                    llTransferFee.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {

    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity riskControlEntity) {

    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {

    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {

    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {

    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity result) {

    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {

    }

    @Override
    public TransferContract.TransferToContactPresenter getPresenter() {
        return new TransferToContactPresenter();
    }
}
