package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.graphics.Typeface;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PayBillRefundListAdapter extends BaseRecyclerAdapter<PayBillRefundEntity> {
    public PayBillRefundListAdapter(@Nullable List<PayBillRefundEntity> data) {
        super(R.layout.item_paybill_refund_list,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillRefundEntity payBillRefundEntity, int position) {
        RoundedImageView ivRefundBill = baseViewHolder.getView(R.id.iv_refund_bill);
        String orderStatus = payBillRefundEntity.orderStatus;
        String orderDirection = payBillRefundEntity.orderDirection;
        String direction;
        if (!TextUtils.isEmpty(orderDirection)) {
            if ("I".equals(orderDirection)) {
                direction = "+" + " ";
            } else {
                direction = "-" + " ";
            }
        } else {
            direction = "";
        }

        String logo = payBillRefundEntity.logo;
        Glide.with(mContext)
                .load(logo)
                .dontAnimate()
                .format(DecodeFormat.PREFER_RGB_565)
                .into(ivRefundBill);

        if ("S".equals(orderStatus)) {
            baseViewHolder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_check));
        } else if ("W".equals(orderStatus)) {
            baseViewHolder.setImageResource(R.id.iv_check, R.drawable.under_processing);
        } else if ("E".equals(orderStatus)) {
            baseViewHolder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_failed));
        } else if ("C".equals(orderStatus)) {
            baseViewHolder.setImageResource(R.id.iv_check, R.drawable.under_processing);
        } else if ("V".equals(orderStatus)) {
            baseViewHolder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_failed));
        } else {
            baseViewHolder.setImageResource(R.id.iv_check, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_check));
        }

        if(!TextUtils.isEmpty(payBillRefundEntity.tradeAmount)) {
            baseViewHolder.setGone(R.id.tv_money,false);
            baseViewHolder.setText(R.id.tv_money, Spans.builder()
                    .text(direction + AndroidUtils.getTransferMoney(payBillRefundEntity.tradeAmount))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_money_color)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(" " + LocaleUtils.getCurrencyCode(payBillRefundEntity.currencyType))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_money_color))).size(10)
                    .build());
        }else{
            baseViewHolder.setGone(R.id.tv_money,true);
        }
        baseViewHolder.setText(R.id.tv_refund_id, payBillRefundEntity.desc);
        baseViewHolder.setTextColor(R.id.tv_refund_id, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_time_color)));
        baseViewHolder.setText(R.id.tv_bill_name, payBillRefundEntity.name);
        baseViewHolder.setTextColor(R.id.tv_bill_name, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_main_title)));
        baseViewHolder.setBackgroundRes(R.id.ll_refund_bill, ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_bg));
    }
}
