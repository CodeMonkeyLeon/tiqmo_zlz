package cn.swiftpass.wallet.tiqmo.support.utils;

import android.graphics.Bitmap;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import java.util.Hashtable;


/**
 * @author ramon
 * Created by on 2018/8/7.
 */
public class QrcodePictureUtil {

    private static final String TAG = "QrcodePictureUtil";
    private static final String LOCAL_BROWSER_PACKAGE = "com.android.browser";
    private static final String LOCAL_BROWSER = "com.android.browser.BrowserActivity";

    private static final double ZOOM_SCALE = 1.75;//图片压缩比例


    public static Result parseQRcodeBitmap(Bitmap bitmap) {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, BarcodeFormat.QR_CODE);
        //新建一个RGBLuminanceSource对象，将bitmap图片传给此对象
        RGBLuminanceSource rgbLuminanceSource = new RGBLuminanceSource(bitmap);
        //将图片转换成二进制图片
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(rgbLuminanceSource));
        //初始化解析对象
        QRCodeReader reader = new QRCodeReader();
        //开始解析
        Result result = null;
        try {
            result = reader.decode(binaryBitmap, hints);
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        if (result == null) {
            try {
                LogUtils.i(TAG, "old methord 压缩一次:" + result);
                //压缩成ZOOM_SCALE比例再识别一次
                Bitmap zoomBitmap = ImageUtil.zoomImage(bitmap, bitmap.getWidth() / ZOOM_SCALE, bitmap.getHeight() / ZOOM_SCALE);
                RGBLuminanceSource zoomRgbLuminanceSource = new RGBLuminanceSource(zoomBitmap);
                BinaryBitmap zoomBinaryBitmap = new BinaryBitmap(new HybridBinarizer(zoomRgbLuminanceSource));
                result = reader.decode(zoomBinaryBitmap, hints);
                zoomBitmap.recycle();
            } catch (Exception e) {
                LogUtils.i(TAG, "old methord" + e.getLocalizedMessage());
            }
        } else {
            LogUtils.i(TAG, "old methord 一次:" + result);
        }
        LogUtils.i(TAG, "old methord:" + result);
        return result;
    }


    static class RGBLuminanceSource extends LuminanceSource {

        private byte bitmapPixels[];

        protected RGBLuminanceSource(Bitmap bitmap) {
            super(bitmap.getWidth(), bitmap.getHeight());

            // 首先，要取得该图片的像素数组内容
            int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
            this.bitmapPixels = new byte[bitmap.getWidth() * bitmap.getHeight()];
            bitmap.getPixels(data, 0, getWidth(), 0, 0, getWidth(), getHeight());

            // 将int数组转换为byte数组，也就是取像素值中蓝色值部分作为辨析内容
            for (int i = 0; i < data.length; i++) {
                this.bitmapPixels[i] = (byte) data[i];
            }
        }

        @Override
        public byte[] getMatrix() {
            // 返回我们生成好的像素数据
            return bitmapPixels;
        }

        @Override
        public byte[] getRow(int y, byte[] row) {
            // 这里要得到指定行的像素数据
            System.arraycopy(bitmapPixels, y * getWidth(), row, 0, getWidth());
            return row;
        }
    }

//    public static Result parseQRcodeBitmap(Bitmap bitmap) {
//
//        Result result = parseQRcodeOldBitmap(bitmap);
////        LogUtils.i(TAG, "old methord qrCodeaOld:" + result);
//
////        byte[] bmpYUVBytes = Bmp2YUV.getBitmapYUVBytes(bitmap);
////        // 2.塞给zxing进行decode
////        String qrCode = decodeYUVByZxing(bmpYUVBytes, bitmap.getWidth(), bitmap.getHeight());
////        LogUtils.i(TAG, "new methord qrCode:" + qrCode);
//
//        return null;
//    }

    private static String decodeYUVByZxing(byte[] bmpYUVBytes, int bmpWidth, int bmpHeight) {
        String zxingResult = "";
        // Both dimensions must be greater than 0
        if (null != bmpYUVBytes && bmpWidth > 0 && bmpHeight > 0) {
            try {
                PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(bmpYUVBytes, bmpWidth,
                        bmpHeight, 0, 0, bmpWidth, bmpHeight, true);
                BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));
                Reader reader = new QRCodeReader();
                Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
                hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
                hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
                hints.put(DecodeHintType.POSSIBLE_FORMATS, BarcodeFormat.QR_CODE);
                hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
                Result result = reader.decode(binaryBitmap, hints);
                if (null != result) {
                    zxingResult = result.getText();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return zxingResult;
    }


}
