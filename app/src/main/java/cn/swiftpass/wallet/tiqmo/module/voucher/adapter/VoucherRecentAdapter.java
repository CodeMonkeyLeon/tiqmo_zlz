package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.RecentVoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class VoucherRecentAdapter extends BaseRecyclerAdapter<RecentVoucherBrandEntity> {

    private int width, height;

    public VoucherRecentAdapter(Context mContext, @Nullable List<RecentVoucherBrandEntity> data) {
        super(R.layout.item_voucher_amount, data);
        width = (AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(mContext, 60)) / 2;
        height = AndroidUtils.dip2px(mContext, 140);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, RecentVoucherBrandEntity item, int position) {
        if (item == null) {
            return;
        }
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(width, height);
        ImageView ivVoucherAmount = baseViewHolder.getView(R.id.iv_voucher_amount);
        ivVoucherAmount.setLayoutParams(layoutParams);
        String logo = item.voucherTypeIconUrl;
        if (!TextUtils.isEmpty(logo)) {
            Glide.with(mContext).clear(ivVoucherAmount);
            Glide.with(mContext)
                    .load(logo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivVoucherAmount);
        }
    }
}
