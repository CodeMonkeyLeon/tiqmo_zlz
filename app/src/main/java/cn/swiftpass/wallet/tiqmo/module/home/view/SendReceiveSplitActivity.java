package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.UnScrollViewPager;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMainPagerAdapter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class SendReceiveSplitActivity extends BaseCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.indicator_split_main)
    MagicIndicator indicatorSplitMain;
    @BindView(R.id.vp_split_main)
    UnScrollViewPager vpSplitMain;

    private SendSplitFragment sendSplitFragment;
    private ReceiveSplitFragment receiveSplitFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_send_receive_split;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        headCircle.setVisibility(View.GONE);
        LocaleUtils.viewRotationY(mContext, ivBack, headCircle);
        tvTitle.setText(R.string.request_transfer_10);

        List<String> title = new ArrayList<>(Arrays.asList(this.getString(R.string.sprint17_12),
                this.getString(R.string.sprint17_13)));
        List<Fragment> fragments = new ArrayList<>();
        sendSplitFragment = SendSplitFragment.getInstance();
        receiveSplitFragment = ReceiveSplitFragment.getInstance();
        fragments.add(sendSplitFragment);
        fragments.add(receiveSplitFragment);
        if (LocaleUtils.isRTL(mContext)) {
            Collections.reverse(title);
            Collections.reverse(fragments);
        }

        ImrMainPagerAdapter adapter = new ImrMainPagerAdapter(getSupportFragmentManager(), fragments);
        vpSplitMain.setScrollEnable(false);// 禁止滑动
        vpSplitMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        vpSplitMain.setAdapter(adapter);
        if (LocaleUtils.isRTL(mContext)) {
            vpSplitMain.setCurrentItem(title.size() - 1);
        }
        indicatorSplitMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return title == null ? 0 : title.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(title.get(index));
                clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                clipPagerTitleView.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                clipPagerTitleView.setClipColor(Color.WHITE);
                clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));

                clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ButtonUtils.isFastDoubleClick()) {
                            return;
                        }
                        vpSplitMain.setCurrentItem(index);
                    }
                });
                return clipPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setLineHeight(UIUtil.dip2px(context, 44));
                indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });
        indicatorSplitMain.setNavigator(commonNavigator);
        commonNavigator.onPageSelected(LocaleUtils.isRTL(mContext) ? title.size() - 1 : 0);
        ViewPagerHelper.bind(indicatorSplitMain, vpSplitMain);
        try {
            //默认第一个
            vpSplitMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);

        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
