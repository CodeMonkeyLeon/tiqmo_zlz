package cn.swiftpass.wallet.tiqmo.support.deep;

import android.content.Context;

import cn.swiftpass.wallet.tiqmo.support.entity.DeepBaseEntity;

public class DeepBuilder {
    private final Context mContext;
    private int deepType = -1;

    public DeepBuilder(Context mContext) {
        this.mContext = mContext;
    }

    public DeepBuilder setType(int type) {
        this.deepType = type;
        return this;
    }

    public void deep(DeepBaseEntity deepEntity) {
        if (deepType > 0) {
            deepToTab(deepType, deepEntity);
        }

    }

    private void deepToTab(int deepType, DeepBaseEntity deepEntity) {

    }
}
