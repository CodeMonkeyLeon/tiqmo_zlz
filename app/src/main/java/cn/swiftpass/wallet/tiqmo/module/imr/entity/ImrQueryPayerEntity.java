package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrQueryPayerEntity extends BaseEntity {
    public String receiptOrgCode;
    public String receiptOrgName;
    public List<ImrOrgBranchEntity> receiptOrgBranchInfoList = new ArrayList<>();
}
