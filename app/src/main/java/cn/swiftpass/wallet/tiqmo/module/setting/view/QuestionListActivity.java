package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.QuestionListAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.HelpContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpThreeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpTwoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.HelpPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class QuestionListActivity extends BaseCompatActivity<HelpContract.HelpListPresenter> implements HelpContract.HelpListView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.ry_help)
    RecyclerView ryHelp;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_no_question)
    LinearLayout ll_no_question;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;

    private List<HelpThreeEntity> filterQuestionList = new ArrayList<>();

    private String searchString;

    private QuestionListAdapter questionListAdapter;

    private List<HelpThreeEntity> allQuestionList = new ArrayList<>();
    private HelpTwoEntity helpTwoEntity;

    private int currentPostion;
    private String currentThumb,currentComment;

    private boolean isSubmitComment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_question_list;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.Help_support_1);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if(getIntent() != null && getIntent().getExtras() != null){
            helpTwoEntity = (HelpTwoEntity) getIntent().getExtras().getSerializable("helpTwoEntity");
            if(helpTwoEntity != null){
                allQuestionList = helpTwoEntity.children;
                if(allQuestionList.size() == 0){
                    ll_no_question.setVisibility(View.VISIBLE);
                    ivSearch.setVisibility(View.GONE);
                }else{
                    ll_no_question.setVisibility(View.GONE);
                    ivSearch.setVisibility(View.VISIBLE);
                }
            }else{
                ll_no_question.setVisibility(View.VISIBLE);
                ivSearch.setVisibility(View.GONE);
            }
        }

        etSearch.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryHelp.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryHelp.setLayoutManager(manager);
        questionListAdapter = new QuestionListAdapter(allQuestionList);
        questionListAdapter.setSubmitListener(new QuestionListAdapter.SubmitListener() {
            @Override
            public void clickSubmitComment(int position, String id, String comment) {
                currentPostion = position;
                currentComment = comment;
                isSubmitComment = true;
                mPresenter.submitHelpQuestion(id,comment,"");
            }

            @Override
            public void clickSubmitThumb(int position, String id,String comment, String thumb) {
                currentPostion = position;
                currentThumb = thumb;
                currentComment = comment;
                isSubmitComment = false;
                mPresenter.submitHelpQuestion(id,"",thumb);
            }
        });
        questionListAdapter.bindToRecyclerView(ryHelp);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchString = s.toString();
                if (allQuestionList != null && allQuestionList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            filterQuestionList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
                filterQuestionList.clear();
                questionListAdapter.setFilterString(filterStr);
                questionListAdapter.setDataList(allQuestionList);
                return;
            } else {
            }
            if (allQuestionList != null && allQuestionList.size() > 0) {
                int size = allQuestionList.size();
                for (int i = 0; i < size; i++) {
                    HelpThreeEntity questionEntity = allQuestionList.get(i);
                    String name = questionEntity.title+" "+questionEntity.content;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterQuestionList.add(questionEntity);
                    }
                }
                questionListAdapter.setFilterString(filterStr);
                questionListAdapter.setDataList(filterQuestionList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void getHelpContentListSuccess(HelpListEntity helpListEntity) {

    }

    @Override
    public void submitHelpQuestionSuccess(Void result) {
        if(!isSubmitComment) {
            allQuestionList.get(currentPostion).thumb = currentThumb;
        }
        allQuestionList.get(currentPostion).comment = currentComment;
        if(!allQuestionList.get(currentPostion).hasSubmit) {
            if (!TextUtils.isEmpty(currentComment)) {
                if (isSubmitComment) {
                    allQuestionList.get(currentPostion).hasSubmit = true;
                }
            }
        }
        questionListAdapter.notifyItemChanged(currentPostion);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public HelpContract.HelpListPresenter getPresenter() {
        return new HelpPresenter();
    }
}
