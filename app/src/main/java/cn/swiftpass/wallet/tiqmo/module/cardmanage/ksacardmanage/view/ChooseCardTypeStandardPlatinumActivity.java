package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.youth.banner.Banner;
import com.youth.banner.listener.OnPageChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.CardTypeChooseAdapter;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.CardTypeChoosePointAdapter;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.EquityAdapter;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.ChooseCardTypeContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardBannerEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardServiceEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardTypePointEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.ChooseCardTypePresenter;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class ChooseCardTypeStandardPlatinumActivity extends BaseCompatActivity<ChooseCardTypeContract.Presenter>
        implements View.OnClickListener, ChooseCardTypeContract.View {

    public static final int TYPE_STANDARD_CARD = 0; //普通卡
    public static final int TYPE_PLATINUM_CARD = 1; //白金卡

    public static final String CARD_FACE_ID_STANDARD_CARD = "02";

    public static final String CARD_FACE_ID_PLATINUM_CARD = "07";


    @BindView(R.id.iv_back)
    ImageView mImgBack;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView mImgHeadCircle;

    @BindView(R.id.id_banner_card)
    Banner mBannerCard;

    @BindView(R.id.id_recycler_view)
    RecyclerView mRecyclerViewEquity;

    @BindView(R.id.id_recycler_choose_point)
    RecyclerView mRecyclerViewChoosePoint;

    @BindView(R.id.id_tv_confirm)
    MyTextView mBtnConfirm;


    private EquityAdapter mEquityAdapter;

    private CardTypeChooseAdapter mCardBannerAdapter;

    private CardTypeChoosePointAdapter mCardTypeChoosePointAdapter;

    private List<CardBannerEntity> mBannerList = new ArrayList<>();

    private OpenCardReqEntity mOpenCardReqEntity;

    //Standard表示标准卡,Platinum表示白金卡
    private String mCardProductType = Constants.card_type_Standard;

    //卡片颜色 : 标准卡的01表示浅色 ,02表示深色；白金卡的06浅色,07深色
    private String mCardFaceId = CARD_FACE_ID_STANDARD_CARD;

    private List<CardServiceEntity> mShowIconList = new ArrayList<>();

    private List<CardTypePointEntity> mCardChoosePointList = new ArrayList<>();


    @Override
    protected int getLayoutID() {
        return R.layout.act_card_type_sta_pla;
    }


    public static void startChooseCardTypeStandardPlatinumActivity(Activity fromActivity) {
        ActivitySkipUtil.startAnotherActivity(
                fromActivity,
                ChooseCardTypeStandardPlatinumActivity.class,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN
        );
    }


    @Override
    protected void init(Bundle savedInstanceState) {
        initView();
        initBanner();
        initRecyclerView();
    }


    private void initRecyclerView() {
        LinearLayoutManager managerEquity = new LinearLayoutManager(mContext);
        managerEquity.setOrientation(RecyclerView.VERTICAL);
        mEquityAdapter = new EquityAdapter(mShowIconList);
        mRecyclerViewEquity.setLayoutManager(managerEquity);
        mEquityAdapter.bindToRecyclerView(mRecyclerViewEquity);


        LinearLayoutManager managerPoint = new LinearLayoutManager(mContext);
        managerPoint.setOrientation(RecyclerView.VERTICAL);
        mCardTypeChoosePointAdapter = new CardTypeChoosePointAdapter(mCardChoosePointList);
        mRecyclerViewChoosePoint.setLayoutManager(managerPoint);
        mCardTypeChoosePointAdapter.bindToRecyclerView(mRecyclerViewChoosePoint);
        mCardTypeChoosePointAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                chooseCard(position);
            }
        });
        chooseCard(TYPE_STANDARD_CARD);
    }


    private void updateCardEquity(int type) {
        mShowIconList.clear();
        switch (type) {
            case TYPE_STANDARD_CARD:
                mShowIconList.add(new CardServiceEntity(
                        R.attr.attr_icon_online_purchase,
                        R.string.sprint21_25
                ));
                mShowIconList.add(new CardServiceEntity(
                        R.attr.attr_icon_freeze,
                        R.string.sprint21_26
                ));
                mShowIconList.add(new CardServiceEntity(
                        R.attr.attr_icon_international_purchase,
                        R.string.sprint21_27
                ));
                break;

            case TYPE_PLATINUM_CARD:
                mShowIconList.add(new CardServiceEntity(
                        R.attr.attr_icon_cashback,
                        R.string.sprint21_28
                ));
                mShowIconList.add(new CardServiceEntity(
                        R.attr.attr_icon_e_commerce,
                        R.string.sprint21_29
                ));
                mShowIconList.add(new CardServiceEntity(
                        R.attr.attr_icon_discounts,
                        R.string.sprint21_30
                ));
                break;
        }
        mEquityAdapter.notifyDataSetChanged();
    }


    private void updateCardChoosePoint(int position) {
        mCardChoosePointList.clear();
        switch (position) {
            case TYPE_STANDARD_CARD:
                mCardChoosePointList.add(
                        new CardTypePointEntity(
                                R.string.DigitalCard_17,
                                "",
                                true,
                                true
                        )
                );
                mCardChoosePointList.add(
                        new CardTypePointEntity(
                                R.string.DigitalCard_18,
                                "130SAR",
                                false,
                                false
                        )
                );
                break;

            case TYPE_PLATINUM_CARD:
                mCardChoosePointList.add(
                        new CardTypePointEntity(
                                R.string.DigitalCard_17,
                                "",
                                true,
                                false
                        )
                );
                mCardChoosePointList.add(
                        new CardTypePointEntity(
                                R.string.DigitalCard_18,
                                "130SAR",
                                false,
                                true
                        )
                );
                break;
        }
        mCardTypeChoosePointAdapter.notifyDataSetChanged();
    }


    private void initView() {
        mTvTitle.setText(R.string.DigitalCard_0);
        setDarkBar();
        fixedSoftKeyboardSliding();
        LocaleUtils.viewRotationY(this, mImgBack, mImgHeadCircle);
        mImgBack.setOnClickListener(this);
        mBtnConfirm.setOnClickListener(this);
    }


    private void initBanner() {
        mOpenCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        mCardProductType = mOpenCardReqEntity.cardProductType;
        try {
            mBannerList.add(new CardBannerEntity(R.drawable.home_standard_card));
            mBannerList.add(new CardBannerEntity(R.drawable.home_card_platinum));
            mCardBannerAdapter = new CardTypeChooseAdapter(mBannerList);
            mBannerCard.addBannerLifecycleObserver(this)//添加生命周期观察者
                    .setAdapter(mCardBannerAdapter, false)
                    .setUserInputEnabled(true)//禁止手动滑动Banner
                    .isAutoLoop(false)//是否允许自动轮播
                    .setBannerGalleryEffect(20, 20, 10, 0.9f)
                    .addOnPageChangeListener(new OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            chooseCard(position);
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }


    private void chooseCard(int position) {
        switch (position) {
            case TYPE_STANDARD_CARD:
                //普通卡
                mCardFaceId = CARD_FACE_ID_STANDARD_CARD;
                mCardProductType = Constants.card_type_Standard;
                updateCardEquity(TYPE_STANDARD_CARD);
                updateCardChoosePoint(TYPE_STANDARD_CARD);
                mBannerCard.setCurrentItem(TYPE_STANDARD_CARD);
                break;

            case TYPE_PLATINUM_CARD:
                //白金卡
                mCardProductType = Constants.card_type_Platinum;
                mCardFaceId = CARD_FACE_ID_PLATINUM_CARD;
                updateCardEquity(TYPE_PLATINUM_CARD);
                updateCardChoosePoint(TYPE_PLATINUM_CARD);
                mBannerCard.setCurrentItem(TYPE_PLATINUM_CARD);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        if (v != null) {
            switch (v.getId()) {
                case R.id.iv_back:
                    finish();
                    break;
                case R.id.id_tv_confirm:
                    mOpenCardReqEntity.cardProductType = mCardProductType;
                    mOpenCardReqEntity.cardFaceId = mCardFaceId;
                    UserInfoManager.getInstance().setOpenCardReqEntity(mOpenCardReqEntity);
                    mPresenter.getKsaCardSummary("", mOpenCardReqEntity.cardProductType);
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public ChooseCardTypeContract.Presenter getPresenter() {
        return new ChooseCardTypePresenter();
    }

    @Override
    public void getKsaCardSummarySuccess(@Nullable KsaCardSummaryEntity ksaCardSummaryEntity) {
        if (ksaCardSummaryEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksaCardSummaryEntity, ksaCardSummaryEntity);
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardFourActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getKsaCardSummaryFailed(@Nullable String errorCode, @Nullable String errorMsg) {
        showTipDialog(errorMsg);
    }
}
