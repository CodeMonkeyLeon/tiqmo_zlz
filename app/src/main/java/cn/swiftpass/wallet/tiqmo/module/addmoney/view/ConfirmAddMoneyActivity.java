package cn.swiftpass.wallet.tiqmo.module.addmoney.view;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.exception.PaymentException;
import com.oppwa.mobile.connect.payment.BrandsValidation;
import com.oppwa.mobile.connect.payment.CheckoutInfo;
import com.oppwa.mobile.connect.payment.ImagesRequest;
import com.oppwa.mobile.connect.payment.PaymentParams;
import com.oppwa.mobile.connect.payment.card.CardPaymentParams;
import com.oppwa.mobile.connect.payment.token.Token;
import com.oppwa.mobile.connect.payment.token.TokenPaymentParams;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.ITransactionListener;
import com.oppwa.mobile.connect.provider.OppPaymentProvider;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.presenter.ConfirmTransferPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityLifeManager;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ProgressDialog;

public class ConfirmAddMoneyActivity extends BaseCompatActivity<AddMoneyContract.ConfirmTransferPresenter> implements AddMoneyContract.ConfirmTransferView, ITransactionListener {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_google_pay)
    ImageView ivGooglePay;
    @BindView(R.id.tv_topup_amount)
    TextView tvTopupAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_wrong_fees)
    TextView tvWrongFees;
    @BindView(R.id.tv_fees)
    TextView tvTransFee;
    @BindView(R.id.tv_wrong_vat_money)
    TextView tvWrongVatMoney;
    @BindView(R.id.tv_vat_money)
    TextView tvVat;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_card_number)
    TextView tvCardNumber;
    @BindView(R.id.tv_card_expire)
    TextView tvCardExpire;
    @BindView(R.id.iv_card)
    ImageView ivCard;
    @BindView(R.id.iv_change_card)
    ImageView ivChangeCard;
    @BindView(R.id.ll_transfer_vat)
    ConstraintLayout llTransferVat;
    @BindView(R.id.ll_transfer_fee)
    ConstraintLayout llTransferFee;
    @BindView(R.id.tv_discount)
    TextView tvDiscount;
    @BindView(R.id.con_discount)
    ConstraintLayout conDiscount;
    @BindView(R.id.tv_discount_content)
    TextView tv_discount_content;
    @BindView(R.id.tv_discount_title)
    TextView tvDiscountTitle;

//    private CheckOutEntity checkOutEntity;

    private String totalAmount;

    private CardEntity mCardEntity;

    //    private IProviderBinder binder;
    String resourcePath = "0";

    private OppPaymentProvider paymentProvider;

    private String topupAmount, transferFees, wrongTransferFee, wrongVat, vatAmount,
            orderCurrencyCode, cvv, protocolNo, custName, cardNo,unMaskCardNo, expire, showCardNo;

    //卡有效期月份
    private String month,year;
    private  String checkcredit;
    private String tokenId,checkoutId;

    private String orderNo;

    private boolean fromAddCard;
    private ProgressDialog progressDialog;

    private TransferPayEntity mTransferPayEntity;

    private RiskControlEntity riskControlEntity;

    private static ConfirmAddMoneyActivity confirmAddMoneyActivity;

    public static ConfirmAddMoneyActivity getConfirmAddMoneyActivity() {
        return confirmAddMoneyActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_confirm_add_money;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();

        if(getIntent() != null && getIntent().getExtras() != null) {
            mTransferPayEntity = (TransferPayEntity) getIntent().getExtras().getSerializable(Constants.transferPayEntity);
            mCardEntity = (CardEntity) getIntent().getExtras().getSerializable(Constants.cardEntity);
            fromAddCard = getIntent().getExtras().getBoolean("fromAddCard");
            if(mCardEntity != null){
                cardNo = mCardEntity.getCardNo();
                unMaskCardNo = mCardEntity.unMaskCardNo;
                expire = mCardEntity.getExpire();
                cvv = mCardEntity.getCvv();
                checkcredit = mCardEntity.getCardOrganization();
                custName = mCardEntity.getCustName();
                StringBuffer sb = new StringBuffer();
                sb.append(expire).insert(2,"/");
                if(!TextUtils.isEmpty(custName)){
                    tvCardNumber.setText(custName);
                }else {
                    if(getUserInfoEntity() != null) {
                        custName = getUserInfoEntity().userName;
                    }
                    if (!TextUtils.isEmpty(cardNo)) {
                        tvCardNumber.setText("**** " + cardNo.substring(12, 16));
//                        custName = "**** "+cardNo.substring(12, 16);
                    }
                }
                tvCardExpire.setText(mContext.getString(R.string.sprint11_38)+" "+sb.toString());
                int cardLogoRes = AndroidUtils.getCardLogo(mCardEntity.getCardOrganization());
                if(cardLogoRes != -1) {
                    ivCard.setImageResource(ThemeSourceUtils.getSourceID(mContext, cardLogoRes));
                }
            }
            tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            totalAmount = "0.00";
            if (mTransferPayEntity != null) {
                orderNo = mTransferPayEntity.getOrderNo();
                riskControlEntity = mTransferPayEntity.riskControlInfo;
                topupAmount = mTransferPayEntity.getOrderAmount();
                transferFees = mTransferPayEntity.getDiscountTransFees();
                wrongTransferFee = mTransferPayEntity.getTransFees();
                totalAmount = mTransferPayEntity.getTotalAmount();
                wrongVat = mTransferPayEntity.getVat();
                vatAmount = mTransferPayEntity.getDiscountVat();
                orderCurrencyCode = mTransferPayEntity.getOrderCurrencyCode();
                String couponAmount = mTransferPayEntity.couponAmount;
                String couponType = mTransferPayEntity.couponType;
                if (Constants.CASH_BACK.equals(couponType)) {
                    conDiscount.setVisibility(View.VISIBLE);
                    tv_discount_content.setVisibility(View.VISIBLE);
                    tvDiscountTitle.setText(R.string.trx_detail_6);
                } else if (Constants.CASH_REDUCE.equals(couponType)) {
                    conDiscount.setVisibility(View.VISIBLE);
                    tvDiscountTitle.setText(R.string.trx_detail_4);
                }
                if (!TextUtils.isEmpty(couponAmount)) {
                    if (!BigDecimalFormatUtils.compareBig(couponAmount, "0")) {
                        conDiscount.setVisibility(View.GONE);
                    } else {
                        tvDiscount.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(couponAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(" " + LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                                .build());

                    }
                }

                if (!TextUtils.isEmpty(topupAmount)) {
                    tvTopupAmount.setText(Spans.builder()
                            .text(topupAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15)))
                            .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                            .build());
                }
                if (!TextUtils.isEmpty(wrongVat)) {
                    ////如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                    llTransferVat.setVisibility(View.VISIBLE);
                    tvWrongVatMoney.setText(Spans.builder()
                            .text(wrongVat + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());

                    if (!TextUtils.isEmpty(vatAmount)) {
                        tvVat.setText(Spans.builder()
                                .text(vatAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }else {
                    //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                    if (!TextUtils.isEmpty(vatAmount) && BigDecimalFormatUtils.compareBig(vatAmount, "0")) {
                        tvWrongVatMoney.setVisibility(View.GONE);
                        tvVat.setText(Spans.builder()
                                .text(vatAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    } else {
                        llTransferVat.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(wrongTransferFee)) {
                    //如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                    llTransferFee.setVisibility(View.VISIBLE);
                    tvWrongFees.setText(Spans.builder()
                            .text(wrongTransferFee + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());

                    if(!TextUtils.isEmpty(transferFees)) {
                        tvTransFee.setText(Spans.builder()
                                .text(transferFees + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }else{
                    //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                    if (!TextUtils.isEmpty(transferFees) && BigDecimalFormatUtils.compareBig(transferFees,"0")) {
                        tvWrongFees.setVisibility(View.GONE);
                        tvTransFee.setText(Spans.builder()
                                .text(transferFees + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }else{
                        llTransferFee.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(totalAmount)) {
                    tvTotalAmount.setText(Spans.builder()
                            .text(totalAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                            .build());
                }
            }
        }
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @OnClick({R.id.tv_cancel, R.id.tv_confirm,R.id.iv_change_card})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_cancel:
                break;
            case R.id.tv_confirm:
                showMainProgress();
                if(fromAddCard){
                    mPresenter.bindCardConfirmPay(orderNo,cvv,"R",Constants.shopperResultURL,checkcredit);
                }else {
                    if(mTransferPayEntity != null){
                        mPresenter.cardAddMoneyConfirmPay(orderNo,cvv,"R",Constants.shopperResultURL,checkcredit);
                    }
                }
                break;
            case R.id.iv_change_card:
                finish();
                break;
            default:break;
        }
    }

    private void addMoney(RiskControlEntity riskControlEntity){
        if (riskControlEntity != null) {
            if (riskControlEntity.isOtp()) {
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                setTransferMap(mHashMaps);
                ActivitySkipUtil.startAnotherActivity(ConfirmAddMoneyActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                jumpToPwd(false);
            }else if(riskControlEntity.isNeedIvr()){
                jumpToPwd(true);
            }else{
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        confirmAddMoneyActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_HyperPay_ADD_MONEY == event.getEventType()) {
            requestTransfer();
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.sceneType, Constants.TYPE_HyperPay_ADD_MONEY);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_HyperPay_ADD_MONEY);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.newhome_3));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void requestTransfer() {
        if(fromAddCard){
            try {
                if(!TextUtils.isEmpty(expire)) {
                    month = expire.substring(0, 2);
                    year = expire.substring(2);
                }
                String niceName = getUserInfoEntity().userName;
                PaymentParams paymentParams = new CardPaymentParams(
                        checkoutId,
                        checkcredit.toUpperCase(),
                        cardNo,
                        niceName,
                        month,
                        "20"+year,
                        cvv
                );
                Transaction transaction = new Transaction(paymentParams);
                paymentProvider.submitTransaction(transaction,ConfirmAddMoneyActivity.this);
            } catch (PaymentException e) {
                if(progressDialog != null)progressDialog.dismiss();
                LogUtils.d("errorMsg", "---"+e+"---");
                showTipDialog(e.getMessage());
            } catch (Throwable e){
                if(progressDialog != null)progressDialog.dismiss();
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }else {
            if (!TextUtils.isEmpty(checkoutId)) {
                openAddMoney(checkoutId);
            }
        }
    }

    protected void showMainProgress(){
        progressDialog = new ProgressDialog(mContext);
        progressDialog.show();

    }

    @Override
    public void cardAddMoneyConfirmPaySuccess(CardBind3DSEntity cardBind3DSEntity) {
        if(cardBind3DSEntity != null){
            checkoutId = cardBind3DSEntity.checkoutId;
            tokenId = cardBind3DSEntity.paymentId;
            orderNo = cardBind3DSEntity.orderNo;
            if(!TextUtils.isEmpty(checkoutId)){
                addMoney(riskControlEntity);
            }
        }
    }

    @Override
    public void bindCardConfirmPayPaySuccess(CardBind3DSEntity cardBind3DSEntity) {
        if(cardBind3DSEntity != null){
            checkoutId = cardBind3DSEntity.checkoutId;
            if(!TextUtils.isEmpty(checkoutId)){
                addMoney(riskControlEntity);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(progressDialog != null)progressDialog.dismiss();
    }

    @Override
    public void request3dsSuccess(CardNotifyEntity cardNotifyEntity) {
        if (cardNotifyEntity == null) {
            return;
        }
        if (!TextUtils.isEmpty(cardNotifyEntity.bizCode) && "010167".equals(cardNotifyEntity.bizCode)) {
            return;
        }
        if (!TextUtils.isEmpty(cardNotifyEntity.operateType) && "recharge".equals(cardNotifyEntity.operateType)) {
            //充值成功
            cardNotifyEntity.respCode = cardNotifyEntity.bizCode;
            if ("000000".equals(cardNotifyEntity.bizCode)) {
            } else {
                cardNotifyEntity.respMsg = cardNotifyEntity.bizMsg;
            }
            cardNotifyEntity.subject = Constants.SUBJECT_ADD_MONEY;
            cardNotifyEntity.paymentMethodDesc = cardNotifyEntity.payMethodDesc;
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            ProjectApp.removeHyperpayTask();
            if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                ProjectApp.addMoneySuccessOnMain = false;
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if(progressDialog != null)progressDialog.dismiss();
        showTipDialog(errorMsg);
    }

    @Override
    public AddMoneyContract.ConfirmTransferPresenter getPresenter() {
        return new ConfirmTransferPresenter();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }

        if(paymentProvider == null) {
            paymentProvider = new OppPaymentProvider(mContext, Connect.ProviderMode.TEST);
        }
    }

    private void openAddMoney(String checkoutId){
        try {
            TokenPaymentParams paymentParams;
//            tokenId = "8ac7a4a182d1e30b0182d382a8cc3069";
//            checkoutId = "3E5185369BE64D9BC479887BF1F4D7F9.uat01-vm-tx04";
            if("MADA".equals(checkcredit.toUpperCase(Locale.ENGLISH))){
                //Mada卡要传cvv
                paymentParams = new TokenPaymentParams(checkoutId, tokenId, checkcredit.toUpperCase(),cvv);
            }else {
                paymentParams = new TokenPaymentParams(checkoutId, tokenId, checkcredit.toUpperCase());
            }
            Transaction transaction = new Transaction(paymentParams);
            paymentProvider.submitTransaction(transaction,this);
        } catch (PaymentException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                /* transaction completed */
                Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);
                /* resource path if needed */
                String resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);

                if (transaction.getTransactionType() == TransactionType.SYNC) {
                    /* check the result of synchronous transaction */
                } else {
                    /* wait for the asynchronous transaction callback in the onNewIntent() */
                }

                break;
            case CheckoutActivity.RESULT_CANCELED:
                /* shopper canceled the checkout process */
                Toast.makeText(getBaseContext(),"canceled",Toast.LENGTH_LONG).show();
                break;
            case CheckoutActivity.RESULT_ERROR:
                /* error occurred */
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);

                Toast.makeText(getBaseContext(),"error",Toast.LENGTH_LONG).show();
                Log.e("errorrr",String.valueOf(error.getErrorInfo()));
                Log.e("errorrr2",String.valueOf(error.getErrorCode()));
                Log.e("errorrr3",String.valueOf(error.getErrorMessage()));
                Log.e("errorrr4",String.valueOf(error.describeContents()));
        }
    }

    @Override
    public void brandsValidationRequestSucceeded(BrandsValidation brandsValidation) {

    }

    @Override
    public void brandsValidationRequestFailed(PaymentError paymentError) {

    }

    @Override
    public void imagesRequestSucceeded(ImagesRequest imagesRequest) {

    }

    @Override
    public void imagesRequestFailed() {

    }

    @Override
    public void paymentConfigRequestSucceeded(CheckoutInfo checkoutInfo) {

        if(checkoutInfo != null) {
            resourcePath = checkoutInfo.getResourcePath();
            /* get the tokens */
            Token[] tokens = checkoutInfo.getTokens();
            LogUtils.d("tokenss", tokens.toString());
        }
    }

    @Override
    public void paymentConfigRequestFailed(PaymentError paymentError) {
        if(paymentError != null) {
            LogUtils.d("tokenss", paymentError.toString());
        }
    }

    @Override
    public void transactionCompleted(Transaction transaction) {
        if(progressDialog != null)progressDialog.dismiss();
        if (transaction == null) {
            return;
        }

        if (transaction.getTransactionType() == TransactionType.SYNC) {
            AndroidUtils.writeLogToFile("TransactionType.SYNC");
            /* check the status of synchronous transaction */
            mPresenter.request3ds("",orderNo);
//            LogUtils.d("getTransactionType", "SYNC");
        } else {
            AndroidUtils.writeLogToFile("TransactionType.WebViewActivity");
            String authPage = transaction.getRedirectUrl();
            //跳转到3ds验证页面
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.BIND_CARD_3DS, authPage);
            mHashMaps.put(Constants.Hyperpay_orderNo, orderNo);
            mHashMaps.put("isCardDetail", true);

            ActivitySkipUtil.startAnotherActivity(ConfirmAddMoneyActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void transactionFailed(Transaction transaction, PaymentError paymentError) {
        if (transaction == null) {
            return;
        }
    }
}
