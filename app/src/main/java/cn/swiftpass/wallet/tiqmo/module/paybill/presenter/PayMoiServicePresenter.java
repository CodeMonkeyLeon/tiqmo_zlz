package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayMoiServicePresenter implements PayBillContract.MoiServicePresenter {


    PayBillContract.MoiServiceView mMoiServiceView;

    @Override
    public void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillerList(countryCode, billerType, billerDescription, channelCode, new LifecycleMVPResultCallback<PayBillerListEntity>(mMoiServiceView, true) {
            @Override
            protected void onSuccess(PayBillerListEntity result) {
                if (mMoiServiceView != null) {
                    mMoiServiceView.getPayBillerListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mMoiServiceView != null) {
                    mMoiServiceView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getMoiServiceIOList(String billerId, String sku, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillServiceIOList(billerId, sku, channelCode, new LifecycleMVPResultCallback<PayBillIOListEntity>(mMoiServiceView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                if (mMoiServiceView != null) {
                    mMoiServiceView.getMoiServiceIOListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mMoiServiceView != null) {
                    mMoiServiceView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void attachView(PayBillContract.MoiServiceView moiServiceView) {
        mMoiServiceView = moiServiceView;
    }

    @Override
    public void detachView() {
        mMoiServiceView = null;
    }
}
