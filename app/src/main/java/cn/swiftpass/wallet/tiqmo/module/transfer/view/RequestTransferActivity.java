package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferToContactPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;


public class RequestTransferActivity extends BaseCompatActivity<TransferContract.TransferToContactPresenter> implements TransferContract.TransferToContactView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_from_avatar)
    RoundedImageView ivFromAvatar;
    @BindView(R.id.iv_to_avatar)
    RoundedImageView ivToAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.et_amount)
    EditTextWithDel etAmount;
    @BindView(R.id.et_note)
    CustomizeEditText etNote;
    @BindView(R.id.tv_transfer)
    TextView tvTransfer;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_send)
    LottieAnimationView ivSend;
    @BindView(R.id.iv_date_to_delete)
    ImageView dateToDelete;
    @BindView(R.id.tv_money_currency)
    TextView tvMoneyCurrency;

    private double money;
    //    private double valiableMoney;
    private KycContactEntity kycContactEntity;

    private double monthLimitMoney, maxLimitBalance;

    private String sceneType;
    private String payerName;

    private RiskControlEntity riskControlEntity;

    private static RequestTransferActivity requestTransferActivity;
    private BottomDialog bottomDialog;

    public static RequestTransferActivity getRequestTransferActivity() {
        return requestTransferActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestTransferActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_request_transfer;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        requestTransferActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle, ivSend);
//        etAmount.setRightText(LocaleUtils.getCurrencyCode(""));

        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        tvTitle.setText(R.string.request_transfer_1);
        etAmount.getTlEdit().setHint(getString(R.string.wtw_22));
        if (getIntent() != null && getIntent().getExtras() != null) {
            kycContactEntity = (KycContactEntity) getIntent().getExtras().get(Constants.CONTACT_ENTITY);
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            sceneType = getIntent().getExtras().getString(Constants.sceneType);
            if (userInfoEntity != null) {
                String avatar = userInfoEntity.avatar;
                String gender = userInfoEntity.gender;
                if (!TextUtils.isEmpty(avatar)) {
                    Glide.with(this).clear(ivFromAvatar);
                    int round = (int) AndroidUtils.dip2px(mContext, 8);
                    Glide.with(this)
                            .load(avatar)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                            .into(ivFromAvatar);
                } else {
                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivFromAvatar);
                }
            }
            if (kycContactEntity != null) {
                tvPhone.setText(kycContactEntity.getPhone());
                String name = kycContactEntity.getContactsName();
                payerName = kycContactEntity.getContactsName();
                if (!TextUtils.isEmpty(name)) {
                    tvName.setVisibility(View.VISIBLE);
//                    name = AndroidUtils.getContactName(name);
                } else {
                    tvName.setVisibility(View.GONE);
                }
                tvName.setText(name);

                if (ThemeUtils.isCurrentDark(mContext)) {
                    ivSend.setAnimation("lottie/arrow_request_transfer.json");
                } else {
                    ivSend.setAnimation("lottie/l_arrow_request_transfer.json");
                }

                String avatarUrl = kycContactEntity.getHeadIcon();
                String gender = kycContactEntity.getSex();
                if (!TextUtils.isEmpty(avatarUrl)) {
                    Glide.with(mContext).clear(ivToAvatar);
                    Glide.with(mContext)
                            .asBitmap()
                            .load(avatarUrl)
                            .diskCacheStrategy(DiskCacheStrategy.DATA)
                            .dontAnimate()
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(new BitmapImageViewTarget(ivToAvatar) {
                                @Override
                                public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                                    ivToAvatar.setImageBitmap(bitmap);
                                }
                            });
                } else {
                    Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivToAvatar);
                }
            }
        }

        //两位小数过滤
        etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
//        etAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        etAmount.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                if (TextUtils.isEmpty(amount)) {
                    tvMoneyCurrency.setVisibility(View.GONE);
                    dateToDelete.setVisibility(View.GONE);
                    tvTransfer.setEnabled(false);
                } else {
                    try {
                        money = Double.parseDouble(amount);
                        if (money > 0) {
                            tvMoneyCurrency.setVisibility(View.VISIBLE);
                            dateToDelete.setVisibility(View.VISIBLE);
                            if (money > monthLimitMoney) {
                                tvTransfer.setEnabled(false);
                                etAmount.showErrorViewWithMsg(getString(R.string.wtw_25_3));
                            } else if (userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                                tvTransfer.setEnabled(false);
                                etAmount.showErrorViewWithMsg(getString(R.string.newhome_45));
                            } else {
                                tvTransfer.setEnabled(true);
                                etAmount.showErrorViewWithMsg("");
                            }
                        } else {
                            tvTransfer.setEnabled(false);
                        }
                    } catch (Exception e) {
                        tvTransfer.setEnabled(false);
                    }
                }
            }
        });

        etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                String amount = etAmount.getEditText().getText().toString().trim();
                amount = amount.replace(",", "");
                String fomatAmount = AmountUtil.dataFormat(amount);
                if (!hasFocus) {
                    if (TextUtils.isEmpty(amount)) {
                    } else {
                        etAmount.getEditText().setText(fomatAmount);
                        money = Double.parseDouble(amount);
                        if (money > monthLimitMoney) {
                            tvTransfer.setEnabled(false);
                            etAmount.showErrorViewWithMsg(getString(R.string.wtw_25_3));
                        } else if (userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                            tvTransfer.setEnabled(false);
                            etAmount.showErrorViewWithMsg(getString(R.string.newhome_45));
                        } else {
                            tvTransfer.setEnabled(true);
                            etAmount.showErrorViewWithMsg("");
                        }
                    }
                } else {
                    etAmount.showErrorViewWithMsg("");
                }
            }
        });

        mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkUser();
    }

    private void checkUser() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.avatar;
            String gender = userInfoEntity.gender;
            String balance = userInfoEntity.getBalance();
            String balanceNumber = userInfoEntity.getBalanceNumber();
//            if (!TextUtils.isEmpty(balanceNumber)) {
//                try {
//                    valiableMoney = Double.parseDouble(balanceNumber);
//                } catch (Exception e) {
//                    LogUtils.d("errorMsg", "---"+e+"---");
//                }
//            }
            if (!TextUtils.isEmpty(avatar)) {
                Glide.with(this).clear(ivFromAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                        .into(ivFromAvatar);
            } else {
                Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivFromAvatar);
            }
        }
    }

    @OnClick({R.id.tv_transfer, R.id.iv_back,R.id.iv_date_to_delete})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_transfer:
                List<String> additionalData = new ArrayList<>();
                additionalData.add(kycContactEntity.getRequestPhoneNumber());
                mPresenter.getOtpType("ActionType","8",additionalData);
                break;
            case R.id.iv_date_to_delete:
                etAmount.getEditText().setText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        transferToContactSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {

    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {

    }

    @Override
    public void transferToContactFail(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        }else {
            etAmount.showErrorViewWithMsg(errMsg);
        }
    }


    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {

    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {

    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {

    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        String monthLimitAmt = transferLimitEntity.monthLimitAmt;
        try {
            monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
            maxLimitBalance = AndroidUtils.getTransferMoneyNumber(transferLimitEntity.maxLimitBalance);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if(rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
//                jumpToPwd();
            }
        }
    }

    private void requestTransfer(){
        if (kycContactEntity != null) {
            String payMoney = etAmount.getEditText().getText().toString().trim();
            payMoney = payMoney.replace(",", "");
            String remark = etNote.getText().toString().trim();
            kycContactEntity.payMoney = payMoney;
            kycContactEntity.remark = remark;
            mPresenter.transferToContact(kycContactEntity.getUserId(), SpUtils.getInstance().getCallingCode(), kycContactEntity.getRequestPhoneNumber(), userInfoEntity.userId,
                    userInfoEntity.getPhone(), AndroidUtils.getReqTransferMoney(kycContactEntity.payMoney), UserInfoManager.getInstance().getCurrencyCode(),
                    kycContactEntity.remark, sceneType, "", "1", "", "", "", payerName);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        String payMoney = etAmount.getEditText().getText().toString().trim();
        payMoney = payMoney.replace(",", "");
        String remark = etNote.getText().toString().trim();
        kycContactEntity.payMoney = payMoney;
        kycContactEntity.remark = remark;
        mHashMap.put(Constants.CONTACT_ENTITY, kycContactEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_TRANSFER_RT);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_REQUEST_TRANSFER);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.request_transfer_1));
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {

    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {

    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity result) {

    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {

    }

    @Override
    public TransferContract.TransferToContactPresenter getPresenter() {
        return new TransferToContactPresenter();
    }
}
