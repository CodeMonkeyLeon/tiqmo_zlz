package cn.swiftpass.wallet.tiqmo.module.voucher.dialog;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.zrq.spanbuilder.Spans;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;

public class OrderRepeatNoticeDia extends BaseDialogFragmentN {

    private TextView tv_continue, tv_back_to_home, tv_order_repeat;
//    private MaxHeightRecyclerView rv_select_store;

    private String storeName;
    private ContinueClickListener continueClickListener;
    private static String TAG = "OrderRepeatNotice";

    public void setListener(ContinueClickListener listener) {
        this.continueClickListener = listener;
    }

    public static OrderRepeatNoticeDia newInstance(String storeName) {
        OrderRepeatNoticeDia fragment = new OrderRepeatNoticeDia();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TAG, storeName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_order_repeat;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
        if (getArguments() != null) {
            storeName = (String) getArguments().getSerializable(TAG);
            initData();
        }
    }

    private void initData() {
        tv_order_repeat.setText(Spans.builder()
                .text(mContext.getString(R.string.Alert_2))
                .text(" (" + storeName + ") ").color(mContext.getColor(R.color.color_fc4f00)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"))
                .text(mContext.getString(R.string.Alert_3))
                .build());
    }

    private void initView(View parentView) {
        tv_continue = parentView.findViewById(R.id.tv_continue);
        tv_back_to_home = parentView.findViewById(R.id.tv_back_to_home);
        tv_order_repeat = parentView.findViewById(R.id.tv_order_repeat);

        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (continueClickListener != null){
                    continueClickListener.continueClick();
                }
                dismiss();
            }
        });
        tv_back_to_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface ContinueClickListener {
        void continueClick();
    }

    public void show(@NonNull FragmentManager manager){
        show(manager, TAG);
    }
}