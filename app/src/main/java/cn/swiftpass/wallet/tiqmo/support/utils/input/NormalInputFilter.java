package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;


public class NormalInputFilter implements InputFilter {
    public static final int CHARSEQUENCE_NUMBER = 901;
    public static final int CHINESE = 902;
    public static final int CHARSEQUENCE_CHINESE__NUMBER = 903;
    public static final int CHARSEQUENCE_CHINESE__NUMBER_SPECIAL = 904;
    public static final int CHARSEQUENCE__NUMBER_SPECIAL = 905;
    public static final int CHINESE__NUMBER_SPECIAL = 906;
    public static final int CHARSEQUENCE__NUMBER_SPACE = 907;
    public static final int CHINESE__NUMBER_SPACE = 908;
    public static final int CHARSEQUENCE_CHINESE__NUMBER_SPACE = 909;
    public static final int CHARSEQUENCE_CHINESE__NUMBER_SPECIAL_SPACE = 910;
    public static final int CHARSEQUENCE__NUMBER_SPECIAL_SPACE = 911;
    public static final int CHINESE_SPECIAL_SPACE = 912;
    public static final int CHINESE_NUMBER_SPECIAL_SPACE = 913;
    private final String STR_CHARSEQUENCE = "A-Za-z";


    private final String STR_SPACE = " ";
    private final String STR_NUMBER = "0-9";
    private final String STR_CHINESE = "\\u4E00-\\u9FFF";
    private final String STR_SPECIAL = "`~!\\-_@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？";

    private Pattern mPattern = Pattern.compile("[A-Za-z0-9\\u4E00-\\u9FFF`~!@#$%^&*()\\-_+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]*");

    public NormalInputFilter(int type) {
        switch (type) {
            case CHARSEQUENCE_NUMBER:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE+STR_NUMBER + "]*");
                break;
            case CHINESE:
                mPattern = Pattern.compile("[" + STR_CHINESE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE+STR_NUMBER + STR_CHINESE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE +STR_NUMBER+ STR_CHINESE + STR_SPECIAL + "]*");
                break;
            case CHARSEQUENCE__NUMBER_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE +STR_NUMBER+ STR_SPECIAL + "]*");
                break;
            case CHINESE__NUMBER_SPECIAL:
                mPattern = Pattern.compile("[" + STR_SPECIAL + "]*");
                break;
            case CHARSEQUENCE__NUMBER_SPACE:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE+STR_NUMBER + STR_SPACE + "]*");
                break;
            case CHINESE__NUMBER_SPACE:
                mPattern = Pattern.compile("[" + STR_CHINESE + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER_SPACE:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE+STR_NUMBER + STR_CHINESE + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_CHINESE__NUMBER_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE+STR_NUMBER + STR_CHINESE + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE__NUMBER_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHARSEQUENCE+STR_NUMBER + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHINESE_NUMBER_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHINESE + STR_SPECIAL + STR_SPACE + STR_NUMBER + "]*");
                break;
            default:
                break;
        }
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (mPattern.matcher(temp).matches()) {
            return source;
        }
        return "";
    }
}
