package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillIOListEntity extends BaseEntity {
    public String paymentRulePrompt;
    public String aliasName;
    public String isValidBiller;
    //场景类型 1拉取账单-单sku 3非拉取账单-单sku
    public int sceneType;
    public List<PayBillIOEntity> ioList = new ArrayList<>();
    //退款IoList
    public List<PayBillIOEntity> refundIoList = new ArrayList<>();
}
