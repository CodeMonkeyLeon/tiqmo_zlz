package cn.swiftpass.wallet.tiqmo.module.topup.presenter;

import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RechargePayPresenter implements RechargeContract.RechargePayPresenter{

    private RechargeContract.RechargePayView mRechargePayView;

    @Override
    public void attachView(RechargeContract.RechargePayView rechargePayView) {
        this.mRechargePayView = rechargePayView;
    }

    @Override
    public void detachView() {
        this.mRechargePayView = null;
    }

    @Override
    public void preRechargeOrder(String productId, String operatorId, String country, String mobileNumber, String amount,String targetAmount,String shortName,String operatorName) {
        AppClient.getInstance().getTransferManager().rechargePreOrder(productId, operatorId, country, mobileNumber, amount,targetAmount,shortName,operatorName,
                new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mRechargePayView,true) {
                    @Override
                    protected void onSuccess(RechargeOrderInfoEntity result) {
                        mRechargePayView.preRechargeOrderSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mRechargePayView.showErrorMsg(errorCode,errorMsg);
                    }
                });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(mRechargePayView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                mRechargePayView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargePayView.checkOutFail(errorCode, errorMsg);
            }
        });
    }
}
