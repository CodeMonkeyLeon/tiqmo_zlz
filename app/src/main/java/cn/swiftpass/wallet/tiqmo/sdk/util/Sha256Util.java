package cn.swiftpass.wallet.tiqmo.sdk.util;

import android.text.TextUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;

public class Sha256Util {

    public static String sha256WithSalt(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }
        string = string + AppConfig.PD_HASH_SALT;
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("SHA-256").digest(string.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, SHA-256 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10)
                hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }
}
