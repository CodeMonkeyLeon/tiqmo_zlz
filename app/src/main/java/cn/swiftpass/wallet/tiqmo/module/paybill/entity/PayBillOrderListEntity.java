package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillOrderListEntity extends BaseEntity {

    /**
     * {\"countryImgUrl\":\"http://wallet-saas-dev.wallyt.net/tiqmo/cms/file/download?imr/country/pk.png\",\"billerId\":\"586000000000015\",\"alaisName\":\"mobile prepaid\",\"countryCode\":\"PAK\",\"countryName\":\"Pakistan\",\"sku\":\"00000000000000000800\",\"billerName\":\"Warid Prepaid\"}
     */
    public List<PayBillOrderEntity> internationalList = new ArrayList<>();
    public List<PayBillOrderEntity> domesticList = new ArrayList<>();

    public List<PayBillOrderEntity> getTestList() {
        List<PayBillOrderEntity> list = new ArrayList<>();
        PayBillOrderEntity payBillOrderEntity = new PayBillOrderEntity();
        payBillOrderEntity.countryImgUrl = "http://wallet-saas-dev.wallyt.net/tiqmo/cms/file/download?imr/country/pk.png";
        payBillOrderEntity.billerId = "586000000000015";
        payBillOrderEntity.alaisName = "mobile prepaid";
        payBillOrderEntity.billerName = "Warid Prepaid";
        payBillOrderEntity.sku = "00000000000000000800";
        payBillOrderEntity.countryName = "Pakistan";
        payBillOrderEntity.countryCode = "PAK";
        list.add(payBillOrderEntity);
        list.add(payBillOrderEntity);
        list.add(payBillOrderEntity);
        list.add(payBillOrderEntity);
        return list;
    }
}
