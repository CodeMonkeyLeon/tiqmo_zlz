package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import cn.swiftpass.wallet.tiqmo.base.contract.BaseTransferPresenter;
import cn.swiftpass.wallet.tiqmo.base.contract.BaseTransferView;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;

public class IvrCallContract {
    public interface IvrCallView extends BaseTransferView<IvrCallContract.IvrCallPresenter> {
        void sendIvrCallSuccess(ResultEntity resultEntity);
        void getIvrResultSuccess(ResultEntity resultEntity);
        void getIvrResultFail(String errorCode,String errorMsg);
        void operateIvrCallSuccess(Void result);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface IvrCallPresenter extends BaseTransferPresenter<IvrCallView> {
        void sendIvrCall(String outActionId,
                         String amount);
        void getIvrResult(String outActionId);
    }
}
