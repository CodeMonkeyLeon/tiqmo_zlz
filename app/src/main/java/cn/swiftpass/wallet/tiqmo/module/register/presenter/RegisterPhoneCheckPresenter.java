package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterPhoneCheckContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RegisterPhoneCheckPresenter implements RegisterPhoneCheckContract.Presenter {

    private RegisterPhoneCheckContract.View baseView;

    @Override
    public void checkPhoneReg(String callingCode, String phone,double longitude,double latitude) {
        if(baseView == null){
            return;
        }
        baseView.showProgress(true);
        AppClient.getInstance().getUserManager().checkUserExist(callingCode, phone, longitude, latitude, new LifecycleMVPResultCallback<CheckPhoneEntity>(baseView,true) {
            @Override
            public void onSuccess(CheckPhoneEntity result) {
                baseView.showProgress(false);
                baseView.checkPhoneRegSuccess(result);
            }

            @Override
            public void onFail(String errorCode, String errorMsg) {
                baseView.showProgress(false);
                baseView.checkPhoneError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void checkChangePhone(String callingCode, String phone, double longitude, double latitude, String type, String checkIdNumber, String birthday,String needCheckIdNumber) {
        if(baseView == null){
            return;
        }
        baseView.showProgress(true);
        AppClient.getInstance().checkPhoneReg(callingCode, phone, longitude, latitude,type,checkIdNumber,birthday, needCheckIdNumber, new LifecycleMVPResultCallback<CheckPhoneEntity>(baseView,true) {
            @Override
            public void onSuccess(CheckPhoneEntity result) {
                baseView.showProgress(false);
                baseView.checkPhoneRegSuccess(result);
            }

            @Override
            public void onFail(String errorCode, String errorMsg) {
                baseView.showProgress(false);
                baseView.checkPhoneError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void checkForgetPd(String callingCode, String phone, String type) {
        if(baseView == null){
            return;
        }
        baseView.showProgress(true);
        AppClient.getInstance().checkForgetPd(callingCode, phone,type, new LifecycleMVPResultCallback<CheckPhoneEntity>(baseView,true) {
            @Override
            public void onSuccess(CheckPhoneEntity result) {
                baseView.showProgress(false);
                baseView.checkPhoneRegSuccess(result);
            }

            @Override
            public void onFail(String errorCode, String errorMsg) {
                baseView.showProgress(false);
                baseView.checkPhoneError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(RegisterPhoneCheckContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
