package cn.swiftpass.wallet.tiqmo.module.addmoney.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Optional;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanBaseActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.googlepay.PaymentsUtil;
import cn.swiftpass.wallet.tiqmo.module.addmoney.presenter.AddMoneyPresenter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TokenDataEntity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CardManageActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanQrActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.HomeBottomView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.GooglePayConfirmDialog;

public class AddMoneyFragment extends BaseFragment<AddMoneyContract.Presenter> implements AddMoneyContract.View {

    public static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;

    private static final long SHIPPING_COST_CENTS = 90 * PaymentsUtil.CENTS_IN_A_UNIT.longValue();
    private static final String TAG = AddMoneyFragment.class.getSimpleName();

    @BindView(R.id.homeview)
    HomeBottomView homeview;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.add_money_bg)
    ConstraintLayout addMoneyBg;
    @BindView(R.id.tv_transfer_title)
    TextView tvTransferTitle;
    @BindView(R.id.tv_transfer_desc)
    TextView tvTransferDesc;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.ll_mada_card)
    LinearLayout llMadaCard;
    @BindView(R.id.ll_google_pay)
    LinearLayout llGooglePay;
    @BindView(R.id.ll_card)
    LinearLayout llCard;
    @BindView(R.id.ll_add_money)
    LinearLayout llAddMoney;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_mada_card)
    ImageView ivMadaCard;
    @BindView(R.id.tv_mada_card_title)
    TextView tvMadaCardTitle;
    @BindView(R.id.tv_google_card_title)
    TextView tvGoogleCardTitle;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_add_money)
    ImageView ivAddMoney;
    @BindView(R.id.iv_add_money_arr)
    ImageView ivAddMoneyArr;

    private static final int AMOUNT_100 = 100;
    private static final int AMOUNT_200 = 200;
    private static final int AMOUNT_1000 = 1000;
    private static final int AMOUNT_2000 = 2000;

    private BottomDialog editMoneyDialog;
    private double money, limitMoney;
    private double minLimitMoney;

    private boolean isBankAdd;
    private boolean isGooglePay;
    private String shareIbanNum;

    private boolean isFromActivity;

    private UserInfoEntity userInfoEntity;

    private PaymentsClient paymentsClient;

    private TransFeeEntity transFeeEntity;

    private GooglePayConfirmDialog googlePayConfirmDialog;

    private double maxLimitBalance;

    public static AddMoneyFragment getInstance(boolean isFromActivity) {
        AddMoneyFragment addMoneyFragment = new AddMoneyFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("isFromActivity", isFromActivity);
        addMoneyFragment.setArguments(bundle);
        return addMoneyFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_add_money;
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, headCircle, ivAddMoneyArr,ivBack);
        if (getArguments() != null) {
            isFromActivity = getArguments().getBoolean("isFromActivity");
            if (isFromActivity) {
                homeview.setVisibility(View.GONE);
                ivBack.setVisibility(View.VISIBLE);
                ivBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    }
                });
            } else {
                ivBack.setVisibility(View.GONE);
            }
        }
        paymentsClient = PaymentsUtil.createPaymentsClient(mActivity);
        //判断手机是否支持Google Pay
        possiblyShowGooglePayButton();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void possiblyShowGooglePayButton() {
//        llGooglePay.setEnabled(false);
        final Optional<JSONObject> isReadyToPayJson = PaymentsUtil.getIsReadyToPayRequest();
        if (!isReadyToPayJson.isPresent()) {
            return;
        }
        IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(isReadyToPayJson.get().toString());
        Task<Boolean> task = paymentsClient.isReadyToPay(request);
        task.addOnCompleteListener(mActivity,
                new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
//                            llGooglePay.setVisibility(task.getResult() ? View.VISIBLE : View.INVISIBLE);
//                            llGooglePay.setEnabled(task.getResult() ? true : false);
                        } else {
                            Log.w("isReadyToPay failed", task.getException());
                        }
                    }
                });
    }

    @Override
    protected void initData() {
        noticeThemeChange();
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkUser();
        if (homeview != null) {
            homeview.setMoneyHide();
            homeview.setBottomListener(new HomeBottomView.BottomListener() {
                @Override
                public void scanClick() {
                    if (isKyc(Constants.KYC_TYPE_ADD_MONEY_SCAN)) {
                        HashMap<String, Object> mHashMap = new HashMap<>(16);
                        mHashMap.put(Constants.isFrom, "home");
                        ActivitySkipUtil.startAnotherActivity(mActivity, ScanQrActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }

                @Override
                public void kycClick() {
                    if (userInfoEntity.isKycChecking()) {
                        ActivitySkipUtil.startAnotherActivity(mActivity, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                    } else {
                        ReferralCodeDialogUtils.showReferralCodeDialog(AddMoneyFragment.this, mActivity);
                    }
                }
            });
        }
    }

    @Override
    protected void restart() {
//        checkUser();
        getBalance();
        if (homeview != null) {
            homeview.setMoneyHide();
        }
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(addMoneyBg, R.attr.splash_bg_color);
        helper.setBackgroundColorByAttr(tvLine, R.attr.color_30white_0d000000);
        helper.setBackgroundResourceByAttr(llMadaCard, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llGooglePay, R.attr.bg_051446_f4f6f9);
        helper.setBackgroundResourceByAttr(llAddMoney, R.attr.bg_051446_f4f6f9);
        helper.setImageBgResourceByAttr(headCircle, R.attr.icon_circle);
        helper.setImageBgResourceByAttr(ivMadaCard, R.attr.icon_mada_logo);
        helper.setImageBgResourceByAttr(ivAddMoney, R.attr.transfer_add_money_bank_account);
        helper.setImageBgResourceByAttr(ivAddMoneyArr, R.attr.icon_right_arrow);

        helper.setTextColorByAttr(tvTransferTitle, R.attr.wallet_setting_text_color);
        helper.setTextColorByAttr(tvTransferDesc, R.attr.color_a3a3a3_a3a3a3);
        helper.setTextColorByAttr(tvMadaCardTitle, R.attr.add_money_card_tc);
        helper.setTextColorByAttr(tvGoogleCardTitle, R.attr.add_money_card_tc);
        helper.setTextColorByAttr(tvAddMoney, R.attr.transfer_item_text_color);

    }

    private void checkUser() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        homeview.setUserInfo(userInfoEntity);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ADD_MONEY_SCAN == event.getEventType()) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            mHashMap.put(Constants.isFrom, "home");
            ActivitySkipUtil.startAnotherActivity(mActivity, ScanQrActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (homeview != null) {
                homeview.setUserInfo(userInfoEntity);
                homeview.setMoneyHide();
            }
        }
    }

    private void showAddMoneyDialog(TransferLimitEntity transferLimitEntity) {
        editMoneyDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_add_money, null);
        TextView tvLimit = contentView.findViewById(R.id.tv_limit);
        TextView tvAdd = contentView.findViewById(R.id.tv_add);
        TextView tvMoneyCurrency = contentView.findViewById(R.id.tv_money_currency);
        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        EditTextWithDel etAmount = contentView.findViewById(R.id.et_amount);
        TextView tv100 = contentView.findViewById(R.id.tv_100);
        TextView tv200 = contentView.findViewById(R.id.tv_200);
        TextView tv1000 = contentView.findViewById(R.id.tv_1000);
        TextView tv2000 = contentView.findViewById(R.id.tv_2000);
        tv100.setText(String.valueOf(AMOUNT_100));
        tv200.setText(String.valueOf(AMOUNT_200));
        tv1000.setText(String.valueOf(AMOUNT_1000));
        tv2000.setText(String.valueOf(AMOUNT_2000));
        String minLimit = transferLimitEntity.tranMinLimitAmt;
        minLimitMoney = 5;
        try {
            minLimitMoney = AndroidUtils.getTransferMoneyNumber(minLimit);
        } catch (Exception e) {
            LogUtils.d(TAG, "---"+e+"---");
        }
        String monthLimit = transferLimitEntity.monthLimitAmt;
        tvLimit.setText(Spans.builder().text(AndroidUtils.getTransferMoney(monthLimit)).size(12)
                .text(" " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode())).size(10).build());
        try {
            limitMoney = AndroidUtils.getTransferMoneyNumber(monthLimit);
            maxLimitBalance = AndroidUtils.getTransferMoneyNumber(transferLimitEntity.maxLimitBalance);
        } catch (Exception e) {
            limitMoney = Integer.MAX_VALUE;
        }

        //两位小数过滤
        etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                if (TextUtils.isEmpty(amount)) {
                    tvMoneyCurrency.setVisibility(View.GONE);
                    tvAdd.setEnabled(false);
                } else {
                    try {
                        money = Double.parseDouble(amount);
                        if (money >= minLimitMoney && money <= limitMoney && !userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                            tvAdd.setEnabled(true);
                        } else {
                            tvAdd.setEnabled(false);
                        }
                    } catch (Exception e) {
                        tvAdd.setEnabled(false);
                    }
                    if (money != AMOUNT_100) {
                        tv100.setSelected(false);
                    }
                    if (money != AMOUNT_200) {
                        tv200.setSelected(false);
                    }
                    if (money != AMOUNT_1000) {
                        tv1000.setSelected(false);
                    }
                    if (money != AMOUNT_2000) {
                        tv2000.setSelected(false);
                    }
                    tvMoneyCurrency.setVisibility(View.VISIBLE);
                }
            }
        });

        etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (!hasFocus) {
                    checkAmount(etAmount, minLimit);
                }
            }
        });

        tv100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv100.setSelected(true);
                tv200.setSelected(false);
                tv1000.setSelected(false);
                tv2000.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_100));
                checkAmount(etAmount, minLimit);
            }
        });

        tv200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv200.setSelected(true);
                tv100.setSelected(false);
                tv1000.setSelected(false);
                tv2000.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_200));
                checkAmount(etAmount, minLimit);
            }
        });

        tv1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1000.setSelected(true);
                tv200.setSelected(false);
                tv100.setSelected(false);
                tv2000.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_1000));
                checkAmount(etAmount, minLimit);
            }
        });

        tv2000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv2000.setSelected(true);
                tv200.setSelected(false);
                tv1000.setSelected(false);
                tv100.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_2000));
                checkAmount(etAmount, minLimit);
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                String money = etAmount.getEditText().getText().toString().trim();
                money = money.replace(",", "");
                if (isGooglePay) {
                    mPresenter.getTransferFee(Constants.TYPE_ADDMONEY, AndroidUtils.getReqTransferMoney(money), UserInfoManager.getInstance().getCurrencyCode());
                } else {
                    HashMap<String, Object> hashMap = new HashMap<>(16);
                    hashMap.put(Constants.ADD_MONEY, money);
                    hashMap.put(Constants.CARD_FROM, Constants.CARD_FROM_ADD_MONEY);
                    ActivitySkipUtil.startAnotherActivity(mActivity, CardManageActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    if (editMoneyDialog != null) {
                        editMoneyDialog.dismiss();
                    }
                }
            }
        });

        editMoneyDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        editMoneyDialog.setContentView(contentView);
        Window dialogWindow = editMoneyDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        editMoneyDialog.showWithBottomAnim();
    }

    private void showGooglePayDialog(TransFeeEntity transFeeEntity) {
        googlePayConfirmDialog = new GooglePayConfirmDialog(mContext, transFeeEntity);
        googlePayConfirmDialog.setPayMoneyListener(new GooglePayConfirmDialog.PayMoneyListener() {
            @Override
            public void payMoney(String money) {
                requestPayment(money);
            }
        });
        googlePayConfirmDialog.showWithBottomAnim();
    }

    public void requestPayment(String money) {
        // The price provided to the API should include taxes and shipping.
        // This price is not displayed to the user.
        try {
            Optional<JSONObject> paymentDataRequestJson = PaymentsUtil.getPaymentDataRequest(money);
            if (!paymentDataRequestJson.isPresent()) {
                return;
            }

            PaymentDataRequest request =
                    PaymentDataRequest.fromJson(paymentDataRequestJson.get().toString());

            // Since loadPaymentData may show the UI asking the user to select a payment method, we use
            // AutoResolveHelper to wait for the user interacting with it. Once completed,
            // onActivityResult will be called with the result.
            if (request != null) {
                AutoResolveHelper.resolveTask(
                        paymentsClient.loadPaymentData(request),
                        mActivity, LOAD_PAYMENT_DATA_REQUEST_CODE);
            }

        } catch (Exception e) {
            throw new RuntimeException("The price cannot be deserialized from the JSON object.");
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        switch (requestCode) {
            // value passed in AutoResolveHelper
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        handlePaymentSuccess(paymentData);
                        break;

                    case Activity.RESULT_CANCELED:
                        // The user cancelled the payment attempt
                        break;

                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        handleError(status.getStatusCode());
                        break;
                }
        }
    }

    private void handleError(int statusCode) {

    }

    private void handlePaymentSuccess(PaymentData paymentData) {
        /**
         * {"apiVersionMinor":0,"apiVersion":2,
         * "paymentMethodData":{"description":"Visa •••• 1111","tokenizationData":{"type":"PAYMENT_GATEWAY","token":"{\"signature\":\"MEUCIDO8dtqfvmiu88aBrbOc9RoiJ+5WQJPNHgMb4q9vLRLLAiEAxKOuXqM8aKMM5\/adyPB1\/Sb0c854vzmionpY0YO7dI0\\u003d\",\"protocolVersion\":\"ECv1\",
         * \"signedMessage\":\"{\\\"encryptedMessage\\\":\\\"jwh51FoJQhLQQ0Z+aNrQRcSFCojbCIUe5aG7M7UvewzD6OR\/0AHP9doGXhuwRmGZWuOJ+ZCjTEdd1cpuDsb+heYY92ACltpxSAgi\/159BLQluVa86Oj9tcXpqM84o62HEQ3NgrEAfgvn7I+oq1CY3Q9\/hlTllVHHCSu5JkMXhFrDpndsDqOQ28PfLLoxDIfKD9uFzjgkWUh3MKXIrth\/jw10lDW0kJ7UpixQChy3cLozZtpvgO\/PirZwINB0VsU+afX+OQcflEkJrHTYtLa23\/iB7Khsd0nHMwzCbAG4TUrkyXv7q4ySkyA85gIPFhlvbtJ76MMXxFLK76oxbm09B3k3R5NDFBGybXxlN5xKnBvU4A7amB8A5gXGJc0iDIsnbDipmBTMmOhGGJo2RN2rPUuRKgW\/CepwmVtRT223eHFFE2SUfIFTxqu6vqyyeip\/zjhEXiqWqiHdBdx0GgMObiyw\/aIYkUUNPZc5QPdURuiwJxEVkW2TCKG+Hvdg0LxWQ7qz8HhAf\/XCi4NSJhNlBcH8DVE0HU5btUZKnpUp7gW6aaWINeVWcOzFTDG2TP6+iixghZq2g+twrcXp18P9eK2GAtu+WnghneJYa7aET5MxXdVMzAFrdXnnqg\\\\u003d\\\\u003d\\\",\\\"ephemeralPublicKey\\\":\\\"BEIyd02QqGLcGi7RfN1LTGRhf3YYk1FKlxyGetJpWi6VQHvBA\/b3B+TrfKS5wAr1z0nriPq1tEPM95mHTg3K0dI\\\\u003d\\\",\\\"tag\\\":\\\"bvenR4yL7OEgBut6Ilt\/wgVs6GOFgC89aVlNUYr83xw\\\\u003d\\\"}\"}"},
         * "type":"CARD","info":{"cardNetwork":"VISA","cardDetails":"1111",
         * "billingAddress":{"address3":"","sortingCode":"","address2":"","countryCode":"US","address1":"1600 Amphitheatre Parkway","postalCode":"94043","name":"Card Holder Name","locality":"Mountain View","administrativeArea":"CA"}}},
         * "shippingAddress":{"address3":"","sortingCode":"","address2":"","countryCode":"US","address1":"1600 Amphitheatre Parkway1","postalCode":"94043","name":"US User","locality":"Mountain View","administrativeArea":"CA"}}
         */

        final String paymentInfo = paymentData.toJson();
        if (paymentInfo == null) {
            return;
        }

        try {
            JSONObject paymentMethodData = new JSONObject(paymentInfo).getJSONObject("paymentMethodData");
            // If the gateway is set to "example", no payment information is returned - instead, the
            // token will only consist of "examplePaymentMethodToken".

            final JSONObject tokenizationData = paymentMethodData.getJSONObject("tokenizationData");
            final String tokenizationType = tokenizationData.getString("type");
            final String token = tokenizationData.getString("token");
            final JSONObject tokenObject = new JSONObject(token);
            String signature = tokenObject.getString("signature");
            String apiVersion = tokenObject.getString("protocolVersion");
            String signedMessage = tokenObject.getString("signedMessage");

            TokenDataEntity tokenDataEntity = new TokenDataEntity();
            tokenDataEntity.data = signedMessage;
            tokenDataEntity.signature = signature;
            tokenDataEntity.version = apiVersion;

            if ("PAYMENT_GATEWAY".equals(tokenizationType) && "examplePaymentMethodToken".equals(token)) {
//                new AlertDialog.Builder(this)
//                        .setTitle("Warning")
//                        .setMessage(getString(R.string.gateway_replace_name_example))
//                        .setPositiveButton("OK", null)
//                        .create()
//                        .show();
            }

            if (transFeeEntity != null) {
                mPresenter.googlePay(Constants.PAY_TYPE_GooglePay, transFeeEntity.getOrderAmount(), UserInfoManager.getInstance().getCurrencyCode(), tokenDataEntity,
                        AndroidUtils.getReqTransferMoney(transFeeEntity.getDiscountTransFees()), AndroidUtils.getReqTransferMoney(transFeeEntity.getDiscountVat()));
            }

            final JSONObject info = paymentMethodData.getJSONObject("info");
            final String billingName = info.getJSONObject("billingAddress").getString("name");

            // Logging token string.
            Log.d("Google Pay token: ", token);

        } catch (JSONException e) {
            throw new RuntimeException("The selected garment cannot be parsed from the list of elements");
        }
    }

    private void checkAmount(EditTextWithDel etAmount, String minLimit) {
        String amount = etAmount.getEditText().getText().toString().trim();
        amount = amount.replace(",", "");
        String formatAmount = AmountUtil.dataFormat(amount);
        if (!TextUtils.isEmpty(amount)) {
            etAmount.getEditText().setText(formatAmount);
            if (money > limitMoney) {
                etAmount.showErrorViewWithMsg(getString(R.string.add_money_12));
            } else if (money < minLimitMoney) {
                etAmount.showErrorViewWithMsg(getString(R.string.add_money_13) + " " +
                        AndroidUtils.getTransferMoney(minLimit) + " " +
                        LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
            } else if (userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                etAmount.showErrorViewWithMsg(getString(R.string.newhome_45));
            }
        }
    }

    @OnClick({R.id.ll_mada_card, R.id.ll_google_pay, R.id.ll_add_money})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.ll_mada_card:
                isBankAdd = false;
                isGooglePay = false;
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
            case R.id.ll_google_pay:
                isBankAdd = false;
                isGooglePay = true;
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
            case R.id.ll_add_money:
                isBankAdd = true;
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
            default:
                break;
        }
    }

    private void showIbankDetailDialog() {
        BottomDialog bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_ibank_detail, null);
        TextView tvBankName = contentView.findViewById(R.id.tv_bank_name);
        TextView tvBankNumber = contentView.findViewById(R.id.tv_bank_number);
        LinearLayout llShare = contentView.findViewById(R.id.ll_share);
        LinearLayout llCopy = contentView.findViewById(R.id.ll_copy);
        LinearLayout llContent = contentView.findViewById(R.id.ll_content);

        if (userInfoEntity != null) {
            tvBankName.setText(userInfoEntity.accountName);
            String ibanNum = userInfoEntity.ibanNum;
            if (!TextUtils.isEmpty(ibanNum)) {
                shareIbanNum = AndroidUtils.changeBanNum(ibanNum);
                tvBankNumber.setText(shareIbanNum);
            }
        }

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downShareImage(llContent, shareIbanNum);
            }
        });

        llCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(-1, 2500)) {
                    return;
                }
                String bankNumber = tvBankNumber.getText().toString().trim();
                AndroidUtils.setClipboardText(mContext, bankNumber, true);
                showTipDialog(getString(R.string.iban_copy));
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    private void showIbankOneDialog(TransferLimitEntity transferLimitEntity) {
        BottomDialog bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_ibank_money, null);
        TextView tvConfirm = contentView.findViewById(R.id.tv_confirm);
        TextView tvTwoContent = contentView.findViewById(R.id.tv_two_content);

        String minLimit = transferLimitEntity.tranMinLimitAmt;
        String monthLimit = transferLimitEntity.monthLimitAmt;
        double monthLimitMoney = 0, maxBanlanceLimit = 0;
        minLimitMoney = 5;
        try {
            minLimitMoney = AndroidUtils.getTransferMoneyNumber(minLimit);
            monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimit);
            maxBanlanceLimit = AndroidUtils.getTransferMoneyNumber(transferLimitEntity.maxLimitBalance) - userInfoEntity.getAvalibleBalanceMoney();

            String limitMsg = getString(R.string.add_money_34);
            if (monthLimitMoney > maxBanlanceLimit) {
                limitMsg = getString(R.string.newhome_40);
            }
            tvTwoContent.setText(Spans.builder()
                    .text(limitMsg).color(mContext.getColor(R.color.color_a3a3a3)).size(14)
                    .text(" " + AmountUtil.dataFormat(String.valueOf(Math.min(monthLimitMoney, maxBanlanceLimit))) + " " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode())).color(mContext.getColor(R.color.color_fc4f00)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(". " + getString(R.string.add_money_35)).color(mContext.getColor(R.color.color_a3a3a3)).size(14)
                    .text(" " + "7.00" + " " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()) + " ").color(mContext.getColor(R.color.color_fc4f00)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(getString(R.string.add_money_36)).color(mContext.getColor(R.color.color_a3a3a3)).size(14)
                    .build());
        } catch (Exception e) {
            LogUtils.d(TAG, "---"+e+"---");
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomDialog != null) {
                    bottomDialog.dismiss();
                }
                showIbankDetailDialog();
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        SpUtils.getInstance().setAvailableBalance(transferLimitEntity.availableBalance);
        if (isBankAdd) {
            showIbankOneDialog(transferLimitEntity);
        } else {
            showAddMoneyDialog(transferLimitEntity);
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void googlePaySuccess(CardNotifyEntity cardNotifyEntity) {
        if (googlePayConfirmDialog != null) {
            googlePayConfirmDialog.dismiss();
        }
        if (editMoneyDialog != null) {
            editMoneyDialog.dismiss();
        }
        if (cardNotifyEntity != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            cardNotifyEntity.subject = Constants.SUBJECT_ADD_MONEY;
            mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            ActivitySkipUtil.startAnotherActivity(mActivity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        }
    }

    @Override
    public void googlePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {
        if (transFeeEntity != null) {
            this.transFeeEntity = transFeeEntity;
            showGooglePayDialog(transFeeEntity);
        }
    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getCardListSuccess(AllCardEntity allCardEntity) {

    }

    @Override
    public void getIbanActivityDetailSuccess(IbanBaseActivityEntity result) {

    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {

    }

    @Override
    public AddMoneyContract.Presenter getPresenter() {
        return new AddMoneyPresenter();
    }
}
