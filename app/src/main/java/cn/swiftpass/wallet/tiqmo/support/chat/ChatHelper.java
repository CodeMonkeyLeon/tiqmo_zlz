package cn.swiftpass.wallet.tiqmo.support.chat;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.gc.gcchat.GcChatConnectionStatus;
import com.gc.gcchat.GcChatError;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatUpdateUserProfileRequest;
import com.gc.gcchat.GcChatUser;
import com.gc.gcchat.callback.ConnectionCallback;
import com.gc.gcchat.callback.CreateOneToOneChatCallback;
import com.gc.gcchat.callback.GcChatRestoreBackupCallback;
import com.gc.gcchat.callback.GcChatSimpleCallback;
import com.gc.gcchat.callback.GetUnReadMessageCountCallback;
import com.gc.gcchat.callback.InitCallback;

import java.nio.charset.StandardCharsets;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class ChatHelper {
    private ChatHelper() {
    }

    private static class ChatHolder {
        public static ChatHelper chatHelper = new ChatHelper();
    }

    public static ChatHelper getInstance() {
        return ChatHelper.ChatHolder.chatHelper;
    }

    /**
     * 登录并初始化聊天sdk  成功后获取历史聊天记录
     * 用户登陆Tiqmo App时，需调用init接口启动聊天功能，并调用restoreBackupData接口获取历史聊天记录。
     * 如用户的 APP保持登陆状态，未被杀掉，聊天功能启用中，历史聊天记录还在，则不需要重新调用init和restoreBackupData.
     */
    public void loginChat(Context context, String phone){
        if(TextUtils.isEmpty(phone))return;
        String jwtToken = getJwtTokenRT(phone);
        GcChatSDK.init(context, new InitCallback() {
            @Override
            public void onSuccessful() {
                GcChatSDK.checkIfRestoreRequired(jwtToken, new GcChatRestoreBackupCallback() {
                    @Override
                    public void onSuccessful(boolean isBackupAvailable) {
                        if (isBackupAvailable) {
                            //APP保持登录状态 聊天功能启用中则直接登录
                        } else {
                            GcChatSDK.connect(jwtToken, new ConnectionCallback() {
                                @Override
                                public void onConnected(@NonNull GcChatUser gcChatUser) {
                                    GcChatSDK.getUnreadSessionCount(new GetUnReadMessageCountCallback() {
                                        @Override
                                        public void onSuccessful(int i) {

                                        }

                                        @Override
                                        public void onFailure(int errorCode) {
                                        }
                                    });
                                }

                                @Override
                                public void onConnectionUpdate(int i) {
                                    if (i == GcChatConnectionStatus.CONNECTED) {
                                    } else if (i == GcChatConnectionStatus.RECONNECTING) {
                                    } else if (i == GcChatConnectionStatus.DISCONNECTED) {
                                    }
                                }


                                @Override
                                public void onDisconnected(int i) {
                                    if (i == GcChatError.INTERNET_ISSUE
                                            || i == GcChatError.AUTHENTICATION) {
                                    } else if (i == GcChatError.BACKUP_AVAILABLE) {
                                    } else {
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(int errorCode) {
                    }
                });
            }

            @Override
            public void onFailure(int errorCode) {
            }
        });
    }

    public String getJwtTokenRT(String uniqueId) {
//        return uniqueId;
        //to be done at server end
        UserUtils.loggedInUserId = uniqueId;
        UserUtils.loggedInUserType = "phoneNumber";
//        UserUtils.loggedInUserType = "userId";

        String currentUserId = UserUtils.loggedInUserId;
        String loggedInUserType = UserUtils.loggedInUserType;
        return Jwts.builder()
                .claim("externalId", currentUserId)
                .claim("externalType", loggedInUserType)
                .claim("name", currentUserId)
                .claim("userId", currentUserId)
                .claim("userType", loggedInUserType)
                .claim("iat", System.currentTimeMillis() / 1000)
                .claim("exp", (System.currentTimeMillis() / 1000) + 24 * 3600)
                .signWith(SignatureAlgorithm.HS256, "N#(*@)@@@ilndjjclmiwe".getBytes(StandardCharsets.UTF_8))
                .compact();
    }


    /**
     * 更新chatSdk用户名称和头像
     * @param avatarUrl
     * @param name
     */
    public void updateChatProfile(String avatarUrl,String name){
        GcChatSDK.updateUserProfile(
                new GcChatUpdateUserProfileRequest()
                        .setName(name)
                        .setProfilePicture(avatarUrl)
                , new GcChatSimpleCallback() {
                    @Override
                    public void onSuccessful() {
                    }

                    @Override
                    public void onFailure(int errorCode) {
                    }
                }
        );
    }

    /**
     * 创建一对一聊天
     * @param phoneNumber
     * @param createOneToOneChatCallback
     */
    public void createNewOneToOneChat(String phoneNumber,CreateOneToOneChatCallback createOneToOneChatCallback) {
        GcChatSDK.createNewOneToOneChat(phoneNumber,createOneToOneChatCallback);
    }

    /**
     * 获取头像路径
     */
    public String getChatUserAvatar(String fileId){
        if(TextUtils.isEmpty(fileId))return "";
        if(fileId.contains("https") || fileId.contains("http")){
            return fileId;
        }
        return GcChatSDK.getTempFileUrlFromFileId(fileId);
    }
}
