package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInterOrderEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;

public class RechargeFilterDialog extends BottomDialog{
    private Context mContext;


    private ApplyFilterListener mApplyFilterListener;

    private HashMap<Integer, String> selectedArray = new HashMap<>();
    //上传接口的国家码
    private List<String> countryCodeList = new ArrayList<>();

    public RechargeFilterDialog(Context context, List<RechargeInterOrderEntity> countryList) {
        super(context);
        this.mContext = context;
        initViews(context, countryList);
    }


    public interface ApplyFilterListener {
        void changeFilter(List<String> countryCodeList, List<RechargeInterOrderEntity> countryList);
    }

    public void setApplyFilterListener(ApplyFilterListener applyFilterListener) {
        this.mApplyFilterListener = applyFilterListener;
    }

    private void initViews(Context context, List<RechargeInterOrderEntity> countryList) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_bill_payment_filter, null);
        FlexboxLayout flCountry = mView.findViewById(R.id.fl_country);
        ScrollView scCountry = mView.findViewById(R.id.sc_country);
        ;
        TextView tvCancel = mView.findViewById(R.id.tv_cancel);
        TextView tvConfirm = mView.findViewById(R.id.tv_confirm);
        initFilterCode(flCountry, countryList);

        tvCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });

        tvConfirm.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mApplyFilterListener != null) {
                    List<RechargeInterOrderEntity> selectList = new ArrayList<>();
                    int size = countryList.size();
                    if (countryCodeList.size() == 0) {
                        selectList.addAll(countryList);
                    } else {
                        for (int i = 0; i < size; i++) {
                            RechargeInterOrderEntity payBillOrderEntity = countryList.get(i);
                            String countryCode = payBillOrderEntity.countryCode;
                            if (countryCodeList.contains(countryCode)) {
                                selectList.add(payBillOrderEntity);
                            }
                        }
                    }
//                    selectList.addAll(countryList);
                    mApplyFilterListener.changeFilter(countryCodeList, selectList);
                }
                dismiss();
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private void initFilterCode(FlexboxLayout flexboxLayout, final List<RechargeInterOrderEntity> countryList) {
        if (countryList != null && countryList.size() > 0) {
            flexboxLayout.removeAllViews();
            flexboxLayout.setVisibility(View.VISIBLE);
            flexboxLayout.setFlexDirection(FlexboxLayout.FLEX_DIRECTION_ROW);
            flexboxLayout.setFlexWrap(FlexboxLayout.FLEX_WRAP_WRAP);
            flexboxLayout.setDividerDrawable(ContextCompat.getDrawable(mContext, R.drawable.item_divider_filter));
            flexboxLayout.setShowDivider(FlexboxLayout.SHOW_DIVIDER_MIDDLE);
            List<String> allCountryList = new ArrayList<>();
            for (int i = 0; i < countryList.size(); i++) {
                final RechargeInterOrderEntity marks = countryList.get(i);
                if (marks != null) {
                    if (allCountryList.contains(marks.countryCode)) {
                        continue;
                    }
                    allCountryList.add(marks.countryCode);
                    TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_filter_text, null);
                    textView.setActivated(true);
                    textView.setText(marks.countryName);
                    textView.setTag(marks.countryCode);
                    if ("1".equals(marks.selected)) {
                        countryCodeList.add(marks.countryCode);
                    }
                    textView.setSelected("1".equals(marks.selected));
                    textView.setTextColor("1".equals(marks.selected) ? mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)) :
                            mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                    if ("1".equals(marks.selected)) {
                        selectedArray.put(i, marks.countryCode);
                    }
                    final int finalI = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if ("1".equals(marks.selected)) {
                                marks.selected = "0";
                                selectedArray.remove(finalI);
                                countryCodeList.remove(marks.countryCode);
                            } else {
                                marks.selected = "1";
                                selectedArray.put(finalI, marks.countryCode);
                                countryCodeList.add(marks.countryCode);
                            }
                            view.setSelected("1".equals(marks.selected));
                            textView.setTextColor("1".equals(marks.selected) ? mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)) :
                                    mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                        }
                    });
                    flexboxLayout.addView(textView);
                }
            }
        } else {
            flexboxLayout.setVisibility(View.GONE);
        }

    }
}
