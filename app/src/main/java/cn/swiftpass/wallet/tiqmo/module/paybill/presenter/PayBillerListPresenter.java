package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillerListPresenter implements PayBillContract.BillerListPresenter {

    PayBillContract.BillerListView billerListView;

    @Override
    public void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillerList(countryCode, billerType, billerDescription, channelCode, new LifecycleMVPResultCallback<PayBillerListEntity>(billerListView, true) {
            @Override
            protected void onSuccess(PayBillerListEntity result) {
                billerListView.getPayBillerListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billerListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getPayBillSkuList(String billerId, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillSkuList(billerId,channelCode,  new LifecycleMVPResultCallback<PayBillSkuListEntity>(billerListView, true) {
            @Override
            protected void onSuccess(PayBillSkuListEntity result) {
                billerListView.getPayBillSkuListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billerListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillerListView billerListView) {
        this.billerListView = billerListView;
    }

    @Override
    public void detachView() {
        this.billerListView = null;
    }
}
