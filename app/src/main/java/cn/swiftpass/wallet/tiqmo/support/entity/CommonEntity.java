package cn.swiftpass.wallet.tiqmo.support.entity;


public class CommonEntity extends BaseEntity {

    /**
     * return_code : SUCCESS
     * return_msg : SUCCESS
     * "content":"{"result_code":"0","result_msg":"OK","sign":"sdjf9239sdlfsd8023jlsdjfosd","data":"{"bankCard":"62212548621564224"，"cvv":"12345"}""
     */

    private String return_code;
    private String return_msg;
    private String content;

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReturn_code() {
        return return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public String getContent() {
        return content;
    }
}
