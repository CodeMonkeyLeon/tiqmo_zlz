package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.os.SystemClock;
import android.view.View;


public abstract class OnOnlySingleClickListener implements View.OnClickListener {

    private static final int MAX_CLICK_INTERVAL = 400;

    private long lastClickTime;

    public abstract void onSingleClick(View v);

    @Override
    public final void onClick(View v) {
        long nowTime = SystemClock.elapsedRealtime();
        if (nowTime - lastClickTime >= MAX_CLICK_INTERVAL) {
            lastClickTime = nowTime;
            onSingleClick(v);
        }
    }
}
