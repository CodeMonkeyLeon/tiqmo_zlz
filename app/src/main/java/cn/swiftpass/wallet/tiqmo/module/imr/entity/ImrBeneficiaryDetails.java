package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrBeneficiaryDetails extends BaseEntity {
    public String partnerNo;
    public ImrBeneficiaryDetail imrPayeeDetailInfo;

    public static class ImrBeneficiaryDetail extends BaseEntity {
        public String payeeInfoId;
        public String orgNo;
        public String payerUserId;
        public String transferDestinationCountryCode;
        public String transferDestinationCountryName;
        public String receiptMethod;
        public String receiptOrgCode;
        public String receiptOrgName;
        public String receiptOrgBranchCode;
        public String receiptOrgBranchName;
        public String payeeFullName;
        public String nickName;
        public String relationshipCode;
        public String callingCode;
        public String phone;
        public String payeeInfoCountryCode;
        public String birthPlace;
        public String birthDate;
        public String sex;
        public String cityName;
        public String districtName;
        public String poBox;
        public String buildingNo;
        public String street;
        public String idNo;
        public String idExpiry;
        public String bankAccountType;
        public String ibanNo;
        public String remark;
        public String bankAccountNo;
        public String createTime;
        public String countryLogo;
        public String payeeInfoCountryName;
        public String agentAddrIdType;
        public String bankAddrIdType;
        public String ibanPreCode;
        public String ibanLength;
        //        public String bankCode;
//        public String agentCode;
        public String countrySupportedCurrencyName;
        public String channelPayeeId;
        public String channelCode;
        public String branchId;

        //	是否支持IBAN，Y：支持，N：不支持
        public String ibanSupport;
        //是否支持ACCOUNT，Y：支持，N：不支持
        public String accountSupport;

        //收款币种代码
        public String currencyCode;
        //所属城市code
        public String cityId;
        //银行模式
        public String bankMode;
        //银行模式支持币种
        public List<String> bankSupportCurrencyCodes = new ArrayList<>();
        //机构模式
        public String agentMode;
        //机构模式支持币种
        public List<String> agentSupportCurrencyCodes = new ArrayList<>();
        /**
         *\"paymentMethodList\":[{\"paymentMode\":\"BT\",\"paymentMethodLogo\":\"\",\"paymentMethodName\":\"Bank Account\",
         * \"defaultPaymentOrgCode\":\"TZ51\",\"supportCurrencyList\":[\"TZS\"],\"channelCode\":\"TransFast\"}
         */
        //付款机构信息
        public List<ImrPaymentEntity> paymentMethodList = new ArrayList<>();
        public List<String> supportCurrencyList = new ArrayList<>();
        public ImrPaymentEntity imrPaymentEntity;
    }

}
