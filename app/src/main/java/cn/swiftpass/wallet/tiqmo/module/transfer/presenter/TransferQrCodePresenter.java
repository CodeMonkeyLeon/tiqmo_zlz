package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.ScanOrderEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class TransferQrCodePresenter implements TransferContract.TransferQrCodePresenter {

    private TransferContract.TransferQrCodeView transferQrCodeView;

    @Override
    public void getQrCode(String currencyCode) {
        AppClient.getInstance().getQrCode(currencyCode, new LifecycleMVPResultCallback<QrCodeEntity>(transferQrCodeView, true) {
            @Override
            protected void onSuccess(QrCodeEntity result) {
                transferQrCodeView.getQrCodeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferQrCodeView.getQrCodeFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getCrCode(String paymentMethodNo) {
        AppClient.getInstance().getCrCode(paymentMethodNo, new LifecycleMVPResultCallback<QrCodeEntity>(transferQrCodeView, true) {
            @Override
            protected void onSuccess(QrCodeEntity result) {
                transferQrCodeView.getCrCodeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferQrCodeView.getCrCodeFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkQrCode(String qrCode) {
        AppClient.getInstance().chenckQrCode(qrCode, new LifecycleMVPResultCallback<UserInfoEntity>(transferQrCodeView, true) {
            @Override
            protected void onSuccess(UserInfoEntity result) {
                transferQrCodeView.checkQrCodeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferQrCodeView.checkQrCodeFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(transferQrCodeView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                transferQrCodeView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferQrCodeView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOrderCode(String qrCode) {
        AppClient.getInstance().checkOrderCode(qrCode, new LifecycleMVPResultCallback<ScanOrderEntity>(transferQrCodeView, true) {
            @Override
            protected void onSuccess(ScanOrderEntity result) {
                transferQrCodeView.checkOrderCodeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferQrCodeView.checkOrderCodeFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(transferQrCodeView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                transferQrCodeView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferQrCodeView.checkOutFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(TransferContract.TransferQrCodeView transferQrCodeView) {
        this.transferQrCodeView = transferQrCodeView;
    }

    @Override
    public void detachView() {
        this.transferQrCodeView = null;
    }
}
