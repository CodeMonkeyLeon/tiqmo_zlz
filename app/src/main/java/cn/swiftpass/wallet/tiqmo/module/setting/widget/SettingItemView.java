package cn.swiftpass.wallet.tiqmo.module.setting.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;

public class SettingItemView extends FrameLayout {

    public ImageView ivSetting;
    public ImageView ivRightArrow;
    private TextView tvSetting;
    private FrameLayout flSettingItem;
    public LinearLayout flSettingItemSub;
    private Context mContext;

    private OnItemClick onItemClickListener;

    public void setOnItemClickListener(OnItemClick itemClickListener) {
        this.onItemClickListener = itemClickListener;
    }

    public void changeTheme(int attr) {
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setImageResourceByAttr(ivSetting, attr);
        helper.setImageResourceByAttr(ivRightArrow, R.attr.icon_right_arrow);
        helper.setTextColorByAttr(tvSetting, R.attr.wallet_setting_item_text);
        helper.setBackgroundResourceByAttr(flSettingItemSub, R.attr.setting_item_bg);
    }

    public interface OnItemClick {
        void onItemClick();
    }

    public SettingItemView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public SettingItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SettingItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.item_setting_normal, this, true);
        ivSetting = findViewById(R.id.iv_setting);
        tvSetting = findViewById(R.id.tv_setting);
        flSettingItem = findViewById(R.id.ll_setting_item);
        flSettingItemSub = findViewById(R.id.ll_setting_item_sub);
        ivRightArrow = findViewById(R.id.iv_right_arrow);
        LocaleUtils.viewRotationY(context, ivRightArrow);
        flSettingItem.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick();
                }
            }
        });
    }

    public void setImage(int imgId) {
        ivSetting.setImageResource(imgId);
    }

    public void setText(int textId) {
        tvSetting.setText(textId);
    }

    public void setImage(String img) {
        Glide.with(mContext)
                .load(img)
                .dontAnimate()
                .format(DecodeFormat.PREFER_RGB_565)
                .into(ivSetting);
    }

    public void setText(String text) {
        tvSetting.setText(text);
    }
}
