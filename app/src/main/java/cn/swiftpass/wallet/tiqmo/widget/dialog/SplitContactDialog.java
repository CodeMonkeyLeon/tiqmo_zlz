package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SplitCheckedAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SplitContactAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class SplitContactDialog extends BottomDialog {

    TextView tvTitle;
    RecyclerView rlContacts;
    TextView idDialog;
    SideBar idSideBar;
    ConstraintLayout conRecycler;
    LinearLayout llSplitWith;
    RecyclerView rlCheckedContacts;
    ConstraintLayout conChecked;
    TextView tvAdd;
    LinearLayout llContent;
    CustomizeEditText etPhone;
    TextView tvNoPermission;
    TextView tvContacts;
    ImageView ivNoPermission;
    ConstraintLayout conInvite;
    RecyclerView rlInvite;
    private Context mContext;
    private static SplitContactDialog splitContactDialog = null;

    private SplitContactAdapter splitContactAdapter;
    private SplitCheckedAdapter splitCheckedAdapter;

    private OnAddClickListener onAddClickListener;
    private TvInviteClickListener tvInviteClickListener;

    private LinearLayoutManager mLayoutManager, mCheckedLayoutManager;

    private StatusView statusView;

    public List<KycContactEntity> phoneList = new ArrayList<>();
    public List<KycContactEntity> inviteList = new ArrayList<>();
    public List<KycContactEntity> checkedPhoneList;
    private List<KycContactEntity> filterContactList = new ArrayList<>();
    private int splitReceiversLimit;
    private boolean meChecked;

    private boolean isAdd;
    private String phone;
    private UserInfoEntity userInfoEntity;
    private boolean isSearchEmpty;
    private TextView tvContinue;


    public void setMeChecked(boolean meChecked) {
        this.meChecked = meChecked;
    }

    public void setSplitReceiversLimit(int splitReceiversLimit) {
        this.splitReceiversLimit = splitReceiversLimit;
    }

    public void setPhoneList(List<KycContactEntity> phoneList, List<KycContactEntity> peopleList) {
        this.phoneList = phoneList;
        checkedPhoneList = new ArrayList<>();
        checkedPhoneList.addAll(peopleList);
        initRecyclerView(phoneList);
    }

    public void setInviteList(List<KycContactEntity> inviteList){
        this.inviteList = inviteList;
    }

    private void initRecyclerView(List<KycContactEntity> phoneList) {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        View noContentView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ImageView iv_no_content = noContentView.findViewById(R.id.iv_no_content);
        TextView tv_no_content = noContentView.findViewById(R.id.tv_no_content);
        iv_no_content.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
        tv_no_content.setText(mContext.getString(R.string.wtw_18_1));
        statusView = new StatusView.Builder(mContext, conRecycler)
                .setNoContentView(noContentView).build();

        idSideBar.setTextView(idDialog);
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rlContacts.setLayoutManager(mLayoutManager);
        splitContactAdapter = new SplitContactAdapter(phoneList);
        splitContactAdapter.bindToRecyclerView(rlContacts);

        mCheckedLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 2f);
        rlCheckedContacts.addItemDecoration(MyItemDecoration.createHorizontal(mContext.getColor(R.color.transparent), ryLineSpace));
        rlCheckedContacts.setLayoutManager(mCheckedLayoutManager);
        splitCheckedAdapter = new SplitCheckedAdapter(checkedPhoneList);
        splitCheckedAdapter.bindToRecyclerView(rlCheckedContacts);

        if (checkedPhoneList.size() > 0) {
            conChecked.setVisibility(View.VISIBLE);
            tvAdd.setEnabled(true);
        } else {
            tvAdd.setEnabled(false);
            conChecked.setVisibility(View.GONE);
        }

        splitCheckedAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                switch (view.getId()) {
                    case R.id.iv_delete:
                        KycContactEntity kycContactEntity = splitCheckedAdapter.getDataList().get(position);
                        if (phoneList.contains(kycContactEntity)) {
                            kycContactEntity.isChecked = false;
                            splitContactAdapter.notifyDataSetChanged();
                        }
                        if (checkedPhoneList.contains(kycContactEntity)) {
                            checkedPhoneList.remove(kycContactEntity);
                        }
                        if (checkedPhoneList.size() > 0) {
                            conChecked.setVisibility(View.VISIBLE);
                            tvAdd.setEnabled(true);
                            splitCheckedAdapter.notifyDataSetChanged();
                        } else {
                            tvAdd.setEnabled(false);
                            conChecked.setVisibility(View.GONE);
                        }
                        break;

                    default:
                        break;
                }
            }
        });

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = splitContactAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (mLayoutManager != null) {
                        mLayoutManager.scrollToPositionWithOffset(position, 0);
                    }
                }
            }
        });

        splitContactAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                KycContactEntity kycContactEntity = splitContactAdapter.getDataList().get(position);
                switch (view.getId()) {
                    case R.id.tv_invite:
                        if (tvInviteClickListener != null) {
//                            if ("invited".equals(kycContactEntity.getInviteStatus())) {
//                                return;
//                            }
                            tvInviteClickListener.sendInvite(kycContactEntity.getRequestPhoneNumber());
//                            kycContactEntity.setInviteStatus("invited");
//                            splitContactAdapter.notifyItemChanged(position);
                        }
                        break;
                    case R.id.iv_avatar:
                    case R.id.tv_user_name:
                    case R.id.tv_phone:
                        if (kycContactEntity.isChecked) {
                            kycContactEntity.isChecked = false;
                            if (checkedPhoneList.contains(kycContactEntity)) {
                                checkedPhoneList.remove(kycContactEntity);
                            }
                        } else {
                            if (meChecked) {
                                if (checkedPhoneList.size() == splitReceiversLimit - 1) {
                                    return;
                                }
                            } else {
                                if (checkedPhoneList.size() == splitReceiversLimit) {
                                    return;
                                }
                            }
                            kycContactEntity.isChecked = true;
                            if (!checkedPhoneList.contains(kycContactEntity)) {
                                checkedPhoneList.add(kycContactEntity);
                            }
                        }
                        splitContactAdapter.notifyItemChanged(position);
                        if (checkedPhoneList.size() > 0) {
                            conChecked.setVisibility(View.VISIBLE);
                            tvAdd.setEnabled(true);
                            splitCheckedAdapter.notifyDataSetChanged();
                        } else {
                            tvAdd.setEnabled(false);
                            conChecked.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        });


        if (phoneList.size() == 0) {
            statusView.showEmpty();
        } else {
            statusView.dismissLoading();
        }
    }

    public void setOnAddClickListener(OnAddClickListener onAddClickListener) {
        this.onAddClickListener = onAddClickListener;
    }

    public void setTvInviteClickListener(TvInviteClickListener tvInviteClickListener){
        this.tvInviteClickListener = tvInviteClickListener;
    }

    public SplitContactDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public static SplitContactDialog getInstance(Context context) {
        splitContactDialog = new SplitContactDialog(context);
        return splitContactDialog;
    }

    public interface OnAddClickListener {
        void getSplitList(List<KycContactEntity> peopleList);
    }

    public interface TvInviteClickListener {
        void sendInvite(String phone);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_split_contact, null);

        tvTitle = view.findViewById(R.id.tv_title);
        rlContacts = view.findViewById(R.id.rl_contacts);
        idDialog = view.findViewById(R.id.id_dialog);
        idSideBar = view.findViewById(R.id.id_sideBar);
        conRecycler = view.findViewById(R.id.con_recycler);
        llSplitWith = view.findViewById(R.id.ll_split_with);
        rlCheckedContacts = view.findViewById(R.id.rl_checked_contacts);
        conChecked = view.findViewById(R.id.con_checked);
        tvAdd = view.findViewById(R.id.tv_add);
        llContent = view.findViewById(R.id.ll_content);
        tvContinue = view.findViewById(R.id.tv_continue);
        etPhone = view.findViewById(R.id.et_phone);
        tvNoPermission = view.findViewById(R.id.tv_no_permission);
        tvContacts = view.findViewById(R.id.tv_contacts);
        ivNoPermission = view.findViewById(R.id.iv_no_permission);

        etPhone.addTextChangedListener(new SimpleTextWatcher(){
            @Override
            public void afterTextChanged(Editable s) {
                phone = s.toString();
                etPhone.setError("");
                if (TextUtils.isEmpty(phone)) {
                    tvContinue.setVisibility(View.GONE);
                } else {
                    if (phone.equals(userInfoEntity.getPhone())) {
                        tvContinue.setVisibility(View.GONE);
                    } else if (RegexUtil.isPhone(phone)) {
                        tvContinue.setVisibility(View.VISIBLE);
                    } else {
                        tvContinue.setVisibility(View.GONE);
                    }
                }
                if (phoneList != null && phoneList.size() > 0) {
                    searchFilterWithAllList(phone);
                }
            }
        });

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KycContactEntity kycContactEntity = new KycContactEntity();
                if (filterContactList.size() ==1) {
                    kycContactEntity = filterContactList.get(0);
                }else {
                    kycContactEntity.setPhone(etPhone.getText().toString());
                    kycContactEntity.setContactsName(etPhone.getText().toString());
                }

                if (kycContactEntity.isChecked) {
                    kycContactEntity.isChecked = false;
                    if (checkedPhoneList.contains(kycContactEntity)) {
                        checkedPhoneList.remove(kycContactEntity);
                    }
                } else {
                    if (meChecked) {
                        if (checkedPhoneList.size() == splitReceiversLimit - 1) {
                            return;
                        }
                    } else {
                        if (checkedPhoneList.size() == splitReceiversLimit) {
                            return;
                        }
                    }
                    kycContactEntity.isChecked = true;
                    if (!checkedPhoneList.contains(kycContactEntity)) {
                        checkedPhoneList.add(kycContactEntity);
                    }
                }
                splitContactAdapter.notifyDataSetChanged();
                if (checkedPhoneList.size() > 0) {
                    conChecked.setVisibility(View.VISIBLE);
                    tvAdd.setEnabled(true);
                    splitCheckedAdapter.notifyDataSetChanged();
                } else {
                    tvAdd.setEnabled(false);
                    conChecked.setVisibility(View.GONE);
                }
                etPhone.handleEndIconClick();

            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onAddClickListener != null) {
                    isAdd = true;
                    onAddClickListener.getSplitList(checkedPhoneList);
                }
                dismiss();
            }
        });



        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (splitContactDialog != null) {
                    splitContactDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }

    }

    private void searchFilterWithAllList(String filterStr) {

        try {
            filterContactList.clear();
            //去除空格的匹配规则
            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            if (TextUtils.isEmpty(filterStr)) {
                idSideBar.setVisibility(View.VISIBLE);
                filterContactList.clear();
                tvNoPermission.setVisibility(View.GONE);
                ivNoPermission.setVisibility(View.GONE);
                rlContacts.setVisibility(View.VISIBLE);
                tvContacts.setVisibility(View.VISIBLE);
                splitContactAdapter.setDataList(phoneList);
                return;
            } else {
                idSideBar.setVisibility(View.GONE);
            }
            filterContactList.clear();
            if (phoneList != null && phoneList.size() > 0) {
                int size = phoneList.size();
                for (int i = 0; i < size; i++) {
                    KycContactEntity kycContactEntity = phoneList.get(i);
                    String name = kycContactEntity.getNameNoMapping() + kycContactEntity.getPhoneNoMapping();
                    String pinyinName = Pinyin.toPinyin(name, "");
                    String phoneNumber = kycContactEntity.getPhone();
                    if (pinyinName.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterContactList.add(kycContactEntity);
                    } else if (phoneNumber.startsWith("+966 " + filterStr)) {
                        filterContactList.add(kycContactEntity);
                    }
                }

                if (filterContactList.size() == 0) {
                    isSearchEmpty = true;
                    tvNoPermission.setVisibility(View.VISIBLE);
                    ivNoPermission.setVisibility(View.VISIBLE);
                    rlContacts.setVisibility(View.GONE);
                    tvContacts.setVisibility(View.GONE);
                } else {
                    isSearchEmpty = false;
                    tvNoPermission.setVisibility(View.GONE);
                    ivNoPermission.setVisibility(View.GONE);
                    rlContacts.setVisibility(View.VISIBLE);
                    tvContacts.setVisibility(View.VISIBLE);
                }
                splitContactAdapter.setDataList(filterContactList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
