package cn.swiftpass.wallet.tiqmo.support.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.SystemInitManager;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class SystemInitEntity extends BaseEntity {
    String otpIntervalTime;
    String idvExpireTime;
    String passCodeExpireTime;

    public int getOtpIntervalTime() {
        if (!TextUtils.isEmpty(otpIntervalTime)){
            try {
                return Integer.parseInt(otpIntervalTime);
            } catch (NumberFormatException e) {
                LogUtils.d("errorMsg", "---"+e+"---");
                return Integer.parseInt(SystemInitManager.DEFAULT_OTP_INTERVAL_TIME);
            }
        }
        return Integer.parseInt(SystemInitManager.DEFAULT_OTP_INTERVAL_TIME);
    }

    public void setOtpIntervalTime(String otpIntervalTime) {
        this.otpIntervalTime = otpIntervalTime;
    }

    public String getPassCodeExpireTime() {
        if (TextUtils.isEmpty(passCodeExpireTime)){
            return "90";
        }
        String str = "";
        try {
            double time = Double.parseDouble(passCodeExpireTime);
            if (time<0.1){
                str="90";
            }else {
                str = String.format("%.1f", time);
            }
        } catch (NumberFormatException e) {
            LogUtils.d("errorMsg", "---"+e+"---");

        }
        return str.replace(".0","");
    }

    public void setPassCodeExpireTime(String passCodeExpireTime) {
        this.passCodeExpireTime = passCodeExpireTime;
    }

    public int getIdvExpireTime() {
        if (!TextUtils.isEmpty(idvExpireTime)){
            try {
                return Integer.parseInt(idvExpireTime);
            } catch (NumberFormatException e) {
                LogUtils.d("errorMsg", "---"+e+"---");
                return Integer.parseInt(SystemInitManager.DEFAULT_IDV_EXPIRE_TIME);
            }
        }
        return Integer.parseInt(SystemInitManager.DEFAULT_IDV_EXPIRE_TIME);
    }

    public void setIdvExpireTime(String idvExpireTime) {
        this.idvExpireTime = idvExpireTime;
    }

    public SystemInitEntity(String otpIntervalTime, String idvExpireTime) {
        this.otpIntervalTime = otpIntervalTime;
        this.idvExpireTime = idvExpireTime;
    }

    public SystemInitEntity(String otpIntervalTime, String idvExpireTime, String passCodeExpireTime) {
        this.otpIntervalTime = otpIntervalTime;
        this.idvExpireTime = idvExpireTime;
        this.passCodeExpireTime = passCodeExpireTime;
    }
}
