package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import cn.swiftpass.wallet.tiqmo.module.home.contract.KycVerifyContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class KycVerifyPresenter implements KycVerifyContract.Presenter {
    KycVerifyContract.View baseView;
    private BusinessParamEntity data;

    interface OnResponseListener {
        void onResponseSuccess();
    }

    public void getData(OnResponseListener listener) {
        AppClient.getInstance().getBusinessParamList(new LifecycleMVPResultCallback<BusinessParamEntity>(baseView, true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                data = result;
                if (listener != null) {
                    listener.onResponseSuccess();
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView != null) {
                    baseView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void kycVerify(String callingCode, String phone, String residentId, String birthday,
                          String sceneType, String statesId, String citiesId, String address,
                          String employmentCode,
                          String professionCode,
                          String sourceOfFundCode,
                          String salaryRangeCode,String employerName,String beneficialOwner,
                          String relativesExposedPerson,String referralCode,
                          String shareLink) {
        AppClient.getInstance().kycVerify(callingCode, phone, residentId, birthday, sceneType, statesId,
                citiesId, address,employmentCode, professionCode, sourceOfFundCode,
                salaryRangeCode,employerName,beneficialOwner,relativesExposedPerson,referralCode, shareLink,
                new LifecycleMVPResultCallback<UserInfoEntity>(baseView, false) {
                    @Override
                    protected void onSuccess(UserInfoEntity result) {
                        if (baseView == null) {
                            return;
                        }
                        baseView.kycVerifySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (baseView == null) {
                            return;
                        }
                        baseView.kycVerifyFail(errorCode, errorMsg);
                    }
                });

    }

    @Override
    public void getCityList(String statesId) {
        AppClient.getInstance().requestCityList(statesId, new LifecycleMVPResultCallback<CityEntity>(baseView, true) {
            @Override
            protected void onSuccess(CityEntity result) {
                if (baseView == null || result == null || result.citiesInfoList == null
                        || result.citiesInfoList.size() <= 0) {
                    return;
                }
                baseView.showCityListDia(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView == null) {
                    return;
                }
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getStateList() {
        AppClient.getInstance().getUserManager().getStateList(new LifecycleMVPResultCallback<StateListEntity>(baseView, true) {
            @Override
            protected void onSuccess(StateListEntity result) {
                if (baseView == null) {
                    return;
                }
                baseView.getStateListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView == null) {
                    return;
                }
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getSourceOfFund() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    baseView.showSourceOfFund(data.sourceOfFund);
                }
            });
        } else {
            baseView.showSourceOfFund(data.sourceOfFund);
        }
    }

    @Override
    public void getProfession(String businessType,String parentBussinessCode) {
        AppClient.getInstance().getBusinessParam(businessType, parentBussinessCode, new LifecycleMVPResultCallback<BusinessParamEntity>(baseView,true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                baseView.showProfession(result.businessParamList);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getSalaryRange() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    baseView.showSalaryRange(data.annualIncome);
                }
            });
        } else {
            baseView.showSalaryRange(data.annualIncome);
        }
    }

    @Override
    public void getEmploymentSector(String businessType, String parentBussinessCode) {
        AppClient.getInstance().getBusinessParam(businessType, parentBussinessCode, new LifecycleMVPResultCallback<BusinessParamEntity>(baseView,true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                baseView.getEmploymentSectorSuccess(result.businessParamList);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(KycVerifyContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
