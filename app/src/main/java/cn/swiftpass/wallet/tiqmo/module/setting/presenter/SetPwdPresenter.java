package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.SetPwdContract;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SetPwdPresenter implements SetPwdContract.Presenter {

    private SetPwdContract.View baseView;

    @Override
    public void resetPwd(String pdType, String password, String oldPassword, String operatorType, String sceneType, String referralCode) {
        AppClient.getInstance().changePassword(pdType, password, oldPassword, operatorType, sceneType, referralCode, new LifecycleMVPResultCallback<UserInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(UserInfoEntity result) {
                baseView.resetPwdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.resetPwdFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void preCheckPd(String passwordType,String operatorType, String oldPassword) {
        AppClient.getInstance().preCheckPd(passwordType,operatorType,oldPassword, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                baseView.preCheckPdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.preCheckPdFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(SetPwdContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
