package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public class PasswordContract {

    public interface View extends BaseView<PasswordContract.Presenter> {
        void checkLoginPwdSuccess(Void result);
        void checkLoginPwdFail(String errorCode,String errorMsg);
    }

    public interface Presenter extends BasePresenter<PasswordContract.View> {
        void checkLoginPwd(String pdType,String oldPassword);
    }
}
