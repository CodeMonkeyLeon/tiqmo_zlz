package cn.swiftpass.wallet.tiqmo.module.paybill.interfaces;

import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;

public interface OnBillerListItemClickListener {
    void onBillServiceBillerListItemClick(int position, PayBillerEntity item);
}
