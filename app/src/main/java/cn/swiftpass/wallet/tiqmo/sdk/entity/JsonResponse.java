package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.text.TextUtils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.JsonAdapter;

import java.lang.reflect.Type;

import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;

public class JsonResponse<T> {
    private String code;
    private String msg;
    @JsonAdapter(JsonResponseContentDeserializer.class)
    private JsonResponseContent<T> content;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JsonResponseContent<T> getContent() {
        return content;
    }

    public void setContent(JsonResponseContent<T> content) {
        this.content = content;
    }

    public static class JsonResponseContentDeserializer implements JsonDeserializer<JsonResponseContent> {
        @Override
        public JsonResponseContent deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            if (jsonElement.isJsonPrimitive()) {
                String json = jsonElement.getAsString();
                if (!TextUtils.isEmpty(json)) {
                    return ApiHelper.GSON.fromJson(json, type);
                }
            }
            return null;
        }
    }
}
