package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.os.Parcel;
import android.os.Parcelable;

import cn.swiftpass.wallet.tiqmo.sdk.util.PinYinUtil;

public class InternationalCodeEntity implements Parcelable, Comparable<InternationalCodeEntity> {
    private String callingCode;
    private String country;
    private String nationalFlag;
    private String abbreviation;

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNationalFlag() {
        return nationalFlag;
    }

    public void setNationalFlag(String nationalFlag) {
        this.nationalFlag = nationalFlag;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.callingCode);
        dest.writeString(this.country);
        dest.writeString(this.nationalFlag);
    }

    public InternationalCodeEntity() {
    }

    protected InternationalCodeEntity(Parcel in) {
        this.callingCode = in.readString();
        this.country = in.readString();
        this.nationalFlag = in.readString();
    }

    public static final Creator<InternationalCodeEntity> CREATOR = new Creator<InternationalCodeEntity>() {
        @Override
        public InternationalCodeEntity createFromParcel(Parcel source) {
            return new InternationalCodeEntity(source);
        }

        @Override
        public InternationalCodeEntity[] newArray(int size) {
            return new InternationalCodeEntity[size];
        }
    };

    public String getAbbreviation() {
        if (abbreviation == null) {
            abbreviation = PinYinUtil.getPinYinAbbreviation(country, true);
        }
        return abbreviation;
    }

    @Override
    public int compareTo(InternationalCodeEntity o) {
        return getAbbreviation().compareTo(o.getAbbreviation());
    }
}
