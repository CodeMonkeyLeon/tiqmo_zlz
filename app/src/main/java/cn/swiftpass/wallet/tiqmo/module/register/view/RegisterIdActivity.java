package cn.swiftpass.wallet.tiqmo.module.register.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterPhoneCheckContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.RegNafathUrlPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;

public class RegisterIdActivity extends BaseCompatActivity<RegisterPhoneCheckContract.RegisterNafathPresenter> implements RegisterPhoneCheckContract.RegisterNafathView {


    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_content_title)
    TextView tvContentTitle;
    @BindView(R.id.et_id_number)
    CustomizeEditText etIdNumber;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_notice)
    TextView tvNotice;

    private String phone;
    private String otpType;

    private String callingCode;
    private int operateType;

    private String checkPhoneType;
    private CheckPhoneEntity checkPhoneEntity;

    private int maxIdLength;
    private String checkIdNumber = "";
    private String validIDNotices = "";
    private String needCheckIdNumber = "Y";

    @Override
    protected int getLayoutID() {
        return R.layout.activity_register_id;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        callingCode = UserInfoManager.getInstance().getCallingCode();
        if (getIntent() != null && getIntent().getExtras() != null) {
            otpType = getIntent().getExtras().getString(Constants.DATA_OTP_TYPE);
            if (Constants.OTP_REG.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_12);
            } else if (Constants.OTP_FORGET_LOGIN_PD.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_31);
            }
        }
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if(checkPhoneEntity != null) {
            phone = checkPhoneEntity.phone;
            checkPhoneType = checkPhoneEntity.checkPhoneType;
            operateType = checkPhoneEntity.operateType;
        }

        maxIdLength = 10;
        etIdNumber.setHint(getString(R.string.forget_ppw_5));
        validIDNotices = getString(R.string.kyc_37);
        etIdNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxIdLength),
                new NormalInputFilter(NormalInputFilter.NUMBER)});
        etIdNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkIdNumber = s.toString();
                if (TextUtils.isEmpty(checkIdNumber)) {
                    tvContinue.setEnabled(false);
                    etIdNumber.setError("");
                } else {
                    if (!checkIdNumber.startsWith("1") && !checkIdNumber.startsWith("2")) {
                        etIdNumber.setError(validIDNotices);
                    }
                    checkButtonStatus();
                }
            }
        });

        etIdNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkIdNumber = etIdNumber.getText().toString().trim();
                if (!hasFocus && !TextUtils.isEmpty(checkIdNumber)) {
                    if (checkIdNumber.length() < maxIdLength || (!checkIdNumber.startsWith("1") &&
                            !checkIdNumber.startsWith("2"))) {
                        etIdNumber.setError(validIDNotices);
                    } else {
                        etIdNumber.setError("");
                    }
                }
            }
        });

        tvNotice.setText(Spans.builder()
                .text(mContext.getString(R.string.MobileValidation_SU_0002_2_D_6)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.login_botton_notice_text_color))).size(12)
                .text(" " + mContext.getString(R.string.IMR_sendMoney_5)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.login_forgot_pwd_color))).size(12).click(tvNotice, new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        HashMap<String,Object> hashMap = new HashMap<>();
                        hashMap.put(Constants.terms_condition_type,"1");
                        ActivitySkipUtil.startAnotherActivity(RegisterIdActivity.this, WebViewActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setColor(mContext.getColor(R.color.color_fc4f00));
                        ds.setUnderlineText(false);
                    }
                }).build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(checkIdNumber) && etIdNumber != null){
            etIdNumber.setText(checkIdNumber);
            etIdNumber.setSelection(checkIdNumber.length());
        }
    }

    private void checkButtonStatus() {
        tvContinue.setEnabled(false);
        if(!checkIdNumber.startsWith("1") && !checkIdNumber.startsWith("2")){
            return;
        }
        if (!TextUtils.isEmpty(checkIdNumber) && checkIdNumber.length() == 10) {
            tvContinue.setEnabled(true);
        }
    }

    @Override
    public void checkNewDeviceSuccess(CheckPhoneEntity result) {
        if(result != null){
            checkPhoneEntity.riskControlInfo = result.riskControlInfo;
        }
        mPresenter.getNafathUrl(callingCode,phone,checkIdNumber,operateType == 2? Constants.SCENE_TYPE_FORGET_PD:Constants.SCENE_TYPE_REG_PHONE);
    }

    @Override
    public void checkPhoneRegSuccess(CheckPhoneEntity checkPhoneEntity) {
        this.checkPhoneEntity = checkPhoneEntity;
        mPresenter.getNafathUrl(callingCode,phone,checkIdNumber,operateType == 2? Constants.SCENE_TYPE_FORGET_PD:Constants.SCENE_TYPE_REG_PHONE);
    }

    @Override
    public void checkNewDeviceError(String errorCode, String errorMsg) {
        etIdNumber.setError(errorMsg);
    }

    @Override
    public void checkPhoneRegError(String errorCode, String errorMsg) {
        if("080121".equals(errorCode)){
            showNafathErrorDialog(errorCode);
        }else if("080122".equals(errorCode)){
            //id无效
            etIdNumber.setError(errorMsg);
        }else if("030208".equals(errorCode) || "030209".equals(errorCode) || "030210".equals(errorCode)){
            ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
            errorBottomDialog.setErrorMsg(mContext.getString(R.string.sprint18_2),errorMsg,ThemeSourceUtils.getSourceID(mContext,R.attr.icon_error_mobile_number));
            errorBottomDialog.showWithBottomAnim();
        }else {
            etIdNumber.setError(errorMsg);
        }
    }

    @Override
    public void getNafathUrlSuccess(NafathUrlEntity nafathUrlEntity) {
        if(nafathUrlEntity != null){
            if(checkPhoneEntity != null) {
                RiskControlEntity riskControlEntity = checkPhoneEntity.riskControlInfo;
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);

                checkPhoneEntity.phone = phone;
                checkPhoneEntity.checkIdNumber = checkIdNumber;
                checkPhoneEntity.authUrl = nafathUrlEntity.authUrl;
                checkPhoneEntity.hashedState = nafathUrlEntity.hashedState;
                checkPhoneEntity.redirectUrl = nafathUrlEntity.redirectUrl;
                checkPhoneEntity.nafathAuthFlag = nafathUrlEntity.nafathAuthFlag;
                checkPhoneEntity.firstName = nafathUrlEntity.firstName;

                UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                if (riskControlEntity != null) {
                    if (riskControlEntity.isOtp()) {
                        ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        if (!nafathUrlEntity.isSkipNafath()) {
                            ActivitySkipUtil.startAnotherActivity(this, NafathAuthActivity.class, mHashMaps, RIGHT_IN);
                        } else {
                            ActivitySkipUtil.startAnotherActivity(this, RegSetPwdOneActivity.class, mHashMaps, RIGHT_IN);
                        }
                    }
                } else {
                    if (!nafathUrlEntity.isSkipNafath()) {
                        ActivitySkipUtil.startAnotherActivity(this, NafathAuthActivity.class, mHashMaps, RIGHT_IN);
                    } else {
                        ActivitySkipUtil.startAnotherActivity(this, RegSetPwdOneActivity.class, mHashMaps, RIGHT_IN);
                    }
                }
            }
        }
    }

    @Override
    public void getNafathUrlFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public RegisterPhoneCheckContract.RegisterNafathPresenter getPresenter() {
        return new RegNafathUrlPresenter();
    }

    @OnClick({R.id.iv_back, R.id.tv_continue})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_continue:
                if(operateType == 2){
                    mPresenter.checkNewDevice(callingCode, phone, checkIdNumber, "", Constants.SCENE_TYPE_FORGET_PD,"");
                }else {
                    mPresenter.checkPhoneReg(callingCode, phone, ProjectApp.getLongitude(), ProjectApp.getLatitude(), checkPhoneType, checkIdNumber, "",needCheckIdNumber);
                }
                break;
            default:break;
        }
    }
}
