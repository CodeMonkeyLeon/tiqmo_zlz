package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class EnterEmailDialog extends BottomDialog{
    private Context mContext;

    private ContinueListener continueListener;

    private  String email;

    public void setContinueListener(ContinueListener chooseTypeListener) {
        this.continueListener = chooseTypeListener;
    }

    public EnterEmailDialog(Context context, String email) {
        super(context, R.style.DialogBottomInTransfer);
        this.mContext = context;
        initViews(mContext,email);
    }

    public interface ContinueListener {
        void clickContinue(String email);
    }

    private void initViews(Context mContext,String email) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_enter_email, null);
        TextView tvSelectTitle = view.findViewById(R.id.tv_select_title);
        TextView tvCardNumber = view.findViewById(R.id.tv_card_number);
        TextView tvCardExpire = view.findViewById(R.id.tv_card_expire);
        TextView tvContinue = view.findViewById(R.id.tv_continue);
        EditTextWithDel etEmail = view.findViewById(R.id.et_email);
        etEmail.getTlEdit().setHint(mContext.getString(R.string.sprint19_7));
        etEmail.setContentText(email);
        etEmail.setFocusableFalse();

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(continueListener != null){
                    continueListener.clickContinue(email);
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
