package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterPhoneCheckContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RegMoreAboutPresenter implements RegisterPhoneCheckContract.RegisterMorePresenter {

    private RegisterPhoneCheckContract.RegisterMoreView registerMoreView;

    @Override
    public void checkNewDevice(String callingCode, String phone, String recipientId, String birthday,String sceneType,String userId) {
        AppClient.getInstance().getUserManager().checkNewDevice(callingCode, phone, recipientId, birthday,sceneType, userId,new LifecycleMVPResultCallback<CheckPhoneEntity>(registerMoreView,true) {
            @Override
            protected void onSuccess(CheckPhoneEntity result) {
                registerMoreView.checkNewDeviceSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                registerMoreView.checkNewDeviceError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(RegisterPhoneCheckContract.RegisterMoreView registerMoreView) {
        this.registerMoreView = registerMoreView;
    }

    @Override
    public void detachView() {
        registerMoreView = null;
    }
}
