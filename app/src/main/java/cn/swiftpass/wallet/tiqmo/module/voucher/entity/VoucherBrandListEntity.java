package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class VoucherBrandListEntity extends BaseEntity {

    //代金券品牌信息
    public List<VoucherBrandEntity> queryVoucherBrandAndCountResList = new ArrayList<>();

    //代金券 最近交易列表
    public List<RecentVoucherBrandEntity> voucherRecentDealsInfoList = new ArrayList<>();
}
