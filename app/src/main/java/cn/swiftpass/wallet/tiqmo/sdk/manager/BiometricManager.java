package cn.swiftpass.wallet.tiqmo.sdk.manager;

import android.os.Build;
import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ASKeyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.FIOEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.PreVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.BiometricApi;
import cn.swiftpass.wallet.tiqmo.sdk.util.CallbackUtil;

//这个是生物识别的管理类，主要用于指纹，目前equicom项目没有使用到该功能，后续如果使用到这个功能，接口ID或参数可以会改变，要注意一下
public class BiometricManager {

    public static final String FINGERPRINT_SECRET_KEY_PAYMENT = ".payment";
    public static final String FINGERPRINT_SECRET_KEY_LOGIN = ".unlock";

    private static final String STORAGE_KEY_ENABLE_TOUCH_ID_UNLOCK = "EnableTouchIDUnlock";
    private static final String STORAGE_KEY_ENABLE_TOUCH_PAY = "EnableTouchIDPay";
    private static final String STORAGE_KEY_FIO = "FIO";

    private AppClient mAppClient;
    private BiometricApi mBiometricApi;

    public BiometricManager(AppClient client) {
        mBiometricApi = ApiHelper.getApi(BiometricApi.class);
        mAppClient = client;
    }

    //是否启动指纹登录APP，本质上就是保存一个状态到Preferences
    public void setEnableFingerprintLogin(boolean isEnable) {
         mAppClient.getStorageHelper().putToUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_ENABLE_TOUCH_ID_UNLOCK, String.valueOf(isEnable));
    }

    public boolean isEnableFingerprintLogin() {
        return String.valueOf(true).equals(mAppClient.getStorageHelper().getFromUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_ENABLE_TOUCH_ID_UNLOCK));
    }

    //是否启动指纹支付APP，本质上就是保存一个状态到Preferences
    public void setEnableFingerprintPay(boolean isEnable) {
        mAppClient.getStorageHelper().putToUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_ENABLE_TOUCH_PAY, String.valueOf(isEnable));
    }

    public boolean isEnableFingerprintPay() {
        return String.valueOf(true).equals(mAppClient.getStorageHelper().getFromUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_ENABLE_TOUCH_PAY));
    }

    //注销指纹支付功能
    public void unregisterFingerprintPayment() {
        mBiometricApi.logoutBiometric(BiometricApi.BIOMETRIC_TYPE_FINGERPRINT)
                .enqueue(new ResponseCallbackWrapper<Void>(null));
        mAppClient.getStorageHelper().putToUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_FIO, "");
    }

    public boolean isEnableFingerprintPayment() {
        return !TextUtils.isEmpty(mAppClient.getStorageHelper().getFromUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_FIO));
    }

    //注册了指纹支付功能，需要调用2个API才能完成注册
    //注册成功会返回一个key，要保存到本地，下次用指纹支付的时候就要带上这个key
    public void registerFingerprintPayment(final ResultCallback<Void> callback) {
        //获取一个asKey
        mBiometricApi.getASKey(BiometricApi.BIOMETRIC_TYPE_FINGERPRINT, Build.BRAND, Build.MODEL, "Android", String.valueOf(Build.VERSION.SDK_INT))
                .enqueue(new ResponseCallbackWrapper<>(new ResultCallback<ASKeyEntity>() {
                    @Override
                    public void onResult(ASKeyEntity result) {
                        final String asKey = result.getAskey();
                        //用asKey去获取一个叫FIO的字符串
                        mBiometricApi.getFIO(asKey)
                                .enqueue(new ResponseCallbackWrapper<>(new ResultCallback<FIOEntity>() {
                                    @Override
                                    public void onResult(FIOEntity result) {
                                        final String fio = result.getFioId();
                                        //这里拿到FIO之后就开始真正的注册指纹
                                        mBiometricApi.registerBiometric(asKey, fio)
                                                .enqueue(new ResponseCallbackWrapper<>(new ResultCallback<Void>() {
                                                    @Override
                                                    public void onResult(Void result) {
                                                        //注册成功之后将FIO保存起来，下次用指纹支付的时候就要带上这个FIO
                                                        mAppClient.getStorageHelper().putToUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_FIO, fio);
                                                        CallbackUtil.callResult(result, callback);
                                                    }

                                                    @Override
                                                    public void onFailure(String code, String error) {
                                                        onError(code, error);
                                                    }
                                                }));
                                    }

                                    @Override
                                    public void onFailure(String code, String error) {
                                        onError(code, error);
                                    }
                                }));
                    }

                    @Override
                    public void onFailure(String code, String error) {
                        onError(code, error);
                    }

                    void onError(String code, String error) {
                        CallbackUtil.callFailure(code, error, callback);
                    }
                }));
    }

    //指纹支付认证，类似验证支付密码一样，验证完之后一般就是直接给钱了
    public void verify(String preAuthId, final ResultCallback<Void> callback) {
        mBiometricApi.verify(mAppClient.getStorageHelper().getFromUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_FIO), preAuthId)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //预认证，等于提前检验指纹(FIO)是否可以使用
    public void preVerify(String type,final ResultCallback<PreVerifyEntity> callback) {
        mBiometricApi.preVerify(mAppClient.getStorageHelper().getFromUserPreferences(AppClient.getInstance().getUserID() + STORAGE_KEY_FIO), type)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void reset() {

    }
}
