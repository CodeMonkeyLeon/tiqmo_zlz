package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.os.Handler;

import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class PreAuthorizationsHistoryFragment extends BaseHistoryFragment {


    public static PreAuthorizationsHistoryFragment getInstance(String isFrom) {
        PreAuthorizationsHistoryFragment preAuthorizationsHistoryFragment = new PreAuthorizationsHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.isFrom, isFrom);
        bundle.putBoolean(Constants.HISTORY_TYPE, true);
        preAuthorizationsHistoryFragment.setArguments(bundle);
        return preAuthorizationsHistoryFragment;
    }

    @Override
    protected void initData() {
        noticeThemeChange();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mTempPreAuthHistory != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage = 1;
                    getHistoryListSuccess(mTempPreAuthHistory);
                    mTempPreAuthHistory = null;
                }
            }, 100);
        }
    }

    public void updatePreAuthListData(TransferHistoryMainEntity historyMainEntity) {
        mTempPreAuthHistory = historyMainEntity;
    }


}
