package cn.swiftpass.wallet.tiqmo.module.home.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.DownloadFileUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherDetailEntity;

public class NotificationContract {

    public interface View extends BaseView<NotificationContract.Presenter> {
        void sendNotifySuccess(PayBillOrderInfoEntity result);

        void sendNotifyFail(String errorCode, String errorMsg);

        void getHistoryDetailSuccess(TransferHistoryDetailEntity result);

        void getHistoryDetailFail(String errorCode, String errorMsg);

        void getSplitDetailListSuccess(SplitPeopleListEntity result);

        void getSplitDetailListFail(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void getVoucherDetailSuccess(EVoucherDetailEntity eVoucherDetailEntity);

        void getVoucherDetailFail(String errCode, String errMsg);

        void getDownloadFileUrlSuccess(DownloadFileUrlEntity result);

        void getDownloadFileUrlFail(String errCode, String errMsg);
    }

    public interface Presenter extends BasePresenter<NotificationContract.View> {
        void sendNotify(List<NotifyEntity> receivers);

        void getHistoryDetail(String orderNo, String orderType,String queryType);

        void getSplitDetailList(String newOrderNo);

        void getLimit(String type);

        void getVoucherDetail(String orderNo);

        void getDownloadFileUrl(String orderNo);
    }
}
