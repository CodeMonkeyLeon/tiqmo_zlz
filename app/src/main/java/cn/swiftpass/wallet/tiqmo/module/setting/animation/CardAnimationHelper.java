package cn.swiftpass.wallet.tiqmo.module.setting.animation;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import java.util.LinkedList;

import cn.swiftpass.wallet.tiqmo.module.setting.animation.adapter.StackCardAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.entry.CardItem;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultCommonTransformer;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultTransformerAdd;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultTransformerToBack;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultTransformerToFront;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultZIndexTransformerCommon;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer.DefaultZIndexTransformerToFront;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;

public class CardAnimationHelper implements Animator.AnimatorListener,
        ValueAnimator.AnimatorUpdateListener {
    static final int ANIM_DURATION = 1000, ANIM_ADD_REMOVE_DELAY = 200,
            ANIM_ADD_REMOVE_DURATION = 500;
    private int mAnimType = StackCardView.ANIM_TYPE_FRONT;
    private int mAnimDuration = ANIM_DURATION, mAnimAddRemoveDelay = ANIM_ADD_REMOVE_DELAY,
            mAnimAddRemoveDuration = ANIM_ADD_REMOVE_DURATION;
    private StackCardView mCardView;
    private LinkedList<CardItem> mCards;
    private int mCardCount;
    private CardItem mCardToBack, mCardToFront;
    private int mPositionToBack = 0, mPositionToFront = 0;
    private int mCardWidth, mCardHeight;
    private boolean mIsAnim = false, mIsAddRemoveAnim = false;
    private ValueAnimator mValueAnimator;
    private AnimationTransformer mTransformerToFront, mTransformerToBack, mTransformerCommon;
    private AnimationTransformer mTransformerAnimAdd;
    private ZIndexTransformer mZIndexTransformerToFront, mZIndexTransformerToBack, mZIndexTransformerCommon;
    private Interpolator mAnimInterpolator, mAnimAddRemoveInterpolator;
    private StackCardAdapter mTempAdapter;
    private float mCurrentFraction = 1;
    private AutoListener mAutoListener;
    private int mPosition;
    private StackCardAdapter mAdapter;
    private int mNextItemPosition = 5;
    private int mDataPosition = 0;
    private Context mContext;
    private CardClickListener cardClickListener;

    public void setCardClickListener(CardClickListener cardClickListener) {
        this.cardClickListener = cardClickListener;
    }

    CardAnimationHelper(int mAnimType, int mAnimDuration, StackCardView stackCardView) {
        this.mAnimType = mAnimType;
        this.mAnimDuration = mAnimDuration;
        this.mCardView = stackCardView;
        initTransformer();
        initAnimator();
    }

    private void initTransformer() {
        mAnimInterpolator = new LinearInterpolator();
        mAnimAddRemoveInterpolator = new LinearInterpolator();
        mTransformerToFront = new DefaultTransformerToFront();
        mTransformerToBack = new DefaultTransformerToBack();
        mTransformerCommon = new DefaultCommonTransformer();
        mTransformerAnimAdd = new DefaultTransformerAdd();
        mZIndexTransformerToFront = new DefaultZIndexTransformerToFront();
        mZIndexTransformerToBack = new DefaultZIndexTransformerCommon();
        mZIndexTransformerCommon = new DefaultZIndexTransformerCommon();
    }

    private void initAnimator() {
        if (mValueAnimator != null) {
            mValueAnimator = null;
        }
        mValueAnimator = ValueAnimator.ofFloat(0, 1).setDuration(mAnimDuration);
        mValueAnimator.addUpdateListener(this);
        mValueAnimator.addListener(this);
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        mCurrentFraction = (float) animation.getAnimatedValue();
        float fractionInterpolated = mCurrentFraction;
        if (mAnimInterpolator != null) {
            fractionInterpolated = mAnimInterpolator.getInterpolation(mCurrentFraction);
        }
        doAnimationBackToFront(mCurrentFraction, fractionInterpolated);
        doAnimationFrontToBack(mCurrentFraction, fractionInterpolated);
        doAnimationCommon(mCurrentFraction, fractionInterpolated);
        bringToFrontByZIndex();

    }


    private void doAnimationBackToFront(float fraction, float fractionInterpolated) {
        mTransformerToFront.transformAnimation(mCardToFront.view,
                fraction, mCardWidth, mCardHeight, mPositionToFront, 0);
        if (mAnimInterpolator != null) {
            mTransformerToFront.transformInterpolatedAnimation(mCardToFront.view,
                    fractionInterpolated, mCardWidth, mCardHeight, mPositionToFront, 0);
        }
        doAnimationZIndex(mZIndexTransformerToFront, mCardToFront, fraction, fractionInterpolated,
                mPositionToFront, 0);
    }


    private void doAnimationFrontToBack(float fraction, float fractionInterpolated) {
        if (mAnimType == StackCardView.ANIM_TYPE_FRONT) {
            return;
        }
        mTransformerToBack.transformAnimation(mCards.getFirst().view, fraction, mCardWidth,
                mCardHeight, 0, mPositionToBack);
        if (mAnimInterpolator != null) {
            mTransformerToBack.transformInterpolatedAnimation(mCards.getFirst().view,
                    fractionInterpolated, mCardWidth, mCardHeight, 0, mPositionToBack);
        }
        doAnimationZIndex(mZIndexTransformerToBack, mCards.getFirst(), fraction, fractionInterpolated,
                0, mPositionToBack);
    }

    private void doAnimationCommon(float fraction, float fractionInterpolated) {
        if (mAnimType == StackCardView.ANIM_TYPE_FRONT) {
            for (int i = 0; i < mPositionToFront; i++) {
                CardItem card = mCards.get(i);
                doAnimationCommonView(card.view, fraction, fractionInterpolated, i, i + 1);
                doAnimationZIndex(mZIndexTransformerCommon, card, fraction, fractionInterpolated,
                        i, i + 1);
            }
        } else if (mAnimType == StackCardView.ANIM_TYPE_FRONT_TO_LAST) {
            for (int i = mPositionToFront + 1; i < mCardCount; i++) {
                CardItem card = mCards.get(i);
                doAnimationCommonView(card.view, fraction, fractionInterpolated, i, i - 1);
                doAnimationZIndex(mZIndexTransformerCommon, card, fraction, fractionInterpolated,
                        i, i - 1);
            }
        }
    }

    private void doAnimationCommonView(View view, float fraction, float fractionInterpolated, int
            fromPosition, int toPosition) {
        mTransformerCommon.transformAnimation(view, fraction, mCardWidth,
                mCardHeight, fromPosition, toPosition);
        if (mAnimInterpolator != null) {
            mTransformerCommon.transformInterpolatedAnimation(view, fractionInterpolated, mCardWidth,
                    mCardHeight, fromPosition, toPosition);
        }
    }


    private void doAnimationZIndex(ZIndexTransformer transformer, CardItem card, float fraction,
                                   float fractionInterpolated, int fromPosition, int toPosition) {
        transformer.transformAnimation(card, fraction, mCardWidth,
                mCardHeight, fromPosition, toPosition);
        if (mAnimInterpolator != null) {
            transformer.transformInterpolatedAnimation(card, fractionInterpolated, mCardWidth,
                    mCardHeight, fromPosition, toPosition);
        }
    }

    private void bringToFrontByZIndex() {
        if (mAnimType == StackCardView.ANIM_TYPE_FRONT) {
            for (int i = mPositionToFront - 1; i >= 0; i--) {
                CardItem card = mCards.get(i);
                if (card.zIndex > mCardToFront.zIndex) {
                    mCardToFront.view.bringToFront();
                    mCardView.updateViewLayout(mCardToFront.view, mCardToFront.view.getLayoutParams());
                } else {
                    card.view.bringToFront();
                    mCardView.updateViewLayout(card.view, card.view.getLayoutParams());
                }
            }
        } else {
            boolean cardToFrontBrought = false;
            for (int i = mCardCount - 1; i > 0; i--) {
                CardItem card = mCards.get(i);
                CardItem cardPre = i > 1 ? mCards.get(i - 1) : null;
                boolean cardToBackBehindCardPre = cardPre == null ||
                        mCardToBack.zIndex > cardPre.zIndex;
                boolean bringCardToBackViewToFront = mCardToBack.zIndex < card.zIndex && cardToBackBehindCardPre;
                boolean cardToFrontBehindCardPre = cardPre == null ||
                        mCardToFront.zIndex > cardPre.zIndex;
                boolean bringCardToFrontViewToFront = mCardToFront.zIndex < card.zIndex && cardToFrontBehindCardPre;
                if (i != mPositionToFront) {
                    card.view.bringToFront();
                    mCardView.updateViewLayout(card.view, card.view.getLayoutParams());
                    if (bringCardToBackViewToFront) {
                        mCardToBack.view.bringToFront();
                        mCardView.updateViewLayout(mCardToBack.view, mCardToBack.view.getLayoutParams());
                    }
                    if (bringCardToFrontViewToFront) {
                        mCardToFront.view.bringToFront();
                        mCardView.updateViewLayout(mCardToFront.view, mCardToFront.view.getLayoutParams());
                        cardToFrontBrought = true;
                    }
                    if (bringCardToBackViewToFront && bringCardToFrontViewToFront &&
                            mCardToBack.zIndex < mCardToFront.zIndex) {
                        mCardToBack.view.bringToFront();
                        mCardView.updateViewLayout(mCardToBack.view, mCardToBack.view.getLayoutParams());
                    }
                } else {
                    if (cardToFrontBehindCardPre) {
                        mCardToFront.view.bringToFront();
                        mCardView.updateViewLayout(mCardToFront.view, mCardToFront.view.getLayoutParams());
                        cardToFrontBrought = true;
                        if (cardToBackBehindCardPre && mCardToBack.zIndex < mCardToFront.zIndex) {
                            mCardToBack.view.bringToFront();
                            mCardView.updateViewLayout(mCardToBack.view, mCardToBack.view.getLayoutParams());
                        }
                    }
                }
            }
            if (!cardToFrontBrought) {
                mCardToFront.view.bringToFront();
                mCardView.updateViewLayout(mCardToFront.view, mCardToFront.view.getLayoutParams());
            }
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {
        mCurrentFraction = 0;
    }


    @Override
    public void onAnimationEnd(Animator animation) {
        if (mAnimType == StackCardView.ANIM_TYPE_FRONT) {
            mCards.remove(mPositionToFront);
            mCards.addFirst(mCardToFront);
        } else if (mAnimType == StackCardView.ANIM_TYPE_SWITCH) {
            mCards.remove(mPositionToFront);
            mCards.removeFirst();
            mCards.addFirst(mCardToFront);
            mCards.add(mPositionToFront, mCardToBack);
        } else {
            mNextItemPosition = mNextItemPosition >= mAdapter.getCount() ? 0 : mNextItemPosition;
            mAdapter.getView(mNextItemPosition, mCardToBack.view, mCardView);
            mCards.remove(mPositionToFront);
            mCards.removeFirst();
            mCards.addFirst(mCardToFront);
            mCards.addLast(mCardToBack);
            mNextItemPosition++;
            mDataPosition++;
            mDataPosition = mDataPosition >= mAdapter.getCount() ? 0 : mDataPosition;
        }
        mPositionToFront = 0;
        mPositionToBack = 0;
        mCurrentFraction = 1;
        mIsAnim = false;
        if (mTempAdapter != null) {
            notifyDataSetChanged(mTempAdapter);
        }
        if (mAutoListener != null) {
            mAutoListener.onAnimationStart();
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    void initAdapterView(StackCardAdapter adapter, boolean reset, Context context) {
        if (mContext == null) {
            mContext = context;
        }
        this.mAdapter = (StackCardAdapter) adapter;
        this.mNextItemPosition = 5;
        this.mDataPosition = 0;
        this.mCurrentFraction = 1;
        this.mPositionToBack = 0;
        this.mPositionToFront = 0;
        if (mCardWidth > 0 && mCardHeight > 0) {
            if (mCards == null) {
                mCardView.removeAllViews();
                firstSetAdapter(adapter);
            } else if (reset || mCards.size() != adapter.getCount()) {
                resetAdapter(adapter);
            } else {
                notifySetAdapter(adapter);
            }
        }
    }

    private void resetAdapter(StackCardAdapter adapter) {
        mCardView.removeAllViews();
        firstSetAdapter(adapter);
    }

    private void firstSetAdapter(StackCardAdapter adapter) {
        if (adapter == null) {
            return;
        }
        if (mTransformerAnimAdd != null) {
            mIsAddRemoveAnim = true;
        }
        mCards = new LinkedList<>();
        mCardCount = adapter.getCount() >= 5 ? 5 : adapter.getCount();
        for (int i = mCardCount - 1; i >= 0; i--) {
            View child = adapter.getView(i, null, mCardView);
            CardItem cardItem = new CardItem(child, 0, i);
            mCardView.addCardView(cardItem);
            mZIndexTransformerCommon.transformAnimation(cardItem, mCurrentFraction, mCardWidth, mCardHeight, i, i);
            mTransformerCommon.transformAnimation(child, mCurrentFraction, mCardWidth, mCardHeight, i, i);
            mCards.addFirst(cardItem);
            child.setVisibility(View.INVISIBLE);
            showAnimAdd(child, i * mAnimAddRemoveDelay, i, i == mCardCount - 1);
        }
    }

    private void showAnimAdd(final View view, int delay, final int position, final boolean isLast) {
        if (mTransformerAnimAdd == null) {
            return;
        }
        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1).setDuration(mAnimAddRemoveDuration);
        valueAnimator.setStartDelay(delay);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float fraction = (float) animation.getAnimatedValue();
                mTransformerAnimAdd.transformAnimation(view, fraction, mCardWidth, mCardHeight,
                        position, position);
                if (mAnimAddRemoveInterpolator != null) {
                    mTransformerAnimAdd.transformInterpolatedAnimation(view,
                            mAnimAddRemoveInterpolator.getInterpolation(fraction),
                            mCardWidth, mCardHeight, position, position);
                }
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isLast) {
                    mIsAddRemoveAnim = false;
                    if (mTempAdapter != null) {
                        notifyDataSetChanged(mTempAdapter);
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mCardView.post(new Runnable() {
            @Override
            public void run() {
                valueAnimator.start();
            }
        });
    }

    private void notifySetAdapter(StackCardAdapter adapter) {
        mCardCount = adapter.getCount();
        for (int i = 0; i < mCardCount; i++) {
            CardItem cardItem = mCards.get(i);
            View child = adapter.getView(cardItem.adapterIndex, cardItem.view, mCardView);
            if (child != cardItem.view) {
                if (cardItem.view != null) {
                    mCardView.removeView(cardItem.view);
                }
                cardItem.view = child;
                mCardView.addCardView(cardItem, i);
                mZIndexTransformerCommon.transformAnimation(cardItem, mCurrentFraction, mCardWidth, mCardHeight,
                        i, i);
                mTransformerCommon.transformAnimation(child, mCurrentFraction, mCardWidth, mCardHeight, i, i);
            }
        }
        for (int i = mCardCount - 1; i >= 0; i--) {
            mCards.get(i).view.bringToFront();
            mCardView.updateViewLayout(mCards.get(i).view, mCards.get(i).view.getLayoutParams());
        }
    }

    void notifyDataSetChanged(StackCardAdapter adapter) {
        if (mIsAnim || mIsAddRemoveAnim) {
            mTempAdapter = adapter;
        } else {
            mTempAdapter = null;
            initAdapterView(adapter, false, null);
        }
    }

    void bringCardToFront(CardItem card, boolean isActionMove) {
        if (mCards == null || mTransformerCommon == null || mTransformerToFront ==
                null || mTransformerToBack == null) {
            return;
        }
        int position = mCards.indexOf(card);
        if (position == 0 && !isActionMove) {
            CardEntity cardEntity = mAdapter.getResIds().get(mDataPosition);
            if (cardClickListener != null) {
                cardClickListener.cardClick(cardEntity);
            }
            return;
        }
        if (position == 0 || position == 1) {
            mAnimDuration = 500;
            initAnimator();
            bringCardToFront();
        } else {
            switch (position) {
                case 2:
                    mAnimDuration = 500;
                    initAnimator();
                    bringCardToFront();
                    mPosition = 2;
                    setOnAutoListener(new AutoListener() {
                        @Override
                        public void onAnimationStart() {
                            --mPosition;
                            if (mPosition > 0) {
                                mCardView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        bringCardToFront();
                                    }
                                });
                            }
                        }
                    });
                    break;
                case 3:
                    mAnimDuration = 500;
                    initAnimator();
                    bringCardToFront();
                    mPosition = 3;
                    setOnAutoListener(new AutoListener() {
                        @Override
                        public void onAnimationStart() {
                            --mPosition;
                            if (mPosition > 0) {
                                mCardView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        bringCardToFront();
                                    }
                                });
                            }
                        }
                    });
                    break;
                case 4:
                    mAnimDuration = 500;
                    initAnimator();
                    bringCardToFront();
                    mPosition = 4;
                    setOnAutoListener(new AutoListener() {
                        @Override
                        public void onAnimationStart() {
                            --mPosition;
                            if (mPosition > 0) {
                                mCardView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        bringCardToFront();
                                    }
                                });
                            }
                        }
                    });
                    break;
            }
        }
    }

    void bringCardToFront() {
        if (mCardCount > 1) {
            if (1 != mPositionToFront && !mIsAnim && !mIsAddRemoveAnim) {
                mPositionToFront = 1;
                mPositionToBack = mAnimType == StackCardView.ANIM_TYPE_SWITCH ? mPositionToFront :
                        (mCardCount - 1);
                mCardToBack = mCards.getFirst();
                mCardToFront = mCards.get(mPositionToFront);
                if (mValueAnimator.isRunning()) {
                    mValueAnimator.end();
                }
                mIsAnim = true;
                mValueAnimator.start();
            }
        }
    }

    void setCardSize(int cardWidth, int cardHeight) {
        this.mCardWidth = cardWidth;
        this.mCardHeight = cardHeight;
    }

    void setTransformerToFront(AnimationTransformer toFrontTransformer) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mTransformerToFront = toFrontTransformer;
    }

    void setTransformerToBack(AnimationTransformer toBackTransformer) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mTransformerToBack = toBackTransformer;
    }

    void setZIndexTransformerToBack(ZIndexTransformer zIndexTransformerToBack) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mZIndexTransformerToBack = zIndexTransformerToBack;
    }

    void setAnimInterpolator(Interpolator animInterpolator) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mAnimInterpolator = animInterpolator;
    }

    void setAnimType(int animType) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mAnimType = animType;
    }


    void setAnimAddRemoveDelay(int animAddRemoveDelay) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mAnimAddRemoveDelay = animAddRemoveDelay;
    }

    void setAnimAddRemoveDuration(int animAddRemoveDuration) {
        if (mIsAnim || mIsAddRemoveAnim) {
            return;
        }
        this.mAnimAddRemoveDuration = animAddRemoveDuration;
    }

    private void setOnAutoListener(AutoListener onAutoListener) {
        this.mAutoListener = onAutoListener;
    }

    public interface AutoListener {
        void onAnimationStart();
    }

    public interface CardClickListener {
        void cardClick(CardEntity cardEntity);
    }
}
