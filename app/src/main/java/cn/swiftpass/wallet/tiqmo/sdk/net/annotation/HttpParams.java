package cn.swiftpass.wallet.tiqmo.sdk.net.annotation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by YZX on 2018年12月17日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class HttpParams {
    public Map<String, Object> params = new LinkedHashMap<>();
    public List<String> headers = new ArrayList<>(2);
    public Map<String, List<String>> uploadPart = new LinkedHashMap<>();
}
