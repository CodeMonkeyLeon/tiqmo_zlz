package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;

/**
 * Created by aijingya on 2020/7/13.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.contract
 * @Description:
 * @date 2020/7/13.14:24.
 */
public class CardDetailsContract {
    public interface View extends BaseView<CardDetailsContract.Presenter> {
        void getBindCardTransactionListSuccess(List<CardEntity> result);

        void getBindCardTransactioListFailed(String errorCode, String errorMsg);

        void unbindCardSuccess(unBindCardEntity result);

        void unbindCardFailed(String errorCode, String errorMsg);

        void setDefaultCardSuccess(Void result);

        void setDefaultCardFailed(String errorCode, String errorMsg);

        void getHistoryListSuccess(TransferHistoryMainEntity result);

        void getHistoryListFail(String errorCode, String errorMsg);

        void getHistoryDetailSuccess(TransferHistoryDetailEntity result);

        void getHistoryDetailFail(String errorCode, String errorMsg);

  /*      void bindNewCardSuccess(CardBind3DSEntity result);
        void bindNewCardFailed(String errorCode,String errorMsg);*/

        void deleteTransferHistorySuccess(Void result);

        void deleteTransferHistoryFail(String errorCode, String errorMsg);
    }


    public interface Presenter extends BasePresenter<CardDetailsContract.View> {
        void getBindCardTransactionList();

        void unBindCard(String protocolNo);

        void setDefaultCard(String protocolNo);
//        void bindNewCard(String cardHolderName,String cardNum, String expireDate,String CVV,String type) ;

        void getHistoryList(String protocolNo, String accountNo, List<String> paymentType,
                            List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum,
                            String direction, String orderNo);

        void getHistoryDetail(String orderNo, String orderType,String queryType);

        void deleteTransferHistory(String orderNo);
    }
}
