package cn.swiftpass.wallet.tiqmo.sdk.net.api;


import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.MapResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.DownloadFileUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TokenDataEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankIdAddressEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPayerDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPurposeListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RegionEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.TransferPdfEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.ScanOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveTransferDetail;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferPwdTimeEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.OrderExistEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AuthorizationVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ImageVerifyCodeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterFinishEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.net.RequestCall;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Headers;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Param;

public interface AuthApi {

    String LOGIN_TYPE_PD = "00";
    String LOGIN_TYPE_FIO = "40";
    String LOGIN_TYPE_PD_AND_IMAGE_VERIFY_CODE = "01";
    String LOGIN_TYPE_THIRD_PART = "20";

    String VERIFY_CODE_TYPE_LOGIN = "L";
    String VERIFY_CODE_TYPE_REGISTER = "M";
    String VERIFY_CODE_TYPE_AUTHORIZE_LOGIN = "A";
    String VERIFY_CODE_TYPE_RESET_LOGIN_PD = "P";
    String VERIFY_CODE_TYPE_FORGOT_LOGIN_PD = "F";
    String VERIFY_CODE_TYPE_RESET_PAYMENT_PD = "R";

    String IMAGE_VERIFY_CODE_TYPE_LOGIN = "LOGIN";
    String IMAGE_VERIFY_CODE_TYPE_RESET_LOGIN = "RESETLOGIN";
    String IMAGE_VERIFY_CODE_TYPE_SMS_REG_BY_PHONE = "SMS_REG_BY_PHONE";

    @Headers({"Service-Id:3034"})
    RequestCall<JsonResponse<UserInfoEntity>> login(@Param("callingCode") String callingCode,
                                                    @Param("loginId") String telephone,
                                                    @Param("password") String password,
                                                    @Param("verifyCode") String verifyCode,
                                                    @Param("type") String type,
                                                    @Param("longitude") double longitude,
                                                    @Param("latitude") double latitude);

    @Headers({"Service-Id:3039"})
    RequestCall<JsonResponse<Void>> logout();

    @Headers({"Service-Id:1072"})
    RequestCall<JsonResponse<Void>> notUseFinger(@Param("bizType") String bizType);

    @Headers({"Service-Id:3133"})
    RequestCall<JsonResponse<Void>> longTimeNoUse();

    @Headers({"Service-Id:3001"})
    RequestCall<JsonResponse<RegisterFinishEntity>> register(@Param("fullName") String fullName,
                                                             @Param("callingCode") String callingCode,
                                                             @Param("phone") String phone,
                                                             @Param("password") String password,
                                                             @Param("passwordType") String passwordType,
                                                             @Param("authType") String authType,
                                                             @Param("longitude") double longitude,
                                                             @Param("latitude") double latitude,
                                                             @Param("noStr") String noStr,
                                                             @Param("referralCode") String referralCode,
                                                             @Param("shareLink") String shareLink);

    @Headers({"Service-Id:3049"})
    RequestCall<JsonResponse<UserInfoEntity>> kycVerify(@Param("callingCode") String callingCode,
                                                        @Param("phone") String phone,
                                                        @Param("residentId") String residentId,
                                                        @Param("birthday") String birthday,
                                                        @Param("sceneType") String sceneType,
                                                        @Param("statesId") String statesId,
                                                        @Param("citiesId") String citiesId,
                                                        @Param("address") String address,
                                                        @Param("employmentCode") String employmentCode,
                                                        @Param("professionCode") String professionCode,
                                                        @Param("sourceOfFundCode") String sourceOfFundCode,
                                                        @Param("salaryRangeCode") String salaryRangeCode,
                                                        @Param("employerName") String employerName,
                                                        @Param("beneficialOwner") String beneficialOwner,
                                                        @Param("relativesExposedPerson") String relativesExposedPerson,
                                                        @Param("referralCode") String referralCode,
                                                        @Param("shareLink") String shareLink);

    @Headers({"Service-Id:3031"})
    RequestCall<JsonResponse<RegisterOtpEntity>> sendVerifyCode(@Param("callingCode") String callingCode,
                                                                @Param("phone") String phone,
                                                                @Param("type") String type,
                                                                @Param("additionalData") List<String> additionalData);

    @Headers({"Service-Id:3043"})
    RequestCall<JsonResponse<CheckPhoneEntity>> checkPhoneReg(@Param("callingCode") String callingCode,
                                                              @Param("phone") String phone,
                                                              @Param("longitude") double longitude,
                                                              @Param("latitude") double latitude,
                                                              @Param("type") String type,
                                                              @Param("checkIdNumber") String checkIdNumber,
                                                              @Param("birthday") String birthday,
                                                              @Param("needCheckIdNumber") String needCheckIdNumber);

    @Headers({"Service-Id:3043"})
    RequestCall<JsonResponse<CheckPhoneEntity>> checkForgetPd(@Param("callingCode") String callingCode,
                                                              @Param("phone") String phone,
                                                              @Param("type") String type);

    @Headers({"Service-Id:3043"})
    RequestCall<JsonResponse<Void>> checkPhoneUpdate(@Param("callingCode") String callingCode,
                                                     @Param("phone") String phone,
                                                     @Param("checkIdNumber") String checkIdNumber);

    @Headers({"Service-Id:3044"})
    RequestCall<JsonResponse<Void>> forgetPwd(@Param("callingCode") String callingCode,
                                              @Param("phone") String phone,
                                              @Param("password") String password,
                                              @Param("passwordType") String passwordType);

    @Headers({"Service-Id:1062"})
    RequestCall<JsonResponse<ImageVerifyCodeEntity>> obtainResetVerifyCode(@Param("callingCode") String callingCode,
                                                                           @Param("phoneNumber") String telephone,
                                                                           @Param("type") String type,
                                                                           @Param("imgVerifyCode") String verifyCode);

    @Headers({"Service-Id:3033"})
    RequestCall<JsonResponse<Void>> checkVerifyCode(@Param("callingCode") String callingCode,
                                                    @Param("phone") String phone,
                                                    @Param("verifyCode") String verifyCode,
                                                    @Param("type") String type,
                                                    @Param("additionalData") String orderNo);

    @Headers({"Service-Id:3042"})
    RequestCall<JsonResponse<UserInfoEntity>> changePassword(@Param("passwordType") String pdType,
                                                             @Param("password") String password,
                                                             @Param("OldPassword") String OldPassword,
                                                             @Param("operatorType") String operatorType,
                                                             @Param("sceneType") String sceneType,
                                                             @Param("referralCode") String referralCode);

    @Headers({"Service-Id:3048"})
    RequestCall<JsonResponse<Void>> verifyReferral(@Param("referralCode") String referralCode);

    @Headers({"Service-Id:3053"})
    RequestCall<JsonResponse<TransferLimitEntity>> getLimit(@Param("type") String type);

    @Headers({"Service-Id:3002"})
    RequestCall<JsonResponse<BalanceListEntity>> getBalance();

    @Headers({"Service-Id:3064"})
    RequestCall<JsonResponse<MsgListEntity>> getMsgList(@Param("pageNum") int pageNum,
                                                        @Param("pageSize") int pageSize);

    @Headers({"Service-Id:3065"})
    RequestCall<JsonResponse<Void>> removeRequest(@Param("msgInfoId") String infoId);

    @Headers({"Service-Id:3054"})
    RequestCall<JsonResponse<TransFeeEntity>> getTransferFee(@Param("type") String type,
                                                             @Param("orderAmount") String orderAmount,
                                                             @Param("orderCurrencyCode") String orderCurrencyCode,
                                                             @Param("transTimeType") String transTimeType);

    @Headers({"Service-Id:3056"})
    RequestCall<JsonResponse<TransferContactListEntity>> getTransferRecentContact();

    @Headers({"Service-Id:3057"})
    RequestCall<JsonResponse<TransferContactListEntity>> getTransferContact(@Param("phoneList") List<RequestContactEntity> phoneList);

    @Headers({"Service-Id:3055"})
    RequestCall<JsonResponse<TransferPayEntity>> transferPay(@Param("type") String type,
                                                             @Param("orderAmount") String orderAmount,
                                                             @Param("orderCurrencyCode") String orderCurrencyCode,
                                                             @Param("cvv") String cvv,
                                                             @Param("protocolNo") String protocolNo,
                                                             @Param("transFees") String transFees,
                                                             @Param("vat") String vat,
                                                             @Param("shopperResultUrl") String shopperResultUrl);

    @Headers({"Service-Id:3055"})
    RequestCall<JsonResponse<CardNotifyEntity>> googlePay(@Param("type") String type,
                                                          @Param("orderAmount") String orderAmount,
                                                          @Param("orderCurrencyCode") String orderCurrencyCode,
                                                          @Param("tokenData") TokenDataEntity tokenData,
                                                          @Param("transFees") String transFees,
                                                          @Param("vat") String vat);

    @Headers({"Service-Id:3062"})
    RequestCall<JsonResponse<TransferEntity>> transferToContact(@Param("payerUserId") String payerUserId,
                                                                @Param("callingCode") String callingCode,
                                                                @Param("payerNum") String payerNum,
                                                                @Param("payeeUserId") String payeeUserId,
                                                                @Param("payeeNum") String payeeNum,
                                                                @Param("payeeAmount") String payeeAmount,
                                                                @Param("payeeCurrencyCode") String payeeCurrencyCode,
                                                                @Param("remark") String remark,
                                                                @Param("sceneType") String sceneType,
                                                                @Param("transTimeType") String transTimeType,
                                                                @Param("payeeNumberType") String payeeNumberType,
                                                                @Param("transferPurpose") String transferPurpose,
                                                                @Param("transFees") String transFees,
                                                                @Param("vat") String vat,
                                                                @Param("payerName") String payerName);

    @Headers({"Service-Id:3062"})
    RequestCall<JsonResponse<TransferEntity>> chatTransferToContact(@Param("chatId") String chatId,
                                                                    @Param("payerUserId") String payerUserId,
                                                                @Param("callingCode") String callingCode,
                                                                @Param("payerNum") String payerNum,
                                                                @Param("payeeUserId") String payeeUserId,
                                                                @Param("payeeNum") String payeeNum,
                                                                @Param("payeeAmount") String payeeAmount,
                                                                @Param("payeeCurrencyCode") String payeeCurrencyCode,
                                                                @Param("remark") String remark,
                                                                @Param("sceneType") String sceneType,
                                                                @Param("transTimeType") String transTimeType,
                                                                @Param("payeeNumberType") String payeeNumberType,
                                                                @Param("transferPurpose") String transferPurpose,
                                                                @Param("transFees") String transFees,
                                                                @Param("vat") String vat,
                                                                @Param("payerName") String payerName);

    @Headers({"Service-Id:3045"})
    RequestCall<JsonResponse<TransferPwdTimeEntity>> checkTransferPwd(@Param("passwordType") String pdType,
                                                                      @Param("sceneType") String sceneType,
                                                                      @Param("oldPassword") String oldPassword,
                                                                      @Param("additionalData") String orderNo);

    @Headers({"Service-Id:3116"})
    RequestCall<JsonResponse<Void>> updateClickTimeService(@Param("messageId") String messageId);

    @Headers({"Service-Id:3098"})
    RequestCall<JsonResponse<ImrBankAgentListEntity>> imrBankAgentNameList(@Param("countryCode") String countryCode,
                                                                           @Param("receiptMethod") String receiptMethod,
                                                                           @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3099"})
    RequestCall<JsonResponse<ImrBankAgentListEntity>> imrBankAgentBranchList(@Param("countryCode") String countryCode,
                                                                             @Param("receiptMethod") String receiptMethod,
                                                                             @Param("parentNodeCode") String parentNodeCode);

    @Headers({"Service-Id:3100"})
    RequestCall<JsonResponse<ImrExchangeRateEntity>> imrExchangeRate(@Param("payeeInfoId") String payeeInfoId,
                                                                     @Param("sourceAmount") String sourceAmount,
                                                                     @Param("destinationAmount") String destinationAmount,
                                                                     @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3101"})
    RequestCall<JsonResponse<ImrOrderInfoEntity>> imrCheckoutOrder(@Param("orderAmount") String orderAmount,
                                                                   @Param("orderCurrency") String orderCurrency,
                                                                   @Param("payeeId") String payeeId,
                                                                   @Param("purpose") String purpose,
                                                                   @Param("receiptMethod") String receiptMethod,
                                                                   @Param("includeFee") String includeFee,
                                                                   @Param("purposeId") String purposeId,
                                                                   @Param("purposeDesc") String purposeDesc,
                                                                   @Param("channelCode") String channelCode,
                                                                   @Param("chargesFee") String chargesFee,
                                                                   @Param("destinationAmount") String destinationAmount,
                                                                   @Param("destinationCurrency") String destinationCurrency,
                                                                   @Param("payAmount") String payAmount,
                                                                   @Param("exchangeRate") String exchangeRate);

    @Headers({"Service-Id:3102"})
    RequestCall<JsonResponse<Void>> imrCheck(@Param("orderNo") String orderNo);

    @Headers({"Service-Id:3103"})
    RequestCall<JsonResponse<ImrBankIdAddressEntity>> imrGetIdOrAddress(@Param("receiptMethod") String receiptMethod,
                                                                        @Param("countryCode") String countryCode);

    @Headers({"Service-Id:3097"})
    RequestCall<JsonResponse<ImrCountryListEntity>> imrCountryList(@Param("countryType") String countryType,
                                                                   @Param("countryCode") String countryCode);

    @Headers({"Service-Id:3089"})
    RequestCall<JsonResponse<ImrAddBeneResultEntity>> imrAddBeneficiary(@Param("receiptMethod") String receiptMethod,
                                                                        @Param("receiptOrgCode") String receiptOrgCode,
                                                                        @Param("receiptOrgBranchCode") String receiptOrgBranchCode,
                                                                        @Param("payeeFullName") String payeeFullName,
                                                                        @Param("nickName") String nickName,
                                                                        @Param("relationshipCode") String relationshipCode,
                                                                        @Param("callingCode") String callingCode,
                                                                        @Param("phone") String phone,
                                                                        @Param("transferDestinationCountryCode") String transferDestinationCountryCode,
                                                                        @Param("payeeInfoCountryCode") String payeeInfoCountryCode,
                                                                        @Param("birthPlace") String birthPlace,
                                                                        @Param("birthDate") String birthDate,
                                                                        @Param("sex") String sex,
                                                                        @Param("cityName") String cityName,
                                                                        @Param("districtName") String districtName,
                                                                        @Param("poBox") String poBox,
                                                                        @Param("buildingNo") String buildingNo,
                                                                        @Param("street") String street,
                                                                        @Param("idNo") String idNo,
                                                                        @Param("idExpiry") String idExpiry,
                                                                        @Param("bankAccountType") String bankAccountType,
                                                                        @Param("ibanNo") String ibanNo,
                                                                        @Param("bankAccountNo") String bankAccountNo,
                                                                        @Param("saveFlag") String saveFlag,
                                                                        @Param("receiptOrgName") String receiptOrgName,
                                                                        @Param("receiptOrgBranchName") String receiptOrgBranchName,
                                                                        @Param("cityId") String cityId,
                                                                        @Param("currencyCode") String currencyCode,
                                                                        @Param("channelPayeeId") String channelPayeeId,
                                                                        @Param("channelCode") String channelCode,
                                                                        @Param("branchId") String branchId);

    @Headers({"Service-Id:1009"})
    RequestCall<JsonResponse<QrCodeEntity>> getQrCode(@Param("currencyCode") String currencyCode);

    @Headers({"Service-Id:3013"})
    RequestCall<JsonResponse<QrCodeEntity>> getCrCode(@Param("paymentMethodNo") String paymentMethodNo);

    @Headers({"Service-Id:3060"})
    RequestCall<JsonResponse<BeneficiaryListEntity>> getBeneficiaryList();

    @Headers({"Service-Id:3066"})
    RequestCall<JsonResponse<TimeEntity>> getTransferTime(@Param("ibanNo") String ibanNo);

    @Headers({"Service-Id:3058"})
    RequestCall<JsonResponse<BeneficiaryEntity>> addBeneficiary(@Param("beneficiaryName") String beneficiaryName,
                                                                @Param("beneficiaryIbanNo") String beneficiaryIbanNo,
                                                                @Param("chooseRelationship") String chooseRelationship,
                                                                @Param("saveFlag") String saveFlag);

    @Headers({"Service-Id:3059"})
    RequestCall<JsonResponse<Void>> deleteBeneficiary(@Param("id") String id);

    @Headers({"Service-Id:3063"})
    RequestCall<JsonResponse<UserInfoEntity>> checkQrCode(@Param("scanInfo") String scanInfo);

    @Headers({"Service-Id:3063"})
    RequestCall<JsonResponse<ScanOrderEntity>> checkOrderCode(@Param("scanInfo") String scanInfo);

    @Headers({"Service-Id:3077"})
    RequestCall<JsonResponse<Void>> checkLanguage();

    @Headers({"Service-Id:3075"})
    RequestCall<JsonResponse<FilterListEntity>> getFilterList();

    @Headers({"Service-Id:3069"})
    RequestCall<JsonResponse<TransferHistoryMainEntity>> getTransferMainHistory(
            @Param("protocolNo") String protocolNo,
            @Param("accountNo") String accountNo,
            @Param("paymentType") List<String> paymentType,
            @Param("tradeType") List<String> tradeType,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate,
            @Param("pageSize") int pageSize,
            @Param("pageNum") int pageNum,
            @Param("direction") String direction,
            @Param("orderNo") String orderNo);

    @Headers({"Service-Id:3281"})
    RequestCall<JsonResponse<TransferHistoryMainEntity>> getPreAuthTransferMainHistory(
            @Param("protocolNo") String protocolNo,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate,
            @Param("pageSize") int pageSize,
            @Param("pageNum") int pageNum,
            @Param("direction") String direction,
            @Param("orderNo") String orderNo);

    @Headers({"Service-Id:3015"})
    RequestCall<JsonResponse<CheckOutEntity>> checkOut(@Param("orderNo") String orderNo,
                                                       @Param("orderInfo") String orderInfo);

    @Headers({"Service-Id:3070"})
    RequestCall<JsonResponse<TransferHistoryDetailEntity>> getHistoryDetail(@Param("orderNo") String orderNo,
                                                                            @Param("orderType") String orderType,
                                                                            @Param("queryType") String queryType);

    @Headers({"Service-Id:3282"})
    RequestCall<JsonResponse<TransferHistoryDetailEntity>> getPreAuthHistoryDetail(
            @Param("orderNo") String orderNo,
            @Param("orderType") String orderType
    );

    @Headers({"Service-Id:3076"})
    RequestCall<JsonResponse<Void>> deleteTransferHistory(@Param("orderNo") String orderNo);

    @Headers({"Service-Id:3017"})
    RequestCall<JsonResponse<TransferEntity>> confirmPay(@Param("orderNo") String orderNo,
                                                         @Param("paymentMethodNo") String paymentMethodNo,
                                                         @Param("orderType") String orderType);

    @Headers({"Service-Id:3050"})
    RequestCall<JsonResponse<TransferEntity>> transferSurePay(@Param("sceneType") String sceneType,
                                                              @Param("orderNo") String orderNo,
                                                              @Param("payMethod") String payMethod,
                                                              @Param("transAmount") String transAmount,
                                                              @Param("exchangeRate") String exchangeRate,
                                                              @Param("transCurrencyCode") String transCurrencyCode,
                                                              @Param("transFees") String transFees,
                                                              @Param("vat") String vat);


    @Headers({"Service-Id:1007"})
    RequestCall<JsonResponse<Void>> resetLoginPassword(@Param("callingCode") String callingCode,
                                                       @Param("phoneNumber") String telephone,
                                                       @Param("password") String secret);

    @Headers({"Service-Id:3061"})
    RequestCall<JsonResponse<KycContactEntity>> checkContact(@Param("callingCode") String callingCode,
                                                             @Param("phone") String phone,
                                                             @Param("contactsName") String contactsName,
                                                             @Param("sceneType") String sceneType);

    @Headers({"Service-Id:3073"})
    RequestCall<JsonResponse<SplitPeopleListEntity>> getSplitDetailList(@Param("newOrderNo") String newOrderNo);

    @Headers({"Service-Id:3071"})
    RequestCall<JsonResponse<TransferEntity>> splitBill(@Param("oldOrderNo") String oldOrderNo,
                                                        @Param("receivableAmount") String receivableAmount,
                                                        @Param("currencyCode") String currencyCode,
                                                        @Param("orderRemark") String orderRemark,
                                                        @Param("receiverInfoList") List<SplitBillPayerEntity> receiverInfoList);

    @Headers({"Service-Id:3067"})
    RequestCall<JsonResponse<RequestCenterListEntity>> getRequestCenterList();

    @Headers({"Service-Id:3052"})
    RequestCall<JsonResponse<PayBillOrderInfoEntity>> sendNotify(@Param("receivers") List<NotifyEntity> receivers);

    @Headers({"Service-Id:1031"})
    RequestCall<JsonResponse<UserInfoEntity>> getUserInfo();


    @Headers({"Service-Id:1023"})
    RequestCall<JsonResponse<AuthorizationVerifyEntity>> authorizationVerify(@Param("openId") String openID);

    @Headers({"Service-Id:3011"})
    RequestCall<JsonResponse<CardBind3DSEntity>> bindCardAddMoney(@Param("custName") String custName,
                                                                  @Param("cardNo") String cardNo,
                                                                  @Param("expire") String expire,
                                                                  @Param("cvv") String cvv,
                                                                  @Param("type") String type,
                                                                  @Param("orderAmount") String orderAmount,
                                                                  @Param("orderCurrencyCode") String orderCurrencyCode,
                                                                  @Param("transFees") String transFees,
                                                                  @Param("vat") String vat,
                                                                  @Param("shopperResultUrl") String shopperResultUrl,
                                                                  @Param("creditType") String creditType);

    @Headers({"Service-Id:3072"})
    RequestCall<JsonResponse<CardNotifyEntity>> request3ds(@Param("sessionId") String sessionId);

    @Headers({"Service-Id:3072"})
    RequestCall<JsonResponse<CardNotifyEntity>> request3ds(@Param("sessionId") String sessionId,
                                                           @Param("orderNo") String orderNo);

    @Headers({"Service-Id:3080"})
    RequestCall<JsonResponse<ReferralCodeDescEntity>> requestReferralCodeDesc(@Param("queryType") String queryType);

    @Headers({"Service-Id:3081"})
    RequestCall<JsonResponse<EVoucherEntity>> requestVoucherBrand();

    @Headers({"Service-Id:3081"})
    RequestCall<JsonResponse<EVoucherEntity>> requestVoucherBrand(@Param("sceneType") String sceneType);

    @Headers({"Service-Id:3082"})
    RequestCall<JsonResponse<VouSupCountryEntity>> requestVouSupCountry(@Param("voucherServiceProviderCode") String providerCode,
                                                                        @Param("voucherBrandCode") String brandCode);

    @Headers({"Service-Id:3083"})
    RequestCall<JsonResponse<VouTypeAndInfoEntity>> requestVoucherTypeAndInfo(@Param("voucherServiceProviderCode") String providerCode,
                                                                              @Param("voucherBrandCode") String brandCode,
                                                                              @Param("countryCode") String countryCode);

    @Headers({"Service-Id:3083"})
    RequestCall<JsonResponse<VouTypeAndInfoEntity>> requestVoucherTypeAndInfo(@Param("voucherServiceProviderCode") String providerCode,
                                                                              @Param("voucherBrandCode") String brandCode,
                                                                              @Param("countryCode") String countryCode,
                                                                              @Param("sceneType") String sceneType);

    @Headers({"Service-Id:3084"})
    RequestCall<JsonResponse<EVoucherDetailEntity>> getVoucherDatail(@Param("orderNo") String orderNo);

    @Headers({"Service-Id:3085"})
    RequestCall<JsonResponse<EVoucherOrderInfoEntity>> eVoucherOrder(@Param("voucherCode") String voucherCode,
                                                                     @Param("voucherAmount") String voucherAmount);

    @Headers({"Service-Id:3086"})
    RequestCall<JsonResponse<OrderExistEntity>> checkOrderExist(@Param("voucherServiceProviderCode") String providerCode,
                                                                @Param("voucherBrandCode") String brandCode,
                                                                @Param("sceneType") String sceneType);

    @Headers({"Service-Id:3074"})
    RequestCall<JsonResponse<ReferralCodeEntity>> requestReferralCode();

    @Headers({"Service-Id:3078"})
    RequestCall<JsonResponse<AllMarketInfoEntity>> requestAllMarket();

    @Headers({"Service-Id:3079"})
    RequestCall<JsonResponse<MarketDetailsEntity>> requestMarketDetails(@Param("activityNo") String activityNo);

    @Headers({"Service-Id:3087"})
    RequestCall<JsonResponse<CityEntity>> requestCityList(@Param("statesId") String statesId);

    @Headers({"Service-Id:3088"})
    RequestCall<JsonResponse<RegionEntity>> requestRegionList(@Param("citiesId") String citiesId);

    @Headers({"Service-Id:3090"})
    RequestCall<JsonResponse<ImrBeneficiaryListEntity>> getImrBeneficiaryList();

    @Headers({"Service-Id:3091"})
    RequestCall<JsonResponse<ImrBeneficiaryDetails>> getImrBeneficiaryDetail(@Param("payeeInfoId") String payeeInfoId);

    @Headers({"Service-Id:3092"})
    RequestCall<JsonResponse<ResponseEntity>> editImrBeneficiaryInfo(@Param("payeeInfoId") String payeeInfoId,
                                                                     @Param("transferDestinationCountryCode") String transferDestinationCountryCode,
                                                                     @Param("receiptMethod") String receiptMethod,
                                                                     @Param("receiptOrgCode") String receiptOrgCode,
                                                                     @Param("receiptOrgBranchCode") String receiptOrgBranchCode,
                                                                     @Param("payeeFullName") String payeeFullName,
                                                                     @Param("nickName") String nickName,
                                                                     @Param("relationshipCode") String relationshipCode,
                                                                     @Param("callingCode") String callingCode,
                                                                     @Param("phone") String phone,
                                                                     @Param("payeeInfoCountryCode") String payeeInfoCountryCode,
                                                                     @Param("birthPlace") String birthPlace,
                                                                     @Param("birthDate") String birthDate,
                                                                     @Param("sex") String sex,
                                                                     @Param("cityName") String cityName,
                                                                     @Param("districtName") String districtName,
                                                                     @Param("poBox") String poBox,
                                                                     @Param("buildingNo") String buildingNo,
                                                                     @Param("street") String street,
                                                                     @Param("idNo") String idNo,
                                                                     @Param("idExpiry") String idExpiry,
                                                                     @Param("bankAccountType") String bankAccountType,
                                                                     @Param("ibanNo") String ibanNo,
                                                                     @Param("bankAccountNo") String bankAccountNo,
                                                                     @Param("receiptOrgName") String receiptOrgName,
                                                                     @Param("receiptOrgBranchName") String receiptOrgBranchName,
                                                                     @Param("cityId") String cityId,
                                                                     @Param("currencyCode") String currencyCode,
                                                                     @Param("channelPayeeId") String channelPayeeId,
                                                                     @Param("channelCode") String channelCode,
                                                                     @Param("branchId") String branchId
    );

    RequestCall<MapResultEntity> mapSearch(@Param("query") String query,
                                           @Param("key") String key);

    @Headers({"Service-Id:3093"})
    RequestCall<JsonResponse<ResponseEntity>> removeImrBeneficiary(@Param("payeeInfoId") String payeeInfoId);

    @Headers({"Service-Id:3095"})
    RequestCall<JsonResponse<BusinessParamEntity>> getBusinessParamList();

    @Headers({"Service-Id:3096"})
    RequestCall<JsonResponse<BusinessParamEntity>> getBusinessParam(@Param("businessType") String businessType,
                                                                    @Param("parentBussinessCode") String parentBussinessCode);

    @Headers({"Service-Id:3249"})
    RequestCall<JsonResponse<BusinessParamEntity>> getTransferPurpose(@Param("businessType") String businessType);

    @Headers({"Service-Id:3123"})
    RequestCall<JsonResponse<ImrPurposeListEntity>> getImrPurposeList(@Param("countryCode") String countryCode,
                                                                      @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3121"})
    RequestCall<JsonResponse<ImrCityListEntity>> getImrCityList(@Param("countryCode") String countryCode,
                                                                @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3122"})
    RequestCall<JsonResponse<ImrPayerDetailEntity>> getImrPayerDetail(@Param("countryCode") String countryCode,
                                                                      @Param("cityId") String cityId,
                                                                      @Param("payerCurrencyCode") String payerCurrencyCode,
                                                                      @Param("payeeCurrencyCode") String payeeCurrencyCode,
                                                                      @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3124"})
    RequestCall<JsonResponse<ImrOrderInfoEntity>> sendImrCompliance(@Param("compliance") List<ImrComplianceEntity> compliance);

    @Headers({"Service-Id:3243"})
    RequestCall<JsonResponse<Void>> trustDevice();

    @Headers({"Service-Id:3245"})
    RequestCall<JsonResponse<RechargeOrderInfoEntity>> getOtpType(@Param("category") String category, @Param("categoryId") String categoryId,
                                                                  @Param("additionalData") List<String> additionalData);

    @Headers({"Service-Id:3131"})
    RequestCall<JsonResponse<HelpListEntity>> getHelpContentList(@Param("content") String content);

    @Headers({"Service-Id:3132"})
    RequestCall<JsonResponse<Void>> submitHelpQuestion(@Param("id") String id, @Param("comment") String comment, @Param("thumb") String thumb);

    @Headers({"Service-Id:3248"})
    RequestCall<JsonResponse<TransferPdfEntity>> getTransferPdfUrl(@Param("orderNo") String orderNo);

    @Headers({"Service-Id:3250"})
    RequestCall<JsonResponse<InviteEntity>> getInviteDetail();


    @Headers({"Service-Id:3251"})
    RequestCall<JsonResponse<DownloadFileUrlEntity>> getDownloadFileUrl(@Param("orderNo") String orderNo);

    @Headers({"Service-Id:3252"})
    RequestCall<JsonResponse<RechargeOrderInfoEntity>> preCheckPd(@Param("passwordType") String passwordType,
                                                                  @Param("operatorType") String operatorType,
                                                                  @Param("oldPassword") String oldPassword);

    @Headers({"Service-Id:3253"})
    RequestCall<JsonResponse<ResultEntity>> sendIvrCall(@Param("outActionId") String outActionId,
                                                        @Param("amount") String amount);


    @Headers({"Service-Id:3255"})
    RequestCall<JsonResponse<ResultEntity>> getIvrResult(@Param("outActionId") String outActionId);

    @Headers({"Service-Id:3256"})
    RequestCall<JsonResponse<Void>> activateIvrBeneficary(@Param("payeeInfoId") String payeeInfoId);

    @Headers({"Service-Id:3264"})
    RequestCall<JsonResponse<SplitListEntity>> getSplit(@Param("splitType") String splitType);

    @Headers({"Service-Id:3265"})
    RequestCall<JsonResponse<SendReceiveSplitDetailEntity>> getSplitDetail(@Param("originalOrderNo") String originalOrderNo,
                                                                           @Param("orderRelevancy") String orderRelevancy,
                                                                           @Param("splitType") String splitType);

    @Headers({"Service-Id:3266"})
    RequestCall<JsonResponse<Void>> sendSplitsRemind(@Param("originalOrderNo") String originalOrderNo,
                                                     @Param("orderRelevancy") String orderRelevancy);


    @Headers({"Service-Id:3267"})
    RequestCall<JsonResponse<List<SendReceiveEntity>>> getSendReceive();

    @Headers({"Service-Id:3268"})
    RequestCall<JsonResponse<SendReceiveDetailListEntity>> SendReceiveDetail(@Param("groupId") String groupId,
                                                                             @Param("tradeType") String tradeType,
                                                                             @Param("isGroup") String isGroup,
                                                                             @Param("pageNum") int pageNum,
                                                                             @Param("pageSize") int pageSize);


    @Headers({"Service-Id:3269"})
    RequestCall<JsonResponse<SendReceiveTransferDetail>> getTransferDetail(@Param("groupId") String groupId,
                                                                           @Param("isGroup") String isGroup);

    @Headers({"Service-Id:3272"})
    RequestCall<JsonResponse<ContactInviteEntity>> contactInvite(@Param("contactInviteDtos") List<KycContactEntity> contactInviteDtos);

    @Headers({"Service-Id:3273"})
    RequestCall<JsonResponse<RequestCenterEntity>> splitBillPayDetail(@Param("orderNo") String orderNo);

    @Headers({"Service-Id:3158"})
    RequestCall<JsonResponse<Void>> updateAddress(@Param("iso2CountryCode") String iso2CountryCode,
                                                  @Param("iso3CountryCode") String iso3CountryCode,
                                                  @Param("countryName") String countryName,
                                                  @Param("longitude") double longitude,
                                                  @Param("latitude") double latitude);

}
