package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class NumberEntity extends BaseEntity {

    public String number;
    //自定义键盘输入类型  1为数字  2为删除键
    public int type = 1;

    public int imgResId;

    public NumberEntity(final String number, final int type,int imgResId) {
        this.number = number;
        this.type = type;
        this.imgResId = imgResId;
    }
}
