package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class RechargeContactAdapter extends BaseRecyclerAdapter<KycContactEntity> {

    private boolean isFilter;

    public RechargeContactAdapter(@Nullable List<KycContactEntity> data) {
        super(R.layout.item_kyc_contact, data);
    }

    public void setIsFilter(boolean isFilter) {
        this.isFilter = isFilter;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, KycContactEntity kycContactEntity, int position) {
        int section = getSectionForPosition(position);
        if (section == -1) {
            return;
        }

        if (position == mDataList.size() - 1) {
            baseViewHolder.setGone(R.id.view, false);
        } else {
            baseViewHolder.setGone(R.id.view, true);
        }
        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        baseViewHolder.setGone(R.id.ll_letter, false);
        if (position == getPositionForSection(section)) {
            baseViewHolder.setGone(R.id.ll_letter, false);
            baseViewHolder.setText(R.id.tv_letter, kycContactEntity.getSortLetter());
            if (isFilter) {
                baseViewHolder.setGone(R.id.ll_letter, true);
            } else {
                baseViewHolder.setGone(R.id.ll_letter, false);
            }
        } else {
            baseViewHolder.setGone(R.id.ll_letter, true);
        }

        String name = kycContactEntity.getContactsName();
        if (TextUtils.isEmpty(name)) {
            baseViewHolder.setGone(R.id.tv_user_name, true);
        } else {
            baseViewHolder.setGone(R.id.tv_user_name, false);
            baseViewHolder.setText(R.id.tv_user_name, name);
        }
        baseViewHolder.setText(R.id.tv_phone, kycContactEntity.getPhoneNumber());
        ImageView ivAvatar = baseViewHolder.getView(R.id.iv_avatar);
        String avatarUrl = kycContactEntity.getHeadIcon();
        String gender = kycContactEntity.getSex();
        if (!TextUtils.isEmpty(avatarUrl)) {
            Glide.with(mContext).clear(ivAvatar);
            Glide.with(mContext)
                    .asBitmap()
                    .load(avatarUrl)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .dontAnimate()
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(new BitmapImageViewTarget(ivAvatar) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                            ivAvatar.setImageBitmap(bitmap);
                        }
                    });
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivAvatar);
        }
        setItemStatus(baseViewHolder, true, true);

    }

    private void setItemStatus(BaseViewHolder baseViewHolder, boolean showText, boolean showWhatApp) {
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        KycContactEntity kycContactEntity = getItem(position);
        if (kycContactEntity != null) {
            return kycContactEntity.getSortLetter().charAt(0);
        }
        return -1;
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mDataList.get(i).getSortLetter();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }
}
