package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.TextUtils;

import java.math.BigDecimal;

/**
 * 小数点处理工具类
 */

public class BigDecimalFormatUtils {


    /**
     * LogUtils.i("TEST", BigDecimalFormatUtils.forMatWithDigs("3.152", 2));
     * LogUtils.i("TEST",BigDecimalFormatUtils.forMatWithDigs("3.1", 2));
     * LogUtils.i("TEST",BigDecimalFormatUtils.forMatWithDigs("3.15", 2));
     * LogUtils.i("TEST",BigDecimalFormatUtils.forMatWithDigs("3.159", 2));
     *
     * @param str
     * @param count 小数点位数
     * @return
     */
    public static String forMatWithDigs(String str, int count) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        BigDecimal b = new BigDecimal(str);
        return forMatWithDigs(b, count);
    }

    public static String forMatWithDigsWithNoPoint(String str, int count) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        BigDecimal b = new BigDecimal(str);
        String value = forMatWithDigs(b, count);
        return value.replace(".00", "");
    }

    public static String forMatWithDigs(BigDecimal str, int count) {
        return str.setScale(count, BigDecimal.ROUND_UP).toString();
    }

    public static String forMatWithDigsDown(BigDecimal str, int count) {
        return str.setScale(count, BigDecimal.ROUND_DOWN).toString();
    }

    public static String forMatWithDigsDown(String str, int count) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        BigDecimal b = new BigDecimal(str);
        return forMatWithDigsDown(b, count);
    }

    public static String forMatWithDigs(double str, int count) {
        return forMatWithDigs(str + "", count);
    }

    /**
     * 提供精确的加法运算
     *
     * @param v1 被加数
     * @param v2 加数
     * @param scale 保留scale 位小数
     * @return 两个参数的和
     */
    public static String add(String v1, String v2, int scale) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.add(b2).setScale(scale, BigDecimal.ROUND_UP).toString();
    }

    /**
     * 提供精确的减法运算
     *
     * @param v1 被减数
     * @param v2 减数
     * @param scale 保留scale 位小数
     * @return 两个参数的差
     */
    public static String sub(String v1, String v2, int scale) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.subtract(b2).setScale(scale, BigDecimal.ROUND_UP).toString();
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @param scale 保留scale 位小数
     * @return 两个参数的积
     */
    public static String mul(String v1, String v2, int scale) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        return mul(v1, v2, scale,BigDecimal.ROUND_UP);
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @param scale 保留scale 位小数
     * @param type 取整方式
     * @return 两个参数的积
     */
    public static String mul(String v1, String v2, int scale,int type) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.multiply(b2).setScale(scale, type).toString();
    }

    /**
     * 提供精确的除法运算。当发生除不尽的情况时，由scale参数指定精度，以后的数字进位
     *
     * @param v1 被除数
     * @param v2 除数
     * @param scale 表示需要精确到小数点以后几位
     * @return 两个参数的商
     */
    public static String div(String v1, String v2, int scale) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.divide(b2, scale, BigDecimal.ROUND_UP).toString();
    }

    /**
     * 提供精确的除法运算。当发生除不尽的情况时，由scale参数指定精度，以后的数字舍弃
     *
     * @param v1 被除数
     * @param v2 除数
     * @param scale 表示需要精确到小数点以后几位
     * @return 两个参数的商
     */
    public static String divToDown(String v1, String v2, int scale) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.divide(b2, scale, BigDecimal.ROUND_DOWN).toString();
    }

    public static String div(String v1, String v2, int scale,int type) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.divide(b2, scale, type).toString();
    }

    /**
     * 提供精确的小数位四舍五入处理
     *
     * @param v 需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    public static double round(double v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        return b.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 提供精确的小数位四舍五入处理
     *
     * @param v 需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    public static String round(String v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b = new BigDecimal(v);
        return b.setScale(scale, BigDecimal.ROUND_HALF_UP).toString();
    }

    /**
     * 取余数
     *
     * @param v1 被除数
     * @param v2 除数
     * @param scale 小数点后保留几位
     * @return 余数
     */
    public static String remainder(String v1, String v2, int scale) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        if (scale < 0) {
            throw new IllegalArgumentException("保留的小数位数必须大于零");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.remainder(b2).setScale(scale, BigDecimal.ROUND_HALF_UP).toString();
    }

    /**
     * 比较大小
     *
     * @param v1 被比较数
     * @param v2 比较数
     * @return 如果v1 大于等于v2 则 返回true 否则false
     */
    public static boolean compare(String v1, String v2) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        int bj = b1.compareTo(b2);
        if (bj >= 0)
            return true;
        else
            return false;
    }

    /**
     * 比较大小
     *
     * @param v1 被比较数
     * @param v2 比较数
     * @return 如果v1 大于v2 则 返回true 否则false
     */
    public static boolean compareBig(String v1, String v2) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        int bj = b1.compareTo(b2);
        if (bj > 0)
            return true;
        else
            return false;
    }

    /**
     * 判断是否等于0
     * @param v
     * @return
     */
    public static boolean isZero(String v){
        return compareInt(v,"0") == 0;
    }

    /**
     * 比较大小
     *
     * @param v1 被比较数
     * @param v2 比较数
     * @return 如果v1 大于v2 则 返回1 如果v1 等于v2 则返回0
     */
    public static int compareInt(String v1, String v2) {
        if(TextUtils.isEmpty(v1)) v1 = "0";
        if(TextUtils.isEmpty(v2)) v2 = "0";
        v1 = v1.replace(",","");
        v2 = v2.replace(",","");
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        int bj = b1.compareTo(b2);
        return bj;
    }

}
