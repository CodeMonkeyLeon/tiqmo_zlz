package cn.swiftpass.wallet.tiqmo.support.chat;

import com.gc.gcchat.GcChatError;

public class GcChatErrorUtils {
    public static String getErrorMessage(int errorCode){
        switch (errorCode){
            case GcChatError.AUTHENTICATION: return "AUTHENTICATION";
            case GcChatError.INTERNET_ISSUE: return "INTERNET_ISSUE";
            case GcChatError.API_TIMEOUT: return "API_TIMEOUT";
            case GcChatError.SERVER_ERROR: return "SERVER_ERROR";
            case GcChatError.INVALID_DATA: return "INVALID_DATA";
            case GcChatError.PARTICIPANT_NOT_BLOCKED: return "PARTICIPANT_NOT_BLOCKED";
            case GcChatError.PARTICIPANT_ALREADY_BLOCKED: return "PARTICIPANT_ALREADY_BLOCKED";
            case GcChatError.ENCRYPTION_ERROR: return "ENCRYPTION_ERROR";
            case GcChatError.NOT_ALLOWED: return "NOT_ALLOWED";
            case GcChatError.FILE_TYPE_NOT_SUPPORTED: return "FILE_TYPE_NOT_SUPPORTED";
            case GcChatError.MESSAGE_ID_NOT_FOUND: return "MESSAGE_ID_NOT_FOUND";
            case GcChatError.USER_ALREADY_ADDED: return "USER_ALREADY_ADDED";
            case GcChatError.NO_OTHER_PARTICIPANT: return "NO_OTHER_PARTICIPANT";
            case GcChatError.PARTICIPANT_ID_NOT_FOUND: return "PARTICIPANT_ID_NOT_FOUND";
            case GcChatError.NOT_A_PARTICIPANT: return "NOT_A_PARTICIPANT";
            case GcChatError.NOT_ONE_TO_ONE_SESSION: return "NOT_ONE_TO_ONE_SESSION";
            case GcChatError.SESSION_ALREADY_EXIST: return "SESSION_ALREADY_EXIST";
            case GcChatError.INVALID_SESSION_ID: return "INVALID_SESSION_ID";
            case GcChatError.INVITE_TO_ONE_TO_ONE_SESSION_NOT_ALLOWED: return "INVITE_TO_ONE_TO_ONE_SESSION_NOT_ALLOWED";
            case GcChatError.BACKUP_AVAILABLE: return "BACKUP_AVAILABLE";
            case GcChatError.BACKUP_UNAVAILABLE: return "BACKUP_UNAVAILABLE";
            case GcChatError.INVALID_PASSWORD: return "INVALID_PASSWORD";
            case GcChatError.INCORRECT_PASSWORD: return "INCORRECT_PASSWORD";
            case GcChatError.INFECTED_FILE: return "INFECTED_FILE";
            default : return "UNKNOWN_ERROR";
        }
    }
}

