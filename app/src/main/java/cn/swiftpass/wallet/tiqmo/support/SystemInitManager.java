package cn.swiftpass.wallet.tiqmo.support;

import cn.swiftpass.wallet.tiqmo.support.entity.SystemInitEntity;

public class SystemInitManager {

    private static SystemInitManager sInstance = new SystemInitManager();
    public static String DEFAULT_OTP_INTERVAL_TIME = "60";
    public static String DEFAULT_IDV_EXPIRE_TIME = "300";
    public static String DEFAULT_PASS_CODE_EXPIRE_TIME = "90";

    private SystemInitManager() {

    }

    public static SystemInitManager getInstance() {
        return sInstance;
    }

    private SystemInitEntity systemInitEntity;


    public SystemInitEntity getSystemInitEntity() {
        if (systemInitEntity == null) {
            systemInitEntity = new SystemInitEntity(DEFAULT_OTP_INTERVAL_TIME, DEFAULT_IDV_EXPIRE_TIME,DEFAULT_PASS_CODE_EXPIRE_TIME);
        }
        return systemInitEntity;
    }

    public void setSystemInitEntity(SystemInitEntity systemInitEntity) {
        this.systemInitEntity = systemInitEntity;
    }
}
