package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeDomeOrderEntity extends BaseEntity {

    //代金券服务提供商代码
    public String voucherServiceProviderCode;
    //代金券品牌名称
    public String voucherBrandName;
    //代金券品牌亮色版logo url
    public String voucherBrandLightLogo;
    //代金券品牌暗色版logo url
    public String voucherBrandDarkLogo;
    //代金券品牌代码
    public String voucherBrandCode;
    //运营商品牌亮色版小图标 url
    public String operatorSmallLightIcon;
    //运营商品牌暗色版小图标 url
    public String operatorSmallDarkIcon;
    //是否有国家限制 Y: 有限制 N: 无限制
    public String limitCountry;

}
