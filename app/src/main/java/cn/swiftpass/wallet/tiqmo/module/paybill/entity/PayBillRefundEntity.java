package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.ExtendEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillRefundEntity extends BaseEntity {
    public String id;
    public String logo;
    //退款金额（返回就展示） - 只有退款成功才有金额 (金额单位：最小币种单位)
    public String tradeAmount;
    //币种符号(返回金额的时候才展示)
    public String currencyType;
    public String orderDesc;
    public String remark;
    //退款对应的bill name
    public String refundFrom;
    public String refundId;
    public String orderStatus;
    public String createTime;
    public String paymentMethod;
    public String orderDirection;
    public List<ExtendEntity> basicExtendProperties = new ArrayList<>();
    public int sceneType;
    public String orderNo;
    //退款所属 sku name
    public String name;
    //描述
    public String desc;
}
