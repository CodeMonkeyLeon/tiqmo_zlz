package cn.swiftpass.wallet.tiqmo.sdk.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * Created by YZX on 2019年08月27日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class AllCardEntity extends BaseEntity {
    public ArrayList<CardEntity> bindCardInfos = new ArrayList<>();
    public List<CardEntity> madaCardInfoList = new ArrayList<>();
    public List<CardEntity> creditCardInfoList = new ArrayList<>();

    public ArrayList<CardEntity> getCardList() {
        return bindCardInfos;
    }

    public void setCardList(ArrayList<CardEntity> cardList) {
        this.bindCardInfos = cardList;
    }

}
