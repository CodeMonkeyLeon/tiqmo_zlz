package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardPinEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardSummaryPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.VerifyPwdCodeView;

public class GetCardPwdTwoActivity extends BaseCompatActivity<CardKsaContract.GetCardSummaryPresenter> implements CardKsaContract.GetCardSummaryView{

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_second_title)
    TextView tvSecondTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_face)
    ImageView ivFace;
    @BindView(R.id.tv_one)
    TextView tvOne;
    @BindView(R.id.tv_two)
    TextView tvTwo;
    @BindView(R.id.verift_pwd)
    VerifyPwdCodeView veriftPwd;
    @BindView(R.id.tv_error_otp)
    TextView tvErrorOtp;

    private OpenCardReqEntity openCardReqEntity;
    private String passwordTwo,passwordOne;
    private String cardType;

    private String proxyCardNo;

    private RiskControlEntity riskControlEntity;

    private static GetCardPwdTwoActivity getCardPwdTwoActivity;

    public static GetCardPwdTwoActivity getGetCardPwdTwoActivity() {
        return getCardPwdTwoActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_get_card_pwd_two;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        getCardPwdTwoActivity = this;
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        if(getIntent() != null && getIntent().getExtras() != null) {
            proxyCardNo = getIntent().getExtras().getString(Constants.proxyCardNo);
        }
        cardType = openCardReqEntity.cardType;
        passwordOne = openCardReqEntity.passwordOne;
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //默认输入框为空
        veriftPwd.setEmpty();
        veriftPwd.setOnCodeFinishListener(new VerifyPwdCodeView.OnCodeFinishListener() {
            @Override
            public void onTextChange(View view, String content) {
                tvErrorOtp.setVisibility(View.GONE);
            }

            @Override
            public void onComplete(View view, String content) {
                passwordTwo = content;
                if(passwordTwo.equals(passwordOne)){
                    CardPinEntity cardPinEntity = new CardPinEntity();
                    cardPinEntity.pin = passwordTwo;
                    openCardReqEntity.pinBean = cardPinEntity;
                    UserInfoManager.getInstance().setOpenCardReqEntity(openCardReqEntity);
                    if(!TextUtils.isEmpty(proxyCardNo)){
                        mPresenter.setKsaCardPin(passwordTwo,proxyCardNo);
                    }else {
                        if (Constants.ksa_card_VIRTUAL.equals(cardType) && openCardReqEntity.isVirtualFree) {
                            List<String> additionalData = new ArrayList<>();
                            mPresenter.getOtpType("ActionType", "TCA02", additionalData);
                        }else if(Constants.ksa_card_PHYSICAL.equals(cardType) && openCardReqEntity.isPhysicalFree){
                            List<String> additionalData = new ArrayList<>();
                            mPresenter.getOtpType("ActionType", "TCA02", additionalData);
                        } else {
                            mPresenter.getKsaCardSummary(cardType, openCardReqEntity.cardProductType);
                        }
                    }
                }else{
                    tvErrorOtp.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getCardPwdTwoActivity = null;
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if("030194".equals(errorCode)){
            HashMap<String,Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.cardResultType,2);
            mHashMap.put(Constants.cardResultErrorMsg,errorMsg);
            ActivitySkipUtil.startAnotherActivity(this, KsaCardResultActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void getKsaCardSummarySuccess(KsaCardSummaryEntity ksaCardSummaryEntity) {
        if(ksaCardSummaryEntity != null){
            HashMap<String,Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksaCardSummaryEntity,ksaCardSummaryEntity);
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardSummaryActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getKsaCardPayResultSuccess(KsaPayResultEntity ksaPayResultEntity) {
        HashMap<String,Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.ksa_cardType,cardType);
        ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void setKsaCardPinSuccess(Void result) {
        HashMap<String,Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.cardResultType,KsaCardResultActivity.cardResultType_Set_Pin);
        ActivitySkipUtil.startAnotherActivity(this, KsaCardResultActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public CardKsaContract.GetCardSummaryPresenter getPresenter() {
        return new CardSummaryPresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        veriftPwd.setEmpty();
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(GetCardPwdTwoActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.sceneType, Constants.TYPE_PAY_CARD_NO_FEE);
        mHashMap.put(Constants.openCardReqEntity,openCardReqEntity);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_PAY_CARD_NO_FEE);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    private void requestTransfer(){
        mPresenter.getKsaCardPayResult("","",openCardReqEntity);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
