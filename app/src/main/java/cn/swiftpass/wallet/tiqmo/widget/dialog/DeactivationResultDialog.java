package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class DeactivationResultDialog extends BottomDialog {

    private Context mContext;

    public DeactivationResultDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_deactivation_result, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        LinearLayout llContent = view.findViewById(R.id.ll_content);

        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DeactivationOtpDialog.getInstance(mContext).showWithBottomAnim();
                    }
                }, 100);
                dismiss();
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
