package cn.swiftpass.wallet.tiqmo.support.utils;

import android.app.Activity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class ActivityLifeManager {

    private ArrayList<Activity> activities = new ArrayList<>();
    private ArrayList<Activity> activitiesFront = new ArrayList<>();
    private static ActivityLifeManager sInstance = new ActivityLifeManager();
    private WeakReference<Activity> sCurrentActivityWeakRef;

    private ActivityLifeManager() {

    }

    public static ActivityLifeManager getInstance() {
        return sInstance;
    }

    public Activity getCurrentActivity() {
        Activity currentActivity = null;
        if (sCurrentActivityWeakRef != null) {
            currentActivity = sCurrentActivityWeakRef.get();
        }
        return currentActivity;
    }


    public ArrayList<Activity> getActivities() {
        return activities;
    }

    public ArrayList<Activity> getFrontActivities() {
        return activitiesFront;
    }

    public void setCurrentActivity(Activity activity) {
        sCurrentActivityWeakRef = new WeakReference<Activity>(activity);
    }

    public void add(Activity activity) {
        activities.add(activity);
    }

    public synchronized void remove(Activity activity) {
        activities.remove(activity);
    }

    public void addFront(Activity activity) {
        activitiesFront.add(activity);
    }

    public void removeFront(Activity activity) {
        activitiesFront.remove(activity);
    }

    /**
     * 关闭所有activity
     */
    public void finish() {
        for (Activity activity : activities) {
            if (activity != null) {
                activity.finish();
            }
        }
        activities.clear();

    }

    /**
     * 退出当前应用
     */
    public void exit(boolean shutdown) {
        finish();
        if (shutdown) {
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    public Activity getTopActivity() {
        if (activities.size() > 0) {
            return activities.get(activities.size() - 1);
        }
        return null;
    }

    public boolean isExitInActivityList(Class<? extends Activity> cls){
        for (Activity activity : activities) {
            if (activity != null&&cls!=null&&activity.getClass().getName().equals(cls.getName()) ) {
                return true;
            }
        }
        return false;
    }


}
