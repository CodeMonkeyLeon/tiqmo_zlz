package cn.swiftpass.wallet.tiqmo.module.topup.presenter;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;

public class RechargeContract {

    public interface RechargeCountryView extends BaseView<RechargeCountryPresenter> {
        void getRechargeOperatorSuccess(RechargeOperatorListEntity rechargeOperatorListEntity);
        void getRechargeCountrySuccess(RechargeCountryListEntity rechargeCountryListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface RechargeCountryPresenter extends BasePresenter<RechargeCountryView> {
        void getRechargeCountry();
        void getRechargeOperator(String phoneNumber);
    }

    public interface RechargeOperatorView extends BaseView<RechargeOperatorPresenter> {
        void getRechargeOperatorSuccess(RechargeOperatorListEntity rechargeOperatorListEntity);

        void getRechargeProductListSuccess(RechargeProductListEntity rechargeProductListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface RechargeOperatorPresenter extends BasePresenter<RechargeOperatorView> {
        void getRechargeOperator(String phoneNumber);

        void getRechargeProductList(String operatorId);
    }

    public interface RechargeOrderListView extends BaseView<RechargeContract.RechargeOrderListPresenter> {

        void getRechargeOrderListSuccess(RechargeOrderListEntity RechargeOrderListEntity);

        void deleteRechargeOrderSuccess(Void result);

        void getRechargeOrderDetailSuccess(RechargeOrderDetailEntity rechargeOrderDetailEntity);

        void showErrorMsg(String errorCode, String errorMsg);

        void getRechargeOrderListFail(String errorCode, String errorMsg);

        void getRechargeProductListSuccess(RechargeProductListEntity rechargeProductListEntity);
    }

    public interface RechargeOrderListPresenter extends BasePresenter<RechargeContract.RechargeOrderListView> {
        void getRechargeOrderList();

        void getRechargeCountry();

        void getRechargeOrderDetail(String recordId);

        void deleteRechargeOrder(String recordId);

        void getRechargeProductList(String operatorId);

    }

    public interface RechargePayView extends BaseView<RechargePayPresenter> {
        void preRechargeOrderSuccess(RechargeOrderInfoEntity preOrderInfoEntity);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void checkOutFail(String errorCode, String errorMsg);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface RechargePayPresenter extends BasePresenter<RechargePayView> {
        void preRechargeOrder(String productId, String operatorId, String country, String mobileNumber,
                              String amount,String targetAmount,String shortName,String operatorName);

        void checkOut(String orderNo, String orderInfo);
    }
}
