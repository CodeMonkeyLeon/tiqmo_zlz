package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class BeneficiaryListEntity extends BaseEntity {

    public String partnerNo;
    public List<BeneficiaryEntity> beneficiaryInfoList = new ArrayList<>();
}
