package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeContract;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeOperatorPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class RechargeConfirmActivity extends BaseCompatActivity<RechargeContract.RechargeOperatorPresenter> implements RechargeContract.RechargeOperatorView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_change_mobile)
    TextView tvChangeMobile;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.et_operator)
    EditTextWithDel etOperator;
    @BindView(R.id.con_operator)
    ConstraintLayout conOperator;
    @BindView(R.id.et_short_name)
    EditTextWithDel etShortName;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_view_plans)
    TextView tvViewPlans;

    private KycContactEntity kycContactEntity;
    private String phoneNumber;
    private String shortName;
    private String operatorName,operatorId;
    private RechargeOperatorEntity mRechargeOperatorEntity;
    private RechargeOperatorListEntity mRechargeOperatorListEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_confirm;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, ivHeadCircle);
        tvTitle.setText(R.string.common_5);
        etShortName.getTlEdit().setHint(getString(R.string.BillPay_17));
        etOperator.getTlEdit().setHint(getString(R.string.Topup_new_4));
        etOperator.setFocusableFalse();
        if(getIntent() != null && getIntent().getExtras() != null){
            kycContactEntity = (KycContactEntity) getIntent().getExtras().getSerializable(Constants.rechargeContactEntity);
            mRechargeOperatorListEntity = (RechargeOperatorListEntity) getIntent().getExtras().getSerializable(Constants.rechargeOperatorListEntity);
            if(mRechargeOperatorListEntity != null){
                int size = mRechargeOperatorListEntity.operatorList.size();
                for (int i = 0; i < size; i++) {
                    RechargeOperatorEntity rechargeOperatorEntity = mRechargeOperatorListEntity.operatorList.get(i);
                    if (rechargeOperatorEntity != null) {
                        if(rechargeOperatorEntity.identified) {
                            mRechargeOperatorEntity = rechargeOperatorEntity;
                            operatorId = rechargeOperatorEntity.id;
                            operatorName = rechargeOperatorEntity.operatorName;
                            etOperator.setContentText(operatorName);
                            checkNextStatus();
                        }
                    }
                }
            }
            if(kycContactEntity != null){
                phoneNumber = kycContactEntity.getPhoneNumber();
                if(TextUtils.isEmpty(phoneNumber)){
                    phoneNumber = kycContactEntity.getPhone();
                }
                tvPhone.setText(phoneNumber);
//                shortName = kycContactEntity.getContactsName();
//                if(!TextUtils.isEmpty(shortName)){
//                    etShortName.setContentText(shortName);
//                }
                checkNextStatus();

                etOperator.getEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mRechargeOperatorListEntity!=null) {
                            showRechargeOperator(mRechargeOperatorListEntity);
                        }
                    }
                });

                etShortName.getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        shortName = s.toString().trim();
                        checkNextStatus();
                    }
                });
            }
        }
    }

    private void checkNextStatus() {
        tvViewPlans.setEnabled(!TextUtils.isEmpty(phoneNumber)&&!TextUtils.isEmpty(shortName)&&!TextUtils.isEmpty(operatorId));
    }

    @Override
    public RechargeContract.RechargeOperatorPresenter getPresenter() {
        return new RechargeOperatorPresenter();
    }

    private void showRechargeOperator(RechargeOperatorListEntity rechargeOperatorListEntity){
        if (rechargeOperatorListEntity != null) {
            List<RechargeOperatorEntity> operatorList = rechargeOperatorListEntity.operatorList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = operatorList.size();
            for (int i = 0; i < size; i++) {
                RechargeOperatorEntity rechargeOperatorEntity = operatorList.get(i);
                if (rechargeOperatorEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = rechargeOperatorEntity.operatorName;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.Topup_new_4), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    mRechargeOperatorEntity = operatorList.get(position);
                    if (mRechargeOperatorEntity != null) {
                        operatorId = mRechargeOperatorEntity.id;
                        operatorName = mRechargeOperatorEntity.operatorName;
                        etOperator.setContentText(operatorName);
                        checkNextStatus();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getRechargeOperatorSuccess");
        }
    }

    @Override
    public void getRechargeOperatorSuccess(RechargeOperatorListEntity rechargeOperatorListEntity) {
        this.mRechargeOperatorListEntity = rechargeOperatorListEntity;
        int size = rechargeOperatorListEntity.operatorList.size();
        for (int i = 0; i < size; i++) {
            RechargeOperatorEntity rechargeOperatorEntity = rechargeOperatorListEntity.operatorList.get(i);
            if (rechargeOperatorEntity != null) {
                if(rechargeOperatorEntity.identified) {
                    operatorId = mRechargeOperatorEntity.id;
                    operatorName = mRechargeOperatorEntity.operatorName;
                    etOperator.setContentText(operatorName);
                    checkNextStatus();
                }
            }
        }
    }

    @Override
    public void getRechargeProductListSuccess(RechargeProductListEntity rechargeProductListEntity) {
        if(rechargeProductListEntity != null){
            HashMap<String,Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.rechargeProductListEntity,rechargeProductListEntity);
            mRechargeOperatorEntity.mobileNumber = phoneNumber;
            mRechargeOperatorEntity.shortName = shortName;
            mHashMap.put(Constants.rechargeOperatorEntity,mRechargeOperatorEntity);
            ActivitySkipUtil.startAnotherActivity(this, RechargeProductListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @OnClick({R.id.iv_back, R.id.tv_get_help, R.id.tv_change_mobile, R.id.tv_view_plans})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_help:
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_change_mobile:
                finish();
                break;
            case R.id.tv_view_plans:
                mPresenter.getRechargeProductList(operatorId);
                break;
            default:break;
        }
    }
}
