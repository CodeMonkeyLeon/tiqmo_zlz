package cn.swiftpass.wallet.tiqmo.sdk.net.annotation;

import androidx.annotation.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by YZX on 2018年12月17日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class AnnotationParser {

    //将方法里面的各种注解(包含参数注解)内容解析到HttpParams
    @NonNull
    public static HttpParams parse(Method method, Object[] args) {
        HttpParams params = new HttpParams();
        parseMethodAnnotation(method.getAnnotations(), params);
        Annotation[][] annotations = method.getParameterAnnotations();
        for (int i = 0, count = annotations.length; i < count; i++) {
            parseParamsAnnotation(annotations[i], args[i], params);
        }
        return params;
    }

    //解析方法注解
    private static void parseMethodAnnotation(Annotation[] annotations, HttpParams httpParams) {
        if (annotations == null || annotations.length == 0) {
            return;
        }
        for (Annotation annotation : annotations) {
            if (annotation instanceof Headers) {
                String[] headers = ((Headers) annotation).value();
                if (headers.length > 0) {
                    httpParams.headers.addAll(Arrays.asList(headers));
                }
            }
        }
    }

    //解析参数注解
    @SuppressWarnings("unchecked")
    private static void parseParamsAnnotation(Annotation[] annotations, Object param, HttpParams httpParams) {
        if (annotations == null || annotations.length == 0 || param == null) {
            return;
        }
        for (Annotation annotation : annotations) {
            if (annotation instanceof Param) { //普通参数
                httpParams.params.put(((Param) annotation).value(), param);
            } else if (annotation instanceof UploadPart) {//上次参数
                String partName = ((UploadPart) annotation).value();
                if (param instanceof String) {
                    httpParams.uploadPart.put(partName, Collections.singletonList((String) param));
                } else if (param instanceof List) {
                    List paths = (List) param;
                    if (paths.size() == 0) {
                        continue;
                    }
                    if (paths.get(0) instanceof String) {
                        httpParams.uploadPart.put(partName, paths);
                    } else {
                        throw new RuntimeException("When the value of @UploadPart is List type, the parameter type of List must be String.");
                    }
                }
            }
        }
    }
}
