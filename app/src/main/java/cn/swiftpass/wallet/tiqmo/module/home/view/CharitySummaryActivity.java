package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherContract;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherPayPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class CharitySummaryActivity extends BaseCompatActivity<EVoucherContract.EVoucherPayPresenter> implements EVoucherContract.EVoucherPayView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_available_balance)
    TextView tvAvailableBalance;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.ll_error)
    LinearLayout llError;
    @BindView(R.id.iv_store_logo)
    ImageView ivStoreLogo;
    @BindView(R.id.tv_store_name)
    TextView tvStoreName;
    @BindView(R.id.tv_order_money)
    TextView tvOrderMoney;
    @BindView(R.id.iv_split_line)
    ImageView ivSplitLine;
    @BindView(R.id.tv_total_title)
    TextView tvTotalTitle;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.tv_pay_confirm)
    TextView tvPayConfirm;

    @BindView(R.id.tv_trans_fee_title)
    TextView tvTransFeeTitle;
    @BindView(R.id.tv_trans_fee)
    TextView tvTransFee;
    @BindView(R.id.tv_vat_title)
    TextView tvVatTitle;
    @BindView(R.id.tv_vat)
    TextView tvVat;
    @BindView(R.id.tv_wrong_fees)
    TextView tvWrongFees;
    @BindView(R.id.tv_wrong_vat_money)
    TextView tvWrongVatMoney;
    @BindView(R.id.ll_transfer_vat)
    ConstraintLayout llTransferVat;
    @BindView(R.id.ll_transfer_fee)
    ConstraintLayout llTransferFee;


    private double monthLimitMoney, valiableMoney, money;

    private TransferLimitEntity transferLimitEntity;

    private CheckOutEntity checkOutEntity;
    private String sceneType;

    private String orderAmount,payerAmount,orderCurrencyCode;
    private String transferFees, vatAmount,wrongTransferFee, wrongVat;
    private String paymentMethodNo, orderNo;
    private String orderType = "P";

    private RechargeOrderInfoEntity mRechargeOrderInfoEntity;

    private RiskControlEntity riskControlEntity;

    private static CharitySummaryActivity charitySummaryActivity;

    public static CharitySummaryActivity getCharitySummaryActivity() {
        return charitySummaryActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_charity_summary;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        charitySummaryActivity = this;
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        charitySummaryActivity = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }

        getBalance();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUser();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                tvBalance.setText(userInfoEntity.getBalance());
            }
        }
    }

    private void checkMoney(String amount) {
        money = AndroidUtils.getTransferMoneyNumber(amount);
        if (money > monthLimitMoney) {
            tvPayConfirm.setEnabled(false);
            llError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.wtw_25_3));
        }else if (money <= valiableMoney) {
            if (money > 0) {
                tvPayConfirm.setEnabled(true);
            }
            tvAddMoney.setEnabled(false);
            llError.setVisibility(View.GONE);
        } else {
            tvAddMoney.setEnabled(true);
            tvPayConfirm.setEnabled(false);
            llError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.wtw_25));
        }
        if (valiableMoney == 0) {
            tvAddMoney.setEnabled(true);
        }
    }


    private void checkUser() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            String balance = userInfoEntity.getBalance();
            String balanceNumber = userInfoEntity.getBalanceNumber();
            if (!TextUtils.isEmpty(balanceNumber)) {
                try {
                    balanceNumber = balanceNumber.replace(",", "");
                    valiableMoney = Double.parseDouble(balanceNumber);
                    checkMoney(payerAmount);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
            tvBalance.setText(balance);
            tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
        }
    }


    private void initView() {
        setDarkBar();
        tvTitle.setText(R.string.BillPay_38);
        LocaleUtils.viewRotationY(this, ivHeadCircle, ivBack);

        if (getIntent() != null && getIntent().getExtras() != null) {
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            checkOutEntity = (CheckOutEntity) getIntent().getExtras().getSerializable(Constants.CHECK_OUT_ENTITY);
            mRechargeOrderInfoEntity = (RechargeOrderInfoEntity) getIntent().getExtras().getSerializable(Constants.rechargeOrderInfoEntity);
            sceneType = getIntent().getExtras().getString(Constants.sceneType);
            if (transferLimitEntity != null) {
                String monthLimitAmt = transferLimitEntity.monthLimitAmt;
                try {
                    monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }

            tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

            if (checkOutEntity != null) {
                orderCurrencyCode = checkOutEntity.orderCurrencyCode;
                payerAmount = checkOutEntity.payerAmount;
                orderAmount = checkOutEntity.orderAmount;

                vatAmount = checkOutEntity.discountVat;
                transferFees = checkOutEntity.discountTransFees;
                wrongTransferFee = checkOutEntity.transFees;
                wrongVat = checkOutEntity.vat;

                String merchantName = checkOutEntity.merchantName;
                if (!TextUtils.isEmpty(merchantName)) {
                    tvStoreName.setText(merchantName);
                }

                if (!TextUtils.isEmpty(payerAmount)) {
                    tvPayConfirm.setText(getString(R.string.sprint17_26) + " " + AndroidUtils.getTransferMoney(payerAmount) + " " + LocaleUtils.getCurrencyCode(orderCurrencyCode));
                }

                if (!TextUtils.isEmpty(payerAmount)) {
                    tvTotalAmount.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(orderAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
                tvOrderMoney.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(orderAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).size(12)
                        .build());

                if (!TextUtils.isEmpty(wrongVat)) {
                    ////如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                    llTransferVat.setVisibility(View.VISIBLE);
                    tvWrongVatMoney.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(wrongVat) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());

                    if (!TextUtils.isEmpty(vatAmount)) {
                        tvVat.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(vatAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }else {
                    //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                    if (!TextUtils.isEmpty(vatAmount) && BigDecimalFormatUtils.compareBig(vatAmount, "0")) {
                        tvWrongVatMoney.setVisibility(View.GONE);
                        tvVat.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(vatAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    } else {
                        llTransferVat.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(wrongTransferFee)) {
                    //如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                    llTransferFee.setVisibility(View.VISIBLE);
                    tvWrongFees.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(wrongTransferFee) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());

                    if(!TextUtils.isEmpty(transferFees)) {
                        tvTransFee.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(transferFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }else{
                    //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                    if (!TextUtils.isEmpty(transferFees) && BigDecimalFormatUtils.compareBig(transferFees,"0")) {
                        tvWrongFees.setVisibility(View.GONE);
                        tvTransFee.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(transferFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }else{
                        llTransferFee.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_add_money, R.id.tv_pay_confirm})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_pay_confirm:
                if (mRechargeOrderInfoEntity != null) {
                    riskControlEntity = mRechargeOrderInfoEntity.riskControlInfo;
                    if (riskControlEntity != null) {
                        if (riskControlEntity.isOtp()) {
                            HashMap<String, Object> mHashMaps = new HashMap<>(16);

                            if (checkOutEntity != null) {
                                setTransferMap(mHashMaps);
                            }
                            ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                            jumpToPwd(false);
                        }else if(riskControlEntity.isNeedIvr()){
                            jumpToPwd(true);
                        } else {
                            requestTransfer();
                        }
                    } else {
                        requestTransfer();
                    }
                } else {
                    requestTransfer();
                }
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            default:
                break;
        }
    }

    private void requestTransfer() {
        if (checkOutEntity != null) {
            orderNo = checkOutEntity.orderNo;
            if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
                paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
            }
            orderType = "P";
            mPresenter.confirmPay(checkOutEntity.orderNo, paymentMethodNo, "P");
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap) {
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_CHARITY);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_CHARITY);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.newhome_12));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    public void jumpToPwd(boolean isNeedIvr) {
        if (checkOutEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            setTransferMap(mHashMap);
            if(isNeedIvr){
                ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else {
                ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public EVoucherContract.EVoucherPayPresenter getPresenter() {
        return new EVoucherPayPresenter();
    }

    @Override
    public void confirmPaySuccess(TransferEntity transferEntity) {
        confirmPaySuccess(this,sceneType,checkOutEntity,false,transferEntity);
    }

    @Override
    public void confirmPayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
