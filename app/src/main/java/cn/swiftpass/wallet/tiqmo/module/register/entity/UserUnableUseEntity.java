package cn.swiftpass.wallet.tiqmo.module.register.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class UserUnableUseEntity extends BaseEntity {

    public List<String> userDisableAppFuncCodeList = new ArrayList<>();
}
