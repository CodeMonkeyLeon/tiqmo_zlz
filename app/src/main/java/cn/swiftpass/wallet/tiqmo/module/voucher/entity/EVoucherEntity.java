package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class EVoucherEntity extends BaseEntity {

   public ArrayList<StoreInfo> voucherBrandOne;//商家各个国家地区的信息
   public ArrayList<StoreInfo> voucherBrandTwo;//商家各个国家地区的信息

   public static class StoreInfo extends BaseEntity {
      public String voucherServiceProviderCode;
      public String voucherBrandName;
      public String voucherBrandCode;
      public String limitCountry;
      public String voucherBrandLightLogo;
      public String voucherBrandDarkLogo;
      public String operatorSmallLightIcon;
      public String operatorSmallDarkIcon;
   }
}
