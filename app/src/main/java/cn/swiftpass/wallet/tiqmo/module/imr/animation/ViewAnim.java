package cn.swiftpass.wallet.tiqmo.module.imr.animation;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class ViewAnim {

    public static void startViewUp(Context context, TextView view, AnimStateListener listener) {
        if (!(view.getParent() instanceof FrameLayout)) {
            return;
        }
        view.measure(0,0);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        int bottomMargin = layoutParams.bottomMargin;
        int width = LocaleUtils.isRTL(context) ? view.getMeasuredWidth() : -view.getMeasuredWidth();
        ValueAnimator anim = ValueAnimator.ofInt(0, AndroidUtils.dip2px(context, 25));

        // 设置动画运行的时长
        anim.setDuration(200);
        // 设置动画重复播放次数
        anim.setRepeatCount(0);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null) {
                    listener.onAnimEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // 获得改变后的值
                int diff = (int) animation.getAnimatedValue();
                layoutParams.bottomMargin = diff + bottomMargin;
                view.setLayoutParams(layoutParams);
                view.setScaleX((1 - 0.3f * ((float) diff / AndroidUtils.dip2px(context, 25))));
                view.setScaleY((1 - 0.3f * ((float) diff / AndroidUtils.dip2px(context, 25))));
                view.setTranslationX(width * 0.15f * ((float) diff / AndroidUtils.dip2px(context, 25)));
                view.requestLayout();
            }
        });
        anim.start();
    }

    public static void resetViewParams(Context context, View view) {
        if (!(view.getParent() instanceof FrameLayout)) {
            return;
        }
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        layoutParams.bottomMargin = AndroidUtils.dip2px(context, 5);
        view.setLayoutParams(layoutParams);
        view.setScaleX(1f);
        view.setScaleY(1f);
        view.setTranslationX(0f);
    }

    public interface AnimStateListener {
        void onAnimEnd();
    }
}
