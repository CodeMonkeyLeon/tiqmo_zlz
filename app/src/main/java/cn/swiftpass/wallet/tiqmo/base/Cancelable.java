package cn.swiftpass.wallet.tiqmo.base;


public interface Cancelable {
    void cancel();
}
