package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeOrderInfoEntity extends BaseEntity {

    public String orderInfo;

    public RiskControlEntity riskControlInfo;
}
