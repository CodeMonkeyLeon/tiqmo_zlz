package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class SelectCardDialog extends BottomDialog{

    private Context mContext;

    private ChooseTypeListener chooseTypeListener;

    /**
     * 1 mada card  2 credit card  3 bank transfer
     */
    private int type = 1;

    public void setChooseTypeListener(ChooseTypeListener chooseTypeListener) {
        this.chooseTypeListener = chooseTypeListener;
    }

    public SelectCardDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public interface ChooseTypeListener {
        void chooseTransferType(int type);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_select_card, null);
        TextView tvSelectTitle = view.findViewById(R.id.tv_select_title);
        TextView tvAddNew = view.findViewById(R.id.tv_add_new);
        LinearLayout llCreditCard = view.findViewById(R.id.ll_credit_card);
        LinearLayout llBankTransfer = view.findViewById(R.id.ll_bank_transfer);

        tvAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 1;
                if(chooseTypeListener != null){
                    chooseTypeListener.chooseTransferType(type);
                }
            }
        });

        llCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 2;
                if(chooseTypeListener != null){
                    chooseTypeListener.chooseTransferType(type);
                }
            }
        });

        llBankTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 3;
                if(chooseTypeListener != null){
                    chooseTypeListener.chooseTransferType(type);
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
