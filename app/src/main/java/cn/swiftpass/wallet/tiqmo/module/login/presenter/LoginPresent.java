package cn.swiftpass.wallet.tiqmo.module.login.presenter;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.login.contract.LoginContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class LoginPresent implements LoginContract.Presenter {

    LoginContract.View baseView;

    @Override
    public void loginAccount(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude) {
        if (baseView == null) {
            return;
        }
        AppClient.getInstance().loginByType(callingCode, phone, password, verifyCode, type, longitude, latitude, new LifecycleMVPResultCallback<UserInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(UserInfoEntity response) {
                if (baseView != null) {
                    baseView.loginAccountSuccess(response);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.loginAccountError(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkPhone(String callingCode, String phone,String type) {
        if(baseView == null){
            return;
        }
    }

    @Override
    public void activateUserStatus(String callingCode, String phone, String lockAcc) {
        if("E".equals(lockAcc)){
            AppClient.getInstance().getUserManager().renewId(callingCode, phone, lockAcc, new LifecycleMVPResultCallback<ResultEntity>(baseView, true) {
                @Override
                protected void onSuccess(ResultEntity result) {
                    baseView.renewIdSuccess(result);
                }

                @Override
                protected void onFail(String errorCode, String errorMsg) {
                    baseView.renewIdFail(errorCode, errorMsg);
                }
            });
        }else {
            AppClient.getInstance().getUserManager().activateUserStatus(callingCode, phone, lockAcc, new LifecycleMVPResultCallback<Void>(baseView, true) {
                @Override
                protected void onSuccess(Void result) {
                    baseView.activateUserStatusSuccess(result);
                }

                @Override
                protected void onFail(String errorCode, String errorMsg) {
                    baseView.activateUserStatusFail(errorCode, errorMsg);
                }
            });
        }
    }

    @Override
    public void trustDevice() {
        AppClient.getInstance().trustDevice(new LifecycleMVPResultCallback<Void>(baseView,false) {
            @Override
            protected void onSuccess(Void result) {
                baseView.trustDeviceSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.trustDeviceFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(LoginContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
