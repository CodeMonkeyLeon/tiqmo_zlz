package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.AppUtility;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseItemDataBinder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class DateFloaterBinder extends BaseItemDataBinder<MessageEntity> {
    @Override
    public int getViewType() {
        return MessageAdapter.DateFloater;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_chat_date;
    }

    @Override
    public void bindData(BaseViewHolder baseViewHolder, MessageEntity messageEntity, int position) {
        baseViewHolder.setText(R.id.tv_date, AppUtility.getCustomDateFromMs(String.valueOf(messageEntity.getTimestamp())));
    }
}
