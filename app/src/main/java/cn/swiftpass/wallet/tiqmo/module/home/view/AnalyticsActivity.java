package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class AnalyticsActivity extends BaseCompatActivity {

    @BindView(R.id.fl_analytics)
    FrameLayout flAnalytics;

    private AnalyticsFragment analyticsFragment;

    int position;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_analytics;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (getIntent() != null && getIntent().getExtras() != null) {
            position = getIntent().getExtras().getInt(Constants.openAnalyticsPage);
        }
        analyticsFragment = AnalyticsFragment.getInstance(position,true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fl_analytics, analyticsFragment);
        ft.commit();
    }
}
