package cn.swiftpass.wallet.tiqmo.module.setting.animation;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.adapter.StackCardAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.animation.entry.CardItem;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;


public class StackCardView extends ViewGroup {

    public static final int ANIM_TYPE_FRONT = 0, ANIM_TYPE_SWITCH = 1, ANIM_TYPE_FRONT_TO_LAST = 2;
    private static final float CARD_SIZE_RATIO = 0.5f;
    private float mCardRatio = CARD_SIZE_RATIO;
    private CardAnimationHelper mAnimationHelper;
    private StackCardAdapter mAdapter;
    private int mCardWidth, mCardHeight;
    float mDownRawY, moveY;

    public StackCardView(@NonNull Context context) {
        this(context, null);
    }

    public StackCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StackCardView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
        setClickable(true);
    }

    private void init(Context context, AttributeSet attrs) {
        int animType = ANIM_TYPE_FRONT;
        int animDuration = CardAnimationHelper.ANIM_DURATION;
        int animAddRemoveDuration = CardAnimationHelper.ANIM_ADD_REMOVE_DURATION;
        int animAddRemoveDelay = CardAnimationHelper.ANIM_ADD_REMOVE_DELAY;
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.StackCardView);
            animType = ta.getInt(R.styleable.StackCardView_animType, ANIM_TYPE_FRONT);
            mCardRatio = ta.getFloat(R.styleable.StackCardView_cardRatio, CARD_SIZE_RATIO);
            animDuration = ta.getInt(R.styleable.StackCardView_animDuration, CardAnimationHelper.ANIM_DURATION);
            animAddRemoveDuration = ta.getInt(R.styleable.StackCardView_animAddRemoveDuration,
                    CardAnimationHelper.ANIM_ADD_REMOVE_DURATION);
            animAddRemoveDelay = ta.getInt(R.styleable.StackCardView_animAddRemoveDelay,
                    CardAnimationHelper.ANIM_ADD_REMOVE_DELAY);
            ta.recycle();
        }
        mAnimationHelper = new CardAnimationHelper(animType, animDuration, this);
        mAnimationHelper.setAnimAddRemoveDuration(animAddRemoveDuration);
        mAnimationHelper.setAnimAddRemoveDelay(animAddRemoveDelay);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);
        int sizeHeight = MeasureSpec.getSize(heightMeasureSpec);
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        if (widthMode != MeasureSpec.EXACTLY || heightMode != MeasureSpec.EXACTLY) {
            int childCount = getChildCount();
            int childWidth = 0, childHeight = 0;
            for (int i = 0; i < childCount; i++) {
                View childView = getChildAt(i);
                childWidth = Math.max(childView.getMeasuredWidth(), childWidth);
                childHeight = Math.max(childView.getMeasuredHeight(), childHeight);
            }
            setMeasuredDimension((widthMode == MeasureSpec.EXACTLY) ? sizeWidth : childWidth,
                    (heightMode == MeasureSpec.EXACTLY) ? sizeHeight : childHeight);
        } else {
            setMeasuredDimension(sizeWidth, sizeHeight);
        }
        if (mCardWidth == 0 || mCardHeight == 0) {
            setCardSize(true);
        }
    }

    private void setCardSize(boolean resetAdapter) {
        mCardWidth = getMeasuredWidth();
        mCardHeight = (int) (mCardWidth * mCardRatio);
        mAnimationHelper.setCardSize(mCardWidth, mCardHeight);
        mAnimationHelper.initAdapterView(mAdapter, resetAdapter, null);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        int childWidth, childHeight;
        int childLeft, childTop, childRight, childBottom;
        int width = getWidth(), height = getHeight();
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            childWidth = childView.getMeasuredWidth();
            childHeight = childView.getMeasuredHeight();
            childLeft = (width - childWidth) / 2;
            childTop = (height - childHeight) / 2;
            childRight = childLeft + childWidth;
            childBottom = childTop + childHeight;
            childView.layout(childLeft + 40, childTop + 40, childRight - 40, childBottom - 400);
        }
    }

    void addCardView(CardItem card) {
        addView(getCardView(card));
    }

    void addCardView(CardItem card, int position) {
        addView(getCardView(card), position);
    }

    private View getCardView(final CardItem card) {
        View view = card.view;
        LayoutParams layoutParams = new LayoutParams(mCardWidth,
                mCardHeight);
        view.setLayoutParams(layoutParams);

        view.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mDownRawY = event.getRawY() - 25;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        moveY = event.getRawY() - 25;
                        if (Math.abs(moveY - mDownRawY) > 10) {
                            bringCardToFront(card, true);
                        } else {
                            bringCardToFront(card, false);
                        }
                        break;

                }
                return true;
            }
        });

        return view;
    }

    public void setCardClickListener(CardAnimationHelper.CardClickListener cardClickListener) {
        if (mAnimationHelper != null) {
            mAnimationHelper.setCardClickListener(cardClickListener);
        }
    }

    private void bringCardToFront(CardItem card, boolean isActionMove) {
        if (!isClickable()) {
            return;
        }
        mAnimationHelper.bringCardToFront(card, isActionMove);
    }

    public void setAdapter(StackCardAdapter adapter, Context context, List<CardEntity> resIds) {
        this.mAdapter = adapter;
        this.mAdapter.setResIds(resIds);
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                mAnimationHelper.notifyDataSetChanged(mAdapter);
            }
        });
        mAnimationHelper.initAdapterView(adapter, true, context);
    }

    public void setTransformerToFront(AnimationTransformer toFrontTransformer) {
        mAnimationHelper.setTransformerToFront(toFrontTransformer);
    }

    public void setTransformerToBack(AnimationTransformer toBackTransformer) {
        mAnimationHelper.setTransformerToBack(toBackTransformer);
    }


    public void setZIndexTransformerToBack(ZIndexTransformer zIndexTransformerToBack) {
        mAnimationHelper.setZIndexTransformerToBack(zIndexTransformerToBack);
    }


    public void setAnimInterpolator(Interpolator animInterpolator) {
        mAnimationHelper.setAnimInterpolator(animInterpolator);
    }

    public void setAnimType(int animType) {
        mAnimationHelper.setAnimType(animType);
    }

}
