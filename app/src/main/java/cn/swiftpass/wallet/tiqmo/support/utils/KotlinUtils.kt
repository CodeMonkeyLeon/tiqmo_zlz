package cn.swiftpass.wallet.tiqmo.support.utils

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.text.TextUtils
import android.widget.ImageView
import cn.swiftpass.wallet.tiqmo.R

object KotlinUtils {


    /**
     * 手机是否支持google play
     */
    fun isSupportGooglePlay() = !TextUtils.isEmpty(SpUtils.getInstance().referralCode)


    /**
     * 将png图片变为白色
     */
    fun changePngToWhite(
        context: Context?,
        imageView: ImageView?,
        pngId: Int = 0
    ) {
        context?.let { c ->
            imageView?.let { i ->
                val originalDrawable = c.resources.getDrawable(pngId) // 加载原始图片
                val colorFilter =
                    PorterDuffColorFilter(
                        c.resources.getColor(R.color.white),
                        PorterDuff.Mode.SRC_ATOP
                    ) // 创建颜色过滤器

                val modifiedDrawable = originalDrawable.mutate() // 创建一个可变的 Drawable
                modifiedDrawable.colorFilter = colorFilter // 应用颜色过滤器

                // 将修改后的图片设置为 ImageView 的 src 属性
                i.setImageResource(0) // 首先清除 ImageView 的现有图片
                i.setImageDrawable(modifiedDrawable)
            }
        }
    }

}