package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.makeramen.roundedimageview.RoundedImageView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class CardExpireNoticeDialog extends BottomDialog{

    private Context mContext;

    private ButtonListener buttonListener;

    private String proxyCardNo,cardNo,expireContent;

    private TextView tvExpire,tvCardHideNumber,tvCardHolderName;

    public void setButtonListener(ButtonListener buttonListener) {
        this.buttonListener = buttonListener;
    }

    public void setProxyCardNo(final String proxyCardNo,String expireContent) {
        this.proxyCardNo = proxyCardNo;
        this.expireContent = expireContent;
        if(tvExpire != null){
            tvExpire.setText(expireContent);
        }
    }

    public void setCardHideNumber(final String cardHideNumber,String cardHolderName) {
        if(tvCardHideNumber != null){
            tvCardHideNumber.setText(cardHideNumber);
        }

        if(tvCardHolderName != null){
            tvCardHolderName.setText(cardHolderName);
        }
    }

    public CardExpireNoticeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface ButtonListener {
//        void getCardDetail();
        void copyCardNo(String cardNo);
        void renewCard();
        void keepVirtual();
        void showError(String errorMsg);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_notification_card_expire, null);
        TextView tvContinue = view.findViewById(R.id.tv_continue);
        RoundedImageView ivBanner = view.findViewById(R.id.iv_banner);
        TextView tvCardNumber = view.findViewById(R.id.tv_card_number);
        TextView tvVirtualCard = view.findViewById(R.id.tv_virtual_card);
        tvCardHideNumber = view.findViewById(R.id.tv_card_hide_number);
        tvCardHolderName = view.findViewById(R.id.tv_card_holder_name);
        ImageView ivCopy = view.findViewById(R.id.iv_copy);
        TextView tvExpireTime = view.findViewById(R.id.tv_expire_time);
        tvExpire = view.findViewById(R.id.tv_expire);
        TextView tvCvv = view.findViewById(R.id.tv_cvv);
        TextView tvViewDetails = view.findViewById(R.id.tv_view_details);
        TextView tvKeepVirtual = view.findViewById(R.id.tv_keep_virtual);
        TextView tvRenewCard = view.findViewById(R.id.tv_renew_card);
        ConstraintLayout llViewDetails = view.findViewById(R.id.ll_view_details);

        tvViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                AppClient.getInstance().getCardManager().getKsaCardDetails(proxyCardNo, new ResultCallback<CardDetailEntity>() {
                    @Override
                    public void onResult(CardDetailEntity cardDetailEntity) {
                        showProgress(false);
                        llViewDetails.setVisibility(View.GONE);
                        if(cardDetailEntity != null) {
                            cardNo = cardDetailEntity.cardNo;
                            tvCardNumber.setText(AndroidUtils.showCardNum(cardDetailEntity.cardNo));
                            tvCvv.setText(cardDetailEntity.cvv2);
                            tvExpireTime.setText(cardDetailEntity.expireDate);
                        }
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                        llViewDetails.setVisibility(View.VISIBLE);
                        if(buttonListener != null) {
                            buttonListener.showError(errorMsg);
                        }
                    }
                });
            }
        });

        ivCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(buttonListener != null){
                    buttonListener.copyCardNo(cardNo);
                }
            }
        });
        tvRenewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonListener != null) {
                    buttonListener.renewCard();
                }
            }
        });
        tvKeepVirtual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonListener != null) {
                    buttonListener.keepVirtual();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
