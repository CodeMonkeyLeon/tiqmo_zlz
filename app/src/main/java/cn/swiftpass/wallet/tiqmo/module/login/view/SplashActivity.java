package cn.swiftpass.wallet.tiqmo.module.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.login.contract.SplashContract;
import cn.swiftpass.wallet.tiqmo.module.login.presenter.SplashPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.TiqmoLaunchSDK;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.entity.DeepBaseEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class SplashActivity extends BaseCompatActivity<SplashContract.Presenter> implements SplashContract.View{
    private static final String TAG = "SplashActivity";

    private static final int ANIM_TIME = 500;
    @BindView(R.id.iv_splash)
    ImageView splashIv;
    private boolean isDeepLink;
    private DeepBaseEntity deepEntity;

    protected int getLayoutID() {
        return R.layout.activity_splash;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        SpUtils.getInstance().setDeviceToken("");
        UserInfoManager.getInstance().setmGcmDeviceToken("");
        if (getUserInfoEntity() == null) {
            SpUtils.getInstance().setAreaEntity(null);
        }

//        //闪屏也全屏显示
//        setSystemUiMode(SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION);
        //解决按home按键退到后台 然后点击桌面图标app重启问题
        if (!isTaskRoot()) {
            Intent intent = getIntent();
            String action = intent.getAction();
            String data = intent.getDataString();
            if(!TextUtils.isEmpty(data) && data.contains("openType=1")){
                Intent intent1 = getPackageManager().getLaunchIntentForPackage("cn.swiftpass.wallet.tiqmo");
                startActivity(intent1);
                finish();
                return;
            }
            else if (intent.hasCategory(Intent.CATEGORY_LAUNCHER)
                    && action != null && action.equals(Intent.ACTION_MAIN)) {
                finish();
                return;
            }
        }
//        initWalletSDK();
        isDeepLink = false;
        deepEntity = null;
        if (getIntent() != null) {
            isDeepLink = getIntent().getBooleanExtra(Constants.IS_DEEP_LINK, false);
            LogUtils.d(TAG, "ceshi" + isDeepLink);
            if (isDeepLink) {
                deepEntity = (DeepBaseEntity) getIntent().getSerializableExtra(Constants.DYNAMICQRCODEENTITY);
            }
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        if (task.getResult() != null) {
                            String token = task.getResult().getToken();
                            UserInfoManager.getInstance().setmGcmDeviceToken(token);
                            if (!TextUtils.isEmpty(token)) {
                                SpUtils.getInstance().setDeviceToken(token);
                            }
                            LogUtils.i(TAG, "onMessageReceived: " + token);
                        }
                    }
                });
        startSplashAnim();

        mPresenter.getConfig();

        if(SpUtils.getInstance().getLanguageListEntity() != null) {
            checkLanguageUpdate(new ResultCallback<LanguageListEntity>() {
                @Override
                public void onResult(LanguageListEntity languageListEntity) {
                    if(languageListEntity != null) {
                        LanguageCodeEntity languageCodeEntity = SpUtils.getInstance().getLanguageCodeEntity();
                        if (languageCodeEntity != null) {
                            String key = languageCodeEntity.key;
                            Locale locale;
                            if ("en_US".equals(key)) {
                                locale = Locale.ENGLISH;
                                changeLanguage(locale);
                            } else {
                                locale = languageCodeEntity.locale;
                                changeLanguage(locale);
                            }
                            LocaleUtils.switchLocale(mContext, key, languageCodeEntity.rTL);
                        }
                    }
                }

                @Override
                public void onFailure(String errorCode, String errorMsg) {

                }
            });
        }
    }

    private void startSplashAnim() {
        Animation ani = new AlphaAnimation(0.5f, 1.0f);
        ani.setDuration(ANIM_TIME);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isDeepLink) {
                    toDeepTab();
                } else {
                    toMainTab();
                }

            }
        });
        splashIv.startAnimation(ani);
    }

    private void toDeepTab() {
    }


    private void toMainTab() {
        ActivitySkipUtil.finishActivityWithAnim(this, ActivitySkipUtil.ANIM_TYPE.LEFT_OUT);

        Intent intent = getIntent();
        String data = intent.getDataString();
        if(!TextUtils.isEmpty(data) && data.contains("tiqmolink.page.link")){
            ActivitySkipUtil.startAnotherActivity(mContext,RegisterActivity.class);
            finish();
            return;
        }

        UserInfoEntity user = AppClient.getInstance().getUserManager().getUserInfo();
//        user = new UserInfoEntity();
        if (user != null) {
//            AppClient.getInstance().getUserManager().bindUserInfo(user);
            HashMap<String, Object> hashMap = new HashMap<>(16);
            hashMap.put(Constants.LAST_USER, user);
            ActivitySkipUtil.startAnotherActivity(SplashActivity.this, LoginFastNewActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.NONE);
        } else {
            ActivitySkipUtil.startAnotherActivity(SplashActivity.this, ChooseLanguageActivity.class);
        }
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.none, R.anim.none);
    }

    @Override
    public SplashContract.Presenter getPresenter() {
        return new SplashPresenter();
    }

    @Override
    public void notifyByThemeChanged() {
        super.notifyByThemeChanged();

    }

    @Override
    public void getConfigSuccess(ConfigEntity configEntity) {
        if (configEntity != null) {
            TiqmoLaunchSDK.showUpdateIfNeeded(mContext, configEntity);
        }
    }
}
