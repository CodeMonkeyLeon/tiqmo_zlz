package cn.swiftpass.wallet.tiqmo.support.deviceinfo;

import android.content.Context;
import android.os.Build;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.lang.reflect.Method;

import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

/**
 * Created by congwei.li on 2021/8/10.
 *
 * @Description:
 */
public class DeviceUtils {
    //判断是否是华为系统 官网提供
    public static boolean isEMUI(){
        //emuiApiLevel>0 即华为系统
        int emuiApiLevel = 0;
        try {
            Class cls = Class.forName("android.os.SystemProperties");
            Method method = cls.getDeclaredMethod("get", new Class[]{String.class});
            emuiApiLevel = Integer.parseInt((String) method.invoke(cls, new Object[]{"ro.build.hw_emui_api_level"}));
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return emuiApiLevel > 0;
    }
    //判断是否是华为系统 官网提供
    public static boolean isSupportGooglePlay(Context context){
        //emuiApiLevel>0 即华为系统
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        LogUtil.i("google play code: " + code);
        return code == ConnectionResult.SUCCESS;
    }
    //判官是否是小米系统 官网提供
    public static  boolean isMIUI(){
        String manufacturer = Build.MANUFACTURER;
        //这个字符串可以自己定义,例如判断华为就填写huawei,魅族就填写meizu
        if ("xiaomi".equalsIgnoreCase(manufacturer)) {
            return true;
        }
        return false;
    }
}
