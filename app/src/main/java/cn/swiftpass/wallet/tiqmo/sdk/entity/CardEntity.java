package cn.swiftpass.wallet.tiqmo.sdk.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CardEntity extends BaseEntity {
    private String protocolNo;
    private String accountId;
    private String cardNo;
    private String cardType;
    private String cardTypeDesc;
    private String bankName;
    public int resId;
    public String showCardNo;
    public String unMaskCardNo;

    private String custName;
    private String expire;
    private String cardOrganization;
    private String cardCategory;
    private String cvv;
    private String defaultCardInd;


    private String cardId;
    private String paymentLimit;

    private String bankLogo;
    private String isDefault;

    private String bindTime;
    private String cardFaceBackColor;
    private String cardFace;
    //卡是否过期 0-未过期，1-已过期
    public String expireFlag;
    //是否需要填写cvv 标识 1：需要 0: 不需要
    public String needCvvFlag;

    /** 卡面mini*/
    private String cardFaceMini;
    /** 卡背面*/
    private String cardFaceBack;
    /** 卡默认卡*/
    private String cardFacebar;

    public String getProtocolNo() {
        return protocolNo;
    }

    public void setProtocolNo(String protocolNo) {
        this.protocolNo = protocolNo;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardTypeDesc() {
        return cardTypeDesc;
    }

    public void setCardTypeDesc(String cardTypeDesc) {
        this.cardTypeDesc = cardTypeDesc;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getCardOrganization() {
        return cardOrganization;
    }

    public void setCardOrganization(String cardOrganization) {
        this.cardOrganization = cardOrganization;
    }

    public String getCardCategory() {
        return cardCategory;
    }

    public void setCardCategory(String cardCategory) {
        this.cardCategory = cardCategory;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getDefaultCardInd() {
        return defaultCardInd;
    }

    public void setDefaultCardInd(String defaultCardInd) {
        this.defaultCardInd = defaultCardInd;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPaymentLimit() {
        return paymentLimit;
    }

    public void setPaymentLimit(String paymentLimit) {
        this.paymentLimit = paymentLimit;
    }

    public String getBankLogo() {
        return bankLogo;
    }

    public void setBankLogo(String bankLogo) {
        this.bankLogo = bankLogo;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getBindTime() {
        return bindTime;
    }

    public void setBindTime(String bindTime) {
        this.bindTime = bindTime;
    }

    public String getCardFaceBackColor() {
        return cardFaceBackColor;
    }

    public void setCardFaceBackColor(String cardFaceBackColor) {
        this.cardFaceBackColor = cardFaceBackColor;
    }

    public String getCardFace() {
        return cardFace;
    }

    public void setCardFace(String cardFace) {
        this.cardFace = cardFace;
    }

    public String getCardFaceMini() {
        return cardFaceMini;
    }

    public void setCardFaceMini(String cardFaceMini) {
        this.cardFaceMini = cardFaceMini;
    }

    public String getCardFaceBack() {
        return cardFaceBack;
    }

    public void setCardFaceBack(String cardFaceBack) {
        this.cardFaceBack = cardFaceBack;
    }

    public String getCardFacebar() {
        return cardFacebar;
    }

    public void setCardFacebar(String cardFacebar) {
        this.cardFacebar = cardFacebar;
    }

}
