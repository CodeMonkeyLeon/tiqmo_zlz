package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PayBillSkuListAdapter extends BaseRecyclerAdapter<PayBillSkuEntity> {
    //1为plan 2为branch
    private int type = 2;

    public void setType(final int type) {
        this.type = type;
    }

    public PayBillSkuListAdapter(@Nullable List<PayBillSkuEntity> data) {
        super(R.layout.item_paybill_select_sku, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillSkuEntity payBillSkuEntity, int position) {
        if (type == 2) {
            baseViewHolder.setGone(R.id.ll_branch, false);
            baseViewHolder.setGone(R.id.con_plan, true);
        } else {
            baseViewHolder.setGone(R.id.ll_branch, true);
            baseViewHolder.setGone(R.id.con_plan, false);
        }
        baseViewHolder.setText(R.id.tv_sku, payBillSkuEntity.description);
        baseViewHolder.setText(R.id.tv_sku_title, payBillSkuEntity.description);
        baseViewHolder.setText(R.id.tv_sku_amount, payBillSkuEntity.amount);
        baseViewHolder.setText(R.id.tv_sku_currency, payBillSkuEntity.currency);
    }
}
