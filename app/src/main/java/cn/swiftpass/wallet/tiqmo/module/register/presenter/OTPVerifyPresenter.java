package cn.swiftpass.wallet.tiqmo.module.register.presenter;


import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public interface OTPVerifyPresenter<V extends BaseView> extends BasePresenter<V> {
    void verifyOTP(String callingCode, String phone, String otp,String type,String orderNo);
}
