package cn.swiftpass.wallet.tiqmo.module.voucher;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;

public class VoucherPayKycDiaFrg extends DialogFragment {

    public Context mContext;

    private UserInfoEntity userInfoEntity;

    public void setUserInfoEntity(UserInfoEntity userInfoEntity) {
        this.userInfoEntity = userInfoEntity;
    }

    public static VoucherPayKycDiaFrg newInstance() {
        VoucherPayKycDiaFrg fragment = new VoucherPayKycDiaFrg();
        Bundle bundle = new Bundle();
//        bundle.putSerializable("eVoucher", eVoucherEntity);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //这里new一个FrameLayout只是用来inflate中的参数，不然layout中得参数无法解析
        if (container == null) {
            container = new FrameLayout(mContext);
        }
        initWinLayoutParams();

        View rootView = inflater.inflate(R.layout.dialog_voucher_kyc, container, false);
        View head_view = rootView.findViewById(R.id.head_view);
        View foot_view = rootView.findViewById(R.id.foot_view);
        TextView tv_complete_kyc = rootView.findViewById(R.id.tv_complete_kyc);
        tv_complete_kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                EventBus.getDefault().post(new EventEntity(EventEntity.START_KYC));
            }
        });
        head_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        foot_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return rootView;
    }

    private void initWinLayoutParams() {
        if (getDialog() == null || getDialog().getWindow() == null) {
            return;
        }
        // 初始化window布局参数
        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(R.color.transparent);
        window.setDimAmount(0.7f);
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(wlp);
    }
}
