package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeOrderDetailEntity extends BaseEntity {

    /**
     * {\"userShortName\":\"Dpp\",\"recordId\":3,\"callingCode\":\"966\",\"isValidBiller\":\"Y\",\"partnerNo\":\"96601\",\"orgNo\":\"96601\",\"phone\":\"500510488\",
     * \"countryCode\":\"IND\",\"operatorName\":\"Airtel India\",\"operatorId\":\"1371\",\"userId\":\"96601000341629095325758\"}}"
     */

    //是否是有效的数据 Y：有效 N：无效
    public String isValidBiller;
    //偏好记录ID
    public String recordId;
    //国家代码 ISO三位码
    public String countryCode;

    public String countryName;
    //用户简称
    public String userShortName;
    //运营商ID
    public String operatorId;
    //运营商名称
    public String operatorName;
    //国家冠号
    public String callingCode;
    //手机号，加密存储
    public String phone;

    public String operatorImgUrl;
}
