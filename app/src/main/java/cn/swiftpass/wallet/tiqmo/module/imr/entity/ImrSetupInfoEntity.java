package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrSetupInfoEntity extends BaseEntity {
    public String sourceOfFund;
    public String profession;
    public String companyType;
    public String companyName;
    public String salaryRange;
    public String sourceOfFundCode;
    public String professionCode;
    public String businessTypeCode;
    public String salaryRangeCode;

    public boolean isFillAllInfo() {
        return !TextUtils.isEmpty(sourceOfFundCode) && !TextUtils.isEmpty(professionCode)
                && !TextUtils.isEmpty(businessTypeCode) && !TextUtils.isEmpty(companyName)
                && !TextUtils.isEmpty(salaryRangeCode);
    }
}
