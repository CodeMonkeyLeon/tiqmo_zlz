/*
 * Copyright (C) 2010 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.swiftpass.wallet.tiqmo.support.zxing.Decoding;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.util.Map;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;


final class DecodeHandler extends Handler {

    private static final String TAG = DecodeHandler.class.getSimpleName();
//    private int frameCount;
    private final QrcodeScanListener activity;

    private final MultiFormatReader multiFormatReader;
    private Rect frameRect;

    public DecodeHandler(QrcodeScanListener activity, Map<DecodeHintType, Object> hints) {
        multiFormatReader = new MultiFormatReader();
        multiFormatReader.setHints(hints);
        this.activity = activity;

    }

    @Override
    public void handleMessage(Message message) {
//        LogUtils.i(TAG,"handleMessage:"+message.what);
        switch (message.what) {
            case R.id.decode:
                //Log.d(TAG, "Got decode message");
                decode((byte[]) message.obj, message.arg1, message.arg2);
                break;
            case R.id.quit:
                Looper.myLooper().quit();
                break;
            default:
                break;
        }
    }

    /**
     * Decode the data within the viewfinder rectangle, and time how long it took. For efficiency,
     * reuse the same reader objects from one decode to the next.
     *
     * @param data   The YUV preview frame.
     * @param width  The width of the preview frame.
     * @param height The height of the preview frame.
     */
    private void decode(byte[] data, int width, int height) {
        long start = System.currentTimeMillis();
        Result rawResult = null;


        byte[] rotatedData = new byte[data.length];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rotatedData[x * height + height - y - 1] = data[x + y * width];
            }
        }
//        frameCount++;
//        //丢弃前2帧并每隔2帧分析下预览帧color值
//        if (frameCount > 2 && frameCount % 2 == 0) {
//            frameRect = activity.getCameraManager().getFramingRect();
//            analysisBitmapColor(data, width, height);
//        }
        int tmp = width; // Here we are swapping, that's the difference to #11
        width = height;
        height = tmp;
//        PlanarYUVLuminanceSource source = activity.getCameraManager().buildLuminanceSource(rotatedData, width, height);
//        PlanarYUVLuminanceSource source = activity.getCameraManager().buildLuminanceSource(data, width, height);
        PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(rotatedData, width, height, 0, 0, width, height, false);

        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        try {
            rawResult = multiFormatReader.decodeWithState(bitmap);
        } catch (ReaderException re) {
            LogUtils.d(TAG, "---" + re + "---");
            // continue
        } finally {
            multiFormatReader.reset();
        }

        if (rawResult != null) {
            long end = System.currentTimeMillis();
            LogUtils.d(TAG, "Found barcode (" + (end - start) + " ms):\n" + rawResult.toString());
            Message message = Message.obtain(activity.getHandler(), R.id.decode_succeeded, rawResult);
            Bundle bundle = new Bundle();
            message.setData(bundle);
            message.sendToTarget();
        } else {
            Message message = Message.obtain(activity.getHandler(), R.id.decode_failed);
            message.sendToTarget();
        }
    }

    /**
     * 弱光检测
     *
     * @param data
     * @param width
     * @param height
     */
//    private void analysisBitmapColor(byte[] data, int width, int height) {
//        int[] rgb = decodeYUV420SP(data, width, height);
//        Bitmap bmp = null;
//        if (null != frameRect) {
//            //取矩形扫描框frameRect的2分之一创建为bitmap来分析
//            bmp = Bitmap.createBitmap(rgb, frameRect.left + (frameRect.right - frameRect.left) / 4, frameRect.width() / 2, frameRect.width() / 2, frameRect.height() / 2, Bitmap.Config.ARGB_4444);
//        }
//        if (bmp != null) {
//            float color = getAverageColor(bmp);
//            DecimalFormat decimalFormat1 = new DecimalFormat("0.00");
//            String percent = decimalFormat1.format(color / -16777216);
//            float floatPercent = Float.parseFloat(percent);
//            Log.e(TAG, " color= " + color + " floatPercent= " + floatPercent + " bmp width= " + bmp.getWidth() + " bmp height= " + bmp.getHeight());
//            ZxingConst.isWeakLight = (color == -16777216 || (floatPercent >= 0.95 && floatPercent <= 1.00));
//            bmp.recycle();
//        }
//    }

//    private int getAverageColor(Bitmap bitmap) {
//        int redBucket = 0;
//        int greenBucket = 0;
//        int blueBucket = 0;
//        int pixelCount = 0;
//
//        for (int y = 0; y < bitmap.getHeight(); y++) {
//            for (int x = 0; x < bitmap.getWidth(); x++) {
//                int c = bitmap.getPixel(x, y);
//
//                pixelCount++;
//                redBucket += Color.red(c);
//                greenBucket += Color.green(c);
//                blueBucket += Color.blue(c);
//            }
//        }
//        int averageColor = Color.rgb(redBucket / pixelCount, greenBucket / pixelCount, blueBucket / pixelCount);
//        return averageColor;
//    }

//    private int[] decodeYUV420SP(byte[] yuv420sp, int width, int height) {
//        final int frameSize = width * height;
//
//        int rgb[] = new int[width * height];
//        for (int j = 0, yp = 0; j < height; j++) {
//            int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
//            for (int i = 0; i < width; i++, yp++) {
//                int y = (0xff & ((int) yuv420sp[yp])) - 16;
//                if (y < 0) {
//                    y = 0;
//                }
//                if ((i & 1) == 0) {
//                    v = (0xff & yuv420sp[uvp++]) - 128;
//                    u = (0xff & yuv420sp[uvp++]) - 128;
//                }
//
//                int y1192 = 1192 * y;
//                int r = (y1192 + 1634 * v);
//                int g = (y1192 - 833 * v - 400 * u);
//                int b = (y1192 + 2066 * u);
//
//                if (r < 0) {
//                    r = 0;
//                } else if (r > 262143) {
//                    r = 262143;
//                }
//                if (g < 0) {
//                    g = 0;
//                } else if (g > 262143) {
//                    g = 262143;
//                }
//                if (b < 0) {
//                    b = 0;
//                } else if (b > 262143) {
//                    b = 262143;
//                }
//
//                rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
//
//
//            }
//        }
//        return rgb;
//    }

}
