package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.contract.SplitBillContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SendReceiveDetailPresenter implements SplitBillContract.SendReceiveDetailPresenter {
    SplitBillContract.SendReceiveDetailView mView;

    @Override
    public void sendSplitsRemind(String originalOrderNo, String orderRelevancy) {
        AppClient.getInstance().sendSplitsRemind(originalOrderNo,orderRelevancy, new LifecycleMVPResultCallback<Void>(mView,true) {

            @Override
            protected void onSuccess(Void result) {
                if (mView != null) {
                    mView.sendSplitsRemindSuccess();
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showError(errorCode,errorMsg);
                }
            }
        });
    }

    @Override
    public void ReceiveSplitBillDetail(String orderNo) {
        AppClient.getInstance().splitBillPayDetail(orderNo, new LifecycleMVPResultCallback<RequestCenterEntity>(mView,true) {
            @Override
            protected void onSuccess(RequestCenterEntity result) {
                if (mView != null) {
                    mView.ReceiveSplitBillDetailSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showError(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void sendNotify(List<NotifyEntity> receivers) {
        AppClient.getInstance().sendNotify(receivers, new LifecycleMVPResultCallback<PayBillOrderInfoEntity>(mView, true) {
            @Override
            protected void onSuccess(PayBillOrderInfoEntity result) {
                if (mView != null) {
                    mView.sendNotifySuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.sendNotifyFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getSplitDetail(String originalOrderNo, String orderRelevancy, String splitType) {
        AppClient.getInstance().getSplitDetail(originalOrderNo,orderRelevancy,splitType ,new LifecycleMVPResultCallback<SendReceiveSplitDetailEntity>(mView,true) {
            @Override
            protected void onSuccess(SendReceiveSplitDetailEntity result) {
                if (mView != null) {
                    mView.getSplitDetailSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showError(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(mView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mView.transferSurePaySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mView.showError(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void attachView(SplitBillContract.SendReceiveDetailView sendReceiveDetailView) {
        this.mView = sendReceiveDetailView;
    }

    @Override
    public void detachView() {
        this.mView = null;
    }


}
