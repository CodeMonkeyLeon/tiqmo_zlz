package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view

import android.app.Activity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity
import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter
import cn.swiftpass.wallet.tiqmo.base.view.BaseView
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil
import cn.swiftpass.wallet.tiqmo.widget.MyTextView

/**
 * @author lizheng.zhao
 * @date 2023/06/08
 * @description 卡片介绍
 */
class CardIntroductionActivity : BaseCompatActivity<BasePresenter<BaseView<*>>>(),
    View.OnClickListener {

    override fun getLayoutID() = R.layout.act_card_introduction

    /**
     * 静态变量和静态方法
     */
    companion object {

        const val INTRO_TEXT_TAG = "Lifestyle"

        fun startCardIntroductionActivity(fromActivity: Activity?) {
            fromActivity?.run {
                ActivitySkipUtil.startAnotherActivity(
                    this,
                    CardIntroductionActivity::class.java,
                    ActivitySkipUtil.ANIM_TYPE.RIGHT_IN
                )
            }
        }
    }


    private val mTvTitle by lazy {
        R.id.tv_title.getView<MyTextView>()
    }


    private val mImgBack by lazy {
        R.id.iv_back.getView<ImageView>()
    }


    private val mTvCardIntroduction by lazy {
        R.id.id_tv_card_introduction.getView<MyTextView>()
    }

    private val mBtnConfirm by lazy {
        R.id.id_tv_confirm.getView<MyTextView>()
    }


    override fun init(savedInstanceState: Bundle?) {
        setDarkBar()
        fixedSoftKeyboardSliding()

        initView()
    }


    val initView = {
        //标题title
        mTvTitle.setText(R.string.DigitalCard_0)
        mTvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 23f)
        mTvTitle.setFontStyle(3)

        val layoutParam = mTvTitle.layoutParams as ConstraintLayout.LayoutParams
        layoutParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        mTvTitle.layoutParams = layoutParam


        //返回按钮
        mImgBack.setImageResource(R.drawable.l_icon_card_intro_close)
        mImgBack.setOnClickListener(this)
        //确认按钮
        mBtnConfirm.setOnClickListener(this)


        //介绍文字
        val introText = getString(R.string.sprint21_24)
        if (!TextUtils.isEmpty(introText) && introText.contains(INTRO_TEXT_TAG)) {
            val startIndex = introText.indexOf(INTRO_TEXT_TAG)
            val endIndex = startIndex + INTRO_TEXT_TAG.length

            //Spanned.SPAN_INCLUSIVE_EXCLUSIVE --- [start, end)
            val spannableString = SpannableString(introText)
            spannableString.setSpan(
                ForegroundColorSpan(getColor(R.color.color_fc4f00)),
                startIndex,
                endIndex,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE
            )

            mTvCardIntroduction.text = spannableString
        }


    }


    override fun onClick(v: View?) {
        v?.run {
            when (id) {
                R.id.id_tv_confirm -> {
                    //点击确认
                    ChooseCardTypeStandardPlatinumActivity.startChooseCardTypeStandardPlatinumActivity(
                        this@CardIntroductionActivity
                    )
                }

                R.id.iv_back -> {
                    //点击关闭
                    finish()
                }

                else -> {

                }
            }
        }
    }


    private fun <T : View> Int.getView(): T {
        return findViewById<T>(this)
    }


}