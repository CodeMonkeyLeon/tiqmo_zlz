package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.contract.HomeFragmentContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HomeAnalysisEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.TopUpSelectActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherSelectActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.OrderExistEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.HomeIconEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;

public class HomeFragmentPresenter implements HomeFragmentContract.Presenter {
    private HomeFragmentContract.View mBaseView;

    public static final String Y = "Y"; // have limit country or order exits
    public static final String N = "N"; // no limit country or order un exits
    public static final String SCENE_TYPE_VOUCHER = "15";
    public static final String SCENE_TYPE_TOP_UP = "5";

    @Override
    public void attachView(HomeFragmentContract.View view) {
        this.mBaseView = view;
    }

    @Override
    public void detachView() {
        this.mBaseView = null;
    }

    @Override
    public void getAnalysisDetail(String periodType, String spendingAnalysisQueryDate) {
        AppClient.getInstance().getTransferManager().getAnalysisDetail(periodType, spendingAnalysisQueryDate, new LifecycleMVPResultCallback<SpendingDetailEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(SpendingDetailEntity result) {
                mBaseView.getAnalysisDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getHomeAnalysis() {
        AppClient.getInstance().getTransferManager().getHomeAnalysis(new LifecycleMVPResultCallback<HomeAnalysisEntity>(mBaseView, false) {
            @Override
            protected void onSuccess(HomeAnalysisEntity result) {
                mBaseView.getHomeAnalysisSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getUserInfo(boolean isLoading) {
        AppClient.getInstance().getUserManager().getUserInfo(new LifecycleMVPResultCallback<UserInfoEntity>(mBaseView, isLoading) {
            @Override
            protected void onSuccess(UserInfoEntity result) {
                mBaseView.getUserInfoSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void autoLogin() {

    }

    @Override
    public void getConfig() {
        AppClient.getInstance().getUserManager().getConfig(null);
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }


    @Override
    public void getAllMarket() {
        AppClient.getInstance().requestAllMarket(new LifecycleMVPResultCallback<AllMarketInfoEntity>(mBaseView, false) {
            @Override
            protected void onSuccess(AllMarketInfoEntity result) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.showMarketList(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getMarketDetails(String activityNo) {
        AppClient.getInstance().requestMarketDetails(activityNo, new LifecycleMVPResultCallback<MarketDetailsEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(MarketDetailsEntity result) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.showMarketDetailsDia(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getRefCodeDescDetails() {
        AppClient.getInstance().requestRefCodeDesc("REFERRAL_CODE_ACTIVITY", new LifecycleMVPResultCallback<ReferralCodeDescEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(ReferralCodeDescEntity result) {
                if (result == null) {
                    return;
                }
                mBaseView.showMarketDetailsForRefCodeDia(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void requestVoucherBrand() {
        AppClient.getInstance().requestVoucherBrand(new LifecycleMVPResultCallback<EVoucherEntity>(mBaseView, false) {
            @Override
            protected void onSuccess(EVoucherEntity result) {
                if (result == null || mBaseView == null) {
                    return;
                }
                mBaseView.showVoucherBrandList(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.hideVoucherBrandList(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void requestSupCountry(String providerCode, String brandCode, String brandName) {
        AppClient.getInstance().requestVouSupCountry(providerCode, brandCode,
                new LifecycleMVPResultCallback<VouSupCountryEntity>(mBaseView, true) {
                    @Override
                    protected void onSuccess(VouSupCountryEntity result) {
                        if (mBaseView == null) {
                            return;
                        }
                        mBaseView.showEVouCountryDia(result, providerCode, brandCode, brandName);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (mBaseView == null) {
                            return;
                        }
                        mBaseView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }


    @Override
    public void getTopUpList() {
        AppClient.getInstance().requestVoucherBrand(SCENE_TYPE_TOP_UP, new LifecycleMVPResultCallback<EVoucherEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(EVoucherEntity result) {
                if (result == null || mBaseView == null) {
                    return;
                }
                mBaseView.showTopUpSelectDia(result.voucherBrandOne);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) {
                    return;
                }
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOrderStatus(ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType) {
        mBaseView.showProgress(true);
        AppClient.getInstance().checkOrderExist(data.get(position).voucherServiceProviderCode,
                data.get(position).voucherBrandCode, sceneType,
                new LifecycleMVPResultCallback<OrderExistEntity>(mBaseView, false) {
                    @Override
                    protected void onSuccess(OrderExistEntity result) {
                        if (result == null || mBaseView == null) {
                            return;
                        }
                        if (Y.equals(result.voucherOrderIsProcessingFlag)) {
                            mBaseView.showProgress(false);
                            mBaseView.showOrderRepeatDia(data, position, sceneType);
                        } else if (N.equals(result.voucherOrderIsProcessingFlag)) {
                            mBaseView.checkOrderStatusSuccess(data, position, sceneType);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (mBaseView == null) {
                            return;
                        }
                        mBaseView.showProgress(false);
                        mBaseView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void skipToVouTypeAndInfo(Activity activity, String providerCode, String brandCode, String brandName, String countryCode) {
        HashMap<String, String> map = new HashMap<>();
        map.put("providerCode", providerCode);
        map.put("brandCode", brandCode);
        map.put("brandName", brandName);
        map.put("countryCode", countryCode);
        ActivitySkipUtil.startAnotherActivity(activity, EVoucherSelectActivity.class, map,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void continueToSelectTickets(Activity activity, ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType) {
        if (SCENE_TYPE_VOUCHER.equals(sceneType)) {// EVoucher
            EVoucherEntity.StoreInfo storeInfo = data.get(position);
            if (storeInfo == null) {
                return;
            }
            if (storeInfo.limitCountry.equals(Y)) {
                requestSupCountry(storeInfo.voucherServiceProviderCode,
                        storeInfo.voucherBrandCode, storeInfo.voucherBrandName);
            } else if (data.get(position).limitCountry.equals(N)) {
                mBaseView.showProgress(false);
                skipToVouTypeAndInfo(activity, storeInfo.voucherServiceProviderCode,
                        storeInfo.voucherBrandCode, storeInfo.voucherBrandName, "");
            }
        } else if (SCENE_TYPE_TOP_UP.equals(sceneType)) {// TopUp
            mBaseView.showProgress(false);
            if (data.get(position) != null) {
                ActivitySkipUtil.startAnotherActivity(activity, TopUpSelectActivity.class, "TopUpInfoList", data.get(position));
            }
        }
    }

    @Override
    public void getServicesList() {
        List<HomeIconEntity> iconList = new ArrayList<>();
        iconList.add(new HomeIconEntity(R.attr.home_service_add_money, R.string.add_money_1));
        iconList.add(new HomeIconEntity(R.attr.home_service_transfer_money, R.string.wtw_1));
        iconList.add(new HomeIconEntity(R.attr.home_service_request_money, R.string.sprint20_69));
        iconList.add(new HomeIconEntity(R.attr.home_service_imr, R.string.sprint20_107));
        iconList.add(new HomeIconEntity(R.attr.home_service_split_bill, R.string.newhome_11));
        iconList.add(new HomeIconEntity(R.attr.home_service_bill_payments, R.string.newhome_10));
        iconList.add(new HomeIconEntity(R.attr.home_service_mobile_topup, R.string.newhome_8));
        iconList.add(new HomeIconEntity(R.attr.home_service_donation, R.string.wtba_35));
        iconList.add(new HomeIconEntity(R.attr.home_service_market_place, R.string.newhome_13));
        mBaseView.showServicesList(iconList);
    }

    @Override
    public void getPayBillOrderList() {
        AppClient.getInstance().getTransferManager().getHomeBillOrderList(new LifecycleMVPResultCallback<PayBillOrderListEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(PayBillOrderListEntity result) {
                if (mBaseView != null) {
                    mBaseView.getPayBillOrderListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView != null) {
                    mBaseView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getTopUpOrderList() {
        AppClient.getInstance().getTransferManager().getRechargeOrderList(new LifecycleMVPResultCallback<RechargeOrderListEntity>(mBaseView,true) {
            @Override
            protected void onSuccess(RechargeOrderListEntity result) {
                if (mBaseView != null) {
                    mBaseView.getTopUpOrderListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView != null) {
                    mBaseView.showErrorMsg(errorCode,errorMsg);
                }
            }
        });
    }

    @Override
    public void getInviteDetail() {
        AppClient.getInstance().getInviteDetail(new LifecycleMVPResultCallback<InviteEntity>(mBaseView,true) {
            @Override
            protected void onSuccess(InviteEntity inviteEntity) {
                if (mBaseView != null) {
                    mBaseView.getInviteDetail(inviteEntity);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView != null) {
                    mBaseView.showErrorMsg(errorCode,errorMsg);
                }
            }
        });
    }

    @Override
    public void getSendReceive() {
        AppClient.getInstance().getSendReceive(new LifecycleMVPResultCallback<List<SendReceiveEntity>>(mBaseView,false) {
            @Override
            protected void onSuccess(List<SendReceiveEntity> result) {
                if (mBaseView != null) {
                    mBaseView.getSendReceiveSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView != null) {
                    mBaseView.showErrorMsg(errorCode,errorMsg);
                }
            }
        });
    }
}
