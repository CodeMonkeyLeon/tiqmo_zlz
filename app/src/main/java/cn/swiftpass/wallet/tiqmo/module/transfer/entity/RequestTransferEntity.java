package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RequestTransferEntity extends BaseEntity {
   public String chatId;
   public String payerUserId;
   public String callingCode;
   public String payerNum;
   public String payeeUserId;
   public String payeeNum;
   public String payeeAmount;
   public String payeeCurrencyCode;
   public String remark;
   public String sceneType;
   public String transTimeType;
   public String payeeNumberType;
   public String transferPurpose;
   public String transFees;
   public String vat;
   public String payerName;
}
