package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class EVoucherMoneyDialog extends BottomDialog {

    private Context mContext;
    private static EVoucherMoneyDialog eVoucherMoneyDialog = null;

    private EVoucherMoneyPayListener eVoucherMoneyPayListener;

    private double eVoucherMoney;
    private int minInputAmount;
    private int maxInputAmount;

    private VouTypeAndInfoEntity.VouTypeAndInfo vouTypeAndInfo;

    EditTextWithDel etEvoucherAmount;
    TextView tvEvoucherPay;
    TextView tvMoneyCurrency;
    TextView tv_content;

    public void setVouTypeAndInfo(VouTypeAndInfoEntity.VouTypeAndInfo vouTypeAndInfo, String brandName) {
        this.vouTypeAndInfo = vouTypeAndInfo;
        if (tv_content != null) {
            tv_content.setText(mContext.getString(R.string.VoucherPrice_1) + " " + brandName + " " + mContext.getString(R.string.VoucherPrice_2));
        }
        try {
            maxInputAmount = (int) (Double.parseDouble(AndroidUtils.getTransferMoney(vouTypeAndInfo.maxInputAmount)));
            minInputAmount = (int) (Double.parseDouble(AndroidUtils.getTransferMoney(vouTypeAndInfo.minInputAmount)));
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public interface EVoucherMoneyPayListener {
        void eVoucherMoneyPay(String eVoucherMoney);
    }

    public void setEVoucherMoneyPayListener(EVoucherMoneyPayListener eVoucherMoneyPayListener) {
        this.eVoucherMoneyPayListener = eVoucherMoneyPayListener;
    }

    public EVoucherMoneyDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public static EVoucherMoneyDialog getInstance(Context context) {
        eVoucherMoneyDialog = new EVoucherMoneyDialog(context);
        return eVoucherMoneyDialog;
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_evoucher_money, null);
        etEvoucherAmount = view.findViewById(R.id.et_evoucher_amount);
        tvEvoucherPay = view.findViewById(R.id.tv_evoucher_pay);
        tv_content = view.findViewById(R.id.tv_content);
        tvMoneyCurrency = view.findViewById(R.id.tv_money_currency);
        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        etEvoucherAmount.getTlEdit().setHint(mContext.getString(R.string.VoucherPrice_3));
        tvEvoucherPay.setText(mContext.getString(R.string.scan_pay_7) + " " + AmountUtil.dataFormat("") + " " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));

        //两位小数过滤
        etEvoucherAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etEvoucherAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                checkMoney(amount);
                if (TextUtils.isEmpty(amount)) {
                    tvMoneyCurrency.setVisibility(View.GONE);
                    tvEvoucherPay.setEnabled(false);
                } else {
                    try {
                        eVoucherMoney = Double.parseDouble(amount);
                        if (eVoucherMoney > maxInputAmount || eVoucherMoney < minInputAmount) {
                            tvEvoucherPay.setEnabled(false);
                        } else {
                            tvEvoucherPay.setEnabled(true);
                        }
//                        if (eVoucherMoney <= valiableMoney) {
//                            if (eVoucherMoney > 0) {
//                                tvEvoucherPay.setEnabled(true);
//                            }
//                        } else {
//                            if (eVoucherMoney > monthLimitMoney) {
//                            } else {
//                                tvEvoucherPay.setEnabled(false);
//                            }
//                        }
                    } catch (Exception e) {
                        tvEvoucherPay.setEnabled(false);
                    }
                    tvMoneyCurrency.setVisibility(View.VISIBLE);
                }
            }
        });

        etEvoucherAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                String amount = etEvoucherAmount.getEditText().getText().toString().trim();
                amount = amount.replace(",", "");
                String formatAmount = AmountUtil.dataFormat(amount);
                if (!hasFocus) {
                    if (TextUtils.isEmpty(amount)) {
                    } else {
                        etEvoucherAmount.getEditText().setText(formatAmount);
                    }
                    checkMoney(amount);
                }
            }
        });

        tvEvoucherPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eVoucherMoneyPayListener != null) {
                    String payMoney = etEvoucherAmount.getEditText().getText().toString().trim();
                    payMoney = payMoney.replace(",", "");
                    eVoucherMoneyPayListener.eVoucherMoneyPay(payMoney);
                }
                dismiss();
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (eVoucherMoneyDialog != null) {
                    eVoucherMoneyDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private boolean checkMoney(String amount) {
        if (TextUtils.isEmpty(amount)) {
            eVoucherMoney = 0d;
        } else {
            eVoucherMoney = Double.parseDouble(amount);
        }
        tvEvoucherPay.setText(mContext.getString(R.string.scan_pay_7) + " " + AmountUtil.dataFormat(amount) + " " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
        String errMsg = mContext.getString(R.string.VoucherPrice_4).replace("XXX", minInputAmount + "-" + maxInputAmount + " " + LocaleUtils.getCurrencyCode(""));
        if (eVoucherMoney > maxInputAmount || eVoucherMoney < minInputAmount) {
            etEvoucherAmount.showErrorViewWithMsg(errMsg);
            return false;
        }
        return true;
//        else if (eVoucherMoney <= valiableMoney) {
//            etEvoucherAmount.showErrorViewWithMsg("");
//        } else {
//            etEvoucherAmount.showErrorViewWithMsg(mContext.getString(R.string.wtw_25));
//        }
    }
}
