package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.TransferRelationDetailAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.BeneficiaryListPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.adapter.ItemTouchHelperListener;
import cn.swiftpass.wallet.tiqmo.widget.adapter.SimpleItemTouchHelperCallback;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class BeneficiaryListActivity extends BaseCompatActivity<TransferContract.BeneficiaryListPresenter> implements TransferContract.BeneficiaryListView, ItemTouchHelperListener {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_add_new)
    TextView tvAddNew;
    @BindView(R.id.common_head)
    RelativeLayout commonHead;
    @BindView(R.id.iv_account_arrow)
    ImageView ivAccountArrow;
    @BindView(R.id.ll_my_account)
    LinearLayout llMyAccount;
    @BindView(R.id.rl_account)
    RecyclerView rlAccount;
    @BindView(R.id.iv_family_arrow)
    ImageView ivFamilyArrow;
    @BindView(R.id.ll_family)
    LinearLayout llFamily;
    @BindView(R.id.rl_family)
    RecyclerView rlFamily;
    @BindView(R.id.iv_friend_arrow)
    ImageView ivFriendArrow;
    @BindView(R.id.ll_friend)
    LinearLayout llFriend;
    @BindView(R.id.rl_friend)
    RecyclerView rlFriend;
    @BindView(R.id.iv_service_arrow)
    ImageView ivServiceArrow;
    @BindView(R.id.ll_service)
    LinearLayout llService;
    @BindView(R.id.rl_service)
    RecyclerView rlService;
    @BindView(R.id.iv_others_arrow)
    ImageView ivOthersArrow;
    @BindView(R.id.ll_other)
    LinearLayout llOther;
    @BindView(R.id.rl_others)
    RecyclerView rlOthers;
    @BindView(R.id.iv_favourite_arrow)
    ImageView ivFavouriteArrow;
    @BindView(R.id.ll_favourite)
    LinearLayout llFavourite;
    @BindView(R.id.rl_favourite)
    RecyclerView rlFavourite;

    private TransferRelationDetailAdapter favouriteAdapter;
    private TransferRelationDetailAdapter accountAdapter;
    private TransferRelationDetailAdapter familyAdapter;
    private TransferRelationDetailAdapter friendAdapter;
    private TransferRelationDetailAdapter serviceProviderAdapter;
    private TransferRelationDetailAdapter otherAdapter;

    private List<BeneficiaryEntity> beneficiaryList = new ArrayList<>();
    private List<BeneficiaryEntity> favouriteList = new ArrayList<>();
    private List<BeneficiaryEntity> accountList = new ArrayList<>();
    private List<BeneficiaryEntity> familyList = new ArrayList<>();
    private List<BeneficiaryEntity> friendList = new ArrayList<>();
    private List<BeneficiaryEntity> serviceProviderList = new ArrayList<>();
    private List<BeneficiaryEntity> otherList = new ArrayList<>();

    private SimpleItemTouchHelperCallback favouriteCallBack, accountCallBack, familyCallBack, friendCallBack, serviceProviderCallBack, otherCallBack;
    private int currentType;

    private TimeEntity timeEntity;

    private int favouritePosition, accountPosition, familyPosition, friendPosition, serviceProviderPosition, otherPosition;
    ItemTouchHelper favouriteTouchHelper, accountTouchHelper, familyTouchHelper, friendTouchHelper, serviceProviderTouchHelper, otherTouchHelper;

    private BeneficiaryEntity beneficiaryEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_relation_list;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack);
        tvTitle.setText(R.string.DigitalCard_106);
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
        favouriteAdapter = new TransferRelationDetailAdapter(favouriteList);
        accountAdapter = new TransferRelationDetailAdapter(accountList);
        familyAdapter = new TransferRelationDetailAdapter(familyList);
        friendAdapter = new TransferRelationDetailAdapter(friendList);
        serviceProviderAdapter = new TransferRelationDetailAdapter(serviceProviderList);
        otherAdapter = new TransferRelationDetailAdapter(otherList);
        LinearLayoutManager mLayoutManager0 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager mLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rlFavourite.setLayoutManager(mLayoutManager0);
        rlAccount.setLayoutManager(mLayoutManager1);
        rlFamily.setLayoutManager(mLayoutManager2);
        rlFriend.setLayoutManager(mLayoutManager3);
        rlService.setLayoutManager(mLayoutManager4);
        rlOthers.setLayoutManager(mLayoutManager5);

        favouriteCallBack = new SimpleItemTouchHelperCallback(this, 0);
        favouriteTouchHelper = new ItemTouchHelper(favouriteCallBack);
        favouriteTouchHelper.attachToRecyclerView(rlFavourite);

        accountCallBack = new SimpleItemTouchHelperCallback(this, 1);
        accountTouchHelper = new ItemTouchHelper(accountCallBack);
        accountTouchHelper.attachToRecyclerView(rlAccount);

        familyCallBack = new SimpleItemTouchHelperCallback(this, 2);
        familyTouchHelper = new ItemTouchHelper(familyCallBack);
        familyTouchHelper.attachToRecyclerView(rlFamily);

        friendCallBack = new SimpleItemTouchHelperCallback(this, 3);
        friendTouchHelper = new ItemTouchHelper(friendCallBack);
        friendTouchHelper.attachToRecyclerView(rlFriend);

        serviceProviderCallBack = new SimpleItemTouchHelperCallback(this, 4);
        serviceProviderTouchHelper = new ItemTouchHelper(serviceProviderCallBack);
        serviceProviderTouchHelper.attachToRecyclerView(rlService);

        otherCallBack = new SimpleItemTouchHelperCallback(this, 5);
        otherTouchHelper = new ItemTouchHelper(otherCallBack);
        otherTouchHelper.attachToRecyclerView(rlOthers);

        favouriteAdapter.bindToRecyclerView(rlFavourite);
        accountAdapter.bindToRecyclerView(rlAccount);
        familyAdapter.bindToRecyclerView(rlFamily);
        friendAdapter.bindToRecyclerView(rlFriend);
        serviceProviderAdapter.bindToRecyclerView(rlService);
        otherAdapter.bindToRecyclerView(rlOthers);

        mPresenter.getBeneficiaryList();

        favouriteAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startBeneficiaryMoney(favouriteList, position);
            }
        });
        accountAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startBeneficiaryMoney(accountList, position);
            }
        });
        familyAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startBeneficiaryMoney(familyList, position);
            }
        });
        friendAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startBeneficiaryMoney(friendList, position);
            }
        });
        serviceProviderAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startBeneficiaryMoney(serviceProviderList, position);
            }
        });
        otherAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startBeneficiaryMoney(otherList, position);
            }
        });
        LocaleUtils.viewRotationY(this, ivAccountArrow, ivFamilyArrow, ivFriendArrow, ivServiceArrow, ivOthersArrow);
    }

    private void startBeneficiaryMoney(List<BeneficiaryEntity> otherList, int position) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        beneficiaryEntity = otherList.get(position);
        if (beneficiaryEntity != null) {
            mPresenter.getTime(beneficiaryEntity.beneficiaryIbanNo);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_add_new, R.id.ll_favourite, R.id.ll_my_account, R.id.ll_family, R.id.ll_friend, R.id.ll_service, R.id.ll_other})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_new:
                ActivitySkipUtil.startAnotherActivity(BeneficiaryListActivity.this, AddBeneficiaryActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.ll_favourite:
                if (rlFavourite.getVisibility() == View.VISIBLE) {
                    rlFavourite.setVisibility(View.GONE);
                    ivFavouriteArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    rlFavourite.setVisibility(View.VISIBLE);
                    ivFavouriteArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                }
                break;
            case R.id.ll_my_account:
                if (rlAccount.getVisibility() == View.VISIBLE) {
                    rlAccount.setVisibility(View.GONE);
                    ivAccountArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    rlAccount.setVisibility(View.VISIBLE);
                    ivAccountArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                }
                break;
            case R.id.ll_family:
                if (rlFamily.getVisibility() == View.VISIBLE) {
                    rlFamily.setVisibility(View.GONE);
                    ivFamilyArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    ivFamilyArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                    rlFamily.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ll_friend:
                if (rlFriend.getVisibility() == View.VISIBLE) {
                    rlFriend.setVisibility(View.GONE);
                    ivFriendArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    ivFriendArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                    rlFriend.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ll_service:
                if (rlService.getVisibility() == View.VISIBLE) {
                    rlService.setVisibility(View.GONE);
                    ivServiceArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    ivServiceArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                    rlService.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ll_other:
                if (rlOthers.getVisibility() == View.VISIBLE) {
                    rlOthers.setVisibility(View.GONE);
                    ivOthersArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                } else {
                    ivOthersArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                    rlOthers.setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void getBeneficiaryListFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity eventEntity) {
        int eventType = eventEntity.getEventType();
        if (eventType == EventEntity.EVENT_ADD_BENEFICIARY_SUCCESS) {
            mPresenter.getBeneficiaryList();
        }
    }

    @Override
    public void getBeneficiaryListSuccess(BeneficiaryListEntity beneficiaryListEntity) {
        favouriteList.clear();
        accountList.clear();
        familyList.clear();
        friendList.clear();
        serviceProviderList.clear();
        otherList.clear();
        if (beneficiaryListEntity != null) {
            beneficiaryList = beneficiaryListEntity.beneficiaryInfoList;
            int size = beneficiaryList.size();
            for (int i = 0; i < size; i++) {
                BeneficiaryEntity beneficiaryEntity = beneficiaryList.get(i);
                if ("0".equals(beneficiaryEntity.chooseRelationship)) {
                    favouriteList.add(beneficiaryEntity);
                } else if ("1".equals(beneficiaryEntity.chooseRelationship)) {
                    accountList.add(beneficiaryEntity);
                } else if ("2".equals(beneficiaryEntity.chooseRelationship)) {
                    familyList.add(beneficiaryEntity);
                } else if ("3".equals(beneficiaryEntity.chooseRelationship)) {
                    friendList.add(beneficiaryEntity);
                } else if ("4".equals(beneficiaryEntity.chooseRelationship)) {
                    serviceProviderList.add(beneficiaryEntity);
                } else {
                    otherList.add(beneficiaryEntity);
                }
            }

            if (otherList.size() > 0) {
                llOther.setVisibility(View.VISIBLE);
                otherAdapter.setDataList(otherList);
                setRlVisible(rlOthers, ivOthersArrow);
            } else {
                llOther.setVisibility(View.GONE);
            }

            if (serviceProviderList.size() > 0) {
                llService.setVisibility(View.VISIBLE);
                serviceProviderAdapter.setDataList(serviceProviderList);
                setRlVisible(rlService, ivServiceArrow);
            } else {
                llService.setVisibility(View.GONE);
            }

            if (friendList.size() > 0) {
                llFriend.setVisibility(View.VISIBLE);
                friendAdapter.setDataList(friendList);
                setRlVisible(rlFriend, ivFriendArrow);
            } else {
                llFriend.setVisibility(View.GONE);
            }

            if (familyList.size() > 0) {
                llFamily.setVisibility(View.VISIBLE);
                familyAdapter.setDataList(familyList);
                setRlVisible(rlFamily, ivFamilyArrow);
            } else {
                llFamily.setVisibility(View.GONE);
            }

            if (accountList.size() > 0) {
                llMyAccount.setVisibility(View.VISIBLE);
                accountAdapter.setDataList(accountList);
                setRlVisible(rlAccount, ivAccountArrow);
            } else {
                llMyAccount.setVisibility(View.GONE);
            }
            if (favouriteList.size() > 0) {
                llFavourite.setVisibility(View.VISIBLE);
                favouriteAdapter.setDataList(favouriteList);
                setRlVisible(rlFavourite, ivFavouriteArrow);
            } else {
                llFavourite.setVisibility(View.GONE);
            }
        }
    }

    private void setRlVisible(RecyclerView rl, ImageView iv) {
        rlFavourite.setVisibility(View.GONE);
        rlAccount.setVisibility(View.GONE);
        rlFamily.setVisibility(View.GONE);
        rlFriend.setVisibility(View.GONE);
        rlService.setVisibility(View.GONE);
        rlOthers.setVisibility(View.GONE);
        ivFavouriteArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        ivAccountArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        ivFamilyArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        ivFriendArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        ivServiceArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        ivOthersArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        rl.setVisibility(View.VISIBLE);
        iv.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
    }

    @Override
    public void deleteBeneficiaryFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
        switch (currentType) {
            case 0:
                if (favouriteAdapter != null) {
                    favouriteAdapter.notifyItemChanged(favouritePosition);
                }
                break;
            case 1:
                if (accountAdapter != null) {
                    accountAdapter.notifyItemChanged(accountPosition);
                }
                break;
            case 2:
                if (familyAdapter != null) {
                    familyAdapter.notifyItemChanged(familyPosition);
                }
                break;
            case 3:
                if (friendAdapter != null) {
                    friendAdapter.notifyItemChanged(friendPosition);
                }
                break;
            case 4:
                if (serviceProviderAdapter != null) {
                    serviceProviderAdapter.notifyItemChanged(serviceProviderPosition);
                }
                break;
            case 5:
                if (otherAdapter != null) {
                    otherAdapter.notifyItemChanged(otherPosition);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void deleteBeneficiarySuccess(Void response) {
        switch (currentType) {
            case 0:
                if (favouriteAdapter != null) {
                    //移除数据
                    favouriteAdapter.getDataList().remove(favouritePosition);
                    favouriteAdapter.notifyItemRemoved(favouritePosition);
                    if (favouriteAdapter.getDataList().size() == 0) {
                        llFavourite.setVisibility(View.GONE);
                    }
                }
                break;
            case 1:
                if (accountAdapter != null) {
                    //移除数据
                    accountAdapter.getDataList().remove(accountPosition);
                    accountAdapter.notifyItemRemoved(accountPosition);
                    if (accountAdapter.getDataList().size() == 0) {
                        llMyAccount.setVisibility(View.GONE);
                    }
                }
                break;
            case 2:
                if (familyAdapter != null) {
                    //移除数据
                    familyAdapter.getDataList().remove(familyPosition);
                    familyAdapter.notifyItemRemoved(familyPosition);
                    if (familyAdapter.getDataList().size() == 0) {
                        llFamily.setVisibility(View.GONE);
                    }
                }
                break;
            case 3:
                if (friendAdapter != null) {
                    //移除数据
                    friendAdapter.getDataList().remove(friendPosition);
                    friendAdapter.notifyItemRemoved(friendPosition);
                    if (friendAdapter.getDataList().size() == 0) {
                        llFriend.setVisibility(View.GONE);
                    }
                }
                break;
            case 4:
                if (serviceProviderAdapter != null) {
                    //移除数据
                    serviceProviderAdapter.getDataList().remove(serviceProviderPosition);
                    serviceProviderAdapter.notifyItemRemoved(serviceProviderPosition);
                    if (serviceProviderAdapter.getDataList().size() == 0) {
                        llService.setVisibility(View.GONE);
                    }
                }
                break;
            case 5:
                if (otherAdapter != null) {
                    //移除数据
                    otherAdapter.getDataList().remove(otherPosition);
                    otherAdapter.notifyItemRemoved(otherPosition);
                    if (otherAdapter.getDataList().size() == 0) {
                        llOther.setVisibility(View.GONE);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void getTimeFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getTimeSuccess(TimeEntity timeEntity) {
        this.timeEntity = timeEntity;
        mPresenter.getLimit(Constants.LIMIT_TRANSFER);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> limitMap = new HashMap<>();
        limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        limitMap.put(Constants.TimeEntity, timeEntity);
        limitMap.put(Constants.BeneficiaryEntity, beneficiaryEntity);
        ActivitySkipUtil.startAnotherActivity(BeneficiaryListActivity.this, BeneficiaryMoneyActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public TransferContract.BeneficiaryListPresenter getPresenter() {
        return new BeneficiaryListPresenter();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDissmiss(int position, int type) {
        this.currentType = type;
        BeneficiaryEntity beneficiaryEntity;
        switch (type) {
            case 0:
                favouritePosition = position;
                beneficiaryEntity = favouriteAdapter.getDataList().get(position);
                mPresenter.deleteBeneficiary(beneficiaryEntity.id);
                break;
            case 1:
                accountPosition = position;
                beneficiaryEntity = accountAdapter.getDataList().get(position);
                mPresenter.deleteBeneficiary(beneficiaryEntity.id);
                break;
            case 2:
                familyPosition = position;
                beneficiaryEntity = familyAdapter.getDataList().get(position);
                mPresenter.deleteBeneficiary(beneficiaryEntity.id);
                break;
            case 3:
                friendPosition = position;
                beneficiaryEntity = friendAdapter.getDataList().get(position);
                mPresenter.deleteBeneficiary(beneficiaryEntity.id);
                break;
            case 4:
                serviceProviderPosition = position;
                beneficiaryEntity = serviceProviderAdapter.getDataList().get(position);
                mPresenter.deleteBeneficiary(beneficiaryEntity.id);
                break;
            case 5:
                otherPosition = position;
                beneficiaryEntity = otherAdapter.getDataList().get(position);
                mPresenter.deleteBeneficiary(beneficiaryEntity.id);
                break;
            default:
                break;
        }
    }
}
