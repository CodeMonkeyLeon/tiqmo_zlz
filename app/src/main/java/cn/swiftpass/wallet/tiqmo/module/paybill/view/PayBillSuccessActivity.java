package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;

public class PayBillSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_download)
    ImageView ivDownload;
    @BindView(R.id.iv_share)
    ImageView ivShare;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_result_image)
    ImageView ivResultImage;
    @BindView(R.id.tv_result_content)
    TextView tvResultContent;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.tv_result_desc)
    TextView tvResultDesc;
    @BindView(R.id.tv_paid_to)
    TextView tvPaidTo;
    @BindView(R.id.tv_transaction_number)
    TextView tvTransactionNumber;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;
    @BindView(R.id.tv_payment_method)
    TextView tvPaymentMethod;
    @BindView(R.id.ll_content)
    LinearLayout llContent;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_success;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_8);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

    }

    @OnClick({R.id.iv_back, R.id.iv_download, R.id.iv_share})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_download:
                break;
            case R.id.iv_share:
                break;
            default:
                break;
        }
    }
}
