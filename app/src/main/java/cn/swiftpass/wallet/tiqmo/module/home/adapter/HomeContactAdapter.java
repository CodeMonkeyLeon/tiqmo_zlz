package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class HomeContactAdapter extends BaseRecyclerAdapter<SendReceiveEntity> {
    public HomeContactAdapter(@Nullable List<SendReceiveEntity> data) {
        super(R.layout.item_home_send_contact, data);
    }


    @Override
    protected void bindData(@NonNull BaseViewHolder holder, SendReceiveEntity item, int position) {

        String name = item.getFirstName();
        holder.setBackgroundRes(R.id.rl_service, ThemeSourceUtils.getSourceID(mContext, R.attr.shape_091b57_white));
        holder.setText(R.id.tv_user_name, name);
        holder.setTextColor(R.id.tv_user_name, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44)));
        RoundedImageView ivAvatar = holder.getView(R.id.iv_avatar);

        RoundedImageView imageView11 = holder.getView(R.id.avatar_line1_1);
        RoundedImageView imageView12 = holder.getView(R.id.avatar_line1_2);
        RoundedImageView imageView21 = holder.getView(R.id.avatar_line2_1);
        RoundedImageView imageView22 = holder.getView(R.id.avatar_line2_2);

        ImageView ivDirection = holder.getView(R.id.iv_direction);
        if( "0".equals(item.getRecentTraType())){
            ivDirection.setImageResource(R.drawable.l_send_receive_deducted);
        }else if ("1".equals(item.getRecentTraType())){
            ivDirection.setImageResource(R.drawable.l_send_receive_added);
        }else {
            ivDirection.setVisibility(View.GONE);
        }
        if (item.getIsGroup() == "Y") {
            ivAvatar.setOval(false);
        }
        if (item.getHeadIcon().size() == 1) {
            String avatarUrl = item.getHeadIcon().get(0);
            if (!TextUtils.isEmpty(avatarUrl)) {
                Glide.with(mContext).clear(ivAvatar);
                Glide.with(mContext)
                        .asBitmap()
                        .load(avatarUrl)
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .dontAnimate()
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(new BitmapImageViewTarget(ivAvatar) {
                            @Override
                            public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                                ivAvatar.setImageBitmap(bitmap);
                            }
                        });
            } else {

                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivAvatar);
            }
        }else {
            //群组头像
            ivAvatar.setVisibility(View.GONE);
            holder.getView(R.id.group_avatar).setVisibility(View.VISIBLE);
            List<String> headIcon = item.getHeadIcon();
            int size = headIcon.size();
            if (size == 2) {
                holder.getView(R.id.ll_avatar_line_2).setVisibility(View.GONE);
            }else if (size == 3){
                imageView22.setVisibility(View.GONE);
                holder.getView(R.id.cl_avatar_more).setVisibility(View.GONE);
            }else if (size == 4){
                holder.getView(R.id.cl_avatar_more).setVisibility(View.GONE);
            }else{
                holder.setText(R.id.tv_avatar_more,"+"+(size-3));
                imageView22.setVisibility(View.GONE);
            }

            for (int i = 0; i < headIcon.size(); i++) {
                String avatar = headIcon.get(i);
                if (i==0) {
                    if (!TextUtils.isEmpty(avatar)) {
                        Glide.with(mContext).clear(imageView11);
                        int round = (int) AndroidUtils.dip2px(mContext, 8);
                        Glide.with(mContext)
                                .load(avatar)
                                .dontAnimate()
                                .format(DecodeFormat.PREFER_RGB_565)
                                .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                                .into(imageView11);
                    } else {
                        Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView11);
                    }
                }else if (i == 1){
                    if (!TextUtils.isEmpty(avatar)) {
                        Glide.with(mContext).clear(imageView12);
                        int round = (int) AndroidUtils.dip2px(mContext, 8);
                        Glide.with(mContext)
                                .load(avatar)
                                .dontAnimate()
                                .format(DecodeFormat.PREFER_RGB_565)
                                .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                                .into(imageView12);
                    } else {
                        Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView12);
                    }
                }else if (i == 2){
                    if (!TextUtils.isEmpty(avatar)) {
                        Glide.with(mContext).clear(imageView21);
                        int round = (int) AndroidUtils.dip2px(mContext, 8);
                        Glide.with(mContext)
                                .load(avatar)
                                .dontAnimate()
                                .format(DecodeFormat.PREFER_RGB_565)
                                .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                                .into(imageView21);
                    } else {
                        Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView21);
                    }
                } else if (i == 3){
                    if (!TextUtils.isEmpty(avatar)) {
                        Glide.with(mContext).clear(imageView22);
                        int round = (int) AndroidUtils.dip2px(mContext, 8);
                        Glide.with(mContext)
                                .load(avatar)
                                .dontAnimate()
                                .format(DecodeFormat.PREFER_RGB_565)
                                .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                                .into(imageView22);
                    } else {
                        Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView22);
                    }
                }
            }
        }


    }



    public void changeTheme() {
        notifyDataSetChanged();
    }
}
