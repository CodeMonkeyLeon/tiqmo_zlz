package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInputProductEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;

public class RechargeCustomDialog extends BottomDialog{

    private PayMoneyListener payMoneyListener;

    private RechargeInputProductEntity inputProductEntity;
    public String exchangeRate;
    private String orderMoney;
    private String changeSecondAmount;
    private double destinationMinAmount = 0d,destinationMaxAmount = 0d,destinationAmountIncrement = 0d,maxIncrementAmount = 0d,
            minIncrementAmount = 0d;

    public RechargeCustomDialog(Context context,RechargeInputProductEntity mInputProductEntity) {
        super(context);
        this.inputProductEntity = mInputProductEntity;
        initViews(context);
    }

    public void setInputProductEntity(final RechargeInputProductEntity inputProductEntity) {
        this.inputProductEntity = inputProductEntity;
    }

    public interface PayMoneyListener {
        void payMoney(String destinationAmount,String money);
    }

    public void setPayMoneyListener(final PayMoneyListener payMoneyListener) {
        this.payMoneyListener = payMoneyListener;
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(context).inflate(R.layout.dialog_recharge_custom_dialog, null);
        EditText etFirstAmount = mView.findViewById(R.id.et_first_amount);
        TextView tvSecondMoney = mView.findViewById(R.id.tv_second_money);
        TextView tvTitle = mView.findViewById(R.id.tv_title);
        TextView tvError = mView.findViewById(R.id.tv_error);
        TextView tvContent = mView.findViewById(R.id.tv_content);
        TextView tvChangeSecondCurrency = mView.findViewById(R.id.tv_change_second_currency);
        TextView tvChangeFirstCurrency = mView.findViewById(R.id.tv_change_first_currency);
        TextView tvRechargePay = mView.findViewById(R.id.tv_recharge_pay);
        tvError.setText(context.getString(R.string.Topup_new_12));
        if(inputProductEntity!= null) {
            try {
//                inputProductEntity.destinationAmountIncrement = "0.01";
//                inputProductEntity.destinationMinAmount = "0";
                tvTitle.setText(inputProductEntity.title);
                tvContent.setText(inputProductEntity.description);
                tvChangeFirstCurrency.setText(inputProductEntity.destinationCurrencyCode);
                tvChangeSecondCurrency.setText(LocaleUtils.getCurrencyCode(""));
                if (!TextUtils.isEmpty(inputProductEntity.exchangeRate)) {
                    exchangeRate = inputProductEntity.exchangeRate;
                }
                destinationMinAmount = Double.parseDouble(inputProductEntity.destinationMinAmount);
                destinationMaxAmount = Double.parseDouble(inputProductEntity.destinationMaxAmount);
                destinationAmountIncrement = Double.parseDouble(inputProductEntity.destinationAmountIncrement);
            }catch (Throwable e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }

        etFirstAmount.setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        tvRechargePay.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (payMoneyListener != null) {
                    if(!TextUtils.isEmpty(changeSecondAmount)) {
                        payMoneyListener.payMoney(orderMoney,changeSecondAmount);
                    }
                }
            }
        });

//        etFirstAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                String amount = etFirstAmount.getText().toString().trim();
//                amount = amount.replace(",", "");
//                double money = Double.parseDouble(amount);
//                if (!hasFocus) {
//                    if(money == destinationMinAmount || money == destinationMaxAmount || money == maxAmount){
//                        tvError.setVisibility(View.GONE);
//                    }else{
//                        tvError.setVisibility(View.VISIBLE);
//                    }
//                }
//            }
//        });

        etFirstAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    String amount = etFirstAmount.getText().toString().trim();
                    amount = amount.replace(",", "");
                    String fomatAmount = AmountUtil.dataFormat(amount);

                    if (!hasFocus) {
                        etFirstAmount.setText(fomatAmount);
                    } else {
                        // 获取焦点时清空输入框
                        if (Double.parseDouble(amount) == 0) {
                            etFirstAmount.setText("");
                        }
                    }
                }catch (Throwable e){
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        });

        etFirstAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    destinationAmountIncrement = Double.parseDouble(inputProductEntity.destinationAmountIncrement);
                    String amount = s.toString();
                    amount = amount.replace(",", "");
                    orderMoney = amount;
                    if (TextUtils.isEmpty(amount)) {
                        tvRechargePay.setEnabled(false);
                        tvError.setVisibility(View.VISIBLE);
                        tvSecondMoney.setText("");
                        tvRechargePay.setText(context.getString(R.string.scan_pay_7) + " " + "0.00" + " " + LocaleUtils.getCurrencyCode(""));
                    } else {
                        double money = Double.parseDouble(orderMoney);
                        changeSecondAmount = exchangeSecondMoney(amount, exchangeRate);
                        tvSecondMoney.setText(changeSecondAmount);
                        tvRechargePay.setText(context.getString(R.string.scan_pay_7) + " " + changeSecondAmount + " " + LocaleUtils.getCurrencyCode(""));
                        double maxMoney = 0d;
                        if(money == 0 || money>destinationMaxAmount || money<destinationMinAmount){
                            tvRechargePay.setEnabled(false);
                            tvError.setVisibility(View.VISIBLE);
                            return;
                        }
                        maxMoney = BigDecimalFormatUtils.round(money-destinationMinAmount,2);
                        if(destinationAmountIncrement < 1){
                            maxMoney = BigDecimalFormatUtils.round(maxMoney*100,0);
                            destinationAmountIncrement =  BigDecimalFormatUtils.round(destinationAmountIncrement*100,0);
                        }
                        if(maxMoney%destinationAmountIncrement == 0){
                            tvRechargePay.setEnabled(true);
                            tvError.setVisibility(View.GONE);
                        }else{
                            tvRechargePay.setEnabled(false);
                            tvError.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    tvRechargePay.setEnabled(false);
                }
            }
        });

        tvRechargePay.setText(context.getString(R.string.scan_pay_7) + " " + "0.00" + " " + LocaleUtils.getCurrencyCode(""));

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private String exchangeSecondMoney(String money, String exchangeRate) {
        try {
            String resultMoney = BigDecimalFormatUtils.div(money,exchangeRate,2);
            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "";
    }
}
