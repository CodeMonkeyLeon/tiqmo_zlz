package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ExtendPropertiesEntity extends BaseEntity {

    public static final String TYPE_EXTEND_KEY_FESS = "Transaction Fees";
    public static final String TYPE_EXTEND_KEY_VAT = "VAT";


    private String extendKey;
    private String extendValue;
    private String extendSubValue;

    public void setExtendKey(String extendKey) {
        this.extendKey = extendKey;
    }

    public String getExtendKey() {
        return extendKey;
    }

    public void setExtendValue(String extendValue) {
        this.extendValue = extendValue;
    }

    public String getExtendValue() {
        return extendValue;
    }

    public void setExtendSubValue(String extendSubValue) {
        this.extendSubValue = extendSubValue;
    }

    public String getExtendSubValue() {
        return extendSubValue;
    }

}
