package cn.swiftpass.wallet.tiqmo.base.view;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import cn.swiftpass.wallet.tiqmo.R;

public abstract class BaseDialogFragmentN extends DialogFragment {

    private View transparentView;
    private View placeHolder;

    public Context mContext;

    @LayoutRes
    protected abstract int getLayoutID();

    protected abstract void init(View view, Bundle savedInstanceState);

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //这里new一个FrameLayout只是用来inflate中的参数，不然layout中得参数无法解析
        if (container == null) {
            container = new FrameLayout(mContext);
        }
        initWinLayoutParams();

        View rootView = inflater.inflate(R.layout.dialog_fragment_root, container, false);
        View realView = inflater.inflate(getLayoutID(), container, false);
        transparentView = rootView.findViewById(R.id.translate);
        placeHolder = rootView.findViewById(R.id.place_holder);
        replaceView(placeHolder, realView);
        return rootView;
    }

    @Override
    public final void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
        transparentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseDialogFragmentN.this.getDialog() != null) {
                    onCancel(BaseDialogFragmentN.this.getDialog());
                }
                dismiss();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    // 只显示一个tag对应的dialog
    @Override
    public void show(@NonNull FragmentManager manager, String tag) {
        if (!TextUtils.isEmpty(tag) && manager.findFragmentByTag(tag) != null) {
            return;
        }
        super.show(manager, tag);
    }

    // 只显示一个tag对应的dialog
    @Override
    public void showNow(@NonNull FragmentManager manager, String tag) {
        if (!TextUtils.isEmpty(tag) && manager.findFragmentByTag(tag) != null) {
            return;
        }
        super.showNow(manager, tag);
    }

    public void dismiss() {
        if (getDialog() != null) {
            getDialog().dismiss();
        }
    }

    private void initWinLayoutParams() {
        if (getDialog() == null || getDialog().getWindow() == null) {
            return;
        }
        // 初始化window布局参数
        Window window = getDialog().getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        window.setBackgroundDrawableResource(R.color.transparent);
        window.setDimAmount(0.7f);
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.windowAnimations = R.style.bottomSheet_animation;
        window.setAttributes(wlp);
    }

    public void replaceView(View oldView, final View newView) {
        if (oldView == null || !(oldView.getParent() instanceof ViewGroup) || newView == null) {
            return;
        }

        ViewGroup parentViewGroup = (ViewGroup) oldView.getParent();
        parentViewGroup.removeView(newView);

        int index = parentViewGroup.indexOfChild(oldView);
        newView.setLayoutParams(oldView.getLayoutParams());
        parentViewGroup.addView(newView, index);
        oldView.setVisibility(View.GONE);
    }
}
