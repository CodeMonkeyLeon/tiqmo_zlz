package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillCountryListEntity extends BaseEntity {
    public List<PayBillCountryEntity> countryList = new ArrayList<>();
}
