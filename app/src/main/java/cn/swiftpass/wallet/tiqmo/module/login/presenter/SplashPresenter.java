package cn.swiftpass.wallet.tiqmo.module.login.presenter;

import cn.swiftpass.wallet.tiqmo.module.login.contract.SplashContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SplashPresenter implements SplashContract.Presenter {
    private SplashContract.View mBaseView;

    @Override
    public void attachView(SplashContract.View view) {
        this.mBaseView = view;
    }

    @Override
    public void detachView() {
        this.mBaseView = null;
    }

    @Override
    public void getConfig() {
        AppClient.getInstance().getUserManager().getConfig(new LifecycleMVPResultCallback<ConfigEntity>(mBaseView, false) {
            @Override
            protected void onSuccess(ConfigEntity result) {
                mBaseView.getConfigSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {

            }
        });
    }

}
