package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class LanguageCodeEntity extends BaseEntity {
    public String name;
    public boolean rTL;
    public String key;
    public Locale locale;

    public LanguageCodeEntity(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}
