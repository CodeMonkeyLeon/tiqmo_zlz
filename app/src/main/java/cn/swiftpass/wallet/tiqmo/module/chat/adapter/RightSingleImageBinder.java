package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseItemDataBinder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class RightSingleImageBinder extends BaseItemDataBinder<MessageEntity> {
    private String searchTerm = "";
    @Override
    public int getViewType() {
        return MessageAdapter.RightSingleImage;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_chat_right_image;
    }

    @Override
    public void bindData(BaseViewHolder baseViewHolder, MessageEntity messageEntity, int position) {
        baseViewHolder.setText(R.id.tv_right_time, TimeUtils.getChatTime(messageEntity.getTimestamp()));
        baseViewHolder.setText(R.id.tv_right_name, messageEntity.getUser().getName());
        ImageView ivLeftImage = baseViewHolder.getView(R.id.iv_right_image);
        if (messageEntity.getGlideUrl() != null || messageEntity.getStrUrl() != null) {
            Glide.with(mContext).load(messageEntity.getGlideUrl()).into(ivLeftImage);
        } else {
            Glide.with(mContext).clear(ivLeftImage);
        }

        baseViewHolder.setBackgroundRes(R.id.con_reply_msg, ThemeSourceUtils.getSourceID(mContext,R.attr.shape_071b55_white_16));
        if(!TextUtils.isEmpty(messageEntity.getReplyMessage())){
            baseViewHolder.setGone(R.id.view_reply_message,false);
            baseViewHolder.setText(R.id.tv_reply_name, messageEntity.getReplyName());
            baseViewHolder.setText(R.id.tv_reply_message, messageEntity.getReplyMessage());
            ImageView ivReply = baseViewHolder.getView(R.id.iv_reply_image);
            if (messageEntity.getReplyImageUrl() != null) {
                ivReply.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(messageEntity.getReplyImageUrl()).into(ivReply);
            } else {
                ivReply.setVisibility(View.GONE);
                Glide.with(mContext).clear(ivReply);
            }
        }else{
            baseViewHolder.setGone(R.id.view_reply_message,true);
        }
    }
}
