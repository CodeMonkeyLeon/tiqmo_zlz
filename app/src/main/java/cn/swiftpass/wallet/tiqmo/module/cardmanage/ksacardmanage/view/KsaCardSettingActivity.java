package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardSetPinPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CardChannelNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;

public class KsaCardSettingActivity extends BaseCompatActivity<CardKsaContract.SetCardPinPresener> implements CardKsaContract.SetCardPinView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.cb_store_purchase)
    ImageView cbStorePurchase;
    @BindView(R.id.cb_online_purchase)
    ImageView cbOnlinePurchase;
    @BindView(R.id.cb_atm_purchase)
    ImageView cbAtmPurchase;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_content_two)
    TextView tvContentTwo;
    @BindView(R.id.tv_content_three)
    TextView tvContentThree;
    @BindView(R.id.tv_block_btn)
    TextView tvBlockBtn;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_change_pin)
    TextView tvChangePin;
    @BindView(R.id.tv_set_pin_title)
    TextView tvSetPinTitle;
    @BindView(R.id.ll_change_pin)
    LinearLayout llChangePin;
    @BindView(R.id.con_atm)
    ConstraintLayout conAtm;

    @BindView(R.id.con_international)
    ConstraintLayout conInternational;
    @BindView(R.id.cb_international)
    ImageView cbInternational;
    @BindView(R.id.view_bottom)
    View viewBottom;

    //ALLOCATED 可用 LOCKED暂时冻结 BLOCKED 注销卡
    private String cardStatus;
    private String cardType;

    private int cbType;
    private String txnTypeEnabled;
    boolean onlineEnable, instoreEnable, atmEnable,interEnable;

    private String proxyCardNo;

    private KsaLimitChannelEntity ksaLimitChannelEntity;
    private KsaCardEntity ksaCardEntity;

    private RiskControlEntity riskControlEntity;

    private static KsaCardSettingActivity ksaCardSettingActivity;

    public static KsaCardSettingActivity getKsaCardSettingActivity() {
        return ksaCardSettingActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_card_setting;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ksaCardSettingActivity = this;
        tvTitle.setText(R.string.Card_settings_1);
        setDarkBar();
        llContent.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        List<String> additionalData = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            ksaLimitChannelEntity = (KsaLimitChannelEntity) getIntent().getExtras().getSerializable(Constants.ksaLimitChannelEntity);
            if (ksaLimitChannelEntity != null) {
                String ecom = ksaLimitChannelEntity.ecom;
                String atm = ksaLimitChannelEntity.atm;
                String instore = ksaLimitChannelEntity.instore;
                String nationtms = ksaLimitChannelEntity.nationtms;
                if ("1".equals(atm)) {
                    atmEnable = true;
                    cbAtmPurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                }else{
                    cbAtmPurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                }
                if ("1".equals(instore)) {
                    instoreEnable = true;
                    cbStorePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                }else{
                    cbStorePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                }
                if ("1".equals(ecom)) {
                    onlineEnable = true;
                    cbOnlinePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                }else{
                    cbOnlinePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                }
                if ("1".equals(nationtms)) {
                    interEnable = true;
                    cbInternational.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                }else{
                    cbInternational.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                }
                List<String> enabledChannels = ksaLimitChannelEntity.enabledChannels;
                List<String> disabledChannels = ksaLimitChannelEntity.disabledChannels;
                if (disabledChannels != null && disabledChannels.size() > 0) {
                    int channelSize = disabledChannels.size();
                    for (int i = 0; i < channelSize; i++) {
                        String channel = disabledChannels.get(i);
                        if ("atm".equals(channel)) {
                            cbAtmPurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                        }
                        if ("instore".equals(channel)) {
                            cbStorePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                        }
                        if ("ecom".equals(channel)) {
                            cbOnlinePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                        }
                        if ("nationtms".equals(channel)) {
                            cbInternational.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                        }
                    }
                }

                if (enabledChannels != null && enabledChannels.size() > 0) {
                    int channelSize = enabledChannels.size();
                    for (int i = 0; i < channelSize; i++) {
                        String channel = enabledChannels.get(i);
                        if ("atm".equals(channel)) {
                            atmEnable = true;
                            cbAtmPurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                        }
                        if ("instore".equals(channel)) {
                            instoreEnable = true;
                            cbStorePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                        }
                        if ("ecom".equals(channel)) {
                            onlineEnable = true;
                            cbOnlinePurchase.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                        }
                        if ("nationtms".equals(channel)) {
                            interEnable = true;
                            cbInternational.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel));
                        }
                    }
                }
            }
            if (ksaCardEntity != null) {
                proxyCardNo = ksaCardEntity.proxyCardNo;
                cardType = ksaCardEntity.cardType;
                cardStatus = ksaCardEntity.cardStatus;
                additionalData.add(ksaCardEntity.maskedCardNo);

                if(ksaCardEntity.isVirtualCard()){
                    tvSetPinTitle.setText(getString(R.string.DigitalCard_100));
                    conAtm.setVisibility(View.GONE);
                }else{
                    tvSetPinTitle.setText(getString(R.string.Physical_card_set_2));
                    conAtm.setVisibility(View.VISIBLE);
                }
                llChangePin.setVisibility(View.VISIBLE);

            }
        }

        cbStorePurchase.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                cbType = 1;
                txnTypeEnabled = !instoreEnable ? "1" : "0";

                mPresenter.getOtpType("ActionType", "0".equals(txnTypeEnabled)?"TCA13":"TCA14", additionalData);
            }
        });

        cbAtmPurchase.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                cbType = 3;
                txnTypeEnabled = !atmEnable ? "1" : "0";
                mPresenter.getOtpType("ActionType", "0".equals(txnTypeEnabled)?"TCA15":"TCA16", additionalData);
            }
        });

        cbOnlinePurchase.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                cbType = 2;
                txnTypeEnabled = !onlineEnable ? "1" : "0";
                mPresenter.getOtpType("ActionType", "0".equals(txnTypeEnabled)?"TCA11":"TCA12", additionalData);
            }
        });

        cbInternational.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                cbType = 4;
                txnTypeEnabled = !interEnable ? "1" : "0";
                mPresenter.getOtpType("ActionType", "0".equals(txnTypeEnabled)?"TCA17":"TCA18", additionalData);
            }
        });
    }

    private void requestSetChannel(int cbType){
        switch (cbType){
            case 1:
                mPresenter.setKsaCardChannel(Constants.TYPE_TXN_INSTORE, txnTypeEnabled,proxyCardNo);
                break;
            case 2:
                mPresenter.setKsaCardChannel(Constants.TYPE_TXN_ECOM,  txnTypeEnabled,proxyCardNo);
                break;
            case 3:
                mPresenter.setKsaCardChannel(Constants.TYPE_TXN_ATM, txnTypeEnabled,proxyCardNo);
                break;
            case 4:
                mPresenter.setKsaCardChannel(Constants.TYPE_TXN_INTER, txnTypeEnabled,proxyCardNo);
                break;
            default:break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ksaCardSettingActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_KSA_CARD_SET_CHANNEL == event.getEventType()) {
            requestSetChannel(cbType);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_block_btn, R.id.tv_change_pin, R.id.iv_setting_channel})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_setting_channel:
                CardChannelNoticeDialog cardChannelNoticeDialog = new CardChannelNoticeDialog(mContext);
                cardChannelNoticeDialog.showWithBottomAnim();
                break;
            case R.id.tv_block_btn:
                mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
                ActivitySkipUtil.startAnotherActivity(this, BlockMyCardActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_change_pin:
                mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
                ActivitySkipUtil.startAnotherActivity(this, SetPinConfirmActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }

    @Override
    public void setKsaCardPinSuccess(Void result) {

    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {

    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {

    }

    @Override
    public void setKsaCardChannelSuccess(Void result) {
        switch (cbType) {
            case 1:
                instoreEnable = "1".equals(txnTypeEnabled) ? true : false;
                cbStorePurchase.setImageResource("1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
            case 2:
                onlineEnable = "1".equals(txnTypeEnabled) ? true : false;
                cbOnlinePurchase.setImageResource("1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
            case 3:
                atmEnable = "1".equals(txnTypeEnabled) ? true : false;
                cbAtmPurchase.setImageResource("1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
            case 4:
                interEnable = "1".equals(txnTypeEnabled) ? true : false;
                cbInternational.setImageResource("1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
        switch (cbType) {
            case 1:
                instoreEnable = !"1".equals(txnTypeEnabled) ? true : false;
                cbStorePurchase.setImageResource(!"1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
            case 2:
                onlineEnable = !"1".equals(txnTypeEnabled) ? true : false;
                cbOnlinePurchase.setImageResource(!"1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
            case 3:
                atmEnable = !"1".equals(txnTypeEnabled) ? true : false;
                cbAtmPurchase.setImageResource(!"1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
            case 4:
                interEnable = !"1".equals(txnTypeEnabled) ? true : false;
                cbInternational.setImageResource(!"1".equals(txnTypeEnabled) ? ThemeSourceUtils.getSourceID(mContext, R.attr.bg_check_channel) : ThemeSourceUtils.getSourceID(mContext, R.attr.bg_uncheck_channel));
                break;
        }
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(KsaCardSettingActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestSetChannel(cbType);
                }
            } else {
                requestSetChannel(cbType);
            }
        } else {
            requestSetChannel(cbType);
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_SET_CHANNEL);
        mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_KSA_CARD_SET_CHANNEL);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {

    }

    @Override
    public CardKsaContract.SetCardPinPresener getPresenter() {
        return new CardSetPinPresenter();
    }
}
