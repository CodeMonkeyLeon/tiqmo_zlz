package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;


public class GpsPermissionDialog extends BottomDialog {

    private Context mContext;

    private GotoPermissionListener gotoPermissionListener;
    private DismissListener dismissListener;

    public GpsPermissionDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(context);
    }

    public interface GotoPermissionListener {
        void gotoPermission();
    }

    public interface DismissListener {
        void dismiss();
    }

    public void setGotoPermissionListener(final GotoPermissionListener gotoPermissionListener) {
        this.gotoPermissionListener = gotoPermissionListener;
    }

    public void setDismissListener(final DismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_gps_permission, null);

        TextView tvCancel = mView.findViewById(R.id.tv_cancel);
        TextView tvConfirm = mView.findViewById(R.id.tv_confirm);


        tvCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (dismissListener != null){
                    dismissListener.dismiss();
                }
            }
        });

        tvConfirm.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (gotoPermissionListener != null) {
                    gotoPermissionListener.gotoPermission();
                }
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            getWindow().setGravity(Gravity.CENTER);
            dialogWindow.setAttributes(layoutParams);
        }
    }

}
