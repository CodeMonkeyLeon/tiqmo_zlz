package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillRefundOrderPresenter implements PayBillContract.BillRefundOrderPresenter {

    private PayBillContract.BillRefundOrderView billRefundOrderView;

    @Override
    public void payBillRefundOrder(String channelCode) {
        AppClient.getInstance().getTransferManager().payBillRefundOrder(channelCode, new LifecycleMVPResultCallback<PayBillRefundEntity>(billRefundOrderView,true) {
            @Override
            protected void onSuccess(PayBillRefundEntity result) {
                billRefundOrderView.payBillRefundOrderSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billRefundOrderView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillRefundOrderView billRefundOrderView) {
        this.billRefundOrderView = billRefundOrderView;
    }

    @Override
    public void detachView() {
        this.billRefundOrderView = null;
    }
}
