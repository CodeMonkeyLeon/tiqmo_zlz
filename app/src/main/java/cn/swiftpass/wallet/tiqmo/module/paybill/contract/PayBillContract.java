package cn.swiftpass.wallet.tiqmo.module.paybill.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.BillServiceSearchResultEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListNewEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;

public class PayBillContract {


    public interface MoiServiceView extends BaseView<PayBillContract.MoiServicePresenter> {

        void getPayBillerListSuccess(PayBillerListEntity payBillerListEntity);

        void getMoiServiceIOListSuccess(PayBillIOListEntity payBillIOListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface MoiServicePresenter extends BasePresenter<PayBillContract.MoiServiceView> {
        void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode);

        void getMoiServiceIOList(String billerId, String sku, String channelCode);
    }


    public interface BillServiceView extends BaseView<PayBillContract.BillServicePresenter> {

        void searchBillerSuccess(BillServiceSearchResultEntity billServiceSearchResult);

        void getBillServiceIOListSuccess(PayBillIOListEntity payBillIOListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface BillServicePresenter extends BasePresenter<PayBillContract.BillServiceView> {
        void searchBiller(String searchBillerNameOrCodeOfLocalService, String countryCode, String channelCode);

        void getBillServiceIOList(String billerId, String sku, String channelCode);
    }

    public interface BillCountryView extends BaseView<PayBillContract.BillCountryPresenter> {
        void getPayBillCountrySuccess(PayBillCountryListEntity payBillCountryListEntity);

        void showErrorMsg(String errorCode, String errorMsg);

        void getPayBillCategorySuccess(PayBillTypeListNewEntity payBillTypeListNewEntity, String countryCode, String channelCode);
    }

    public interface BillCountryPresenter extends BasePresenter<PayBillContract.BillCountryView> {
        void getPayBillCountry();

        void getPayBillCategory(String countryCode, String channelCode);
    }

    public interface BillTypeView extends BaseView<PayBillContract.BillTypePresenter> {
        void getPayBillTypeSuccess(PayBillTypeListEntity payBillTypeListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface BillTypePresenter extends BasePresenter<PayBillContract.BillTypeView> {
        void getPayBillTypeList(String countryCode, String channelCode);
    }


    public interface BillServiceBillerListView extends BaseView<PayBillContract.BillServiceBillerListPresenter> {
        void getPayBillerListSuccess(PayBillerListEntity payBillerListEntity);

        void getBillServiceIOListSuccess(PayBillIOListEntity payBillIOListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface BillServiceBillerListPresenter extends BasePresenter<PayBillContract.BillServiceBillerListView> {
        void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode);

        void getBillServiceIOList(String billerId, String sku, String channelCode);
    }


    public interface BillerListView extends BaseView<PayBillContract.BillerListPresenter> {
        void getPayBillerListSuccess(PayBillerListEntity payBillerListEntity);

        void getPayBillSkuListSuccess(PayBillSkuListEntity payBillSkuListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface BillerListPresenter extends BasePresenter<PayBillContract.BillerListView> {
        void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode);

        void getPayBillSkuList(String billerId, String channelCode);
    }

    public interface BillIOListView extends BaseView<PayBillContract.BillIOListPresenter> {
        void getPayBillIOListSuccess(PayBillIOListEntity payBillIOListEntity);

        void getPayBillDetailSuccess(PayBillDetailEntity payBillDetailEntity);

        void getPayBillDetailFail(String errorCode, String errorMsg);

        void showErrorMsg(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getNewPayBillIOListSuccess(PayBillIOListEntity result);

        void getLimitFail(String errCode, String errMsg);
    }

    public interface BillIOListPresenter extends BasePresenter<PayBillContract.BillIOListView> {
        void getPayBillIOList(String billerId, String sku);

//        void getPayBillDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data);

        void getNewPayBillIOList(String billerId, String sku, String channelCode);


        void getPayBillServiceDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data,String paymentType);

        void getLimit(String type);
    }

    public interface BillRefundPresenter extends BasePresenter<PayBillContract.BillRefundView>{
        void getPayBillRefundList(String billerId,String channelCode);
        void getPayBillRefundDetail(String channelCode,String id);
        void getPayBillIOList(String billerId, String sku);

        void getNewPayBillIOList(String billerId, String sku, String channelCode);

        void getPayBillServiceDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data,String paymentType);

        void getLimit(String type);
    }

    public interface BillRefundView extends BaseView<PayBillContract.BillRefundPresenter>{
        void getPayBillRefundListSuccess(PayBillRefundListEntity payBillRefundListEntity);

        void getPayBillRefundDetailSuccess(PayBillRefundEntity payBillRefundEntity);

        void getPayBillIOListSuccess(PayBillIOListEntity payBillIOListEntity);

        void getPayBillDetailSuccess(PayBillDetailEntity payBillDetailEntity);

        void getPayBillDetailFail(String errorCode, String errorMsg);

        void showErrorMsg(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getNewPayBillIOListSuccess(PayBillIOListEntity result);

        void getLimitFail(String errCode, String errMsg);
    }

    public interface BillRefundOrderPresenter extends BasePresenter<PayBillContract.BillRefundOrderView>{
        void payBillRefundOrder(String channelCode);
    }

    public interface BillRefundOrderView extends BaseView<PayBillContract.BillRefundOrderPresenter>{
        void payBillRefundOrderSuccess(PayBillRefundEntity payBillRefundEntity);
        void showErrorMsg(String errorCode, String errorMsg);
    }


    public interface BillOrderListView extends BaseView<PayBillContract.BillOrderListPresenter> {
        void getPayBillOrderIOListSuccess(PayBillIOListEntity payBillIOListEntity);

        void getPayBillOrderListSuccess(PayBillOrderListEntity payBillOrderListEntity);

        void deletePayBillOrderSuccess(Void result);

        void showErrorMsg(String errorCode, String errorMsg);

        void getPayBillOrderListFail(String errorCode, String errorMsg);

        void getPayBillCountrySuccess(PayBillCountryListEntity payBillCountryListEntity);
    }

    public interface BillOrderListPresenter extends BasePresenter<PayBillContract.BillOrderListView> {
        void getPayBillOrderList(List<String> countryCodeList, String domesticFlag);

        void getPayBillOrderIOList(String billerId, String sku, String aliasName, String channelCode);

        void getPayBillCountry();

        void deletePayBillOrder(String billerId, String aliasName);
    }

    public interface BillOrderCheckoutView extends BaseView<PayBillContract.BillOrderCheckoutPresenter> {
        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void fetchBillSuccess(PayBillOrderInfoEntity payBillOrderInfoEntity);

        void checkOutFail(String errorCode, String errorMsg);

        void fetchBillFail(String errorCode, String errorMsg);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface BillOrderCheckoutPresenter extends BasePresenter<PayBillContract.BillOrderCheckoutView> {
        void fetchBill(String payAmount, String billAmount, String billProcessingFeePayAmount, String billProcessingFeeAmount, String channelCode);

        void checkOut(String orderNo, String orderInfo);
    }
}
