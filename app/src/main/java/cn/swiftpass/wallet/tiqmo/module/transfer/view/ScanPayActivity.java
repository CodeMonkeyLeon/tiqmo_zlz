package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.PaymentMethodEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.app2app.PaySDKEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class ScanPayActivity extends BaseCompatActivity implements BaseView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.ll_money)
    ConstraintLayout llMoney;
    @BindView(R.id.tv_store_name)
    TextView tvStoreName;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.iv_store)
    ImageView ivStore;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.tv_payment_title)
    TextView tvPaymentTitle;
    @BindView(R.id.tv_payment_source)
    TextView tvPaymentSource;
    @BindView(R.id.con_payment_source)
    ConstraintLayout conPaymentSource;
    @BindView(R.id.tv_discount_title)
    TextView tvDiscountTitle;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;
    @BindView(R.id.tv_discount)
    TextView tvDiscount;
    @BindView(R.id.con_discount)
    ConstraintLayout conDiscount;
    @BindView(R.id.tv_pay)
    TextView tvPay;
    @BindView(R.id.tv_discount_content)
    TextView tv_discount_content;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;

    @BindView(R.id.tv_wrong_fees)
    TextView tvWrongFees;
    @BindView(R.id.tv_fees)
    TextView tvTransFee;
    @BindView(R.id.tv_wrong_vat_money)
    TextView tvWrongVatMoney;
    @BindView(R.id.tv_vat_money)
    TextView tvVat;
    @BindView(R.id.ll_transfer_vat)
    ConstraintLayout llTransferVat;
    @BindView(R.id.ll_transfer_fee)
    ConstraintLayout llTransferFee;

    private CheckOutEntity checkOutEntity;
    private List<PaymentMethodEntity> paymentMethodList = new ArrayList<>();
    private String orderAmount, orderCurrencyCode, paymentDesc, payerAmount;
    private double monthLimitMoney, orderMoney, valiableMoney;
    //	营销活动-优惠类型,CASH_BACK：返现,CASH_REDUCE：立减
    private String couponType;
    private String couponAmount;
//    private String couponAmountModule;
    //手续费  增值税
    private String transferFees, vatAmount, wrongTransferFee, wrongVat;

    private PaySDKEntity paySDKEntity;

    private boolean isFromSdk = false;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_scan_pay;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                checkUser(userInfoEntity);
            }
        }
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String balance = userInfoEntity.getBalance();
            String balanceNumber = userInfoEntity.getBalanceNumber();
            if (!TextUtils.isEmpty(balanceNumber)) {
                try {
                    balanceNumber = balanceNumber.replace(",", "");
                    valiableMoney = Double.parseDouble(balanceNumber);
                    checkMoney(payerAmount);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
            tvAmount.setText(balance);
            tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkUser(userInfoEntity);
    }

    private void checkKyc() {
        if (AppClient.getInstance().getUserManager().getUserInfo().isKycChecking()) {
            ActivitySkipUtil.startAnotherActivity(this,
                    KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        } else if (AppClient.getInstance().getUserManager().getUserInfo().isKycLow()) {
            ReferralCodeDialogUtils.showReferralCodeDialog(this, this);
        } else {
            AppClient.getInstance().checkOut("", paySDKEntity.getOrderInfo(), new LifecycleMVPResultCallback<CheckOutEntity>(this) {
                @Override
                protected void onSuccess(CheckOutEntity result) {
                    checkOutEntity = result;
                    initData(checkOutEntity);
                }

                @Override
                protected void onFail(String errorCode, String errorMsg) {
                    showProgress(false);
                    finish();
                }
            });

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isFromSdk) {
            if (AppClient.getInstance().isLoginSuccess()) {
                checkKyc();
            } else {
                returnToLoginPage();
            }
        }
    }

    private void returnToLoginPage() {
        RegisterActivity.startActivityOfNewTaskType(ScanPayActivity.this);
        finish();
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (!AppClient.getInstance().isLoginSuccess()) {
            returnToLoginPage();
            return;
        }

        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        tvTitle.setText(R.string.scan_pay_0);
        if (getIntent() != null && getIntent().getExtras() != null) {
            checkOutEntity = (CheckOutEntity) getIntent().getExtras().getSerializable(Constants.CHECK_OUT_ENTITY);
            paySDKEntity = (PaySDKEntity) getIntent().getSerializableExtra("PaySDKData");
            if (paySDKEntity != null) {
                isFromSdk = true;
                showProgress(true);
                checkKyc();
            } else if (checkOutEntity != null) {
                initData(checkOutEntity);
            }
        }
    }

    private void initData(CheckOutEntity checkOutEntity) {
        couponType = checkOutEntity.couponType;
        orderAmount = checkOutEntity.orderAmount;
        payerAmount = checkOutEntity.payerAmount;
        couponAmount = checkOutEntity.couponAmount;
        orderCurrencyCode = checkOutEntity.orderCurrencyCode;

        vatAmount = checkOutEntity.discountVat;
        transferFees = checkOutEntity.discountTransFees;
        wrongTransferFee = checkOutEntity.transFees;
        wrongVat = checkOutEntity.vat;

        tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        String merchantName = checkOutEntity.merchantName;
        if (!TextUtils.isEmpty(merchantName)) {
            tvStoreName.setText(AndroidUtils.getContactName(merchantName));
        }
        if (Constants.CASH_BACK.equals(couponType)) {
            conDiscount.setVisibility(View.VISIBLE);
            tv_discount_content.setVisibility(View.VISIBLE);
            tvDiscountTitle.setText(R.string.trx_detail_6);
        } else if (Constants.CASH_REDUCE.equals(couponType)) {
            conDiscount.setVisibility(View.VISIBLE);
            tvDiscountTitle.setText(R.string.trx_detail_4);
        }
        paymentMethodList = checkOutEntity.paymentMethodList;
        if (paymentMethodList.size() > 0) {
            paymentDesc = paymentMethodList.get(0).paymentMethodDesc;
            tvPaymentSource.setText(paymentDesc);
        }
        String merchantLogo = checkOutEntity.merchantLogo;
        Glide.with(mContext)
                .load(merchantLogo)
                .dontAnimate()
                .placeholder(ThemeUtils.isCurrentDark(mContext) ? R.drawable.d_merchant : R.drawable.l_merchant)
                .format(DecodeFormat.PREFER_RGB_565)
                .into(ivStore);
        String monthLimitAmt = checkOutEntity.monthLimitAmt;
        try {
            tvMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferMoney(orderAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(22)
                    .text(" " + LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_090a15))).size(14)
                    .build());
            tvPay.setText(getString(R.string.scan_pay_7) + " " + AndroidUtils.getTransferMoney(payerAmount) + " " + LocaleUtils.getCurrencyCode(orderCurrencyCode));

            if (!TextUtils.isEmpty(wrongVat)) {
                ////如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                llTransferVat.setVisibility(View.VISIBLE);
                tvWrongVatMoney.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(wrongVat) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());

                if (!TextUtils.isEmpty(vatAmount)) {
                    tvVat.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(vatAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
            }else {
                //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                if (!TextUtils.isEmpty(vatAmount) && BigDecimalFormatUtils.compareBig(vatAmount, "0")) {
                    tvWrongVatMoney.setVisibility(View.GONE);
                    tvVat.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(vatAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                } else {
                    llTransferVat.setVisibility(View.GONE);
                }
            }

            if (!TextUtils.isEmpty(wrongTransferFee)) {
                //如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                llTransferFee.setVisibility(View.VISIBLE);
                tvWrongFees.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(wrongTransferFee) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());

                if(!TextUtils.isEmpty(transferFees)) {
                    tvTransFee.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(transferFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
            }else{
                //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                if (!TextUtils.isEmpty(transferFees) && BigDecimalFormatUtils.compareBig(transferFees,"0")) {
                    tvWrongFees.setVisibility(View.GONE);
                    tvTransFee.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(transferFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }else{
                    llTransferFee.setVisibility(View.GONE);
                }
            }

            if (!TextUtils.isEmpty(couponAmount)) {
                if (!BigDecimalFormatUtils.compareBig(couponAmount, "0")) {
                    conDiscount.setVisibility(View.GONE);
                } else {
                    tvDiscount.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(couponAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(" " + LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_090a15))).size(10)
                            .build());

                }
            }
            monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);

        } catch (Exception e) {
            if(e != null) {
                LogUtils.d("errorMsg", "---" + e.getMessage() + "---");
            }
        }
        paymentMethodList = checkOutEntity.paymentMethodList;

        showProgress(false);
    }

    private void checkMoney(String orderAmount) {
        if (TextUtils.isEmpty(payerAmount)) {
            return;
        }
        orderMoney = AndroidUtils.getTransferMoneyNumber(payerAmount);
        if (orderMoney > monthLimitMoney) {
            tvAddMoney.setEnabled(false);
            tvPay.setEnabled(false);
            tvError.setText(getString(R.string.wtw_25_3));
        } else if (orderMoney <= valiableMoney) {
//            if (orderMoney > 0) {
                tvPay.setEnabled(true);
//            }
            tvAddMoney.setEnabled(false);
            tvError.setText("");
        } else {
            tvAddMoney.setEnabled(true);
            tvPay.setEnabled(false);
            tvError.setText(getString(R.string.wtw_25));
        }

        if (valiableMoney == 0) {
            tvAddMoney.setEnabled(true);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_add_money, R.id.con_payment_source, R.id.tv_pay})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            case R.id.con_payment_source:
                break;
            case R.id.tv_pay:
                if (checkOutEntity != null) {
                    HashMap<String, Object> mHashMap = new HashMap<>(16);
                    mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
                    mHashMap.put(Constants.pd_sceneType, Constants.TYPE_TRANSFER_QR);
                    mHashMap.put(Constants.sceneType, Constants.TYPE_SCAN_MERCHANT);
                    mHashMap.put(Constants.IS_FROM_SDK, isFromSdk);
                    ActivitySkipUtil.startAnotherActivity(ScanPayActivity.this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public Object getPresenter() {
        return null;
    }
}
