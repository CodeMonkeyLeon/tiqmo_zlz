package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillerEntity extends BaseEntity {

    //商户logo，可能为空
    public String imgUrl;
    //商户唯一Id，后续所有接口都需要该字段
    public String billerId;
    //商户名称
    public String billerName;

    public String billerDesc;

    //当前biller是否支持退款(当sadad的MOI的biller时候，必反回此值: 0表示不支持，1表示支持退款)
    public int supportRefundFlag;

    public boolean isSupportRefund(){
        return supportRefundFlag == 1;
    }
}
