package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpTwoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class HelpTwoAdapter extends BaseRecyclerAdapter<HelpTwoEntity> {
    public HelpTwoAdapter(@Nullable List<HelpTwoEntity> data) {
        super(R.layout.item_help_two,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, HelpTwoEntity helpTwoEntity, int position) {
        baseViewHolder.setText(R.id.tv_help_title, helpTwoEntity.title);
        ImageView iv_setting_pwd_arrow = baseViewHolder.getView(R.id.iv_setting_pwd_arrow);
        LocaleUtils.viewRotationY(mContext,iv_setting_pwd_arrow);
    }
}
