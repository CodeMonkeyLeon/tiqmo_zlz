package cn.swiftpass.wallet.tiqmo.module.home.entity

import java.io.Serializable

data class HistoryListParamEntity(
    val protocolNo: String?,
    val accountNo: String?,
    val typeList: List<String?>?,
    val tradeType: List<String?>?,
    val startDate: String?,
    val endDate: String?,
    val pageSize: Int,
    val currentPage: Int,
    val direction: String?,
    val orderNo: String?
) : Serializable
