package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.UnScrollViewPager;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMainPagerAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class PayBillMoiFetchActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_biller)
    RoundedImageView ivBiller;
    @BindView(R.id.tv_biller_name)
    TextView tvBillerName;
    @BindView(R.id.ll_sku_head)
    LinearLayout llSkuHead;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.indicator_paybill_main)
    MagicIndicator indicatorPaybillMain;
    @BindView(R.id.vp_paybill_main)
    UnScrollViewPager vpPaybillMain;

    private PayBillFetchFragment payBillFetchFragment;
    private PayBillRefundFragment payBillRefundFragment;

    private PayBillIOListEntity payBillIOListEntity;
    private PayBillOrderEntity payBillOrderEntity;

    private String billerId, sku, billerName, billerLogo;

    private String mChannelCode;
    private int sceneType = -1;
    private int supportRefundFlag;
    private int currentPosition;

    public static void startBillMoiServiceFetchActivity(Activity fromActivity,
                                                        PayBillIOListEntity payBillIOListEntity,
                                                        String billerId,
                                                        String billerName,
                                                        String billerLogo,
                                                        String channelCode,
                                                        int supportRefundFlag) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(Constants.paybill_billerIOListEntity, payBillIOListEntity);
        hashMap.put(Constants.paybill_billerId, billerId);
        hashMap.put(Constants.paybill_billerName, billerName);
        hashMap.put(Constants.paybill_billerLogo, billerLogo);
        hashMap.put(Constants.CHANNEL_CODE, channelCode);
        hashMap.put(Constants.paybill_supportRefundFlag, supportRefundFlag);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillMoiFetchActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startBillMoiServiceFetchActivity(Activity fromActivity,
                                                 String billerId,
                                                 PayBillIOListEntity payBillIOListEntity,
                                                 PayBillOrderEntity payBillOrderEntity,
                                                 String channelCode) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.paybill_billerId, billerId);
        mHashMap.put(Constants.paybill_billerIOListEntity, payBillIOListEntity);
        mHashMap.put(Constants.paybill_billerOrderEntity, payBillOrderEntity);
        mHashMap.put(Constants.CHANNEL_CODE, channelCode);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillMoiFetchActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_moi_fetch;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, ivHeadCircle);

        List<String> title = new ArrayList<>(Arrays.asList(this.getString(R.string.Tiqmo_Home_V1_Copy_16_11),
                this.getString(R.string.sprint17_46)));
        currentPosition = (LocaleUtils.isRTL(mContext) ? 1 : 0);
        if (getIntent().getExtras() != null) {
            billerId = getIntent().getExtras().getString(Constants.paybill_billerId);
            billerName = getIntent().getExtras().getString(Constants.paybill_billerName);
            billerLogo = getIntent().getExtras().getString(Constants.paybill_billerLogo);
            supportRefundFlag = getIntent().getExtras().getInt(Constants.paybill_supportRefundFlag);
            sku = getIntent().getExtras().getString(Constants.paybill_billerSku);
            payBillIOListEntity = (PayBillIOListEntity) getIntent().getSerializableExtra(Constants.paybill_billerIOListEntity);
            payBillOrderEntity = (PayBillOrderEntity) getIntent().getSerializableExtra(Constants.paybill_billerOrderEntity);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);


            if (payBillOrderEntity != null) {
                billerId = payBillOrderEntity.billerId;
                billerName = payBillOrderEntity.billerName;
                billerLogo = payBillOrderEntity.billerImgUrl;
                sku = payBillOrderEntity.sku;
                sceneType = payBillOrderEntity.sceneType;
                supportRefundFlag = payBillOrderEntity.supportRefundFlag;
            }
            tvBillerName.setText(billerName);
            Glide.with(mContext)
                    .load(billerLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.utilities))
                    .into(ivBiller);
        }

        List<Fragment> fragments = new ArrayList<>();
        payBillFetchFragment = PayBillFetchFragment.getInstance(payBillIOListEntity,billerId,billerName,mChannelCode,true,false);
        payBillRefundFragment = PayBillRefundFragment.getInstance(payBillIOListEntity,billerId,billerName,mChannelCode);
        fragments.add(payBillFetchFragment);
        fragments.add(payBillRefundFragment);
        if (LocaleUtils.isRTL(mContext)) {
            Collections.reverse(title);
            Collections.reverse(fragments);
        }
        if(supportRefundFlag == 0 || payBillOrderEntity != null){
            //不支持退款
            indicatorPaybillMain.setVisibility(View.GONE);
        }
        ImrMainPagerAdapter adapter = new ImrMainPagerAdapter(getSupportFragmentManager(), fragments);
        vpPaybillMain.setScrollEnable(false);// 禁止滑动
        vpPaybillMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        vpPaybillMain.setAdapter(adapter);
        if (LocaleUtils.isRTL(mContext)) {
            vpPaybillMain.setCurrentItem(title.size() - 1);
        }
        indicatorPaybillMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return title == null ? 0 : title.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(title.get(index));
                clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                clipPagerTitleView.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                clipPagerTitleView.setClipColor(Color.WHITE);
                clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));

                clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ButtonUtils.isFastDoubleClick()) {
                            return;
                        }
                        vpPaybillMain.setCurrentItem(index);
                    }
                });
                return clipPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setLineHeight(UIUtil.dip2px(context, 44));
                indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });
        indicatorPaybillMain.setNavigator(commonNavigator);
        commonNavigator.onPageSelected(LocaleUtils.isRTL(mContext) ? title.size() - 1 : 0);
        ViewPagerHelper.bind(indicatorPaybillMain, vpPaybillMain);

    }

    public void saveEditData(int position, String content) {
        if(payBillFetchFragment != null && currentPosition == (LocaleUtils.isRTL(mContext) ? 1 : 0)){
            payBillFetchFragment.saveEditData(position,content);
        }

        if(payBillRefundFragment != null && currentPosition == (LocaleUtils.isRTL(mContext) ? 0 : 1)){
            payBillRefundFragment.saveEditData(position,content);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_get_help})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_help:
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }
}
