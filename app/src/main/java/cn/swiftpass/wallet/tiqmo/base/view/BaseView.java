package cn.swiftpass.wallet.tiqmo.base.view;

public interface BaseView<P> extends Loading {

    P getPresenter();

    void showErrorDialog(String error);

    void showToast(String content);

    boolean isAttachedToPresenter();
}
