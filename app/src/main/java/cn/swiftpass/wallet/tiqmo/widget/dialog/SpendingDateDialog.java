package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SpendingDateAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.CategoryEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class SpendingDateDialog extends BottomDialog {
    private Context mContext;
    private MaxHeightRecyclerView rvDateList;

    private LinearLayoutManager mLayoutManager;
    private SpendingDateAdapter spendingDateAdapter;

    private DateItemClickListener dateItemClickListener;

    private int choosePosition;

    public void setDateItemClickListener(final DateItemClickListener dateItemClickListener) {
        this.dateItemClickListener = dateItemClickListener;
    }

    public List<CategoryEntity> dateList = new ArrayList<>();

    public void setDateList(final List<CategoryEntity> dateList, int choosePosition) {
        this.dateList = dateList;
        this.choosePosition = choosePosition;
        initRecyclerView(dateList);
    }

    private void initRecyclerView(List<CategoryEntity> dateList) {
        spendingDateAdapter = new SpendingDateAdapter(dateList);
        spendingDateAdapter.setChoosePosition(choosePosition);
        mLayoutManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 5));
//        rvDateList.addItemDecoration(myItemDecoration);
        rvDateList.setLayoutManager(mLayoutManager);
        spendingDateAdapter.bindToRecyclerView(rvDateList);
        spendingDateAdapter.setDataList(dateList);

        spendingDateAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                CategoryEntity categoryEntity = spendingDateAdapter.getDataList().get(position);
                if (dateItemClickListener != null) {
                    if (categoryEntity != null) {
                        choosePosition = position;
                        dateItemClickListener.chooseDate(position);
                        spendingDateAdapter.notifyDataSetChanged();
                        dismiss();
                    }
                }
            }
        });
    }

    public SpendingDateDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_spending_date, null);
        rvDateList = view.findViewById(R.id.rv_date_list);

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    public interface DateItemClickListener {
        void chooseDate(int position);
    }
}
