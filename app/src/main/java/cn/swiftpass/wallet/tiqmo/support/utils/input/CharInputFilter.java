package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;

/**
 * 字符过滤器，给定不能输入字符集
 */
public class CharInputFilter implements InputFilter {
    private Pattern mPattern;

    /**
     * 需要过滤的字符集
     *
     * @param charSet 字符集，如"12",则1，2，12都不能输入
     */
    public CharInputFilter(String charSet) {
        mPattern = Pattern.compile("[" + charSet + "]+");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (mPattern.matcher(source).matches()) {
            return "";
        }
        return source;
    }
}
