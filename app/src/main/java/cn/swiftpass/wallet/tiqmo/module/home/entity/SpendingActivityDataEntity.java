package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SpendingActivityDataEntity extends BaseEntity {

    public String receivedAmount;
    public String receivedTxnCurrency;
    public String receivedAmountRatio;
    public String sendingAmount;
    public String sendingTxnCurrency;
    public String sendingAmountRatio;
}
