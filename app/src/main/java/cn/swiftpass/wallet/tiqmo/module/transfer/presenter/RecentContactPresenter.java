package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RecentContactPresenter implements TransferContract.RecentContactPresenter {

    TransferContract.RecentContactView recentContactView;

    @Override
    public void getBeneficiaryList() {
        AppClient.getInstance().getBeneficiaryList(new LifecycleMVPResultCallback<BeneficiaryListEntity>(recentContactView, true) {
            @Override
            protected void onSuccess(BeneficiaryListEntity result) {
                recentContactView.getBeneficiaryListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                recentContactView.getBeneficiaryListFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getRecentContact() {
        AppClient.getInstance().getTransferRecentContactList(new LifecycleMVPResultCallback<TransferContactListEntity>(recentContactView, false) {
            @Override
            protected void onSuccess(TransferContactListEntity result) {
                recentContactView.getRecentContactSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                recentContactView.getRecentContactFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getQrCode(String currencyCode) {
        AppClient.getInstance().getQrCode(currencyCode, new LifecycleMVPResultCallback<QrCodeEntity>(recentContactView, true) {
            @Override
            protected void onSuccess(QrCodeEntity result) {
                recentContactView.getQrCodeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                recentContactView.getQrCodeFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(TransferContract.RecentContactView recentContactView) {
        this.recentContactView = recentContactView;
    }

    @Override
    public void detachView() {
        this.recentContactView = null;
    }
}
