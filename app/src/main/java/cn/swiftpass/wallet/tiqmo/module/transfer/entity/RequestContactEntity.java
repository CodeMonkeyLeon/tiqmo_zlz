package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class RequestContactEntity extends BaseEntity {

    public String callingCode = SpUtils.getInstance().getCallingCode();
    public String phone;
    public String contactsName;
}
