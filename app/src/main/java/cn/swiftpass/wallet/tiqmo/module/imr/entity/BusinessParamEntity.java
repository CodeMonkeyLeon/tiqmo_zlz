package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class BusinessParamEntity extends BaseEntity {
    public List<SelectInfoEntity> sourceOfFund;
    public List<SelectInfoEntity> profession;
    public List<SelectInfoEntity> companyType;
    public List<SelectInfoEntity> annualIncome;
    public List<SelectInfoEntity> businessParamList = new ArrayList<>();
}