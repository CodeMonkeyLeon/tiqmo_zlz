package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SpendingBudgetListEntity extends BaseEntity {

    //总共已花预算金额 单位：最小币种单位
    public String totalAlreadySpendingAmount;
    //总预算金额 单位：最小币种单位
    public String totalSpendingBudgetAmount;

    //预算分类详情列表
    public List<SpendingBudgetEntity> userSpendingBudgetInfoList = new ArrayList<>();
}
