package cn.swiftpass.wallet.tiqmo.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.input.ChineseInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.input.EnglishInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.input.InputUtils;


public class EditTextWithDel extends LinearLayout {
    /**
     * 正则表达式
     */
    private Context context;
    private LinearLayout ll_EditTextWithDel;
    private EditText mEditText;

    public ImageView getmDelImageView() {
        return mDelImageView;
    }

    private ImageView mDelImageView;
    private LinearLayout ll_Image_layout;
    private View ll_line;
    private LinearLayout ll_errorTextView;
    private ConstraintLayout ll_login;
    private ConstraintLayout ll_bank;
    private ImageView iv_login_del;
    private ImageView iv_bank_del;
    private TextView tv_login_hint;
    private TextView tv_bank_hint;
    private TextView tv_label;
    private TextView tv_bank_label;
    private EditText edit_login;
    private EditText edit_bank;
    private TextInputLayout tlEdit;
    private TextView mErrorTextView;
    private TextView mLeftTitleView;
    private boolean hasErrorView = true; //是否显示错误提示的view,在定制EditText的某些情况下不需要显示ErrorView
    private boolean showLoginEdittext = false;
    private boolean showBankEdittext = false;
    private boolean isPayPwd = false;
    private boolean isLoginPwd = false;
    private boolean isUserName = false;
//    private String digits;
    private float letterSpacing;

    public void setShowDelView(boolean showDelView) {
        this.showDelView = showDelView;
    }

    private boolean showDelView = true;
    private boolean hasDelView = true;   //是否显示删除图标 ,在定制EditText的某些情况下需要永远的不显示删除按钮则此值为false
    private TextWatcher mTextWatcher;
    private List<RegExpBean> mRegExp = new ArrayList<RegExpBean>();//所有要满足的正则表达式的列表
    private RegExpBean mLengthRegExp = null;//对字符串长度的正则，如果长度不满足就显示errorMsg

    //属性设置采用dp
    private int mLeftPadding;
    private int mRightPadding;

    public void changeTheme(Context mContext) {
        mEditText.setTextColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44));
    }

    public TextInputLayout getTlEdit() {
        return tlEdit;
    }

    public EditTextWithDel.onFocusChangeListener getOnFocusChangeExtraListener() {
        return onFocusChangeExtraListener;
    }

    public void setOnFocusChangeExtraListener(EditTextWithDel.onFocusChangeListener onFocusChangeExtraListener) {
        this.onFocusChangeExtraListener = onFocusChangeExtraListener;
    }

    private onFocusChangeListener onFocusChangeExtraListener;
    private onFocusChangeListener onFocusLoginExtraListener;

    public void setOnFocusLoginExtraListener(EditTextWithDel.onFocusChangeListener onFocusLoginExtraListener) {
        this.onFocusLoginExtraListener = onFocusLoginExtraListener;
    }

    public void setFocusableFalse() {
        this.getEditText().setCursorVisible(false);
        this.getEditText().setInputType(InputType.TYPE_NULL);
        this.getEditText().setFocusable(false);
        this.getEditText().setFocusableInTouchMode(false);
    }

    public void setViewDisable() {
        setFocusableFalse();
        this.setAlpha(0.4f);
    }

    public EditTextWithDel(Context context) {
        super(context);
        init(context, null, 0);
    }

    public EditTextWithDel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public EditTextWithDel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public void setBankPreText(String preText, boolean isBold) {
        if (isBold) {
            tv_bank_label.setTypeface(Typeface.createFromAsset(ProjectApp.getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
        } else {
            tv_bank_label.setTypeface(Typeface.createFromAsset(ProjectApp.getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
        }
        tv_bank_label.setTextColor(context.getColor(ThemeSourceUtils.getSourceID(context, R.attr.color_80white_3a3b44)));
        tv_bank_label.setText(preText);
    }

    public class RegExpBean {
        public RegExpBean(String mRegExp, String mErrorMsg) {
            this(mRegExp, mErrorMsg, true);
        }

        public RegExpBean(String mRegExp, String mErrorMsg, boolean isNotRegExp) {
            this.mErrorMsg = mErrorMsg;
            this.mRegExp = mRegExp;
            this.regExpIsTrue = isNotRegExp;
        }

        public String mRegExp;
        public String mErrorMsg;
        /**
         * 正则表达式有两种情况，第一种是要符合正则表达式的条件就算通过，另外一种是不符合正则表达式的条件才算通过
         * 假设条件是要限制输入的不能全部为数字，传入^[\\d)+$表示全为数字，让regExpIsTrue=false，
         * 此时正则通过则表示不能全为数字。默认是正向的。
         */
        public boolean regExpIsTrue = true;
    }

    public TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (mTextWatcher != null) {
                mTextWatcher.beforeTextChanged(s, start, count, after);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mTextWatcher != null) {
                mTextWatcher.onTextChanged(s, start, before, count);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mTextWatcher != null) {
                mTextWatcher.afterTextChanged(s);
            }
            if (ll_line != null) {
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            }
            if (ll_errorTextView != null) {
                ll_errorTextView.setVisibility(GONE);
            }
            if (mErrorTextView != null) {
                mErrorTextView.setVisibility(GONE);
            }
            if (TextUtils.isEmpty(s.toString())) {
                mEditText.setTypeface(Typeface.createFromAsset(ProjectApp.getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
                hideDelView();
            } else {
                mEditText.setTypeface(Typeface.createFromAsset(ProjectApp.getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                hasDelView = true;
                showDelView();
            }
        }
    };

    public void addTextChangedListener(TextWatcher watcher) {
        mTextWatcher = watcher;
    }

    public OnClickListener delClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mEditText.setText(null);
        }
    };

    public void animationDown() {
        edit_login.setVisibility(View.GONE);
        tv_label.setVisibility(View.GONE);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(tv_login_hint, "scaleX", 1);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(tv_login_hint, "scaleY", 1);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(100);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.play(scaleX).with(scaleY); //两个动画同时开始
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                edit_login.setVisibility(View.VISIBLE);
                tv_label.setVisibility(View.GONE);
//                edit_login.requestFocus();
//                //弹出键盘
//                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.showSoftInput(edit_login, 0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void animationUp(EditText edit, TextView tv_hint, TextView tv_label) {
        if (LocaleUtils.isRTL(context)) {
            tv_hint.setPivotX(tv_hint.getWidth());  //从左到右缩放
        }
//        tv_hint.setPivotY(0);   // Y方向底边
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(tv_hint, "scaleX", 0.7f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(tv_hint, "scaleY", 0.7f);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(100);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.play(scaleX).with(scaleY); //两个动画同时开始
        animatorSet.start();

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                edit.setVisibility(View.VISIBLE);
                String label = tv_label.getText().toString().trim();
                if (!TextUtils.isEmpty(label)) {
                    tv_label.setVisibility(View.VISIBLE);
                }
                edit.requestFocus();
                //弹出键盘
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edit, 0);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void init(Context context, AttributeSet attrs, int defStyle) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.edit_text_with_del, this);
        ll_login = findViewById(R.id.ll_edit_login);
        ll_bank = findViewById(R.id.ll_edit_bank);
        edit_login = findViewById(R.id.edit_login);
        edit_bank = findViewById(R.id.edit_bank);
        iv_login_del = findViewById(R.id.iv_login_del);
        iv_bank_del = findViewById(R.id.iv_bank_del);
        tv_login_hint = findViewById(R.id.tv_login_hint);
        tv_bank_hint = findViewById(R.id.tv_bank_hint);
        tv_label = findViewById(R.id.tv_label);
        tv_bank_label = findViewById(R.id.tv_bank_label);
        ll_EditTextWithDel = findViewById(R.id.llayout_edit_text);
        mEditText = findViewById(R.id.edit_text);
        mDelImageView = findViewById(R.id.iv_del);
        tlEdit = findViewById(R.id.tl_edit);
        mLeftTitleView = findViewById(R.id.id_left_title);
        ll_Image_layout = findViewById(R.id.ll_Image_layout);
        ll_line = findViewById(R.id.ll_line);
        ll_errorTextView = findViewById(R.id.ll_errorTextView);
        mErrorTextView = findViewById(R.id.tv_error);
        mEditText.addTextChangedListener(textWatcher);
        mDelImageView.setOnClickListener(delClickListener);
        mDelImageView.setVisibility(GONE);
//        mEditText.setFocusableInTouchMode(true);
        mEditText.setOnFocusChangeListener(onFocusChangeListener);
        edit_login.setOnFocusChangeListener(onLoginFocusChangeListener);
        edit_bank.setOnFocusChangeListener(onBankFocusChangeListener);

        edit_bank.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ll_line != null) {
                    ll_line.setBackgroundColor(getNormalUnderLineBg());
                }
                if (ll_errorTextView != null) {
                    ll_errorTextView.setVisibility(GONE);
                }
                if (mErrorTextView != null) {
                    mErrorTextView.setVisibility(GONE);
                }
                if (TextUtils.isEmpty(s.toString())) {
                    iv_bank_del.setVisibility(View.GONE);
                } else {
                    animationUp(edit_bank, tv_bank_hint, tv_bank_label);
                    iv_bank_del.setVisibility(View.VISIBLE);
                }
            }
        });

        iv_bank_del.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_bank != null) {
                    edit_bank.setText(null);
                }
            }
        });
        edit_login.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ll_line != null) {
                    ll_line.setBackgroundColor(getNormalUnderLineBg());
                }
                if (ll_errorTextView != null) {
                    ll_errorTextView.setVisibility(GONE);
                }
                if (mErrorTextView != null) {
                    mErrorTextView.setVisibility(GONE);
                }
                if (TextUtils.isEmpty(s.toString())) {
                    iv_login_del.setVisibility(View.GONE);
                } else {
                    iv_login_del.setVisibility(View.VISIBLE);
                }
            }
        });

        iv_login_del.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit_login != null){
                    edit_login.setText(null);
                }
            }
        });

        final Resources.Theme theme = context.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.EditTextWithDel, defStyle, 0);
        //以下的部分为默认的值，如有需要可以定义方法可以修改下面的值
        int maxLength = -1;
        float textSize = 15;
        CharSequence text = "";
        CharSequence hint = null;
        boolean singleLine = false;
        boolean showLeftTitle = false;
        int ellipsize = -1;
        int inputType = EditorInfo.TYPE_NULL;
        ColorStateList textColor = null;
        ColorStateList textColorHint = null;
        int n = a.getIndexCount();
        //设置默认的值
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.EditTextWithDel_hasErrorView) {
                hasErrorView = a.getBoolean(attr, true);
            } else if (attr == R.styleable.EditTextWithDel_showDelView) {
                showDelView = a.getBoolean(attr, true);
            } else if (attr == R.styleable.EditTextWithDel_textSize) {
                textSize = a.getDimension(attr, textSize);
                mEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else if (attr == R.styleable.EditTextWithDel_maxLength) {
                maxLength = a.getInt(attr, -1);
            } else if (attr == R.styleable.EditTextWithDel_hasLeftTitle) {
                showLeftTitle = a.getBoolean(attr, showLeftTitle);
            } else if (attr == R.styleable.EditTextWithDel_singleLine) {
                singleLine = a.getBoolean(attr, singleLine);
            } else if (attr == R.styleable.EditTextWithDel_minLines) {
                mEditText.setMinLines(a.getInt(attr, -1));
            } else if (attr == R.styleable.EditTextWithDel_maxLines) {
                mEditText.setMaxLines(a.getInt(attr, -1));
            } else if (attr == R.styleable.EditTextWithDel_lines) {
                mEditText.setLines(a.getInt(attr, -1));
            } else if (attr == R.styleable.EditTextWithDel_textColor) {
                textColor = a.getColorStateList(attr);
            } else if (attr == R.styleable.EditTextWithDel_textColorHint) {
                textColorHint = a.getColorStateList(attr);
            } else if (attr == R.styleable.EditTextWithDel_textIsSelectable) {
                mEditText.setTextIsSelectable(a.getBoolean(attr, false));
            } else if (attr == R.styleable.EditTextWithDel_letterSpacing) {
                letterSpacing = a.getFloat(attr, 1.0f);
                mEditText.setLetterSpacing(letterSpacing);
            } else if (attr == R.styleable.EditTextWithDel_ellipsize) {
                ellipsize = a.getInt(attr, ellipsize);
            } else if (attr == R.styleable.EditTextWithDel_inputType) {
                inputType = a.getInt(attr, EditorInfo.TYPE_NULL);
            } else if (attr == R.styleable.EditTextWithDel_hint) {
                hint = a.getText(attr);
            } else if (attr == R.styleable.EditTextWithDel_text) {
                text = a.getText(attr);
            } else if (attr == R.styleable.EditTextWithDel_regExpText) {
                //mRegExp.add(a.getText(attr).toString());
            } else if (attr == R.styleable.EditTextWithDel_errorText) {
                //mErrorMsg.add(a.getText(attr).toString());
            }else if (attr == R.styleable.EditTextWithDel_showLoginEdittext) {
                showLoginEdittext = a.getBoolean(attr, false);
            } else if (attr == R.styleable.EditTextWithDel_showBankEdittext) {
                showBankEdittext = a.getBoolean(attr, false);
            } else if (attr == R.styleable.EditTextWithDel_isLoginPwd) {
                isLoginPwd = a.getBoolean(attr, false);
            } else if (attr == R.styleable.EditTextWithDel_isPayPwd) {
                isPayPwd = a.getBoolean(attr, false);
            } else if (attr == R.styleable.EditTextWithDel_isUserName) {
                isUserName = a.getBoolean(attr, false);
            }
        }
        switch (ellipsize) {
            case 1:
                mEditText.setEllipsize(TextUtils.TruncateAt.START);
                break;
            case 2:
                mEditText.setEllipsize(TextUtils.TruncateAt.MIDDLE);
                break;
            case 3:
                mEditText.setEllipsize(TextUtils.TruncateAt.END);
                break;
            case 4:
                mEditText.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                break;
            default:
                break;
        }
        if (textColor != null) {
            mEditText.setTextColor(textColor);
        }
        if (textColorHint != null) {
//            mEditText.setHintTextColor(textColorHint);
            tlEdit.setDefaultHintTextColor(textColorHint);
        }

        if (showLoginEdittext) {
            ll_login.setVisibility(View.VISIBLE);
            ll_bank.setVisibility(View.GONE);
            tlEdit.setVisibility(View.GONE);
        } else if (showBankEdittext) {
            ll_login.setVisibility(View.GONE);
            ll_bank.setVisibility(View.VISIBLE);
            tv_bank_hint.setText(hint);
            tlEdit.setVisibility(View.GONE);
        } else {
            ll_login.setVisibility(View.GONE);
            ll_bank.setVisibility(View.GONE);
            tlEdit.setVisibility(View.VISIBLE);
        }

        if (maxLength >= 0) {
            mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        }
        if (showLeftTitle) {
            mLeftTitleView.setVisibility(VISIBLE);
        } else {
            mLeftTitleView.setVisibility(GONE);
        }
        if (!TextUtils.isEmpty(text)) {
            mEditText.setText(text);
        }
        if (hint != null) {
            tlEdit.setHint(hint);
            SpannableString ss = new SpannableString(hint);
            //14代表hint显示的字体大小
            AbsoluteSizeSpan ass = new AbsoluteSizeSpan(18, true);
            ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tlEdit.setHint(new SpannedString(ss));
        }
        mEditText.setSingleLine(singleLine);
        //必须放到setSingleLine后，否则密码不能隐藏
        if (inputType != EditorInfo.TYPE_NULL) {
            mEditText.setInputType(inputType);
        }
        mLeftPadding = a.getDimensionPixelSize(R.styleable.EditTextWithDel_paddingStart, 0);
        mRightPadding = a.getDimensionPixelSize(R.styleable.EditTextWithDel_paddingEnd, 0);
        mEditText.setPaddingRelative(mLeftPadding, mEditText.getPaddingTop(), mRightPadding, mEditText.getPaddingBottom());
        if (isLoginPwd) {
            if (maxLength >= 0) {
                mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_NUMBER)});
            }
        }
        if (isPayPwd) {
            if (maxLength >= 0) {
                mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6), new NormalInputFilter(NormalInputFilter.NUMBER)});
            }
        }
        if (isUserName) {
            if (maxLength >= 0) {
                mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(60), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_SPACE)});
            }
        }
        a.recycle();
    }

    /***
     * 正则判断表达式字符的长度
     * addLengthRegExp(getString(R.string.reg_exp_length, 4, 20), getString(R.string.error_member_nickname));
     * 注意要判断中文的长度，一个中文等于2个字符
     * @param mRegExp
     * @param mErrorMsg
     */
    public void addLengthRegExp(String mRegExp, String mErrorMsg) {
        mLengthRegExp = new RegExpBean(mRegExp, mErrorMsg);
    }

    /**
     * 设置正则表达式
     *
     * @param mRegExp
     * @param mErrorMsg
     */
    public void addRegExp(String mRegExp, String mErrorMsg) {
        this.mRegExp.add(new RegExpBean(mRegExp, mErrorMsg));
    }

    /**
     * 设置正则表达式
     *
     * @param mRegExp
     * @param mErrorMsg
     */
    public void addRegExpIsFalse(String mRegExp, String mErrorMsg) {
        this.mRegExp.add(new RegExpBean(mRegExp, mErrorMsg, false));
    }

    /**
     * 设置只能输入中文
     */
    public void setOnlyInputChinese() {
        InputUtils.setInputFilter(mEditText, new ChineseInputFilter());
    }

    /**
     * 设置只能输入英文
     */
    public void setOnlyInputEnglish() {
        InputUtils.setInputFilter(mEditText, new EnglishInputFilter());
    }

    public String getHint() {
        if (mEditText == null) {
            return "";
        }
        return mEditText.getHint().toString().trim();
    }

    public void setHint(String HintString) {

        if (HintString != null) {
            mEditText.setHint(HintString);
        }
    }

    /**
     * 开始正则
     *
     * @return 是否正则成功
     */
    public boolean onRegExp(boolean isShowErrorView) {
        LogUtils.i(this, mEditText.getText());
        if (mLengthRegExp != null) {//对字符长度的正则
            //即ASCII编码不在0-255的字符,也就是汉字转换成两个字符来判断长度
            String newStr = mEditText.getText().toString().replaceAll("[^\\x00-\\xff]", "**");
            boolean res = onOneRegExp(mLengthRegExp, newStr, isShowErrorView);
            if (!res) {
                return res;
            }
        }
        //如果长度正则通过才进行下面的条件正则
        for (int i = 0; i < mRegExp.size(); i++) {
            RegExpBean regExp = mRegExp.get(i);
            LogUtils.i(this, regExp);
            boolean res = onOneRegExp(regExp, mEditText.getText().toString(), isShowErrorView);
            if (!res) {
                return res;
            }
        }
        hideErrorView();
        return true;
    }

    private boolean onOneRegExp(RegExpBean regExp, String text, boolean isShowErrorView) {
        if (!TextUtils.isEmpty(regExp.mRegExp)) {
            if (matcherRegExp(regExp.mRegExp, text) != regExp.regExpIsTrue) {
                if (isShowErrorView) {
                    showErrorViewWithMsg(regExp.mErrorMsg);
                }
                return false;
            }
        }
        return true;
    }

    public boolean matcherRegExp(String regExp, String str) {
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    private OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                hideErrorView();
                mDelImageView.setVisibility((mEditText.getText().toString().length() > 0 && showDelView) ? VISIBLE : GONE);
                hasDelView = mEditText.getText().toString().length() > 0;
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            } else {
//                onRegExp(true);
                AndroidUtils.hideKeyboard(v);
                mDelImageView.setVisibility(GONE);
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            }
            if (onFocusChangeExtraListener != null) {
                //外部view处理
                onFocusChangeExtraListener.onFocusChange(hasFocus);
            }
        }
    };

    private OnFocusChangeListener onLoginFocusChangeListener = new OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                if(TextUtils.isEmpty(edit_login.getText())){
                    animationUp(edit_login, tv_login_hint, tv_label);
                }
                hideErrorView();
                mDelImageView.setVisibility((edit_login.getText().toString().length() > 0 && showDelView) ? VISIBLE : GONE);
                hasDelView = edit_login.getText().toString().length() > 0;
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            } else {
//                onRegExp(true);
                AndroidUtils.hideKeyboard(v);
                mDelImageView.setVisibility(GONE);
                iv_login_del.setVisibility(GONE);
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            }
            if (onFocusLoginExtraListener != null) {
                //外部view处理
                onFocusLoginExtraListener.onFocusChange(hasFocus);
            }
        }
    };

    private OnFocusChangeListener onBankFocusChangeListener = new OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                if (TextUtils.isEmpty(edit_bank.getText())) {
                    animationUp(edit_bank, tv_bank_hint, tv_bank_label);
                }
                hideErrorView();
                mDelImageView.setVisibility((edit_bank.getText().toString().length() > 0 && showDelView) ? VISIBLE : GONE);
                hasDelView = edit_bank.getText().toString().length() > 0;
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            } else {
//                onRegExp(true);
                AndroidUtils.hideKeyboard(v);
                mDelImageView.setVisibility(GONE);
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            }
            if (onFocusLoginExtraListener != null) {
                //外部view处理
                onFocusLoginExtraListener.onFocusChange(hasFocus);
            }
        }
    };

    /**
     * 左边文案
     *
     * @param title
     */
    public void setLeftTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mLeftTitleView.setText(title);
        }

    }

    /**
     * EditText 获取焦点的时候 底部的线条变黑
     */
    public void setBottomLineHighSel() {

        ll_line.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.black));
    }

    public void setBottomLineNoSel() {

        ll_line.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_dddddd));
    }

    public void showDelView() {
        if (mDelImageView != null) {
            if (showDelView) {
                mDelImageView.setVisibility(VISIBLE);
            } else {
                mDelImageView.setVisibility(GONE);
            }
        }
    }

    public void hideDelView() {
        if (mDelImageView != null) {
            mDelImageView.setVisibility(GONE);
            hasDelView = false;
        }
    }

    public void showErrorViewWithMsg(String errorMsg) {
        if (mErrorTextView != null && !TextUtils.isEmpty(errorMsg)) {
            mErrorTextView.setText(errorMsg);
            if (hasErrorView) {
                ll_errorTextView.setVisibility(VISIBLE);
                mErrorTextView.setVisibility(VISIBLE);
                if (ll_line != null) {
                    ll_line.setBackgroundColor(getErrUnderLineBg());
                }
            } else {
                ll_errorTextView.setVisibility(GONE);
                mErrorTextView.setVisibility(GONE);
            }
        } else {
            ll_errorTextView.setVisibility(GONE);
            mErrorTextView.setVisibility(GONE);
            if (ll_line != null) {
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            }
        }
    }

    public void setErrorColor(int colorId) {
        if (mErrorTextView != null) {
            mErrorTextView.setTextColor(ContextCompat.getColor(getContext(), colorId));
        }
    }

    public void hideErrorView() {
        if (mErrorTextView != null) {
            ll_errorTextView.setVisibility(GONE);
            mErrorTextView.setVisibility(GONE);
            if (ll_line != null) {
                ll_line.setBackgroundColor(getNormalUnderLineBg());
            }
        }
    }

    /**
     * 获取输入文字，如果正则不通过则返回null,默认显示错误信息
     *
     * @return
     */
    public String getText() {
        return getText(true);
    }

    /**
     * 获取输入文字，如果正则不通过则返回null
     *
     * @param isShowErrorView 是否显示错误提示信息
     * @return
     */
    public String getText(boolean isShowErrorView) {
        if (mEditText != null) {
            if (onRegExp(isShowErrorView)) {
                return mEditText.getText().toString().trim();
            }
        }
        return null;
    }

    /**
     * 设置内容
     *
     * @param text
     */
    public void setContentText(String text) {
        if (mEditText != null) {
            mEditText.setText(text);
            if(!TextUtils.isEmpty(text)) {
                setSelectionToEnd();
            }
        }
    }

    public EditText getTextView() {
        return mEditText;
    }

    public EditText getEditText() {
        if (showLoginEdittext) {
            return edit_login;
        } else if (showBankEdittext) {
            return edit_bank;
        }
        return mEditText;
    }

    public TextView getLeftTitleView() {
        return mLeftTitleView;
    }

    public void setSelectionToEnd() {
        if (mEditText != null) {
            mEditText.setSelection(mEditText.getText().length());//将光标移至文字末尾
        }
    }


    public void showPasswordText() {
        mEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        mEditText.postInvalidate();
        mEditText.setSelection(mEditText.length());//将光标追踪到内容的最后
    }

    public void hidePasswordText() {
        mEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        mEditText.postInvalidate();
        mEditText.setSelection(mEditText.length());//将光标追踪到内容的最后
    }

    //为整个EditText设置背景,如果需要为editText设置边框或者背景
    public void setEditTextBackgroundDrawable(int resid) {
        ll_EditTextWithDel.setBackgroundResource(resid);
    }

    //如果更改EditText的背景，或者输入正则不通过，记得要把线条设置成GONE
    public void setLineVisible(boolean isVisible) {
        ll_line.setVisibility(isVisible ? VISIBLE : GONE);
    }

    public void setEditTextEnable(boolean enable) {
        mEditText.setEnabled(enable);
    }

    public void setLineColorRed() {
        if(ll_line != null) {
            ll_line.setBackgroundColor(getErrUnderLineBg());
        }
    }

    //是否显示删除按钮
    public void showDeleteView(Boolean show) {
        hasDelView = show;
    }

    //是否显示出错的提示框
    public void showErrorView(Boolean show) {
        hasErrorView = show;
    }

    private int getNormalUnderLineBg(){
        return ContextCompat.getColor(getContext(),
                ThemeSourceUtils.getSourceID(getContext(), R.attr.edittext_under_line_bg_color));
    }

    private int getErrUnderLineBg(){
        return ContextCompat.getColor(getContext(), R.color.color_70fd1d2d);
    }

    //设置editText的文字的显示发gravity，是居中还是靠左，还是
    public void setEditTextGravity(int textGravity) {
        mEditText.setGravity(textGravity);
    }

    /**
     * 内部外部viewd都需要事件监听
     */
    public interface onFocusChangeListener {

        void onFocusChange(boolean hasFocus);
    }
}
