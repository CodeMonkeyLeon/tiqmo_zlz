package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeDomeOrderEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class RechargeDomeOrderAdapter extends BaseRecyclerAdapter<RechargeDomeOrderEntity> {
    public RechargeDomeOrderAdapter(@Nullable List<RechargeDomeOrderEntity> data) {
        super(R.layout.item_dome_order,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, RechargeDomeOrderEntity rechargeDomeOrderEntity, int position) {
        String voucherBrandName = rechargeDomeOrderEntity.voucherBrandName;
        baseViewHolder.setText(R.id.tv_operator_name,voucherBrandName);
        RoundedImageView ivOperator = baseViewHolder.getView(R.id.iv_operator);
        String operatorLogo = ThemeUtils.isCurrentDark(mContext) ? rechargeDomeOrderEntity.voucherBrandDarkLogo : rechargeDomeOrderEntity.voucherBrandLightLogo;

        if (!TextUtils.isEmpty(operatorLogo)) {
            Glide.with(mContext).clear(ivOperator);
            Glide.with(mContext)
                    .load(operatorLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivOperator);
        }
    }
}
