package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class EVoucherDetailEntity extends BaseEntity {

    //代金券详情title
    public String voucherDetailTitle;

    //代金券详情描述
    public String voucherDetailDesc;

    /**
     * 代金券详情 亮色版 图片 url
     */
    public String voucherLightPicUrl;

    /**
     * 代金券详情 暗色版 图片 url
     */
    public String voucherDarkPicUrl;
}
