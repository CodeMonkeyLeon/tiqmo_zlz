package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AccountInfoEntity;

public class AccountInfoContract {

    public interface View extends BaseView<AccountInfoContract.Presenter> {
        void getAccountInfoSuccess(AccountInfoEntity accountInfoEntity);

        void getAccountInfoFail(String errorCode, String errorMsg);

        void getHistoryListSuccess(TransferHistoryMainEntity result);

        void getHistoryListFail(String errorCode, String errorMsg);

        void getHistoryDetailSuccess(TransferHistoryDetailEntity result);

        void getHistoryDetailFail(String errorCode, String errorMsg);

        void deleteTransferHistorySuccess(Void result);

        void deleteTransferHistoryFail(String errorCode, String errorMsg);
    }

    public interface Presenter extends BasePresenter<AccountInfoContract.View> {
        void getAccountInfo(String type);

        void getHistoryList(String protocolNo, String accountNo, List<String> paymentType,
                            List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum, String direction, String orderNo);

        void getHistoryDetail(String orderNo, String orderType,String queryType);

        void deleteTransferHistory(String orderNo);
    }
}
