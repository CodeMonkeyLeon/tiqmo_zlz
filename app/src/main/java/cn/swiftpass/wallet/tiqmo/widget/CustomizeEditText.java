package cn.swiftpass.wallet.tiqmo.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.lang.reflect.Field;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class CustomizeEditText extends EditText {

    private final float DIMEN_5_DP = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
    private final float DIMEN_8_DP = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
    private final float DIMEN_16_DP = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
    private final float DIMEN_12_DP = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
    private final long DURATION_SHORT = getResources().getInteger(android.R.integer.config_shortAnimTime);

    private final int COLOR_HINT;

    // Paddings
    private float basePaddingLeft;
    private float basePaddingTop;
    private float basePaddingRight;
    private float basePaddingBottom;
    private float paddingLeft;
    private float paddingTop;
    private float paddingRight;
    private float paddingBottom;
//    private float baseHeight;

    // Unselected line
    private Paint underLinePaint;
    private Path underLine;
    private int underLineColor;

    // Common line fields
    private float lineHeight;

    // Error text
    private int errorColor;
    private CharSequence errorText;
    private TextPaint errorTextPaint;
    private boolean isShowError = false;

    // Left text
    private CharSequence leftText;
    private TextPaint leftTextPaint;
    private boolean isLeftTextShow = false;
    private boolean isTextShow = false;
    private Drawable icStart; // left icon

    // Floating label
    private boolean floatingLabel;
    private TextPaint labelTextPaint;
    private float labelTextSize;
    private int labelTextColor;
    private AnimatorSet labelAnimation;
    private long labelAnimationElapsedDuration;
    private float labelX, labelY;

    private Drawable iconEnd; // right icon
    private int rightIcX;
    private int rightIcY;
    private int rightIcWidth;
    private int rightIcHeight;
    private int rightIcShowMode = 2;
    private boolean isRightIconShow = false;
    private CharSequence rightText;
    private int rightTextColor;
    private TextPaint rightTextPaint;
    private boolean inputNumber = false;

    //是否去掉左边前缀的空格
    private boolean deleteLeftSpace;
    private OnViewClickListener clickListener;
    private OnLeftViewClickListener onLeftViewClickListener;
    private OnUnableClickListener onUnableClickListener;

    private Context context;

    boolean isLogin = false;

    boolean isSpecialPaste = true;

    public void setOnLeftViewClickListener(final OnLeftViewClickListener onLeftViewClickListener) {
        this.onLeftViewClickListener = onLeftViewClickListener;
    }

    public void setClickListener(OnViewClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setOnUnableClickListener(OnUnableClickListener onUnableClickListener) {
        this.onUnableClickListener = onUnableClickListener;
    }

    public CustomizeEditText(Context context) {
        this(context, null);
    }

    public CustomizeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        COLOR_HINT = getCurrentHintTextColor();
        init(context, attrs);
    }

    public CustomizeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        COLOR_HINT = getCurrentHintTextColor();
        init(context, attrs);
    }

    @SuppressLint("NewApi")
    public CustomizeEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        COLOR_HINT = getCurrentHintTextColor();
        init(context, attrs);
    }

    public void setText(String text) {
        super.setText(text);
        if (TextUtils.isEmpty(text)) {
            clearFocus();
            isTextShow = false;
            if (labelAnimation != null) {
                labelAnimation.cancel();
                labelAnimation = createLabelAnimation(labelX, labelY, getScrollX(), getBaseline(),
                        labelTextSize, getTextSize(), labelAnimationElapsedDuration, false);
                //赋值的时候就不要动画效果了
                labelAnimation.setDuration(0);
                labelAnimation.start();
            } else {
                labelAnimation = createLabelAnimation(getScrollX(), DIMEN_16_DP + DIMEN_12_DP,
                        getScrollX(), getBaseline(), DIMEN_12_DP, getTextSize(), 0, false);
                //赋值的时候就不要动画效果了
                labelAnimation.setDuration(0);
                labelAnimation.start();
            }
            return;
        }
        if (floatingLabel && !isLeftTextShow && !isTextShow) {
            labelAnimation = createLabelAnimation(getScrollX(), getBaseline(), getScrollX(),
                    DIMEN_16_DP + DIMEN_12_DP, getTextSize(), DIMEN_12_DP, 0, true);
            //赋值的时候就不要动画效果了
            labelAnimation.setDuration(0);
            labelAnimation.start();
            isTextShow = true;
        }
    }

    private void init(Context context, AttributeSet attrs) {
        this.context = context;
        // Override API padding
        basePaddingLeft = 0;
        basePaddingTop = DIMEN_16_DP;
        basePaddingRight = 0;
        basePaddingBottom = DIMEN_8_DP;

        // Remove text top/bottom padding
        setIncludeFontPadding(false);
        // Remove API background
        setBackgroundDrawable(null);

        // Initialize the unselected line
        underLinePaint = new Paint();
        underLinePaint.setStyle(Paint.Style.STROKE);
        underLine = new Path();

        setHintTextColor(Color.TRANSPARENT);

        // 0btain XML attributes
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomizeEditText, 0, 0);
            errorColor = ta.getColor(R.styleable.CustomizeEditText_errorColor, ContextCompat.getColor(getContext(), R.color.color_fd1d2d));
            floatingLabel = ta.getBoolean(R.styleable.CustomizeEditText_floatingLabel, true) && !TextUtils.isEmpty(getHint());
            isLogin = ta.getBoolean(R.styleable.CustomizeEditText_isLogin, false);
            isSpecialPaste = ta.getBoolean(R.styleable.CustomizeEditText_isSpecialPaste, true);
            deleteLeftSpace = ta.getBoolean(R.styleable.CustomizeEditText_deleteLeftSpace, false);
//            iconResId = ta.getResourceId(R.styleable.CustomizeEditText_withIcon, 0);
            leftText = ta.getString(R.styleable.CustomizeEditText_leftText);
            rightText = ta.getString(R.styleable.CustomizeEditText_rightText);
            if (!TextUtils.isEmpty(rightText)) {
                rightText = LocaleUtils.getCurrencyCode("");
            }

            rightTextColor = ta.getColor(R.styleable.CustomizeEditText_rightTextColor,
                    ContextCompat.getColor(getContext(), ThemeSourceUtils.getSourceID(getContext(),
                            R.attr.color_50white_3a3b44)));
            // 1. 获取资源ID
            // Right icon
            // 删除图标 资源ID
            int icRightResID = ta.getResourceId(R.styleable.CustomizeEditText_rightIcon, R.mipmap.icon_type_deletexhdpi);
            // 2. 根据资源ID获取图标资源（转化成Drawable对象）
            iconEnd = ContextCompat.getDrawable(context, icRightResID);
            rightIcX = (int) ta.getDimension(R.styleable.CustomizeEditText_rightX, 0);
            rightIcY = (int) ta.getDimension(R.styleable.CustomizeEditText_rightY, 0);
            rightIcWidth = (int) ta.getDimension(R.styleable.CustomizeEditText_rightWidth, dip2px(20));
            rightIcHeight = (int) ta.getDimension(R.styleable.CustomizeEditText_rightHeight, dip2px(20));
            rightIcShowMode = (int) ta.getInt(R.styleable.CustomizeEditText_rightIconShowMode, 2);
            iconEnd.setBounds(rightIcX, rightIcY, rightIcWidth, rightIcHeight);

            /**
             * 初始化光标（颜色 & 粗细）
             */
            // 原理：通过 反射机制 动态设置光标
            // 1. 获取资源ID
            try {
                // 2. 通过反射 获取光标属性
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
//                f.setAccessible(true);
                // 3. 传入资源ID
                f.set(this, ta.getResourceId(R.styleable.CustomizeEditText_cursor, R.drawable.edittext_cursor));

            } catch (Exception e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
            ta.recycle();
        }

        errorTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        errorTextPaint.setTextSize(DIMEN_12_DP);

        leftTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        leftTextPaint.setColor(getTextColors().getColorForState(new int[]{-android.R.attr.state_enabled}, 0));
        leftTextPaint.setTextSize(dip2px(18));
        if (!TextUtils.isEmpty(leftText)) {
            Rect rect = new Rect();
            leftTextPaint.getTextBounds(leftText.toString(), 0, LocaleUtils.isRTL(getContext()) ? 0 : leftText.length(), rect);
            icStart = new ColorDrawable(android.graphics.Color.TRANSPARENT);
            //当左边有前缀文字时   阿拉伯语隐藏右边icon
            if(rightIcShowMode != 0) {
                iconEnd = new ColorDrawable(android.graphics.Color.TRANSPARENT);
                iconEnd.setBounds(rightIcX, rightIcY, (int) leftTextPaint.measureText(leftText.toString()) + dip2px(8), rightIcHeight);
            }
            icStart.setBounds(0, 0, LocaleUtils.isRTL(getContext()) ? 0 : deleteLeftSpace ? (rect.left + rect.right) : (rect.left + rect.right + dip2px(8)), 10);
        }
        updateIcon(false);


        rightTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        rightTextPaint.setColor(rightTextColor);
        rightTextPaint.setTextSize(dip2px(14));
        if (!TextUtils.isEmpty(rightText)) {
            Rect rect = new Rect();
            rightTextPaint.getTextBounds(rightText.toString(), 0, rightText.length(), rect);
            iconEnd = new ColorDrawable(android.graphics.Color.TRANSPARENT);
            iconEnd.setBounds(0, 0, rect.left + rect.right + dip2px(8), 10);
        }

        labelTextSize = getTextSize();
        labelTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        if (floatingLabel) {
            basePaddingTop += DIMEN_8_DP + DIMEN_12_DP;
            labelX = 0;
            labelY = DIMEN_16_DP + DIMEN_12_DP * 3;
            lineHeight = basePaddingTop + dip2px(28);
        } else {
            lineHeight = dip2px(32);
        }
        updatePadding();

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    private void updatePadding() {
        super.setPadding((int) (basePaddingLeft + paddingLeft), (int) (basePaddingTop + paddingTop),
                (int) (basePaddingRight + paddingRight), (int) (basePaddingBottom + paddingBottom));
    }

    private void updatePadding(int left, int top, int right, int bottom) {
        super.setPadding((int) (basePaddingLeft + paddingLeft) + left, (int) (basePaddingTop + paddingTop) + top,
                (int) (basePaddingRight + paddingRight) + right, (int) (basePaddingBottom + paddingBottom) + bottom);
    }

    public void setBasePadding(int left, int top, int right, int bottom) {
        basePaddingLeft = left;
        basePaddingTop = top;
        basePaddingRight = right;
        basePaddingBottom = bottom;
        super.setPadding((int) paddingLeft + left, (int) paddingTop + top,
                (int) paddingRight + right, (int) paddingBottom + bottom);
    }

    public float getBasePaddingLeft() {
        return basePaddingLeft;
    }

    public float getBasePaddingTop() {
        return basePaddingTop;
    }

    public float getBasePaddingRight() {
        return basePaddingRight;
    }

    public float getBasePaddingBottom() {
        return basePaddingBottom;
    }

    public void setInputNumber(boolean inputNumber){
        this.inputNumber = inputNumber;
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        paddingLeft = left;
        paddingTop = top;
        paddingRight = right;
        paddingBottom = bottom;
        super.setPadding((int) basePaddingLeft + left, (int) basePaddingTop + top,
                (int) basePaddingRight + right, (int) basePaddingBottom + bottom);
    }


    @Override
    public boolean onTextContextMenuItem(int id) {
        boolean consumed = super.onTextContextMenuItem(id);
        // 监听
        switch (id) {
            case android.R.id.paste:
                if(isSpecialPaste) {
                    onTextPaste();
                }
                break;
        }
        return consumed;
    }

    /**
     * 粘贴操作
     */
    public void onTextPaste() {
        try {
            ClipboardManager cm = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData abc = cm.getPrimaryClip();
            if (abc.getItemCount() > 0) {
                String text = abc.getItemAt(0).coerceToText(getContext()).toString();
                if (inputNumber) {
                    String number = text.replaceAll("[^0-9]", "");
                    setText(number);
                    setSelection(number.length());
                }else {
                    setText(text);
                    setSelection(text.length());
                }
                    inputNumber = false;
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        labelTextColor = COLOR_HINT;
        underLineColor = COLOR_HINT;

        if (isShowError) {
            underLineColor = errorColor;
        }

        if (!TextUtils.isEmpty(leftText) && isLeftTextShow && leftTextPaint != null) {
            leftTextPaint.setTypeface(getTypeface());
            leftTextPaint.setTextAlign(LocaleUtils.isRTL(getContext()) ? Paint.Align.LEFT : Paint.Align.LEFT);
//            canvas.drawText(leftText, 0, leftText.length(), LocaleUtils.isRTL(getContext()) ?
//                    getScrollX() + getWidth() : getScrollX(), DIMEN_16_DP + DIMEN_12_DP * 3 + 3, leftTextPaint);
            canvas.drawText(leftText, 0, leftText.length(), LocaleUtils.isRTL(getContext()) ?
                    getScrollX() : getScrollX(), DIMEN_16_DP + DIMEN_12_DP * 3 + 3, leftTextPaint);
        }

        if (!TextUtils.isEmpty(rightText) && !TextUtils.isEmpty(getText())) {
            rightTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
            rightTextPaint.setTextAlign(LocaleUtils.isRTL(getContext()) ? Paint.Align.LEFT : Paint.Align.RIGHT);
            canvas.drawText(rightText, 0, rightText.length(), LocaleUtils.isRTL(getContext()) ?
                    getScrollX() : getScrollX() + getWidth(), DIMEN_16_DP + DIMEN_12_DP * 3 + 3, rightTextPaint);
        }

        // Draw the unselected line
        underLinePaint.setColor(underLineColor);
        underLinePaint.setStrokeWidth(1);
        underLine.reset();
        underLine.moveTo(getScrollX(), lineHeight);
        underLine.lineTo(getScrollX() + getWidth(), lineHeight);
        canvas.drawPath(underLine, underLinePaint);

        if (floatingLabel) {
            labelTextPaint.setColor(labelTextColor);
            labelTextPaint.setTextSize(labelTextSize);
            labelTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
            labelTextPaint.setTextAlign(LocaleUtils.isRTL(getContext()) ? Paint.Align.RIGHT : Paint.Align.LEFT);
            canvas.drawText(getHint(), 0, getHint().length(), LocaleUtils.isRTL(getContext()) ?
                    getScrollX() + getWidth() : getScrollX() + labelX, labelY, labelTextPaint);
        }

        if (!TextUtils.isEmpty(getError())) {
            errorTextPaint.setColor(errorColor);
            errorTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), LocaleUtils.isRTL(context)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
            errorTextPaint.setTextSize(dip2px(12));
            // 必须放到最后执行，不然绘制后面的图形会位移
            canvas.translate(getScrollX(), lineHeight + DIMEN_5_DP);
            StaticLayout layout = new StaticLayout(getError(), errorTextPaint, getWidth(),
                    Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
            layout.draw(canvas);
            canvas.translate(0, 0);
//            canvas.drawText(getError(), 0, getError().length(), getScrollX(),
//                    getHeight() - DIMEN_5_DP, errorTextPaint);
        }

    }

    @Override
    public void setError(CharSequence error) {
        errorText = error;
        isShowError = !TextUtils.isEmpty(getError());
        if (!TextUtils.isEmpty(getError())) {
            Rect rect = new Rect();
            errorTextPaint.getTextBounds(errorText.toString(), 0, errorText.length(), rect);
            int errorTextWight = rect.left + rect.right;
            int rows = 2;
//            while (errorTextWight > getWidth()) {
//                rows++;
//                errorTextWight -= getWidth();
//            }
            updatePadding(0, 0, 0, (int) (DIMEN_8_DP + DIMEN_12_DP * rows));
        } else {
            setError(null, null);
            updatePadding();
        }
    }

    @Override
    public CharSequence getError() {
        return errorText;
    }

    private float oldTouchX = -1;
    private float oldTouchY = -1;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isEnabled()) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    oldTouchX = event.getX();
                    oldTouchY = event.getY();
                    if (event.getY() <= getHeight() - (DIMEN_5_DP + DIMEN_12_DP)
                            && !TextUtils.isEmpty(errorText)) {
//                        setError("");
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (Math.abs(event.getX() - oldTouchX) < 2 && Math.abs(event.getY() - oldTouchY) < 2) {
                        Drawable drawable = iconEnd;
                        Drawable leftDrawable = icStart;
                        if (LocaleUtils.isRTL(getContext())) {
                            if (isRightIconShow && drawable != null && event.getX() >=  getPaddingLeft()
                                    && event.getX() <= (getPaddingLeft() + drawable.getBounds().width())
                                    && event.getY() > basePaddingTop && event.getY() < lineHeight) {
                                handleEndIconClick();
                            }
                        } else {
                            if (isRightIconShow && drawable != null && event.getX() <= (getWidth() - getPaddingRight())
                                    && event.getX() >= (getWidth() - getPaddingRight() - drawable.getBounds().width())
                                    && event.getY() > basePaddingTop && event.getY() < lineHeight) {
                                handleEndIconClick();
                            }

                            if (isLeftTextShow && event.getX() <= (icStart.getBounds().width())
                                    && event.getX() >= (0)
                                    && event.getY() > basePaddingTop && event.getY() < lineHeight) {
                                handleStartIconClick();
                            }
                        }
                    }
                    break;
            }
        } else {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    oldTouchX = event.getX();
                    oldTouchY = event.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    if (Math.abs(event.getX() - oldTouchX) < 2 && Math.abs(event.getY() - oldTouchY) < 2) {
                        if (onUnableClickListener != null) {
                            onUnableClickListener.onUnableViewClick();
                        }
                    }
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    public void handleEndIconClick() {
        if (!TextUtils.isEmpty(rightText)) {
            return;
        }
//        clearFocus();
        if (clickListener != null) {
            clickListener.onRightViewClick();
        } else {
            super.setText("");
        }
    }

    private void handleStartIconClick() {
        if (TextUtils.isEmpty(leftText)) {
            return;
        }
        clearFocus();
        if (onLeftViewClickListener != null) {
            onLeftViewClickListener.onLeftViewClick();
        } else {
            super.setText("");
        }
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        if (focused) {
            isShowError = false;
            if (floatingLabel && getText().length() == 0 && !isLeftTextShow) {
                labelAnimation = createLabelAnimation(getScrollX(), getBaseline(), getScrollX(),
                        DIMEN_16_DP + DIMEN_12_DP, getTextSize(), DIMEN_12_DP, 0, true);
                labelAnimation.start();
            }
            if (!TextUtils.isEmpty(errorText)) {
                setError("");
            }
        } else {
            if (floatingLabel && getText().length() == 0 && !isLeftTextShow) {
                if (labelAnimation != null) {
                    labelAnimation.cancel();
                    labelAnimation = createLabelAnimation(labelX, labelY, getScrollX(), getBaseline(),
                            labelTextSize, getTextSize(), labelAnimationElapsedDuration, false);
                    labelAnimation.start();
                } else {
                    labelAnimation = createLabelAnimation(getScrollX(), DIMEN_16_DP + DIMEN_12_DP,
                            getScrollX(), getBaseline(), DIMEN_12_DP, getTextSize(), 0, false);
                    labelAnimation.start();
                }
            }
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isActive(this)) {
                imm.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        }
        updateIcon(focused);
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (isLogin && text.length() > 5) {
            labelAnimation = createLabelAnimation(getScrollX(), getBaseline(), getScrollX(),
                    DIMEN_16_DP + DIMEN_12_DP, getTextSize(), DIMEN_12_DP, 0, true);
            //赋值的时候就不要动画效果了
            labelAnimation.setDuration(0);
            labelAnimation.start();
            isTextShow = true;
        }
        updateIcon(hasFocus());
    }

    private AnimatorSet createLabelAnimation(float startX, float startY, float targetX, float targetY,
                                             float startTextSize, float targetTextSize, long durationOffset, boolean isFloatingLabel) {
        ValueAnimator xAnim = ValueAnimator.ofFloat(startX, targetX);
        xAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                labelX = (Float) animation.getAnimatedValue();
            }
        });
        ValueAnimator yAnim = ValueAnimator.ofFloat(startY, targetY);
        yAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                labelY = (Float) animation.getAnimatedValue();
            }
        });
        ValueAnimator textSizeAnim = ValueAnimator.ofFloat(startTextSize, targetTextSize);
        textSizeAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                labelTextSize = (Float) animation.getAnimatedValue();
                labelAnimationElapsedDuration = animation.getCurrentPlayTime();
                invalidate();
            }
        });

        AnimatorSet set = new AnimatorSet();
        set.setDuration(Math.max(0, DURATION_SHORT - durationOffset)); // Ensure positive duration
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        set.playTogether(xAnim, yAnim, textSizeAnim);
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                labelAnimationElapsedDuration = 0;
                if (isFloatingLabel) {
                    isLeftTextShow = !TextUtils.isEmpty(leftText);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        return set;
    }

    private void updateIcon(boolean focus) {
        switch (rightIcShowMode) {
            case 0:
                isRightIconShow = true;
                break;
            case 1:
                isRightIconShow = length() > 0;
                break;
            case 2:
            default:
                isRightIconShow = !TextUtils.isEmpty(leftText) || (focus && length() > 0);
                break;
        }
        setCompoundDrawables((LocaleUtils.isRTL(getContext()) ? (isRightIconShow ? iconEnd : null) : icStart), null,
                LocaleUtils.isRTL(getContext()) ? icStart : (isRightIconShow ? iconEnd : null), null);
        invalidate();
    }

    public void setIconEnd(int src) {
        setIcRight(src, rightIcHeight);
    }

    public void setIcRight(int src, int rightIcHeight) {
        iconEnd = ContextCompat.getDrawable(getContext(), src);
        if (iconEnd != null) {
            iconEnd.setBounds(rightIcX, rightIcY, rightIcWidth, dip2px(rightIcHeight));
        }
    }

    public void setLeftText(String leftText) {
        if (TextUtils.isEmpty(leftText)) {
            return;
        }
        if (leftText.contains("784-")) {
            deleteLeftSpace = true;
        }
        if (!TextUtils.isEmpty(leftText)) {
            Rect rect = new Rect();
            leftTextPaint.getTextBounds(leftText.toString(), 0, LocaleUtils.isRTL(getContext()) ? 0 : leftText.length(), rect);
            icStart = new ColorDrawable(android.graphics.Color.TRANSPARENT);
            //当左边有前缀文字时   阿拉伯语隐藏右边icon
            if(rightIcShowMode != 0) {
                iconEnd = new ColorDrawable(android.graphics.Color.TRANSPARENT);
                iconEnd.setBounds(rightIcX, rightIcY, (int) leftTextPaint.measureText(leftText.toString()) + dip2px(8), rightIcHeight);
            }
            icStart.setBounds(0, 0, LocaleUtils.isRTL(getContext()) ? 0 : deleteLeftSpace ? (rect.left + rect.right) : (rect.left + rect.right + dip2px(8)), 10);
        }
        updateIcon(false);
        this.leftText = leftText;
        invalidate();
    }

    public String getRightText() {
        return rightText.toString();
    }

    public void setRightText(String rightText) {
        if (TextUtils.isEmpty(rightText)) {
            return;
        }
        if (!TextUtils.isEmpty(rightText)) {
            Rect rect = new Rect();
            rightTextPaint.getTextBounds(rightText.toString(), 0, rightText.length(), rect);
            iconEnd = new ColorDrawable(android.graphics.Color.TRANSPARENT);
            iconEnd.setBounds(0, 0, rect.left + rect.right + dip2px(8), 10);
        }
        updateIcon(false);
        this.rightText = rightText;
        invalidate();
    }

    public void setViewDisable() {
        setEnabled(false);
        this.setAlpha(0.4f);
    }

    public int dip2px(float dpValue) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public void setUnderLineRed() {
        this.isShowError = true;
    }

    public interface OnViewClickListener {
        void onRightViewClick();
    }

    public interface OnLeftViewClickListener {
        void onLeftViewClick();
    }

    public interface OnUnableClickListener {
        void onUnableViewClick();
    }
}