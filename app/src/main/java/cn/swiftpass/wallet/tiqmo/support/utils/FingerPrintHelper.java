package cn.swiftpass.wallet.tiqmo.support.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.biometrics.BiometricPrompt;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;

public class FingerPrintHelper {

    private static final String KEY_PROVIDER = "AndroidKeyStore";
    private static final String KEY_ALGORITHM = KeyProperties.KEY_ALGORITHM_AES;
    private static final String KEY_BLOCK_MODE = KeyProperties.BLOCK_MODE_CBC;
    private static final String KEY_ENCRYPTION_PADDING = KeyProperties.ENCRYPTION_PADDING_PKCS7;
    private static final String KEY_TRANSFORMATION = KEY_ALGORITHM + "/" + KEY_BLOCK_MODE + "/" + KEY_ENCRYPTION_PADDING;


    @Nullable
    public static FingerPrintHelper newInstance(Context context) {
        FingerprintManager fManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
        if (fManager == null || !fManager.isHardwareDetected()) {
            return null;
        }
        try {
            KeyStore keyStore = KeyStore.getInstance(KEY_PROVIDER);
            keyStore.load(null);
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, KEY_PROVIDER);
            Cipher.getInstance(KEY_TRANSFORMATION);
            return new FingerPrintHelper(context, keyStore, keyGenerator, fManager);
        } catch (KeyStoreException | NoSuchPaddingException | NoSuchAlgorithmException | NoSuchProviderException | CertificateException | IOException e) {
            LogUtil.d(e.toString(), e);
            return null;
        }
    }

    private Context mAppContext;
    private FingerprintManager mFManager;
    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;
    private final List<AuthenticationCallback> mAuthenticationCallbackList;

    private FingerPrintHelper(Context context, KeyStore keyStore, KeyGenerator keyGenerator, FingerprintManager fManager) {
        mAppContext = context.getApplicationContext();
        mKeyStore = keyStore;
        mKeyGenerator = keyGenerator;
        mFManager = fManager;
        mAuthenticationCallbackList = Collections.synchronizedList(new LinkedList<AuthenticationCallback>());
    }

    public boolean hasEnrolledFingerprints() {
        return mFManager.hasEnrolledFingerprints();
    }

    public Cipher getCipherIfFingerPrintNoChange(String secretKeyName) {
        try {
            SecretKey secretKey = (SecretKey) mKeyStore.getKey(secretKeyName, null);
            if (secretKey == null) {
                return null;
            }
            Cipher cipher = Cipher.getInstance(KEY_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            if (hasEnrolledFingerprints()) {
                return cipher;
            } else {
                return null;
            }
        } catch (KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException e) {
            return null;
        }
    }


    public Cipher generateNewSecretKey(String secretKeyName) {
        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec
                .Builder(secretKeyName, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KEY_BLOCK_MODE)
                .setUserAuthenticationRequired(true)
                .setEncryptionPaddings(KEY_ENCRYPTION_PADDING);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setInvalidatedByBiometricEnrollment(true);
        }
        try {
            mKeyGenerator.init(builder.build());
            SecretKey secretKey = mKeyGenerator.generateKey();
            Cipher cipher = Cipher.getInstance(KEY_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher;
        } catch (InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
            LogUtil.d(e.toString(), e);
            return null;
        }
    }

    public void addAuthenticationCallback(AuthenticationCallback callback) {
        synchronized (mAuthenticationCallbackList) {
            if (!mAuthenticationCallbackList.contains(callback)) {
                mAuthenticationCallbackList.add(callback);
            }
        }
    }

    public void removeAuthenticationCallback(AuthenticationCallback callback) {
        synchronized (mAuthenticationCallbackList) {
            mAuthenticationCallbackList.remove(callback);
        }
    }

    public void authenticate(@NonNull Cipher cipher, @NonNull CancellationSignal cancellationSignal) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            authenticateApi28(cipher, cancellationSignal);
//        } else {
//            authenticateApi23(cipher, cancellationSignal);
//        }

        authenticateApi23(cipher, cancellationSignal);
    }

    private void authenticateApi23(Cipher cipher, CancellationSignal cancellationSignal) {
        mFManager.authenticate(
                new FingerprintManager.CryptoObject(cipher),
                cancellationSignal,
                0,
                new AuthenticationCallbackApi23(mAuthenticationCallbackList),
                null);
    }

    @RequiresApi(28)
//    private void authenticateApi28(Cipher cipher, final CancellationSignal cancellationSignal) {
//        BiometricPrompt prompt = new BiometricPrompt.Builder(mAppContext)
//                .setTitle(mAppContext.getString(R.string.Title_TouchID))
//                .setSubtitle(mAppContext.getString(R.string.Hint_TouchIDConfirmFingerprintToContinue))
//                .setNegativeButton(mAppContext.getString(R.string.common_6), mAppContext.getMainExecutor(), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        cancellationSignal.cancel();
//                    }
//                })
//                .build();
//        prompt.authenticate(
//                new BiometricPrompt.CryptoObject(cipher),
//                cancellationSignal,
//                mAppContext.getMainExecutor(),
//                new AuthenticationCallbackApi28(mAuthenticationCallbackList));
//    }

    public abstract static class AuthenticationCallback {
        public void onAuthenticationError(int errorCode, CharSequence errString) {
        }

        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        }

        public void onAuthenticationSucceeded(Cipher cipher) {

        }

        public void onAuthenticationFailed() {
        }
    }

    private static class AuthenticationCallbackApi23 extends FingerprintManager.AuthenticationCallback {
        private List<AuthenticationCallback> mCallbackList;

        AuthenticationCallbackApi23(List<AuthenticationCallback> callbackList) {
            mCallbackList = callbackList;
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationError(errorCode, errString);
            }
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationHelp(helpCode, helpString);
            }
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            FingerprintManager.CryptoObject cryptoObject = result.getCryptoObject();
            Cipher cipher = cryptoObject == null ? null : cryptoObject.getCipher();
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationSucceeded(cipher);
            }
        }

        @Override
        public void onAuthenticationFailed() {
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationFailed();
            }
        }
    }

    @TargetApi(28)
    private static class AuthenticationCallbackApi28 extends BiometricPrompt.AuthenticationCallback {
        private List<AuthenticationCallback> mCallbackList;

        AuthenticationCallbackApi28(List<AuthenticationCallback> callbackList) {
            mCallbackList = callbackList;
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationError(errorCode, errString);
            }
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationHelp(helpCode, helpString);
            }
        }

        @Override
        public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
            BiometricPrompt.CryptoObject cryptoObject = result.getCryptoObject();
            Cipher cipher = cryptoObject == null ? null : cryptoObject.getCipher();
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationSucceeded(cipher);
            }
        }

        @Override
        public void onAuthenticationFailed() {
            for (AuthenticationCallback callback : mCallbackList) {
                callback.onAuthenticationFailed();
            }
        }
    }
}
