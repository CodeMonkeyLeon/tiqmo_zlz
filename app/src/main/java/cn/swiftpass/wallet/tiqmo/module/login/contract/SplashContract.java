package cn.swiftpass.wallet.tiqmo.module.login.contract;


import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;

public class SplashContract {

    public interface View extends BaseView<Presenter> {
        void getConfigSuccess(ConfigEntity configEntity);
    }


    public interface Presenter extends BasePresenter<View> {

        void getConfig();
    }
}
