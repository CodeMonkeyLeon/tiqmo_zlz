package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.TransferRelationAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.GetTimePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class BeneficiaryMoneyActivity extends BaseCompatActivity<TransferContract.GetTimePresenter> implements TransferContract.GetTimeView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.ll_money)
    ConstraintLayout llMoney;
    @BindView(R.id.et_amount)
    EditTextWithDel etAmount;
    @BindView(R.id.tv_money_currency)
    TextView tvMoneyCurrency;
    @BindView(R.id.tv_now_one)
    TextView tvNowOne;
    @BindView(R.id.tv_now)
    TextView tvNow;
    @BindView(R.id.iv_now_checked)
    ImageView ivNowChecked;
    @BindView(R.id.con_now)
    ConstraintLayout conNow;
    @BindView(R.id.tv_later)
    TextView tvLater;
    @BindView(R.id.tv_later_one)
    TextView tvLaterOne;
    @BindView(R.id.iv_later_checked)
    ImageView ivLaterChecked;
    @BindView(R.id.con_later)
    ConstraintLayout conLater;
    @BindView(R.id.rl_relation)
    RecyclerView rlRelation;
    @BindView(R.id.tv_transfer)
    TextView tvTransfer;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.et_purpose)
    EditTextWithDel etPurpose;
    @BindView(R.id.con_purpose)
    ConstraintLayout conPurpose;
    @BindView(R.id.iv_date_to_delete)
    ImageView dateToDelete;

    private double valiableMoney, money;

    private TransferRelationAdapter transferRelationAdapter;
    private LinearLayoutManager mLayoutManager;
    public List<KycContactEntity> contactList = new ArrayList<>();

    private BeneficiaryEntity beneficiaryEntity;
    private String transTimeType;
    private double monthLimitMoney;
    private String transferPurpose;
    private String transferPurposeDesc;
    private String transTimeDescOne, transTimeDescTwo;

    private TimeEntity timeEntity;
    private TransferLimitEntity transferLimitEntity;

    private boolean isFromHome;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_bank_detail;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUser();

        if (etAmount != null) {
            String amount = etAmount.getEditText().getText().toString().trim();
            if (!TextUtils.isEmpty(amount)) {
                amount = amount.replace(",", "");
                checkMoney(amount);
            }
        }
    }

    private void checkUser() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            String balance = userInfoEntity.getBalance();
            String balanceNumber = userInfoEntity.getBalanceNumber();
            if (!TextUtils.isEmpty(balanceNumber)) {
                try {
                    balanceNumber = balanceNumber.replace(",", "");
                    valiableMoney = Double.parseDouble(balanceNumber);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");

                }
            }
            tvAmount.setText(balance);
            tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
        }
    }

    private void checkContinueStatus() {
        String amount = etAmount.getEditText().getText().toString().trim();
        amount = amount.replace(",", "");
        if (!TextUtils.isEmpty(amount)) {
            money = Double.parseDouble(amount);
            if (money == 0 || money > valiableMoney || money > monthLimitMoney || TextUtils.isEmpty(transTimeType) || TextUtils.isEmpty(transferPurpose)) {
                tvTransfer.setEnabled(false);
            } else {
                tvTransfer.setEnabled(true);
            }
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        etAmount.getTlEdit().setHint(getString(R.string.wtw_22));
        etPurpose.getTlEdit().setHint(getString(R.string.IMR_new_10));
        if(getIntent() != null && getIntent().getExtras() != null){
            isFromHome = getIntent().getExtras().getBoolean(Constants.isFromHome);
        }
        if(isFromHome){
            transferPurpose = "1";
            conPurpose.setVisibility(View.GONE);
            tvTitle.setText(R.string.wtba_56);
        }else {
            tvTitle.setText(R.string.DigitalCard_106);
        }
        conNow.setEnabled(false);
        conLater.setEnabled(false);
        etPurpose.setFocusableFalse();
        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        if (getIntent() != null && getIntent().getExtras() != null) {
            beneficiaryEntity = (BeneficiaryEntity) getIntent().getExtras().getSerializable(Constants.BeneficiaryEntity);
            timeEntity = (TimeEntity) getIntent().getExtras().getSerializable(Constants.TimeEntity);
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            if (beneficiaryEntity != null) {

            }
            if (timeEntity != null) {
                Boolean nowAvailable = timeEntity.nowAvailable;
                if (nowAvailable) {
                    tvNow.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
                    tvNowOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
                    tvLater.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
                    tvLaterOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
                    tvLater.setAlpha(0.3f);
                    tvLaterOne.setAlpha(0.3f);
                    ivNowChecked.setVisibility(View.VISIBLE);
                    ivLaterChecked.setVisibility(View.GONE);
                    transTimeDescOne = tvNowOne.getText().toString();
                    transTimeDescTwo = tvNow.getText().toString();
                    transTimeType = "1";
                } else {
                    tvNow.setAlpha(0.3f);
                    tvNowOne.setAlpha(0.3f);
                    tvLater.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
                    tvLaterOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
                    tvNow.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
                    tvNowOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
                    ivLaterChecked.setVisibility(View.VISIBLE);
                    ivNowChecked.setVisibility(View.GONE);
                    transTimeDescOne = tvLaterOne.getText().toString();
                    transTimeDescTwo = tvLater.getText().toString();
                    transTimeType = "2";
                }
            }
            if (transferLimitEntity != null) {
                String monthLimitAmt = transferLimitEntity.monthLimitAmt;
                try {
                    monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        }

        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rlRelation.setLayoutManager(mLayoutManager);
        contactList.add(new KycContactEntity(getString(R.string.wtba_33), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_purpose_personal)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_34), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_purpose_payment)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_34_1), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_purpose_saving)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_35), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_purpose_donation)));
        contactList.add(new KycContactEntity(getString(R.string.wtba_36), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_purpose_other)));
        transferRelationAdapter = new TransferRelationAdapter(contactList);
        transferRelationAdapter.bindToRecyclerView(rlRelation);

        transferRelationAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.con_choose:
                        transferRelationAdapter.setPosition(position);
                        transferPurpose = String.valueOf(position + 1);
                        transferPurposeDesc = contactList.get(position).getContactsName();
                        checkContinueStatus();
                        break;
                    default:
                        break;
                }
            }
        });

        etPurpose.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                AppClient.getInstance().getTransferPurpose("PURPOSE_OF_TRANSFER",new ResultCallback<BusinessParamEntity>(){

                    @Override
                    public void onResult(BusinessParamEntity businessParamEntity) {
                        showProgress(false);

                        if (businessParamEntity != null) {
                            List<SelectInfoEntity> selectInfoEntityList = businessParamEntity.businessParamList;
                            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_new_10), false, true);
                            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    SelectInfoEntity  selectInfoEntity = selectInfoEntityList.get(position);
                                    if (selectInfoEntity != null) {
                                        transferPurpose = selectInfoEntity.businessParamKey;
                                        transferPurposeDesc = selectInfoEntity.businessParamValue;
                                        etPurpose.getEditText().setText(transferPurposeDesc);
                                    }
                                    checkContinueStatus();
                                }
                            });
                            dialog.showNow(getSupportFragmentManager(), "getNationalitySuccess");
                        }
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                    }
                });
            }
        });

        //两位小数过滤
        etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                if (TextUtils.isEmpty(amount)) {
                    tvMoneyCurrency.setVisibility(View.GONE);
                    dateToDelete.setVisibility(View.GONE);
                    tvTransfer.setEnabled(false);
                } else {
                    try {
                        money = Double.parseDouble(amount);
                    } catch (Exception e) {
                        tvTransfer.setEnabled(false);
                    }
                    tvMoneyCurrency.setVisibility(View.VISIBLE);
                    dateToDelete.setVisibility(View.VISIBLE);
                    checkContinueStatus();
                }
            }
        });

        etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                String amount = etAmount.getEditText().getText().toString().trim();
                amount = amount.replace(",", "");
                String fomatAmount = AmountUtil.dataFormat(amount);
                if (!hasFocus) {
                    if (TextUtils.isEmpty(amount)) {
                        tvAddMoney.setEnabled(false);
                    } else {
                        etAmount.getEditText().setText(fomatAmount);
                        checkMoney(amount);
                    }
                }
            }
        });

//        mPresenter.getTime();
//        mPresenter.getLimit(Constants.LIMIT_TRANSFER);
    }

    private void checkMoney(String amount) {
        money = Double.parseDouble(amount);
        if (money > monthLimitMoney) {
            tvAddMoney.setEnabled(false);
            etAmount.showErrorViewWithMsg(getString(R.string.wtw_25_3));
        } else if (money <= valiableMoney) {
            tvAddMoney.setEnabled(false);
            etAmount.showErrorViewWithMsg("");
        } else {
            tvAddMoney.setEnabled(true);
            etAmount.showErrorViewWithMsg(getString(R.string.wtw_25));
        }

        if (valiableMoney == 0) {
            tvAddMoney.setEnabled(true);
        }

        checkContinueStatus();
    }

    @OnClick({R.id.iv_back, R.id.con_now, R.id.con_later, R.id.tv_transfer, R.id.tv_add_money,R.id.iv_date_to_delete})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
//            case R.id.con_now:
//                tvNow.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
//                tvNowOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
//                tvLater.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
//                tvLaterOne.setTextColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15));
//                ivNowChecked.setVisibility(View.VISIBLE);
//                ivLaterChecked.setVisibility(View.GONE);
//                transTimeDescOne = tvNowOne.getText().toString();
//                transTimeDescTwo = tvNow.getText().toString();
//                transTimeType = "1";
//                checkContinueStatus();
//                break;
//            case R.id.con_later:
//                tvLater.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
//                tvLaterOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_gender_selected)));
//                tvNow.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
//                tvNowOne.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_090a15)));
//                ivLaterChecked.setVisibility(View.VISIBLE);
//                ivNowChecked.setVisibility(View.GONE);
//                transTimeDescOne = tvLaterOne.getText().toString();
//                transTimeDescTwo = tvLater.getText().toString();
//                transTimeType = "2";
//                checkContinueStatus();
//                break;
            case R.id.tv_transfer:
                String orderAmount = etAmount.getEditText().getText().toString().trim();
                orderAmount = orderAmount.replace(",", "");
                if (beneficiaryEntity != null) {
                    HashMap<String, Object> mHashMap = new HashMap<>(16);
                    if(!isFromHome) {
                        beneficiaryEntity.transferPurpose = transferPurpose;
                        beneficiaryEntity.transferPurposeDesc = transferPurposeDesc;
                    }
                    beneficiaryEntity.transTimeDescOne = transTimeDescOne;
                    beneficiaryEntity.transTimeDescTwo = transTimeDescTwo;
                    beneficiaryEntity.orderAmount = orderAmount;
                    beneficiaryEntity.transTimeType = transTimeType;
                    mHashMap.put("beneficiaryEntity", beneficiaryEntity);
                    mHashMap.put(Constants.isFromHome, isFromHome);

                    AppsFlyerEntity appsFlyerEntity = new AppsFlyerEntity();
                    appsFlyerEntity.purpose = beneficiaryEntity.transferPurposeDesc;
                    UserInfoManager.getInstance().setAppsFlyerEntity(appsFlyerEntity);
                    ActivitySkipUtil.startAnotherActivity(BeneficiaryMoneyActivity.this, TransferSummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.iv_date_to_delete:
                etAmount.getEditText().setText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void getTimeFailed(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getTimeSuccess(TimeEntity timeEntity) {
        Boolean nowAvailable = timeEntity.nowAvailable;
        if (nowAvailable) {

        } else {
            conNow.setEnabled(false);
            tvNow.setEnabled(false);
            ivNowChecked.setVisibility(View.GONE);
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        String monthLimitAmt = transferLimitEntity.monthLimitAmt;
        try {
            monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public TransferContract.GetTimePresenter getPresenter() {
        return new GetTimePresenter();
    }
}
