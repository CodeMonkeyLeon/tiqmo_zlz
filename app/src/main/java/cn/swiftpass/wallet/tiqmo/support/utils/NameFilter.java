package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

/**
 * imr beneficiary name filter
 * 最少两个单词，最多4个单词，每个词最长16
 */
public class NameFilter implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String orgStr = dest.toString();
        String newStr = source.toString();
        String resultStr = orgStr+newStr;
        String result = newStr;
        if(!TextUtils.isEmpty(orgStr) && orgStr.split(" ").length>4 || resultStr.split(" ").length>4){
            return "";
        }else if (newStr.equals("")) {
            try {
                if (start != end) {
                    if (dstart > 0 && " ".equals(String.valueOf(orgStr.charAt(dstart))) && !isDeleteAble(orgStr, dstart)) {
                        result = " ";
                    }
                } else if (dend - dstart != 1) {
                    result = orgStr.substring(dstart, Math.max(dstart, dend - 1));
                }
            } catch (IndexOutOfBoundsException e) {
                result = "";
            }
        } else if (newStr.equals(" ")) {
            if ((dstart > 0 && " ".equals(String.valueOf(orgStr.charAt(dstart - 1)))) ||
                    (dstart < orgStr.length() && " ".equals(String.valueOf(orgStr.charAt(dstart)))) ||
                    StringUtils.countStr(orgStr, " ") >= 3) {

                result = "";
            }
        }
//        else if (newStr.length() > 1 && dstart == dend && dstart != 0) {
//            result = "";
//        }
        else {
            StringBuilder spannedString = new StringBuilder().append(orgStr.substring(0, dstart))
                    .append(newStr).append(orgStr.substring(dend));
            if (isToLong(spannedString.toString(), dstart + end - 1) != -1) {
                result = newStr.length() <= 1 ?  "" : newStr.substring(start,
                        Math.min(isToLong(spannedString.toString(), dstart + end - 1), newStr.length()));
            }
        }
        return result;
    }

    public boolean isDeleteAble(String orgStr, int index) {
        int startInd = 0;
        int length = 0;
        String[] temps = orgStr.split(" ");
        for (int i = 0; i < temps.length; i++) {
            startInd = startInd + temps[i].length();
            if (startInd == index && i < (temps.length - 1)) {
                length = temps[i].length() + temps[i + 1].length();
            }
            startInd = startInd + 1;
        }
        return length <= 16;
    }

    public int isToLong(String orgStr, int index) {
        int startInd = 0;
        int lastIndex = -1;
        String[] temps = orgStr.split(" ");
        for (String temp : temps) {
            if (temp.length() >= 16 && (index >= startInd && index <= startInd + temp.length())) {
                lastIndex = 16 - (temp.length() - (index - startInd));
                break;
            }
            startInd = startInd + temp.length() + 1;
        }
        return lastIndex;
    }
}
