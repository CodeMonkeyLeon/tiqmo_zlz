package cn.swiftpass.wallet.tiqmo.base.view;


public interface Loading {
    void showProgress(boolean iShow);

    void showProgress(boolean iShow, boolean isDim);

    /**
     * 代理，简化判空
     */
    class LoadingWrapper implements Loading {

        private Loading mLoading;

        public LoadingWrapper() {
        }

        public LoadingWrapper(Loading loading) {
            mLoading = loading;
        }

        @Override
        public void showProgress(boolean iShow) {
            if (mLoading != null) {
                mLoading.showProgress(iShow);
            }
        }

        @Override
        public void showProgress(boolean iShow, boolean isDim) {
            if (mLoading != null) {
                mLoading.showProgress(iShow, isDim);
            }
        }
    }
}
