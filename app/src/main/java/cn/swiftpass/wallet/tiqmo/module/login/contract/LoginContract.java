package cn.swiftpass.wallet.tiqmo.module.login.contract;


import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class LoginContract {

    public interface View extends BaseView<Presenter> {

        void loginAccountSuccess(UserInfoEntity response);

        void loginAccountError(String errorCode, String errorMsg);

        void checkPhoneSuccess(Void result);
        void checkPhoneError(String errorCode, String errorMsg);

        void activateUserStatusSuccess(Void result);
        void renewIdSuccess(ResultEntity result);
        void activateUserStatusFail(String errorCode, String errorMsg);
        void renewIdFail(String errorCode, String errorMsg);

        void trustDeviceFail(String errorCode, String errorMsg);

        void trustDeviceSuccess(Void result);
    }


    public interface Presenter extends BasePresenter<View> {
        void loginAccount(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude);

        void checkPhone(String callingCode, String phone,String type);

        void activateUserStatus(String callingCode, String phone,String lockAcc);

        void trustDevice();
    }
}
