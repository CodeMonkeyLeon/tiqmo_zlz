package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class UserInfoEntity implements Serializable {

    public static final String SEX_MAN = "0";
    public static final String SEX_WOMAN = "1";
    public static final String SEX_OTHER = "2";

    public String message;
    public String callingCode;
    public String nickName;
    public String fullName;
    public String birthday;
    public String gender;
    public String scannedCodeDisplay;
    public String sessionId;
    public String userName;
    public String userId;
    public String balance;
    public String payPasswordSet;
    //Y是kyc用户 H是高风险
    public String authentication;
    public String loginPasswordSet;
    public String phone;
    public String email;
    public String type;
    public String codeLength;
    public String codeType;
    public String accountName;
    public String ibanNum;
    public String avatar;
    public String cities;
    public String citiesId;
    public String statesName;
    public String statesId;
    public String states;
    public String address;
    /**
     * 请求中心未处理条数
     */
    public int requestCounts;
    public int expiredTime;
    public String balanceSum;
    public String sourceOfFundCode;
    public String sourceOfFundDesc;
    public String employmentCode;
    public String employmentDesc;
    public String professionCode;
    public String professionDesc;
    public String businessTypeCode;
    public String businessTypeDesc;
    public String companyName;
    public String salaryRangeCode;
    public String salaryRangeDesc;
    //IMR现在不需要设置
    public String imrSetUpFlag = "Y";
    public String countryCode;
    public String countryName;
    public String idNumber;
    //证件过期日期 （dd-MM-yyyy）
    public String expiryDate;
    //证件是否过期 1：过期，0：未过期
    public String isExpiryDate;
    //邮箱验证标识 Y:已验证 N:未验证
    public String emailVerifyFlag;
    //OTP
    public RiskControlEntity riskControlInfo;

    //用户ID过期标志 Y:已过期  N:未过期 I即将过期 YG已过期，但在宽限期内
    public String userIdCardExpiredFlag;
    //用户ID过期信息
    public String userIdCardExpiredMessage;
    //SILVER，适用于邀请 （0-10） 人的用户，GOLD，邀请（11-99）人的用户，PLATINUM，适用于邀请 （100++） 用户
    public String badges;

    public String orderNo;

    public List<String> userDisableAppFuncCodeList = new ArrayList<>();
    /**
     * 用户当前状态 激活状态 Active A(正常状态)/非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
     */
    public String lockAcc = "A";
    /**
     * 是否需要重新激活 （Y-是，N-否）
     */
    public String lockReActive;
    //用户注册时间
    public String userCreateDateTime = "";

    /**
     * N-Normal FF-Frozen Fraud, BF-Blocked Fraud
     */
    public String fraudStatus;

    public boolean isFreezeAccount(){
        return "FF".equals(fraudStatus);
    }

    public boolean isBlockAccount(){
        return "BF".equals(fraudStatus);
//        return !"BF".equals(fraudStatus);
    }
    public String getFirstName(){
        if (!TextUtils.isEmpty(userName)) {
            if (userName.contains(" ")) {
                String[] fastName = userName.split(" ");
                return fastName[0];
            } else {
                return userName;
            }
        }
        return "";
    }

    //用户ID是否过期 Y：已过期 N：未过期 I：即将过期
    public boolean isUserIdCardExpried() {
        return "Y".equals(userIdCardExpiredFlag);
    }

    //用户是否邮箱验证标识 Y:已验证 N:未验证
    public boolean isEmailVerify() {
        return "Y".equals(emailVerifyFlag);
    }

//    public boolean isSetImr() {
//        return "Y".equals(imrSetUpFlag);
//    }

    public String getStatesName() {
        if (!TextUtils.isEmpty(statesName)) {
            return statesName;
        }
        return states;
    }

    /**
     * 证件是否过期
     *
     * @return
     */
    public boolean isExpiryId(String isExpiryDate) {
        return "1".equals(isExpiryDate);
    }

    //是否超过了充值限额 “用户现余额+用户输入的充值金额＞100,000 SAR
    public boolean isOverLimitBalance(UserInfoEntity userInfoEntity, String amount, double maxLimitBalance) {
        try {

            double balanceAll = userInfoEntity.getAvalibleBalanceMoney();
            double money = Double.parseDouble(amount);
            if (balanceAll + money > maxLimitBalance) {
                return true;
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return false;
    }

    //是否超过了充值限额 “用户现余额+用户输入的充值金额＞100,000 SAR
    public boolean isOverLimitBalance(UserInfoEntity userInfoEntity, String amount, String maxLimitBalance) {
        try {

            String balanceAll = userInfoEntity.getAvalibleBalance();
            String money = BigDecimalFormatUtils.add(balanceAll,amount,2);
            if (BigDecimalFormatUtils.compareBig(money,maxLimitBalance)) {
                return true;
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return false;
    }

    public String getBalance() {
        try {
            if (!TextUtils.isEmpty(balance)) {
                balance = balance.replace(",", "");
                String resultMoney = BigDecimalFormatUtils.div(balance,UserInfoManager.getInstance().getConvertUnit(),2);
                return AmountUtil.dataFormat(resultMoney);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return null;
    }

    /**
     * 根据总余额来判断是否允许停用账户 总余额>0不能停用
     *
     * @return
     */
    public double getBalanceSumMoney() {
        try {
            if (!TextUtils.isEmpty(balanceSum)) {
                return AndroidUtils.getTransferMoneyNumber(balanceSum);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public double getAvalibleBalanceMoney() {
        try {
            if (!TextUtils.isEmpty(SpUtils.getInstance().getAvailableBalance())) {
                return AndroidUtils.getTransferMoneyNumber(SpUtils.getInstance().getAvailableBalance());
            } else {
                return AndroidUtils.getTransferMoneyNumber(balance);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public String getAvalibleBalance() {
        try {
            if (!TextUtils.isEmpty(SpUtils.getInstance().getAvailableBalance())) {
                return AndroidUtils.getTransferStringMoney(SpUtils.getInstance().getAvailableBalance());
            } else {
                return AndroidUtils.getTransferStringMoney(balance);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "0";
    }

    public double getBalanceMoney() {
        try {
            if (!TextUtils.isEmpty(balance)) {
                return AndroidUtils.getTransferMoneyNumber(balance);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public String getBalanceNumber() {
        try {
            if (!TextUtils.isEmpty(balance)) {
                balance = balance.replace(",", "");
                String resultMoney = BigDecimalFormatUtils.div(balance,UserInfoManager.getInstance().getConvertUnit(),2);
                return resultMoney;
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return null;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public boolean isKyc() {
        return "Y".equals(authentication);
    }

    //kyc高风险
    public boolean isKycHigh() {
        return "H".equals(authentication);
    }

    //kyc处理中
    public boolean isKycChecking() {
        return "P".equals(authentication);
    }

    //kyc低风险
    public boolean isKycLow() {
        return TextUtils.isEmpty(authentication) || "N".equals(authentication);
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getScannedCodeDisplay() {
        return scannedCodeDisplay;
    }

    public void setScannedCodeDisplay(String scannedCodeDisplay) {
        this.scannedCodeDisplay = scannedCodeDisplay;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getPayPasswordSet() {
        return payPasswordSet;
    }

    public boolean isSetPayPass() {
        return AppConfig.TRUE.equalsIgnoreCase(payPasswordSet);
    }

    public boolean isSetLoginPass() {
        return AppConfig.TRUE.equalsIgnoreCase(loginPasswordSet);
    }

    public boolean isAuthentication() {
        return AppConfig.TRUE.equalsIgnoreCase(authentication);
    }

    public void setPayPasswordSet(String payPasswordSet) {
        this.payPasswordSet = payPasswordSet;
    }

    public String getLoginPasswordSet() {
        return loginPasswordSet;
    }

    public void setLoginPasswordSet(String loginPasswordSet) {
        this.loginPasswordSet = loginPasswordSet;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(String codeLength) {
        this.codeLength = codeLength;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getIbanNum() {
        return ibanNum;
    }

    public void setIbanNum(String ibanNum) {
        this.ibanNum = ibanNum;
    }
}
