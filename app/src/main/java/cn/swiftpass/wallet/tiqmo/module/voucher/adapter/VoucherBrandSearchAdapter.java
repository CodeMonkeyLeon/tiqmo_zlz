package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class VoucherBrandSearchAdapter extends BaseRecyclerAdapter<VoucherBrandEntity> {

    public VoucherBrandSearchAdapter(@Nullable List<VoucherBrandEntity> data) {
        super(R.layout.item_search_brand, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VoucherBrandEntity voucherBrandEntity, int position) {
        int count = voucherBrandEntity.voucherCount;
        String brandName = voucherBrandEntity.voucherBrandName;
        String brandLogo = voucherBrandEntity.voucherBrandLogo;
        ImageView ivBrand = baseViewHolder.getView(R.id.iv_brand);
        baseViewHolder.setText(R.id.tv_deals_count, mContext.getString(R.string.newhome_31).replace("XXX", count + ""));
        baseViewHolder.setText(R.id.tv_brand_name, brandName);
        if (!TextUtils.isEmpty(brandLogo)) {
            Glide.with(mContext).clear(ivBrand);
            Glide.with(mContext)
                    .load(brandLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivBrand);
        }
    }
}
