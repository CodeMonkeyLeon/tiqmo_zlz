package cn.swiftpass.wallet.tiqmo.sdk.fcm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseHintCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ExtendEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.MainActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.RequestCenterActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AddCardFailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AddCardSuccessActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityLifeManager;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.FileUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Firebase";
    private final String CHANNEL_ID = ProjectApp.getContext().getPackageName();
    private final String CHANNEL_NAME = ProjectApp.getContext().getPackageName() + "_Notification";
    private static int NOTIFICATION_ID = 100;
    private final int MSG_CARD_BIND = -1;
    private final int MSG_ADD_MONEY = -2;
    //请求中心推送 请求转账  拆分账单
    private final int MSG_REQUEST_CENTER = -3;
    //消息中心的推送
    private final int MSG_NOTIFY = -4;
    private final int MSG_CHANGE_LOGIN_DEVICE = -5;
    private final int MSG_CQR_PAYMENT = -6;

    private String subject;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        LogUtils.i(TAG, "onMessageReceived From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            LogUtils.i(TAG, "onMessageReceived data payload: " + remoteMessage.getData());
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            LogUtils.i(TAG, "onMessageReceived Notification Body: " + remoteMessage.getNotification().getBody());
        }
        if (remoteMessage.getData() != null) {
            //消息类型字段subject BindCard绑卡  ADD_MONEY充值
            try {
                subject = remoteMessage.getData().get("subject");
                String userId = remoteMessage.getData().get("userId");
                if (userId != null && !userId.equals(AppClient.getInstance().getUserID())) {
                    return;
                }
                int messageCount = SpUtils.getInstance().getReqCounts();
                messageCount++;
                SpUtils.getInstance().setReqCounts(messageCount);
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_GET_NEW_NOTIFICATION));

                int msgType = 0;
                if (Constants.SUBJECT_BIND_CARD.equals(subject)) {
                    String cardNo = remoteMessage.getData().get("cardNo");
                    String cardFaceMini = remoteMessage.getData().get("cardFaceMini");
                    String custName = remoteMessage.getData().get("custName");
                    String expire = remoteMessage.getData().get("expire");
                    String title = remoteMessage.getData().get("title");
                    String respMsg = remoteMessage.getData().get("respMsg");
                    String respCode = remoteMessage.getData().get("respCode");
                    String bankName = remoteMessage.getData().get("bankName");
                    CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
                    cardNotifyEntity.cardNo = cardNo;
                    cardNotifyEntity.title = title;
                    cardNotifyEntity.respCode = respCode;
                    cardNotifyEntity.respMsg = respMsg;
                    cardNotifyEntity.bankName = bankName;
                    cardNotifyEntity.cardFaceMini = cardFaceMini;
//                    if (!TextUtils.isEmpty(cardNo)) {
                    msgType = MSG_CARD_BIND;
//                    }
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                } else if (Constants.SUBJECT_ADD_MONEY.equals(subject)) {
                    msgType = MSG_ADD_MONEY;
                    CardNotifyEntity cardNotifyEntity = getCardNotifyEntity(remoteMessage);
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                } else if (Constants.SUBJECT_NOTIFY.equals(subject)) {
                    msgType = MSG_REQUEST_CENTER;
                    CardNotifyEntity cardNotifyEntity = getCardNotifyEntity(remoteMessage);
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                } else if (Constants.SUBJECT_CHANGE_LOGIN_DEVICE.equals(subject)) {
                    msgType = MSG_CHANGE_LOGIN_DEVICE;
                    CardNotifyEntity cardNotifyEntity = getCardNotifyEntity(remoteMessage);
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                } else if (Constants.SUBJECT_CQR_PAYMENT.equals(subject) || Constants.SUBJECT_MQR_PAYMENT.equals(subject) ||
                        Constants.SUBJECT_H5_PAYMENT.equals(subject) ||
                        Constants.SUBJECT_APP_PAYMENT.equals(subject)) {
                    msgType = MSG_CQR_PAYMENT;
                    CardNotifyEntity cardNotifyEntity = getCardNotifyEntity(remoteMessage);
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                } else if (Constants.SUBJECT_SPLIT_BILL.equals(subject)) {
                    msgType = MSG_REQUEST_CENTER;
                    CardNotifyEntity cardNotifyEntity = getCardNotifyEntity(remoteMessage);
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                } else {
                    msgType = MSG_NOTIFY;
                    CardNotifyEntity cardNotifyEntity = getCardNotifyEntity(remoteMessage);
                    sendNotification(cardNotifyEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                }
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        }
    }

    private CardNotifyEntity getCardNotifyEntity(RemoteMessage remoteMessage) {
        String paymentMethodDesc = remoteMessage.getData().get("payMethodDesc");
        String orderNo = remoteMessage.getData().get("orderNo");
        String respCode = remoteMessage.getData().get("respCode");
        String startTime = remoteMessage.getData().get("startTimeStr");
        String title = remoteMessage.getData().get("title");
        String orderDesc = remoteMessage.getData().get("orderDesc");
        String respMsg = remoteMessage.getData().get("respMsg");
        String orderStatus = remoteMessage.getData().get("orderStatus");
        String orderAmount = remoteMessage.getData().get("orderAmount");
        String merchantId = remoteMessage.getData().get("merchantId");
        String merchantName = remoteMessage.getData().get("merchantName");
        String outTradeNo = remoteMessage.getData().get("outTradeNo");
        String payerAmount = remoteMessage.getData().get("payerAmount");
        String orderDirection = remoteMessage.getData().get("orderDirection");
        String orderCurrencyCode = remoteMessage.getData().get("payerCurrency");
        if(TextUtils.isEmpty(orderCurrencyCode)){
            orderCurrencyCode = remoteMessage.getData().get("orderCurrencyCode");
        }
        if(TextUtils.isEmpty(paymentMethodDesc)){
            paymentMethodDesc = remoteMessage.getData().get("paymentMethodDesc");
        }
        if(TextUtils.isEmpty(startTime)){
            startTime = remoteMessage.getData().get("startTime");
        }
        CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();

        if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(subject)) {
            cardNotifyEntity.activityNo = remoteMessage.getData().get("activityNo");
            cardNotifyEntity.activityType = remoteMessage.getData().get("activityType");
        }
        cardNotifyEntity.receiveIcon = remoteMessage.getData().get("receiveIcon");
        cardNotifyEntity.requestPhone = remoteMessage.getData().get("requestPhone");
        cardNotifyEntity.requestIcon = remoteMessage.getData().get("requestIcon");
        cardNotifyEntity.requestUserName = remoteMessage.getData().get("requestUserName");
        cardNotifyEntity.requestSex = remoteMessage.getData().get("requestSex");
        cardNotifyEntity.receiveSex = remoteMessage.getData().get("receiveSex");
        cardNotifyEntity.transferAmount = remoteMessage.getData().get("transferAmount");
        cardNotifyEntity.inAppTitle = remoteMessage.getData().get("inAppTitle");
        cardNotifyEntity.requestPhone = remoteMessage.getData().get("requestPhone");
        cardNotifyEntity.discountType = remoteMessage.getData().get("discountType");
        cardNotifyEntity.requestSubject = subject;
        cardNotifyEntity.paymentMethodDesc = paymentMethodDesc;
        cardNotifyEntity.orderNo = orderNo;
        cardNotifyEntity.subject = subject;
        cardNotifyEntity.orderDirection = orderDirection;
        cardNotifyEntity.orderStatus = orderStatus;
        cardNotifyEntity.respCode = respCode;
        cardNotifyEntity.outTradeNo = outTradeNo;
        cardNotifyEntity.merchantId = merchantId;
        cardNotifyEntity.merchantName = merchantName;
        String extendProperties = remoteMessage.getData().get("extendProperties");
        String basicExtendProperties = remoteMessage.getData().get("basicExtendProperties");
        JSONArray jsonArray = JSON.parseArray(extendProperties);
        JSONArray basicJsonArray = JSON.parseArray(basicExtendProperties);
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<List<ExtendEntity>>() {
            }.getType();
            cardNotifyEntity.extendProperties = gson.fromJson(extendProperties, type);
            cardNotifyEntity.basicExtendProperties = gson.fromJson(basicExtendProperties, type);
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
//        if (!TextUtils.isEmpty(merchantId)) {
//            ExtendEntity extendEntity = new ExtendEntity(merchantId, merchantName);
//            cardNotifyEntity.extendProperties.add(extendEntity);
//        }
        cardNotifyEntity.orderDesc = orderDesc;
        cardNotifyEntity.startTime = startTime;
        cardNotifyEntity.respMsg = respMsg;
        cardNotifyEntity.title = title;
        cardNotifyEntity.orderAmount = orderAmount;
        cardNotifyEntity.payerAmount = payerAmount;
        cardNotifyEntity.orderCurrencyCode = orderCurrencyCode;
        return cardNotifyEntity;
    }

    private void sendNotification(CardNotifyEntity cardNotifyEntity, Context mContext, String id, String name, int msgType) {

        Intent intent = new Intent(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (msgType == MSG_CARD_BIND) {
            ProjectApp.addCardSuccess = true;
            ProjectApp.setCardNotifyEntity(cardNotifyEntity);
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                addCardSuccess(cardNotifyEntity, msgType, mContext);
            } else {
                sendNotifacation(mContext, cardNotifyEntity, id, name, msgType);
            }
            //要判断app此时是否在前台 如果在前台需要发通知 弹框提示
            ProjectApp.removeKycTask();
        } else if (msgType == MSG_ADD_MONEY) {
            ProjectApp.addMoneySuccessOnMain = true;
            ProjectApp.setCardNotifyEntity(cardNotifyEntity);
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                addMoneySuccess(cardNotifyEntity, msgType, mContext);
            } else {
                sendNotifacation(mContext, cardNotifyEntity, id, name, msgType);
            }

            //要判断app此时是否在前台 如果在前台需要发通知 弹框提示
            ProjectApp.removeAddMoneyTask();
        } else if (msgType == MSG_REQUEST_CENTER) {
            ProjectApp.requestTransferSuccess = true;
            ProjectApp.setCardNotifyEntity(cardNotifyEntity);
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                requestTransferSuccess(cardNotifyEntity, msgType, mContext, false);
            } else {
                sendNotifacation(mContext, cardNotifyEntity, id, name, msgType);
            }
        } else if (msgType == MSG_CHANGE_LOGIN_DEVICE) {
            ProjectApp.setCardNotifyEntity(cardNotifyEntity);
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                loginOtherDeviceSuccess(cardNotifyEntity, msgType, mContext, false);
            } else {
                sendNotifacation(mContext, cardNotifyEntity, id, name, msgType);
            }
        } else if (msgType == MSG_CQR_PAYMENT) {
            ProjectApp.setCardNotifyEntity(cardNotifyEntity);
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                if (Constants.SUBJECT_CQR_PAYMENT.equals(subject)) {
                    cqrPaymentSuccess(cardNotifyEntity, msgType, mContext, false);
                }
            } else {
                sendNotifacation(mContext, cardNotifyEntity, id, name, msgType);
            }
        } else if (msgType == MSG_NOTIFY) {
            ProjectApp.setCardNotifyEntity(cardNotifyEntity);
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                notificationSuccess(cardNotifyEntity, msgType, mContext, false);
            } else {
                sendNotifacation(mContext, cardNotifyEntity, id, name, msgType);
            }
        }
    }

    private static void notificationSuccess(CardNotifyEntity cardNotifyEntity, int msgType, Context mContext, boolean isPush) {
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            Activity activity = ActivityLifeManager.getInstance().getCurrentActivity();
            if (isPush) {
                if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(cardNotifyEntity.subject)) {
                    ProjectApp.showMarketActivity = true;
                }
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), RequestCenterActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                if (activity instanceof MainActivity) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(cardNotifyEntity.subject)) {
                                String activityNo = cardNotifyEntity.activityNo;
                                String activityType = cardNotifyEntity.activityType;
                                ((BaseHintCompatActivity) activity).showMarketActivityDetail(activityNo, activityType);
                            } else {
                                ((MainActivity) activity).showNotifyDialog(cardNotifyEntity);
                            }
                            ((MainActivity) activity).homeFragment.onResume();
                        }
                    });
                } else if (activity instanceof BaseHintCompatActivity) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(cardNotifyEntity.subject)) {
                                String activityNo = cardNotifyEntity.activityNo;
                                String activityType = cardNotifyEntity.activityType;
                                ((BaseHintCompatActivity) activity).showMarketActivityDetail(activityNo, activityType);
                            } else {
                                ((BaseHintCompatActivity) activity).showNotifyDialog(cardNotifyEntity);
                            }
                        }
                    });
                }
            }
        } else {
            Intent it = new Intent(mContext, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
            ProjectApp.requestTransferSuccess = true;
            mContext.startActivity(it);
        }
    }

    private static void addMoneySuccess(CardNotifyEntity cardNotifyEntity, int msgType, Context context) {
        //充值成功
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        mHashMaps.put(Constants.MSGTYPE, msgType);
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.addMoneySuccessOnMain = false;
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            context.startActivity(it);
        }
    }

    private static void cqrPaymentSuccess(CardNotifyEntity cardNotifyEntity, int msgType, Context context, boolean isPush) {
        //背扫交易成功
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        mHashMaps.put(Constants.MSGTYPE, msgType);
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            context.startActivity(it);
        }
    }

    private static void loginOtherDeviceSuccess(CardNotifyEntity cardNotifyEntity, int msgType, Context context, boolean isPush) {
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            Activity activity = ActivityLifeManager.getInstance().getCurrentActivity();
            if (isPush) {
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), LoginFastNewActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                ProjectApp.removeAllTaskIncludeMainStack();
            } else {
                if (activity instanceof MainActivity) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity) activity).showNotifyDialog(cardNotifyEntity);
                            ((MainActivity) activity).homeFragment.onResume();
                        }
                    });
                } else if (activity instanceof BaseHintCompatActivity) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseHintCompatActivity) activity).showNotifyDialog(cardNotifyEntity);
                        }
                    });
                }
            }
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.REQUEST_TRANSFER_ENTITY, cardNotifyEntity);
            context.startActivity(it);
        }
    }

    private static void requestTransferSuccess(CardNotifyEntity cardNotifyEntity, int msgType, Context context, boolean isPush) {
        //请求转账成功
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.REQUEST_TRANSFER_ENTITY, cardNotifyEntity);
        mHashMaps.put(Constants.MSGTYPE, msgType);
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            Activity activity = ActivityLifeManager.getInstance().getCurrentActivity();
            if (isPush) {
                ActivitySkipUtil.startAnotherActivity(activity, RequestCenterActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                if (activity instanceof RequestCenterActivity) {
                    activity.finish();
                }
            } else {
                if (activity instanceof RequestCenterActivity) {
                    RequestCenterActivity requestCenterActivity = (RequestCenterActivity) activity;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            requestCenterActivity.showRequestDialog(cardNotifyEntity);
                            requestCenterActivity.getNotifyList();
                        }
                    });
                } else if (activity instanceof MainActivity) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity) activity).showRequestDialog(cardNotifyEntity);
                            ((MainActivity) activity).homeFragment.onResume();
                        }
                    });
                } else if (activity instanceof BaseHintCompatActivity) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseHintCompatActivity) activity).showRequestDialog(cardNotifyEntity);
                        }
                    });
                }
            }
            ProjectApp.requestTransferSuccess = false;
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.REQUEST_TRANSFER_ENTITY, cardNotifyEntity);
            context.startActivity(it);
        }
    }

    private static void addCardSuccess(CardNotifyEntity cardNotifyEntity, int msgType, Context context) {
        //绑卡成功
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
        mHashMaps.put(Constants.MSGTYPE, msgType);
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            if ("000000".equals(cardNotifyEntity.respCode)) {
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AddCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AddCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            }
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
            ProjectApp.addCardSuccess = true;
            context.startActivity(it);
        }
    }

    private void sendNotifacation(Context mContext, CardNotifyEntity cardNotifyEntity, String id, String name, int msgType) {
        Intent intent = new Intent(mContext, NotifyClickReceiver.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.MSGTYPE, msgType);
        intent.putExtra(Constants.NOTIFY_ENTITY, cardNotifyEntity);
//        if (msgType == MSG_ADD_MONEY) {
//        } else if(msgType == MSG_CARD_BIND) {
//            intent.putExtra(Constants.NOTIFY_ENTITY, cardNotifyEntity);
//        } else if(msgType == MSG_REQUEST_CENTER){
//            intent.putExtra(Constants.NOTIFY_ENTITY, cardNotifyEntity);
//        }
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, (int) (Math.random() * 100), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getBroadcast(mContext, new SecureRandom().nextInt(100), intent, PendingIntent.FLAG_IMMUTABLE);
        } else {
            pendingIntent = PendingIntent.getBroadcast(mContext, new SecureRandom().nextInt(100), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
//        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap iconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.tiqmo_logo);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        mBuilder.setSmallIcon(R.mipmap.tiqmo_logo_small).
                setContentTitle(cardNotifyEntity.title).
                setContentText(cardNotifyEntity.title).
                setAutoCancel(true).
                setLargeIcon(iconBitmap).
                setSound(defaultSoundUri).
                setContentIntent(pendingIntent);
        //创建大文本样式
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(cardNotifyEntity.title);
        mBuilder.setStyle(bigTextStyle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            mBuilder.setChannelId(id);
            channel.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification), Notification.AUDIO_ATTRIBUTES_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(NOTIFICATION_ID++, mBuilder.build());
    }

    public static class NotifyClickReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int msgType = intent.getIntExtra(Constants.MSGTYPE, -1);
            CardNotifyEntity cardNotifyEntity = (CardNotifyEntity) intent.getSerializableExtra(Constants.NOTIFY_ENTITY);
            switch (msgType) {
                case -1:
                    addCardSuccess(cardNotifyEntity, msgType, context);
                    //绑卡成功
                    break;
                case -2:
                    //充值成功
                    addMoneySuccess(cardNotifyEntity, msgType, context);
                    break;
                case -3:
                    //请求转账成功
                    requestTransferSuccess(cardNotifyEntity, msgType, context, true);
                    break;
                case -4:
                    notificationSuccess(cardNotifyEntity, msgType, context, true);
                    break;
                case -5:
                    loginOtherDeviceSuccess(cardNotifyEntity, msgType, context, true);
                    break;
                case -6:
                    cqrPaymentSuccess(cardNotifyEntity, msgType, context, true);
                    break;
                case -10:
                    FileUtils.openFile(context,cardNotifyEntity.pdfFilePath);
                    break;
                default:
                    break;
            }
        }
    }
}
