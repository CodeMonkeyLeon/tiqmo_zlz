package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class VoucherMarketAdapter extends BaseRecyclerAdapter<VoucherBrandEntity> {

    private int width, height;

    public VoucherMarketAdapter(Context mContext, @Nullable List<VoucherBrandEntity> data) {
        super(R.layout.item_voucher_market_brand, data);
        width = (AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(mContext, 60)) / 3;
        height = AndroidUtils.dip2px(mContext, 80);
    }


    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VoucherBrandEntity voucherBrandEntity, int position) {
        ImageView ivBrand = baseViewHolder.getView(R.id.iv_brand);
        ConstraintLayout conBrand = baseViewHolder.getView(R.id.con_brand);
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(width, height);
        conBrand.setLayoutParams(layoutParams);
        String brandLogo = ThemeUtils.isCurrentDark(mContext) ? voucherBrandEntity.operatorSmallDarkIcon : voucherBrandEntity.operatorSmallLightIcon;
        if (TextUtils.isEmpty(brandLogo)) {
            brandLogo = voucherBrandEntity.voucherBrandLogo;
        }
        if (!TextUtils.isEmpty(brandLogo)) {
            Glide.with(mContext).clear(ivBrand);
            Glide.with(mContext)
                    .load(brandLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivBrand);
        }
    }
}
