package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillSkuEntity extends BaseEntity {

    public String description;
    public String currency;
    public String amount;
    public String sku;
}
