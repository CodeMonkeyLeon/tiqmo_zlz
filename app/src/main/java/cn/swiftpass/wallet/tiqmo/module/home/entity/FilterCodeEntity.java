package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class FilterCodeEntity extends BaseEntity {

    public String title;
    public String value;
    /**
     * 1 category 2 type  3 date
     */
    public int type;
    public int index;
    public String selected = "0";
}
