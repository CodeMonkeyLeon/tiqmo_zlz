package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;

public class BlockMyCardActivity extends BaseCompatActivity {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.rg_reason)
    RadioGroup rgReason;
    @BindView(R.id.rb_reason_one)
    RadioButton rgReasonOne;
    @BindView(R.id.rb_reason_two)
    RadioButton rgReasonTwo;
    @BindView(R.id.rb_reason_three)
    RadioButton rgReasonThree;
    @BindView(R.id.rb_reason_four)
    RadioButton rgReasonFour;
    @BindView(R.id.ll_reason)
    LinearLayout llReason;
    @BindView(R.id.et_reason)
    CustomizeEditText etReason;
    @BindView(R.id.tv_block)
    TextView tvBlock;
    @BindView(R.id.tv_explain)
    TextView tvExplain;

    private String reason;

    private String proxyCardNo;
    private KsaCardEntity ksaCardEntity;
    private String blockCardReasonStatus;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_block_my_card;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.DigitalCard_73);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        proxyCardNo = getIntent().getExtras().getString("proxyCardNo");
        ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
        tvExplain.setText(Spans.builder().text(getString(R.string.Card_block_10))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_6b6c73)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"))
                .size(12)
                .text(" " + getString(R.string.Card_block_9))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_6b6c73)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"))
                .size(12)
                .build());
        llReason.setVisibility(View.GONE);
        rgReason.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                llReason.setVisibility(View.GONE);
                rgReasonOne.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
                rgReasonTwo.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
                rgReasonThree.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
                rgReasonFour.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"));
                switch (checkedId) {
                    case R.id.rb_reason_one:
                        rgReasonOne.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"));
                        reason = getString(R.string.Card_block_3);
                        tvBlock.setEnabled(true);
                        blockCardReasonStatus = "S";
                        break;
                    case R.id.rb_reason_two:
                        rgReasonTwo.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"));
                        reason = getString(R.string.Card_block_4);
                        tvBlock.setEnabled(true);
                        blockCardReasonStatus = "L";
                        break;
                    case R.id.rb_reason_three:
                        rgReasonThree.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"));
                        reason = getString(R.string.Card_block_5);
                        tvBlock.setEnabled(true);
                        blockCardReasonStatus = "H";
                        break;
                    case R.id.rb_reason_four:
                        rgReasonFour.setTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"));
                        llReason.setVisibility(View.VISIBLE);
                        tvBlock.setEnabled(false);
                        blockCardReasonStatus = "C";
                        break;
                    default:
                        break;
                }
            }
        });

        etReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString())) {
                    tvBlock.setEnabled(true);
                } else {
                    tvBlock.setEnabled(false);
                }
            }
        });
    }

    @OnClick({R.id.tv_block, R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_block:
                if (llReason.getVisibility() == View.VISIBLE) {
                    reason = etReason.getText().toString().trim();
                }
                HashMap<String, Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
                mHashMap.put(Constants.blockCardReason, reason);
                mHashMap.put(Constants.blockCardReasonStatus, blockCardReasonStatus);
                ActivitySkipUtil.startAnotherActivity(this, BlockCardConfirmActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }
}
