package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class CardSetPinPresenter implements CardKsaContract.SetCardPinPresener {

    CardKsaContract.SetCardPinView mSetCardPinView;

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mSetCardPinView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mSetCardPinView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mSetCardPinView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardPin(String pin, String proxyCardNo) {
        AppClient.getInstance().getCardManager().setKsaCardPin(pin, proxyCardNo, new LifecycleMVPResultCallback<Void>(mSetCardPinView,true) {
            @Override
            protected void onSuccess(Void result) {
                mSetCardPinView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mSetCardPinView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardStatus(String status, String proxyCardNo, String reason,String reasonStatus) {
        AppClient.getInstance().getCardManager().setKsaCardStatus(status, proxyCardNo, reason,reasonStatus, new LifecycleMVPResultCallback<Void>(mSetCardPinView,true) {
            @Override
            protected void onSuccess(Void result) {
                mSetCardPinView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mSetCardPinView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardLimit(String txnType,  String dailyLimitValue, String monthlyLimitValue, String proxyCardNo,String up) {
        AppClient.getInstance().getCardManager().setKsaCardLimit(txnType, dailyLimitValue, monthlyLimitValue, proxyCardNo,up, new LifecycleMVPResultCallback<Void>(mSetCardPinView,true) {
            @Override
            protected void onSuccess(Void result) {
                mSetCardPinView.setKsaCardLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mSetCardPinView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardChannel(String txnType, String txnTypeEnabled, String proxyCardNo) {
        AppClient.getInstance().getCardManager().setKsaCardChannel(txnType, txnTypeEnabled, proxyCardNo, new LifecycleMVPResultCallback<Void>(mSetCardPinView,true) {
            @Override
            protected void onSuccess(Void result) {
                mSetCardPinView.setKsaCardChannelSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mSetCardPinView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }


    @Override
    public void attachView(CardKsaContract.SetCardPinView setCardPinView) {
        this.mSetCardPinView = setCardPinView;
    }

    @Override
    public void detachView() {
        this.mSetCardPinView = null;
    }
}
