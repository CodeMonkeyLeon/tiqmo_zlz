package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class OtherInfoDialog extends BottomDialog {

    private Context mContext;

    private int errorType = 1;

    private BottomButtonListener bottomButtonListener;

    private String msg;

    public void setBottomButtonListener(BottomButtonListener bottomButtonListener) {
        this.bottomButtonListener = bottomButtonListener;
    }

    public OtherInfoDialog(Context context, String message) {
        super(context);
        this.mContext = context;
        this.msg = message;
        initViews(mContext);
    }


    public interface BottomButtonListener {
        void clickButton();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_other_info, null);
        TextView tvDialogTitle = view.findViewById(R.id.tv_title);
        TextView tvDialogContent = view.findViewById(R.id.tv_content);
        ImageView ivexpanded = view.findViewById(R.id.iv_expanded);
        tvDialogTitle.setText(mContext.getString(R.string.BillPay_62));
        tvDialogContent.setText(msg);

        ivexpanded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
