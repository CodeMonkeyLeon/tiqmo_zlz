package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.OrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.ScanOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferQrCodePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.FileUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ParseQrCodeAysnTask;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.QRCodeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.zxing.BeepManager;
import cn.swiftpass.wallet.tiqmo.support.zxing.Camera.CameraManager;
import cn.swiftpass.wallet.tiqmo.support.zxing.Decoding.CaptureActivityHandler;
import cn.swiftpass.wallet.tiqmo.support.zxing.Decoding.DecodeFormatManager;
import cn.swiftpass.wallet.tiqmo.support.zxing.Decoding.InactivityTimer;
import cn.swiftpass.wallet.tiqmo.support.zxing.Decoding.QrcodeScanListener;
import cn.swiftpass.wallet.tiqmo.support.zxing.ViewfinderView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ScanQrActivity extends BaseCompatActivity<TransferContract.TransferQrCodePresenter> implements TransferContract.TransferQrCodeView, SurfaceHolder.Callback {

    private static final int request_crcode = 1002;
    private static final int REQUEST_PICK_FORM_ALBUM = 0x16;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.preview_view)
    SurfaceView previewView;
    @BindView(R.id.viewfinder_view)
    ViewfinderView viewfinderView;
    @BindView(R.id.scan_layout)
    FrameLayout scanLayout;
    @BindView(R.id.con_qr_code)
    ConstraintLayout con_qr_code;
    @BindView(R.id.iv_scan_background)
    ImageView ivScanBackground;
    @BindView(R.id.iv_scan_code)
    ImageView ivScanCode;
    @BindView(R.id.con_my_scan)
    ConstraintLayout conMyScan;
    @BindView(R.id.ll_wrong_qr)
    LinearLayout llWrongQr;
    @BindView(R.id.tv_wrong_code_one)
    TextView tvWrongCodeOne;
    @BindView(R.id.tv_wrong_code_two)
    TextView tvWrongCodeTwo;
    @BindView(R.id.tv_show_code)
    TextView tvShowCode;
    @BindView(R.id.ll_my_code)
    ConstraintLayout llMyCode;
    @BindView(R.id.tv_scan_qr_title)
    TextView tv_scan_qr_title;
    @BindView(R.id.iv_scan_light)
    ImageView ivScanLight;
    @BindView(R.id.iv_scan_photo)
    ImageView ivScanPhoto;

    private BottomDialog webDialog;
    private BottomDialog qrDialog;

    private ImageView ivQrCode;

    /**
     * 解析二维码成功 控制震动
     */
    private BeepManager beepManager;
    private CaptureActivityHandler handler;
    private Handler mHandler;
    /**
     * zxing 二维码解析类型
     */
    private Collection<BarcodeFormat> decodeFormats;
    private Map<DecodeHintType, ?> decodeHints;
    private InactivityTimer inactivityTimer;
    private QrcodeScanListener qrcodeScanListener;
    private Handler crHandler;
    private CameraManager cameraManager;
    private static final String TAG = "QrScanActivity";
    /**
     * 判断srufaceview是否已经创建
     */
    private boolean mSurfaceViewCreated;

    //是否第一次选择拒绝且不再询问权限
    public boolean firstRejectCamera = true;

    private String orderNo, monthLimitAmt;
    private String mJSCallback;
    private String mJSData;
//    private ProgressBar mProgressBar;

    private String paymentMethodNo;

    private String url;
    private String type;
    private WebView mWebView;

    private String qrCodeUrl, paymentOne;

    private int cqCodeRefreshGapTime;

    private String isFrom;

    private boolean isOpenLight;

    /**
     * 特殊场景控制onResume的时候不能重新扫描
     */
    private boolean isSpecialPageEvent = false;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_qr_code;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void checkPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.CAMERA)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(ScanQrActivity.this, Manifest.permission.CAMERA)) {
                //拒绝并勾选不再询问
                firstRejectCamera = false;
            } else {
                firstRejectCamera = true;
            }
            PermissionInstance.getInstance().getPermission(ScanQrActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(ScanQrActivity.this, Manifest.permission.CAMERA)) {
                        if (!firstRejectCamera) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_29));
                        } else {
                            finish();
                            firstRejectCamera = false;
                        }
                    } else {
                        finish();
                    }
                }
            }, Manifest.permission.CAMERA);
        } else {
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack);
        tvTitle.setText(R.string.stt_1);
        tv_scan_qr_title.setText("\u202A" + getString(R.string.scan_qr_1));
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            cqCodeRefreshGapTime = configEntity.cqCodeRefreshGapTime;
        }
        if (getIntent().getExtras() != null) {
            isFrom = getIntent().getExtras().getString(Constants.isFrom);
            //暂时改为转账的二维码
            isFrom = "send";
            if("home".equals(isFrom)){
                tvShowCode.setText(getString(R.string.stt_4));
            }else{
                tvShowCode.setText(getString(R.string.sprint11_136));
            }
        }
        //检查是否有相机权限
        if (!isGranted(Manifest.permission.CAMERA)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(ScanQrActivity.this, Manifest.permission.CAMERA)) {
                //拒绝并勾选不再询问
                firstRejectCamera = false;
            } else {
                firstRejectCamera = true;
            }
        }

        checkPermission();

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        inactivityTimer = new InactivityTimer(this);
        beepManager = new BeepManager(this);
        mHandler = new Handler();

//        if (TextUtils.isEmpty(SpUtils.getInstance().getQrCode())) {
//            mPresenter.getQrCode(UserInfoManager.getInstance().getCurrencyCode());
//        } else {
//            qrCodeUrl = SpUtils.getInstance().getQrCode();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initCamera();

        if (qrDialog != null && qrDialog.isShowing()) {
            if (!TextUtils.isEmpty(paymentMethodNo)) {
                mPresenter.getCrCode(paymentMethodNo);
            }
        }
    }

    private void initCamera() {
        try {
            if (cameraManager == null) {
                cameraManager = new CameraManager(mContext);
                decodeFormats = EnumSet.noneOf(BarcodeFormat.class);
                decodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
            }
            if (viewfinderView != null) {
                viewfinderView.setCameraManager(cameraManager);
                viewfinderView.setOnFlashLightStateChangeListener(new ViewfinderView.onFlashLightStateChangeListener() {
                    @Override
                    public void openFlashLight(boolean open) {
                        turnOnFlashLight(open);
                        viewfinderView.setStopDraw(false);
                        viewfinderView.reOnDraw();
                    }
                });
            }
            resumeCamera();
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }


    /**
     * 打开关闭闪光灯
     *
     * @param open
     */
    protected void turnOnFlashLight(boolean open) {
        if (cameraManager != null) {
            viewfinderView.setOpenFlashLight(open);
            cameraManager.setTorch(open);
        }
    }

    private void initScanListener() {
        qrcodeScanListener = new QrcodeScanListener() {
            @Override
            public void scanResult(boolean isOk, String result) {

            }

            @Override
            public void submitData(String result, boolean status) {

            }

            @Override
            public void drawViewfinder() {
            }

            @Override
            public void handleDecode(Result obj, Bitmap barcode) {
                LogUtils.i(TAG, "message;" + obj.getText());
                beepManager.playBeepSoundAndVibrate();
                parseCodeStr(obj.getText());

            }

            @Override
            public void setResult(int resultOk, Intent obj) {

            }

            @Override
            public ViewfinderView getViewfinderView() {
                return viewfinderView;
            }

            @Override
            public Handler getHandler() {
                return handler;
            }

            @Override
            public CameraManager getCameraManager() {
                return cameraManager;
            }

            @Override
            public Context getContext() {
                return getContext();
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        onStopPreview();
    }

    /**
     * 二维码解析
     *
     * @param uuid
     */
    private void parseCodeStr(final String uuid) {
        url = uuid;
        try {
            //停止预览
            stopPreview();
            if (isConnected()) {
//                mPresenter.checkQrCode(uuid);
                mPresenter.checkOrderCode(uuid);
            } else {
                llWrongQr.setVisibility(View.VISIBLE);
                tvWrongCodeOne.setText(R.string.common_3);
                tvWrongCodeTwo.setText("");

                viewfinderView.reErrorDraw();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restartPreviewAfterDelay();
                        if (viewfinderView != null) {
                            viewfinderView.reRightDraw();
                        }
                    }
                }, 3000);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    /**
     * 停止预览并扫描 收银台/密码等弹出层出现 背景扫描应该停止
     */
    private void onStopPreview() {
        if (handler != null) {
            handler.stopPreviewAndDecode();
            viewfinderView.setStopDraw(true);
        }
    }

    /**
     * camera 释放
     */
    private void resumeCamera() {
        SurfaceHolder surfaceHolder = previewView.getHolder();
        surfaceHolder.addCallback(this);
        initCamera(surfaceHolder);
        viewfinderView.setStopDraw(false);
        viewfinderView.reOnDraw();
        beepManager.updatePrefs();
        inactivityTimer.onResume();
        decodeFormats = null;
    }

    /**
     * 打开摄像头
     *
     * @param surfaceHolder
     */
    private void initCamera(SurfaceHolder surfaceHolder) {
        try {
            LogUtils.i(TAG, "open camera:");
            cameraManager.openDriver(surfaceHolder);
            if (handler == null) {
                initScanListener();
                handler = new CaptureActivityHandler(qrcodeScanListener, decodeFormats, null, decodeHints, mHandler, cameraManager);
            } else {
                //重新自动聚焦 华为 三星某些手机适配
                if (qrDialog != null && qrDialog.isShowing()) {

                } else if (webDialog != null && webDialog.isShowing()) {

                } else {
                    restartPreviewAfterDelay();
                }
            }
        } catch (Exception ioe) {
            Log.w(TAG, ioe);
        }
    }

    /**
     * 重新开始扫描解析
     *
     * @param
     */
    public void restartPreviewAfterDelay() {
        if (handler != null) {
            viewfinderView.setStopDraw(false);
            viewfinderView.reOnDraw();
            handler.restartPreview();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        LogUtils.i(TAG, "surfaceCreated---> " + mSurfaceViewCreated);
        if (!mSurfaceViewCreated) {
            mSurfaceViewCreated = true;
            initCamera(holder);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        LogUtils.i(TAG, "surfaceChanged---> ");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mSurfaceViewCreated = false;
        LogUtils.i(TAG, "surfaceDestroyed---> ");
    }

    @Override
    public void getQrCodeSuccess(QrCodeEntity qrCodeEntity) {
        if (qrCodeEntity != null) {
            qrCodeUrl = qrCodeEntity.qRCodeInfo;
            if("send".equals(isFrom)){
                HashMap<String,Object> hashMap = new HashMap<>();
                hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                hashMap.put(Constants.isFrom,isFrom);
                ActivitySkipUtil.startAnotherActivity(this,MyQrCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else {
                HashMap<String,Object> hashMap = new HashMap<>();
                hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                hashMap.put(Constants.paymentMethod,paymentOne);
                ActivitySkipUtil.startAnotherActivity(this,MyPaymentCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                Bitmap bitmap = QRCodeUtils.createQRCode(qrCodeUrl, AndroidUtils.dip2px(mContext, 220));
//                showQrDialog(bitmap, paymentOne);
            }
        }
    }

    @Override
    public void getQrCodeFail(String errCode, String errMsg) {

    }

    @Override
    public void getCrCodeSuccess(QrCodeEntity qrCodeEntity) {
        if (qrCodeEntity != null) {
            qrCodeUrl = qrCodeEntity.codeFlow;
            Bitmap bitmap = QRCodeUtils.createQRCode(qrCodeUrl, AndroidUtils.dip2px(mContext, 220));
            if (bitmap != null) {
                if (crHandler == null) {
                    crHandler = new Handler();
                }
                if (cqCodeRefreshGapTime > 0) {
                    crHandler.postDelayed(requestRunnable, cqCodeRefreshGapTime * 1000);
                } else {
                    crHandler.postDelayed(requestRunnable, 60000);
                }
                if (qrDialog != null && qrDialog.isShowing()) {
                    ivQrCode.setImageBitmap(bitmap);
                } else {
                    HashMap<String,Object> hashMap = new HashMap<>();
                    hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                    hashMap.put(Constants.paymentMethod,paymentOne);
                    ActivitySkipUtil.startAnotherActivity(this,MyPaymentCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                    showQrDialog(bitmap, paymentOne);
                }
            }
        }
    }

    @Override
    public void getCrCodeFail(String errCode, String errMsg) {

    }

    @Override
    public void checkQrCodeFailed(String errorCode, String errorMsg) {
        try {
            llWrongQr.setVisibility(View.VISIBLE);
            if ("010072".equals(errorCode)) {
                tvWrongCodeOne.setText(R.string.stt_2);
//                tvWrongCodeTwo.setText(R.string.stt_3);
            } else {
                tvWrongCodeOne.setText(errorMsg);
//                tvWrongCodeTwo.setText("");
            }
            viewfinderView.reErrorDraw();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    restartPreviewAfterDelay();
                    if (viewfinderView != null) {
                        viewfinderView.reRightDraw();
                    }
                }
            }, 3000);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
//        showTipDialog(errorMsg);
    }

    @Override
    public void checkQrCodeSuccess(UserInfoEntity qrUserEntity) {
        llWrongQr.setVisibility(View.GONE);
        if (qrUserEntity != null) {
            KycContactEntity kycContactEntity = new KycContactEntity();
            kycContactEntity.setUserId(qrUserEntity.userId);
            kycContactEntity.setContactsName(qrUserEntity.userName);
            kycContactEntity.setSex(qrUserEntity.gender);
            kycContactEntity.setPhone(qrUserEntity.phone);
            kycContactEntity.setHeadIcon(qrUserEntity.avatar);
            HashMap<String, Object> map = new HashMap<>(16);
            kycContactEntity.setContactsName(AndroidUtils.getContactName(kycContactEntity.getContactsName()));
            map.put(Constants.CONTACT_ENTITY, kycContactEntity);
            map.put(Constants.sceneType, Constants.TYPE_TRANSFER_ST);
            ActivitySkipUtil.startAnotherActivity(ScanQrActivity.this, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void checkOrderCodeFailed(String errorCode, String errorMsg) {
        checkCodeFail(errorCode, errorMsg);
    }

    private void checkCodeFail(String errorCode, String errorMsg) {
        try {
            llWrongQr.setVisibility(View.VISIBLE);
            if ("010072".equals(errorCode)) {
                tvWrongCodeOne.setText(R.string.stt_2);
            } else {
                tvWrongCodeOne.setText(errorMsg);
            }
            viewfinderView.reErrorDraw();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    restartPreviewAfterDelay();
                    if (viewfinderView != null) {
                        viewfinderView.reRightDraw();
                    }
                }
            }, 3000);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void checkOrderCodeSuccess(ScanOrderEntity scanOrderEntity) {
        llWrongQr.setVisibility(View.GONE);
        if (scanOrderEntity != null) {
            type = scanOrderEntity.type;
            if ("c".equals(type)) {
                UserInfoEntity qrUserEntity = scanOrderEntity.scanUserInfo;
                if (qrUserEntity != null) {
                    KycContactEntity kycContactEntity = new KycContactEntity();
                    kycContactEntity.setUserId(qrUserEntity.userId);
                    kycContactEntity.setContactsName(qrUserEntity.userName);
                    kycContactEntity.setSex(qrUserEntity.gender);
                    kycContactEntity.setPhone(qrUserEntity.phone);
                    kycContactEntity.setHeadIcon(qrUserEntity.avatar);
                    HashMap<String, Object> map = new HashMap<>(16);
                    kycContactEntity.setContactsName(AndroidUtils.getContactName(kycContactEntity.getContactsName()));
                    map.put(Constants.CONTACT_ENTITY, kycContactEntity);
                    map.put(Constants.sceneType, Constants.TYPE_TRANSFER_ST);
                    ActivitySkipUtil.startAnotherActivity(ScanQrActivity.this, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            } else if ("o".equals(type)) {
                OrderInfoEntity orderInfoEntity = scanOrderEntity.orderInfo;
                if (orderInfoEntity != null) {
                    orderNo = orderInfoEntity.orderNo;
                    mPresenter.getLimit(Constants.LIMIT_TRANSFER);
                }
            } else if ("m".equals(type)) {
                if (!TextUtils.isEmpty(url)) {
                    mPresenter.getLimit(Constants.LIMIT_TRANSFER);
//                    //跳转到商户html页面
//                    HashMap<String, Object> mHashMaps = new HashMap<>();
//                    mHashMaps.put(Constants.M_URL, url);
//                    ActivitySkipUtil.startAnotherActivity(ScanQrActivity.this, ScanWebActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                }
            } else if ("s".equals(type)) {
                checkCodeFail("010072", getString(R.string.stt_2));
            }
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        monthLimitAmt = transferLimitEntity.monthLimitAmt;

        if ("m".equals(type)) {
//            showWebDialog(url);
            //跳转到商户html页面
            // 为了适配商户html页面，将本地的亮暗色传给后台
//            url = url + "&theme=" + (ThemeUtils.isCurrentDark(mContext) ? "dark" : "white");
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.M_URL, url);
            ActivitySkipUtil.startAnotherActivity(ScanQrActivity.this, ScanWebActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        }
        if (!TextUtils.isEmpty(orderNo)) {
            if ("o".equals(type)) {
                mPresenter.checkOut(orderNo, "");
            }
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                restartPreviewAfterDelay();
                if (viewfinderView != null) {
                    viewfinderView.reRightDraw();
                }
            }
        }, 3000);
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        HashMap<String, Object> map = new HashMap<>(16);
        checkOutEntity.monthLimitAmt = monthLimitAmt;
        map.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        ActivitySkipUtil.startAnotherActivity(ScanQrActivity.this, ScanPayActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkOutFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                restartPreviewAfterDelay();
                if (viewfinderView != null) {
                    viewfinderView.reRightDraw();
                }
            }
        }, 3000);
    }

    /**
     * 停止预览并扫描 收银台/密码等弹出层出现 背景扫描应该停止
     */
    private void stopPreview() {
        if (handler != null) {
            handler.stopPreviewAndDecode();
            viewfinderView.setStopDraw(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (viewfinderView != null) {
            viewfinderView.drawViewfinder();
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        if (crHandler != null) {
            crHandler.removeCallbacksAndMessages(null);
        }
    }

    private void releaseCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
//        if (inactivityTimer != null) {
//            inactivityTimer.onPause();
//        }
        if (mSurfaceViewCreated && previewView != null) {
            previewView = findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = previewView.getHolder();
            surfaceHolder.removeCallback(this);
            mSurfaceViewCreated = false;
            LogUtils.i(TAG, "releaseCamera--->mSurfaceViewCreated" + mSurfaceViewCreated);
        }
        if (viewfinderView != null) {
            viewfinderView.setOpenFlashLight(false);
        }
        if (cameraManager != null) {
            cameraManager.stopPreview();
            cameraManager.closeDriver();
            cameraManager = null;
        }
    }


    @OnClick({R.id.iv_back, R.id.con_qr_code,R.id.tv_show_code,R.id.iv_scan_light,R.id.iv_scan_photo})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_scan_light:
                if(isOpenLight){
                    isOpenLight = false;
                }else {
                    isOpenLight = true;
                }
                turnOnFlashLight(isOpenLight);
                break;
            case R.id.iv_scan_photo:
                pickFromAlbum();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_show_code:
                if("home".equals(isFrom)){
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("openQrCode", true);
                    hashMap.put(Constants.pd_sceneType, Constants.TYPE_TRANSFER_QR);
                    hashMap.put(Constants.sceneType, Constants.TYPE_SCAN_MERCHANT);
                    ActivitySkipUtil.startAnotherActivityForResult(ScanQrActivity.this, TransferPwdActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, request_crcode);
                }else if("send".equals(isFrom)){
                    if(AndroidUtils.isCloseUse(this,Constants.send_myCode))return;
                    qrCodeUrl = SpUtils.getInstance().getQrCode();
                    if (TextUtils.isEmpty(qrCodeUrl) || qrCodeUrl.startsWith("swiftpass://")) {
                        mPresenter.getQrCode(UserInfoManager.getInstance().getCurrencyCode());
                    } else {
                        HashMap<String,Object> hashMap = new HashMap<>();
                        hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                        hashMap.put(Constants.isFrom,isFrom);
                        ActivitySkipUtil.startAnotherActivity(this,MyQrCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }else{
                    finish();
                }
                break;
            case R.id.con_qr_code:

                break;
            default:
                break;
        }
    }

    /**
     * 从相册选择二维码
     */
    private void pickFromAlbum() {
        Intent innerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
        innerIntent.setType("image/*");
        Intent wrapperIntent = Intent.createChooser(innerIntent, "");
        this.startActivityForResult(wrapperIntent, REQUEST_PICK_FORM_ALBUM);
    }


    public void parserLocalQrcodeSuccess(String content) {
        parseCodeStr(content);
    }

    private void parserLocalQrcode(final Activity context, Uri photoUri) {
        stopPreview();
        showProgress(true);
        if (photoUri == null) {
            parserLocalQrcodeSuccess(null);
        } else {
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoUri);
            } catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
            try {
                bitmap = FileUtils.getBitmapFormUri(context, photoUri);
            } catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
            if (bitmap == null) {
                parserLocalQrcodeSuccess(null);
                showProgress(false);
            } else {
                ParseQrCodeAysnTask.onParseResultListener listener = new ParseQrCodeAysnTask.onParseResultListener() {
                    @Override
                    public void onResult(String res) {
                        parserLocalQrcodeSuccess(res);
                        showProgress(false);
                    }
                };
                new ParseQrCodeAysnTask(bitmap, listener).execute();
            }
        }

    }

    private final Runnable requestRunnable = new Runnable() {

        @Override
        public void run() {
            if (!TextUtils.isEmpty(paymentMethodNo)) {
                mPresenter.getCrCode(paymentMethodNo);
            }
        }
    };

    private void showQrDialog(Bitmap bitmap, String paymentOne) {
        stopPreview();
        qrDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_qrcode_source, null);
        ivQrCode = contentView.findViewById(R.id.iv_qr_code);
        TextView tvPaymentOne = contentView.findViewById(R.id.tv_payment_one);
        tvPaymentOne.setText(paymentOne);
        ivQrCode.setImageBitmap(bitmap);

        qrDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                restartPreviewAfterDelay();
                crHandler.removeCallbacks(requestRunnable);
                setAlpha(mContext, 1f);
            }
        });

        qrDialog.setContentView(contentView);
        Window dialogWindow = qrDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.height = AndroidUtils.dip2px(mContext, 500);
            dialogWindow.setAttributes(layoutParams);
            dialogWindow.setDimAmount(0f);
        }
        setAlpha(mContext, 0.7f);
        qrDialog.showWithBottomAnim();
    }

    @Override
    public TransferContract.TransferQrCodePresenter getPresenter() {
        return new TransferQrCodePresenter();
    }

    protected void initWebView(WebView webView, String url) {
        LogUtils.i(TAG, "original url :" + url);
        WebSettings settings = webView.getSettings();
        String userAgent = settings.getUserAgentString();
        settings.setUserAgentString(userAgent + " ;" + BuildConfig.UserAgent);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAllowFileAccessFromFileURLs(false);
        // 设置WebView可触摸放大缩小
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        //隐藏掉缩放工具
        settings.setDisplayZoomControls(false);

        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setAllowUniversalAccessFromFileURLs(false);

        webView.loadUrl(url);
        webView.setLongClickable(true);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }

    private final WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            String url = mWebView.getUrl();
            if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(title) && url.contains(title)) {//防止title显示网站
                return;
            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
//            if (newProgress == 100) {
//                mProgressBar.setVisibility(View.GONE);
//            } else {
//                mProgressBar.setVisibility(View.VISIBLE);
//                mProgressBar.setProgress(newProgress);
//            }
        }
    };

    private final WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //拦截URL并且解析
            return analyzeUrl(Uri.parse(url));
        }
    };

    //解析URI，判断是否是H5支付请求
    private boolean analyzeUrl(Uri uri) {
        String protocol = uri.getScheme();
        String host = uri.getHost();
        //判空，同时判断host是否是swallet开头的H5支付请求
        if (TextUtils.isEmpty(protocol) || TextUtils.isEmpty(host) || !AppConfig.H5_REQUEST_FLAG.equals(protocol)) {
            return false;
        }
        mJSCallback = uri.getQueryParameter(AppConfig.H5_PARAM_CALLBACK);//回调的JS接口
        mJSData = uri.getQueryParameter(AppConfig.H5_PARAM_DATA);//JS数据
        switch (host) {//host对应了不同的操作，本质上就是网页通过url调用APP的功能
            case AppConfig.H5_FUNCTION_PAY://支付
                mPresenter.checkOut("", mJSData);
                break;
            case AppConfig.H5_FUNCTION_CLOSE://关闭
                finish();
                break;
            default:
                break;
        }
        return true;
    }

//    private void showWebDialog(String url) {
//        stopPreview();
//        webDialog = new BottomDialog(mContext);
//        View contentView = LayoutInflater.from(mContext).inflate(R.layout.activity_scan_web, null);
//        mWebView = contentView.findViewById(R.id.webView);
//
//        mWebView.setWebChromeClient(mWebChromeClient);
//        mWebView.setWebViewClient(mWebViewClient);
//        initWebView(mWebView, url);
//
//        webDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                restartPreviewAfterDelay();
//            }
//        });
//
//        webDialog.setContentView(contentView);
//        Window dialogWindow = webDialog.getWindow();
//        if (dialogWindow != null) {
//            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
//            dialogWindow.setAttributes(layoutParams);
//        }
//        webDialog.showWithBottomAnim();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == request_crcode) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    CheckOutEntity checkOutEntity = (CheckOutEntity) data.getSerializableExtra("checkOutEntity");
                    if (checkOutEntity != null) {
                        if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
                            paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
                            paymentOne = checkOutEntity.paymentMethodList.get(0).paymentMethodDesc;
                            if (!TextUtils.isEmpty(paymentMethodNo)) {
                                mPresenter.getCrCode(paymentMethodNo);
                            }
                        }
                    }
                }
            }
        }else if (requestCode == REQUEST_PICK_FORM_ALBUM){
            if (data != null) {
                parserLocalQrcode(ScanQrActivity.this, data.getData());
            }
        }
    }
}
