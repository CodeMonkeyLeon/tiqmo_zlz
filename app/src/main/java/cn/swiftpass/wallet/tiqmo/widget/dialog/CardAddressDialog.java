package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;

public class CardAddressDialog extends BottomDialog{

    private Context mContext;

    private SaveAddressListener mSaveAddressListener;

    public void setSaveListener(SaveAddressListener saveAddressListener) {
        this.mSaveAddressListener = saveAddressListener;
    }

    public CardAddressDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface SaveAddressListener {
        void clickSave();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_card_add_address, null);
        TextView tvSave = view.findViewById(R.id.tv_save);
        TextView tvPhone = view.findViewById(R.id.tv_phone);
        TextView tvProvinceSubtitle = view.findViewById(R.id.tv_province_subtitle);
        TextView tvProvince = view.findViewById(R.id.tv_province);
        TextView tvCity = view.findViewById(R.id.tv_city);
        TextView tvCityChooseSubtitle = view.findViewById(R.id.tv_city_choose_subtitle);
        FrameLayout llProvince = view.findViewById(R.id.ll_province);
        FrameLayout llCityChoose = view.findViewById(R.id.ll_city_choose);
        CustomizeEditText etAddress = view.findViewById(R.id.et_address);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSaveAddressListener != null) {
                    mSaveAddressListener.clickSave();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
