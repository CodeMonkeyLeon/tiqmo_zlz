package cn.swiftpass.wallet.tiqmo.module.chat.utils;

import androidx.annotation.Nullable;

import com.gc.gcchat.GcChatMessage;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.gc.gcchat.GcChatUser;
import com.google.gson.Gson;

import java.util.HashMap;

import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatMessage;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.UserChat;
import cn.swiftpass.wallet.tiqmo.module.chat.view.ChatRoomActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.UserUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;

public class MessageUtils {

    @Nullable
    public static MessageEntity getMessage(GcChatMessage chatMessage, String loggedInUserId, HashMap<String, String> messageMetaData, boolean isGroupChat) {
        if (chatMessage == null) {
            return null;
        }
        try {
            long timeStamp = chatMessage.getTimestamp();
            ChatMessage sampleAppChatMessage = getGcChatMessage(chatMessage);
            ChatTransferEntity chatTransferEntity = getChatTransferMessage(chatMessage);
            if (sampleAppChatMessage == null) {
                return null;
            }
            String content = sampleAppChatMessage.getMessage();
            String downloadLink;
            String fileName;
            if (sampleAppChatMessage.getAttachments().size() > 0) {
                ChatMessage.Attachment firstAttachment = sampleAppChatMessage.getAttachments().get(0);
                content = firstAttachment.fileId.trim();
                firstAttachment = sampleAppChatMessage.getAttachments().get(0);
                downloadLink = ChatHelper.getInstance().getChatUserAvatar(firstAttachment.fileId.trim());
                fileName = firstAttachment.fileName;
            } else {
                downloadLink = "";
                fileName = "";
            }

            String userId = "";
            String userName = "";
            GcChatUser sender = chatMessage.getSender();
            if (sender != null) {
                userId = sender.getExternalId();
                userName = sender.getName();
            }

            String messageId = chatMessage.getMessageId();


            MessageEntity message = new MessageEntity();
            boolean isOwner = loggedInUserId.equals(userId);
            if(chatTransferEntity != null){
                message.setMessageType(isOwner ? MessageEntity.MessageType.RightTransferMessage : MessageEntity.MessageType.LeftTransferMessage);
                message.setContent(content);
            }
            else if (sampleAppChatMessage.getAttachments().isEmpty()) {
                message.setMessageType(isOwner ? MessageEntity.MessageType.RightSimpleMessage : MessageEntity.MessageType.LeftSimpleMessage);
                message.setContent(content);
            } else if (
                    ChatRoomActivity.FileType.IMAGE_PNG.equals(sampleAppChatMessage.getAttachments().get(0).fileType)
                            || ChatRoomActivity.FileType.IMAGE_JPEG.equals(sampleAppChatMessage.getAttachments().get(0).fileType)
            ) {
                message.setMessageType(isOwner ? MessageEntity.MessageType.RightSingleImage : MessageEntity.MessageType.LeftSingleImage);
                String[] fullContentSplitsArray = content.split(" ");
                if (fullContentSplitsArray.length > 0) {
                    message.setContent(fullContentSplitsArray[0]);
                } else {
                    message.setContent("");
                }
                message.setBody(content.replaceFirst(message.getContent(), ""));
            } else if (ChatRoomActivity.FileType.PDF.equals(sampleAppChatMessage.getAttachments().get(0).fileType)) {
                message.setMessageType(isOwner ? MessageEntity.MessageType.RightFileImage : MessageEntity.MessageType.LeftFileImage);
            }
            UserChat usr = new UserChat();
            usr.setName(userName);
            usr.setId(userId);
            message.setUser(usr);
            message.setTimestamp(timeStamp);
            message.setDownloadLink(downloadLink);
            message.setFileName(fileName);
            message.setId(messageId);
            message.setMetaInfo(messageMetaData);
            message.setGroupChat(isGroupChat);
            message.setChatTransferEntity(chatTransferEntity);
            return message;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static ChatTransferEntity getChatTransferMessage(GcChatMessage chatMessage) {
        Object messageObject = chatMessage.getMessage();
        if (messageObject != null) {
            try {
                return new Gson().fromJson(new Gson().toJson(messageObject), ChatTransferEntity.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Nullable
    private static ChatMessage getGcChatMessage(GcChatMessage chatMessage) {
        Object messageObject = chatMessage.getMessage();
        if (messageObject != null) {
            try {
                return new Gson().fromJson(new Gson().toJson(messageObject), ChatMessage.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getChatSessionName(GcChatSession chatSession) {
        if (chatSession == null) {
            return "";
        }
        if (chatSession.getTitle() != null && !chatSession.getTitle().isEmpty()) {
            return chatSession.getTitle();
        } else {
            for (GcChatParticipant gcChatParticipant : chatSession.getParticipantUsers()) {
                if (!UserUtils.loggedInUserId.equals(gcChatParticipant.getUser().getExternalId())) {
                    return gcChatParticipant.getUser().getName();
                }
            }
        }
        return "";
    }

    public static String getChatPhoneNumber(GcChatSession chatSession) {
        if (chatSession == null) {
            return "";
        }
        for (GcChatParticipant gcChatParticipant : chatSession.getParticipantUsers()) {
            if (!UserUtils.loggedInUserId.equals(gcChatParticipant.getUser().getExternalId())) {
                return gcChatParticipant.getUser().getPhoneNumber();
            }
        }
        return "";
    }

    public static String getChatUserId(GcChatSession chatSession) {
        if (chatSession == null) {
            return "";
        }
        for (GcChatParticipant gcChatParticipant : chatSession.getParticipantUsers()) {
            if (!UserUtils.loggedInUserId.equals(gcChatParticipant.getUser().getExternalId())) {
                return gcChatParticipant.getUser().getExternalId();
            }
        }
        return "";
    }

    public static String getChatSessionDate(GcChatSession chatSession) {
        if (chatSession == null) {
            return "";
        }
        if (chatSession.getTitle() != null && !chatSession.getTitle().isEmpty()) {
            return chatSession.getTitle();
        } else {
            for (GcChatParticipant gcChatParticipant : chatSession.getParticipantUsers()) {
                if (!UserUtils.loggedInUserId.equals(gcChatParticipant.getUser().getExternalId())) {
                    return TimeUtils.getChatUserJoinDay(gcChatParticipant.getJoinDate());
                }
            }
        }
        return "";
    }

    @Nullable
    public static String getChatSessionDisplayPic(GcChatSession chatSession) {
        if (chatSession == null) {
            return "";
        }
        if (chatSession.isGroupChat()) {
            return chatSession.getFileId();
        } else {
            for (GcChatParticipant gcChatParticipant : chatSession.getParticipantUsers()) {
                if (!UserUtils.loggedInUserId.equals(gcChatParticipant.getUser().getExternalId())) {
                    return gcChatParticipant.getUser().getFileId();
                }
            }
        }
        return "";
    }
}
