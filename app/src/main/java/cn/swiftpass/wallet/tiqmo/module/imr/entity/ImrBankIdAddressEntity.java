package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrBankIdAddressEntity extends BaseEntity {
    //前端页面展示地址&ID的类型，* 0：都不展示（跳过此页面），1：只展示Address页面，2：只展示ID页面，3：展示Address&Id页面
    public String bankAddrIdType;
    //前端页面展示地址&ID的类型，* 0：都不展示（跳过此页面），1：只展示Address页面，2：只展示ID页面，3：展示Address&Id页面
    public String agentAddrIdType;
}
