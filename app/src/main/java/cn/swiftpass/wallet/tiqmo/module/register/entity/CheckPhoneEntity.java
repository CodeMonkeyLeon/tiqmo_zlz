package cn.swiftpass.wallet.tiqmo.module.register.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CheckPhoneEntity extends BaseEntity {
    //用户存在Y 不存在N
    public String userExists;
    //用户存在 userId
    public String userId;
    //用户存在时才有这个字段 新设备Y 旧设备N
    public String deviceChange;

    public String phone;
    public String checkPhoneType;
    /**
     * 修改手机号的订单号
     */
    public String changePhoneOrderNo = "";

    //1为注册流程   2为忘记密码流程 3为修改手机号流程
    public int operateType = 1;

    public String noStr = "";
    public String passwordOne,passwordTwo,oldPassword;
    public String checkIdNumber;

    public String codeType;
    public String type;
    public String codeLength;
    public RiskControlEntity riskControlInfo;
    public String birthday = "";

    /**
     * 认证url
     */
    public String authUrl = "";
    /**
     * hashState用来查询认证状态
     */
    public String hashedState;
    public String random = "";
    public long countSeconds;

    /**
     * 跳转url用来监听
     */
    public String redirectUrl = "";
    /**
     * 1跳过认证流程 0不跳过  默认为0
     */
    public String nafathAuthFlag = "0";
    public String firstName = "";

    public boolean isUserExist(){
        return "Y".equals(userExists);
    }

    public boolean isNewDevice(){
        return "Y".equals(deviceChange);
    }
}
