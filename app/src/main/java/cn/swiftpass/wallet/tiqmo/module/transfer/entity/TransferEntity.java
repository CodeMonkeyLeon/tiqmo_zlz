package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.chat.entity.TransferPersonEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ExtendEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferEntity extends BaseEntity {

    /**
     * "partnerNo": "86602",
     * "orderType": "T",
     * "orderStatus": "S",
     * "remark": "Apple",
     * "accountName": "Omar Hasson",
     * "accountNo": "Sa 42184719481491011",
     * "pspName": "Samba Bank",
     * "payerCurrencyCode": "USD",
     * "orderAmount": 10,
     * "exchangeRate": 1.000000,
     * "payMethod": "BAL",
     * "startTime": "1584327820524",
     * "transTime": "1584327820524",
     * "orderNo": "CT86601000000007061578537638423",
     * "orderCurrencyCode": "USD",
     * "payMethodDesc": "BAL",
     * "payerAmount": 10,
     * "endTime": "1584327820524",
     * "payeeRemark": "Hello,For you",
     * “transTimeType”:”1”,
     * “transFees”:570,
     * “discountTransFees”:0,
     * “vat”:300,
     * “discountVat”:0
     */

    public String partnerNo;
    public String orderType;
    public String orderStatus;
    public String remark;
    public String accountName;
    public String accountNo;
    public String pspName;
    public String payerCurrencyCode;
    public String orderAmount;
    public String exchangeRate;
    public String payMethod;
    public String startTime;
    public String popupInfo;
    public String transTime;
    public String orderNo;
    public String payerName;
    public String orderCurrencyCode;
    public String payMethodDesc;
    public String payerAmount;
    public String endTime;
    public String payeeRemark;
    public String transTimeType;
    public String transFees;
    public String discountTransFees;
    public String vat;
    public String discountVat;
    public String subPspName;
    public String cardType;
    public String cardNo;
    public String protocolNo;
    public String pspOrderNo;
    public String serviceFeeCurrencyCode;
    public String paymentMethodNo;
    public String payeeName;
    public String logo;
    public String respMsg;

    /**
     * \"payerCurrencyCode\":\"SAR\",\"orderType\":\"P\",\"partnerOrderNo\":\"PCS1a647ec4142b486587ff4228bf08a8ab\",\"openId\":\"966011595319078044fZVDM4K5xt5LmZJL4k\",\"orderStatus\":\"S\",\"startTimeStr\":\"11:56 AM, 16 Sep 2020\",\"remark\":\"\",\"merchantName\":\"Tiqmo测试商户\",\"partnerNo\":\"96601\",\"orderAmount\":500,\"exchangeRate\":1.000000,\"orderCurrencyCode\":\"SAR\",\"merchantId\":\"202004170001\",\"startTime\":1600246615926,\"serviceFee\":0,\"orderNo\":\"CS96601020000006931600246616691\",\"serviceFeeCurrencyCode\":\"SAR\",\"payMethodDesc\":\"Basic Wallet Account\",\"partnerUserId\":\"96601ccb972c66a3549d58978eae053990fea\",\"endTime\":1600246626681,\"payerAmount\":500,\"paymentMethodNo\":\"005014\
     */

    public String merchantId;
    public String merchantName;
    public String startTimeStr;
    public String outTradeNo;
    public List<SplitBillPayerEntity> receiverInfoList = new ArrayList<>();
    public List<ExtendEntity> extendProperties = new ArrayList<>();
    public List<ExtendEntity> basicExtendProperties = new ArrayList<>();
    public String orderDesc;
    public String currencyCode;
    public String payerNameRemark;
    public String createTime;
    public int sceneType;
    public int subSceneType;
    public String tradeAmount;
    public String directionDesc;
    public String orderDirection;

    //分享描述
    public String shareDesc;
    //优惠类型 1：返现 2：立减
    public String discountType;
    // 代金券的 pin 码
    public String voucherPinCode;

    public RiskControlEntity riskControlInfo;

    //是否展示 发票 按钮 1-展示；0-不展示
    public int isShowEInvoice;

    //download按钮是否需要下载pdf   1-下载；0-不下载
    public int downloadFile;

    //聊天相关
    public String note;
    public String chatId;
    public String remindStatus;
    public String payStatus;
    public String declineStatus;

    public TransferPersonEntity recentTransferPersons;

    public boolean isNeedDownloadFile() {
        return downloadFile == 1;
    }
}
