package cn.swiftpass.wallet.tiqmo.base.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;

public interface BaseTransferView <V extends BasePresenter> extends BaseView<V>{

    void transferSurePaySuccess(TransferEntity transferEntity);

    void transferSurePayFail(String errCode, String errMsg);

    void transferToContactSuccess(TransferEntity transferEntity);

    void transferToContactFail(String errCode, String errMsg);

    void confirmPaySuccess(TransferEntity transferEntity);

    void confirmPayFail(String errCode, String errMsg);

    void splitBillSuccess(TransferEntity transferEntity);

    void splitBillFail(String errorCode, String errorMsg);

    void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity);

    void imrAddBeneficiaryFail(String errorCode, String errorMsg);

    void imrEditBeneficiarySuccess(ResponseEntity result);

    void imrEditBeneficiaryFail(String errorCode, String errorMsg);

    void getKsaCardPayResultSuccess(KsaPayResultEntity result);
    void getKsaCardPayResultFail(String errorCode, String errorMsg);

    void setKsaCardStatusSuccess(Void result);
    void setKsaCardLimitSuccess(Void result);
    void getKsaCardDetailsSuccess(CardDetailEntity result);
    void activateCardSuccess(Void result);
    void showErrorMsg(String errorCode, String errorMsg);
}
