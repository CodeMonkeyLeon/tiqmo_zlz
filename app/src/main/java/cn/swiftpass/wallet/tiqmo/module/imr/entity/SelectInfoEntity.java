package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import android.text.TextUtils;

import com.github.promeg.pinyinhelper.Pinyin;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SelectInfoEntity extends BaseEntity {
    private static final String ATOZSTR = "[A-Z]";
    private static final String SPECIAL_CHAR_ONE = "#";

    public String imgUrl;
    // value
    public String businessParamValue;
    // key
    public String businessParamKey;

    private String sortLetter;

    public SelectInfoEntity() {
    }

    public SelectInfoEntity(String imgUrl, String businessParamValue) {
        this.imgUrl = imgUrl;
        this.businessParamValue = businessParamValue;
    }

    public SelectInfoEntity(String businessParamValue) {
        this.businessParamValue = businessParamValue;
    }

    public String getSortLetter() {
        return sortLetter;
    }

    public void setSortLetter(String sortLetter) {
        this.sortLetter = sortLetter;
    }

    public void setSortLetterName(String userName) {
        // 汉字转换成拼音
        if (!TextUtils.isEmpty(userName)) {
//            String pinyin = converterToFirstSpell(userName);
            String pinyin = Pinyin.toPinyin(userName, "");
//            LogUtils.i(TAG, "userName：--->" + userName + " pinyin:" + pinyin);
            if (!TextUtils.isEmpty(pinyin)) {
                String sortString = pinyin.substring(0, 1).toUpperCase();
                // 正则表达式，判断首字母是否是英文字母
                if (sortString.matches(ATOZSTR)) {
                    this.sortLetter = sortString.toUpperCase();
                } else {
                    this.sortLetter = SPECIAL_CHAR_ONE;
                }
            } else {
                this.sortLetter = SPECIAL_CHAR_ONE;
            }
        } else {
            this.sortLetter = SPECIAL_CHAR_ONE;
        }
    }
}
