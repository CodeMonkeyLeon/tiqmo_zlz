package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SpendingDateAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.CategoryEntity;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;

public class SpendingDateSelectDialog extends BottomDialog {
    private Context mContext;
    private MaxHeightRecyclerView rvDateList;

    private LinearLayoutManager mLayoutManager;
    private SpendingDateAdapter spendingDateAdapter;

    private DateItemClickListener dateItemClickListener;

    private int choosePosition;

    public void setDateItemClickListener(final DateItemClickListener dateItemClickListener) {
        this.dateItemClickListener = dateItemClickListener;
    }

    public List<CategoryEntity> dateList = new ArrayList<>();

    public void setDateList(final List<CategoryEntity> dateList, int choosePosition) {
        this.dateList = dateList;
        this.choosePosition = choosePosition;
    }

    public SpendingDateSelectDialog(Context context,int chooseDatePosition) {
        super(context);
        this.mContext = context;
        this.choosePosition = chooseDatePosition;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_spending_date_select, null);
        RadioGroup rgDate = view.findViewById(R.id.rg_date);
        RadioButton rbYear = view.findViewById(R.id.rb_year);
        RadioButton rbMonth = view.findViewById(R.id.rb_month);
        RadioButton rbWeek = view.findViewById(R.id.rb_week);
        rbMonth.setChecked(true);
        switch (choosePosition){
            case 0:
                rbWeek.setChecked(true);
                break;
            case 1:
                rbMonth.setChecked(true);
                break;
            case 2:
                rbYear.setChecked(true);
                break;
            default:break;
        }

        rbWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dateItemClickListener != null){
                    dateItemClickListener.chooseDate(0);
                }
            }
        });

        rbMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dateItemClickListener != null){
                    dateItemClickListener.chooseDate(1);
                }
            }
        });

        rbYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dateItemClickListener != null){
                    dateItemClickListener.chooseDate(2);
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    public interface DateItemClickListener {
        void chooseDate(int position);
    }
}
