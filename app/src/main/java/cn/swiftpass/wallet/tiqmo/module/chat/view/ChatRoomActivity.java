package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gc.gcchat.GcChatGetSessionsRequest;
import com.gc.gcchat.GcChatMessage;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.gc.gcchat.GcChatSpamMessage;
import com.gc.gcchat.GcChatUser;
import com.gc.gcchat.callback.GcChatEventListener;
import com.gc.gcchat.callback.GcChatSimpleCallback;
import com.gc.gcchat.callback.GetChatMessageListCallback;
import com.gc.gcchat.callback.GetSessionListCallback;
import com.gc.gcchat.callback.SendMessageCallback;
import com.gc.gcchat.callback.UploadFileCallback;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.ChatDeletePopupWindow;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.MessageAdapter;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.MessageLongPressListener;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatSendMoneyDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChooseChatTypeDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatMessage;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatSendMoneyEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.AppUtility;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.MessageUtils;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.RequestCenterActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.RequestTransferActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferMoneyActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.GcChatErrorUtils;
import cn.swiftpass.wallet.tiqmo.support.chat.UserUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.CompressImageUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.FilePickerUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class ChatRoomActivity extends BaseCompatActivity{

    private static final int REQUEST_STORAGE_PERMISSION = 101;
    private static final int REQUEST_PICK_GENERAL = 102;

    private static final int PERMISSION_REQUEST_CODE_CAMERA = ActivityHelper.generateRequestCode();

    private static final int REQUEST_CODE_CAMERA = ActivityHelper.generateRequestCode();
    private static final int REQUEST_CODE_ALBUM = ActivityHelper.generateRequestCode();
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.tv_name)
    MyTextView tvName;
    @BindView(R.id.ll_chat_top)
    ConstraintLayout llChatTop;
    @BindView(R.id.rl_chat_message)
    RecyclerView rlChatMessage;
    @BindView(R.id.iv_add_res)
    ImageView ivAddRes;
    @BindView(R.id.et_chat_message)
    EditText etChatMessage;
    @BindView(R.id.iv_chat_edit)
    ImageView ivChatEdit;
    @BindView(R.id.iv_chat_photo)
    ImageView ivChatPhoto;
    @BindView(R.id.iv_chat_send)
    ImageView ivChatSend;
    @BindView(R.id.iv_chat_voice)
    ImageView ivChatVoice;
    @BindView(R.id.ll_chat_bottom)
    LinearLayout llChatBottom;
    @BindView(R.id.con_reply_message)
    ConstraintLayout conReplyMessage;
    @BindView(R.id.tv_reply_name)
    TextView tvReplyName;
    @BindView(R.id.tv_reply_message)
    TextView tvReplyMessage;
    @BindView(R.id.iv_reply_delete)
    ImageView ivReplyDelete;
    @BindView(R.id.iv_reply_image)
    ImageView ivReplyImage;
    @BindView(R.id.con_chat_room)
    ConstraintLayout conChatRoom;
    @BindView(R.id.iv_user_status)
    ImageView ivUserStatus;
    private boolean isSessionBlockedByUser = false;
    private GcChatEventListener gcChatEventListener = null;
    private String myParticipantId = "";
    private boolean addedEventListener = false;
    public static boolean refreshOnResume = false;
    public static boolean finishOnResume = false;
    private MessageAdapter messageAdapter;
    private List<MessageEntity> messageList = new ArrayList<>();

    private Uri mImageSaveUri;

    //快速回复的消息
    private MessageEntity quickRepliedMessage;

    private ChooseChatTypeDialog chooseChatTypeDialog;

    private GcChatSession gcChatSession;
    private LinearLayoutManager layoutManager;

    private KeyboardOnGlobalChangeListener keyboardOnGlobalChangeListener;

    private boolean isShowUp;
    /**
     * 交易相关
     */
    private String transferType;

    private MessageEntity sendMoneyMessage;

    /**
     * 是否是群聊
     */
    private boolean isGroupChat = false;

    private TransferEntity mTransferEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_chat_room;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        showProgressCancelable(true,true);
        LocaleUtils.viewRotationY(mContext,ivBack);
        if(getIntent() != null && getIntent().getExtras() != null) {
            gcChatSession = (GcChatSession) getIntent().getSerializableExtra(Constants.gcChatSession);
            String sessionId = getIntent().getStringExtra("chatSessionId");
            isGroupChat = getIntent().getBooleanExtra("isGroupChat",false);
            if(gcChatSession != null) {
                isGroupChat = gcChatSession.isGroupChat();
                sessionId = gcChatSession.getSessionId();
            }
            updateProfilePicture();
            loadCurrentSessionDetails(sessionId);
        }
        keyboardOnGlobalChangeListener = new KeyboardOnGlobalChangeListener();
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(keyboardOnGlobalChangeListener);//监听软键盘弹出

        layoutManager = new LinearLayoutManager(mContext,RecyclerView.VERTICAL,false);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 0));
        rlChatMessage.addItemDecoration(myItemDecoration);
        rlChatMessage.setLayoutManager(layoutManager);
        messageAdapter = new MessageAdapter(messageList);
        messageAdapter.bindToRecyclerView(rlChatMessage);

        messageAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                MessageEntity messageEntity = (MessageEntity) baseRecyclerAdapter.getDataList().get(position);
                if(messageEntity == null)return;
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                String orderNo = messageEntity.getChatTransferEntity().transferMoney.orderNo;
                switch (view.getId()){
                    case R.id.tv_send_show_details:
                        showProgressCancelable(true,true);
                        AppClient.getInstance().getTransferManager().getTransferDetail(orderNo, "","", new ResultCallback<TransferHistoryDetailEntity>() {
                            @Override
                            public void onResult(TransferHistoryDetailEntity transferHistoryDetailEntity) {
                                showProgress(false);
                                AndroidUtils.jumpToTransferDetail(ChatRoomActivity.this, transferHistoryDetailEntity, "", true);
                            }

                            @Override
                            public void onFailure(String errorCode, String errorMsg) {
                                showProgress(false);
                                showTipDialog(errorMsg);
                            }
                        });
                        break;
                    case R.id.tv_left:
                        if (!TextUtils.isEmpty(orderNo)) {
                            NotifyEntity notifyEntity = new NotifyEntity();
                            notifyEntity.msgSubject = "TRANSFER";
                            notifyEntity.msgResult = "NOTIFY";
                            notifyEntity.orderNo = orderNo;
                            List<NotifyEntity> receivers = new ArrayList<>();
                            receivers.add(notifyEntity);
                            showProgressCancelable(true,true);
                            AppClient.getInstance().sendNotify(receivers, new ResultCallback<PayBillOrderInfoEntity>() {
                                @Override
                                public void onResult(PayBillOrderInfoEntity response) {
                                    showProgress(false);
//                                    GcChatSDK.updateMessageMetaInfo();
                                }

                                @Override
                                public void onFailure(String errorCode, String errorMsg) {
                                    showProgress(false);
                                    showTipDialog(errorMsg);
                                }
                            });
                        }
                        break;
                    default:break;
                }
            }
        });
        messageAdapter.setmOnItemLongClickListener(new BaseRecyclerAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, int position) {
                try {
                    //屏幕中最后一个可见子项的position
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    MessageEntity messageEntity = messageList.get(position);
                    if (messageEntity == null) return;
                    if (messageEntity.getMessageType() == MessageEntity.MessageType.DateFloater)
                        return;
                    ChatDeletePopupWindow chatDeletePopupWindow = new ChatDeletePopupWindow(mContext, messageEntity);
                    chatDeletePopupWindow.setOnChatDeleteClickListener(new ChatDeletePopupWindow.OnChatDeleteClickListener() {
                        @Override
                        public void onChatReply(MessageEntity messageEntity) {
                            chatDeletePopupWindow.dismiss();
                            conReplyMessage.setVisibility(View.VISIBLE);
                            setReplyMessage(messageEntity);
                        }

                        @Override
                        public void onChatDeleteMe(MessageEntity messageEntity) {
                            chatDeletePopupWindow.dismiss();
                            showChatConfirmDialog("",getString(R.string.common_10), getString(R.string.common_9),
                                    getString(R.string.sprint20_60), new ChatConfirmDialog.ConfirmListener() {
                                        @Override
                                        public void clickLeft() {
                                            dissmissChatConfirmDialog();
                                        }

                                        @Override
                                        public void clickRight() {
                                            dissmissChatConfirmDialog();
                                            GcChatSDK.deleteMessageForMe(messageEntity.getId(), new GcChatSimpleCallback() {
                                                @Override
                                                public void onSuccessful() {
                                                    removeMessage(messageEntity);
                                                }

                                                @Override
                                                public void onFailure(int errorCode) {
                                                    showTipDialog("Error in deleting message - " + GcChatErrorUtils.getErrorMessage(errorCode));
                                                }
                                            });
                                        }
                                    });
                        }

                        @Override
                        public void onChatDeleteOther(MessageEntity messageEntity) {
                            chatDeletePopupWindow.dismiss();
                            showChatConfirmDialog("",getString(R.string.common_10), getString(R.string.common_9),
                                    getString(R.string.sprint20_60), new ChatConfirmDialog.ConfirmListener() {
                                        @Override
                                        public void clickLeft() {
                                            dissmissChatConfirmDialog();
                                        }

                                        @Override
                                        public void clickRight() {
                                            dissmissChatConfirmDialog();
                                            switch (messageEntity.getMessageType()) {
                                                case LeftSimpleMessage:
                                                case LeftSingleImage:
                                                case LeftFileImage:
                                                    GcChatSDK.deleteMessageForMe(messageEntity.getId(), new GcChatSimpleCallback() {
                                                        @Override
                                                        public void onSuccessful() {
                                                            removeMessage(messageEntity);
                                                        }

                                                        @Override
                                                        public void onFailure(int errorCode) {
                                                            showTipDialog("Error in deleting message - " + GcChatErrorUtils.getErrorMessage(errorCode));
                                                        }
                                                    });
                                                    break;
                                                default:
                                                    deleteMessage(messageEntity);
                                                    break;
                                            }
                                        }
                                    });
                        }

                        @Override
                        public void onChatReport(MessageEntity messageEntity) {
                            chatDeletePopupWindow.dismiss();
                            showProgressCancelable(true,true);
                            GcChatSpamMessage gcChatSpamMessage = new GcChatSpamMessage();
                            gcChatSpamMessage.setMessageId(messageEntity.getId());
                            gcChatSpamMessage.setAttachments(new ArrayList<>(Collections.singletonList(messageEntity.getContent())));
                            gcChatSpamMessage.setText("");
                            gcChatSpamMessage.setSentTime(messageEntity.getTimestamp());
                            List<GcChatSpamMessage> lastMessages = new ArrayList<>();
                            lastMessages.add(gcChatSpamMessage);
                            GcChatSDK.reportSpam(gcChatSession.getSessionId(),
                                    "Spam",
                                    "Spam",
                                    lastMessages,
                                    new GcChatSimpleCallback() {
                                        @Override
                                        public void onSuccessful() {
                                            showProgress(false);
                                            showChatConfirmDialog(getString(R.string.sprint20_65),"", getString(R.string.common_7),
                                                    getString(R.string.sprint20_66), new ChatConfirmDialog.ConfirmListener() {
                                                        @Override
                                                        public void clickLeft() {
                                                            dissmissChatConfirmDialog();
                                                        }

                                                        @Override
                                                        public void clickRight() {
                                                            dissmissChatConfirmDialog();
                                                        }
                                                    });
                                        }

                                        @Override
                                        public void onFailure(int i) {
                                            showProgress(false);
                                            showTipDialog("Error in reporting spam - " + GcChatErrorUtils.getErrorMessage(i));
                                        }
                                    });
                        }
                    });
                    View windowContentViewRoot = chatDeletePopupWindow.getView();
                    if(windowContentViewRoot == null){
                        windowContentViewRoot = view;
                    }
                    int windowPos[] = calculatePopWindowPos(view, windowContentViewRoot);
                    int xOff = 20;// 可以自己调整偏移
                    windowPos[0] -= xOff;
                    switch (messageEntity.getMessageType()) {
                        case LeftSimpleMessage:
                        case LeftSingleImage:
                        case LeftFileImage:
                        case LeftTransferMessage:
                            if (isShowUp) {
                                chatDeletePopupWindow.showAtLocation(view, Gravity.BOTTOM | Gravity.START, 0, 0);
                            } else {
                                chatDeletePopupWindow.showAtLocation(view, Gravity.TOP | Gravity.START, 0, windowPos[1]);
                            }
                            break;
                        case RightSimpleMessage:
                        case RightSingleImage:
                        case RightFileImage:
                        case RightTransferMessage:
                            if (isShowUp) {
                                chatDeletePopupWindow.showAtLocation(view, Gravity.BOTTOM | Gravity.END, 0, 0);
                            }else{
                                chatDeletePopupWindow.showAtLocation(view, Gravity.TOP | Gravity.END, 0, windowPos[1]);
                            }
                            break;
                        default:
                            chatDeletePopupWindow.showAsDropDown(view);
                            break;
                    }
                }catch (Throwable e){
                    e.printStackTrace();
                }
            }
        });

        etChatMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if(!TextUtils.isEmpty(s)){
                    ivChatPhoto.setVisibility(View.GONE);
                    ivChatSend.setVisibility(View.VISIBLE);
                }else{
                    ivChatPhoto.setVisibility(View.VISIBLE);
                    ivChatSend.setVisibility(View.GONE);
                }
            }
        });

        if (gcChatEventListener == null) {
            gcChatEventListener = new GcChatEventListener() {
                @Override
                public void onNewMessageReceived(@NonNull GcChatMessage chatMessage) {
                    LogUtils.d("onNewMessageReceived for %s and %s", chatMessage.getSessionId() + " " + new Gson().toJson(chatMessage));
                    if (gcChatSession != null && chatMessage.getSessionId().equals(gcChatSession.getSessionId())) {
                        MessageEntity message = getMessage(chatMessage, gcChatSession.isGroupChat());
                        addMessage(message);
                        if (!chatMessage.getSender().getExternalId().equals(UserUtils.loggedInUserId)) {
                            markMessageRead(chatMessage.getMessageId());
                        }
                    }
                }

                @Override
                public void onMessageUpdated(@NonNull GcChatMessage chatMessage) {
                    LogUtils.d("onMessageUpdated %s", new Gson().toJson(chatMessage));
                    MessageEntity oldMessageToUpdate = null;
                    if (gcChatSession != null && chatMessage.getSessionId().equals(gcChatSession.getSessionId())) {
                        if (messageList != null) {
                            for (MessageEntity message : messageList) {
                                if (chatMessage.getMessageId().equals(message.getId())) {
                                    oldMessageToUpdate = message;
                                    break;
                                }
                            }
                        }
                        if (oldMessageToUpdate != null) {
                            oldMessageToUpdate.setMetaInfo(chatMessage.getMessageMetadata());
                            messageAdapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onMessageDeleted(String messageId) {
                    LogUtils.d("onMessageDeleted %s", messageId);
                    MessageEntity oldMessageToDelete = null;
                    if (messageList != null) {
                        for (MessageEntity message : messageList) {
                            if (messageId.equals(message.getId())) {
                                oldMessageToDelete = message;
                                break;
                            }
                        }
                    }
                    if (oldMessageToDelete != null) {
                        removeMessage(oldMessageToDelete);
                    }
                }

                @Override
                public void onInvitationResponded(boolean accepted, @NonNull String sessionId, @NonNull GcChatUser respondedByUser, @NonNull GcChatUser invitedByUser, @Nullable String groupName) {
                    if (gcChatSession != null && gcChatSession.getSessionId().equals(sessionId)) {
                        GcChatParticipant participantToRemove = null;
                        for (GcChatParticipant participant : gcChatSession.getParticipantUsers()) {
                            if (respondedByUser.getExternalId().equals(participant.getUser().getExternalId())) {
                                if (accepted) {
                                    participant.setInvited(false);
                                } else {
                                    participantToRemove = participant;
                                }
                            }
                        }
                        gcChatSession.getParticipantUsers().remove(participantToRemove);
//                        onSessionInfoUpdated();
                    }
                    if (invitedByUser.getExternalId().equals(UserUtils.loggedInUserId)) {
                        String messageText = "";
                        if (accepted) {
                            messageText = "Your invitation was accepted by " + respondedByUser.getName();
                        } else {
                            messageText = "Your invitation was rejected by " + respondedByUser.getName();
                        }
                        if (groupName != null) {
                            messageText += " for " + groupName + " group";
                        }
                        Toast.makeText(ChatRoomActivity.this, messageText, Toast.LENGTH_SHORT).show();
                        if (!accepted && !gcChatSession.isGroupChat()) {
                            //one to one session so destroy chat
                            finish();
                        }
                    } else {
                        String messageText = "";
                        if (accepted) {
                            messageText = invitedByUser.getName() + " added  " + respondedByUser.getName();
                            if (groupName != null) {
                                messageText += " in " + groupName + " group";
                            }
                            Toast.makeText(ChatRoomActivity.this, messageText, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onUserLeftGroup(@NonNull GcChatUser leftUser, @NonNull String sessionId, @NonNull String groupName) {
                    if (gcChatSession != null && gcChatSession.getSessionId().equals(sessionId)) {
                        GcChatParticipant participantToRemoved = null;
                        for (GcChatParticipant gcChatParticipant : gcChatSession.getParticipantUsers()) {
                            if (gcChatParticipant.getUser().getExternalId().equals(leftUser.getExternalId())) {
                                participantToRemoved = gcChatParticipant;
                            }
                        }
                        if (participantToRemoved != null) {
                            gcChatSession.getParticipantUsers().remove(participantToRemoved);
                        }
                    }
                    Toast.makeText(ChatRoomActivity.this, leftUser.getName() + " left " + groupName, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onUserRemovedFromGroup(@NonNull GcChatUser removedUser, @NonNull GcChatUser removedByUser, @NonNull String sessionId, @NonNull String groupName) {
                    if (gcChatSession != null && gcChatSession.getSessionId().equals(sessionId)) {
                        GcChatParticipant participantToRemoved = null;
                        for (GcChatParticipant gcChatParticipant : gcChatSession.getParticipantUsers()) {
                            if (gcChatParticipant.getUser().getExternalId().equals(removedUser.getExternalId())) {
                                participantToRemoved = gcChatParticipant;
                            }
                        }
                        if (participantToRemoved != null) {
                            gcChatSession.getParticipantUsers().remove(participantToRemoved);
                        }
                        if (removedUser.getExternalId().equals(UserUtils.loggedInUserId)) {
                            finish();
                        }
                    }
                    if (removedUser.getExternalId().equals(UserUtils.loggedInUserId)) {
                        Toast.makeText(ChatRoomActivity.this, removedByUser.getName() + " removed you from " + groupName, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChatRoomActivity.this, removedByUser.getName() + " removed " + removedUser.getName() + " from " + groupName, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onUserInfoUpdated(@NonNull String externalId, @Nullable String name, @Nullable String imageId) {
                    for (GcChatParticipant gcChatParticipant: gcChatSession.getParticipantUsers()){
                        if(gcChatParticipant.getUser().getExternalId().equals(externalId)){
                            if(name != null){
                                gcChatParticipant.getUser().setName(name);
                            }
                            if(imageId != null){
                                gcChatParticipant.getUser().setFileId(imageId);
                            }
                        }
                    }
                    onSessionInfoUpdated();
                }

                @Override
                public void onGroupSessionInfoUpdated(@NonNull String s, @Nullable String s1, @Nullable String s2) {

                }

                @Override
                public void onNewChatInvitation(String s, GcChatUser gcChatUser, @Nullable String groupName) {
                    String messageText = "You got an invitation to join chat from " + gcChatUser.getName();
                    if (groupName != null) {
                        messageText += " for " + groupName + " group";
                    }
                    Toast.makeText(ChatRoomActivity.this, messageText, Toast.LENGTH_SHORT).show();
                }
            };
            if (!addedEventListener) {
                addedEventListener = true;
                GcChatSDK.addChatEventListener(gcChatEventListener);
            }
        }
    }

    /**
     * 计算出来的位置，y方向就在anchorView的上面和下面对齐显示，x方向就是与屏幕右边对齐显示
     * 如果anchorView的位置有变化，就可以适当自己额外加入偏移来修正
     * @param anchorView 呼出window的view
     * @param contentView  window的内容布局
     * @return window显示的左上角的xOff,yOff坐标
     */
    private int[] calculatePopWindowPos(final View anchorView, final View contentView) {
        final int windowPos[] = new int[2];
        try {
            final int anchorLoc[] = new int[2];
            // 获取锚点View在屏幕上的左上角坐标位置
            anchorView.getLocationOnScreen(anchorLoc);
            final int anchorHeight = anchorView.getHeight();
            // 获取屏幕的高宽
            final int screenHeight = AndroidUtils.getScreenHeight(anchorView.getContext());
            final int screenWidth = AndroidUtils.getScreenWidth(anchorView.getContext());
            contentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            // 计算contentView的高宽
            final int windowHeight = contentView.getMeasuredHeight();
            final int windowWidth = contentView.getMeasuredWidth();
            // 判断需要向上弹出还是向下弹出显示
            final boolean isNeedShowUp = (screenHeight - anchorLoc[1] - anchorHeight < windowHeight);
            if (isNeedShowUp) {
                windowPos[0] = screenWidth - windowWidth;
                windowPos[1] = anchorLoc[1] - windowHeight;
                isShowUp = true;
            } else {
                windowPos[0] = screenWidth - windowWidth;
                windowPos[1] = anchorLoc[1] + anchorHeight;
                isShowUp = false;
            }
        }catch (Throwable e){
            e.printStackTrace();
        }
        return windowPos;
    }

    /**
     * 获取一个视图的宽高（软键盘）
     */
    private boolean isShow;

    private class KeyboardOnGlobalChangeListener implements ViewTreeObserver.OnGlobalLayoutListener {

        private int getScreenHeight() {
            return ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
        }

        @Override
        public void onGlobalLayout() {
            Rect rect = new Rect();
            getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            int screenHeight = getScreenHeight();
            int keyboardHeight = screenHeight - rect.bottom;//软键盘高度
            if (Math.abs(keyboardHeight) > screenHeight / 5 && !isShow) {
                setScrollBottom();
                isShow = true;
            } else {
                isShow = false;
            }
        }
    }

    private void setScrollBottom() {

        rlChatMessage.post(() -> {
            rlChatMessage.scrollToPosition(messageAdapter.getItemCount() - 1);
//            View target = layoutManager.findViewByPosition(messageAdapter.getItemCount() - 1);
//            if (target != null) {
//                // int offset=  recyclerView.getMeasuredHeight() - target.getMeasuredHeight();
//                layoutManager.scrollToPositionWithOffset(messageAdapter.getItemCount() - 1, Integer.MAX_VALUE);//滚动偏移到底部
//            }
        });
    }

    //显示回复信息布局
    private void setReplyMessage(MessageEntity messageEntity) {
        if(messageEntity != null) {
            quickRepliedMessage = messageEntity;
            tvReplyMessage.setText(messageEntity.getSimpleMessageToDisplay(""));
            tvReplyName.setText(messageEntity.getUser().getName());
            if (messageEntity.getGlideUrl() != null || messageEntity.getStrUrl() != null) {
                Glide.with(mContext).load(messageEntity.getGlideUrl()).into(ivReplyImage);
            } else {
                Glide.with(mContext).clear(ivReplyImage);
            }
            etChatMessage.setFocusable(true);
            AndroidUtils.showKeyboard(this, etChatMessage);
        }else{
            conReplyMessage.setVisibility(View.GONE);
            quickRepliedMessage = null;
        }
    }

    private void deleteMessage(MessageEntity messageEntity) {
        GcChatSDK.deleteMessage(messageEntity.getId(), new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                removeMessage(messageEntity);
//                showTipDialog("Message deleted successfully!!");
            }

            @Override
            public void onFailure(int errorCode) {
                showTipDialog("Error in deleting message - " + GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }

    private void markMessageRead(String messageId) {
        GcChatSDK.markMessageRead(messageId, new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                LogUtils.d("onSuccessful marked message %s as read", messageId);
            }

            @Override
            public void onFailure(int errorCode) {
                LogUtils.d("onFailure marked message %s as read %s", messageId + "  " + errorCode);
            }
        });
    }

    private MessageEntity getMessage(GcChatMessage chatMessage, boolean isGroupChat) {
        return MessageUtils.getMessage(chatMessage, UserUtils.loggedInUserId, chatMessage.getMessageMetadata(), isGroupChat);
    }

    public void addMessage(MessageEntity message, Boolean shouldScrollToBottom, Boolean notifyAdapter) {
        int earlierItemSize = messageList.size();
        if (messageList.isEmpty()) {
            MessageEntity timeMsg = new MessageEntity();
            timeMsg.setMessageType(MessageEntity.MessageType.DateFloater);
            timeMsg.setTimestamp(message.getTimestamp());
            messageList.add(0, timeMsg);
        } else {
            MessageEntity msg = messageList.get(0);
            long lastKnownDate = msg.getTimestamp();
            if (!AppUtility.isSameDay(lastKnownDate, message.getTimestamp())) {
                MessageEntity timeMsg = new MessageEntity();
                timeMsg.setMessageType(MessageEntity.MessageType.DateFloater);
                timeMsg.setTimestamp(message.getTimestamp());
                messageList.add(0, timeMsg);
            }
        }
//        messageList.add(0, message);
        messageList.add(message);
        if (notifyAdapter) {
            int itemInserted = messageList.size() - earlierItemSize;
            if (itemInserted == 1) {
                messageAdapter.notifyItemInserted(messageList.size()-1);
            } else {
                messageAdapter.notifyItemRangeInserted(0, messageList.size() + itemInserted);
            }
        }
        if (shouldScrollToBottom) {
            rlChatMessage.scrollToPosition(messageList.size()-1);
        }
    }

    public void addMessageList(List<MessageEntity> messagesList) {
        for (int i = 0; i < messagesList.size(); i++) {
            addMessage(messagesList.get(i), i == messagesList.size() - 1, i == messagesList.size() - 1);
        }
    }

    public void addMessage(MessageEntity messageEntity) {
        addMessage(messageEntity, true, true);
    }

    public void removeMessage(MessageEntity messageEntity) {
        int position = messageList.indexOf(messageEntity);
        if (position >= 0) {
            messageList.remove(position);
            messageAdapter.notifyItemRemoved(position);
        }
    }

    public void clearAllMessages() {
        messageList.clear();
        messageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_BLOCK_CHAT_USER == event.getEventType()) {
            gcChatSession = event.getGcChatSession();
            setUserStatus();
        }else if(EventEntity.EVENT_CHAT_SEND_MONEY_SUCCESS == event.getEventType()){
            TransferEntity transferEntity = event.getTransferEntity();
            if(transferEntity != null) {
                ChatTransferEntity chatTransferEntity = new ChatTransferEntity();
                ChatSendMoneyEntity chatSendMoneyEntity = ChatSendMoneyEntity.getChatSendEntity(transferEntity);
                chatTransferEntity.text = transferEntity.note;
                chatTransferEntity.transferMoney = chatSendMoneyEntity;
                sendTransferMessage(gcChatSession.getSessionId(),chatTransferEntity);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
        if (addedEventListener) {
            addedEventListener = false;
            GcChatSDK.removeChatEventListener(gcChatEventListener);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gcChatSession != null && refreshOnResume) {
            refreshOnResume = false;
            loadCurrentSessionDetails(gcChatSession.getSessionId());
        }
        if (finishOnResume) {
            finishOnResume = false;
            finish();
        }
        if (gcChatEventListener != null && !addedEventListener) {
            addedEventListener = true;
            GcChatSDK.addChatEventListener(gcChatEventListener);
        }
    }

    private void unblockSession(GcChatSession gcChatSession) {
        showProgressCancelable(true,true);
        GcChatSDK.unblockSession(gcChatSession.getSessionId(), new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                showProgress(false);
                gcChatSession.setBlocked(false);
                setUserStatus();
            }

            @Override
            public void onFailure(int i) {
                showTipDialog("Error in unblock session - " + GcChatErrorUtils.getErrorMessage(i));
                showProgress(false);
            }
        });
    }

    @OnClick({R.id.iv_back, R.id.iv_add_res, R.id.iv_chat_edit, R.id.iv_chat_photo, R.id.iv_chat_send,
            R.id.iv_chat_voice,R.id.iv_reply_delete,R.id.iv_avatar,R.id.tv_name})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_avatar:
            case R.id.tv_name:
                HashMap<String,Object> hashMap = new HashMap<>();
                hashMap.put(Constants.gcChatSession, gcChatSession);
                if(gcChatSession.isGroupChat()){
                    ActivitySkipUtil.startAnotherActivity(this, EditChatGroupActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else{
                    ActivitySkipUtil.startAnotherActivity(this,EditChatUserActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.iv_reply_delete:
                setReplyMessage(null);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_add_res:
                showChooseTypeDialog();
                break;
            case R.id.iv_chat_edit:

                break;
            case R.id.iv_chat_send:
                if(gcChatSession.isBlocked()){
                    showChatConfirmDialog(getString(R.string.sprint20_43), getString(R.string.common_6), getString(R.string.sprint20_113),
                            getString(R.string.sprint20_44), new ChatConfirmDialog.ConfirmListener() {
                                @Override
                                public void clickLeft() {
                                    dissmissChatConfirmDialog();
                                }

                                @Override
                                public void clickRight() {
                                    dissmissChatConfirmDialog();
                                    unblockSession(gcChatSession);
//                                    HashMap<String,Object> hashMap = new HashMap<>();
//                                    hashMap.put(Constants.gcChatSession, gcChatSession);
//                                    ActivitySkipUtil.startAnotherActivity(ChatRoomActivity.this,EditChatUserActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }
                            });
                }else {
                    sendMessage(gcChatSession.getSessionId(), etChatMessage.getText().toString());
                }
                break;
            case R.id.iv_chat_photo:
                PermissionInstance.getInstance().getPermission(ChatRoomActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        openCameraCapture();
                    }

                    @Override
                    public void rejectPermission() {
                        showChatConfirmDialog("",getString(R.string.common_10), getString(R.string.common_9),
                                getString(R.string.sprint20_70), new ChatConfirmDialog.ConfirmListener() {
                                    @Override
                                    public void clickLeft() {
                                        dissmissChatConfirmDialog();
                                    }

                                    @Override
                                    public void clickRight() {
                                        dissmissChatConfirmDialog();
                                        AndroidUtils.startAppSetting(mContext);
                                    }
                                });
                    }
                }, PermissionInstance.getPermissons(1));
                break;
            case R.id.iv_chat_voice:
                break;
            default:break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        dismissChooseTypeDialog();
        try {
            if (data != null && requestCode == REQUEST_CODE_ALBUM) {//从相册回来
                File file = CompressImageUtils.getSmallBitmap(FilePickerUtils.getFileFromResult(ChatRoomActivity.this, data));
                if (file != null) {
                    if (file.getAbsolutePath().endsWith(".png")
                            || file.getAbsolutePath().endsWith(".jpeg")
                            || file.getAbsolutePath().endsWith(".jpg")
                            || file.getAbsolutePath().endsWith(".pdf")
                    ) {
                        //upload file get file id and send message
                        showProgressCancelable(true, true);
                        GcChatSDK.uploadFile(file, new UploadFileCallback() {
                            @Override
                            public void onSuccessful(String s) {
                                String type = "";
                                if (file.getAbsolutePath().toLowerCase().endsWith(".png")) {
                                    type = FileType.IMAGE_PNG;
                                } else if (file.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
                                    type = FileType.PDF;
                                } else {
                                    type = FileType.IMAGE_JPEG;
                                }
                                sendMessage(gcChatSession.getSessionId(), s, file.length(), type, file.getName());
                            }

                            @Override
                            public void onFailure(int errorCode) {
                                showProgress(false);
                                showTipDialog("Error in upload image!! - " + GcChatErrorUtils.getErrorMessage(errorCode));
                            }
                        });
                    } else {
                        showTipDialog("Invalid file format!!");
                    }
                }
            } else if (requestCode == REQUEST_CODE_CAMERA) {//从相机回来
                if (resultCode == Activity.RESULT_OK) {
                    File file = AndroidUtils.getFileFromUriApiQ(mContext, mImageSaveUri);
                    if (file != null) {
                        if (file.getAbsolutePath().endsWith(".png")
                                || file.getAbsolutePath().endsWith(".jpeg")
                                || file.getAbsolutePath().endsWith(".jpg")
                                || file.getAbsolutePath().endsWith(".pdf")
                        ) {
                            //upload file get file id and send message
                            showProgressCancelable(true, true);
                            GcChatSDK.uploadFile(file, new UploadFileCallback() {
                                @Override
                                public void onSuccessful(String s) {
                                    String type = "";
                                    if (file.getAbsolutePath().toLowerCase().endsWith(".png")) {
                                        type = FileType.IMAGE_PNG;
                                    } else if (file.getAbsolutePath().toLowerCase().endsWith(".pdf")) {
                                        type = FileType.PDF;
                                    } else {
                                        type = FileType.IMAGE_JPEG;
                                    }
                                    sendMessage(gcChatSession.getSessionId(), s, file.length(), type, file.getName());
                                }

                                @Override
                                public void onFailure(int errorCode) {
                                    showProgress(false);
                                    showTipDialog("Error in upload image!! - " + GcChatErrorUtils.getErrorMessage(errorCode));
                                }
                            });
                        } else {
                            showTipDialog("Invalid file format!!");
                        }
                    }
                } else {
                    captureFailure();
                }
            }
        }catch (Throwable e){
            e.printStackTrace();
        }
    }

    public void setMessageLongPressCallback(MessageLongPressListener callback) {
        if (messageAdapter == null) return;
        messageAdapter.messageLongPressListener = callback;
    }

    private void captureFailure() {
        //如果取消拍照，就删除提前保存的uri
        getContentResolver().delete(mImageSaveUri, null, null);
    }

    private void openCameraCapture() {
        //打开相机拍照，指定文件名字
        try {
            String fileName = String.format(Locale.getDefault(), "capture_%d%s", System.currentTimeMillis() / 1000, ".jpg");
            ContentValues values = new ContentValues();
            long now = System.currentTimeMillis() / 1000;
            values.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, fileName);
            values.put(MediaStore.Images.ImageColumns.MIME_TYPE, "image/jpg");
            values.put(MediaStore.Images.ImageColumns.DATE_ADDED, now);
            values.put(MediaStore.Images.ImageColumns.DATE_MODIFIED, now);
            values.put(MediaStore.Images.ImageColumns.DATE_TAKEN, now);
            mImageSaveUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageSaveUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void showSendMoneyDialog(TransferLimitEntity transferLimitEntity){
        ChatSendMoneyDialog chatSendMoneyDialog = new ChatSendMoneyDialog(mContext,transferLimitEntity,1);
        chatSendMoneyDialog.setConfirmListener(new ChatSendMoneyDialog.ConfirmListener() {
            @Override
            public void clickConfirm(ChatTransferEntity chatTransferEntity) {
//                if(chatTransferEntity != null){
//                    chatTransferEntity.chatId = gcChatSession.getSessionId();
//                    chatTransferEntity.payeeName = MessageUtils.getChatSessionName(gcChatSession);
//                    chatTransferEntity.phoneNumber = MessageUtils.getChatPhoneNumber(gcChatSession);
//                    chatTransferEntity.payeeUserId = MessageUtils.getChatUserId(gcChatSession);
//                    chatTransferEntity.chatFileId = MessageUtils.getChatSessionDisplayPic(gcChatSession);
//                    HashMap<String,Object> mHashMap = new HashMap<>();
//                    mHashMap.put(Constants.chatTransferEntity,chatTransferEntity);
//                    ActivitySkipUtil.startAnotherActivity(ChatRoomActivity.this, ChatConfirmPayActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
//                }

            }

            @Override
            public void clickCancel() {

            }
        });
        chatSendMoneyDialog.showWithBottomAnim();
    }

    private void dismissChooseTypeDialog(){
        if(chooseChatTypeDialog != null){
            chooseChatTypeDialog.dismiss();
        }
    }
    private void showChooseTypeDialog(){
        chooseChatTypeDialog = new ChooseChatTypeDialog(mContext);
        chooseChatTypeDialog.setConfirmListener(new ChooseChatTypeDialog.ConfirmListener() {
            @Override
            public void clickSendMoney() {
                transferType = Constants.TYPE_TRANSFER_WW;
                transferToChat();
            }

            @Override
            public void clickRequestMoney() {
                transferType = Constants.TYPE_TRANSFER_RT;
                transferToChat();
            }

            @Override
            public void clickChoosePhoto() {
                PermissionInstance.getInstance().getPermission(ChatRoomActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(intent, REQUEST_CODE_ALBUM);
                    }

                    @Override
                    public void rejectPermission() {
                        showChatConfirmDialog("",getString(R.string.common_10), getString(R.string.common_9),
                                getString(R.string.sprint20_70), new ChatConfirmDialog.ConfirmListener() {
                                    @Override
                                    public void clickLeft() {
                                        dissmissChatConfirmDialog();
                                    }

                                    @Override
                                    public void clickRight() {
                                        dissmissChatConfirmDialog();
                                        AndroidUtils.startAppSetting(mContext);
                                    }
                                });
                    }
                }, PermissionInstance.getPermissons(2));
            }
        });
        chooseChatTypeDialog.showWithBottomAnim();
    }

    private void transferToChat(){
        KycContactEntity kycContactEntity = new KycContactEntity();
        kycContactEntity.setPhone(MessageUtils.getChatPhoneNumber(gcChatSession));
        kycContactEntity.setContactsName(MessageUtils.getChatSessionName(gcChatSession));
        kycContactEntity.setHeadIcon(ChatHelper.getInstance().getChatUserAvatar(MessageUtils.getChatSessionDisplayPic(gcChatSession)));
        kycContactEntity.setUserId(MessageUtils.getChatUserId(gcChatSession));
        kycContactEntity.setSex("1");
        HashMap<String, Object> map = new HashMap<>(16);
        map.put(Constants.CONTACT_ENTITY, kycContactEntity);
        map.put(Constants.sceneType, transferType);
        map.put(Constants.chatId, gcChatSession.getSessionId());
        if (Constants.TYPE_TRANSFER_WW.equals(transferType)) {
            ActivitySkipUtil.startAnotherActivity(ChatRoomActivity.this, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.TYPE_TRANSFER_RT.equals(transferType)) {
            ActivitySkipUtil.startAnotherActivity(ChatRoomActivity.this, RequestTransferActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void sendTransferMessage(String sessionId, ChatTransferEntity chatTransferEntity){
        GcChatMessage chatMessage = new GcChatMessage();
        chatMessage.setMessage(chatTransferEntity);
        chatMessage.setSessionId(sessionId);
        sendMessage(sessionId, chatMessage);
    }

    private void sendMessage(String sessionId, String message) {
        HashMap<String,String> replyMap = new HashMap<>();
        if(quickRepliedMessage != null){
            replyMap.put("replyName",quickRepliedMessage.getUser().getName());
            replyMap.put("replyMessageId",quickRepliedMessage.getId());
            if(quickRepliedMessage.getGlideUrl() != null) {
                replyMap.put("replyMessage",mContext.getString(R.string.sprint20_73));
                replyMap.put("replyImageUrl", quickRepliedMessage.getGlideUrl().toStringUrl());
            }else{
                replyMap.put("replyMessage",quickRepliedMessage.getSimpleMessageToDisplay("").toString());
            }
        }
        ChatMessage sampleAppChatMessage = new ChatMessage();
        sampleAppChatMessage.setMessage(message);
        GcChatMessage chatMessage = new GcChatMessage();
        chatMessage.setMessage(sampleAppChatMessage);
        chatMessage.setMessageMetadata(replyMap);
        chatMessage.setSessionId(sessionId);
        sendMessage(sessionId, chatMessage);
    }

    private void sendMessage(String sessionId, String fileId, long fileSize, String fileType, String fileName) {
        String downloadLink = "";

        ArrayList<ChatMessage.Attachment> attachments = new ArrayList<ChatMessage.Attachment>();
        ChatMessage.Attachment attachment = new ChatMessage.Attachment();
        attachment.fileId = fileId;
        attachment.downloadLink = downloadLink;
        attachment.fileType = fileType;
        attachment.fileName = fileName;

        attachments.add(attachment);

        GcChatMessage chatMessage = new GcChatMessage();

        ChatMessage sampleAppChatMessage = new ChatMessage();
        sampleAppChatMessage.setAttachments(attachments);

        chatMessage.setMessage(sampleAppChatMessage);
        chatMessage.setSessionId(sessionId);
        chatMessage.setMessageMetadata(new HashMap<>());
        sendMessage(sessionId, chatMessage);
    }

    private void sendMessage(String sessionId, GcChatMessage chatMessage) {
        showProgressCancelable(true,true);
        GcChatSDK.sendMessage(sessionId, chatMessage, new SendMessageCallback() {
            @Override
            public void onSuccessful(String s) {
                showProgress(false);
                LogUtils.d("message sent successfully. message id - %s", s);
                etChatMessage.setText("");
                setReplyMessage(null);
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                LogUtils.d("","error in message sending");
                showTipDialog("Error in sending message!! - " + GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }

    private void loadCurrentSessionDetails(String chatSessionId) {
        GcChatGetSessionsRequest request = new GcChatGetSessionsRequest().setTake(1).setSessionId(chatSessionId);
        GcChatSDK.getSessionList(request, new GetSessionListCallback() {
            @Override
            public void onSuccessful(List<GcChatSession> list, int i) {
                if (!list.isEmpty()) {
                    gcChatSession = list.get(0);
                    onSessionInfoUpdated();
                } else {
                    showProgress(false);
//                    showTipDialog("Problem in fetching session info!!");
                    finish();
                }
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                showTipDialog("Problem in fetching session info!! - " + GcChatErrorUtils.getErrorMessage(errorCode));
                finish();
            }
        });
    }

    private void onSessionInfoUpdated() {
        if (gcChatSession.isGroupChat()) {
            //group chat
        }
        isSessionBlockedByUser = gcChatSession.isBlocked();
        setUserStatus();
        tvName.setText(MessageUtils.getChatSessionName(gcChatSession));
        updateProfilePicture();
        for (GcChatParticipant chatParticipant : gcChatSession.getParticipantUsers()) {
            boolean isMeParticipant = UserUtils.loggedInUserId.equals(chatParticipant.getUser().getExternalId());
            if (isMeParticipant) {
                myParticipantId = chatParticipant.getId();
            }
        }

        loadChatMessages();
    }

    private void setUserStatus(){
        if(gcChatSession.isBlocked()){
            ivUserStatus.setVisibility(View.VISIBLE);
            ivAvatar.setAlpha(0.3f);
        }else{
            ivUserStatus.setVisibility(View.GONE);
            ivAvatar.setAlpha(1.0f);
        }
    }

    private void updateProfilePicture() {
        if(isGroupChat){
            ivAvatar.setImageResource(R.drawable.l_group_avatar);
        }else {
            String displayPic = MessageUtils.getChatSessionDisplayPic(gcChatSession);
            if (displayPic != null && !displayPic.isEmpty()) {
                Glide.with(mContext).load(ChatHelper.getInstance().getChatUserAvatar(displayPic)).centerCrop().autoClone().into(ivAvatar);
            } else {
                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivAvatar);
            }
        }
    }

    private void loadChatMessages() {
        GcChatSDK.getChatHistoryOfSession(gcChatSession.getSessionId(), 50, 0, new GetChatMessageListCallback() {
            @Override
            public void onSuccessful(List<GcChatMessage> list) {
                showProgress(false);
                List<MessageEntity> messageList = new ArrayList<>();
                for (GcChatMessage chatMessage : list) {
                    //{"content":"hi","timestamp":"1656905866169","type":"text","user":{"name":"Sopia","id":"1"}}
                    //{ "downloadLink": "https://dms.pointsinside.com/url", "content": "6d0b8009-9f4c-4b6e-b05a-744a82f7aff8", "type": "image", "timestamp": "1656906372464", "user": { "name": "Sopia", "id": "1" } }
                    MessageEntity message = getMessage(chatMessage, gcChatSession.isGroupChat());
                    if (message != null) {
                        messageList.add(message);
                    }
                }
                for (int i = list.size() - 1; i >= 0; i--) {
                    if (!list.get(i).getSender().getExternalId().equals(UserUtils.loggedInUserId)) {
                        markMessageRead(list.get(i).getMessageId());
                        break;
                    }
                }
                clearAllMessages();
                addMessageList(messageList);
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                LogUtils.d("","getChatHistoryOfSession onFailure");
                showTipDialog("Error in getting chat history - " + GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }

    public static class FileType {
        public static final String IMAGE_PNG = "image/png";
        public static final String IMAGE_JPEG = "image/jpeg";
        public static final String PDF = "application/pdf";
    }
}
