package cn.swiftpass.wallet.tiqmo.module.login.view;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.ChooseLanguageAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.util.LanguageDownloadManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import dev.b3nedikt.app_locale.AppLocale;
import dev.b3nedikt.restring.Restring;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class ChooseLanguageActivity extends BaseCompatActivity {
    @BindView(R.id.ll_english)
    LinearLayout llEnglish;
    @BindView(R.id.ll_arabic)
    LinearLayout llArabic;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_arr_eng)
    ImageView ivArrEng;
    @BindView(R.id.iv_arr_ar)
    ImageView ivArrAr;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.ry_language)
    RecyclerView ryLanguage;

    private ChooseLanguageAdapter chooseLanguageAdapter;

    @OnClick({R.id.iv_back,R.id.ll_english, R.id.ll_arabic})
    public void onViewClicked(View view) {
        if(ButtonUtils.isFastDoubleClick()){
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_english:
                changeLanguage(Locale.ENGLISH);
                switchLang(LocaleUtils.LOCALE_ENGLISH,false);
                break;
            case R.id.ll_arabic:
                changeLanguage(AppLocale.getSupportedLocales().get(1));
                switchLang(LocaleUtils.LOCALE_ARABIC,true);
                break;
            default:
                break;
        }
    }

    private void switchLang(String lang,boolean isRtl) {
        UserInfoEntity user = AppClient.getInstance().getUserManager().getUserInfo();
        if (LocaleUtils.switchLocale(ChooseLanguageActivity.this, lang,isRtl)) {
            restartAct();
            requestSwitchLang();
        }else if(user == null){
            restartAct();
            requestSwitchLang();
        }
        if(user != null) {
            finish();
        }

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build())).build());
    }

    private void requestSwitchLang() {
        AppClient.getInstance().checkLanguage(new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {

            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }

        });
    }

    /**
     * 重启当前Activity
     */
    private void restartAct() {
        SpUtils.getInstance().setAppVersionName(mContext);
        ReferralCodeDialogUtils.resetKeyDialog();
        UserInfoEntity user = AppClient.getInstance().getUserManager().getUserInfo();
        if(user != null){
            AppClient.getInstance().getUserManager().bindUserInfo(user, false);
            ActivitySkipUtil.clearTaskToMainActivity(ChooseLanguageActivity.this);
        } else{
            ActivitySkipUtil.startAnotherActivity(ChooseLanguageActivity.this, RegisterActivity.class);
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_choose_language;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack,headCircle, ivArrEng, ivArrAr);
        if (getIntent() != null && getIntent().getExtras() != null) {
            boolean isFromProfile = getIntent().getExtras().getBoolean(Constants.IS_FROM_PROFILE);
            if(isFromProfile){
                ivBack.setVisibility(View.VISIBLE);
            }
        }

        //启动后获取多语言  如果没有则在这个页面继续调接口
        LanguageListEntity languageListEntity = SpUtils.getInstance().getLanguageListEntity();
        if(languageListEntity != null){
            List<LanguageCodeEntity> languageCodes = languageListEntity.languageCodes;
            setLanguageList(languageCodes);
        }else{
            showProgress(true);
            AppClient.getInstance().getUserManager().checkLanguageAgain(new ResultCallback<LanguageListEntity>() {
                @Override
                public void onResult(LanguageListEntity languageListEntity) {
                    try {
                        if (languageListEntity != null) {
                            List<Locale> localeList = languageListEntity.getLocaleList();
                            if (localeList != null && localeList.size() > 0) {
                                AppLocale.setSupportedLocales(localeList);
                            } else {
                                AppLocale.setSupportedLocales(Arrays.asList(Locale.ENGLISH, new Locale("ar")));
                            }

                            SpUtils.getInstance().setLanguageListEntity(languageListEntity);
                            String version = languageListEntity.version;
                            String lastVersion = SpUtils.getInstance().getLanguageVersion();
                            if (!TextUtils.isEmpty(version) && !lastVersion.equals(version)) {
                                LanguageDownloadManager.getInstance().initCertificate(ProjectApp.getContext(), version, new LanguageDownloadManager.OnCertDownLoadListener() {
                                    @Override
                                    public void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates) {
                                        List<LanguageCodeEntity> languageCodes = new ArrayList<>();
                                        if (isSuccess) {
                                            if (languageListEntity != null) {
                                                languageCodes = languageListEntity.languageCodes;
                                            } else {
                                                LanguageCodeEntity languageCodeEntity1 = new LanguageCodeEntity("English", "en_US");
                                                LanguageCodeEntity languageCodeEntity2 = new LanguageCodeEntity("العربية", "ar");
                                                languageCodes.add(languageCodeEntity1);
                                                languageCodes.add(languageCodeEntity2);
                                            }
                                            int size = localeList.size();
                                            for (int i = 0; i < size; i++) {
                                                Locale locale = localeList.get(i);
                                                LanguageCodeEntity languageCodeEntity = languageCodes.get(i);
                                                Restring.putStrings(locale, AndroidUtils.getLanguageMap(languageCodeEntity));
                                            }
                                        } else {
                                            LanguageCodeEntity languageCodeEntity1 = new LanguageCodeEntity("English", "en_US");
                                            LanguageCodeEntity languageCodeEntity2 = new LanguageCodeEntity("العربية", "ar");
                                            languageCodes.add(languageCodeEntity1);
                                            languageCodes.add(languageCodeEntity2);
                                        }
                                        setLanguageList(languageCodes);
                                        showProgress(false);
                                    }
                                });
                            }
                        }
                    }catch (Throwable e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure (String errorCode, String errorMsg){
                    showProgress(false);
                    List<Locale> localeList = Arrays.asList(Locale.ENGLISH, new Locale("ar"));
                    AppLocale.setSupportedLocales(localeList);
                    List<LanguageCodeEntity> languageCodes = new ArrayList<>();
                    LanguageCodeEntity languageCodeEntity1 = new LanguageCodeEntity("English", "en_US");
                    LanguageCodeEntity languageCodeEntity2 = new LanguageCodeEntity("العربية", "ar");
                    languageCodes.add(languageCodeEntity1);
                    languageCodes.add(languageCodeEntity2);
                    int size = localeList.size();
                    for (int i = 0; i < size; i++) {
                        Locale locale = localeList.get(i);
                        LanguageCodeEntity languageCodeEntity = languageCodes.get(i);
                        Restring.putStrings(locale, AndroidUtils.getLanguageMap(languageCodeEntity));
                    }

                    setDefaultLanguage();
                }
            });
        }
    }

    private void setDefaultLanguage(){
        List<LanguageCodeEntity> languageCodes = new ArrayList<>();
        LanguageCodeEntity languageCodeEntity1 = new LanguageCodeEntity("English","en_US");
        LanguageCodeEntity languageCodeEntity2 = new LanguageCodeEntity("العربية","ar");
        languageCodes.add(languageCodeEntity1);
        languageCodes.add(languageCodeEntity2);
        setLanguageList(languageCodes);
    }

    private void setLanguageList(List<LanguageCodeEntity> languageCodes){
        try {
            if (languageCodes != null && languageCodes.size() > 0) {
                LinearLayoutManager manager = new LinearLayoutManager(mContext);
                MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 20));
                ryLanguage.addItemDecoration(myItemDecoration);
                manager.setOrientation(RecyclerView.VERTICAL);
                ryLanguage.setLayoutManager(manager);
                chooseLanguageAdapter = new ChooseLanguageAdapter(languageCodes);
                chooseLanguageAdapter.bindToRecyclerView(ryLanguage);

                chooseLanguageAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                        LanguageCodeEntity languageCodeEntity = languageCodes.get(position);
                        if (languageCodeEntity != null) {
                            String key = languageCodeEntity.key;
                            Locale locale = null;
                            if ("en_US".equals(key)) {
                                locale = Locale.ENGLISH;
                                changeLanguage(locale);
                            } else {
                                if (AppLocale.getSupportedLocales().size() >= position + 1) {
                                    locale = AppLocale.getSupportedLocales().get(position);
                                    changeLanguage(locale);
                                }
                            }
                            languageCodeEntity.locale = locale;
                            switchLang(key, languageCodeEntity.rTL);
                            SpUtils.getInstance().setLanguageCodeEntity(languageCodeEntity);
                            getSignatures(ChooseLanguageActivity.this);
                        }
                    }
                });
            }
        }catch (Throwable e){
            e.printStackTrace();
        }
    }

    public String getSignatures(Activity activity) {
        PackageInfo info;
        try {
            info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA256");

            byte[] publicKey = md.digest(cert);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i]).toUpperCase(Locale.US);
                if (appendString.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(appendString);
                hexString.append(":");
            }
            String value = hexString.toString();
            String msg = "SHA256:" + value.substring(0, value.length() - 1);
            LogUtils.d("ProjectApp", msg);
            return value.substring(0, value.length() - 1);

        } catch (NoSuchAlgorithmException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
