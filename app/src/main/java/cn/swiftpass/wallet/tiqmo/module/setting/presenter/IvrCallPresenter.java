package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.IvrCallContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class IvrCallPresenter implements IvrCallContract.IvrCallPresenter {

    IvrCallContract.IvrCallView mIvrCallView;

    @Override
    public void sendIvrCall(String outActionId, String amount) {
        AppClient.getInstance().sendIvrCall(outActionId, amount, new LifecycleMVPResultCallback<ResultEntity>(mIvrCallView,true) {
            @Override
            protected void onSuccess(ResultEntity result) {
                mIvrCallView.sendIvrCallSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getIvrResult(String outActionId) {
        AppClient.getInstance().getIvrResult(outActionId, new LifecycleMVPResultCallback<ResultEntity>(mIvrCallView,false) {
            @Override
            protected void onSuccess(ResultEntity result) {
                mIvrCallView.getIvrResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.getIvrResultFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(IvrCallContract.IvrCallView ivrCallView) {
        this.mIvrCallView = ivrCallView;
    }

    @Override
    public void detachView() {
        this.mIvrCallView = null;
    }

    @Override
    public void getKsaCardDetails(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaCardDetails(proxyCardNo, new LifecycleMVPResultCallback<CardDetailEntity>(mIvrCallView,true) {
            @Override
            protected void onSuccess(CardDetailEntity result) {
                mIvrCallView.getKsaCardDetailsSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire) {
        AppClient.getInstance().getCardManager().activateCard(proxyCardNo, cardNo, cardExpire, new LifecycleMVPResultCallback<Void>(mIvrCallView,true) {
            @Override
            protected void onSuccess(Void result) {
                mIvrCallView.activateCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue, String proxyCardNo,String up) {
        AppClient.getInstance().getCardManager().setKsaCardLimit(txnType, dailyLimitValue, monthlyLimitValue, proxyCardNo,up, new LifecycleMVPResultCallback<Void>(mIvrCallView,true) {
            @Override
            protected void onSuccess(Void result) {
                mIvrCallView.setKsaCardLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardStatus(String status, String proxyCardNo, String reason,String reasonStatus) {
        AppClient.getInstance().getCardManager().setKsaCardStatus(status, proxyCardNo, reason,reasonStatus, new LifecycleMVPResultCallback<Void>(mIvrCallView,true) {
            @Override
            protected void onSuccess(Void result) {
                mIvrCallView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaCardPayResult(String vat, String totalAmount, OpenCardReqEntity appNiOpenCardReq) {
        AppClient.getInstance().getCardManager().getKsaCardPayResult(vat, totalAmount, appNiOpenCardReq, new LifecycleMVPResultCallback<KsaPayResultEntity>(mIvrCallView,true) {
            @Override
            protected void onSuccess(KsaPayResultEntity result) {
                mIvrCallView.getKsaCardPayResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.getKsaCardPayResultFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(mIvrCallView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mIvrCallView.transferSurePaySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mIvrCallView.transferSurePayFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark, String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName) {
        AppClient.getInstance().transferToContact(payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, payerName, new LifecycleMVPResultCallback<TransferEntity>(mIvrCallView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mIvrCallView.transferToContactSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mIvrCallView.transferToContactFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType) {
        AppClient.getInstance().confirmPay(orderNo, paymentMethodNo, orderType, new LifecycleMVPResultCallback<TransferEntity>(mIvrCallView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mIvrCallView.confirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.confirmPayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void splitBill(String oldOrderNo, String receivableAmount, String currencyCode, String orderRemark, List<SplitBillPayerEntity> receiverInfoList) {
        AppClient.getInstance().splitBill(oldOrderNo, receivableAmount, currencyCode, orderRemark, receiverInfoList, new LifecycleMVPResultCallback<TransferEntity>(mIvrCallView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mIvrCallView.splitBillSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mIvrCallView.splitBillFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName, String nickName, String relationshipCode, String callingCode, String phone, String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace, String birthDate, String sex, String cityName, String districtName, String poBox, String buildingNo, String street, String idNo, String idExpiry, String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag, String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode, String channelPayeeId, String channelCode,String branchId) {
        AppClient.getInstance().imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag,receiptOrgName,
                receiptOrgBranchName,cityId,currencyCode,channelPayeeId,channelCode,branchId, new LifecycleMVPResultCallback<ImrAddBeneResultEntity>(mIvrCallView, true) {
                    @Override
                    protected void onSuccess(ImrAddBeneResultEntity result) {
                        mIvrCallView.imrAddBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mIvrCallView.imrAddBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo) {
        AppClient.getInstance().saveImrBeneficiaryInfo(beneficiaryInfo.payeeInfoId,
                beneficiaryInfo.transferDestinationCountryCode, beneficiaryInfo.receiptMethod,
                beneficiaryInfo.receiptOrgCode, beneficiaryInfo.receiptOrgBranchCode,
                beneficiaryInfo.payeeFullName, beneficiaryInfo.nickName, beneficiaryInfo.relationshipCode,
                beneficiaryInfo.callingCode, beneficiaryInfo.phone, beneficiaryInfo.payeeInfoCountryCode,
                beneficiaryInfo.birthPlace, beneficiaryInfo.birthDate, beneficiaryInfo.sex,
                beneficiaryInfo.cityName, beneficiaryInfo.districtName, beneficiaryInfo.poBox,
                beneficiaryInfo.buildingNo, beneficiaryInfo.street, beneficiaryInfo.idNo,
                beneficiaryInfo.idExpiry, beneficiaryInfo.bankAccountType, beneficiaryInfo.ibanNo,
                beneficiaryInfo.bankAccountNo,beneficiaryInfo.receiptOrgName,beneficiaryInfo.receiptOrgBranchName,
                beneficiaryInfo.cityId,beneficiaryInfo.currencyCode,beneficiaryInfo.channelPayeeId,beneficiaryInfo.channelCode,beneficiaryInfo.branchId,
                new LifecycleMVPResultCallback<ResponseEntity>(mIvrCallView, true) {
                    @Override
                    protected void onSuccess(ResponseEntity result) {
                        mIvrCallView.imrEditBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mIvrCallView.imrEditBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }
}
