package cn.swiftpass.wallet.tiqmo.base.view;

public interface OnMsgClickCallBack {
    void onBtnClickListener();
}
