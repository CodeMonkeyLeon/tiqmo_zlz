package cn.swiftpass.wallet.tiqmo.sdk.net;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponseContent;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResponseCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.ResourcesHelper;

//这个类是ResponseCallback的包装，主要做了对某些错误码的处理，比如登录过期，没有网络等等
public class ResponseCallbackWrapper<T> implements ResponseCallback<JsonResponse<T>> {

    //下面保存了一些后台返回错误码，他们错误码有时候会改但是可能会忘记同步到我们，这点开发过程中可能要注意一下

    public static final String RESPONSE_CODE_SUCCESS = "000000";//成功
    public static final String RESPONSE_CODE_NOT_EXIST = "030001";//账号不存在
    public static final String RESPONSE_CODE_EXPIRED = "030067";//token过期
    public static final String RESPONSE_CODE_INVALID = "000301";//token无效
    public static final String RESPONSE_CODE_LOG_IN_ON_OTHER_DEVICES = "030073";//账户在其他设备登录


    public static final String RESPONSE_CODE_TRANSACTION_SECRET_ERROR = "030077";//交易密码错误N次之后
    public static final String RESPONSE_CODE_TRANSACTION_SECRET_FROZEN = "030076";//交易密码冻结
    public static final String RESPONSE_CODE_INSUFFICIENT_ACCOUNT_BALANCE = "010107";//账户余额不足
    public static final String RESPONSE_CODE_INSUFFICIENT_BANK_CARD_BALANCE = "010108";//卡余额不足
    public static final String RESPONSE_CODE_BIOMETRIC_NOT_MATCH = "010859";//生物认证不匹配
    public static final String RESPONSE_CODE_LOGIN_NEED_IMAGE_VERIFY_CODE_1 = "030019";//登陆密码验证需要图形验证码,登录界面用的（需要弹出忘记密码提示框）
    public static final String RESPONSE_CODE_LOGIN_NEED_IMAGE_VERIFY_CODE_2 = "030075";//登陆密码验证需要图形验证码,登录界面用的
    public static final String RESPONSE_CODE_REGISTER_NEED_IMAGE_VERIFY_CODE = "040001";//需要弹出验证码
    public static final String RESPONSE_CODE_REGISTER_NEED_IMAGE_VERIFY_CODE1 = "030020";//需要弹出验证码
    public static final String RESPONSE_CODE_REGISTER_NEED_IMAGE_VERIFY_CODE2 = "030105";//需要弹出验证码
    public static final String RESPONSE_CODE_REGISTER_NEED_RESENT_VERIFY_CODE = "040002";//短信验证码已失效
    public static final String RESPONSE_CODE_REGISTER_NEED_RESENT_VERIFY_CODE1 = "030056";//手机验证码校验错误次数过多
    public static final String RESPONSE_CODE_REGISTER_NEED_RESENT_VERIFY_CODE2 = "030057";//手机验证码校验错误次数过多

    public static final String RESPONSE_CODE_ALREADY_REGISTERED = "030002";//账号已经注册
    public static final String RESPONSE_CODE_TRANSFER_USER_DOES_NOT_EXIST = "030001";//转账用户不存在
    public static final String RESPONSE_CODE_CAN_T_TRANSFER_TO_MYSELF = "010103";//不能转账给自己
    public static final String RESPONSE_CODE_TOP_UP_AMOUNT_IS_TOO_SMALL = "010004";//充值金额太小
    public static final String RESPONSE_CODE_TRANSFER_AMOUNT_IS_TOO_SMALL = "010015";//转账金额太小
    public static final String RESPONSE_CODE_PAYMENT_AMOUNT_EXCEEDS_THE_LIMIT = "010027";//单笔支付金额超出限额

    //网络错误，自己定义的
    public static final String NETWORK_ERROR_CODE_UNKNOWN = "NetworkUnknown";
    public static final String NETWORK_ERROR_CODE_TIMEOUT = "NetworkTimeout";
    public static final String NETWORK_ERROR_CODE_UNAVAILABLE = "NetworkUnavailable";

    protected final ResultCallback<T> mCallback;

    public ResponseCallbackWrapper(ResultCallback<T> callback) {
        mCallback = callback;
    }

    //对请求做了一些处理，主要判断是不是登录过期，是就回调listener
    @Override
    public void onResponse(JsonResponse<T> response) {
        handleResponse(response, new ResultCallback<T>() {
            @Override
            public void onResult(T result) {
                if (mCallback != null) {
                    mCallback.onResult(result);
                }
            }

            @Override
            public void onFailure(String code, String error) {
                switch (code) {
                    //判断是否是过期的CODE
                    case RESPONSE_CODE_EXPIRED:
                    case RESPONSE_CODE_INVALID:
                    case RESPONSE_CODE_LOG_IN_ON_OTHER_DEVICES:
                        if (AppClient.getInstance().isLogged()) {
                            AppClient.OnLoginExpiredListener listener = AppClient.getInstance().getOnLoginExpiredListener();
                            if (listener != null) {
                                listener.onExpired(code,error+"("+code+")");
                            }
                            return;
                        }
                    default:
                        if (mCallback != null) {
                            mCallback.onFailure(code, error+"("+code+")");
                        }
                }
            }
        });
    }

    @Override
    public void onFailure(Throwable t) {
        LogUtil.w(t.toString(), t);
        handleError(t);
    }

    //检查jsonResponse，看看code是否是成功，根据不同的内容回调不同结果
    static <T> void handleResponse(JsonResponse<T> response, ResultCallback<T> callback) {
        if (response == null || callback == null) {
            return;
        }
        if (RESPONSE_CODE_SUCCESS.equals(response.getCode())) {
            JsonResponseContent<T> content = response.getContent();
            callback.onResult(content == null ? null : content.getServiceBody());
        } else {
            callback.onFailure(response.getCode(), response.getMsg());
        }
    }

    private void handleError(Throwable t) {
        if (mCallback == null) {
            return;
        }
//        if (t instanceof SocketTimeoutException) {
//            mCallback.onFailure(NETWORK_ERROR_CODE_TIMEOUT, ResourcesHelper.getString(R.string.Core_NetworkTimeout));
//        } else if (t instanceof ConnectException) {
//            mCallback.onFailure(NETWORK_ERROR_CODE_TIMEOUT, ResourcesHelper.getString(R.string.Core_ConnectFail));
//        } else if (t instanceof UnknownHostException) {
//            mCallback.onFailure(NETWORK_ERROR_CODE_UNAVAILABLE, ResourcesHelper.getString(R.string.Core_ConnectFail));
//        } else if (t instanceof ResponseException) {
//            mCallback.onFailure(((ResponseException) t).getErrorCode(), ((ResponseException) t).getErrorMessage());
//        } else {
//            mCallback.onFailure(NETWORK_ERROR_CODE_UNKNOWN, ResourcesHelper.getString(R.string.Core_ServerError));
//        }

        mCallback.onFailure(NETWORK_ERROR_CODE_TIMEOUT, ResourcesHelper.getString(R.string.common_3));
    }

    public static boolean isConnectionError(String errorCode) {
        if (TextUtils.isEmpty(errorCode)) {
            return false;
        }
        switch (errorCode) {
            case NETWORK_ERROR_CODE_UNKNOWN:
            case NETWORK_ERROR_CODE_TIMEOUT:
            case NETWORK_ERROR_CODE_UNAVAILABLE:
                return true;
        }
        return false;
    }
}
