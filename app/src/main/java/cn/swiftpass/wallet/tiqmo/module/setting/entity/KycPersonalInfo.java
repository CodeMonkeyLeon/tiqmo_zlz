package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import android.text.TextUtils;

public class KycPersonalInfo {
    public String idNum;
    public String dataOfBirth;
    public String address;
    public String employerName;
    public String employmentCode;
    public String employmentDesc;
    public String professionCode;
    public String professionDesc;
    public String sourceOfFundCode;
    public String salaryRangeCode;
    public String salaryRangeDesc;
    public String businessTypeCode;
    public String businessTypeDesc;
    public String sourceOfFundDesc;
    public String beneficialOwner = "";
    public String relativesExposedPerson = "";
    public String sceneType;
    public CityEntity.City city;
    public StateEntity stateEntity;
//    public RegionEntity.RegionInfo regionInfo;

//    public boolean isRegFull() {
//        return !TextUtils.isEmpty(idNum) && idNum.length() == 10 && !TextUtils.isEmpty(dataOfBirth)
//                && city != null && !TextUtils.isEmpty(city.citiesName)
//                && !TextUtils.isEmpty(city.citiesId)
//                && stateEntity != null && !TextUtils.isEmpty(stateEntity.statesName)
//                && !TextUtils.isEmpty(stateEntity.statesId)
//                && !TextUtils.isEmpty(professionCode)
//                && !TextUtils.isEmpty(sourceOfFundCode)
//                && !TextUtils.isEmpty(salaryRangeCode)
//                && !TextUtils.isEmpty(address)
//                && !TextUtils.isEmpty(employerName);
//    }

    public boolean isRegFull() {
        return city != null && !TextUtils.isEmpty(city.citiesName)
                && !TextUtils.isEmpty(city.citiesId)
                && stateEntity != null && !TextUtils.isEmpty(stateEntity.statesName)
                && !TextUtils.isEmpty(stateEntity.statesId)
                && !TextUtils.isEmpty(employmentCode)
                && !TextUtils.isEmpty(professionCode)
                && !TextUtils.isEmpty(sourceOfFundCode)
                && !TextUtils.isEmpty(salaryRangeCode)
                && !TextUtils.isEmpty(beneficialOwner)
                && !TextUtils.isEmpty(relativesExposedPerson)
                && !TextUtils.isEmpty(address);
    }

    public boolean isForgetPPFull(int maxIdLength) {
        if (maxIdLength == 15) {
            return !TextUtils.isEmpty(idNum) && idNum.length() == maxIdLength &&
                    idNum.startsWith("784") && !TextUtils.isEmpty(dataOfBirth);
        }
        return !TextUtils.isEmpty(idNum) && idNum.length() == maxIdLength && !TextUtils.isEmpty(dataOfBirth);
    }
}
