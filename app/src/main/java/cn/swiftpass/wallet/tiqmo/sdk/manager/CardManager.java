package cn.swiftpass.wallet.tiqmo.sdk.manager;
//
//
//import androidx.annotation.NonNull;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.concurrent.CopyOnWriteArrayList;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardListEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.CardApi;
import cn.swiftpass.wallet.tiqmo.sdk.util.CallbackUtil;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;

//import cn.swiftpass.wallet.sdk.AppConfig;
//import cn.swiftpass.wallet.sdk.entity.AllCardEntity;
//import cn.swiftpass.wallet.sdk.entity.BindCardOPTEntity;
//import cn.swiftpass.wallet.sdk.entity.CardEntity;
//import cn.swiftpass.wallet.sdk.entity.CardTypeEntity;
//import cn.swiftpass.wallet.sdk.entity.CheckOTPEntity;
//import cn.swiftpass.wallet.sdk.listener.ResultCallback;
//import cn.swiftpass.wallet.sdk.net.ApiHelper;
//import cn.swiftpass.wallet.sdk.net.ResponseCallbackWrapper;
//import cn.swiftpass.wallet.sdk.net.api.CardApi;
//import cn.swiftpass.wallet.sdk.util.CallbackUtil;
//
///**
// * Created by 叶智星 on 2018年09月13日.
// * 每一个不曾起舞的日子，都是对生命的辜负。
// */
//
////卡管理，包含了对银行卡相关得操作
public class CardManager {
    private AppClient mAppClient;
    private CardApi mCardApi;

////    private Map<String, CardEntity> mCardsMap;//包含了当前卡得信息，key是卡ID，value是包含卡信息得实体，每次获取卡列表之后要更新这个map
//    private List<CardEntity> mCardCache;//未保证顺序 Cache用List实现
//    private final List<OnCardChangeListener> mCardChangeListeners;
//
    public CardManager(AppClient appClient) {
        mAppClient = appClient;
        mCardApi = ApiHelper.getApi(CardApi.class);
//        mCardChangeListeners = Collections.synchronizedList(new LinkedList<OnCardChangeListener>());
//        mCardsMap = Collections.synchronizedMap(new LinkedHashMap<String, CardEntity>());
//        mCardCache = new CopyOnWriteArrayList<>();
    }
//
//    //获取所有得卡，不会调用接口，因为用到的LinkedHashMap，所以list的顺序和服务器返回的顺序是一i杨的
//    @NonNull
//    public List<CardEntity> getAllCard() {
//        return mCardCache;
//    }
//
//    //获取所有得信用卡
//    @NonNull
//    public List<CardEntity> getAllCreditCard() {
//        List<CardEntity> cardList = getAllCard();
//        Iterator<CardEntity> it = cardList.iterator();
//        while (it.hasNext()) {
//            if (AppConfig.CARD_TYPE_DEBIT.equals(it.next().getCardType())) {
//                it.remove();
//            }
//        }
//        return cardList;
//    }
//
//    //获取所有的借记卡
//    @NonNull
//    public List<CardEntity> getAllDebitCard() {
//        List<CardEntity> cardList = getAllCard();
//        Iterator<CardEntity> it = cardList.iterator();
//        while (it.hasNext()) {
//            if (AppConfig.CARD_TYPE_CREDIT.equals(it.next().getCardType())) {
//                it.remove();
//            }
//        }
//        return cardList;
//    }
//
//    //调用接口获取所有的卡
    public void getAllBindCard(final ResultCallback<List<CardEntity>> callback) {
        mCardApi.getBankCardList().enqueue(new ResponseCallbackWrapper<>(new ResultCallback<AllCardEntity>() {
            @Override
            public void onResult(AllCardEntity result) {
                List<CardEntity> newCardList = result.getCardList();
                if (newCardList == null) {
                    newCardList = new ArrayList<>();
                }
                CallbackUtil.callResult(newCardList, callback);
                //和当前的CardList进行比较，如果发生改变就更新缓存同时回调Listener
//                List<CardEntity> oldCardList = getAllCard(); 注释掉 因为添加卡后，绑卡成功后会将新卡添加mCardMap中，新卡位于最后面，而需求要求新卡排最前 note by msl
//                if (newCardList.size() != oldCardList.size() || !oldCardList.containsAll(newCardList)) {
//                mCardsMap.clear();
//                for (CardEntity c : newCardList) {
//                    mCardsMap.put(c.getCardId(), c);
//                }
//                for (OnCardChangeListener listener : mCardChangeListeners) {
//                    listener.onCardListChange(newCardList);
//                }
//                }

                //和当前的CardList进行比较，如果发生改变就更新缓存同时回调Listener
/*                List<CardEntity> oldCardList = getAllCard();
                if (newCardList.size() != oldCardList.size() || !oldCardList.containsAll(newCardList)) {
                    mCardCache.clear();
                    mCardCache.addAll(newCardList);
                    for (OnCardChangeListener listener : mCardChangeListeners) {
                        listener.onCardListChange(newCardList);
                    }
                }*/

            }

            @Override
            public void onFailure(String code, String error) {
                CallbackUtil.callFailure(code, error, callback);
            }
        }));
    }

    public void bindCard(String cardHolderName,String cardNum, String expireDate,String CVV,String type ,final ResultCallback<CardBind3DSEntity> callback){
        mCardApi.bindCard(cardHolderName,cardNum,expireDate,CVV,type).enqueue(new ResponseCallbackWrapper<>(new ResultCallback<CardBind3DSEntity>() {
            @Override
            public void onResult(CardBind3DSEntity response) {
                CallbackUtil.callResult(response, callback);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                CallbackUtil.callFailure(errorCode, errorMsg, callback);
            }
        }));
    }

    public void unBindCard(String protocolNo ,final ResultCallback<unBindCardEntity> callback) {
        mCardApi.unBindCard(protocolNo).enqueue(new ResponseCallbackWrapper<>(new ResultCallback<unBindCardEntity>() {
            @Override
            public void onResult(unBindCardEntity response) {
                CallbackUtil.callResult(response, callback);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                CallbackUtil.callFailure(errorCode, errorMsg, callback);
            }
        }));
    }

    public void setDefaultCard(String protocolNo ,final ResultCallback<Void> callback) {
        mCardApi.setDefaultCard(protocolNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void verifyCard(String cardNo ,final ResultCallback<verifyCardEntity> callback) {
        mCardApi.verifyCard(cardNo).enqueue(new ResponseCallbackWrapper<>(new ResultCallback<verifyCardEntity>() {
            @Override
            public void onResult(verifyCardEntity response) {
                CallbackUtil.callResult(response, callback);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                CallbackUtil.callFailure(errorCode, errorMsg, callback);
            }
        }));
    }

    //第三方账户绑定（3125）
    public void openKsaCardAccount(String productType,final ResultCallback<Void> callback) {
        mCardApi.openKsaCardAccount(productType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发卡set pin APP接口（3126）
    public void setKsaCardPin(String pin,String proxyCardNo, final ResultCallback<Void> callback) {
        mCardApi.setKsaCardPin(pin,proxyCardNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发卡列表APP接口（3127）
    public void getKsaCardList(final ResultCallback<KsaCardListEntity> callback) {
        mCardApi.getKsaCardList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发卡卡费接口和summary接口APP接口（3128）
    public void getKsaCardSummary(String cardType,String cardProductType, final ResultCallback<KsaCardSummaryEntity> callback) {
        mCardApi.getKsaCardSummary(cardType,cardProductType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发卡卡费支付接口（3129）
    public void getKsaCardPayResult(String vat, String totalAmount,
                                    OpenCardReqEntity appNiOpenCardReq, final ResultCallback<KsaPayResultEntity> callback) {
        mCardApi.getKsaCardPayResult(vat,totalAmount,appNiOpenCardReq).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //卡激活接口（3246）
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire, final ResultCallback<Void> callback) {
        mCardApi.activateCard(proxyCardNo,cardNo,cardExpire).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //激活卡校验(3254)
    public void activateCardResult(ResultCallback<Void> callback) {
        mCardApi.activateCardResult().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //open loop交易历史列表（ 3247）
    public void getKsaCardHistory(String protocolNo, String accountNo, List<String> paymentType,
                                       List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum, String direction,
                                       String orderNo, LifecycleMVPResultCallback<TransferHistoryMainEntity> callback) {
        mCardApi.getKsaCardHistory(protocolNo, accountNo, paymentType,
                tradeType, startDate, endDate, pageSize, pageNum, direction, orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //卡详情（3108）
    public void getKsaCardDetails(String proxyCardNo, final ResultCallback<CardDetailEntity> callback) {
        mCardApi.getKsaCardDetails(proxyCardNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //修改卡状态（3114）
    public void setKsaCardStatus(String status,
                                  String proxyCardNo,
                                  String reason,String reasonStatus, final ResultCallback<Void> callback) {
        mCardApi.setKsaCardStatus(status,proxyCardNo,reason,reasonStatus).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取卡限额（3111）
    public void getKsaLimitChannel(String proxyCardNo, final ResultCallback<KsaLimitChannelEntity> callback) {
        mCardApi.getKsaLimitChannel(LocaleUtils.getRequestCurrencyCode(),proxyCardNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取充值卡列表（3009）
    public void getBankCardList(String cardType,final ResultCallback<AllCardEntity> callback) {
        mCardApi.getBankCardList(cardType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //第三方账户限额和交易渠道设置（3112）
    public void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue,String proxyCardNo,String up, final ResultCallback<Void> callback) {
        if (!TextUtils.isEmpty(dailyLimitValue)) {
            mCardApi.setKsaDayCardLimit(txnType, LocaleUtils.getRequestCurrencyCode(), dailyLimitValue,proxyCardNo,up).enqueue(new ResponseCallbackWrapper<>(callback));
        } else if (!TextUtils.isEmpty(monthlyLimitValue)) {
            mCardApi.setKsaMonthCardLimit(txnType, LocaleUtils.getRequestCurrencyCode(), monthlyLimitValue,proxyCardNo,up).enqueue(new ResponseCallbackWrapper<>(callback));
        }
    }
    //第三方账户限额和交易渠道设置（3112）
    public void setKsaCardChannel(String txnType, String txnTypeEnabled,String proxyCardNo, final ResultCallback<Void> callback) {
        mCardApi.setKsaCardChannel(txnType, LocaleUtils.getRequestCurrencyCode(), txnTypeEnabled,proxyCardNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }
}
