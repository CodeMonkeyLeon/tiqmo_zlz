package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SplitPeopleAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferToContactPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ContactUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.dialog.SplitContactDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class CreateSplitBillActivity extends BaseCompatActivity<TransferContract.TransferToContactPresenter> implements TransferContract.TransferToContactView {
    private static final String TAG = CreateSplitBillActivity.class.getSimpleName();
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_bill_desc)
    CustomizeEditText etBillDesc;
    @BindView(R.id.et_bill_amount)
    CustomizeEditText etBillAmount;
    @BindView(R.id.cb_bill_switch)
    CheckBox cbBillSwitch;
    @BindView(R.id.tv_add_people)
    TextView tvAddPeople;
    @BindView(R.id.tv_error_money)
    TextView tvError;
    @BindView(R.id.rl_split_people)
    RecyclerView rlSplitPeople;
    @BindView(R.id.tv_split)
    TextView tvSplit;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_split_people)
    LinearLayout llSplitPeople;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;

    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;

    //通讯录联系人List
    public List<KycContactEntity> phoneList = new ArrayList<>();
    //选中的拆分账单联系人List
    public List<KycContactEntity> selectedSplitPeopleList = new ArrayList<>();
    //所有的拆分账单联系人List  没有勾选包含自己 就和选中的一致
    public List<KycContactEntity> allSplitPeopleList = new ArrayList<>();
    //Invite联系人
    public List<KycContactEntity> inviteSplitPeopleList = new ArrayList<>();

    private SplitContactDialog splitContactDialog;

    private SplitPeopleAdapter splitPeopleAdapter;

    private LinearLayoutManager mLayoutManager;

    //拆分账单限制最大人数(包含自己)
    private int splitReceiversLimit;

    private String splitMoney = "0";
    private String orderMoney;
    private String monthLimitMoney, maxLimitBalance;

    private int peopleSize;
    private KycContactEntity myEntity;
    //判断是否包含自己
    private boolean isCheckedMy;
    //是否可添加联系人
    private boolean cannotAdd;

    private TransferLimitEntity transferLimitEntity;

    private String orderNo, orderRemark, orderCurrencyCode;
    //这个List是用来传参给服务器
    private List<SplitBillPayerEntity> receiverInfoList = new ArrayList<>();

    private TransferEntity mTransferEntity;

    private RiskControlEntity riskControlEntity;

    private static CreateSplitBillActivity crateSplitBillActivity;
    //联系人是否注册列表
    private List<KycContactEntity> contactInviteDtos;

    public static CreateSplitBillActivity getCreateSplitBillActivity() {
        return crateSplitBillActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        crateSplitBillActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_create_split_bill;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        crateSplitBillActivity = this;
        setDarkBar();
        tvTitle.setText(getString(R.string.splitbill_1));
        LocaleUtils.viewRotationY(this, ivBack, headCircle);

        etBillAmount.setRightText(LocaleUtils.getCurrencyCode(""));
        UxcamHelper.getInstance().hideScreen(true);

        fixedSoftKeyboardSliding();
        if (getIntent() != null && getIntent().getExtras() != null) {
            orderNo = getIntent().getExtras().getString(Constants.orderNo);
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            orderMoney = getIntent().getExtras().getString(Constants.splitMoney);
            if (getIntent().getExtras().getSerializable(Constants.CONTACT_ENTITY) != null) {
                selectedSplitPeopleList = (List<KycContactEntity>) getIntent().getExtras().getSerializable(Constants.CONTACT_ENTITY);
            }
            cannotAdd = getIntent().getExtras().getBoolean("cannotAdd",false);
            if (transferLimitEntity != null) {
                String totalMonthLimitAmt = transferLimitEntity.totalMonthLimitAmt;
                try {
                    monthLimitMoney = AndroidUtils.getTransferStringMoney(totalMonthLimitAmt);
                    maxLimitBalance = AndroidUtils.getTransferStringMoney(transferLimitEntity.maxLimitBalance);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }

            if (!TextUtils.isEmpty(orderMoney)) {
                orderMoney = orderMoney.replace(",", "");
                splitMoney = orderMoney;
                etBillAmount.setText(orderMoney);
                checkMoney(orderMoney,etBillDesc.getText().toString().trim());
            }
        }

        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            splitReceiversLimit = configEntity.splitReceiversLimit;
//            splitReceiversLimit = 5;
        }

        if (!TextUtils.isEmpty(orderCurrencyCode)) {
        } else {
            orderCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
        }

        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        myEntity = new KycContactEntity();
        if (userInfoEntity != null) {
            myEntity.setContactsName(userInfoEntity.userName);
            myEntity.setCallingCode(SpUtils.getInstance().getCallingCode());
            myEntity.setPhone(userInfoEntity.phone);
            myEntity.setHeadIcon(userInfoEntity.avatar);
            myEntity.setSex(userInfoEntity.gender);
        }


        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 12f);
        rlSplitPeople.addItemDecoration(MyItemDecoration.createVertical(getColor(R.color.transparent), ryLineSpace));
        rlSplitPeople.setLayoutManager(mLayoutManager);
        splitPeopleAdapter = new SplitPeopleAdapter(allSplitPeopleList);
        splitPeopleAdapter.bindToRecyclerView(rlSplitPeople);

        cbBillSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckedMy = isChecked;
                initSplitList(selectedSplitPeopleList);
                checkMoney(etBillAmount.getText().toString().trim(),etBillDesc.getText().toString().trim());
            }
        });
        cbBillSwitch.setChecked(true);

        splitPeopleAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.iv_minus:

                        break;
                    case R.id.iv_plus:

                        break;
                    default:
                        break;
                }
            }
        });


        //两位小数过滤
        etBillAmount.setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etBillDesc.setFilters(new InputFilter[]{AndroidUtils.getEmjoFilter(),new InputFilter.LengthFilter(32)});
        etBillAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                if (TextUtils.isEmpty(amount)) {
                    tvSplit.setEnabled(false);
                    etBillAmount.setError("");
                    tvError.setVisibility(View.GONE);
                    splitMoney = "0";
                } else {
                    try {
                        splitMoney = amount;
                        if (BigDecimalFormatUtils.compareBig(splitMoney,"0")) {
                            if (userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                                tvSplit.setEnabled(false);
                                etBillAmount.setError(getString(R.string.newhome_45));
                            } else {
                                etBillAmount.setError("");
                                tvSplit.setEnabled(true);
                            }
                        }
                    } catch (Exception e) {
                        tvSplit.setEnabled(false);
                    }
                }
                initSplitList(selectedSplitPeopleList);
                checkMoney(amount,etBillDesc.getText().toString().trim());
            }
        });
        etBillDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String desc = s.toString();
                try {

                    if (TextUtils.isEmpty(desc)) {
                        tvSplit.setEnabled(false);
                    }else {
                        checkMoney(etBillAmount.getText().toString().trim(),desc);
                    }
                }catch (Exception e){
                    tvSplit.setEnabled(false);
                }
            }
        });
        etBillAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String amount = etBillAmount.getText().toString().trim();
                amount = amount.replace(",", "");
                if(!TextUtils.isEmpty(amount)) {
                    splitMoney = amount;
                }
                String fomatAmount = AmountUtil.dataFormat(amount);
                if (!hasFocus) {
                    if (TextUtils.isEmpty(amount)) {
                        tvSplit.setEnabled(false);
                    } else {
                        etBillAmount.setText(fomatAmount);
//                        initSplitList(selectedSplitPeopleList);
                        checkMoney(amount,etBillDesc.getText().toString().trim());
                    }
                }
            }
        });

        if (selectedSplitPeopleList != null && selectedSplitPeopleList.size()>0 && cannotAdd) {
            isCheckedMy = true;
            tvAddPeople.setVisibility(View.GONE);
            initSplitList(selectedSplitPeopleList);
            tvSplit.setEnabled(false);
        }
    }

    public void changeMoney(int position, String peopleMoney) {
        etBillDesc.setFocusable(false);
        etBillDesc.setFocusableInTouchMode(false);
        boolean hasZero = false;
        String allMoney;
        try {
            allMoney = peopleMoney;
            for (int i = 0; i < allSplitPeopleList.size(); i++) {
                KycContactEntity kycContactEntity = allSplitPeopleList.get(i);
                if (i == position) {
                    kycContactEntity.splitMoney = peopleMoney;
                } else {
                    allMoney = BigDecimalFormatUtils.add(allMoney,kycContactEntity.splitMoney,2);
                }
                if (BigDecimalFormatUtils.isZero(peopleMoney)) {
                    hasZero = true;
                }
            }
            if (hasZero) {
                tvError.setText(getString(R.string.split_amount_limit));
                tvSplit.setEnabled(false);
                tvError.setVisibility(View.VISIBLE);
                return;
            }
            String billAmount = etBillAmount.getText().toString().trim();
            billAmount = billAmount.replace(",", "");
            if (BigDecimalFormatUtils.compareBig(allMoney,billAmount)) {
                tvError.setText(getString(R.string.splitbill_16) + " " + billAmount);
                tvSplit.setEnabled(false);
                tvError.setVisibility(View.VISIBLE);
            } else if (BigDecimalFormatUtils.compareInt(allMoney,billAmount)<0) {
                if(isCheckedMy) {
                    if (position == 0) {
                        tvError.setText(getString(R.string.splitbill_16) + " " + billAmount);
                        tvSplit.setEnabled(false);
                        tvError.setVisibility(View.VISIBLE);
                    } else {
                        allSplitPeopleList.get(0).splitMoney = BigDecimalFormatUtils.add(myEntity.splitMoney,BigDecimalFormatUtils.sub(billAmount,allMoney,2),2);
                        splitPeopleAdapter.notifyDataSetChanged();
                        tvError.setVisibility(View.GONE);

                        if (peopleSize > splitReceiversLimit) {
                            tvSplit.setEnabled(false);
                        } else {
                            tvSplit.setEnabled(true);
                        }

                        if (checkMoney(billAmount,etBillDesc.getText().toString().trim())) {
                            tvSplit.setEnabled(true);
                        } else {
                            tvSplit.setEnabled(false);
                        }
                    }
                }else{
                    tvError.setText(getString(R.string.splitbill_16) + " " + billAmount);
                    tvSplit.setEnabled(false);
                    tvError.setVisibility(View.VISIBLE);
                }
            }else{
                tvError.setVisibility(View.GONE);

                if (peopleSize > splitReceiversLimit) {
                    tvSplit.setEnabled(false);
                } else {
                    tvSplit.setEnabled(true);
                }

                if (checkMoney(billAmount,etBillDesc.getText().toString().trim())) {
                    tvSplit.setEnabled(true);
                } else {
                    tvSplit.setEnabled(false);
                }
            }
        } catch (Exception e) {
            LogUtils.d(TAG, "---" + e + "---");

        }

        etBillDesc.setFocusable(true);
        etBillDesc.setFocusableInTouchMode(true);
    }

    public void saveEditData(int position, String peopleMoney) {
        etBillDesc.setFocusable(false);
        etBillDesc.setFocusableInTouchMode(false);
        boolean hasZero = false;
        String allMoney;
        try {
            allMoney = peopleMoney;
            for (int i = 0; i < allSplitPeopleList.size(); i++) {
                KycContactEntity kycContactEntity = allSplitPeopleList.get(i);
                if (i == position) {
                    kycContactEntity.splitMoney = peopleMoney;
                } else {
                    allMoney = BigDecimalFormatUtils.add(allMoney,kycContactEntity.splitMoney,2);
                }
                if (BigDecimalFormatUtils.isZero(peopleMoney)) {
                    hasZero = true;
                }
            }
            if (hasZero) {
                tvError.setText(getString(R.string.split_amount_limit));
                tvSplit.setEnabled(false);
                tvError.setVisibility(View.VISIBLE);
                return;
            }
            String billAmount = etBillAmount.getText().toString().trim();
            billAmount = billAmount.replace(",", "");
            if (BigDecimalFormatUtils.compareInt(allMoney,billAmount)==0) {
                tvError.setVisibility(View.GONE);

                if (peopleSize > splitReceiversLimit) {
                    tvSplit.setEnabled(false);
                } else {
                    tvSplit.setEnabled(true);
                }

                if (checkMoney(billAmount,etBillDesc.getText().toString().trim())) {
                    tvSplit.setEnabled(true);
                } else {
                    tvSplit.setEnabled(false);
                }
            } else {
                tvError.setText(getString(R.string.splitbill_16) + " " + billAmount);
                tvSplit.setEnabled(false);
                tvError.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            LogUtils.d(TAG, "---"+e+"---");

        }

        etBillDesc.setFocusable(true);
        etBillDesc.setFocusableInTouchMode(true);
    }

    private boolean checkMoney(String amount,String desc) {
        tvSplit.setEnabled(true);
        if (TextUtils.isEmpty(amount) || TextUtils.isEmpty(desc)) {
            tvSplit.setEnabled(false);
            return false;
        }
        amount = amount.replace(",", "");
        splitMoney = amount;

        if (allSplitPeopleList.size() > 0) {
            String allMoney = "0";
            for (int i = 0; i < allSplitPeopleList.size(); i++) {
                KycContactEntity kycContactEntity = allSplitPeopleList.get(i);
                allMoney = BigDecimalFormatUtils.add(allMoney,kycContactEntity.splitMoney,2);
            }
            if (BigDecimalFormatUtils.compareBig(allMoney,splitMoney)) {
                tvError.setText(getString(R.string.splitbill_16) + " " + amount);
                tvSplit.setEnabled(false);
                tvError.setVisibility(View.VISIBLE);
                return false;
            }
        }else {
            tvError.setVisibility(View.GONE);
            return false;
        }

        if (BigDecimalFormatUtils.compareInt(splitMoney,"0") == 0) {
            tvError.setText(getString(R.string.splitbill_16) + " " + amount);
            tvSplit.setEnabled(false);
            tvError.setVisibility(View.VISIBLE);
            return false;
        }


        if (BigDecimalFormatUtils.compareBig(splitMoney,monthLimitMoney)) {
            etBillAmount.setError(getString(R.string.splitbill_15));
            tvSplit.setEnabled(false);
            return false;
        } else if (userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
            tvSplit.setEnabled(false);
            etBillAmount.setError(getString(R.string.newhome_45));
            return false;
        } else {
            etBillAmount.setError("");
            tvSplit.setEnabled(true);
            return true;
        }

    }


    @OnClick({R.id.iv_back, R.id.tv_add_people, R.id.tv_split})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_people:
                if (selectedSplitPeopleList.size() == 0) {
                    checkPermission();
                } else {
//                    showContactDialog(contactInviteDtos);
                    mPresenter.contactInvite(phoneList);
                }
                break;
            case R.id.tv_split:
                AppsFlyerEntity appsFlyerEntity = new AppsFlyerEntity();
                appsFlyerEntity.isRegisterUser = "True";
                if (peopleSize == 0) {
                    checkPermission();
                } else {
                    if ("0.01".equals(etBillAmount.getText().toString().trim()) &&allSplitPeopleList.size()>1) {
                        tvSplit.setEnabled(false);
                    }
                    List<String> additionalData = new ArrayList<>();
                    if (allSplitPeopleList.size() > 0) {
                        peopleSize = allSplitPeopleList.size();
                        receiverInfoList.clear();
                        for (int i = 0; i < peopleSize; i++) {
                            KycContactEntity kycContactEntity = allSplitPeopleList.get(i);
                            if(!kycContactEntity.isRegistered()){
                                appsFlyerEntity.isRegisterUser = "False";
                            }
                            SplitBillPayerEntity splitBillPayerEntity = new SplitBillPayerEntity();
//                            if (i == peopleSize - 1) {
//                                splitBillPayerEntity.payerAmount = AndroidUtils.getReqTransferMoney(String.valueOf(lastMoney));
//                            } else {
//                                splitBillPayerEntity.payerAmount = AndroidUtils.getReqTransferMoney(String.valueOf(peopleMoney));
//                            }
                            splitBillPayerEntity.payerAmount = AndroidUtils.getReqTransferMoney(String.valueOf(kycContactEntity.splitMoney));
                            String payerCallingCode = kycContactEntity.getCallingCode();
                            splitBillPayerEntity.payerCallingCode = payerCallingCode;
                            if (payerCallingCode.startsWith("+")) {
                                splitBillPayerEntity.payerCallingCode = payerCallingCode.substring(1);
                            }
                            splitBillPayerEntity.payerName = kycContactEntity.getContactsName();
                            splitBillPayerEntity.payerPhone = kycContactEntity.getRequestPhoneNumber();
                            additionalData.add(kycContactEntity.getRequestPhoneNumber());
                            receiverInfoList.add(splitBillPayerEntity);
                        }
                    }

                    mPresenter.getOtpType("ActionType","18",additionalData);

//                    HashMap<String, Object> mHashMap = new HashMap<>(16);
//                    TransferEntity transferEntity = new TransferEntity();
//                    transferEntity.orderCurrencyCode = orderCurrencyCode;
//                    transferEntity.orderAmount = String.valueOf(splitMoney);
//                    orderRemark = etBillDesc.getText().toString().trim();
//                    transferEntity.remark = orderRemark;
//                    transferEntity.receiverInfoList.addAll(receiverInfoList);
//                    mHashMap.put(Constants.TRANSFER_ENTITY, transferEntity);
//                    mHashMap.put(Constants.sceneType, Constants.TYPE_SPLIT);
//                    ActivitySkipUtil.startAnotherActivity(SplitBillActivity.this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void checkPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(CreateSplitBillActivity.this, Manifest.permission.READ_CONTACTS)) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
            PermissionInstance.getInstance().getPermission(CreateSplitBillActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    if (selectedSplitPeopleList.size() == 0) {
                        ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                            @Override
                            public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                                if (contactInviteDtos != null && contactInviteDtos.size()>0) {
                                    //已经请求过接口的情况下，直接展示数据
                                    showContactDialog(phoneList);
                                    return;
                                }
                                phoneList.clear();
                                phoneList.addAll(contactEntities);
//                                showContactDialog(phoneList);
                                mPresenter.contactInvite(phoneList);
                            }
                        });
                    } else {
//                        showContactDialog(phoneList);
                        mPresenter.contactInvite(phoneList);
                    }
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(CreateSplitBillActivity.this, Manifest.permission.READ_CONTACTS)) {
                        if (!firstRejectContacts) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_30));
                        } else {
                            firstRejectContacts = false;
                        }
                    }
                }
            }, Manifest.permission.READ_CONTACTS);
        } else {
            if (selectedSplitPeopleList.size() == 0) {
                ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                    @Override
                    public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                        if (contactInviteDtos != null && contactInviteDtos.size()>0) {
                            //已经请求过接口的情况下，直接展示数据
                            showContactDialog(phoneList);
                            return;
                        }
                        phoneList.clear();
                        phoneList.addAll(contactEntities);
//                        showContactDialog(phoneList);
                        mPresenter.contactInvite(phoneList);
                    }
                });
            } else {
//                showContactDialog(phoneList);
                mPresenter.contactInvite(phoneList);
            }
        }
    }

    private void initSplitList(List<KycContactEntity> list) {
        if(list.size() == 0)return;
        allSplitPeopleList.clear();
        if (isCheckedMy) {
            if (selectedSplitPeopleList.size() > 0) {
                allSplitPeopleList.add(0, myEntity);
            }
        } else {
            if (allSplitPeopleList.contains(myEntity)) {
                allSplitPeopleList.remove(myEntity);
            }
        }
        if (list.size() > 0) {
            if (list.size() == splitReceiversLimit) {
                cbBillSwitch.setEnabled(false);
            } else {
                cbBillSwitch.setEnabled(true);
            }
            allSplitPeopleList.addAll(list);
        }
        if (allSplitPeopleList.size() > 0) {
            peopleSize = allSplitPeopleList.size();
//            if (peopleSize > splitReceiversLimit) {
//                tvSplit.setEnabled(false);
//            } else {
//                tvSplit.setEnabled(true);
//            }
            String peopleMoney = BigDecimalFormatUtils.divToDown(splitMoney,String.valueOf(peopleSize),2);
            BigDecimal bg = new BigDecimal(peopleMoney);
            if(BigDecimalFormatUtils.compareBig("0.01",peopleMoney)){
                peopleMoney = "0.01";
            }
            String lastMoney = BigDecimalFormatUtils.sub(splitMoney,BigDecimalFormatUtils.mul(peopleMoney,String.valueOf(peopleSize-1),2),2);
            if(BigDecimalFormatUtils.compareBig("0.01",lastMoney)){
                lastMoney = "0.01";
            }
            for (int i = 0; i < peopleSize; i++) {
                KycContactEntity kycContactEntity = allSplitPeopleList.get(i);
                kycContactEntity.splitMoney = peopleMoney;
                if (i == peopleSize - 1) {
                    kycContactEntity.splitMoney = lastMoney;
                }
            }
            splitPeopleAdapter.setEditMoney(peopleMoney);
            splitPeopleAdapter.setLastPeopleMoney(lastMoney);
            tvSplit.setText(getString(R.string.splitbill_10));
            llSplitPeople.setVisibility(View.VISIBLE);
            tvError.setVisibility(View.GONE);
            tvSplit.setEnabled(true);
        } else {
            tvSplit.setText(getString(R.string.splitbill_5));
            llSplitPeople.setVisibility(View.GONE);
        }
        splitPeopleAdapter.setInitMoney(true);
        etBillDesc.setFocusable(false);
        etBillDesc.setFocusableInTouchMode(false);
        splitPeopleAdapter.notifyDataSetChanged();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                etBillDesc.setFocusable(true);
                etBillDesc.setFocusableInTouchMode(true);
            }
        },200);
    }


    private void showContactDialog(List<KycContactEntity> phoneList) {
        splitContactDialog = SplitContactDialog.getInstance(mContext);
        splitContactDialog.setPhoneList(phoneList, selectedSplitPeopleList);
        splitContactDialog.setSplitReceiversLimit(splitReceiversLimit);
        splitContactDialog.setMeChecked(isCheckedMy);
        splitContactDialog.setOnAddClickListener(new SplitContactDialog.OnAddClickListener() {
            @Override
            public void getSplitList(List<KycContactEntity> peopleList) {
                selectedSplitPeopleList.clear();
                selectedSplitPeopleList.addAll(peopleList);
                initSplitList(peopleList);
                checkMoney(etBillAmount.getText().toString().trim(),etBillDesc.getText().toString().trim());
            }
        });
        splitContactDialog.setTvInviteClickListener(new SplitContactDialog.TvInviteClickListener() {
            @Override
            public void sendInvite(String phone) {
//                NotifyEntity notifyEntity = new NotifyEntity();
//                notifyEntity.msgSubject = "INVITE";
//                notifyEntity.msgResult = "INVITE";
//                notifyEntity.receiverPhone = phone;
//                List<NotifyEntity> receivers = new ArrayList<>();
//                receivers.add(notifyEntity);
//                mPresenter.sendNotify(receivers);
                String msg = mContext.getString(R.string.sprint18_12);
                String[] s = userInfoEntity.userName.split(" ");
                String body = msg.replace("XXX", s[0])
                        .replace("YYY",etBillDesc.getText().toString().trim())
                        .replace("ZZZ",etBillAmount.getText().toString().trim());
                AndroidUtils.sendSmsWithBody(mContext, BuildConfig.DefaultInternationalAreaCode+phone,body);
//
            }
        });
        splitContactDialog.showWithBottomAnim();
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {

    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {
        if (contactInviteDtos != null && contactInviteDtos.size()>0) {
            showContactDialog(phoneList);
            return;
        }
        if (contactInviteEntity != null) {
            contactInviteDtos = contactInviteEntity.getContactInviteDtos();

            for (int i = 0; i < contactInviteDtos.size(); i++) {
                KycContactEntity kycContactEntity = contactInviteDtos.get(i);
                if (kycContactEntity.getContactsName() != null) {
                    kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                }
            }

            Collections.sort(contactInviteDtos, new registerComparator());
            phoneList.clear();
            phoneList.addAll(contactInviteDtos);
            showContactDialog(contactInviteDtos);
        }
    }

    /**
     * 根据是否注册来排序
     */
    public class registerComparator implements Comparator<KycContactEntity> {

        @Override
        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getUserStatus().equals(Constants.unregistered) || o2.getUserStatus().equals(Constants.registered)) {
                return 1;
            }else if (o1.getUserStatus().equals(Constants.registered) || o2.getUserStatus().equals(Constants.unregistered)){
                return -1;
            }
            return 0;
        }
    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }


    @Override
    public void transferToContactFail(String errCode, String errMsg) {

    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {

    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {

    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {

    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {

    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {

    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {

    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if(rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            TransferEntity transferEntity = new TransferEntity();
            transferEntity.orderCurrencyCode = orderCurrencyCode;
            transferEntity.orderAmount = String.valueOf(splitMoney);
            orderRemark = etBillDesc.getText().toString().trim();
            transferEntity.remark = orderRemark;
            transferEntity.receiverInfoList.addAll(receiverInfoList);
            this.mTransferEntity = transferEntity;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
//                jumpToPwd();
            }
        }
    }

    private void requestTransfer(){
        if (mTransferEntity != null) {
            mPresenter.splitBill(mTransferEntity.orderNo, AndroidUtils.getReqTransferMoney(mTransferEntity.orderAmount),
                    mTransferEntity.orderCurrencyCode, mTransferEntity.remark,
                    mTransferEntity.receiverInfoList);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.TRANSFER_ENTITY, mTransferEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_SPLIT);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_SPLIT_BILL);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.splitbill_1));
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {
        splitBillSuccess(this,Constants.TYPE_SPLIT,transferEntity);
    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity result) {
        showTipDialog(mContext.getString(R.string.sprint17_40));
    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {

    }

    @Override
    public TransferContract.TransferToContactPresenter getPresenter() {
        return new TransferToContactPresenter();
    }
}
