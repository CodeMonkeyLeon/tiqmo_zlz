package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ViewAnimUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class TopUpCategoryAdapter extends BaseRecyclerAdapter<VouTypeAndInfoEntity.VouTypeAndInfo> {

    private Context context;
    private OnTopUpClickListener mTopUpClickListener;

    public void setVoucherClickListener(OnTopUpClickListener mTopUpClickListener) {
        this.mTopUpClickListener = mTopUpClickListener;
    }

    public TopUpCategoryAdapter(Context context, @Nullable List<VouTypeAndInfoEntity.VouTypeAndInfo> data) {
        super(R.layout.item_voucher_category, data);
        this.context = context;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VouTypeAndInfoEntity.VouTypeAndInfo item, int voucherCategory) {
        if (item == null) {
            return;
        }
        LinearLayout llVoucherCategory = baseViewHolder.getView(R.id.ll_voucher_category);
        ImageView ivStoreLogo = baseViewHolder.getView(R.id.iv_store_logo);
        ivStoreLogo.setVisibility(View.GONE);
        ImageView ivArrow = baseViewHolder.getView(R.id.iv_arrow);
        TextView tvCategory = baseViewHolder.getView(R.id.tv_voucher_category);
        RecyclerView ryVoucherAmount = baseViewHolder.getView(R.id.rv_voucher_amount);
        ryVoucherAmount.setVisibility(View.GONE);
        ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
        LocaleUtils.viewRotationY(mContext, ivArrow);

        if (voucherCategory == 0) {
            ryVoucherAmount.setVisibility(View.VISIBLE);
            ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
        }

        if (!TextUtils.isEmpty(item.voucherTypeName)) {
            tvCategory.setText(Spans.builder()
                    .text(item.voucherTypeName)
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_80black)))
                    .size(14).build());
        }
        TopUpAmountAdapter amountAdapter = new TopUpAmountAdapter(item.voucherInfos);
        GridLayoutManager manager = new GridLayoutManager(mContext, 2);
        ryVoucherAmount.setLayoutManager(manager);
        ryVoucherAmount.setAdapter(amountAdapter);
        amountAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int voucherPosition) {
                if (mTopUpClickListener != null) {
                    mTopUpClickListener.onTopUpClick(voucherCategory, voucherPosition);
                }
            }
        });
        int count = Math.min((item.voucherInfos.size() + 1) >> 1, 4);
        llVoucherCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ryVoucherAmount.measure(0, 0);
                if (ryVoucherAmount.getVisibility() == View.VISIBLE) {
                    ViewAnimUtils.hideViewWithAnim(ryVoucherAmount, 100 * count,
                            ryVoucherAmount.getMeasuredHeight(), new ViewAnimUtils.AnimationEnd() {
                                @Override
                                public void onAnimationEnd() {
                                    ivArrow.setRotationX(0);
                                }
                            });
                } else {
                    ViewAnimUtils.showViewWithAnim(ryVoucherAmount, 100 * count,
                            ryVoucherAmount.getMeasuredHeight());
                    ivArrow.setRotationX(180);
                }
            }
        });
    }

    public interface OnTopUpClickListener {
        void onTopUpClick(int topUpCategory, int topUpPosition);
    }
}
