package cn.swiftpass.wallet.tiqmo.module.register.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.NafathContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.NafathRegPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class NafathAuthActivity extends BaseCompatActivity<NafathContract.Presenter> implements NafathContract.View{

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.ll_login)
    ConstraintLayout llLogin;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.tv_error)
    TextView tvError;

    private String callingCode,phoneNo,recipientId,noStr;
    private int operateType;
    private CheckPhoneEntity checkPhoneEntity;
    private String otpType;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_reg_nafath_authentication;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack,headCircle);
        callingCode = UserInfoManager.getInstance().getCallingCode();
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if (getIntent() != null && getIntent().getExtras() != null) {
            otpType = getIntent().getExtras().getString(Constants.DATA_OTP_TYPE);
            ivBack.setVisibility(View.GONE);
            if (Constants.OTP_REG.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_12);
            } else if (Constants.OTP_FORGET_LOGIN_PD.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_31);
            }else if (Constants.OTP_CHANGE_PHONE.equals(otpType)) {
                tvTitle.setText(R.string.ChangeMobile_1);
            }
        }
        if(checkPhoneEntity != null){
            noStr = checkPhoneEntity.noStr;
            phoneNo = checkPhoneEntity.phone;
            recipientId = checkPhoneEntity.checkIdNumber;
            operateType = checkPhoneEntity.operateType;
            userInfoEntity = getUserInfoEntity();
            if(!TextUtils.isEmpty(phoneNo)){
                userInfoEntity.phone = phoneNo;
            }
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }

    @OnClick({R.id.tv_verify, R.id.tv_login,R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_verify:
                mPresenter.getNafathUrl(callingCode,phoneNo,recipientId,operateType==2? Constants.SCENE_TYPE_FORGET_PD:Constants.SCENE_TYPE_REG_PHONE);
                break;
            case R.id.tv_login:
                if (getUserInfoEntity() != null) {
                    ActivitySkipUtil.startAnotherActivity(this, LoginFastNewActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    ActivitySkipUtil.startAnotherActivity(this, RegisterActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                finish();
                break;
        }
    }

    @Override
    public void getNafathResultSuccess(NafathResultEntity result) {

    }

    @Override
    public void getNafathUrlSuccess(NafathUrlEntity nafathUrlEntity) {
        if(nafathUrlEntity != null){
            String authUrl = nafathUrlEntity.authUrl;
            String hashedState = nafathUrlEntity.hashedState;
            String redirectUrl = nafathUrlEntity.redirectUrl;
            String nafathAuthFlag = nafathUrlEntity.nafathAuthFlag;
            String firstName = nafathUrlEntity.firstName;
            String random = nafathUrlEntity.random;
            long countSeconds = nafathUrlEntity.countSeconds;

            if(checkPhoneEntity != null) {
                checkPhoneEntity.authUrl = nafathUrlEntity.authUrl;
                checkPhoneEntity.hashedState = nafathUrlEntity.hashedState;
                checkPhoneEntity.redirectUrl = nafathUrlEntity.redirectUrl;
                checkPhoneEntity.nafathAuthFlag = nafathUrlEntity.nafathAuthFlag;
                checkPhoneEntity.firstName = nafathUrlEntity.firstName;
                checkPhoneEntity.random = nafathUrlEntity.random;
                checkPhoneEntity.countSeconds = nafathUrlEntity.countSeconds;
                UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
            }

            //跳过nafath认证则直接去设置密码页面
            if(nafathUrlEntity.isSkipNafath()){
                if(operateType == 3){
                    AppClient.getInstance().getUserManager().updateUserInfo(userInfoEntity, new ResultCallback<UserInfoEntity>() {
                        @Override
                        public void onResult(UserInfoEntity userInfoEntity) {
                            showProgress(false);
//                            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_CHANGE_PHONE_SUCCESS));
                            ProjectApp.changePhoneSuccess = true;
                            ProjectApp.removeAllTaskExcludeMainStack();
                            finish();
                        }

                        @Override
                        public void onFailure(String errorCode, String errorMsg) {
                            showProgress(false);
                            showTipDialog(errorMsg);
                        }
                    });
                }else {
                    ActivitySkipUtil.startAnotherActivity(NafathAuthActivity.this, RegSetPwdOneActivity.class, RIGHT_IN);
                }
            }else {
                if (!TextUtils.isEmpty(random)) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
                    mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);
                    ActivitySkipUtil.startAnotherActivity(this, NafathResultActivity.class,mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        }
    }

    @Override
    public void getNafathUrlFail(String errorCode, String errorMsg) {
        tvError.setText(errorMsg);
        tvError.setTextColor(getColor(R.color.color_fd1d2d));
    }

    @Override
    public void getNafathResultFail(String errorCode, String errorMsg) {

    }

    @Override
    public NafathContract.Presenter getPresenter() {
        return new NafathRegPresenter();
    }
}
