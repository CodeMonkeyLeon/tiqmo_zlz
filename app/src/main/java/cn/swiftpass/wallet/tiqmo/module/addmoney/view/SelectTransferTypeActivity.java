package cn.swiftpass.wallet.tiqmo.module.addmoney.view;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanBaseActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.presenter.AddMoneyPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class SelectTransferTypeActivity extends BaseCompatActivity<AddMoneyContract.Presenter> implements AddMoneyContract.View {
    private static final String TAG = SelectTransferTypeActivity.class.getSimpleName();
    @BindView(R.id.tv_select_title)
    TextView tvSelectTitle;
    @BindView(R.id.ll_mada_card)
    LinearLayout llMadaCard;
    @BindView(R.id.ll_credit_card)
    LinearLayout llCreditCard;
    @BindView(R.id.ll_bank_transfer)
    LinearLayout llBankTransfer;
    @BindView(R.id.iv_arrow_one)
    ImageView ivArrowOne;
    @BindView(R.id.iv_arrow_two)
    ImageView ivArrowTwo;

    private static final int AMOUNT_100 = 100;
    private static final int AMOUNT_200 = 200;
    private static final int AMOUNT_1000 = 1000;
    private static final int AMOUNT_2000 = 2000;

    private BottomDialog editMoneyDialog;
    private double money, limitMoney;
    private double minLimitMoney;

    private double maxLimitBalance;

    boolean isBankTransfer;

    private String shareIbanNum;

    private int transferCardType = 0;
    private String cardType = "MADA";
    private AllCardEntity allCardEntity;

    private TransferLimitEntity transferLimitEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_select_transfer_type;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
        LocaleUtils.viewRotationY(mContext,ivArrowOne,ivArrowTwo);
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(0,0);
//        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @OnClick({R.id.ll_mada_card, R.id.ll_credit_card, R.id.ll_bank_transfer})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_mada_card:
                if(AndroidUtils.isCloseUse(this,Constants.addMoney_cards))return;
                transferCardType = 1;
                cardType = "MADA";
                isBankTransfer = false;
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
            case R.id.ll_credit_card:
                if(AndroidUtils.isCloseUse(this,Constants.addMoney_cards))return;
                cardType = "CREDIT";
                transferCardType = 2;
                isBankTransfer = false;
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
            case R.id.ll_bank_transfer:
                if(AndroidUtils.isCloseUse(this,Constants.addMoney_bank))return;
                isBankTransfer = true;
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
            default:break;
        }
    }

    private void showAddMoneyDialog(TransferLimitEntity transferLimitEntity) {
        editMoneyDialog = new BottomDialog(mContext,R.style.DialogBottomInTransfer);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_add_money, null);
        TextView tvLimit = contentView.findViewById(R.id.tv_limit);
        TextView tvAdd = contentView.findViewById(R.id.tv_add);
        TextView tvMoneyCurrency = contentView.findViewById(R.id.tv_money_currency);
        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        EditTextWithDel etAmount = contentView.findViewById(R.id.et_amount);
        etAmount.getTlEdit().setHint(getString(R.string.add_money_9));
        TextView tv100 = contentView.findViewById(R.id.tv_100);
        TextView tv200 = contentView.findViewById(R.id.tv_200);
        TextView tv1000 = contentView.findViewById(R.id.tv_1000);
        TextView tv2000 = contentView.findViewById(R.id.tv_2000);
        tv100.setText(String.valueOf(AMOUNT_100));
        tv200.setText(String.valueOf(AMOUNT_200));
        tv1000.setText(String.valueOf(AMOUNT_1000));
        tv2000.setText(String.valueOf(AMOUNT_2000));
        String minLimit = transferLimitEntity.tranMinLimitAmt;
        minLimitMoney = 10;
        try {
            minLimitMoney = AndroidUtils.getTransferMoneyNumber(minLimit);
        } catch (Exception e) {
            LogUtils.d(TAG, "---"+e+"---");
        }
        String monthLimit = transferLimitEntity.monthLimitAmt;
        tvLimit.setText(Spans.builder().text(AndroidUtils.getTransferMoney(monthLimit)).size(12)
                .text(" " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode())).size(10).build());
        try {
            limitMoney = AndroidUtils.getTransferMoneyNumber(monthLimit);
            maxLimitBalance = AndroidUtils.getTransferMoneyNumber(transferLimitEntity.maxLimitBalance);
        } catch (Exception e) {
            limitMoney = Integer.MAX_VALUE;
        }

        //两位小数过滤
        etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                if (TextUtils.isEmpty(amount)) {
                    tvMoneyCurrency.setVisibility(View.GONE);
                    tvAdd.setEnabled(false);
                } else {
                    try {
                        money = Double.parseDouble(amount);
                        if (money >= minLimitMoney && money <= limitMoney && !userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                            tvAdd.setEnabled(true);
                        } else {
                            tvAdd.setEnabled(false);
                        }
                    } catch (Exception e) {
                        tvAdd.setEnabled(false);
                    }
                    if (money != AMOUNT_100) {
                        tv100.setSelected(false);
                    }
                    if (money != AMOUNT_200) {
                        tv200.setSelected(false);
                    }
                    if (money != AMOUNT_1000) {
                        tv1000.setSelected(false);
                    }
                    if (money != AMOUNT_2000) {
                        tv2000.setSelected(false);
                    }
                    tvMoneyCurrency.setVisibility(View.VISIBLE);
                }
            }
        });

        etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (!hasFocus) {
                    checkAmount(etAmount, minLimit);
                }
            }
        });

        tv100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv100.setSelected(true);
                tv200.setSelected(false);
                tv1000.setSelected(false);
                tv2000.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_100));
                checkAmount(etAmount, minLimit);
            }
        });

        tv200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv200.setSelected(true);
                tv100.setSelected(false);
                tv1000.setSelected(false);
                tv2000.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_200));
                checkAmount(etAmount, minLimit);
            }
        });

        tv1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1000.setSelected(true);
                tv200.setSelected(false);
                tv100.setSelected(false);
                tv2000.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_1000));
                checkAmount(etAmount, minLimit);
            }
        });

        tv2000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv2000.setSelected(true);
                tv200.setSelected(false);
                tv1000.setSelected(false);
                tv100.setSelected(false);

                etAmount.setContentText(String.valueOf(AMOUNT_2000));
                checkAmount(etAmount, minLimit);
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                String money = etAmount.getEditText().getText().toString().trim();
                money = money.replace(",", "");
//                if (isGooglePay) {
//                    mPresenter.getTransferFee(Constants.TYPE_ADDMONEY, AndroidUtils.getReqTransferMoney(money), UserInfoManager.getInstance().getCurrencyCode());
//                } else {
                HashMap<String, Object> mHashMap = new HashMap<>(16);
                mHashMap.put(Constants.ADD_MONEY, money);
                mHashMap.put(Constants.allCardEntity, allCardEntity);
                mHashMap.put(Constants.transferCardType, cardType);
                mHashMap.put(Constants.CARD_FROM, Constants.CARD_FROM_ADD_MONEY);
                ActivitySkipUtil.startAnotherActivity(SelectTransferTypeActivity.this, SelectCardListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                finish();
                if (editMoneyDialog != null) {
                    editMoneyDialog.dismiss();
                }
//                }
            }
        });

        editMoneyDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        editMoneyDialog.setContentView(contentView);
        Window dialogWindow = editMoneyDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        editMoneyDialog.showWithBottomAnim();
    }

    private void checkAmount(EditTextWithDel etAmount, String minLimit) {
        String amount = etAmount.getEditText().getText().toString().trim();
        amount = amount.replace(",", "");
        String formatAmount = AmountUtil.dataFormat(amount);
        if (!TextUtils.isEmpty(amount)) {
            etAmount.getEditText().setText(formatAmount);
            if (money > limitMoney) {
                etAmount.showErrorViewWithMsg(getString(R.string.add_money_12));
            } else if (money < minLimitMoney) {
                etAmount.showErrorViewWithMsg(getString(R.string.add_money_13) + " " +
                        AndroidUtils.getTransferMoney(minLimit) + " " +
                        LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
            } else if (userInfoEntity.isOverLimitBalance(userInfoEntity, amount, maxLimitBalance)) {
                etAmount.showErrorViewWithMsg(getString(R.string.newhome_45));
            }
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity mTransferLimitEntity) {
        this.transferLimitEntity = mTransferLimitEntity;
        SpUtils.getInstance().setAvailableBalance(transferLimitEntity.availableBalance);
        if (isBankTransfer) {
            mPresenter.getIbanActivityDetail();
        } else {
            mPresenter.getCardList("");
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    private void showIbankDetailDialog() {
        BottomDialog bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_ibank_detail, null);
        TextView tvBankName = contentView.findViewById(R.id.tv_bank_name);
        TextView tvBankNumber = contentView.findViewById(R.id.tv_bank_number);
        LinearLayout llShare = contentView.findViewById(R.id.ll_share);
        LinearLayout llCopy = contentView.findViewById(R.id.ll_copy);
        LinearLayout llContent = contentView.findViewById(R.id.ll_content);

        if (userInfoEntity != null) {
            tvBankName.setText(userInfoEntity.accountName);
            String ibanNum = userInfoEntity.ibanNum;
            if (!TextUtils.isEmpty(ibanNum)) {
                shareIbanNum = AndroidUtils.changeBanNum(ibanNum);
                tvBankNumber.setText(shareIbanNum);
            }
        }

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downShareImage(llContent, shareIbanNum);
            }
        });

        llCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(-1, 2500)) {
                    return;
                }
                String bankNumber = tvBankNumber.getText().toString().trim();
                AndroidUtils.setClipboardText(mContext, bankNumber, true);
                showTipDialog(getString(R.string.iban_copy));
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    private void showIbankOneDialog(TransferLimitEntity transferLimitEntity,IbanBaseActivityEntity ibanBaseActivityEntity) {
        BottomDialog bottomDialog = new BottomDialog(mContext,R.style.DialogBottomInTransfer);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_ibank_money, null);
        TextView tvConfirm = contentView.findViewById(R.id.tv_confirm);
        TextView tvTwoContent = contentView.findViewById(R.id.tv_two_content);

        LinearLayout llIbanActivity = contentView.findViewById(R.id.ll_iban_activity);
        TextView tvIbanContent = contentView.findViewById(R.id.tv_iban_content);

        if(ibanBaseActivityEntity != null){
            String activityNo = ibanBaseActivityEntity.activityNo;
            String activityEndTime = ibanBaseActivityEntity.activityEndTime;
            IbanActivityEntity ibanMarketActivityInfo2App = ibanBaseActivityEntity.ibanMarketActivityInfo2App;
            if(ibanMarketActivityInfo2App != null){
                llIbanActivity.setVisibility(View.VISIBLE);
                String discountModel = ibanMarketActivityInfo2App.discountModel;
                String discountValue = ibanMarketActivityInfo2App.discountValue;
                String currencyCode = ibanMarketActivityInfo2App.currencyCode;
                if("2".equals(discountModel)){
                    //折扣
                    String discountContent = AndroidUtils.getTransferStringMoney(discountValue)+"%";
                    String discountMsg = mContext.getString(R.string.sprint19_39).replace("XXX",discountContent)
                            +" "+ mContext.getString(R.string.sprint19_40)+" "+activityEndTime;
                    //固定金额
                    tvIbanContent.setText(AndroidUtils.getHighLightText(mContext,discountMsg, discountContent,LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"));
                }else if("1".equals(discountModel)){
                    String discountContent = AndroidUtils.getTransferStringMoney(discountValue)+" "+currencyCode;
                    String discountMsg = mContext.getString(R.string.sprint19_39).replace("XXX",discountContent)
                            +" "+ mContext.getString(R.string.sprint19_40)+" "+activityEndTime;
                    //固定金额
                    tvIbanContent.setText(AndroidUtils.getHighLightText(mContext,discountMsg, discountContent,LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"));
                }
            }
        }

        String minLimit = transferLimitEntity.tranMinLimitAmt;
        String monthLimit = transferLimitEntity.monthLimitAmt;
        double monthLimitMoney = 0, maxBanlanceLimit = 0;
        minLimitMoney = 5;
        try {
            minLimitMoney = AndroidUtils.getTransferMoneyNumber(minLimit);
            monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimit);
            maxBanlanceLimit = AndroidUtils.getTransferMoneyNumber(transferLimitEntity.maxLimitBalance) - userInfoEntity.getAvalibleBalanceMoney();

            String limitMsg = getString(R.string.add_money_34);
            if (monthLimitMoney > maxBanlanceLimit) {
                limitMsg = getString(R.string.newhome_40);
            }
            tvTwoContent.setText(Spans.builder()
                    .text(limitMsg).color(mContext.getColor(R.color.color_a3a3a3)).size(14)
                    .text(" " + AmountUtil.dataFormat(String.valueOf(Math.min(monthLimitMoney, maxBanlanceLimit))) + " " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode())).color(mContext.getColor(R.color.color_fc4f00)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(". " + getString(R.string.add_money_35)).color(mContext.getColor(R.color.color_a3a3a3)).size(14)
                    .text(" " + "7.00" + " " + LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()) + " ").color(mContext.getColor(R.color.color_fc4f00)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(getString(R.string.add_money_36)).color(mContext.getColor(R.color.color_a3a3a3)).size(14)
                    .build());
        } catch (Exception e) {
            LogUtils.d(TAG, "---"+e+"---");
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomDialog != null) {
                    bottomDialog.dismiss();
                }
                showIbankDetailDialog();
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void googlePaySuccess(CardNotifyEntity cardNotifyEntity) {

    }

    @Override
    public void googlePayFail(String errCode, String errMsg) {

    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {

    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {

    }

    @Override
    public void getCardListSuccess(AllCardEntity mAllCardEntity) {
        this.allCardEntity = mAllCardEntity;
        if(mAllCardEntity != null){
            showAddMoneyDialog(transferLimitEntity);
//            if(transferCardType == 1){
//            }else {
//                HashMap<String, Object> mHashMap = new HashMap<>();
//                mHashMap.put(Constants.allCardEntity, allCardEntity);
//                mHashMap.put(Constants.transferCardType, transferCardType);
//                ActivitySkipUtil.startAnotherActivity(SelectTransferTypeActivity.this, SelectCardListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                finish();
//            }
//            List<CardEntity> madaCardInfoList = allCardEntity.madaCardInfoList;
//            List<CardEntity> creditCardInfoList = allCardEntity.creditCardInfoList;
        }
    }

    @Override
    public void getIbanActivityDetailSuccess(IbanBaseActivityEntity ibanBaseActivityEntity) {
        showIbankOneDialog(transferLimitEntity,ibanBaseActivityEntity);
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public AddMoneyContract.Presenter getPresenter() {
        return new AddMoneyPresenter();
    }
}
