package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

/**
 * 原订单页面
 */
public class OrginalTransactionActivity extends BaseCompatActivity {

    private CardNotifyEntity cardNotifyEntity;
    private String transferType;
    private int sceneType;

    private boolean fromHistory = true;

    private boolean isFinishInside;

    private TransferDetailFragment transferDetailFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_orginal_transaction;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();

        if (getIntent() != null && getIntent().getExtras() != null) {
            cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_MONEY_ENTITY);
            transferType = getIntent().getExtras().getString(Constants.sceneType);
            fromHistory = getIntent().getExtras().getBoolean(Constants.fromHistory, true);
            if (cardNotifyEntity != null) {
                sceneType = cardNotifyEntity.sceneType;
                cardNotifyEntity.transferType = transferType;
                cardNotifyEntity.fromHistory = fromHistory;
//                if (sceneType == 11) {
                int height = AndroidUtils.dip2px(mContext, 550f);
                this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
//                }

                transferDetailFragment = TransferDetailFragment.getInstance(cardNotifyEntity, true);
                transferDetailFragment.setOnButtonClickListener(new TransferDetailFragment.OnButtonClickListener() {
                    @Override
                    public void onOrginalClick() {
                        isFinishInside = true;
                        finish();
                    }
                });
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fl_transfer_detail, transferDetailFragment);
                ft.commit();
            }
        }
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
        if (!isFinishInside) {
            ProjectApp.removeTransferDetailTask();
        }
    }
}
