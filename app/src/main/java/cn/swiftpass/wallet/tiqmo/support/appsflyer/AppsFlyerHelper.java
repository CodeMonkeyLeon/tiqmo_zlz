package cn.swiftpass.wallet.tiqmo.support.appsflyer;

import com.appsflyer.AppsFlyerLib;

import java.util.HashMap;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class AppsFlyerHelper {

    private static final String app_key = "BSTyhFsCJPM7k26Jya8yLA";

    private static final String event_sign_up = "sign_up";
    private static final String event_kyc_with_code = "kyc_with_code";
    private static final String event_kyc_no_code = "kyc_no_code";
    private static final String event_login = "af_login";
    private static final String event_add_payment_info = "af_add_payment_info";
    private static final String event_first_top_up = "first_top_up";
    private static final String event_top_up = "top_up";
    private static final String event_add_beneficiary_l = "add_beneficiary_l";
    private static final String event_local_transfer = "local_transfer";
    private static final String event_cash_withdrawal = "cash_withdrawal";
    private static final String event_p2p = "p2p";
    private static final String event_bill_split = "bill_split";
    private static final String event_voucher = "voucher";
    private static final String event_mobile_topup = "mobile_topup";
    private static final String event_int_mobile_topup = "int_mobile_topup";
    private static final String event_request_money = "request_money";
    private static final String event_add_beneficiary_r = "add_beneficiary_r";
    private static final String event_remittance = "remittance";

    private AppsFlyerHelper() {
    }

    private static class AppsFlyerHolder {
        public static AppsFlyerHelper appsFlyerHelper = new AppsFlyerHelper();
    }

    public static AppsFlyerHelper getInstance() {
        return AppsFlyerHolder.appsFlyerHelper;
    }

    public void sendEvent(String eventType, HashMap<String, Object> eventData) {
        AppsFlyerLib.getInstance().logEvent(ProjectApp.getContext(), eventType, eventData);
    }

    public void sendUserId(String eventType, String userId,HashMap<String, Object> eventData){
        AppsFlyerLib.getInstance().setCustomerUserId(userId);
        AppsFlyerLib.getInstance().logEvent(ProjectApp.getContext(), eventType, eventData);
    }

    public void sendSignUpEvent(String userId) {
        HashMap<String, Object> eventData = new HashMap<>();
        sendUserId(event_sign_up, userId,eventData);
    }

    public void sendKycWithCodeEvent(UserInfoEntity userInfoEntity) {
        if (userInfoEntity == null) {
            return;
        }
        HashMap<String, Object> eventData = new HashMap<>();
        sendUserId(event_kyc_with_code,userInfoEntity.userId, eventData);
    }

    public void sendKycNoCodeEvent(UserInfoEntity userInfoEntity) {
        if (userInfoEntity == null) {
            return;
        }
        HashMap<String, Object> eventData = new HashMap<>();
        sendUserId(event_kyc_no_code,userInfoEntity.userId, eventData);
    }

    public void sendLoginEvent(UserInfoEntity userInfoEntity) {
        if (userInfoEntity == null) {
            return;
        }
        HashMap<String, Object> eventData = new HashMap<>();
        sendUserId(event_login,userInfoEntity.userId, eventData);
    }

    //完成绑卡流程
    public void sendAddCardEvent() {
        HashMap<String, Object> eventData = new HashMap<>();
        sendEvent(event_add_payment_info, eventData);
    }

    //成功完成首次充值流程
    public void sendFirstTopupEvent(String source){
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("source",source);
        sendEvent(event_first_top_up, eventData);
    }

    //成功完成非首次充值
    public void sendTopUpEvent(String source){
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("source",source);
        sendEvent(event_top_up, eventData);
    }

    //完成添加受益人
    public void sendAddBeneEvent(){
        HashMap<String, Object> eventData = new HashMap<>();
        sendEvent(event_add_beneficiary_l, eventData);
    }

    //成功(发起?)本地转账
    public void sendTransferToLocalBank(String purpose){
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("af_content",purpose);
        sendEvent(event_local_transfer, eventData);
    }

    //成功的(发起)提现交易
    public void sendWithdrawEvent(){
        HashMap<String, Object> eventData = new HashMap<>();
        sendEvent(event_cash_withdrawal, eventData);
    }

    public void sendP2PEvent(String af_validated,String purpose) {
        HashMap<String, Object> eventData = new HashMap<>();

        eventData.put("af_validated", af_validated);
        eventData.put("af_content",purpose);
        sendEvent(event_p2p, eventData);
    }

    public void sendBillSplitEvent(String af_validated,String purpose) {
        HashMap<String, Object> eventData = new HashMap<>();

        eventData.put("af_validated", af_validated);
        eventData.put("af_content",purpose);
        sendEvent(event_bill_split, eventData);
    }

    public void sendVoucherEvent(String voucherName) {
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("af_content",voucherName);
        sendEvent(event_voucher, eventData);
    }

    public void sendMobileTopupEvent(String categoryName) {
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("af_description",categoryName);
        sendEvent(event_mobile_topup, eventData);
    }

    public void sendIntMobileTopupEvent(String categoryName,String desitinationCode) {
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("af_description",categoryName);
        eventData.put("af_destination_b",desitinationCode);
        sendEvent(event_int_mobile_topup, eventData);
    }

    //成功的请求转账
    public void sendRequestTransferEvent() {
        HashMap<String, Object> eventData = new HashMap<>();
        sendEvent(event_request_money, eventData);
    }

    //成功的国际转账添加收益人
    public void sendAddImrBeneEvent() {
        HashMap<String, Object> eventData = new HashMap<>();
        sendEvent(event_add_beneficiary_r, eventData);
    }


    //成功的国际转账成功
    public void sendImrTransferEvent(String purpose,String desitinationCode) {
        HashMap<String, Object> eventData = new HashMap<>();
        eventData.put("af_content",purpose);
        eventData.put("af_destination_b",desitinationCode);
        sendEvent(event_remittance, eventData);
    }

}
