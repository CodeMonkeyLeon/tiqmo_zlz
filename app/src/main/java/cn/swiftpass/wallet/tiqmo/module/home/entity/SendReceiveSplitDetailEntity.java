package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SendReceiveSplitDetailEntity extends BaseEntity {
    private String splitType;
    private String payerCount;
    private String totalAmount;
    private String paidAmount;
    private String paidProportion;
    private String unpaidAmount;
    private String unpaidProportion;
    private String rejectedAmount;
    private String rejectedProportion;
    private String splitTitel;
    private String splitTime;
    private String originalOrderNo;
    private String orderNo;
    //是否可提示 disable-不可提示，able-可提示
    private String remind;
    //订单状态  C：未支付  S：已支付 V：拒绝
    private String paymentStatus;
    private String currency;
    //需要支付金额
    private String myReceiveAmount;
    private String orderRelevancy;

    private List<SplitPayerDtos> splitPayerDtos;
    public String getOrderRelevancy() {
        return this.orderRelevancy;
    }

    public void setOrderRelevancy(final String orderRelevancy) {
        this.orderRelevancy = orderRelevancy;
    }
    public String getSplitType() {
        return this.splitType;
    }

    public void setSplitType(final String splitType) {
        this.splitType = splitType;
    }

    public String getPayerCount() {
        return this.payerCount;
    }

    public void setPayerCount(final String payerCount) {
        this.payerCount = payerCount;
    }

    public String getTotalAmount() {
        return this.totalAmount;
    }

    public void setTotalAmount(final String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaidAmount() {
        return this.paidAmount;
    }

    public void setPaidAmount(final String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaidProportion() {
        return this.paidProportion;
    }

    public void setPaidProportion(final String paidProportion) {
        this.paidProportion = paidProportion;
    }

    public String getUnpaidAmount() {
        return this.unpaidAmount;
    }

    public void setUnpaidAmount(final String unpaidAmount) {
        this.unpaidAmount = unpaidAmount;
    }

    public String getUnpaidProportion() {
        return this.unpaidProportion;
    }

    public void setUnpaidProportion(final String unpaidProportion) {
        this.unpaidProportion = unpaidProportion;
    }

    public String getRejectedAmount() {
        return this.rejectedAmount;
    }

    public void setRejectedAmount(final String rejectedAmount) {
        this.rejectedAmount = rejectedAmount;
    }

    public String getRejectedProportion() {
        return this.rejectedProportion;
    }

    public void setRejectedProportion(final String rejectedProportion) {
        this.rejectedProportion = rejectedProportion;
    }

    public String getSplitTitel() {
        return this.splitTitel;
    }

    public void setSplitTitel(final String splitTitel) {
        this.splitTitel = splitTitel;
    }

    public String getSplitTime() {
        return this.splitTime;
    }

    public void setSplitTime(final String splitTime) {
        this.splitTime = splitTime;
    }

    public String getOriginalOrderNo() {
        return this.originalOrderNo;
    }

    public void setOriginalOrderNo(final String originalOrderNo) {
        this.originalOrderNo = originalOrderNo;
    }

    public String getOrderNo() {
        return this.orderNo;
    }

    public void setOrderNo(final String orderNo) {
        this.orderNo = orderNo;
    }

    public String getRemind() {
        return this.remind;
    }

    public void setRemind(final String remind) {
        this.remind = remind;
    }

    public String getPaymentStatus() {
        return this.paymentStatus;
    }

    public void setPaymentStatus(final String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public String getMyReceiveAmount() {
        return this.myReceiveAmount;
    }

    public void setMyReceiveAmount(final String myReceiveAmount) {
        this.myReceiveAmount = myReceiveAmount;
    }

    public List<SplitPayerDtos> getSplitPayerDtos() {
        return this.splitPayerDtos;
    }

    public void setSplitPayerDtos(final List<SplitPayerDtos> splitPayerDtos) {
        this.splitPayerDtos = splitPayerDtos;
    }

    public static class SplitPayerDtos extends BaseEntity{
        private String paymentStatus;
        private String payerName;
        private String iconUrl;
        private String currency;
        private String amount;


        public String getPaymentStatus() {
            return this.paymentStatus;
        }

        public void setPaymentStatus(final String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getPayerName() {
            return this.payerName;
        }

        public void setPayerName(final String payerName) {
            this.payerName = payerName;
        }

        public String getIconUrl() {
            return this.iconUrl;
        }

        public void setIconUrl(final String iconUrl) {
            this.iconUrl = iconUrl;
        }

        public String getCurrency() {
            return this.currency;
        }

        public void setCurrency(final String currency) {
            this.currency = currency;
        }

        public String getAmount() {
            return this.amount;
        }

        public void setAmount(final String amount) {
            this.amount = amount;
        }
    }
}
