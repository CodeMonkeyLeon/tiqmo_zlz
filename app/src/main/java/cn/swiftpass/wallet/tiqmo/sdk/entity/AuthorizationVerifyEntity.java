package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by YZX on 2018年11月05日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class AuthorizationVerifyEntity {
    private boolean firstAuthorization;

    public boolean isFirstAuthorization() {
        return firstAuthorization;
    }

    public void setFirstAuthorization(boolean firstAuthorization) {
        this.firstAuthorization = firstAuthorization;
    }
}
