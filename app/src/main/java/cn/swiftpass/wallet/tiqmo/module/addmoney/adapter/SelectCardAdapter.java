package cn.swiftpass.wallet.tiqmo.module.addmoney.adapter;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SelectCardAdapter extends BaseRecyclerAdapter<CardEntity> {
    public SelectCardAdapter(@Nullable List<CardEntity> data) {
        super(R.layout.item_select_card,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, CardEntity cardEntity, int position) {
        String cardNo = cardEntity.getCardNo();
        String expire = cardEntity.getExpire();
        String expireFlag = cardEntity.expireFlag;

        TextView tvCardExpire = baseViewHolder.getView(R.id.tv_card_expire);
        if("1".equals(expireFlag)){
            tvCardExpire.setTextColor(mContext.getColor(R.color.color_fc4f00));
        }
        String custName = cardEntity.getCustName();
        StringBuffer sb = new StringBuffer();
        sb.append(expire).insert(2,"/");
        if(!TextUtils.isEmpty(custName)){
            baseViewHolder.setText(R.id.tv_card_number, custName);
        }else {
            baseViewHolder.setText(R.id.tv_card_number, "**** "+cardNo.substring(12,16));
        }
        baseViewHolder.setText(R.id.tv_card_expire,mContext.getString(R.string.sprint11_38)+" "+sb.toString());
        ImageView ivCard = baseViewHolder.getView(R.id.iv_card);
        int cardLogoRes = AndroidUtils.getCardLogo(cardEntity.getCardOrganization());
        if(cardLogoRes != -1) {
            ivCard.setImageResource(ThemeSourceUtils.getSourceID(mContext, cardLogoRes));
        }
        baseViewHolder.addOnClickListener(R.id.iv_card_delete);

    }
}
