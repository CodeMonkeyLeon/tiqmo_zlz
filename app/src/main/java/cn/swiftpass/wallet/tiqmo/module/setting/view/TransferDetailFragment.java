package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SplitDetailAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.NotificationContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.DownloadFileUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ExtendEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.NotificationPresenter;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.TransferPdfEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherTAndCDialogFrag;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherDetailEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.DownloadUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.QRCodeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.EInvoiceNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OtherInfoDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

/**
 * 交易详情页面fragment
 */
public class TransferDetailFragment extends BaseFragment<NotificationContract.Presenter> implements NotificationContract.View {
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.ll_time)
    LinearLayout llTime;
    @BindView(R.id.tv_pay_method)
    TextView tvPayMethod;
    @BindView(R.id.ll_payment_method)
    LinearLayout llPaymentMethod;
    @BindView(R.id.tv_pay_method_title)
    TextView tvPayMethodTitle;
    @BindView(R.id.ll_method)
    LinearLayout llMethod;
    @BindView(R.id.tv_number_title)
    TextView tvNumberTitle;
    @BindView(R.id.tv_transcation_number)
    TextView tvTranscationNumber;
    @BindView(R.id.ll_order_number)
    ConstraintLayout llOrderNumber;
    @BindView(R.id.transfer_number_line)
    View transferNumberLine;
    @BindView(R.id.iv_show_qrcode)
    ImageView ivShowQrcode;
    @BindView(R.id.tv_transfer_id_title)
    TextView tvTransferIdTitle;
    @BindView(R.id.tv_transfer_id)
    TextView tvTransferId;
    @BindView(R.id.iv_orginal_detail)
    ImageView ivOrginalDetail;
    @BindView(R.id.iv_orginal_detail_down)
    ImageView iv_orginal_detail_down;
    @BindView(R.id.con_orginal_id)
    ConstraintLayout conOrginalId;
    @BindView(R.id.tv_terms_conditions)
    TextView tvTermsConditions;
    @BindView(R.id.iv_terms_conditions)
    ImageView ivTermsConditions;
    @BindView(R.id.con_terms_conditions)
    ConstraintLayout conTermsConditions;
    @BindView(R.id.ll_base_extend)
    LinearLayout llBaseExtend;
    @BindView(R.id.con_base_extend)
    ConstraintLayout conBaseExtend;
    @BindView(R.id.tv_show_more)
    TextView tvShowMore;
    @BindView(R.id.iv_show_more)
    ImageView ivShowMore;
    @BindView(R.id.ll_show_more)
    LinearLayout llShowMore;
    @BindView(R.id.iv_pay)
    ImageView ivPay;
    @BindView(R.id.tv_split)
    TextView tvSplit;
    @BindView(R.id.tv_download)
    TextView tvDownload;
    @BindView(R.id.tv_share)
    TextView tvShare;
    @BindView(R.id.ll_share_content)
    LinearLayout llShareContent;
    @BindView(R.id.con_share)
    ConstraintLayout conShare;
    @BindView(R.id.tv_download_two)
    TextView tvDownloadTwo;
    @BindView(R.id.tv_split_two)
    TextView tvSplitTwo;
    @BindView(R.id.tv_share_two)
    TextView tvShareTwo;
    @BindView(R.id.ll_share_two)
    LinearLayout llShareTwo;
    @BindView(R.id.iv_pay_method)
    RoundedImageView ivPayMethod;
    @BindView(R.id.tv_pay_title)
    TextView tvPayTitle;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.iv_check)
    ImageView ivCheck;
    @BindView(R.id.tv_notify)
    TextView tvNotify;
    @BindView(R.id.tv_view_details)
    TextView tvViewDetails;
    @BindView(R.id.tv_top_line)
    View tvTopLine;
    @BindView(R.id.tv_name_desc_id)
    TextView tvNameDescId;
    @BindView(R.id.tv_name_desc)
    TextView tvNameDesc;
    @BindView(R.id.tv_to_name)
    TextView tvToName;
    @BindView(R.id.con_transfer)
    RelativeLayout conTransfer;
    @BindView(R.id.tv_two_line)
    View tvTwoLine;
    @BindView(R.id.ll_extend)
    LinearLayout llExtend;
    @BindView(R.id.con_extend)
    ConstraintLayout conExtend;
    @BindView(R.id.ll_extend_main)
    LinearLayout llExtendMain;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.con_topView)
    LinearLayout conTopView;
    @BindView(R.id.con_bottomView)
    LinearLayout conBottomView;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_original_back)
    LinearLayout llOriginalBack;
    @BindView(R.id.ll_title)
    LinearLayout llTitle;
    @BindView(R.id.ll_loyalty_anim)
    LinearLayout ll_loyalty_anim;
    @BindView(R.id.anim_loyalty)
    LottieAnimationView anim_loyalty;
    @BindView(R.id.tv_ori_money)
    TextView tvOriMoney;
    @BindView(R.id.ll_status)
    LinearLayout llStatus;
    @BindView(R.id.con_voucherCode)
    ConstraintLayout conVoucherCode;
    @BindView(R.id.tv_voucher_title)
    TextView tvVoucherTitle;
    @BindView(R.id.tv_voucher_code)
    TextView tvVoucherCode;
    @BindView(R.id.iv_voucher_copy)
    ImageView ivVoucherCopy;
    @BindView(R.id.ll_details)
    LinearLayout llDetails;
    @BindView(R.id.tv_details)
    TextView tvDetails;
    @BindView(R.id.iv_result_details)
    ImageView ivResultDetails;
    @BindView(R.id.con_other_info)
    ConstraintLayout conOtherInfo;

    private ObjectAnimator objectButtomAnimation;
    private ObjectAnimator objectTopAnimation;
    private ObjectAnimator objectLoyaltyAnimation;
    private ValueAnimator scaleAnimation;
    private Animation scaleTvAnimation;
    private int allHeight;
    private CardNotifyEntity cardNotifyEntity;

    private Bitmap codeBitmap;

    private String dateTime, paymentMethod, orderNo, orderCurrencyCode, outTradeNo;
    private String transferType;

    private String originalOrderNo;
    /**
     * 5 手机充值 6 国际账单 7 钱包转账 8 请求转账 9 IBAN转账和提现 10 余额充值 11 国际汇款 12 QR支付 13 余额充值退款
     * 14 QR转账 15 消费券购买 18 拆分账单 19 返现订单 20 立减订单  21 邀请码返现 22 退款订单
     * 23 国际话费充值  25 NI卡费  27和29本地账单 28慈善 36 Paybill退款 40卡相关的手续费
     */
    private int sceneType;
    private int subSceneType;
    /**
     * C 初始化 W 处理中 R 认证中 S 成功 V 已关闭 E 失败 X 转入退款 Y 全部退款 B 撤销
     */
    private String orderStatus;
    /**
     * R 充值 P 支付 T 转账 CB 返现 CR返现退还 C 收款 W 提现 D 退款 S 支付结算 E 退款结算
     */
    private String orderType;

    private BottomDialog splitDialog;

    private Bitmap bigBitmap;

    private String subject;

    private boolean fromHistory = false;

    private String shareDesc;
    // 代金券的 pin 码  如果不为空  则显示代金券码布局
    private String voucherPinCode;

    //是否是原订单
    private boolean isOrginalOrder = false;

    //原金额
    private String orderAmount;
    //实际交易显示金额
    private String transferMoney;

    //优惠类型 1：返现 2：立减
    public String discountType;
    //返现订单号
    public String cashBackOrderNo;

    private int maxHeight, minHeight;

    private String popupInfo;

    private List<ExtendEntity> extendProperties = new ArrayList<>();
    //是否展示 发票 按钮 1-展示；0-不展示
    private int isShowEInvoice;
    private String invoiceType;

    private boolean isShare = false;

    private boolean isNeedDownloadFile = false;

    public static TransferDetailFragment getInstance(CardNotifyEntity cardNotifyEntity, boolean isOrginalOrder) {
        TransferDetailFragment transferDetailFragment = new TransferDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("cardNotifyEntity", cardNotifyEntity);
        bundle.putSerializable("isOrginalOrder", isOrginalOrder);
        transferDetailFragment.setArguments(bundle);
        return transferDetailFragment;
    }


    OnButtonClickListener onButtonClickListener;

    public interface OnButtonClickListener {
        void onOrginalClick();
    }

    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener) {
        this.onButtonClickListener = onButtonClickListener;
    }

    private void startMethodAnimation(boolean openMethod) {
        if (openMethod) {
            llMethod.setVisibility(View.VISIBLE);
            llShareContent.setVisibility(View.VISIBLE);
            if (codeBitmap != null) {
                ivPay.setImageBitmap(codeBitmap);
            }
        } else {
            llMethod.setVisibility(View.GONE);
            llShareContent.setVisibility(View.GONE);
            ivPay.setImageBitmap(bigBitmap);
        }
    }


    private void startAnimation() {
        objectTopAnimation = ObjectAnimator.ofFloat(conTopView, "translationY", allHeight, 0f);
        objectTopAnimation.setDuration(500);
        objectTopAnimation.setInterpolator(new AccelerateDecelerateInterpolator());

        objectButtomAnimation = ObjectAnimator.ofFloat(conBottomView, "translationY", allHeight, 0f);
        objectButtomAnimation.setDuration(500);
        objectButtomAnimation.setInterpolator(new DecelerateInterpolator());

        objectLoyaltyAnimation = ObjectAnimator.ofFloat(ll_loyalty_anim, "alpha", 1f, 0f);
        objectLoyaltyAnimation.setDuration(2500);
        objectLoyaltyAnimation.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (ll_loyalty_anim != null) {
                    ll_loyalty_anim.setVisibility(View.GONE);
                }
            }
        });

        int height = AndroidUtils.dip2px(mContext, 40) / 2;
        int toHeight = AndroidUtils.dip2px(mContext, 70) / 2;
        int tvHeight = AndroidUtils.dip2px(mContext, 160) / 2;
        int tvToHeight = AndroidUtils.dip2px(mContext, 240) / 2;
        scaleAnimation = ValueAnimator.ofInt(0, toHeight, height);
//        scaleTvAnimation = ValueAnimator.ofInt(0, tvToHeight, tvHeight);

        scaleTvAnimation = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_transfer);
        scaleTvAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (Constants.TYPE_TRANSFER_RT.equals(transferType) || Constants.TYPE_SPLIT.equals(transferType)) {

                } else if (sceneType == 8) {
                    if ("V".equals(orderStatus) || "S".equals(orderStatus)) {
                        ivCheck.setVisibility(View.VISIBLE);
                    } else {
                        tvNotify.setVisibility(View.VISIBLE);
                    }
                } else if (sceneType == 18) {
                    if (!fromHistory) {
                        if ("C".equals(orderStatus)) {
                            ivCheck.setVisibility(View.GONE);
                            tvNotify.setVisibility(View.VISIBLE);
                        } else if ("V".equals(orderStatus)) {
                            ivCheck.setVisibility(View.VISIBLE);
                        } else if ("S".equals(orderStatus)) {
                            ivCheck.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if ("SB".equals(orderType)) {
                            ivCheck.setVisibility(View.GONE);
                            tvViewDetails.setVisibility(View.VISIBLE);
                        } else {
                            ivCheck.setVisibility(View.VISIBLE);
                        }
                    }

                } else {
                    ivCheck.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        scaleAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int animatorValue = Integer.valueOf(valueAnimator.getAnimatedValue() + "");
                if (ivCheck != null) {
                    ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) ivCheck.getLayoutParams();
                    params.width = animatorValue;
                    params.height = animatorValue;
                    ivCheck.setLayoutParams(params);
                    if (Constants.TYPE_TRANSFER_RT.equals(transferType) || Constants.TYPE_SPLIT.equals(transferType)) {

                    } else if (sceneType == 8) {
                        if ("V".equals(orderStatus) || "S".equals(orderStatus)) {
                            ivCheck.setVisibility(View.VISIBLE);
                        }
                    } else if (sceneType == 18) {
                        if (!fromHistory) {
                            if ("C".equals(orderStatus)) {
                                ivCheck.setVisibility(View.GONE);
                            } else if ("V".equals(orderStatus)) {
                                ivCheck.setVisibility(View.VISIBLE);
                            } else if ("S".equals(orderStatus)) {
                                ivCheck.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if ("SB".equals(orderType)) {
                                ivCheck.setVisibility(View.GONE);
                            } else {
                                ivCheck.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {
                        ivCheck.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        objectButtomAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (conBottomView != null) {
                    conBottomView.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if ("CB".equals(orderType)) {
                                if (anim_loyalty != null) {
                                    anim_loyalty.setAnimation("lottie/loyalty_confetti.json");
                                    anim_loyalty.playAnimation();
                                    objectLoyaltyAnimation.start();
                                }
                            }
                        }
                    }, 500);
                }
            }
        });

        scaleAnimation.setTarget(ivCheck);
//        scaleTvAnimation.setTarget(tvNotify);
        scaleAnimation.setDuration(500);
//        tvNotify.startAnimation(scaleTvAnimation);
//        tvViewDetails.startAnimation(scaleTvAnimation);
        llStatus.startAnimation(scaleTvAnimation);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(objectTopAnimation).before(objectButtomAnimation);
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //除了交易历史页面进入该详情页外,都要更新余额
                if (!Constants.history_detail.equals(transferType)) {
                    getBalance();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        conTopView.setVisibility(View.VISIBLE);

    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_transfer_detail;
    }

    @Override
    protected void initView(View parentView) {
        llMethod.setAlpha(1f);
        maxHeight = AndroidUtils.getScreenHeight(mContext) * 8 / 10;
        if (getArguments() != null) {
            cardNotifyEntity = (CardNotifyEntity) getArguments().getSerializable("cardNotifyEntity");
            isOrginalOrder = (boolean) getArguments().getSerializable("isOrginalOrder");
            if (isOrginalOrder) {
                llOriginalBack.setBackgroundResource(R.drawable.bg_pay_detail);
                llTitle.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams topLayoutParams = (LinearLayout.LayoutParams) conTopView.getLayoutParams();
                topLayoutParams.setMarginStart(AndroidUtils.dip2px(mContext, 16));
                topLayoutParams.setMarginEnd(AndroidUtils.dip2px(mContext, 16));
                conTopView.setLayoutParams(topLayoutParams);

            } else {
                llTitle.setVisibility(View.GONE);
            }
        }

        tvDetails.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        llContent.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        llContent.getViewTreeObserver()
                                .removeOnGlobalLayoutListener(this);
                        minHeight = llContent.getHeight();
                        if (minHeight > maxHeight) {
                            mActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, maxHeight);
                        }
                    }
                });

        llContent.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                llContent.removeOnLayoutChangeListener(this);
                int height = v.getHeight();
                allHeight = height - AndroidUtils.dip2px(mContext, 10);
            }
        });
    }

    @Override
    protected void initData() {
        initData(cardNotifyEntity);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startAnimation();
                MediaPlayer medPlay = MediaPlayer.create(mActivity, R.raw.notification);
                medPlay.start();
            }
        }, 300);
    }

    @Override
    protected void restart() {

    }

    private void initData(CardNotifyEntity cardNotifyEntity) {
        if (cardNotifyEntity != null) {
            transferType = cardNotifyEntity.transferType;
            fromHistory = cardNotifyEntity.fromHistory;
            subject = cardNotifyEntity.subject;
            String respCode = cardNotifyEntity.respCode;
            String respMsg = cardNotifyEntity.respMsg;
            orderStatus = cardNotifyEntity.orderStatus;
            String payerName = cardNotifyEntity.payerName;
            String payerAmount = cardNotifyEntity.payerAmount;
            String merchantId = cardNotifyEntity.merchantId;
            String merchantName = cardNotifyEntity.merchantName;
            String remark = cardNotifyEntity.remark;
            originalOrderNo = cardNotifyEntity.originalOrderNo;
            orderType = cardNotifyEntity.orderType;
            outTradeNo = cardNotifyEntity.outTradeNo;
            discountType = cardNotifyEntity.discountType;
            cashBackOrderNo = cardNotifyEntity.cashBackOrderNo;
            voucherPinCode = cardNotifyEntity.voucherPinCode;
            popupInfo = cardNotifyEntity.popupInfo;
            isShowEInvoice = cardNotifyEntity.isShowEInvoice;
            invoiceType = cardNotifyEntity.invoiceType;
            isNeedDownloadFile = cardNotifyEntity.isNeedDownloadFile();
            if (!TextUtils.isEmpty(voucherPinCode)) {
                conVoucherCode.setVisibility(View.VISIBLE);
                tvVoucherCode.setText(voucherPinCode);
                ivVoucherCopy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ButtonUtils.isFastDoubleClick(-1, 2500)) {
                            return;
                        }
                        String voucherCode = tvVoucherCode.getText().toString().trim();
                        AndroidUtils.setClipboardText(mContext, voucherCode, false);
                        showTipDialog(getString(R.string.result_page_1));
                    }
                });
            }

            orderCurrencyCode = cardNotifyEntity.orderCurrencyCode;
            if (TextUtils.isEmpty(orderCurrencyCode)) {
                orderCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
            }

            shareDesc = cardNotifyEntity.shareDesc;

            tvOriMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

            if (!TextUtils.isEmpty(outTradeNo)) {
                codeBitmap = QRCodeUtils.createQRCode(outTradeNo, AndroidUtils.dip2px(mContext, 80));
                bigBitmap = AndroidUtils.scaleBitmap(codeBitmap, AndroidUtils.dip2px(mContext, 200), AndroidUtils.dip2px(mContext, 200));
            }
            sceneType = cardNotifyEntity.sceneType;
            subSceneType = cardNotifyEntity.subSceneType;
            if (sceneType == 15) {
                //EVoucher隐藏交易记录
                UxcamHelper.getInstance().hideScreen(true);
            }
            if ("P".equals(orderType) && sceneType == 12) {
                tvSplit.setVisibility(View.VISIBLE);
                tvSplitTwo.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(respMsg) && !"000000".equals(respCode)) {
                tvError.setVisibility(View.VISIBLE);
                tvError.setText(respMsg);
            } else {
                tvError.setVisibility(View.GONE);
            }

            if(sceneType == 36 || "D".equals(orderType)){
                tvPayMethodTitle.setText(R.string.sprint19_43);
            }

            //ID1059911 只有sceneType == 6时才显示other info
            if (sceneType == 6 && !TextUtils.isEmpty(popupInfo)) {
                conOtherInfo.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(remark)) {
                tvError.setVisibility(View.VISIBLE);
                tvError.setText(remark);
                tvError.setTextColor(mContext.getColor(R.color.color_80white));
            }

            dateTime = cardNotifyEntity.startTime;
            paymentMethod = cardNotifyEntity.paymentMethodDesc;
            if (!TextUtils.isEmpty(paymentMethod)) {
                if ("BA".equals(paymentMethod)) {
                    cardNotifyEntity.topUpType = "BANK TRANSFER";
                }
                llPaymentMethod.setVisibility(View.VISIBLE);
            } else {
                llPaymentMethod.setVisibility(View.GONE);
            }
            orderNo = cardNotifyEntity.orderNo;
            orderAmount = cardNotifyEntity.orderAmount;
            tvOriMoney.setText(Spans.builder().text(AndroidUtils.getTransferMoney(orderAmount)).color(mContext.getColor(R.color.white_50)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(" " + LocaleUtils.getCurrencyCode(orderCurrencyCode)).size(10)
                    .build());
            if ("2".equals(discountType) && !TextUtils.isEmpty(orderAmount)) {
                tvOriMoney.setVisibility(View.VISIBLE);
            } else {
                tvOriMoney.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(payerAmount)) {
                transferMoney = payerAmount;
            } else {
                transferMoney = orderAmount;
            }
            transferMoney = AndroidUtils.getTransferMoney(transferMoney);
            String orderDirection = cardNotifyEntity.orderDirection;
            String direction;
            if (!TextUtils.isEmpty(orderDirection)) {
                if ("I".equals(orderDirection)) {
                    direction = "+" + " ";
                } else {
                    direction = "-" + " ";
                }
            } else {
                direction = "";
            }

            if(TextUtils.isEmpty(transferMoney)){
                tvMoney.setVisibility(View.GONE);
            }else {
                tvMoney.setVisibility(View.VISIBLE);
                if (transferMoney.length() > 8) {
                    tvMoney.setText(Spans.builder().text(direction)
                            .text(transferMoney).color(mContext.getColor(R.color.white)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(18)
                            .build());

                    tvCurrency.setText(Spans.builder().text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).size(10).build());
                } else {
                    tvMoney.setText(Spans.builder().text(direction)
                            .text(transferMoney).color(mContext.getColor(R.color.white)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                            .build());
                    tvCurrency.setText(LocaleUtils.getCurrencyCode(orderCurrencyCode));
                }
            }

            tvDate.setText(dateTime);
            if (!TextUtils.isEmpty(paymentMethod) && paymentMethod.contains("****")) {
                tvPayMethod.setTextDirection(View.TEXT_DIRECTION_LTR);
            } else {
                tvPayMethod.setTextDirection(View.TEXT_DIRECTION_LOCALE);
            }
            tvPayMethod.setText(paymentMethod);
            if(TextUtils.isEmpty(orderNo)){
                llOrderNumber.setVisibility(View.GONE);
            }else {
                llOrderNumber.setVisibility(View.VISIBLE);
                tvTranscationNumber.setText(orderNo);
            }

            String name = cardNotifyEntity.transferName;
            if (sceneType != 7 && sceneType != 8) {
                name = AndroidUtils.getContactName(name);
            }
            if (!TextUtils.isEmpty(name) && name.contains(SpUtils.getInstance().getCallingCode())) {
                tvToName.setTextDirection(View.TEXT_DIRECTION_LTR);
            } else {
                tvToName.setTextDirection(View.TEXT_DIRECTION_LOCALE);
            }
            tvToName.setText(name);
            merchantName = AndroidUtils.getContactName(merchantName);
            tvNameDesc.setText(R.string.wtw_29);
            tvNameDescId.setVisibility(View.GONE);
            tvPayTitle.setText(cardNotifyEntity.orderDesc);
            setExtend(cardNotifyEntity);
            if ("S".equals(orderStatus)) {
                ivCheck.setImageResource(R.drawable.d_check);
            } else if ("W".equals(orderStatus)) {
                ivCheck.setImageResource(R.drawable.under_processing);
            } else if ("E".equals(orderStatus)) {
                ivCheck.setImageResource(R.drawable.d_failed);
                conTermsConditions.setVisibility(View.GONE);
            }
            int drawableId = AndroidUtils.getHistoryDrawableD(sceneType);
            if (drawableId == -1) {
                ivPayMethod.setOval(true);
                if (sceneType == 15) {
                    ivPayMethod.setOval(false);
                }
                Glide.with(mContext)
                        .load(cardNotifyEntity.logo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(sceneType == 23?R.drawable.d_mobile_topup:ThemeSourceUtils.getDefAvatar(mContext, cardNotifyEntity.sex + ""))
                        .into(ivPayMethod);
            } else {
                ivPayMethod.setOval(false);
                ivPayMethod.setImageResource(drawableId);
            }

            if (sceneType == 15 || sceneType == 5 || sceneType == 11 || sceneType == 23) {
                //EVoucher代金券 或 话费充值
                conTransfer.setVisibility(View.GONE);
                if (sceneType != 11) {
                    if (sceneType == 15 || sceneType == 5) {
                        conTermsConditions.setVisibility(View.VISIBLE);
                    }
                    if ("E".equals(orderStatus)) {
                        conTermsConditions.setVisibility(View.GONE);
                    }
                } else {
                    //IMR
                    llShowMore.setVisibility(View.VISIBLE);
                    transferNumberLine.setVisibility(View.GONE);
                }

            } else if (Constants.SUBJECT_CQR_PAYMENT.equals(subject)) {
                tvSplit.setVisibility(View.VISIBLE);
                tvSplitTwo.setVisibility(View.VISIBLE);
                orderType = "P";
                conShare.setVisibility(View.VISIBLE);
                llShareTwo.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(cardNotifyEntity.orderDesc)) {
                    tvPayTitle.setText(cardNotifyEntity.orderDesc);
                } else {
                    tvPayTitle.setText(R.string.status_mqr_1);
                }
                if ("S".equals(orderStatus)) {
                    ivShowQrcode.setVisibility(View.VISIBLE);
                    ivPay.setVisibility(View.INVISIBLE);
                    ivCheck.setImageResource(R.drawable.d_check);
                } else if ("W".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.under_processing);
                } else {
                    conShare.setVisibility(View.GONE);
                    llShareTwo.setVisibility(View.VISIBLE);
                    ivShowQrcode.setVisibility(View.GONE);
                    ivPay.setVisibility(View.GONE);
                    ivCheck.setImageResource(R.drawable.d_failed);
                }

//                    tvId.setVisibility(View.VISIBLE);
                conTransfer.setVisibility(View.GONE);
                tvNameDesc.setText(" - " + merchantId);
                tvNameDescId.setVisibility(View.VISIBLE);
                tvToName.setText(merchantName);
                if (codeBitmap != null) {
                    ivPay.setImageBitmap(codeBitmap);
                }

            } else if (Constants.SUBJECT_ADD_MONEY.equals(subject) || sceneType == 10) {
                if ("000000".equals(respCode) && "S".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_check);
                    if (!TextUtils.isEmpty(cardNotifyEntity.orderDesc)) {
                        tvPayTitle.setText(cardNotifyEntity.orderDesc);
                    }else {
                        tvPayTitle.setText(R.string.add_money_21);
                    }
                } else {
                    if ("S".equals(orderStatus)) {
                        ivCheck.setImageResource(R.drawable.d_check);
                        if (!TextUtils.isEmpty(cardNotifyEntity.orderDesc)) {
                            tvPayTitle.setText(cardNotifyEntity.orderDesc);
                        }else {
                            tvPayTitle.setText(R.string.add_money_21);
                        }
                    }else if ("W".equals(orderStatus)) {
                        //ID1059889 处理中不显示
                        ivCheck.setImageResource(R.drawable.under_processing);
                        tvPayTitle.setText("");
                    } else {
                        ivCheck.setImageResource(R.drawable.d_failed);
                        if (!TextUtils.isEmpty(cardNotifyEntity.orderDesc)) {
                            tvPayTitle.setText(cardNotifyEntity.orderDesc);
                        }else {
                            tvPayTitle.setText(R.string.add_money_22);
                        }
                    }
                }
                conTransfer.setVisibility(View.GONE);
                //充值
                if("true".equals(SpUtils.getInstance().isFirstTopUp())) {
                    AppsFlyerHelper.getInstance().sendFirstTopupEvent(cardNotifyEntity.topUpType);
                    SpUtils.getInstance().setFirstTopUp("false");
                }else{
                    AppsFlyerHelper.getInstance().sendTopUpEvent(cardNotifyEntity.topUpType);
                }

            } else if (Constants.TYPE_TRANSFER_ST.equals(transferType)) {
                if ("S".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_check);
                    tvPayTitle.setText(R.string.status_mqr_1);
                } else if ("W".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.under_processing);
                    tvPayTitle.setText(R.string.wtba_57);
                } else {
                    ivCheck.setImageResource(R.drawable.d_failed);
                    tvPayTitle.setText(R.string.wtba_60);
                }
                if (!TextUtils.isEmpty(cardNotifyEntity.orderDesc)) {
                    tvPayTitle.setText(cardNotifyEntity.orderDesc);
                } else {
                    tvPayTitle.setText(R.string.status_mqr_1);
                }
            } else if (sceneType == 8) {
                //请求转账
                if ("C".equals(orderStatus)) {
                    ivCheck.setVisibility(View.GONE);
                    tvNotify.setVisibility(View.VISIBLE);
                    tvNotify.setText(R.string.request_transfer_6);
                } else if ("V".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_failed);
                } else if ("S".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_check);
                }
            } else if (Constants.TYPE_TRANSFER_BT.equals(transferType)) {
                if ("S".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_check);
                    tvPayTitle.setText(R.string.wtba_49);
                } else if ("W".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.under_processing);
                    tvPayTitle.setText(R.string.wtba_49);
                } else {
                    ivCheck.setImageResource(R.drawable.d_failed);
                    tvPayTitle.setText(R.string.wtba_49);
                }
            } else if (Constants.TYPE_TRANSFER_RC.equals(transferType)) {
                tvPayTitle.setText(cardNotifyEntity.orderDesc);
                if ("C".equals(orderStatus)) {
                    ivCheck.setVisibility(View.GONE);
                    tvNotify.setVisibility(View.VISIBLE);
                    tvNotify.setText(R.string.request_transfer_6);
                } else if ("V".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_failed);
                } else if ("S".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_check);
                }
                if (!TextUtils.isEmpty(cardNotifyEntity.directionDesc)) {
                    tvNameDesc.setText(cardNotifyEntity.directionDesc);
                    tvNameDescId.setVisibility(View.GONE);
                }

                eventBus.post(new EventEntity(EventEntity.EVENT_APPROVE_TRANSFER_SUCCESS));
            } else if (Constants.TYPE_SCAN_MERCHANT.equals(transferType)) {
                tvSplit.setVisibility(View.VISIBLE);
                tvSplitTwo.setVisibility(View.VISIBLE);
                conShare.setVisibility(View.VISIBLE);
                llShareTwo.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(cardNotifyEntity.orderDesc)) {
                    tvPayTitle.setText(cardNotifyEntity.orderDesc);
                } else {
                    tvPayTitle.setText(R.string.status_mqr_1);
                }
                if ("S".equals(orderStatus)) {
                    ivShowQrcode.setVisibility(View.VISIBLE);
                    ivPay.setVisibility(View.INVISIBLE);
                    ivCheck.setImageResource(R.drawable.d_check);
                } else if ("W".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.under_processing);
                } else {
                    conShare.setVisibility(View.GONE);
                    llShareTwo.setVisibility(View.VISIBLE);
                    ivShowQrcode.setVisibility(View.GONE);
                    ivPay.setVisibility(View.GONE);
                    ivCheck.setImageResource(R.drawable.d_failed);
                }
                tvNameDesc.setText(" - " + merchantId);
                tvNameDescId.setVisibility(View.VISIBLE);
                tvToName.setText(merchantName);
                if (codeBitmap != null) {
                    ivPay.setImageBitmap(codeBitmap);
                }
            } else if (Constants.history_detail.equals(transferType)) {
                if ("P".equals(orderType)) {
                    conShare.setVisibility(View.VISIBLE);
                    llShareTwo.setVisibility(View.GONE);
                    ivShowQrcode.setVisibility(View.VISIBLE);
                    ivPay.setVisibility(View.INVISIBLE);
                    if (codeBitmap != null) {
                        ivPay.setImageBitmap(codeBitmap);
                    }
                }

                if ("D".equals(orderType)) {
                    conOrginalId.setVisibility(View.VISIBLE);
                    tvTransferId.setText(originalOrderNo);
                }
                conTransfer.setVisibility(View.GONE);

                if (sceneType == 24) {
                    drawableId = AndroidUtils.getLoopHistoryDrawableD(subSceneType);
                }
                tvPayTitle.setText(cardNotifyEntity.orderDesc);
                if ("S".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_check);
                } else if ("W".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.under_processing);
                } else if ("E".equals(orderStatus)) {
                    ivCheck.setImageResource(R.drawable.d_failed);
                }
                if (sceneType == 8) {
                    if ("C".equals(orderStatus)) {
                        ivCheck.setVisibility(View.GONE);
                        tvNotify.setVisibility(View.VISIBLE);
                        tvNotify.setText(R.string.request_transfer_6);
                    } else if ("V".equals(orderStatus)) {
                        ivCheck.setImageResource(R.drawable.d_failed);
                    } else if ("N".equals(orderStatus)) {
                        tvNotify.setEnabled(false);
                        ivCheck.setVisibility(View.GONE);
                        tvNotify.setVisibility(View.VISIBLE);
                        tvNotify.setText(R.string.request_transfer_8);
                        tvNotify.setCompoundDrawables(null, null, null, null);
                        tvNotify.setTextColor(mContext.getColor(R.color.color_fc4f00));
                    }
                } else if (sceneType == 18) {
                    if (fromHistory) {
                        if ("SB".equals(orderType)) {
                            ivCheck.setVisibility(View.GONE);

                            tvViewDetails.setVisibility(View.VISIBLE);
                            tvViewDetails.setText(R.string.splitbill_11);
                        } else {
                            if ("V".equals(orderStatus)) {
                                ivCheck.setImageResource(R.drawable.d_failed);
                            } else if ("S".equals(orderStatus)) {
                                ivCheck.setImageResource(R.drawable.d_check);
                            }
                        }
                    } else {
                        if ("C".equals(orderStatus)) {
                            ivCheck.setVisibility(View.GONE);
                            tvNotify.setVisibility(View.VISIBLE);
                            tvNotify.setText(R.string.request_transfer_6);
                        } else if ("V".equals(orderStatus)) {
                            ivCheck.setImageResource(R.drawable.d_failed);
                        } else if ("S".equals(orderStatus)) {

                            ivCheck.setImageResource(R.drawable.d_check);
                        } else if ("N".equals(orderStatus)) {
                            tvNotify.setEnabled(false);
                            ivCheck.setVisibility(View.GONE);
                            tvNotify.setVisibility(View.VISIBLE);
                            tvNotify.setText(R.string.request_transfer_8);
                            tvNotify.setCompoundDrawables(null, null, null, null);
                            tvNotify.setTextColor(mContext.getColor(R.color.color_fc4f00));
                        }
                    }
                }

            } else if (Constants.TYPE_SPLIT.equals(transferType)) {
                ivCheck.setVisibility(View.GONE);

                tvViewDetails.setVisibility(View.VISIBLE);
                tvViewDetails.setText(R.string.splitbill_11);
                conTransfer.setVisibility(View.GONE);

                tvPayTitle.setText(cardNotifyEntity.orderDesc);
            }

            if (!TextUtils.isEmpty(outTradeNo) && !"E".equals(orderStatus)) {
                ivShowQrcode.setVisibility(View.VISIBLE);
                ivPay.setVisibility(View.INVISIBLE);
                if (codeBitmap != null) {
                    ivPay.setImageBitmap(codeBitmap);
                }
                conShare.setVisibility(View.VISIBLE);
                llShareTwo.setVisibility(View.GONE);
            } else {
                ivShowQrcode.setVisibility(View.GONE);
                conShare.setVisibility(View.GONE);
                llShareTwo.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showEInvoiceNoticeDialog() {
        EInvoiceNoticeDialog eInvoiceNoticeDialog = new EInvoiceNoticeDialog(mContext,
                mContext.getString(R.string.DigitalCard_92),
                fromHistory?mContext.getString(R.string.sprint11_64_1):mContext.getString(R.string.sprint11_64));
        eInvoiceNoticeDialog.showWithBottomAnim();
    }

    private void showDownloadFileErrorDialog() {
        EInvoiceNoticeDialog eInvoiceNoticeDialog = new EInvoiceNoticeDialog(mContext,
                mContext.getString(R.string.sprint11_70),
                fromHistory?mContext.getString(R.string.sprint11_71_1):mContext.getString(R.string.sprint11_71));
        eInvoiceNoticeDialog.showWithBottomAnim();
    }


    private void setExtend(CardNotifyEntity cardNotifyEntity) {
        extendProperties = cardNotifyEntity.extendProperties;
        if (isShowEInvoice == 1) {
            ExtendEntity extendEntity = new ExtendEntity();
            if("381".equals(invoiceType)){
                extendEntity.extendKey = getString(R.string.sprint20_76);
            }else {
                extendEntity.extendKey = getString(R.string.DigitalCard_90);
            }
            extendEntity.extendValue = getString(R.string.DigitalCard_91);
            extendEntity.extraFunctionType = "showPdf";
            if(extendProperties == null){
                extendProperties = new ArrayList<>();
            }
            extendProperties.add(extendEntity);
        }
        List<ExtendEntity> basicExtendProperties = cardNotifyEntity.basicExtendProperties;
        if (extendProperties != null && extendProperties.size() > 0) {
            conTransfer.setVisibility(View.GONE);

            int size = extendProperties.size();
            llExtend.removeAllViews();
            llDetails.setVisibility(View.VISIBLE);
            for (int i = 0; i < size; i++) {
                ExtendEntity extendEntity = extendProperties.get(i);
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_extend_properties, null);
                TextView tvExtendKey = view.findViewById(R.id.tv_extend_key);
                TextView tvExtendValue = view.findViewById(R.id.tv_extend_value);
                TextView tvSubExtendValue = view.findViewById(R.id.tv_sub_extend_value);
                ImageView ivExtend = view.findViewById(R.id.iv_extend);
                ImageView ivExtendDiscount = view.findViewById(R.id.iv_extend_discount);

                String extraFunctionType = extendEntity.extraFunctionType;
                if ("showPdf".equals(extraFunctionType)) {
                    tvExtendValue.setTextColor(getResources().getColor(R.color.color_1da1f1));
                    tvExtendValue.setBackgroundResource(R.drawable.text_under_line_1da1f1);
                    tvExtendKey.setTextColor(getResources().getColor(R.color.white));
                    tvExtendValue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showProgress(true);
                            AppClient.getInstance().getTransferPdfUrl(orderNo, new ResultCallback<TransferPdfEntity>() {
                                @Override
                                public void onResult(TransferPdfEntity transferPdfEntity) {
                                    showProgress(false);
                                    if (transferPdfEntity != null && !TextUtils.isEmpty(transferPdfEntity.url)) {
                                        transferPdfEntity.pdfType = 1;
                                        ShowPdfActivity.startShowPdfActivity(mActivity, transferPdfEntity);
                                    } else {
                                        showEInvoiceNoticeDialog();
                                    }
                                }

                                @Override
                                public void onFailure(String errorCode, String errorMsg) {
                                    showProgress(false);
                                    showTipDialog(errorMsg);
                                }
                            });
                        }
                    });
                }
                if (Constants.extend_order_percent_icon.equals(extraFunctionType)) {
                    ivExtendDiscount.setVisibility(View.VISIBLE);
                } else {
                    ivExtendDiscount.setVisibility(View.GONE);
                }
                if (Constants.extend_copy.equals(extraFunctionType)) {
                    ivExtend.setImageResource(R.drawable.d_copy);
                    ivExtend.setVisibility(View.VISIBLE);
                    tvExtendValue.setMaxWidth(AndroidUtils.dip2px(mContext, 130));
                    ivExtend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ButtonUtils.isFastDoubleClick(-1, 2500)) {
                                return;
                            }
                            String voucherCode = tvExtendValue.getText().toString().trim();
                            AndroidUtils.setClipboardText(mContext, voucherCode, false);
                            showTipDialog(getString(R.string.result_page_1));
                        }
                    });
                } else {
                    ivExtend.setVisibility(View.GONE);
                }
                if ("SB".equals(orderType)) {
                    tvExtendValue.setEllipsize(TextUtils.TruncateAt.MIDDLE);
                } else {
                    tvExtendValue.setEllipsize(TextUtils.TruncateAt.END);
                }
                String extendSubValue = extendEntity.extendSubValue;
                String extendValue = extendEntity.extendValue;
                if (!TextUtils.isEmpty(extendValue)) {
                    if (extendValue.contains(SpUtils.getInstance().getCallingCode()) || extendValue.contains("&")) {
                        tvExtendValue.setTextDirection(View.TEXT_DIRECTION_LTR);
                    } else {
                        tvExtendValue.setTextDirection(View.TEXT_DIRECTION_LOCALE);
                    }
                    tvExtendValue.setText(extendValue);
                }
                if (!TextUtils.isEmpty(extendSubValue)) {
                    tvSubExtendValue.setText(extendSubValue);
                    tvSubExtendValue.setVisibility(View.VISIBLE);
                } else {
                    tvSubExtendValue.setVisibility(View.GONE);
                }

                tvExtendKey.setText(extendEntity.extendKey);
                llExtend.addView(view);
            }
        } else {
            llDetails.setVisibility(View.GONE);
        }
        if (basicExtendProperties != null && basicExtendProperties.size() > 0) {
            llBaseExtend.removeAllViews();
            if (sceneType != 11) {
                conBaseExtend.setVisibility(View.VISIBLE);
            }
            int size = basicExtendProperties.size();
            for (int i = 0; i < size; i++) {
                ExtendEntity extendEntity = basicExtendProperties.get(i);
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_base_extend_properties, null);
                TextView tvExtendKey = view.findViewById(R.id.tv_base_extend_key);
                TextView tvExtendValue = view.findViewById(R.id.tv_base_extend_value);
                ImageView ivExtendLogo = view.findViewById(R.id.iv_extend_logo);
                View view_line = view.findViewById(R.id.view_line);
                if (sceneType == 11) {
                    if (i == size - 1) {
                        view_line.setVisibility(View.GONE);
                    } else {
                        view_line.setVisibility(View.VISIBLE);
                    }
                }
                String extraFunctionType = extendEntity.extraFunctionType;
                String logoUrl = extendEntity.logoUrl;
                if (!TextUtils.isEmpty(logoUrl)) {
                    Glide.with(mContext)
                            .load(logoUrl)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(ivExtendLogo);
                }

                if (Constants.extend_cash_back_order.equals(extraFunctionType)) {
                    ivExtendLogo.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_transaction_expand));
                    if (!TextUtils.isEmpty(cashBackOrderNo)) {
                        ivExtendLogo.setVisibility(View.VISIBLE);
                        ivExtendLogo.setOnClickListener(new OnOnlySingleClickListener() {
                            @Override
                            public void onSingleClick(View v) {
                                mPresenter.getHistoryDetail(cashBackOrderNo, "","");
                            }
                        });
                    } else {
                        ivExtendLogo.setVisibility(View.GONE);
                    }
                }
                tvExtendKey.setText(extendEntity.extendKey);
                tvExtendValue.setText(extendEntity.extendValue);
                llBaseExtend.addView(view);
            }
        } else {
            conBaseExtend.setVisibility(View.GONE);
        }
    }


    @OnClick({R.id.ll_details, R.id.iv_terms_conditions, R.id.iv_orginal_detail_down, R.id.ll_show_more, R.id.tv_download, R.id.tv_split_two, R.id.tv_split, R.id.tv_download_two, R.id.tv_view_details, R.id.tv_share_two, R.id.tv_share, R.id.tv_notify,
            R.id.iv_show_qrcode, R.id.iv_pay, R.id.iv_orginal_detail, R.id.iv_other_info})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.ll_details:
                if (conExtend.getVisibility() == View.VISIBLE) {
                    if (conBaseExtend.getVisibility() == View.VISIBLE) {
                        mActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, maxHeight);
                    } else {
                        mActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, minHeight > maxHeight ? maxHeight : minHeight);
                    }
                    conExtend.setVisibility(View.GONE);
                    ivResultDetails.setImageResource(R.drawable.result_details_down);
                } else {
                    mActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, extendProperties.size() > 1 ? maxHeight : WindowManager.LayoutParams.WRAP_CONTENT);
                    conExtend.setVisibility(View.VISIBLE);
                    ivResultDetails.setImageResource(R.drawable.result_details_up);
                }
                break;
            case R.id.ll_show_more:
                if (conBaseExtend.getVisibility() == View.VISIBLE) {
                    if (conExtend.getVisibility() == View.GONE) {
                        mActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, minHeight);
                    }
                    conBaseExtend.setVisibility(View.GONE);
                    transferNumberLine.setVisibility(View.GONE);
                    ivShowMore.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_more_down));
                    tvShowMore.setText(getString(R.string.IMR_Trxdetail_12));
                } else {
                    mActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, maxHeight);
                    conBaseExtend.setVisibility(View.VISIBLE);
                    transferNumberLine.setVisibility(View.VISIBLE);
                    ivShowMore.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_more_up));
                    tvShowMore.setText(getString(R.string.IMR_Trxdetail_13));
                }
                break;
            case R.id.iv_terms_conditions:
                mPresenter.getVoucherDetail(orderNo);
                break;
            case R.id.iv_other_info:
                OtherInfoDialog otherInfoDialog = new OtherInfoDialog(mContext, popupInfo);
                otherInfoDialog.showWithBottomAnim();
                break;
            case R.id.iv_orginal_detail:
                mPresenter.getHistoryDetail(originalOrderNo, "","");
                break;
            case R.id.iv_orginal_detail_down:
                if (onButtonClickListener != null) {
                    onButtonClickListener.onOrginalClick();
                }
                break;
            case R.id.tv_split_two:
            case R.id.tv_split:
                mPresenter.getLimit(Constants.LIMIT_TRANSFER);
                break;
            case R.id.tv_view_details:
                if (Constants.TYPE_SPLIT.equals(transferType) || sceneType == 18) {
                    if (!TextUtils.isEmpty(orderNo)) {
                        mPresenter.getSplitDetailList(orderNo);
                    }
                }
                break;
            case R.id.tv_notify:
                if (!TextUtils.isEmpty(orderNo)) {
                    NotifyEntity notifyEntity = new NotifyEntity();
                    notifyEntity.msgSubject = "TRANSFER";
                    notifyEntity.msgResult = "NOTIFY";
                    notifyEntity.orderNo = orderNo;
                    List<NotifyEntity> receivers = new ArrayList<>();
                    receivers.add(notifyEntity);
                    mPresenter.sendNotify(receivers);
                }
                break;
            case R.id.tv_download_two:
            case R.id.tv_download:
                isShare = false;
                if (isNeedDownloadFile) {
                    //需要下载pdf
                    if("E".equals(orderStatus)){
                        saveImage(llContent);
                        return;
                    }else if("W".equals(orderStatus)){
                        showDownloadFileErrorDialog();
                        return;
                    }

                    if (mPresenter != null && !TextUtils.isEmpty(orderNo)) {
                        mPresenter.getDownloadFileUrl(orderNo);
                    }
                } else {
                    //直接保存截图
                    saveImage(llContent);
                }
                break;
            case R.id.tv_share_two:
            case R.id.tv_share:
                isShare = true;
                if (isNeedDownloadFile) {
                    //需要下载pdf
                    if("E".equals(orderStatus)){
                        downShareImage(llContent, shareDesc);
                        return;
                    }else if("W".equals(orderStatus)){
                        showEInvoiceNoticeDialog();
                        return;
                    }

                    if (mPresenter != null && !TextUtils.isEmpty(orderNo)) {
                        mPresenter.getDownloadFileUrl(orderNo);
                    }
                } else {
                    downShareImage(llContent, shareDesc);
                }
                break;
            case R.id.iv_pay:
            case R.id.iv_show_qrcode:
                if (llMethod.getVisibility() == View.VISIBLE) {
                    ivShowQrcode.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_transaction_collapse));
                    startMethodAnimation(false);
                } else {
                    ivShowQrcode.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_transaction_expand));
                    startMethodAnimation(true);
                }
                break;
            default:
                break;
        }
    }

    private void downLoadFile(TransferPdfEntity transferPdfEntity){
        if (!isGranted_(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    downFile("receiptPdfFile_" + AndroidUtils.localImageStr(System.currentTimeMillis()), transferPdfEntity.url);
                }

                @Override
                public void rejectPermission() {

                }
            }, PermissionInstance.getPermissons(2));
        } else {
            downFile("receiptPdfFile_" + AndroidUtils.localImageStr(System.currentTimeMillis()), transferPdfEntity.url);
        }
    }

    @Override
    public void getDownloadFileUrlSuccess(DownloadFileUrlEntity result) {
        if (result != null) {
            if (TextUtils.equals(result.status, DownloadFileUrlEntity.PDF_CREATE_SUCCESS)) {
                //pdf已生成
                TransferPdfEntity transferPdfEntity = new TransferPdfEntity();
                transferPdfEntity.url = result.url;
                if(isShare){
                    downLoadFile(transferPdfEntity);
                }else {
                    transferPdfEntity.pdfType = 2;
                    ShowPdfActivity.startShowPdfActivity(mActivity, transferPdfEntity);
                }
            } else {
                //pdf生成中/生成失败
                showDownloadFileErrorDialog();
            }
        }
    }

    /**
     * 下載文件
     *
     * @param name
     * @param urlString
     */
    public void downFile(String name, String urlString) {
        Log.d("info", "filename==" + name + ".pdf");
        String destFileDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator;
        showProgress(true);
        DownloadUtil.get().download(urlString, destFileDir, name + ".pdf", new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(File file) {
                showProgress(false);
//                pdfFile = file;
                Log.v("info", "download success" + " " + destFileDir);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isShare) {
                            AndroidUtils.shareFile(mContext, file);
                        }
                        showTipDialog(getString(R.string.DigitalCard_105));
//                        showToast("download success"+" "+file.getAbsolutePath());
                    }
                });

                Looper.prepare();//增加部分
                Looper.loop();//增加部分
            }

            @Override
            public void onDownloading(int progress) {
                Log.v("info", "下载進度" + progress);
//                progressDialog.setProgress(progress);
            }

            @Override
            public void onDownloadFailed(Exception e) {
                showProgress(false);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast("download failed");
                    }
                });

                Log.d("info", "下载失败了 " + e.toString());
//                if (progressDialog != null && progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }

            }
        });
    }

    @Override
    public void getDownloadFileUrlFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity result) {
        tvNotify.setEnabled(false);
        tvNotify.setText(R.string.request_transfer_8);
        tvNotify.setCompoundDrawables(null, null, null, null);
        tvNotify.setTextColor(mContext.getColor(R.color.color_fc4f00));
    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {
        if (!"060018".equals(errorCode)) {
            showTipDialog(errorMsg);
        } else {
            tvNotify.setEnabled(false);
            tvNotify.setText(R.string.request_transfer_8);
            tvNotify.setCompoundDrawables(null, null, null, null);
            tvNotify.setTextColor(mContext.getColor(R.color.color_fc4f00));
        }
    }

    @Override
    public void getHistoryDetailSuccess(TransferHistoryDetailEntity transferHistoryDetailEntity) {
        AndroidUtils.jumpToTransferDetail(mActivity, transferHistoryDetailEntity, orderType, false);
    }

    @Override
    public void getHistoryDetailFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getSplitDetailListSuccess(SplitPeopleListEntity splitPeopleListEntity) {
        if (splitPeopleListEntity != null) {
            List<SplitPeopleEntity> splitPeopleEntityList = splitPeopleListEntity.billReceiverList;
            showSplitDialog(splitPeopleEntityList);
        }
    }

    @Override
    public void getSplitDetailListFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> limitMap = new HashMap<>();
        limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        limitMap.put(Constants.splitMoney, transferMoney);
        ActivitySkipUtil.startAnotherActivity(mActivity, CreateSplitBillActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getVoucherDetailSuccess(EVoucherDetailEntity eVoucherDetailEntity) {
        if (eVoucherDetailEntity != null) {
            EVoucherTAndCDialogFrag diaFrg = EVoucherTAndCDialogFrag.newInstance(eVoucherDetailEntity);
            diaFrg.show(getChildFragmentManager(), "EVoucherTAndCDialog");
        }
    }

    @Override
    public void getVoucherDetailFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public NotificationContract.Presenter getPresenter() {
        return new NotificationPresenter();
    }

    private void showSplitDialog(List<SplitPeopleEntity> peopleEntityList) {
        splitDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_split_detail, null);
        MaxHeightRecyclerView rlDetail = contentView.findViewById(R.id.ry_transaction_detail);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 12f);
        rlDetail.addItemDecoration(MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), ryLineSpace));
        rlDetail.setLayoutManager(mLayoutManager);
        SplitDetailAdapter splitDetailAdapter = new SplitDetailAdapter(peopleEntityList);
        splitDetailAdapter.bindToRecyclerView(rlDetail);

        splitDetailAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                SplitPeopleEntity splitPeopleEntity = peopleEntityList.get(position);
                if (splitPeopleEntity != null) {
                    if ("SB".equals(splitPeopleEntity.orderType)) {
                        mPresenter.getHistoryDetail(splitPeopleEntity.transferOrderNo, splitPeopleEntity.orderType,"myself");
                    }else {
                        mPresenter.getHistoryDetail(splitPeopleEntity.transferOrderNo, splitPeopleEntity.orderType,"");
                    }
                }
            }
        });

        splitDialog.setContentView(contentView);
        Window dialogWindow = splitDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        splitDialog.showWithBottomAnim();
    }
}
