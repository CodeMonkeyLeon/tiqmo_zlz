package cn.swiftpass.wallet.tiqmo.support.appsflyer;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class AppsFlyerEntity extends BaseEntity {
    //交易目的
    public String purpose;
    //国家码
    public String desitinationCode;
    //话费充值商户名称
    public String merchantName;
    //是否是已注册用户
    public String isRegisterUser;
    //voucher产品名称
    public String voucherBrandName;
    //充值方式
    public String credit;
}
