package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

import java.util.regex.Pattern;

public class MoneyInputFilter implements InputFilter {

    public static final int MONEY = 9166;
    private final String STR_CHARSEQUENCE = "A-Za-z";

    private int digits = 2;

    private final String STR_SPACE = " ";
    private final String STR_HENGGANG = "-";
    private final String STR_NUMBER = "0-9";
    private final String STR_MONEY_SPECIAL = ".,";

    private Pattern mPattern;

    public MoneyInputFilter() {
        mPattern = Pattern.compile("[" + STR_NUMBER + STR_MONEY_SPECIAL + "]*");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        CharSequence out = source;
        String result = dest.toString();
        result = result.replace(",", "");

//        // if changed, replace the source
        if (out != null) {
            source = out;
            start = 0;
            end = out.length();
        }


        int len = end - start;
        if (len == 0) {
            return source;
        }

        //以点开始的时候，自动在前面添加0
        if (source != null && source.toString().equals(".") && dstart == 0) {
            return "";
        }
        //如果起始位置为0,且第二位跟的不是".",则无法后续输入
        if (!source.toString().equals(".") && result.equals("0") && dstart != 0) {
            return "";
        }

        //.前头只能有一个0
        //source.toString().equals("0") &&
        if (result.contains(".")) {
            String firstCharStr = result.charAt(0) + "";
            int pointPos = result.indexOf(".");
            if (result.contains(",")) {
                if (pointPos >= 6 && dstart <= 7) {
                    return "";
                }
            } else {
                if (pointPos >= 6 && dstart < 7) {
                    return "";
                }
            }
            //dstart>0&&d
            if (firstCharStr.equals("0") && dstart <= pointPos) {
                if (source.toString().equals("0")) {
                    //如果之前小数点前 是0开始 这个时候再加进去0 非法
                    return "";
                } else if (dstart != 0) {
                    //09.00 非法
                    return "";
                }
            }
        } else {
            int length = result.length();
            if (length > 5) {
                if (dstart <= 6) {
                    if (".".equals(source.toString())) {
                        return ".";
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            }
        }

        //如果起始位置为[0],且第二位跟的不是".",则无法后续输入
        if (source.toString().equals("0") && result.toString().length() > 0 && dstart == 0) {
            //009
            return "";
        }

        int dlen = dest.length();
        // Find the position of the decimal .
        for (int i = 0; i < dstart; i++) {
            if (dest.charAt(i) == '.') {
                // being here means, that a number has
                // been inserted after the dot
                // check if the amount of digits is right
                return (dlen - (i + 1) + len > digits) ? "" : new SpannableStringBuilder(source, start, end);
            }
        }

        for (int i = start; i < end; ++i) {
            if (source.charAt(i) == '.') {
                // being here means, dot has been inserted
                // check if the amount of digits is right
                if ((dlen - dend) + (end - (i + 1)) > digits) return "";
                else break;  // return new SpannableStringBuilder(source, start, end);
            }
        }


        // if the dot is after the inserted part,
        // nothing can break
        return new SpannableStringBuilder(source, start, end);
    }
}
