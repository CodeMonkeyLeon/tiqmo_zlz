package cn.swiftpass.wallet.tiqmo.module.chat.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static String getDayStringFromDateTimeAlert(long date) {
        long differenceSeconds = (System.currentTimeMillis() - date)/1000;
        if(differenceSeconds <= 60){
            return differenceSeconds + " seconds";
        }
        else if(differenceSeconds <= 3600){
            return (differenceSeconds/60) + " Minutes";
        }
        else if(differenceSeconds <= 86400){
            return (differenceSeconds/3600) + " Hours";
        }
        else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
            return dateFormat.format(new Date(date));
        }
    }

    public static String getChatListTimeToDisplay(long date) {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        if(date >= todayStart.getTimeInMillis()){
            SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
            String format = dateFormat.format(date);
            format = format.replace("AM","am");
            format = format.replace("PM","pm");
//            return "Today, "+dateFormat.format(new Date(date));
            return format;
        }
//        else if(date >= (todayStart.getTimeInMillis() - 24 * 60 * 60 * 1000)){
//            return "Yesterday";
//        }
        else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            return dateFormat.format(new Date(date));
        }
    }
}
