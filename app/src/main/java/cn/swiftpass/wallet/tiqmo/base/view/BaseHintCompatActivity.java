package cn.swiftpass.wallet.tiqmo.base.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.InviteFriendsActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.MarketDetailsDialogFragment;
import cn.swiftpass.wallet.tiqmo.module.home.view.MarketDetailsForRefCodeDia;
import cn.swiftpass.wallet.tiqmo.module.home.view.RequestCenterActivity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AreaEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CustomerInfoActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanQrActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.ServerConfigHelper;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityLifeManager;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomProgressDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ExpireIdDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.KycProgressDialog;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public abstract class BaseHintCompatActivity extends BaseViewCompatActivity implements Loading {
    private static final String TAG = "BaseHintCompatActivity";
    protected InputMethodManager mInputManager;
    protected CustomProgressDialog mProgressDialog;
    protected KycProgressDialog mKycProgressDialog;
    protected ErrorDialog mErrorDialog;
    private int mCurrentPermissionsRequestCode;
    private String missingPermissionTitle;//默认为指纹的提示
    private String missingPermissionMessage;//默认为指纹的提示
    private String missingPermissionPosBtn;
    public long startTime;
    private long visibleTime;

    private WindowManager windowManager;
    private Handler mHandler;
    private View tipView;
    protected TextView tvUndo;

    protected EventBus eventBus;

    private CustomDialog mDealDialog;

    private ChatConfirmDialog chatConfirmDialog;

    @Override
    protected void onInit(final Bundle savedInstanceState) {
        eventBus = EventBus.getDefault();
        super.onInit(savedInstanceState);
    }

    //--------------------------- -------------------------------------- 显示错误dialog -------------------------------------- --------------------------------------//

    public CustomDialog showCustomDialog(Context mContext, String title, String msg, int posStr, int navId,boolean showTime, DialogInterface.OnClickListener posClick, DialogInterface.OnClickListener navClick) {
        CustomDialog mDealDialog = null;
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setShowTime(showTime);
        builder.setNegativeButton(getString(navId),navClick);
        builder.setPositiveButton(getString(posStr), posClick);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
        return mDealDialog;
    }

    public void setAlpha(Context context, float alpha) {
        WindowManager.LayoutParams params = ((Activity) context).getWindow().getAttributes();
        params.alpha = alpha;
        ((Activity) context).getWindow().setAttributes(params);
    }

    protected void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(getString(R.string.SH2_4_1a));
        builder.setMessage(getString(R.string.SH2_4_1b));
        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mContext);
            }
        });
        builder.setNegativeButton(getString(R.string.common_6), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_1da1f1).setRightColor(R.color.color_1da1f1);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    protected void showLackOfPermissionDialog(String title, String message) {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.common_9), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mContext);
            }
        });
        builder.setNegativeButton(getString(R.string.common_10), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Activity mActivity = (Activity) mContext;
                if (mActivity != null && !mActivity.isFinishing()) {
                    if (mActivity instanceof ScanQrActivity) {
                        mActivity.finish();
                    }
                }
                dialog.dismiss();
            }
        });
        builder.setLeftColor(R.color.color_1da1f1).setRightColor(R.color.color_1da1f1);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    public void getUserInfo(){
        AppClient.getInstance().getUserManager().getUserInfo(new ResultCallback<UserInfoEntity>() {
            @Override
            public void onResult(UserInfoEntity response) {
                userInfoEntity = response;
                AppClient.getInstance().getUserManager().bindUserInfo(response, false);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }
        });
    }

    public void getUserDisableAppFuncCodeList(){
        userInfoEntity = getUserInfoEntity();
        AppClient.getInstance().getUserManager().getUserDisableAppFuncCodeList(userInfoEntity.callingCode, userInfoEntity.phone, new ResultCallback<UserUnableUseEntity>() {
            @Override
            public void onResult(UserUnableUseEntity userUnableUseEntity) {
                SpUtils.getInstance().setUserUnableUseEntity(userUnableUseEntity);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }
        });
    }

    public void getBalance() {
        AppClient.getInstance().getBalance(new ResultCallback<BalanceListEntity>() {
            @Override
            public void onResult(BalanceListEntity balanceListEntity) {
                if (balanceListEntity != null) {
                    if(balanceListEntity.balanceInfos.size()>0){
                        BalanceEntity balanceEntity = balanceListEntity.balanceInfos.get(0);
                        if (balanceEntity != null) {
                            String balance = balanceEntity.balance;
                            if (!TextUtils.isEmpty(balance)) {
                                if (userInfoEntity != null) {
                                    userInfoEntity.setBalance(balance);
                                    AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
                                    if (eventBus != null) {
                                        eventBus.post(new EventEntity(EventEntity.UPDATE_BALANCE,balanceListEntity));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }
        });
    }

    //根据状态来显示需要显示的view
    protected void showStatusView(StatusView statusView, int status) {
        switch (status) {
            case StatusView.CONTENT_VIEW:
                statusView.dismissLoading();
                break;
            case StatusView.NET_ERROR_VIEW:
                statusView.showNetError();
                break;
            case StatusView.NO_CONTENT_VIEW:
                statusView.showEmpty();
                break;
            case StatusView.LOADING_VIEW:
                statusView.showLoading();
                break;
            default:
                statusView.dismissLoading();
                break;
        }
    }

    private final Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            removeView();
        }
    };

    private final Runnable timerCenterRunnable = new Runnable() {

        @Override
        public void run() {
            removeView();
        }
    };

    private final Runnable notifyRunnable = new Runnable() {

        @Override
        public void run() {
            removeView();
        }
    };

    private final Runnable requestRunnable = new Runnable() {

        @Override
        public void run() {
            removeView();
        }
    };


    public void removeView() {
        if (tipView != null && tipView.getParent() != null) {
            if (windowManager != null) {
                windowManager.removeViewImmediate(tipView);
                windowManager = null;
                mHandler.removeCallbacksAndMessages(null);
//                mHandler.removeCallbacks(timerRunnable);
//                mHandler.removeCallbacks(timerCenterRunnable);
//                mHandler.removeCallbacks(notifyRunnable);
//                mHandler.removeCallbacks(requestRunnable);
            }
        }
    }

    public void showTipCenterDialog(String msg, View.OnClickListener listener) {
        removeView();
        if(TextUtils.isEmpty(msg))return;
        tipView = LayoutInflater.from(this).inflate(R.layout.layout_tips_notification, null);
        tvUndo = tipView.findViewById(R.id.tv_undo);
        TextView tvMsg = tipView.findViewById(R.id.tv_msg);
        tvMsg.setText(msg);
        tvUndo.setOnClickListener(listener);
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
//            layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |    //不拦截页面点击事件
//                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
//                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
            layoutParams.format = PixelFormat.TRANSLUCENT;
            layoutParams.gravity = Gravity.TOP;
//            layoutParams.y = (int) getResources().getDimension(R.dimen.dimen_30);
            layoutParams.height = (int) getResources().getDimension(R.dimen.dimen_62);
            layoutParams.windowAnimations = R.style.popwindowAnimStyle;    //自定义动画
            windowManager.addView(tipView, layoutParams);

            if (mHandler == null) {
                mHandler = ProjectApp.getInstance().getMainHandler();
            }
            mHandler.postDelayed(timerCenterRunnable, 4000);
        }
    }

    public void showNotifyDialog(CardNotifyEntity cardNotifyEntity) {
        if (cardNotifyEntity == null) {
            return;
        }
        mHandler = new Handler();
        removeView();
        tipView = LayoutInflater.from(this).inflate(R.layout.view_notification_center, null);

        TextView tvTitle = tipView.findViewById(R.id.tv_notification);
        ImageView ivNotify = tipView.findViewById(R.id.iv_notification);
        ImageView iv_check = tipView.findViewById(R.id.iv_check);

        String msgSubject = cardNotifyEntity.subject;
        String msgTitle = cardNotifyEntity.inAppTitle;

        if ("REQUEST_TRANSFER_REJECTED".equals(msgSubject) ||
                "SPLIT_BILL_REJECT".equals(msgSubject)) {
            iv_check.setVisibility(View.VISIBLE);
        } else {
            iv_check.setVisibility(View.GONE);
        }

        tvTitle.setText(msgTitle);
        ivNotify.setImageResource(AndroidUtils.getNotifyDrawable(mContext, msgSubject));

        tipView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(cardNotifyEntity.subject)) {
                    String activityNo = cardNotifyEntity.activityNo;
                    String activityType = cardNotifyEntity.activityType;
                    showMarketActivityDetail(activityNo, activityType);
                } else {
                    if(Constants.NOTIFY_SUBJECT_USER_KYC_STATUS_UNPD.equals(msgSubject)){
                    }
                    if (Constants.NOTIFY_SUBJECT_PORTAL_PUSH.equals(cardNotifyEntity.subject)) {
                        AppClient.getInstance().updateClickTimeService(cardNotifyEntity.msgId);
                    }
                    if (!(ActivityLifeManager.getInstance().getCurrentActivity() instanceof RequestCenterActivity)) {
                        ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), RequestCenterActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        ProjectApp.removeTransferDetailTask();
                    }
                }
            }
        });

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
//            layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |    //不拦截页面点击事件
//                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
//                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            layoutParams.format = PixelFormat.TRANSLUCENT;
            layoutParams.gravity = Gravity.TOP;
//            layoutParams.y = (int) getResources().getDimension(R.dimen.dimen_30);
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            layoutParams.windowAnimations = R.style.popwindowAnimStyle;    //自定义动画
            windowManager.addView(tipView, layoutParams);

            mHandler.postDelayed(notifyRunnable, 4000);
        }
        getBalance();
    }

    public void showMarketActivityDetail(String activityNo, String activityType) {
        if (Constants.TRADE_ORDER.equals(activityType)) {
            showProgress(true);
            AppClient.getInstance().requestMarketDetails(activityNo, new ResultCallback<MarketDetailsEntity>() {
                @Override
                public void onResult(MarketDetailsEntity result) {
                    showProgress(false);
                    MarketDetailsDialogFragment fragment = MarketDetailsDialogFragment.newInstance(result);
                    fragment.show(getSupportFragmentManager());
                }

                @Override
                public void onFailure(String errorCode, String errorMsg) {
                    showProgress(false);
                    showTipDialog(errorMsg);
                }
            });
        } else if (Constants.ACTIVITY_REFERRAL_CODE.equals(activityType)) {
            showProgress(true);
            AppClient.getInstance().requestRefCodeDesc("REFERRAL_CODE_ACTIVITY", new ResultCallback<ReferralCodeDescEntity>() {
                @Override
                public void onResult(ReferralCodeDescEntity result) {
                    showProgress(false);
                    MarketDetailsForRefCodeDia dialog = MarketDetailsForRefCodeDia.newInstance(result);
                    dialog.setConfirmClickListener(new MarketDetailsForRefCodeDia.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            if (isKyc(Constants.KYC_TYPE_NULL)) {
                                showProgress(true);
                                AppClient.getInstance().getInviteDetail(new ResultCallback<InviteEntity>() {
                                    @Override
                                    public void onResult(InviteEntity inviteEntity) {
                                        showProgress(false);
                                        if(inviteEntity != null){
                                            HashMap<String, Object> mHashMap = new HashMap<>();
                                            mHashMap.put(Constants.INVITE_ENTITY, inviteEntity);
                                            String badges = userInfoEntity.badges;
                                            mHashMap.put(Constants.badges, badges);
                                            ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), InviteFriendsActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                        }
                                    }

                                    @Override
                                    public void onFailure(String errorCode, String errorMsg) {
                                        showProgress(false);
                                        showTipDialog(errorMsg);
                                    }
                                });
//                                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), ReferralCodeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            }
                        }
                    });
                    dialog.show(getSupportFragmentManager(), "ReferralCodeMarket");
                }

                @Override
                public void onFailure(String errorCode, String errorMsg) {
                    showProgress(false);
                    showTipDialog(errorMsg);
                }
            });
        }
    }

    protected void showExpireIdDialog() {
        ExpireIdDialog expireIdDialog = new ExpireIdDialog(mContext);
        expireIdDialog.setCloseListener(new ExpireIdDialog.RenewIdListener() {
            @Override
            public void clickRenew() {
                ActivitySkipUtil.startAnotherActivity((Activity) mContext, CustomerInfoActivity.class,
                        ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                expireIdDialog.dismiss();
            }
        });
        expireIdDialog.showWithBottomAnim();
    }

    protected boolean isKyc(int kycType) {
        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            if (userInfoEntity.isKyc()) {
                if (userInfoEntity.isUserIdCardExpried()) {
                    showExpireIdDialog();
                    return false;
                }
                return true;
            } else {
                if (userInfoEntity.isKycChecking()) {
                    ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                } else {
                    UserInfoManager.getInstance().setKycType(kycType);
                    ReferralCodeDialogUtils.showReferralCodeDialog(this, ActivityLifeManager.getInstance().getCurrentActivity());
                }
                return false;
            }
        }
        return false;
    }

    public void showRequestDialog(CardNotifyEntity cardNotifyEntity) {
        if (cardNotifyEntity == null) {
            return;
        }
        removeView();
        tipView = LayoutInflater.from(this).inflate(R.layout.view_request_center, null);
        RoundedImageView ivFromAvatar = tipView.findViewById(R.id.iv_from_avatar);
        RoundedImageView ivToAvatar = tipView.findViewById(R.id.iv_to_avatar);
        TextView tvMoney = tipView.findViewById(R.id.tv_money);
        TextView tvDesc = tipView.findViewById(R.id.tv_desc);
        TextView tvName = tipView.findViewById(R.id.tv_name);
        TextView tvPhone = tipView.findViewById(R.id.tv_phone);
        LottieAnimationView ivSend = tipView.findViewById(R.id.iv_send);
        LocaleUtils.viewRotationY(mContext, ivSend);
        if (ThemeUtils.isCurrentDark(mContext)) {
            ivSend.setAnimation("lottie/arrow_right_transfer.json");
        } else {
            ivSend.setAnimation("lottie/l_request_arrow.json");
        }

        String requestIcon = cardNotifyEntity.requestIcon;
        String receiveIcon = cardNotifyEntity.receiveIcon;
        String requestPhone = cardNotifyEntity.requestPhone;
        String receivePhone = cardNotifyEntity.receivePhone;
        String requestSubject = cardNotifyEntity.requestSubject;
        String requestUserName = cardNotifyEntity.requestUserName;
        String receiveUserName = cardNotifyEntity.receiveUserName;
        String requestSex = cardNotifyEntity.requestSex;
        String receiveSex = cardNotifyEntity.receiveSex;
        String transferAmount = cardNotifyEntity.transferAmount;
        String currencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferAmount = AndroidUtils.getTransferMoney(transferAmount);

        if (Constants.SUBJECT_SPLIT_BILL.equals(requestSubject)) {
            tvDesc.setText(getString(R.string.request_transfer_28));
        } else {
            tvDesc.setText(getString(R.string.request_transfer_12));
        }

        if (!TextUtils.isEmpty(transferAmount)) {
            if (transferAmount.length() > 8) {
                tvMoney.setText(Spans.builder()
                        .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(16)
                        .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(12)
                        .build());
            } else {
                tvMoney.setText(Spans.builder()
                        .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(20)
                        .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(12)
                        .build());
            }
        }

        tvName.setText(AndroidUtils.getContactName(requestUserName));
        tvPhone.setText(AndroidUtils.getPhone(requestPhone));

        if (!TextUtils.isEmpty(receiveIcon)) {
            Glide.with(mContext).clear(ivFromAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(receiveIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, receiveSex))
                    .into(ivFromAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, receiveSex)).into(ivFromAvatar);
        }

        if (!TextUtils.isEmpty(requestIcon)) {
            Glide.with(mContext).clear(ivToAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(requestIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, requestSex))
                    .into(ivToAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, requestSex)).into(ivToAvatar);
        }

        tipView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(ActivityLifeManager.getInstance().getCurrentActivity() instanceof RequestCenterActivity)) {
                    ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), RequestCenterActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    ProjectApp.removeTransferDetailTask();
                }
            }
        });
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
//            layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |    //不拦截页面点击事件
//                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
//                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            layoutParams.format = PixelFormat.TRANSLUCENT;
            layoutParams.gravity = Gravity.TOP;
//            layoutParams.y = (int) getResources().getDimension(R.dimen.dimen_30);
            layoutParams.height = (int) getResources().getDimension(R.dimen.dimen_150);
            layoutParams.windowAnimations = R.style.popwindowAnimStyle;    //自定义动画
            windowManager.addView(tipView, layoutParams);

            if (mHandler == null) {
                mHandler = ProjectApp.getInstance().getMainHandler();
            }
            mHandler.postDelayed(requestRunnable, 4000);
        }
        getBalance();
    }

    public void showTipDialog(String msg) {
        showTipDialog(msg, 4000);
    }

    public void showTipDialog(String msg, long duration) {
        if(TextUtils.isEmpty(msg))return;
        mHandler = new Handler();
        tipView = LayoutInflater.from(BaseHintCompatActivity.this).inflate(R.layout.layout_tips, null);
        TextView tvMsg = tipView.findViewById(R.id.tv_msg);
        tvMsg.setText(msg);
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |    //不拦截页面点击事件
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            layoutParams.format = PixelFormat.TRANSLUCENT;
            layoutParams.gravity = Gravity.TOP;
//                                layoutParams.height = (int) getResources().getDimension(R.dimen.dimen_42);
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            layoutParams.windowAnimations = R.style.popwindowAnimStyle;    //自定义动画
            Looper.getMainLooper().isCurrentThread();
            windowManager.addView(tipView, layoutParams);
            mHandler.postDelayed(timerRunnable, duration);
        }
        showProgress(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (windowManager != null && tipView != null) {
                windowManager.removeViewImmediate(tipView);
                windowManager = null;
                tipView = null;
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public void showErrorPwdDialog(Context mContext, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.common_5), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.common_5), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String title, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(mContext.getString(R.string.common_5), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String title, String msg, String btnStr) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(btnStr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String title, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(mContext.getString(R.string.common_5), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public void showErrorMsgDialog(Context mContext, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.common_5), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    public void windowColor() {
        Window window = getWindow();
        //取消设置Window半透明的Flag
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //添加Flag把状态栏设为可绘制模式
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        //设置状态栏为透明/或者需要的颜色
//        window.setStatusBarColor(mContext.getColor(R.color.color_4c686969));

        getWindow().setGravity(Gravity.BOTTOM);//设置显示在底部 默认在中间
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;//设置宽度满屏
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//        window.setDimAmount(0.5f);
        getWindow().setAttributes(lp);
        setFinishOnTouchOutside(true);//允许点击空白处关闭
    }

    public void setMissingPermissionTitle(String missingPermissionTitle) {
        this.missingPermissionTitle = missingPermissionTitle;
    }

    public void setMissingPermissionMessage(String missingPermissionMessage) {
        this.missingPermissionMessage = missingPermissionMessage;
    }

    //--------------------------- -------------------------------------- 显示错误dialog -------------------------------------- --------------------------------------//

    public final void requestPermissionsInCompatMode(@NonNull String[] permissions, int requestCode) {
        List<String> permissionList = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(permission);
            }
        }

        int size = permissionList.size();
        if (size != 0) {
            mCurrentPermissionsRequestCode = requestCode;
            ActivityCompat.requestPermissions(this, permissionList.toArray(new String[size]), requestCode);
        } else {
            onRequestPermissionsResult(requestCode, true, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != mCurrentPermissionsRequestCode) {
            return;
        }
        mCurrentPermissionsRequestCode = -1;
        boolean isNeedShowMissingPermissionDialog = false;
        boolean result = true;
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (int i = 0, length = permissions.length; i < length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                result = false;
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                    isNeedShowMissingPermissionDialog = true;
                }
                deniedPermissions.add(permissions[i]);
            }
        }
        final String[] deniedArr = deniedPermissions.toArray(new String[0]);
        if (result) {
            onRequestPermissionsResult(requestCode, true, null);
        } else if (isNeedShowMissingPermissionDialog) {
            CustomDialog.Builder builder = new CustomDialog.Builder(this);
//            builder.setTitle(!TextUtils.isEmpty(missingPermissionTitle)?missingPermissionTitle:getString(R.string.SET2_12_6));//与IOS统一，都不设置标题
            builder.setMessage(!TextUtils.isEmpty(missingPermissionMessage) ? missingPermissionMessage : "");
            builder.setNegativeButton(R.string.common_6, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    onRequestPermissionsResult(requestCode, false, deniedArr);
                }
            });

            builder.setPositiveButton(!TextUtils.isEmpty(missingPermissionPosBtn) ? missingPermissionPosBtn : "", new android.content.DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    startAppSettings();
                }
            }).setLeftColor(R.color.color_007AFF).setRightColor(R.color.color_007AFF);
            CustomDialog mDealDialog = builder.create();
            mDealDialog.show();

        } else {
            onRequestPermissionsResult(requestCode, false, deniedArr);
        }
    }

    protected void onRequestPermissionsResult(int requestCode, boolean isSuccess, String[] deniedPermissions) {

    }

    // 启动应用的设置界面
    private void startAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }

    public void showToast(String content) {
        showToast(content, Toast.LENGTH_SHORT);
    }

    public void showLongToast(String content) {
        showToast(content, Toast.LENGTH_LONG);
    }

    public void showToast(String content, int duration) {
        Toast toast = Toast.makeText(this, content, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    public void showSoftKeyboard(View focusView) {
        if (mInputManager == null) {
            mInputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        focusView.setFocusable(true);
        focusView.setFocusableInTouchMode(true);
        focusView.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        mInputManager.showSoftInput(focusView, InputMethodManager.SHOW_IMPLICIT);
    }

    public void hideSoftKeyboard() {
        if (mInputManager == null) {
            mInputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
            if (getCurrentFocus() != null) {
                mInputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    @Override
    public void showProgress(boolean iShow) {
        showProgress(iShow, false, true);
    }

    public void showProgressCancelable(boolean iShow, boolean cancelable) {
        showProgress(iShow, cancelable, true);
    }

    @Override
    public void showProgress(boolean iShow, boolean isDim) {
        showProgress(iShow, false, isDim);
    }

    public void showProgress(boolean iShow, final boolean cancelable, boolean isDim) {
        Activity activity = (Activity) this;
        if (mProgressDialog == null) {
            try {
                LogUtils.i(TAG, "current activity" + activity);
                if (!activity.isFinishing()) {
                    if(isDim){
                        mProgressDialog = CustomProgressDialog.createDialog(activity, activity.getString(R.string.common_34),R.style.CustomProgressDialog);
                    }else {
                        mProgressDialog = CustomProgressDialog.createDialog(activity, activity.getString(R.string.common_34),R.style.NoDimCustomProgressDialog);
                    }
                    mProgressDialog.setCancel(false);

                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        }
        if (iShow) {
            mProgressDialog.setCancelable(cancelable);
            if (!mProgressDialog.isShowing()) {
                if (!activity.isFinishing()) {
                    mProgressDialog.show();
                }
            }
        } else {
            mProgressDialog.setOnCancelListener(null);
            mProgressDialog.dismiss();
        }
    }

    public void showKycProgress(boolean iShow) {
        Activity activity = (Activity) this;
        if (mKycProgressDialog == null) {
            try {
                LogUtils.i(TAG, "current activity" + activity);
                if (!activity.isFinishing()) {
                    mKycProgressDialog = KycProgressDialog.createDialog(activity, activity.getString(R.string.common_34));
                    mKycProgressDialog.setCancel(false);
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        }
        if (iShow) {
            mKycProgressDialog.setCancelable(false);
            mKycProgressDialog.setOnCancelListener(null);
            if (!mKycProgressDialog.isShowing()) {
                if (!activity.isFinishing()) {
                    mKycProgressDialog.show();
                }
            }
        } else {
            mKycProgressDialog.setOnCancelListener(null);
            mKycProgressDialog.dismiss();
        }
    }

    public void showErrorDialog(String error) {
        showErrorMsgDialog(this, error);
    }

    public void showErrorDialog(String error, OnMsgClickCallBack listener) {
        showErrorMsgDialog(this, error, listener);
    }

    @Override
    protected void onDestroy() {
        if (mErrorDialog != null) {
            mErrorDialog.dismiss();
            mErrorDialog.setOnDismissListener(null);
            mErrorDialog = null;
        }
        if (mProgressDialog != null) {
            mProgressDialog.setOnCancelListener(null);
            mProgressDialog.dismiss();
            mErrorDialog = null;
        }
        if (mKycProgressDialog != null) {
            mKycProgressDialog.setOnCancelListener(null);
            mKycProgressDialog.dismiss();
            mErrorDialog = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        startTime = System.currentTimeMillis();

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        if (task.getResult() != null) {
                            String token = task.getResult().getToken();
                            UserInfoManager.getInstance().setmGcmDeviceToken(token);
                            if (!TextUtils.isEmpty(token)) {
                                SpUtils.getInstance().setDeviceToken(token);
                            }
                            LogUtils.i(TAG, "onMessageReceived: " + token);
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        visibleTime = System.currentTimeMillis();
    }

    /**
     * 判断是否为TranFast机构
     * @return
     */
    public boolean isChannelTF(){
        return Constants.imr_TransFast.equals(SpUtils.getInstance().getChannelCode());
    }
    public boolean isChannelThunes(String channelCode){
        return Constants.imr_Thunes.equals(channelCode);
    }

    protected void initWalletSDK() {
        AreaEntity areaEntity = SpUtils.getInstance().getAreaEntity();
        if (areaEntity == null) {
            areaEntity = new AreaEntity(Constants.CALLINGCODE_KSA, Constants.KsaServerPublicKey, Constants.KsaPartnerNo, "Saudi Arabia", R.drawable.icon_ksa);
        }
        UserInfoManager.getInstance().setAreaEntity(areaEntity);
        if (BuildConfig.DEBUG) {
            ServerConfigHelper.Model model = ServerConfigHelper.getConfig(this);
            AppClient.init(this,
                    model.getUrl() + BuildConfig.RequestPath,
                    model.getUrl() + BuildConfig.UploadRequestPath,
                    areaEntity.partnerNo,
                    BuildConfig.OSType,
                    BuildConfig.DefaultLanguage,
                    areaEntity.callingCode,
                    AndroidUtils.getCurrentAppVersionName(mContext),
                    model.isEncodeData(),
                    areaEntity.publicKey);
        } else {
            AppClient.init(this,
                    BuildConfig.ServerUrl + BuildConfig.RequestPath,
                    BuildConfig.ServerUrl + BuildConfig.UploadRequestPath,
                    areaEntity.partnerNo,
                    BuildConfig.OSType,
                    BuildConfig.DefaultLanguage,
                    areaEntity.callingCode,
                    AndroidUtils.getCurrentAppVersionName(mContext),
                    BuildConfig.isEnableEncryptMode,
                    areaEntity.publicKey);
        }

        LogUtil.setEnableLog(BuildConfig.isLogDebug);
//        LanguageUtil.setAppClientLanguage(LanguageUtil.getCurrentLanguage());
    }

    @Override
    protected void onPause() {
        super.onPause();
        visibleTime = System.currentTimeMillis() - visibleTime;
    }

    public void showChatConfirmDialog(String title,String left, String right, String message, ChatConfirmDialog.ConfirmListener confirmListener){

        chatConfirmDialog = new ChatConfirmDialog(mContext);
        chatConfirmDialog.setLeft(left);
        chatConfirmDialog.setRight(right);
        chatConfirmDialog.setMessage(message);
        chatConfirmDialog.setTitle(title);
        chatConfirmDialog.setConfirmListener(confirmListener);
        chatConfirmDialog.showMatchParentWidthAndGravityCenter(AndroidUtils.dip2px(mContext,280));
    }

    public void dissmissChatConfirmDialog(){
        if(chatConfirmDialog != null){
            chatConfirmDialog.dismiss();
            chatConfirmDialog = null;
        }
    }

    protected void getNotifyPermission(){
        //安卓13申请通知权限
        PermissionInstance.getInstance().getPermission(this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {

            }

            @Override
            public void rejectPermission() {

            }
        },PermissionInstance.getPermissons(3));
    }
}
