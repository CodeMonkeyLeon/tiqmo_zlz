package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

import java.util.regex.Pattern;

/**
 * 整数数字过滤器
 * created by junhua on 2019/7/24 13:54
 */
public class HKIDInputFilter implements InputFilter {
    private Pattern mPattern = Pattern.compile("^[A-Z]{1,2}[0-9]{6}[0-9A]?$");

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        Log.d("filter", "filter() called with: source = [" + source + "], start = [" + start + "], end = [" + end + "], dest = [" + dest + "], dstart = [" + dstart + "], dend = [" + dend + "]");
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (mPattern.matcher(temp).matches()) {
            return source;
        }
        return "";
    }
}
