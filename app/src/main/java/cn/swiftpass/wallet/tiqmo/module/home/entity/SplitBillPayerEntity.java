package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SplitBillPayerEntity extends BaseEntity {

    public String payerName;
    public String payerCallingCode;
    public String payerPhone;
    public String payerAmount;
}
