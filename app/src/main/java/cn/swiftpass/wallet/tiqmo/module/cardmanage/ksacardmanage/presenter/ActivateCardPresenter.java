package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ActivateCardPresenter implements CardKsaContract.ActivateCardPresener {

    CardKsaContract.ActivateCardView mActivateCardView;

    @Override
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire) {
        AppClient.getInstance().getCardManager().activateCard(proxyCardNo, cardNo, cardExpire, new LifecycleMVPResultCallback<Void>(mActivateCardView,true) {
            @Override
            protected void onSuccess(Void result) {
                mActivateCardView.activateCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mActivateCardView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardKsaContract.ActivateCardView activateCardView) {
        this.mActivateCardView = activateCardView;
    }

    @Override
    public void detachView() {
        this.mActivateCardView = null;
    }

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mActivateCardView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mActivateCardView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mActivateCardView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }
}
