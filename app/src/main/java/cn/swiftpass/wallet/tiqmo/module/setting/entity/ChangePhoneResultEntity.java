package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChangePhoneResultEntity extends BaseEntity {

    public String orderNo;
    //订单状态 S成功 E失败
    public String orderStatus;

    public boolean isOrderSuccess(){
        return "S".equals(orderStatus);
    }
}
