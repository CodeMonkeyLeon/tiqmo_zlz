package cn.swiftpass.wallet.tiqmo.sdk.fcm;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;

import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "Firebase";

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        UserInfoManager.getInstance().setmGcmDeviceToken(refreshedToken);
        LogUtils.i(TAG, "Refreshed token: " + refreshedToken);
    }
}
