package cn.swiftpass.wallet.tiqmo.module.register.presenter;


import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;

public interface OTPTryView<V extends BasePresenter> extends BaseView<V> {
    void tryOTPFailed(String errorCode, String errorMsg);

    void tryOTPSuccess(RegisterOtpEntity response);

}
