package cn.swiftpass.wallet.tiqmo.module.voucher;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherDetailEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;

/**
 * EVoucher store Term And Conditions
 */
public class EVoucherTAndCDialogFrag extends BaseDialogFragmentN {

    TextView tv_store_tv_title;
    TextView tv_store_tc_subtitle;
    TextView tv_store_tc_content;
    ImageView iv_tc_expanded;
    RoundedImageView iv_evoucher_card;

    private EVoucherDetailEntity eVoucherDetailEntity;

    public static EVoucherTAndCDialogFrag newInstance(EVoucherDetailEntity eVoucherDetailEntity) {
        EVoucherTAndCDialogFrag fragment = new EVoucherTAndCDialogFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("eVoucher", eVoucherDetailEntity);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_evoucher_tc;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
    }

    private void initView(View parentView) {
        tv_store_tv_title = parentView.findViewById(R.id.tv_store_tv_title);
        tv_store_tc_subtitle = parentView.findViewById(R.id.tv_store_tc_subtitle);
        tv_store_tc_content = parentView.findViewById(R.id.tv_store_tc_content);
        iv_tc_expanded = parentView.findViewById(R.id.iv_tc_expanded);
        iv_evoucher_card = parentView.findViewById(R.id.iv_evoucher_card);

        iv_tc_expanded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (getArguments() != null) {
            eVoucherDetailEntity = (EVoucherDetailEntity) getArguments().getSerializable("eVoucher");
            if (eVoucherDetailEntity != null) {
                String voucherDetailDesc = eVoucherDetailEntity.voucherDetailDesc;
                String voucherLightPicUrl = eVoucherDetailEntity.voucherLightPicUrl;
                String voucherDetailTitle = eVoucherDetailEntity.voucherDetailTitle;
                String voucherDarkPicUrl = eVoucherDetailEntity.voucherDarkPicUrl;
                tv_store_tc_subtitle.setText(voucherDetailTitle);
                tv_store_tc_content.setText(voucherDetailDesc);
                String showPicUrl = ThemeUtils.isCurrentDark(mContext) ? voucherDarkPicUrl : voucherLightPicUrl;
                if (TextUtils.isEmpty(showPicUrl)) {
                    iv_evoucher_card.setVisibility(View.GONE);
                } else {
                    iv_evoucher_card.setVisibility(View.VISIBLE);
                    Glide.with(mContext)
                            .load(showPicUrl)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(iv_evoucher_card);
                }
            }
        }
    }
}
