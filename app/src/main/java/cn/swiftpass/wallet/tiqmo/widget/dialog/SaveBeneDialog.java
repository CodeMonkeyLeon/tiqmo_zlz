package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class SaveBeneDialog extends BottomDialog {

    private Context mContext;

    private BottomButtonListener bottomButtonListener;

    public void setBottomButtonListener(BottomButtonListener bottomButtonListener) {
        this.bottomButtonListener = bottomButtonListener;
    }

    public SaveBeneDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface BottomButtonListener {
        void clickSaveButton();

        void clickCancelButton();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_save_bene, null);
        TextView tvCancel = view.findViewById(R.id.tv_cancel);
        TextView tvSave = view.findViewById(R.id.tv_save);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomButtonListener != null) {
                    bottomButtonListener.clickSaveButton();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomButtonListener != null) {
                    bottomButtonListener.clickCancelButton();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
