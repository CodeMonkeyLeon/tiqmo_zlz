package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class OrderInfoEntity extends BaseEntity {

    public String exchangeRate;
    public String needBindCard;
    public String orderAmount;
    public String orderCurrencyCode;
    public String orderDesc;
    public String orderNo;
    public String orderType;
    public String payMethod;
    public String payMethodDesc;
    public String payeeName;
    public String payeePortraitUrl;
    public String paymentId;
    public String supportMethod;
    public String transAmount;
    public String transCurrencyCode;

}
