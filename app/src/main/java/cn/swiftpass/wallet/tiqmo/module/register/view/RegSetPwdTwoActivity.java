package cn.swiftpass.wallet.tiqmo.module.register.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.MainActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterUserDetailContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.RegUserDetailPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.NumberGridAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.NumberEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterFinishEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.AuthApi;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyGridView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;

public class RegSetPwdTwoActivity extends BaseCompatActivity<RegisterUserDetailContract.Presenter> implements RegisterUserDetailContract.View {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_reg_name)
    TextView tvUserName;
    @BindView(R.id.grid_number)
    MyGridView gridNumber;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.cb_reg_one)
    CheckBox cbRegOne;
    @BindView(R.id.cb_reg_two)
    CheckBox cbRegTwo;
    @BindView(R.id.cb_reg_three)
    CheckBox cbRegThree;
    @BindView(R.id.cb_reg_four)
    CheckBox cbRegFour;
    @BindView(R.id.cb_reg_five)
    CheckBox cbRegFive;
    @BindView(R.id.cb_reg_six)
    CheckBox cbRegSix;
    @BindView(R.id.ll_check)
    LinearLayout llCheck;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.iv_user_avatar)
    RoundedImageView ivUserAvatar;
    @BindView(R.id.con_avatar)
    ConstraintLayout conAvatar;
    @BindView(R.id.tv_name_first)
    TextView tvNameFirst;
    @BindView(R.id.tv_content_title)
    TextView tvContentTitle;

    private NumberGridAdapter numberGridAdapter;
    private StringBuilder stringBuilder;
    private CheckPhoneEntity checkPhoneEntity;
    private String pdTwo,pdOne;
    private String phone,callingCode;

    private String referralCode;
    private String shareLink,noStr = "";

    private int operateType;
    //是否是忘记登录密码流程
    private boolean isChangeLoginPd;

    private String pdType, operatorType,oldPd = "";
    private RiskControlEntity riskControlEntity;

    private static RegSetPwdTwoActivity regSetPwdTwoActivity;

    public static RegSetPwdTwoActivity getRegSetPwdTwoActivity() {
        return regSetPwdTwoActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_set_reg_pwd_one;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        regSetPwdTwoActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvContentTitle.setText(R.string.sprint19_23);
        if(getIntent() != null && getIntent().getExtras() != null){
            isChangeLoginPd = getIntent().getExtras().getBoolean(Constants.isChangeLoginPd);
        }
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if(isChangeLoginPd){
            tvTitle.setText(R.string.sprint19_33);
        }else {
            if (checkPhoneEntity != null && checkPhoneEntity.operateType == 1) {
                tvTitle.setText(R.string.sprint19_21);
            } else {
                tvTitle.setText(R.string.sprint19_31);
            }
        }
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkUser(userInfoEntity);
        if(checkPhoneEntity != null) {
            noStr = checkPhoneEntity.noStr;
            operateType = checkPhoneEntity.operateType;
            pdOne = checkPhoneEntity.passwordOne;
            phone = checkPhoneEntity.phone;
        }
        callingCode = UserInfoManager.getInstance().getCallingCode();
        referralCode = SpUtils.getInstance().getReferralCode();
        shareLink = SpUtils.getInstance().getShareLink();


        stringBuilder = new StringBuilder();
        numberGridAdapter = new NumberGridAdapter(mContext);
        gridNumber.setAdapter(numberGridAdapter);
        numberGridAdapter.setOnItemClickListener(new NumberGridAdapter.OnItemClick() {
            @Override
            public void onItemClick(NumberEntity numberEntity, int position) {
                if(numberEntity != null){
                    int type = numberEntity.type;
                    int length = stringBuilder.length();
                    if(type == 1){
                        if(length == 6)return;
                        String number = numberEntity.number;
                        if(!TextUtils.isEmpty(number)) {
                            stringBuilder.append(number);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length);
                            checkBox.setChecked(true);
                        }
                    }else if(type == 2){
                        if(stringBuilder.length()>0) {
                            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length-1);
                            checkBox.setChecked(false);
                        }
                    }
                    if(stringBuilder.length() == 6){
                        pdTwo = stringBuilder.toString();
                        if(pdTwo.equals(pdOne)) {
                            if(isChangeLoginPd){
                                pdType = "B";
                                operatorType = "U";
                                if(checkPhoneEntity != null) {
                                    oldPd = checkPhoneEntity.oldPassword;
                                }
                                mPresenter.preCheckPd(pdType,operatorType,oldPd);
                            }else{
                                if(operateType == 1) {
                                    //注册流程
                                    mPresenter.finishRegister("", callingCode, phone, pdTwo, Constants.REG_PHONE, ProjectApp.getLongitude(), ProjectApp.getLatitude(), noStr,
                                            referralCode,
                                            shareLink);
                                }else{
                                    //忘记登录密码流程
                                    mPresenter.forgetSetPwd(SpUtils.getInstance().getCallingCode(), phone, pdTwo);
                                }
                            }
                        }else{
                            setErrorMsg(getString(R.string.Change_Login_Password_CPP_0021_1_D_12));
                        }
                    }
                }
            }
        });
        numberGridAdapter.changeData(AndroidUtils.getRegNumberList(mContext));

    }

    private void setErrorMsg(String errorMsg){
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(errorMsg);
        for(int i=0;i<6;i++) {
            CheckBox checkBox = (CheckBox) llCheck.getChildAt(i);
            checkBox.setChecked(false);
        }
        stringBuilder = new StringBuilder();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        regSetPwdTwoActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_CHANGE_LOGIN_PD == event.getEventType()) {
            requestTransfer();
        }
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.getAvatar();
            String gender = userInfoEntity.gender;
            if (!TextUtils.isEmpty(userInfoEntity.userName)) {
                if (userInfoEntity.userName.contains(" ")) {
                    String[] fastName = userInfoEntity.userName.split(" ");
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + fastName[0] + "!");
                } else {
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + userInfoEntity.userName + "!");
                }
                tvNameFirst.setText(userInfoEntity.userName.substring(0,1).toUpperCase(Locale.ENGLISH));
            }
            if (!TextUtils.isEmpty(avatar)) {
                conAvatar.setVisibility(View.GONE);
                ivUserAvatar.setVisibility(View.VISIBLE);
                Glide.with(this).clear(ivUserAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .placeholder(R.drawable.l_default_avatar)
                        .into(ivUserAvatar);
            } else {
                conAvatar.setVisibility(View.VISIBLE);
                ivUserAvatar.setVisibility(View.GONE);
            }
        }else{
            if(checkPhoneEntity != null){
                tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + checkPhoneEntity.firstName + "!");
                conAvatar.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(checkPhoneEntity.firstName)) {
                    tvNameFirst.setText(checkPhoneEntity.firstName.substring(0, 1).toUpperCase(Locale.ENGLISH));
                }
            }
        }
    }

    @OnClick(R.id.iv_back)
    public void onClick() {
        finish();
    }

    @Override
    public void finishRegSuccess(RegisterFinishEntity response) {
        if (!TextUtils.isEmpty(response.userId)) {
//            AppClient.getInstance().setLastUserID(response.userId);
//            AppClient.getInstance().getStorageHelper().initUserPreferences(mContext, response.userId);

            AppsFlyerHelper.getInstance().sendSignUpEvent(response.userId);
        }
        loginRequest();

    }

    private void loginRequest() {
        mPresenter.loginAccount(callingCode, phone, pdTwo, "", AuthApi.LOGIN_TYPE_PD, ProjectApp.getLongitude(), ProjectApp.getLatitude());
    }

    @Override
    public void finishRegFail(String errorCode, String errorMsg) {
        setErrorMsg(errorMsg);
    }

    @Override
    public void loginAccountSuccess(UserInfoEntity response) {
        response.phone = phone;
        SpUtils.getInstance().setAreaEntity(UserInfoManager.getInstance().getAreaEntity());
        AppClient.getInstance().getUserManager().bindUserInfo(response, true);
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.LOGIN_FROM,true);
        ActivitySkipUtil.startAnotherActivity(this, MainActivity.class,mHashMaps,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        ProjectApp.removeAllTaskExcludeMainStack();
    }

    @Override
    public void loginAccountError(String errorCode, String errorMsg) {
        if("030208".equals(errorCode) || "030209".equals(errorCode) || "030210".equals(errorCode)){
            ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
            errorBottomDialog.setErrorMsg(mContext.getString(R.string.sprint18_2),errorMsg, ThemeSourceUtils.getSourceID(mContext,R.attr.icon_error_mobile_number));
            errorBottomDialog.showWithBottomAnim();
        }else {
            ActivitySkipUtil.startAnotherActivity(this, RegisterActivity.class,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            ProjectApp.removeAllTaskExcludeLoginStack();
        }
    }

    @Override
    public void forgetSetPwdSuccess(Void response) {
        AppClient.getInstance().getUserManager().clearLastUserInfo();
        ActivitySkipUtil.startAnotherActivity(this, LoginFastNewActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        ProjectApp.removeAllTaskExcludeLoginStack();
    }

    @Override
    public void forgetSetPwdFail(String errorCode, String errorMsg) {
        setErrorMsg(errorMsg);
    }

    @Override
    public void changePwdSuccess(UserInfoEntity userInfoEntity) {
        ProjectApp.removeAllTaskExcludeMainStack();
        finish();
    }

    @Override
    public void changePwdFail(String errorCode, String errorMsg) {
        setErrorMsg(errorMsg);
    }

    @Override
    public RegisterUserDetailContract.Presenter getPresenter() {
        return new RegUserDetailPresenter();
    }

    @Override
    public void preCheckPdSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            checkPhoneEntity.riskControlInfo = riskControlEntity;
            UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(RegSetPwdTwoActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.sceneType, Constants.TYPE_CHANGE_LOGIN_PD);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_CHANGE_LOGIN_PD);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.sprint19_35));
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void requestTransfer() {
        mPresenter.changePwd(pdType, pdTwo, oldPd, operatorType, "",
                "");
    }

    @Override
    public void preCheckPdFail(String errorCode, String errorMsg) {
        setErrorMsg(errorMsg);
    }
}
