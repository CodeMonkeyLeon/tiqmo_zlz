package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.KotlinUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class InviteFriendsActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.tv_invite_people)
    TextView tvInvitePeople;
    @BindView(R.id.tv_rewards_money)
    TextView tvRewardsMoney;
    @BindView(R.id.tv_rewards_content)
    TextView tvRewardsContent;
    @BindView(R.id.tv_terms_conditions)
    TextView tvTermsConditions;
    @BindView(R.id.ll_not_expire)
    LinearLayout llNotExpire;
    @BindView(R.id.ll_has_expired)
    LinearLayout llHasExpired;
    @BindView(R.id.tv_invite_url)
    TextView tvInviteUrl;
    @BindView(R.id.ll_invite_url)
    LinearLayout llInviteUrl;
    @BindView(R.id.tv_invite_friends)
    TextView tvInviteFriends;
    @BindView(R.id.ll_rewards)
    LinearLayout llRewards;
    @BindView(R.id.iv_rewards)
    ImageView ivRewards;
    @BindView(R.id.tv_rewards)
    TextView tvRewards;
    @BindView(R.id.tv_invite_time)
    TextView tvInviteTime;
    @BindView(R.id.iv_reward_gift)
    ImageView ivRewardGift;
    @BindView(R.id.iv_reward_gift_expired)
    ImageView ivRewardGiftExpired;

    private InviteEntity mInviteEntity;

    //	邀请人数
    public String countRecord;
    //	邀请人用户收到返现金额
    public String userAmount;
    //	邀请人返现金额
    public String inviterBenefit;
    //	被邀请人返现金额
    public String inviteeBenefit;
    //	币种
    public String currency;
    //	活动开始时间 格式： dd/MM/yyyy
    public String activityStartTime;
    //	活动结束时间 格式： dd/MM/yyyy
    public String activityEndTime;
    //	分享链接
    public String shareUrl;
    //	是否有邀请码活动 1: 有 0: 无
    public String hasReferralCodeActivityFlag;
    public String link;
    public String firebaseDynamicLink;
    public String referralCode;

    private static String STRING_REFERRAL_CODE = "";

    @Override
    protected int getLayoutID() {
        return R.layout.activity_invite_rewards;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        STRING_REFERRAL_CODE = getString(R.string.sprint21_19);
        tvTitle.setText(R.string.sprint11_9);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        if (getIntent() != null && getIntent().getExtras() != null) {
            mInviteEntity = (InviteEntity) getIntent().getExtras().getSerializable(Constants.INVITE_ENTITY);
            String badges = getIntent().getExtras().getString(Constants.badges);
            ivRewards.setImageResource(AndroidUtils.getRewardIcon(badges));
            tvRewards.setText(getString(AndroidUtils.getRewardTitle(badges)));
            if (mInviteEntity != null) {
                countRecord = mInviteEntity.countRecord;
                userAmount = mInviteEntity.userAmount;
                inviterBenefit = mInviteEntity.inviterBenefit;
                inviteeBenefit = mInviteEntity.inviteeBenefit;
                currency = mInviteEntity.currency;
                activityStartTime = mInviteEntity.activityStartTime;
                activityEndTime = mInviteEntity.activityEndTime;
                shareUrl = mInviteEntity.shareUrl;
                hasReferralCodeActivityFlag = mInviteEntity.hasReferralCodeActivityFlag;
                link = mInviteEntity.link;
                firebaseDynamicLink = mInviteEntity.firebaseDynamicLink;
//                firebaseDynamicLink = "https://tiqmolink.page.link";
                String fallbackUrl = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;

                if (KotlinUtils.INSTANCE.isSupportGooglePlay()) {
                    //支持google play
                    if (!TextUtils.isEmpty(link) && !TextUtils.isEmpty(firebaseDynamicLink)) {
                        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLink(Uri.parse(link))
                                .setDomainUriPrefix(firebaseDynamicLink)
                                .setAndroidParameters(
                                        new DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID)
                                                .setFallbackUrl(Uri.parse(fallbackUrl))
                                                .build())
                                .setNavigationInfoParameters(new DynamicLink.NavigationInfoParameters.Builder().setForcedRedirectEnabled(true)
                                        .build())
                                .setIosParameters(
                                        new DynamicLink.IosParameters.Builder(BuildConfig.app_bundle_id)
                                                .setAppStoreId(BuildConfig.app_store_id)
                                                .setMinimumVersion("11")
                                                .build())
                                // Set parameters
                                // ...
//                            .buildShortDynamicLink()
                                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                                    @Override
                                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                                        if (task.isSuccessful()) {
                                            // Short link created
                                            Uri shortLink = task.getResult().getShortLink();
                                            Uri flowchartLink = task.getResult().getPreviewLink();
                                            String strShortLink = shortLink.toString();
                                            if (TextUtils.isEmpty(strShortLink)) {
                                                //不支持google play, 只展示邀请码
                                                unSupportGooglePlay();
                                            } else {
                                                //支持google play, 展示短链接和邀请码
                                                tvInviteUrl.setText(strShortLink + ";" + STRING_REFERRAL_CODE + ":" + mInviteEntity.referralCode);
                                            }
                                        } else {
                                            // Error
                                            // ...
                                            unSupportGooglePlay();
                                        }
                                    }
                                });
                    }
                } else {
                    //不支持google play
                    unSupportGooglePlay();
                }


                RequestOptions options = new RequestOptions()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.DATA);
                Glide.with(mContext)
                        .asGif()
                        .load(R.drawable.reward_gift)
                        .apply(options)
                        .into(ivRewardGift);
                Glide.with(mContext)
                        .asGif()
                        .load(R.drawable.reward_gift)
                        .apply(options)
                        .into(ivRewardGiftExpired);


                ivRewards.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AndroidUtils.copyToClipBoard(mContext, tvInviteUrl.getText().toString().trim());
                    }
                });

                if (TextUtils.isEmpty(currency)) {
                    currency = LocaleUtils.getCurrencyCode("");
                }

                tvInvitePeople.setText(Spans.builder()
                        .text(countRecord + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(24)
                        .text(getString(R.string.sprint11_11)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(14)
                        .build());

                tvRewardsMoney.setText(Spans.builder()
                        .text(BigDecimalFormatUtils.add(userAmount, "0", 0) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(24)
                        .text(currency).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(14)
                        .build());

                if (!TextUtils.isEmpty(inviterBenefit) && !TextUtils.isEmpty(inviteeBenefit)) {
                    tvRewardsContent.setText(getString(R.string.sprint11_14).replace("XXX", inviterBenefit + "" + currency)
                            .replace("YYY", inviteeBenefit + "" + currency));
                }

                if (!TextUtils.isEmpty(activityStartTime) && !TextUtils.isEmpty(activityEndTime)) {
                    tvInviteTime.setText(getString(R.string.sprint11_23).replace("XXX", activityStartTime)
                            .replace("YYY", activityEndTime));
                }

//                tvInviteUrl.setText(shareUrl);

                if ("1".equals(hasReferralCodeActivityFlag)) {
                    llNotExpire.setVisibility(View.VISIBLE);
                    llHasExpired.setVisibility(View.GONE);
                } else {
                    llNotExpire.setVisibility(View.GONE);
                    llHasExpired.setVisibility(View.VISIBLE);
                }
            }
        }

        tvTermsConditions.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvTermsConditions.getPaint().setAntiAlias(true);//抗锯齿
    }


    /**
     * 不支持google play, 只展示邀请码
     */
    private void unSupportGooglePlay() {
        tvInviteUrl.setText(STRING_REFERRAL_CODE + ":" + mInviteEntity.referralCode);
    }


    @OnClick({R.id.iv_back, R.id.tv_terms_conditions, R.id.tv_invite_friends})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_terms_conditions:
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put(Constants.terms_condition_type, "1");
                ActivitySkipUtil.startAnotherActivity(InviteFriendsActivity.this, WebViewActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_invite_friends:
                String inviteUrl = tvInviteUrl.getText().toString().trim();
                if (!TextUtils.isEmpty(inviteUrl)) {
                    if ("1".equals(hasReferralCodeActivityFlag)) {
                        if (!TextUtils.isEmpty(inviterBenefit) && !TextUtils.isEmpty(inviteeBenefit)) {
                            AndroidUtils.shareInviteImage(mContext, getString(R.string.sprint11_65).replace("XXX", inviterBenefit + " " + currency)
                                            .replace("YYY", inviteeBenefit + " " + currency) + "\n" + inviteUrl,
                                    Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.drawable.l_invite_tiqmo_img));
                        }
                    } else {
                        AndroidUtils.shareInviteImage(mContext, getString(R.string.sprint11_66) + "\n" + inviteUrl,
                                Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.drawable.l_invite_tiqmo_img));
                    }
                }
                break;
            default:
                break;
        }
    }
}
