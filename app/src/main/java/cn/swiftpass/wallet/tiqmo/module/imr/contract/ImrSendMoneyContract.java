package cn.swiftpass.wallet.tiqmo.module.imr.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPurposeListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;

public class ImrSendMoneyContract {

    public interface View extends BaseView<ImrSendMoneyContract.Presenter> {

        void imrCheckoutOrderSuccess(ImrOrderInfoEntity imrOrderInfoEntity);
        void getImrPurposeListSuccess(ImrPurposeListEntity imrPurposeListEntity);

        void imrCheckoutOrderFail(String errorCode, String errorMsg);
        void getImrPurposeListFail(String errorCode, String errorMsg);

        void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity);

        void imrExchangeRateFail(String errorCode, String errorMsg);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void checkOutFail(String errorCode, String errorMsg);
    }


    public interface Presenter extends BasePresenter<ImrSendMoneyContract.View> {

        void imrCheckoutOrder(String orderAmount, String orderCurrency, String payeeId, String purpose,
                              String receiptMethod, String includeFee,String purposeId, String purposeDesc,
                              String channelCode,
                              String transFer,
                              String destinationAmount,
                              String destinationCurrency,
                              String payAmount,
                              String exchangeRate);

        void getImrPurposeList(String countryCode,String channelCode);

        void imrExchangeRate(String payeeInfoId,String sourceAmount,
                             String destinationAmount,String channelCode);

        void checkOut(String orderNo, String orderInfo);
    }

    public interface SummaryPresenter extends BasePresenter<ImrSendMoneyContract.SummaryView> {
        void imrCheck(String orderNo);

        void confirmPay(String orderNo, String paymentMethodNo, String orderType);
    }

    public interface SummaryView extends BaseView<ImrSendMoneyContract.SummaryPresenter> {
        void imrCheckSuccess(Void result);

        void imrCheckFail(String errorCode, String errorMsg);

        void confirmPaySuccess(TransferEntity transferEntity);

        void confirmPayFail(String errCode, String errMsg);
    }

    public interface MoreInfoPresenter extends BasePresenter<ImrSendMoneyContract.MoreInfoView> {
        void sendMoreInfo(List<ImrComplianceEntity> compliance);

        void checkOut(String orderNo, String orderInfo);
    }

    public interface MoreInfoView extends BaseView<ImrSendMoneyContract.MoreInfoPresenter> {
        void sendMoreInfoSuccess(ImrOrderInfoEntity result);

        void sendMoreInfoFail(String errorCode, String errorMsg);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void checkOutFail(String errorCode, String errorMsg);
    }
}
