package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zrq.spanbuilder.Spans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SpendingCategoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.AnalysisContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AnalysisGraphicColorEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AnalysisGraphicEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.CategoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.LineChartDataEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.LinePointEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingActivityDataEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingCategoryBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingCategoryListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.SpendingPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.widget.BrokenView;
import cn.swiftpass.wallet.tiqmo.widget.CircularProgressView;
import cn.swiftpass.wallet.tiqmo.widget.ColorItem;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ChooseDateDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.SpendingDateDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.SpendingDateSelectDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class SpendingsFragment extends BaseFragment<AnalysisContract.SpendingPresenter> implements AnalysisContract.SpendingView {

    @BindView(R.id.iv_left_arrow)
    ImageView ivLeftArrow;
    @BindView(R.id.tv_spending_date)
    TextView tvSpendingDate;
    @BindView(R.id.iv_right_arrow)
    ImageView ivRightArrow;
    @BindView(R.id.ll_spending_date)
    LinearLayout llSpendingDate;
    @BindView(R.id.tv_set_up)
    TextView tvSetUp;
    @BindView(R.id.iv_sort)
    ImageView ivSort;
    @BindView(R.id.rl_budget_list)
    RecyclerView rlBudgetList;
    //    @BindView(R.id.progress_received)
//    CircleProgress progressReceived;
    @BindView(R.id.tv_received_amount)
    TextView tvReceivedAmount;
    //    @BindView(R.id.progress_sending)
//    CircleProgress progressSending;
    @BindView(R.id.tv_sending_amount)
    TextView tvSendingAmount;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_left_money)
    TextView tvLeftMoney;
    @BindView(R.id.tv_activity)
    TextView tvActivity;
    @BindView(R.id.ll_money_left)
    LinearLayout llMoneyLeft;
    @BindView(R.id.progress_sending)
    CircularProgressView progressSending;
    @BindView(R.id.progress_received)
    CircularProgressView progressReceived;
    @BindView(R.id.tv_line_total_money)
    TextView tvLineTotalMoney;
    @BindView(R.id.tv_pie_avg_money)
    TextView tvPieAvgMoney;
    @BindView(R.id.tv_received_progress)
    TextView tvReceivedProgress;
    @BindView(R.id.tv_received)
    TextView tvReceived;
    @BindView(R.id.tv_sending)
    TextView tvSending;
    @BindView(R.id.tv_sending_progress)
    TextView tvSendingProgress;
    @BindView(R.id.progressView)
    CircularProgressView mProgressView;
    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.ll_activity)
    ConstraintLayout llActivity;
    @BindView(R.id.iv_date)
    ImageView ivDate;
    @BindView(R.id.ll_budget)
    ConstraintLayout llBudget;
    @BindView(R.id.ll_spending_content)
    LinearLayout llSpendingContent;
    @BindView(R.id.iv_budget)
    ImageView ivBudget;
    @BindView(R.id.tv_budget)
    TextView tvBudget;
    @BindView(R.id.view_empty)
    View viewEmpty;
    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;
    @BindView(R.id.tv_line_total_title)
    TextView tvLineTotalTitle;
    @BindView(R.id.con_spending_line)
    ConstraintLayout conSpendingLine;
    @BindView(R.id.tv_pie_total_title)
    TextView tvPieTotalTitle;
    @BindView(R.id.tv_pie_total_money)
    TextView tvPieTotalMoney;
    @BindView(R.id.ll_line_total)
    LinearLayout llLineTotal;
    @BindView(R.id.tv_pie_avg_title)
    TextView tvPieAvgTitle;
    @BindView(R.id.con_spending_pie)
    ConstraintLayout conSpendingPie;
    @BindView(R.id.ll_spending_notice)
    LinearLayout llSpendingNotice;
    @BindView(R.id.tv_spending_notice)
    TextView tvSpendingNotice;
    @BindView(R.id.brokenView)
    BrokenView brokenView;
    @BindView(R.id.tv_spending_title)
    TextView tvSpendingTitle;
    @BindView(R.id.con_spending_budget)
    ConstraintLayout conSpendingBudget;
    @BindView(R.id.rb_pie)
    RadioButton rbPie;
    @BindView(R.id.rb_line)
    RadioButton rbLine;
    @BindView(R.id.rg_pie_line)
    RadioGroup rgPieLine;
    @BindView(R.id.ll_pie_avr)
    LinearLayout llPieAvr;

    //周期类型 W 周维度 M 月维度  Y 年维度
    private String periodType = "M";
    //当前日期 格式为 YYYY-MM-DD  传给后台用
    private String spendingAnalysisQueryDate;

    private List<SpendingCategoryListEntity> spendingCategoryList = new ArrayList<>();
    private List<SpendingCategoryListEntity> sortCategoryList = new ArrayList<>();

    private SpendingCategoryAdapter spendingCategoryAdapter;

    private List<CategoryEntity> dateCategoryList = new ArrayList<>();

    private int choosePosition = 1;

    private SetUpListener setUpListener;

    private boolean isSort;

    //最新的月份和周  用来判断
    private String lastMonth, lastWeek,lastYear;

    //当前月份和周
    private String currentMonth, currentWeek;
    private String currentYear;

    //传给后台的月份或周的第一天
    private Date currentDate;
    //获取当前周
    private Date currentWeekDate;

    private Date userCreateDate = null;

    private int currentPostion,yearPosition,monthPosition,weekPosition,chooseDatePosition=-1;

    private ImageView ivNoContent;
    private View noContentView;
    private StatusView noStatusView;
    //用来计算的月份
    private List<Date> dateYearList = new ArrayList<>();
    //用来显示的月份
    private List<String> showYearList = new ArrayList<>();
    //传给接口的月份
    private List<String> sendYearList = new ArrayList<>();
    //用来计算的月份
    private List<Date> dateMonthList = new ArrayList<>();
    //用来显示的月份
    private List<String> showMonthList = new ArrayList<>();
    //传给接口的月份
    private List<String> sendMonthList = new ArrayList<>();
    //用来计算的周
    private List<Date> dateWeekList = new ArrayList<>();
    //用来显示的周
    private List<String> showWeekList = new ArrayList<>();
    //传给接口的周
    private List<String> sendWeekList = new ArrayList<>();
    //显示日期类型选择  年月周
    private SpendingDateSelectDialog spendingDateSelectDialog;

    //点击展开单个子列表
    private List<SpendingCategoryListEntity> chooseCategoryList = new ArrayList<>();

    private SpendingDetailEntity mSpendingDetailEntity;

    private boolean isEmptyItems;

    List<ColorItem> items;
    List<ColorItem> allItems;

    private Date mSelectedDate = null;

    //点对应的数据
    public List<List<String>> data = new ArrayList<>();
    //X轴刻度值
    public List<String> x = new ArrayList<>();
    //Y轴刻度
    public List<String> y = new ArrayList<>();

    List<String> xStrList = new ArrayList<>();
    List<String> yStrList = new ArrayList<>();
    HashMap<Double,Double> map=new HashMap<>();
    private List<LinePointEntity> pointList = new ArrayList<>();

    private String totalSpendingAmount;

    public void setSetUpListener(final SetUpListener setUpListener) {
        this.setUpListener = setUpListener;
    }

    public static SpendingsFragment getInstance() {
        SpendingsFragment spendingsFragment = new SpendingsFragment();
        return spendingsFragment;
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(bgMain, R.attr.splash_bg_color);
//        helper.setBackgroundResourceByAttr(llMain, R.attr.bg_radius10_all_051446_white);
        helper.setBackgroundResourceByAttr(conSpendingBudget, R.attr.shape_051446_white_8);
        helper.setBackgroundResourceByAttr(llActivity, R.attr.shape_051446_white_8);
        helper.setBackgroundResourceByAttr(llSpendingContent, R.attr.shape_051446_white_8);
        helper.setBackgroundResourceByAttr(llBudget, R.attr.shape_10white_spending_d9e1ec);
        helper.setImageResourceByAttr(ivLeftArrow, R.attr.icon_left_date_arrow);
        helper.setImageResourceByAttr(ivRightArrow, R.attr.icon_right_date_arrow);
        helper.setImageResourceByAttr(ivDate, R.attr.icon_spending_date);
        helper.setImageResourceByAttr(ivBudget, R.attr.icon_spending_budget);
        helper.setImageResourceByAttr(ivSort, R.attr.icon_spending_sort);
        helper.setTextColorByAttr(tvSpendingDate, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvLineTotalTitle, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvPieTotalTitle, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvPieAvgTitle, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvLineTotalMoney, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvPieTotalMoney, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvPieAvgMoney, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvBudget, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvSetUp, R.attr.color_1da1f1_fc4f00);
        helper.setTextColorByAttr(tvLeftMoney, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvActivity, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvReceivedProgress, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvReceived, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvReceivedAmount, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvSendingProgress, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvSending, R.attr.color_50white_503a3b44);
        helper.setTextColorByAttr(tvSendingAmount, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvSpendingTitle, R.attr.color_white_3a3b44);

        Drawable drawable_set = getResources().getDrawable(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_set_up));
        drawable_set.setBounds(0, 0, drawable_set.getMinimumWidth(), drawable_set.getMinimumHeight());
        tvSetUp.setCompoundDrawables(null, null, drawable_set, null);

        dateCategoryList.clear();
        CategoryEntity weekCategory = new CategoryEntity(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_week), R.string.analytics_5);
        CategoryEntity monthCategory = new CategoryEntity(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_month), R.string.analytics_6);
        dateCategoryList.add(weekCategory);
        dateCategoryList.add(monthCategory);

        if (spendingCategoryAdapter != null) {
            spendingCategoryAdapter.changeTheme();
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_spendings;
    }

    private void initNoContentView() {
        noContentView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_spending, null);
        TextView tvContent = noContentView.findViewById(R.id.tv_result_content);
        TextView tvNoContent = noContentView.findViewById(R.id.tv_no_content);
        tvContent.setVisibility(View.VISIBLE);
        ivNoContent = noContentView.findViewById(R.id.iv_no_content);
        ivNoContent.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_spending));
        tvContent.setText(getString(R.string.IMR_new_8));
        tvNoContent.setText(getString(R.string.IMR_new_9));
        noStatusView = new StatusView.Builder(mContext, scrollView).setNoContentView(noContentView).build();
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, ivLeftArrow, ivRightArrow,rbLine,rbPie);
        CategoryEntity weekCategory = new CategoryEntity(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_week), R.string.analytics_5);
        CategoryEntity monthCategory = new CategoryEntity(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_month), R.string.analytics_6);
        dateCategoryList.add(weekCategory);
        dateCategoryList.add(monthCategory);
        initNoContentView();
        initRecyclerView();

        rgPieLine.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_pie:
                        brokenView.setVisibility(View.GONE);
                        conSpendingPie.setVisibility(View.VISIBLE);
                        llPieAvr.setVisibility(View.VISIBLE);
                        llLineTotal.setVisibility(View.GONE);
                        break;

                    case R.id.rb_line:
                        brokenView.setVisibility(View.VISIBLE);
                        conSpendingPie.setVisibility(View.GONE);
                        llPieAvr.setVisibility(View.GONE);
                        llLineTotal.setVisibility(View.VISIBLE);
                        break;
                    default:break;
                }
            }
        });

        currentWeek = TimeUtils.dateToEn(TimeUtils.getCurrentWeekStart()) + " - " + TimeUtils.dateToEn(TimeUtils.getWeekEnd(TimeUtils.getCurrentWeekStart()));
        lastWeek = TimeUtils.dateToEn(TimeUtils.getCurrentWeekStart()) + " - " + TimeUtils.dateToEn(TimeUtils.getWeekEnd(TimeUtils.getCurrentWeekStart()));

        currentDate = TimeUtils.getCurrentMonth();
        currentMonth = TimeUtils.monthToEn(TimeUtils.getCurrentMonth());
        lastMonth = TimeUtils.monthToEn(TimeUtils.getCurrentMonth());
        currentYear = TimeUtils.yearToEn(TimeUtils.getCurrentYear());
        lastYear = TimeUtils.yearToEn(TimeUtils.getCurrentYear());
        tvSpendingDate.setText(currentMonth+" "+currentYear);
        ivRightArrow.setVisibility(View.GONE);
        spendingAnalysisQueryDate = TimeUtils.getRequestDay(TimeUtils.getCurrentMonth());
        if (LocaleUtils.isRTL(mContext)) {
            llSpendingDate.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.color_10white_eaeef3_ar));
        }
        getAllMonthList();
//        spendingAnalysisQueryDate = AndroidUtils.getCurrentTime(System.currentTimeMillis(), "yyyy-MM-dd");
        mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
    }

    private void generateData(AnalysisGraphicEntity analysisGraphicEntity) {
        try {
            isEmptyItems = false;
            List<AnalysisGraphicColorEntity> itemInfo = analysisGraphicEntity.itemInfo;
            //总支出金额
            totalSpendingAmount = analysisGraphicEntity.totalSpendingAmount;
            spendingCategoryAdapter.setTotalSpendingAmount(totalSpendingAmount);
            String totalSpendingCategoryCurrency = analysisGraphicEntity.totalSpendingCategoryCurrency;
            String avgSpendingAmount = analysisGraphicEntity.avgSpendingAmount;
            String avgSpendingCategoryCurrency = analysisGraphicEntity.avgSpendingCategoryCurrency;
            LineChartDataEntity lineChartDataEntity = analysisGraphicEntity.lineChartData;
            if(lineChartDataEntity != null){
                data.clear();
                x.clear();
                y.clear();
                //点对应的数据
                data = lineChartDataEntity.data;
                //X轴刻度值
                x = lineChartDataEntity.x;
                //Y轴刻度
                y = lineChartDataEntity.y;

                pointList.clear();
                yStrList.clear();
                for(int i=0;i<data.get(0).size();i++) {
                    LinePointEntity linePointEntity = new LinePointEntity();
                    linePointEntity.isSelect = 0;
                    if(i == data.get(0).size()-1){
                        linePointEntity.isSelect = 1;
                    }
                    pointList.add(linePointEntity);
                }
                for(int i=0;i<y.size();i++) {
                    yStrList.add(AndroidUtils.getTransferIntNumber(y.get(i)));
                }
                int maxY = AndroidUtils.getTransferMoneyInt(y.get(y.size()-1));
                float avgValue = AndroidUtils.getTransferMoneyFloat(avgSpendingAmount);
                brokenView.setView(lineChartDataEntity.getDataMap(data.get(0)),x,yStrList,maxY,avgValue,pointList,50);

            }

//            itemInfo.clear();
//            itemInfo.addAll(new AnalysisGraphicColorEntity().getList());
            int numValues = itemInfo.size();

            items = new ArrayList<>();
            allItems = new ArrayList<>();
            for (int i = 0; i < numValues; ++i) {
                AnalysisGraphicColorEntity analysisGraphicColorEntity = itemInfo.get(i);
                int ratio = Integer.parseInt(analysisGraphicColorEntity.spendingCategoryRatio);
                String spendingCategoryId = analysisGraphicColorEntity.spendingCategoryId;
                if (ratio != 0) {
                    items.add(new ColorItem(ratio, mContext.getColor(AndroidUtils.getSpendingCategoryColor(spendingCategoryId)),
                            spendingCategoryId));
                    allItems.add(new ColorItem(ratio, mContext.getColor(AndroidUtils.getSpendingCategoryColor(spendingCategoryId)),
                            spendingCategoryId));
                }
            }

            if (items.isEmpty()) {
                isEmptyItems = true;
                items.add(new ColorItem(100, mContext.getColor(R.color.color_50dcdcdc), "1"));
//                items.add(new ColorItem(33, mContext.getColor(R.color.color_50dcdcdc), 2));
//                items.add(new ColorItem(33, mContext.getColor(R.color.color_50dcdcdc), 3));
            }
            if (allItems.isEmpty()) {
                allItems.add(new ColorItem(100, mContext.getColor(R.color.color_50dcdcdc), "1", 0));
//                allItems.add(new ColorItem(33, mContext.getColor(R.color.color_50dcdcdc), 2, 0));
//                allItems.add(new ColorItem(33, mContext.getColor(R.color.color_50dcdcdc), 3, 0));
            }

            mProgressView.setColorItemClickListener(new CircularProgressView.ColorItemClickListener() {
                @Override
                public void clickColor(ColorItem colorItem) {
                    String spendingCategoryId = colorItem.spendingCategoryId;
                    int position = Integer.parseInt(spendingCategoryId) - 1;
                    int select = colorItem.isSelect;
                    setCirularCheck(position, select, spendingCategoryId);
                }
            });

            mProgressView.setItems(items);

            tvLineTotalMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferStringMoney(totalSpendingAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(20)
                    .text(" "+LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Light.ttf":"fonts/SFProDisplay-Light.ttf")).size(16)
                    .build());

            tvPieTotalMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferStringMoney(totalSpendingAmount)+"\n").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(22)
                    .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Light.ttf":"fonts/SFProDisplay-Light.ttf")).size(14)
                    .build());

            tvPieAvgMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferStringMoney(avgSpendingAmount)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(20)
                    .text(" "+LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Light.ttf":"fonts/SFProDisplay-Light.ttf")).size(14)
                    .build());

        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void setCirularCheck(int position, int select, String spendingCategoryId) {
        if (isEmptyItems) return;
        if (select == 1) {
            if (currentPostion != position) {
                clickHideDetail(position);
                clickShowDetail(position);
                currentPostion = position;
            } else {
                clickShowDetail(position);
                currentPostion = position;
            }
        } else {
            clickHideDetail(position);
        }
        if (select == 1) {
            if (position == items.size()) {
                position = position - 1;
            }
            for (int i = 0; i < items.size(); i++) {
                if (i == position) {
                    items.get(i).setColor(mContext.getColor(AndroidUtils.getSpendingCategoryColor(spendingCategoryId)));
                } else {
                    items.get(i).isSelect = 0;
                    items.get(i).setColor(mContext.getColor(R.color.color_50dcdcdc));
                }
            }
            mProgressView.setItems(items);
        } else {
            mProgressView.setItems(allItems);
        }
    }

    private void setActivityPie(CircularProgressView progress, int ratio, int checkColor) {
        try {
            progress.setVisibility(View.VISIBLE);
            List<ColorItem> items = new ArrayList<>();
            if (ratio == 0) {
                progress.setBackColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_707891_30a3a3a3));
            } else if (ratio == 100) {
                progress.setBackColor(checkColor);
            } else {
                progress.setBackColor(R.color.transparent);
                items.add(new ColorItem(ratio, mContext.getColor(checkColor), "0"));
                items.add(new ColorItem(100 - ratio, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_707891_30a3a3a3)), "0"));
            }
            progress.setSpaceProgressAngle(360 * 4 / 100);
            progress.setItems(items);
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
//        rlBudgetList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rlBudgetList.setLayoutManager(manager);
        spendingCategoryAdapter = new SpendingCategoryAdapter(spendingCategoryList);
        spendingCategoryAdapter.setDetailShowListener(new SpendingCategoryAdapter.DetailShowListener() {
            @Override
            public void showDetail(int position) {
                setCirularCheck(position, 1, String.valueOf(position + 1));
            }

            @Override
            public void hideDetail(int position) {
                setCirularCheck(position, 0, String.valueOf(position + 1));
            }
        });
        spendingCategoryAdapter.bindToRecyclerView(rlBudgetList);

    }

    private void clickShowDetail(int position) {
        try {
            ivSort.setVisibility(View.GONE);
            ivSort.setEnabled(false);
            spendingCategoryAdapter.getDataList().get(position).showDetail = 1;
            chooseCategoryList.clear();
            if (spendingCategoryAdapter.getDataList().size() == 1) return;
            chooseCategoryList.add(spendingCategoryAdapter.getDataList().get(position));
            spendingCategoryAdapter.setDataList(chooseCategoryList);
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void clickHideDetail(int position) {
        ivSort.setVisibility(View.VISIBLE);
        ivSort.setEnabled(true);
        int size = spendingCategoryList.size();
        for (int i = 0; i < size; i++) {
            spendingCategoryList.get(i).showDetail = 0;
        }
        spendingCategoryAdapter.setDataList(spendingCategoryList);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {
        mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
    }

    @Override
    public void getAnalysisDetailSuccess(SpendingDetailEntity spendingDetailEntity) {
        try {
            spendingCategoryList.clear();
            sortCategoryList.clear();
            if (spendingDetailEntity != null) {
                mSpendingDetailEntity = spendingDetailEntity;
                showStatusView(noStatusView, StatusView.CONTENT_VIEW);
                SpendingActivityDataEntity spendingActivityDataEntity = spendingDetailEntity.tiqmoInnerActivityData;
                String receivedAmount = spendingActivityDataEntity.receivedAmount;
                String receivedTxnCurrency = spendingActivityDataEntity.receivedTxnCurrency;
                String receivedAmountRatio = spendingActivityDataEntity.receivedAmountRatio;
                String sendingAmount = spendingActivityDataEntity.sendingAmount;
                String sendingTxnCurrency = spendingActivityDataEntity.sendingTxnCurrency;
                String sendingAmountRatio = spendingActivityDataEntity.sendingAmountRatio;
                tvReceivedProgress.setText(Integer.parseInt(receivedAmountRatio) + "%");
                tvSendingProgress.setText(Integer.parseInt(sendingAmountRatio) + "%");
                setActivityPie(progressReceived, Integer.parseInt(receivedAmountRatio), R.color.color_6AD995);
                setActivityPie(progressSending, Integer.parseInt(sendingAmountRatio), R.color.color_fc4f00);
                tvReceivedAmount.setText(Spans.builder()
                        .text(AndroidUtils.getTransferStringMoney(receivedAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(18)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(16)
                        .build());
                tvSendingAmount.setText(Spans.builder()
                        .text(AndroidUtils.getTransferStringMoney(sendingAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(18)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(16)
                        .build());

                AnalysisGraphicEntity analysisGraphicEntity = spendingDetailEntity.graphicInfo;
                if (analysisGraphicEntity != null) {
                    generateData(analysisGraphicEntity);
                }

                spendingCategoryList.addAll(spendingDetailEntity.spendingCategoryList);
                if (spendingCategoryList.size() > 0) {
                    spendingCategoryAdapter.setDataList(spendingCategoryList);
                    int size = spendingCategoryList.size();

                    sortCategoryList.addAll(spendingCategoryList);
                    Collections.sort(sortCategoryList, new Comparator<SpendingCategoryListEntity>() {
                        @Override
                        public int compare(SpendingCategoryListEntity o1, SpendingCategoryListEntity o2) {
                            return (int) (AndroidUtils.getTransferMoneyNumber(o2.spendingCategoryAmount) - AndroidUtils.getTransferMoneyNumber(o1.spendingCategoryAmount));
                        }
                    });
                }

                SpendingCategoryBudgetEntity spendingCategoryBudgetInfo = spendingDetailEntity.spendingCategoryBudgetInfo;
                if (spendingCategoryBudgetInfo != null) {
                    //是否设置预算的标识
                    boolean setBudgetFlag = spendingCategoryBudgetInfo.setBudgetFlag;
                    //剩余预算金额
                    String leftSpendableAmount = spendingCategoryBudgetInfo.leftSpendableAmount;
                    //剩余预算币种
                    String leftSpendableTxnCurrency = spendingCategoryBudgetInfo.leftSpendableTxnCurrency;
                    //预算耗尽提醒
                    String budgetSpendingReminder = spendingCategoryBudgetInfo.budgetSpendingReminder;
                    if(!TextUtils.isEmpty(budgetSpendingReminder)){
                        llSpendingNotice.setVisibility(View.VISIBLE);
                        tvSpendingNotice.setText(budgetSpendingReminder);
                    }else{
                        llSpendingNotice.setVisibility(View.GONE);
                    }

                    if (setBudgetFlag) {
                        tvLeftMoney.setText(Spans.builder()
                                .text(AndroidUtils.getTransferStringMoney(leftSpendableAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(16)
                                .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .build());
                        tvSetUp.setVisibility(View.GONE);
                        llMoneyLeft.setVisibility(View.VISIBLE);
                    } else {
                        tvSetUp.setVisibility(View.VISIBLE);
                        llMoneyLeft.setVisibility(View.GONE);
                    }
                }

            } else {
                showStatusView(noStatusView, StatusView.NO_CONTENT_VIEW);
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
        if (mSpendingDetailEntity == null) {
            showStatusView(noStatusView, StatusView.NO_CONTENT_VIEW);
        }
    }

    private void showChooseMonthDialog(){
        ChooseDateDialog chooseDateDialog = new ChooseDateDialog(mContext,showMonthList,monthPosition,2);
        chooseDateDialog.setConfirmListener(new ChooseDateDialog.ConfirmListener() {
            @Override
            public void clickConfrim(int position) {
                if(spendingDateSelectDialog != null){
                    spendingDateSelectDialog.dismiss();
                }

                monthPosition = position;
                periodType = "M";
                spendingAnalysisQueryDate = sendMonthList.get(position);
                mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);

                currentDate = dateMonthList.get(position);
                currentMonth = TimeUtils.monthToEn(currentDate);
                tvSpendingDate.setText(showMonthList.get(position));
                if (lastMonth.equals(currentMonth)) {
                    ivRightArrow.setVisibility(View.GONE);
                } else {
                    ivRightArrow.setVisibility(View.VISIBLE);
                }

                if (monthPosition == 0) {
                    ivLeftArrow.setVisibility(View.GONE);
                } else {
                    ivLeftArrow.setVisibility(View.VISIBLE);
                }
            }
        });
        chooseDateDialog.showWithBottomAnim();
    }

    private void showChooseWeekDialog(){
        ChooseDateDialog chooseDateDialog = new ChooseDateDialog(mContext,showWeekList,weekPosition,1);
        chooseDateDialog.setConfirmListener(new ChooseDateDialog.ConfirmListener() {
            @Override
            public void clickConfrim(int position) {
                if(spendingDateSelectDialog != null){
                    spendingDateSelectDialog.dismiss();
                }

                weekPosition = position;
                periodType = "W";
                spendingAnalysisQueryDate = sendWeekList.get(position);
                mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);

                currentDate = dateWeekList.get(position);
                currentWeek = showWeekList.get(position);
                tvSpendingDate.setText(currentWeek);
                if (lastWeek.equals(currentWeek)) {
                    ivRightArrow.setVisibility(View.GONE);
                } else {
                    ivRightArrow.setVisibility(View.VISIBLE);
                }

                if (weekPosition == 0) {
                    ivLeftArrow.setVisibility(View.GONE);
                } else {
                    ivLeftArrow.setVisibility(View.VISIBLE);
                }
            }
        });
        chooseDateDialog.showWithBottomAnim();
    }

    private void showChooseYearDialog(){
        ChooseDateDialog chooseDateDialog = new ChooseDateDialog(mContext,showYearList,yearPosition,3);
        chooseDateDialog.setConfirmListener(new ChooseDateDialog.ConfirmListener() {
            @Override
            public void clickConfrim(int position) {
                if(spendingDateSelectDialog != null){
                    spendingDateSelectDialog.dismiss();
                }
                yearPosition = position;
                currentDate = dateYearList.get(position);
                periodType = "Y";
                spendingAnalysisQueryDate = sendYearList.get(position);
                mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
                currentYear = showYearList.get(position);
                tvSpendingDate.setText(currentYear);
                if (lastYear.equals(currentYear)) {
                    ivRightArrow.setVisibility(View.GONE);
                } else {
                    ivRightArrow.setVisibility(View.VISIBLE);
                }
                if (yearPosition == 0) {
                    ivLeftArrow.setVisibility(View.GONE);
                } else {
                    ivLeftArrow.setVisibility(View.VISIBLE);
                }
            }
        });
        chooseDateDialog.showWithBottomAnim();
    }

    private void clickLeftRight(boolean isLeft){
        try {
            switch (periodType) {
                case "W":
                    if (isLeft) {
                        weekPosition = weekPosition - 1;
                    } else {
                        weekPosition = weekPosition + 1;
                    }
                    spendingAnalysisQueryDate = sendWeekList.get(weekPosition);
                    mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);

                    currentDate = dateWeekList.get(weekPosition);
                    currentWeek = showWeekList.get(weekPosition);
                    tvSpendingDate.setText(currentWeek);
                    if (lastWeek.equals(currentWeek)) {
                        ivRightArrow.setVisibility(View.GONE);
                    } else {
                        ivRightArrow.setVisibility(View.VISIBLE);
                    }

                    if (weekPosition == 0) {
                        ivLeftArrow.setVisibility(View.GONE);
                    } else {
                        ivLeftArrow.setVisibility(View.VISIBLE);
                    }
                    break;
                case "M":
                    if (isLeft) {
                        monthPosition = monthPosition - 1;
                    } else {
                        monthPosition = monthPosition + 1;
                    }
                    spendingAnalysisQueryDate = sendMonthList.get(monthPosition);
                    mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);

                    currentDate = dateMonthList.get(monthPosition);
                    currentMonth = TimeUtils.monthToEn(currentDate);
                    tvSpendingDate.setText(showMonthList.get(monthPosition));
                    if (lastMonth.equals(currentMonth)) {
                        ivRightArrow.setVisibility(View.GONE);
                    } else {
                        ivRightArrow.setVisibility(View.VISIBLE);
                    }

                    if (monthPosition == 0) {
                        ivLeftArrow.setVisibility(View.GONE);
                    } else {
                        ivLeftArrow.setVisibility(View.VISIBLE);
                    }
                    break;
                case "Y":
                    if (isLeft) {
                        yearPosition = yearPosition - 1;
                    } else {
                        yearPosition = yearPosition + 1;
                    }
                    currentDate = dateYearList.get(yearPosition);
                    periodType = "Y";
                    spendingAnalysisQueryDate = sendYearList.get(yearPosition);
                    mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
                    currentYear = showYearList.get(yearPosition);
                    tvSpendingDate.setText(currentYear);
                    if (lastYear.equals(currentYear)) {
                        ivRightArrow.setVisibility(View.GONE);
                    } else {
                        ivRightArrow.setVisibility(View.VISIBLE);
                    }
                    if (yearPosition == 0) {
                        ivLeftArrow.setVisibility(View.GONE);
                    } else {
                        ivLeftArrow.setVisibility(View.VISIBLE);
                    }
                    break;
                default:
                    break;
            }
        }catch (Throwable e){
            e.getStackTrace();
        }
    }

    private void showSpendingDateSelectDialog(){
        spendingDateSelectDialog = new SpendingDateSelectDialog(mContext,chooseDatePosition);
        spendingDateSelectDialog.setDateItemClickListener(new SpendingDateSelectDialog.DateItemClickListener() {
            @Override
            public void chooseDate(int position) {
                chooseDatePosition = position;
                switch (position){
                    case 0:
                        showChooseWeekDialog();
                        break;
                    case 1:
                        showChooseMonthDialog();
                        break;
                    case 2:
                        showChooseYearDialog();
                        break;
                    default:break;
                }
            }
        });
        spendingDateSelectDialog.showWithBottomAnim();
    }

    @Override
    public AnalysisContract.SpendingPresenter getPresenter() {
        return new SpendingPresenter();
    }

    @OnClick({R.id.iv_left_arrow, R.id.iv_right_arrow, R.id.iv_date, R.id.tv_set_up, R.id.iv_sort})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_left_arrow:
                clickLeftRight(true);
                break;
            case R.id.iv_right_arrow:
                clickLeftRight(false);
                break;
            case R.id.iv_date:
                showSpendingDateSelectDialog();
//                showSpendingDateDialog();
                break;
            case R.id.tv_set_up:
                if (setUpListener != null) {
                    setUpListener.changePage();
                }
                break;
            case R.id.iv_sort:
                if (isSort) {
                    isSort = false;
                    ivSort.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_sort));
                    spendingCategoryAdapter.setDataList(spendingCategoryList);
                } else {
                    ivSort.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_sort_enable));
//                    ivSort.setImageResource(R.drawable.sort_enable);
                    spendingCategoryAdapter.setDataList(sortCategoryList);
                    isSort = true;
                }
                break;
            default:
                break;
        }
    }

    public interface SetUpListener {
        void changePage();
    }

    private void showSpendingDateDialog() {
        SpendingDateDialog spendingDateDialog = new SpendingDateDialog(mContext);
        spendingDateDialog.setDateList(dateCategoryList, choosePosition);
        spendingDateDialog.setDateItemClickListener(new SpendingDateDialog.DateItemClickListener() {
            @Override
            public void chooseDate(int position) {
                choosePosition = position;
                if (position == 0) {
                    periodType = "W";
                    tvSpendingDate.setText(lastWeek);
                    currentDate = TimeUtils.getCurrentWeekStart();
                } else if (position == 1) {
                    periodType = "M";
                    tvSpendingDate.setText(lastMonth);
                    currentDate = TimeUtils.getCurrentMonth();
                }
                ivLeftArrow.setVisibility(View.VISIBLE);
                ivRightArrow.setVisibility(View.GONE);
                spendingAnalysisQueryDate = TimeUtils.getRequestDay(currentDate);
                mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
            }
        });
        spendingDateDialog.showWithBottomAnim();
    }

    private void getAllMonthList(){
        try {
            String userCreateDateTime = SpUtils.getInstance().getLoginUser().userCreateDateTime;
//          userCreateDateTime = "2021-10-05 16:12:49";
            if(TextUtils.isEmpty(userCreateDateTime)){
                userCreateDateTime = "2021-01-01 00:00:00";
            }
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            userCreateDate = df.parse(userCreateDateTime);
            int year = Calendar.getInstance().get(Calendar.YEAR);

            dateYearList.clear();
            sendYearList.clear();
            showYearList.clear();
            //获取当前年份第一天
            Date currentYearDay = TimeUtils.getFirstDayByYear(currentDate);
            String sendYearDay = TimeUtils.getRequestDay(currentYearDay);
            String showYear = TimeUtils.yearToEn(currentYearDay);
            dateYearList.add(currentYearDay);
            sendYearList.add(sendYearDay);
            showYearList.add(showYear);
            if(userCreateDate.after(currentYearDay)){
            }else {
                while (true){
                    currentYearDay = TimeUtils.getLastYear(currentYearDay);
                    String sendYearDate = TimeUtils.getRequestDay(currentYearDay);
                    String showYearDate = TimeUtils.yearToEn(currentYearDay);
                    dateYearList.add(currentYearDay);
                    sendYearList.add(sendYearDate);
                    showYearList.add(showYearDate);
                    if (userCreateDate.after(currentYearDay)) {
                        yearPosition = showYearList.size()-1;
                        break;
                    }
                }
            }

            dateMonthList.clear();
            sendMonthList.clear();
            showMonthList.clear();

            //获取当前月份的第一天
            Date currentDate = TimeUtils.getCurrentMonth();
            String requestDay = TimeUtils.getRequestDay(currentDate);
            String showDay = TimeUtils.monthToEn(currentDate);
            dateMonthList.add(currentDate);
            sendMonthList.add(requestDay);
            showMonthList.add(showDay +" "+year);
            if(userCreateDate.after(currentDate)){
            }else {
                while(true){
                    currentDate = TimeUtils.getLastMonth(currentDate);
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(currentDate);
                    year = calendar1.get(Calendar.YEAR);
                    String send_day = TimeUtils.getRequestDay(currentDate);
                    String show_day = TimeUtils.monthToEn(currentDate);
                    dateMonthList.add(currentDate);
                    sendMonthList.add(send_day);
                    showMonthList.add(show_day + " " + year);
                    if (userCreateDate.after(currentDate)) {
                        monthPosition = showMonthList.size()-1;
                        break;
                    }
                }
            }
            if(showMonthList.size() == 1){
                ivLeftArrow.setVisibility(View.GONE);
            }

            dateWeekList.clear();
            sendWeekList.clear();
            showWeekList.clear();
            //获取当月的第一天
            Date firstDay = TimeUtils.getCurrentMonth();
            //获取当前周
            Date mCurrentWeek = TimeUtils.getCurrentWeekStart();

            String requestWeek = TimeUtils.getRequestDay(mCurrentWeek);
            String showWeek = TimeUtils.dateToEn(TimeUtils.getWeekStart(mCurrentWeek)) + " - " + TimeUtils.dateToEn(TimeUtils.getWeekEnd(mCurrentWeek));
            dateWeekList.add(mCurrentWeek);
            sendWeekList.add(requestWeek);
            showWeekList.add(showWeek);
            if(userCreateDate.after(mCurrentWeek)){
            }else {
                while (true){
                    Date currentWeekDate = TimeUtils.getLastWeek1(mCurrentWeek);
                    mCurrentWeek = TimeUtils.getWeekStart(currentWeekDate);
                    String sendWeek = TimeUtils.getRequestDay(mCurrentWeek);
                    String show_week = TimeUtils.dateToEn(TimeUtils.getWeekStart(mCurrentWeek)) + " - " + TimeUtils.dateToEn(TimeUtils.getWeekEnd(mCurrentWeek));
                    dateWeekList.add(mCurrentWeek);
                    sendWeekList.add(sendWeek);
                    showWeekList.add(show_week);
                    if (userCreateDate.after(mCurrentWeek)) {
                        weekPosition = showWeekList.size()-1;
                        break;
                    }
                }
            }
            Collections.reverse(showYearList);
            Collections.reverse(sendYearList);
            Collections.reverse(dateYearList);
            Collections.reverse(showMonthList);
            Collections.reverse(sendMonthList);
            Collections.reverse(dateMonthList);
            Collections.reverse(showWeekList);
            Collections.reverse(sendWeekList);
            Collections.reverse(dateWeekList);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
