package cn.swiftpass.wallet.tiqmo.module.guide.entity

import java.io.Serializable

data class GuideSelectedPoint(
    val x: Float,
    val y: Float,
    val r: Float,
    val size: Float
) : Serializable {


    fun getTopRightX() = x + r

    fun getTopRightY() = y


    fun getBottomRightX() = getTopRightX() + size * 4


    fun getBottomRightY() = y + size


    fun getLeftRx() = x + r

    fun getLeftRy() = y + r


    fun getRightRx() = getBottomRightX()

    fun getRightRy() = y + r


}
