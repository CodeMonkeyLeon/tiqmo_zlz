package cn.swiftpass.wallet.tiqmo.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.MapResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.DownloadFileUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TokenDataEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankIdAddressEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPayerDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPurposeListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RegionEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.TransferPdfEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.ScanOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveTransferDetail;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferPwdTimeEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.OrderExistEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.database.DBHelper;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AuthorizationVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterFinishEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.manager.BiometricManager;
import cn.swiftpass.wallet.tiqmo.sdk.manager.CardManager;
import cn.swiftpass.wallet.tiqmo.sdk.manager.TransferManager;
import cn.swiftpass.wallet.tiqmo.sdk.manager.UserManager;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.AuthApi;
import cn.swiftpass.wallet.tiqmo.sdk.util.CallbackUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.ResourcesHelper;
import cn.swiftpass.wallet.tiqmo.sdk.util.Sha256Util;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class AppClient {

    private static String sServerUrl;
    private static String sServerUploadUrl;
    private static String sPartnerNo;
    private static String sOSType;
    private static String sVersion;
    private static String sLanguage;
    private static String sDefaultInternationalAreaCode;
    private static boolean sEnableEncryptMode;

    private static final String APP_STORAGE_NAME = "SwiftPassPreferences";

    @SuppressLint("StaticFieldLeak")
    private static AppClient sAppClient;

    //初始化过程，参数包括服务器url，文件服务器Url(用来上次头像)，机构号，系统类型，语言，默认的国际手机区号，app版本号
    public static void init(Context context, String serverUrl, String serverUploadUrl,
                            String partnerNo, String osType, String language,
                            String defaultInternationalAreaCode, String version,
                            boolean isEnableEncryptMode, String serverPublicKey) {
        sServerUrl = serverUrl;
        sServerUploadUrl = serverUploadUrl;
        sPartnerNo = partnerNo;
        sOSType = osType;
        sVersion = version;
        sDefaultInternationalAreaCode = defaultInternationalAreaCode;
        sLanguage = language;
        sEnableEncryptMode = isEnableEncryptMode;
        AppConfig.SERVER_PUBLIC_KEY = serverPublicKey;
        ResourcesHelper.init(context.getApplicationContext());//初始化ResourcesHelper,里面包含了getString(id)的操做
        sAppClient = new AppClient(context.getApplicationContext());//生成单例
        ApiHelper.createKey();
    }

    public static AppClient getInstance() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sAppClient;
    }

    public static String getServerUrl() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sServerUrl;
    }


    public static String getServerUploadUrl() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sServerUploadUrl;
    }


    public static String getPartnerNo() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sPartnerNo;
    }

    public static String getOSType() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sOSType;
    }

    public static String getVersion() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sVersion;
    }

    public static String getLanguage() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sLanguage;
    }

    public boolean isLoginSuccess() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return getUserManager().getUserInfo() != null &&
                !TextUtils.isEmpty(getUserManager().getUserInfo().getSessionId());
    }

    public static void setLanguage(String sLanguage) {
        AppClient.sLanguage = sLanguage;
    }

    public static boolean isEnableEncryptMode() {
        if (sAppClient == null) {
            throw new RuntimeException("AppClient is not initialized");
        }
        return sEnableEncryptMode;
    }

    public static String getDefaultInternationalAreaCode() {
        return sDefaultInternationalAreaCode;
    }

    private Context mAppContext;
    private CardManager mCardManager;
    private UserManager mUserManager;
    private TransferManager mTransferManager;
    private BiometricManager mBiometricManager;
    private StorageHelper mStorageHelper;

    private DBHelper mDBHelper;
    private AuthApi mAuthApi;
    private String mSessionID;
    private String mUserID;
    private String mDeviceID;
    private String mDeviceModel;
    private boolean isDeviceLogin;

    private static OnLoginExpiredListener mOnLoginExpiredListener;

    private AppClient(Context appContext) {
        mAppContext = appContext;
        mDBHelper = new DBHelper(mAppContext, mAppContext.getPackageName(), 1);//初始化数据库helper
        mAuthApi = ApiHelper.getApi(AuthApi.class);
        mStorageHelper = new StorageHelper(mAppContext, APP_STORAGE_NAME);//初始化用于加密存储的helper
        //初始化各种manager
        mUserManager = new UserManager(this);
        mCardManager = new CardManager(this);
        mTransferManager = new TransferManager(this);
        mBiometricManager = new BiometricManager(this);
        mStorageHelper.initUserPreferences(mAppContext, mUserID);
    }

    //获取手机验证码，类型自己传
    public void obtainVerifyCodeOfType(String callingCode, String phone, String type, List<String> additionalData, ResultCallback<RegisterOtpEntity> callback) {
        mAuthApi.sendVerifyCode(callingCode, phone, type, additionalData)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //校验验证码，类型自己传
    public void checkVerifyCodeOfType(String callingCode, String phone, String verifyCode, String type, String orderNo, ResultCallback<Void> callback) {
        mAuthApi.checkVerifyCode(callingCode, phone, verifyCode, type, orderNo)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //判断手机号是否已注册
    public void checkPhoneReg(String callingCode, String phone, double longitude, double latitude,
                              String type, String checkIdNumber, String birthday, String needCheckIdNumber, LifecycleMVPResultCallback<CheckPhoneEntity> callback) {
        mAuthApi.checkPhoneReg(callingCode, phone, longitude, latitude, type, checkIdNumber, birthday, needCheckIdNumber).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    public void checkForgetPd(String callingCode, String phone, String type, LifecycleMVPResultCallback<CheckPhoneEntity> callback) {
        mAuthApi.checkForgetPd(callingCode, phone, type).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void checkPhoneUpdate(String callingCode, String phone, String checkIdNumber, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.checkPhoneUpdate(callingCode, phone, checkIdNumber).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void mapSearch(String query, String key, ResultCallback<MapResultEntity> callback) {
        mAuthApi.mapSearch(query, key).mapEnqueue(BuildConfig.googleMapUrl, callback);
    }

    //修改登录支付密码
    public void changePassword(String pdType, String password, String oldPassword, String operatorType, String sceneType, String referralCode, ResultCallback<UserInfoEntity> callback) {
        mAuthApi.changePassword("B", Sha256Util.sha256WithSalt(password), Sha256Util.sha256WithSalt(oldPassword), operatorType, sceneType, referralCode)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //忘记登录密码
    public void forgetPassword(String callingCode, String phone, String password, ResultCallback<Void> callback) {
        mAuthApi.forgetPwd(callingCode, phone, Sha256Util.sha256WithSalt(password), "B")
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //用手机号注册
    public void registerByPhone(String fullName, String callingCode, String phone, String password, double longitude, double latitude, String noStr,
                                String referralCode,
                                String shareLink, LifecycleMVPResultCallback<RegisterFinishEntity> callback) {
        mAuthApi.register(fullName, callingCode, phone, Sha256Util.sha256WithSalt(password), "B", "TYPE_1", longitude, latitude, noStr,
                        referralCode, shareLink)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //Kyc验证
    public void kycVerify(String callingCode, String phone, String residentId, String birthday,
                          String sceneType, String statesId, String citiesId, String address,
                          String employmentCode,
                          String professionCode,
                          String sourceOfFundCode,
                          String salaryRangeCode, String employerName, String beneficialOwner, String relativesExposedPerson,
                          String referralCode,
                          String shareLink,
                          LifecycleMVPResultCallback<UserInfoEntity> callback) {
        mAuthApi.kycVerify(callingCode, phone, residentId, birthday, sceneType, statesId, citiesId,
                address, employmentCode, professionCode, sourceOfFundCode, salaryRangeCode, employerName,
                beneficialOwner, relativesExposedPerson, referralCode, shareLink).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //kyc邀请码验证
    public void referralVerify(String referralCode, ResultCallback<Void> callback) {
        mAuthApi.verifyReferral(referralCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询限额
    public void getLimit(String type, ResultCallback<TransferLimitEntity> callback) {
        mAuthApi.getLimit(type).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询拆分账单
    public void getSplit(String splitType, LifecycleMVPResultCallback<SplitListEntity> callback) {
        mAuthApi.getSplit(splitType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //通知未支付的人
    public void sendSplitsRemind(String originalOrderNo, String orderRelevancy, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.sendSplitsRemind(originalOrderNo, orderRelevancy).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询拆分账单详情
    public void getSplitDetail(String originalOrderNo, String orderRelevancy, String splitType, LifecycleMVPResultCallback<SendReceiveSplitDetailEntity> callback) {
        mAuthApi.getSplitDetail(originalOrderNo, orderRelevancy, splitType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询手续费
    public void getTransferFee(String type, String orderAmount, String orderCurrencyCode, String transTimeType, LifecycleMVPResultCallback<TransFeeEntity> callback) {
        mAuthApi.getTransferFee(type, orderAmount, orderCurrencyCode, transTimeType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询余额
    public void getBalance(ResultCallback<BalanceListEntity> callback) {
        mAuthApi.getBalance().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //消息中心消息列表
    public void getMsgList(int pageNum, int pageSize, LifecycleMVPResultCallback<MsgListEntity> callback) {
        mAuthApi.getMsgList(pageNum, pageSize).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除消息
    public void removeRequest(String infoId, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.removeRequest(infoId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //生成二维码
    public void getQrCode(String currencyCode, ResultCallback<QrCodeEntity> callback) {
        mAuthApi.getQrCode(currencyCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //生成被扫二维码
    public void getCrCode(String paymentMethodNo, LifecycleMVPResultCallback<QrCodeEntity> callback) {
        mAuthApi.getCrCode(paymentMethodNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //添加受益人
    public void addBeneficiary(String beneficiaryName,
                               String beneficiaryIbanNo,
                               String chooseRelationship, String saveFlag, LifecycleMVPResultCallback<BeneficiaryEntity> callback) {
        mAuthApi.addBeneficiary(beneficiaryName, beneficiaryIbanNo, chooseRelationship, saveFlag).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发送通知
    public void sendNotify(List<NotifyEntity> receivers, ResultCallback<PayBillOrderInfoEntity> callback) {
        mAuthApi.sendNotify(receivers).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //检查联系人
    public void checkContact(String callingCode, String phone, String contactsName, String sceneType, LifecycleMVPResultCallback<KycContactEntity> callback) {
        mAuthApi.checkContact(callingCode, phone, contactsName, sceneType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //sendreceive列表
    public void SendReceiveDetail(String groupId, String tradeType, String isGroup, int pageNum, int pageSize, LifecycleMVPResultCallback<SendReceiveDetailListEntity> callback) {
        mAuthApi.SendReceiveDetail(groupId, tradeType, isGroup, pageNum, pageSize).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //转账、请求转账接口
    public void getTransferDetail(String groupId, String isGroup, LifecycleMVPResultCallback<SendReceiveTransferDetail> callback) {
        mAuthApi.getTransferDetail(groupId, isGroup).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //请求中心查询
    public void getRequestCenterList(LifecycleMVPResultCallback<RequestCenterListEntity> callback) {
        mAuthApi.getRequestCenterList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除受益人
    public void deleteBeneficiary(String id, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.deleteBeneficiary(id).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取受益人列表
    public void getBeneficiaryList(LifecycleMVPResultCallback<BeneficiaryListEntity> callback) {
        mAuthApi.getBeneficiaryList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询当前转账时间是否可用
    public void getTransferTime(String ibanNo, LifecycleMVPResultCallback<TimeEntity> callback) {
        mAuthApi.getTransferTime(ibanNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //扫码获取用户信息
    public void chenckQrCode(String qRCodeInfo, LifecycleMVPResultCallback<UserInfoEntity> callback) {
        mAuthApi.checkQrCode(qRCodeInfo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //扫码获取信息
    public void checkOrderCode(String qRCodeInfo, LifecycleMVPResultCallback<ScanOrderEntity> callback) {
        mAuthApi.checkOrderCode(qRCodeInfo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取交易历史
    public void getTransferMainHistory(String protocolNo, String accountNo, List<String> paymentType,
                                       List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum, String direction,
                                       String orderNo, ResultCallback<TransferHistoryMainEntity> callback) {
        mAuthApi.getTransferMainHistory(protocolNo, accountNo, paymentType,
                tradeType, startDate, endDate, pageSize, pageNum, direction, orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取交易历史
    public void getTransferMainHistory(
            String protocolNo,
            String accountNo,
            List<String> paymentType,
            List<String> tradeType,
            String startDate,
            String endDate,
            int pageSize,
            int pageNum,
            String direction,
            String orderNo,
            LifecycleMVPResultCallback<TransferHistoryMainEntity> callback
    ) {
        mAuthApi.getTransferMainHistory(
                protocolNo,
                accountNo,
                paymentType,
                tradeType,
                startDate,
                endDate,
                pageSize,
                pageNum,
                direction,
                orderNo
        ).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取预授权交易历史
    public void getPreAuthTransferMainHistory(
            String protocolNo,
            String startDate,
            String endDate,
            int pageSize,
            int pageNum,
            String direction,
            String orderNo,
            ResultCallback<TransferHistoryMainEntity> callback
    ) {
        mAuthApi.getPreAuthTransferMainHistory(
                protocolNo,
                startDate,
                endDate,
                pageSize,
                pageNum,
                direction,
                orderNo
        ).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //交易历史详情
    public void getTransferHistoryDetail(
            String orderNo,
            String orderType,
            String queryType,
            ResultCallback<TransferHistoryDetailEntity> callback
    ) {
        mAuthApi.getHistoryDetail(
                orderNo,
                orderType,
                queryType
        ).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //预授权交易历史详情
    public void getPreAuthTransferHistoryDetail(
            String orderNo,
            String orderType,
            ResultCallback<TransferHistoryDetailEntity> callback
    ) {
        mAuthApi.getPreAuthHistoryDetail(
                orderNo,
                orderType
        ).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取下载pdf地址
    public void getDownloadFileUrl(String orderNo, LifecycleMVPResultCallback<DownloadFileUrlEntity> callback) {
        mAuthApi.getDownloadFileUrl(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //密码校验前置(3252)
    public void preCheckPd(String passwordType,
                           String operatorType,
                           String oldPassword, LifecycleMVPResultCallback<RechargeOrderInfoEntity> callback) {
        mAuthApi.preCheckPd(passwordType,
                operatorType,
                Sha256Util.sha256WithSalt(oldPassword)).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IVR Call(3253)
    public void sendIvrCall(String outActionId,
                            String amount, LifecycleMVPResultCallback<ResultEntity> callback) {
        mAuthApi.sendIvrCall(outActionId,
                amount).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IVR呼叫结果查询 （3255）
    public void getIvrResult(String outActionId, LifecycleMVPResultCallback<ResultEntity> callback) {
        mAuthApi.getIvrResult(outActionId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //激活收款人（3256）
    public void activateIvrBeneficary(String payeeInfoId, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.activateIvrBeneficary(payeeInfoId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除交易记录
    public void deleteTransferHistory(String orderNo, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.deleteTransferHistory(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除交易记录
    public void deleteTransferHistory(String orderNo, ResultCallback<Void> callback) {
        mAuthApi.deleteTransferHistory(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //收银台唤起
    public void checkOut(String orderNo, String orderInfo, LifecycleMVPResultCallback<CheckOutEntity> callback) {
        mAuthApi.checkOut(orderNo, orderInfo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //同步多语言
    public void checkLanguage(ResultCallback<Void> callback) {
        mAuthApi.checkLanguage().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询订单筛选条件列表
    public void getFilterList(ResultCallback<FilterListEntity> callback) {
        mAuthApi.getFilterList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //确认支付
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType, LifecycleMVPResultCallback<TransferEntity> callback) {
        mAuthApi.confirmPay(orderNo, paymentMethodNo, orderType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //手机联系人列表
    public void getTransferContactList(List<RequestContactEntity> phoneList, LifecycleMVPResultCallback<TransferContactListEntity> callback) {
        mAuthApi.getTransferContact(phoneList).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //最近联系人列表
    public void getTransferRecentContactList(LifecycleMVPResultCallback<TransferContactListEntity> callback) {
        mAuthApi.getTransferRecentContact().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //转账  联系人转账和请求转账
    public void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark,
                                  String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName, LifecycleMVPResultCallback<TransferEntity> callback) {
        mAuthApi.transferToContact(payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, TextUtils.isEmpty(payerName) ? ("+" + callingCode + " " + payerNum) : payerName).enqueue(new ResponseCallbackWrapper<>(callback));
    }
    public void chatTransferToContact(RequestTransferEntity requestTransferEntity, LifecycleMVPResultCallback<TransferEntity> callback) {
        mAuthApi.chatTransferToContact(requestTransferEntity.chatId,requestTransferEntity.payerUserId, requestTransferEntity.callingCode, requestTransferEntity.payerNum, requestTransferEntity.payeeUserId, requestTransferEntity.payeeNum, requestTransferEntity.payeeAmount, requestTransferEntity.payeeCurrencyCode, requestTransferEntity.remark,
                requestTransferEntity.sceneType, requestTransferEntity.transTimeType, requestTransferEntity.payeeNumberType, requestTransferEntity.transferPurpose, requestTransferEntity.transFees, requestTransferEntity.vat, TextUtils.isEmpty(requestTransferEntity.payerName) ? ("+" + requestTransferEntity.callingCode + " " + requestTransferEntity.payerNum) : requestTransferEntity.payerName).enqueue(new ResponseCallbackWrapper<>(callback));
    }
    public void chatTransferToContact(String chatId,String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark,
                                  String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName, LifecycleMVPResultCallback<TransferEntity> callback) {
        mAuthApi.chatTransferToContact(chatId,payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, TextUtils.isEmpty(payerName) ? ("+" + callingCode + " " + payerNum) : payerName).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //校验支付密码
    public void checkTransferPwd(String pdType, String sceneType, @Nullable String oldPassword, String orderNo, ResultCallback<TransferPwdTimeEntity> callback) {
        mAuthApi.checkTransferPwd(pdType, sceneType, Sha256Util.sha256WithSalt(oldPassword), orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //转账确认支付
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                                String exchangeRate, String transCurrencyCode, String transFees, String vat, LifecycleMVPResultCallback<TransferEntity> callback) {
        mAuthApi.transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账单拆分接口
    public void splitBill(String oldOrderNo,
                          String receivableAmount,
                          String currencyCode,
                          String orderRemark,
                          List<SplitBillPayerEntity> receiverInfoList, LifecycleMVPResultCallback<TransferEntity> callback) {
        mAuthApi.splitBill(oldOrderNo, receivableAmount, currencyCode, orderRemark, receiverInfoList).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //联系人是否Invite
    public void contactInvite(List<KycContactEntity> contactInviteDtos, LifecycleMVPResultCallback<ContactInviteEntity> callback) {

        mAuthApi.contactInvite(contactInviteDtos).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //ReceiveSplitBill支付详情
    public void splitBillPayDetail(String orderNo, LifecycleMVPResultCallback<RequestCenterEntity> callback) {

        mAuthApi.splitBillPayDetail(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账单拆分列表接口
    public void getSplitDetailList(String newOrderNo, LifecycleMVPResultCallback<SplitPeopleListEntity> callback) {
        mAuthApi.getSplitDetailList(newOrderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //下单支付
    public void transferPay(String type, String orderAmount, String orderCurrencyCode, String cvv,
                            String protocolNo, String transFees, String vat, String shopperResultUrl, ResultCallback<TransferPayEntity> callback) {
        mAuthApi.transferPay(type, orderAmount, orderCurrencyCode, cvv, protocolNo, transFees, vat, shopperResultUrl).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //Google充值支付
    public void googlePay(String type, String orderAmount, String orderCurrencyCode, TokenDataEntity tokenData, String transFees, String vat, LifecycleMVPResultCallback<CardNotifyEntity> callback) {
        mAuthApi.googlePay(type, orderAmount, orderCurrencyCode, tokenData, transFees, vat).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //绑新卡充值
    public void addMoneyCard(String custName, String cardNo, String expire, String cvv, String type, String orderAmount,
                             String orderCurrencyCode, String transFees, String vat, String shopperResultUrl, String creditType, LifecycleMVPResultCallback<CardBind3DSEntity> callback) {
        mAuthApi.bindCardAddMoney(custName, cardNo, expire, cvv, type, orderAmount,
                orderCurrencyCode, transFees, vat, shopperResultUrl, creditType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //3ds验证结果查询
    public void request3ds(String sessionId, ResultCallback<CardNotifyEntity> callback) {
        mAuthApi.request3ds(sessionId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //3ds验证结果查询
    public void request3ds(String sessionId, String orderNo, ResultCallback<CardNotifyEntity> callback) {
        mAuthApi.request3ds(sessionId, orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //推荐码和奖励活动详情页(3250)
    public void getInviteDetail(ResultCallback<InviteEntity> callback) {
        mAuthApi.getInviteDetail().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //首页sendReceive列表(3267)
    public void getSendReceive(ResultCallback<List<SendReceiveEntity>> callback) {
        mAuthApi.getSendReceive().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //邀请码描述查询
    public void requestRefCodeDesc(String queryType, ResultCallback<ReferralCodeDescEntity> callback) {
        mAuthApi.requestReferralCodeDesc(queryType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    // 查询代金券品牌列表
    public void requestVoucherBrand(LifecycleMVPResultCallback<EVoucherEntity> callback) {
        mAuthApi.requestVoucherBrand().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    // 查询代金券品牌列表
    public void requestVoucherBrand(String sceneType, LifecycleMVPResultCallback<EVoucherEntity> callback) {
        mAuthApi.requestVoucherBrand(sceneType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    /**
     * 查询代金券详情 (3084 )
     */
    public void getVoucherDatail(String orderNo, LifecycleMVPResultCallback<EVoucherDetailEntity> callback) {
        mAuthApi.getVoucherDatail(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    /**
     * 代金券购买下单接口(3085 )
     */
    public void eVoucherOrder(String voucherCode, String voucherAmount, LifecycleMVPResultCallback<EVoucherOrderInfoEntity> callback) {
        mAuthApi.eVoucherOrder(voucherCode, voucherAmount).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    // 查询代金券支持的国家列表
    public void requestVouSupCountry(String providerCode, String brandCode, LifecycleMVPResultCallback<VouSupCountryEntity> callback) {
        mAuthApi.requestVouSupCountry(providerCode, brandCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    // 查询代金券类型和代金券信息
    public void requestVoucherTypeAndInfo(String providerCode, String brandCode, String countryCode, String sceneType, LifecycleMVPResultCallback<VouTypeAndInfoEntity> callback) {
        mAuthApi.requestVoucherTypeAndInfo(providerCode, brandCode, countryCode, sceneType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    // 查询用户是否在该品牌下是否已经下过订单
    public void checkOrderExist(String providerCode, String brandCode, String sceneType, LifecycleMVPResultCallback<OrderExistEntity> callback) {
        mAuthApi.checkOrderExist(providerCode, brandCode, sceneType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //邀请码查询
    public void requestRefCode(LifecycleMVPResultCallback<ReferralCodeEntity> callback) {
        mAuthApi.requestReferralCode().enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //查询首页营销活动列表
    public void requestAllMarket(LifecycleMVPResultCallback<AllMarketInfoEntity> callback) {
        mAuthApi.requestAllMarket().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询首页营销活动详情
    public void requestMarketDetails(String activityNo, ResultCallback<MarketDetailsEntity> callback) {
        mAuthApi.requestMarketDetails(activityNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询城市列表
    public void requestCityList(String statesId, LifecycleMVPResultCallback<CityEntity> callback) {
        mAuthApi.requestCityList(statesId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询地区详情
    public void requestRegionList(String citiesId, LifecycleMVPResultCallback<RegionEntity> callback) {
        mAuthApi.requestRegionList(citiesId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询Imr国家列表
    public void imrCountryList(String countryType, String countryCode, LifecycleMVPResultCallback<ImrCountryListEntity> callback) {
        mAuthApi.imrCountryList(countryType, countryCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR查询ID或Address标志（3103）
    public void imrGetIdOrAddress(String receiptMethod, String countryCode, LifecycleMVPResultCallback<ImrBankIdAddressEntity> callback) {
        mAuthApi.imrGetIdOrAddress(receiptMethod, countryCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR汇率查询（3100）
    public void imrExchangeRate(String payeeInfoId, String sourceAmount,
                                String destinationAmount, String channelCode, LifecycleMVPResultCallback<ImrExchangeRateEntity> callback) {
        mAuthApi.imrExchangeRate(payeeInfoId, sourceAmount, destinationAmount, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR校验（3102）
    public void imrCheck(String orderNo, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.imrCheck(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void updateClickTimeService(String messageId, ResultCallback<Void> callback) {
        mAuthApi.updateClickTimeService(messageId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void updateAddress(String iso2CountryCode,
                              String iso3CountryCode,
                              String countryName,
                              double longitude,
                              double latitude, ResultCallback<Void> callback) {
        mAuthApi.updateAddress(iso2CountryCode,
                iso3CountryCode,
                countryName,
                longitude, latitude).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void updateClickTimeService(String messageId) {
        updateClickTimeService(messageId, new ResultCallback<Void>() {
            @Override
            public void onResult(Void response) {

            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {

            }
        });
    }

    //IMR下单（3101）
    public void imrCheckoutOrder(String orderAmount, String orderCurrency, String payeeId, String purpose,
                                 String receiptMethod, String includeFee, String purposeId, String purposeDesc,
                                 String channelCode,
                                 String transFer,
                                 String destinationAmount,
                                 String destinationCurrency,
                                 String payAmount,
                                 String exchangeRate, LifecycleMVPResultCallback<ImrOrderInfoEntity> callback) {
        mAuthApi.imrCheckoutOrder(orderAmount, orderCurrency, payeeId, purpose,
                receiptMethod, includeFee, purposeId, purposeDesc,
                channelCode, transFer, destinationAmount,
                destinationCurrency, payAmount, exchangeRate).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR 查询银行/代理类型
    public void imrBankAgentNameList(String countryCode, String receiptMethod, String channelCode, LifecycleMVPResultCallback<ImrBankAgentListEntity> callback) {
        mAuthApi.imrBankAgentNameList(countryCode, receiptMethod, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR 查询银行/代理分支类型(3099)
    public void imrBankAgentBranchList(String countryCode, String receiptMethod, String parentNodeCode, LifecycleMVPResultCallback<ImrBankAgentListEntity> callback) {
        mAuthApi.imrBankAgentBranchList(countryCode, receiptMethod, parentNodeCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR添加收款人接口
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName,
                                  String nickName, String relationshipCode, String callingCode, String phone,
                                  String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace,
                                  String birthDate, String sex, String cityName, String districtName,
                                  String poBox, String buildingNo, String street, String idNo, String idExpiry,
                                  String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                                  String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,
                                  String channelPayeeId, String channelCode, String branchId,
                                  LifecycleMVPResultCallback<ImrAddBeneResultEntity> callback) {
        mAuthApi.imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
                cityId, currencyCode, channelPayeeId, channelCode, branchId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询imr收款人列表
    public void getImrBeneficiaryList(LifecycleMVPResultCallback<ImrBeneficiaryListEntity> callback) {
        mAuthApi.getImrBeneficiaryList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询imr收款人详情
    public void getImrBeneficiaryDetail(String payeeInfoId, LifecycleMVPResultCallback<ImrBeneficiaryDetails> callback) {
        mAuthApi.getImrBeneficiaryDetail(payeeInfoId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //修改imr收款人详情信息
    public void saveImrBeneficiaryInfo(String payeeInfoId, String transferDestinationCountryCode,
                                       String receiptMethod, String receiptOrgCode,
                                       String receiptOrgBranchCode, String payeeFullName, String nickName,
                                       String relationshipCode, String callingCode, String phone,
                                       String payeeInfoCountryCode, String birthPlace, String birthDate,
                                       String sex, String cityName, String districtName, String poBox,
                                       String buildingNo, String street, String idNo, String idExpiry,
                                       String bankAccountType, String ibanNo, String bankAccountNo,
                                       String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,
                                       String channelPayeeId, String channelCode, String branchId,
                                       LifecycleMVPResultCallback<ResponseEntity> callback) {
        mAuthApi.editImrBeneficiaryInfo(payeeInfoId, transferDestinationCountryCode, receiptMethod, receiptOrgCode, receiptOrgBranchCode,
                payeeFullName, nickName, relationshipCode, callingCode, phone, payeeInfoCountryCode,
                birthPlace, birthDate, sex, cityName, districtName, poBox, buildingNo, street,
                idNo, idExpiry, bankAccountType, ibanNo, bankAccountNo, receiptOrgName, receiptOrgBranchName,
                cityId, currencyCode, channelPayeeId, channelCode, branchId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除某个imr收款人
    public void removeImrBeneficiary(String payeeInfoId, LifecycleMVPResultCallback<ResponseEntity> callback) {
        mAuthApi.removeImrBeneficiary(payeeInfoId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR查询汇款目的(3123)
    public void getImrPurposeList(String countryCode, String channelCode, LifecycleMVPResultCallback<ImrPurposeListEntity> callback) {
        mAuthApi.getImrPurposeList(countryCode, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR查询城市列表(3121)
    public void getImrCityList(String countryCode, String channelCode, LifecycleMVPResultCallback<ImrCityListEntity> callback) {
        mAuthApi.getImrCityList(countryCode, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR查询城市列表(3122)
    public void getImrPayerDetail(String countryCode, String cityId,
                                  String payerCurrencyCode,
                                  String payeeCurrencyCode, String channelCode, LifecycleMVPResultCallback<ImrPayerDetailEntity> callback) {
        mAuthApi.getImrPayerDetail(countryCode, cityId, payerCurrencyCode, payeeCurrencyCode, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR完善信息(3124)
    public void sendImrCompliance(List<ImrComplianceEntity> list, LifecycleMVPResultCallback<ImrOrderInfoEntity> callback) {
        mAuthApi.sendImrCompliance(list).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //IMR完善信息(3124)
    public void trustDevice(LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.trustDevice().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //用户行为风控（3245）
    public void getOtpType(String category, String categoryId, List<String> additionalData, LifecycleMVPResultCallback<RechargeOrderInfoEntity> callback) {
        mAuthApi.getOtpType(category, categoryId, additionalData).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询Faq（3131）
    public void getHelpContentList(String content, LifecycleMVPResultCallback<HelpListEntity> callback) {
        mAuthApi.getHelpContentList(content).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //Faq问题反馈(3132)
    public void submitHelpQuestion(String id, String comment, String thumb, LifecycleMVPResultCallback<Void> callback) {
        mAuthApi.submitHelpQuestion(id, comment, thumb).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //E-invoice查看按钮（3248）
    public void getTransferPdfUrl(String orderNo, ResultCallback<TransferPdfEntity> callback) {
        mAuthApi.getTransferPdfUrl(orderNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取商务列表
    public void getBusinessParamList(LifecycleMVPResultCallback<BusinessParamEntity> callback) {
        mAuthApi.getBusinessParamList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //用户未开启生物认证(1072)
    public void notUseFinger(String bizType, ResultCallback<Void> callback) {
        mAuthApi.notUseFinger(bizType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //长时间未操作点击Yes(3133)
    public void longTimeNoUse(ResultCallback<Void> callback) {
        mAuthApi.longTimeNoUse().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取某个商务列表
    // 资金来源:SOURCE_OF_FUNDS,职业:PROFESSION,公司类型:COMPANY_TYPE,年收入:SALARY_RANGE,从业的行业:EMPLOYMENT_SECTOR
    public void getBusinessParam(String businessType, String parentBussinessCode, ResultCallback<BusinessParamEntity> callback) {
        mAuthApi.getBusinessParam(businessType, parentBussinessCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void getTransferPurpose(String businessType, ResultCallback<BusinessParamEntity> callback) {
        mAuthApi.getTransferPurpose(businessType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //用手机号登录
    public void loginByType(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude, final ResultCallback<UserInfoEntity> callback) {
        login(callingCode, phone, password, verifyCode, type, longitude, latitude, callback);
    }

    //登录,本质上无论任何登录方式，都会转成一个loginID
    private void login(String callingCode, String loginID, String password, @Nullable String verifyCode, @Nullable String type, double longitude, double latitude, final ResultCallback<UserInfoEntity> callback) {
        //密码要进行sha256加密
        mAuthApi.login(callingCode, loginID, Sha256Util.sha256WithSalt(password), verifyCode, type, longitude, latitude)
                .enqueue(new ResponseCallbackWrapper<>(new ResultCallback<UserInfoEntity>() {
                    @Override
                    public void onResult(final UserInfoEntity userInfo) {
                        if (userInfo == null) return;
                        SpUtils.getInstance().setReqCounts(userInfo.requestCounts);
                        mUserManager.getUserDisableAppFuncCodeList(callingCode, loginID, new ResultCallback<UserUnableUseEntity>() {
                            @Override
                            public void onResult(UserUnableUseEntity userUnableUseEntity) {
                                SpUtils.getInstance().setUserUnableUseEntity(userUnableUseEntity);
                            }

                            @Override
                            public void onFailure(String errorCode, String errorMsg) {

                            }
                        });
                        SpUtils.getInstance().setLoginUser(userInfo);
                        if (userInfo.riskControlInfo != null) {
                            CallbackUtil.callResult(userInfo, callback);
                        } else {
                            //尝试初始化，初始化成功了算是真正的登录
                            tryInit(userInfo.getSessionId(), userInfo.getUserId(), userInfo, new ResultCallback<Void>() {
                                @Override
                                public void onResult(Void result) {
                                    CallbackUtil.callResult(userInfo, callback);
                                }

                                @Override
                                public void onFailure(String code, String error) {
                                    //失败就删除本地信息
                                    clearLastSessionInfo();
                                    CallbackUtil.callFailure(code, error, callback);
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(String code, String error) {
                        CallbackUtil.callFailure(code, error, callback);
                    }
                }));
    }

    //设备登录，就是如果以前登录过了，那么下次就不需要登录了，直接拿本地保存的token进行登录，如果没有网会登录失败。
    public void deviceLogin(final ResultCallback<UserInfoEntity> callback) {
        mSessionID = getLastSessionID();
        mUserID = getLastUserID();
        if (TextUtils.isEmpty(mSessionID) || TextUtils.isEmpty(mUserID)) {
            clearLastSessionInfo();
            if (callback != null) {
                CallbackUtil.callFailure(null, null, callback);
            }
            return;
        }
        //本质上其实就是用本地保存的sessionId和userID去调一次接口(获取用户信息)来判断sessionID是否过期
        mAuthApi.getUserInfo().enqueue(new ResponseCallbackWrapper<>(new ResultCallback<UserInfoEntity>() {
            @Override
            public void onResult(final UserInfoEntity userInfo) {
                userInfo.setSessionId(mSessionID);
                userInfo.setUserId(mUserID);
                //和登录一样，尝试初始化
                tryInit(mSessionID, mUserID, userInfo, new ResultCallback<Void>() {
                    @Override
                    public void onResult(Void result) {
                        //如果是采用设备登录的情况，用一个变量表示，这个变量以后可能会有需要用到
                        isDeviceLogin = true;
                        CallbackUtil.callResult(userInfo, callback);
                    }

                    @Override
                    public void onFailure(String code, String error) {
                        //失败就删除本地信息
                        clearLastSessionInfo();
                        CallbackUtil.callFailure(code, error, callback);
                    }
                });
            }

            @Override
            public void onFailure(String code, String error) {
                clearLastSessionInfo();
                CallbackUtil.callFailure(code, error, callback);
            }
        }));
    }

    //判断ID是否已经注册过的，这个ID是第三方授权的ID，比如微信的openID
    public void isThirdPartyAlreadyBound(String openID, final ResultCallback<AuthorizationVerifyEntity> callback) {
        mAuthApi.authorizationVerify(openID).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //登录也要调用结果，不过登出一般不需要回调，回调建议设置为null
    public synchronized void logout(final ResultCallback<Void> callback) {
        mAuthApi.logout().enqueue(new ResponseCallbackWrapper<>(new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {
                //登出会删除信息
                clearLastSessionInfo();
                CallbackUtil.callResult(result, callback);
            }

            @Override
            public void onFailure(String code, String error) {
                //登出会删除信息
                //而且就是接口调用失败，也要回调登录成功，总不能说没网就不能登出
                clearLastSessionInfo();
                CallbackUtil.callResult(null, callback);
            }
        }));
    }

    public boolean isDeviceLogin() {
        return isDeviceLogin;
    }

    public synchronized void setSessionID(String sessionID) {
        mSessionID = sessionID;
    }

    public String getSessionID() {
        if (TextUtils.isEmpty(mSessionID)) {
            return getLastSessionID();
        }
        return mSessionID;
    }

    public String getUserID() {
        if (mUserID == null) {
            return getLastUserID();
        }
        return mUserID;
    }

    public OnLoginExpiredListener getOnLoginExpiredListener() {
        return mOnLoginExpiredListener;
    }

    public void setOnLoginExpiredListener(OnLoginExpiredListener onLoginExpiredListener) {
        mOnLoginExpiredListener = new OnLoginExpiredListenerWrapper(onLoginExpiredListener);
    }

    public Context getAppContext() {
        return mAppContext;
    }

    @NonNull
    public CardManager getCardManager() {
        return mCardManager;
    }

    @NonNull
    public UserManager getUserManager() {
        return mUserManager;
    }

    @NonNull
    public TransferManager getTransferManager() {
        return mTransferManager;
    }

    @NonNull
    public StorageHelper getStorageHelper() {
        return mStorageHelper;
    }

    @NonNull
    public BiometricManager getBiometricManager() {
        return mBiometricManager;
    }
//
//    @NonNull
//    public ConfigurationManager getConfigurationManager() {
//        return mConfigurationManager;
//    }

    //尝试初始化，其实这里说是初始化，实际上也是调用接口，提前调用一些马上要用到的接口来获取数据
    //这里调用了获取币种配置信息接口，获取余额接口，获取卡列表接口，提前把这些数据获取，后续就可以直接拿这里数据了，而无需再次调用接口拿
    //因为调用接口拿到数据之后会缓存到本地的
    private void tryInit(final String sessionID, final String userID, final UserInfoEntity userInfo, @NonNull final ResultCallback<Void> callback) {
        if (TextUtils.isEmpty(sessionID) || TextUtils.isEmpty(userID) || userInfo == null) {
            LogUtil.e("userID || session ==null");
            CallbackUtil.callResult(null, callback);
//            CallbackUtil.callFailure("", ResourcesHelper.getString(R.string.Core_ServerError), callback);
            return;
        }
        mStorageHelper.initUserPreferences(mAppContext, mUserID);
        UserInfoManager.getInstance().saveLoginStatus();
        mUserID = userID;
        mSessionID = sessionID;
        //将数据保存到本地配置文件
        if (!mStorageHelper.putToConfigurationPreferences(STORAGE_KEY_SESSION_ID, mSessionID)) {
            LogUtil.w("saveSessionIDToLocal fail");
        }
        if (!mStorageHelper.putToConfigurationPreferences(STORAGE_KEY_USER_ID, mUserID)) {
            LogUtil.w("saveUserIDToLocal fail");
        }
        CallbackUtil.callResult(null, callback);
    }

    //一下三个Key对应3个本地保存的数据对应的key
    private static final String STORAGE_KEY_SESSION_ID = "SessionID";
    private static final String STORAGE_KEY_USER_ID = "UserID";
    private static final String STORAGE_KEY_DEVICE_ID = "DeviceID";
    private static final String STORAGE_KEY_DEVICE_MODEL = "DeviceModel";
    private static final String STORAGE_KEY_SERVER_APP_VERSION = "ServerVersion";

    //获取设备ID
    public String getDeviceID() {
        //先从本地拿
        if (TextUtils.isEmpty(mDeviceID)) {
            mDeviceID = mStorageHelper.getFromConfigurationPreferences(STORAGE_KEY_DEVICE_ID);
        }
        //如果本地没有就初始化一个，目前设备ID的格式是我自己定的，uuid加上一些手机型号版本组成字符串
        if (TextUtils.isEmpty(mDeviceID)) {
            mDeviceID = String.format(Locale.getDefault(), "%s(%s).%s", Build.BRAND, Build.MODEL, UUID.randomUUID().toString());
            //设备ID如果出现空格就替换成_,一些手机型号信息可能带空格
            mDeviceID = mDeviceID.replaceAll(" ", "_");
            //保存ID到本地
            if (!mStorageHelper.putToConfigurationPreferences(STORAGE_KEY_DEVICE_ID, mDeviceID)) {
                LogUtil.w("saveDeviceIDToLocal fail");
            }
        }
        return mDeviceID;
    }

    //获取设备型号
    public String getDeviceModel() {
        //先从本地拿
        if (TextUtils.isEmpty(mDeviceModel)) {
            mDeviceModel = mStorageHelper.getFromConfigurationPreferences(STORAGE_KEY_DEVICE_MODEL);
        }
        //如果本地没有就初始化一个，目前设备ID的格式是我自己定的，uuid加上一些手机型号版本组成字符串
        if (TextUtils.isEmpty(mDeviceModel)) {
            mDeviceModel = String.format(Locale.getDefault(), "%s(%s)", Build.BRAND, Build.MODEL);
            //设备ID如果出现空格就替换成_,一些手机型号信息可能带空格
            mDeviceModel = mDeviceModel.replaceAll(" ", "_");
            //保存ID到本地
            if (!mStorageHelper.putToConfigurationPreferences(STORAGE_KEY_DEVICE_MODEL, mDeviceModel)) {
                LogUtil.w("saveDeviceIDToLocal fail");
            }
        }
        return mDeviceModel;
    }

    public boolean isLogged() {
        return !TextUtils.isEmpty(getLastSessionID());
    }

    //获取上一次登录的sessionID
    public String getLastSessionID() {
        return mStorageHelper.getFromConfigurationPreferences(STORAGE_KEY_SESSION_ID);
    }

    //获取上一次登录的userID
    public String getLastUserID() {
        return mStorageHelper.getFromConfigurationPreferences(STORAGE_KEY_USER_ID);
    }

    //设置上一次登录的userID
    public void setLastUserID(String mUserID) {
        mStorageHelper.putToConfigurationPreferences(STORAGE_KEY_USER_ID, mUserID);
    }

    //删除上一次登录的信息，并重置所有状态
    public void clearLastSessionInfo() {
        mStorageHelper.putToConfigurationPreferences(STORAGE_KEY_SESSION_ID, "");
        UserInfoManager.getInstance().saveLogoutStatus();
        reset();
    }

    private void reset() {
        mUserManager.reset();
        mBiometricManager.reset();
        mSessionID = null;
        mUserID = null;
        isDeviceLogin = false;
    }

    //如果调用接口发现sessionID过去的时候会回调这里
    private class OnLoginExpiredListenerWrapper implements OnLoginExpiredListener {
        private OnLoginExpiredListener mListener;

        OnLoginExpiredListenerWrapper(OnLoginExpiredListener listener) {
            mListener = listener;
        }

        @Override
        public void onExpired(String code, String message) {
            //首先清楚当下的数据
            clearLastSessionInfo();
            //然后回调lister，如果用户设置了Listener的话，一般都会设置，比如用户信息过期了就回到登录页面，WalletApp类用到
            if (mListener != null) {
                mListener.onExpired(code, message);
            }
        }
    }

    //格式化手机号格式，将区号和手机号合成一个字符串，这个格式不能动，IOS和android是统一的
    public static String formatTelephone(String internationalCode, String telephone) {
        if (TextUtils.isEmpty(internationalCode)) {
            return telephone;
        }
        return internationalCode + "-" + telephone;
    }

    public interface OnLoginExpiredListener {
        void onExpired(String code, String message);
    }

}
