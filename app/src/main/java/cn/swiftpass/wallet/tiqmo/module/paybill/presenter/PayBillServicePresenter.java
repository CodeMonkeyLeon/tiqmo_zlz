package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.BillServiceSearchResultEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillServicePresenter implements PayBillContract.BillServicePresenter {


    PayBillContract.BillServiceView mBillServiceView;


    @Override
    public void getBillServiceIOList(String billerId, String sku, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillServiceIOList(billerId, sku, channelCode, new LifecycleMVPResultCallback<PayBillIOListEntity>(mBillServiceView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                if (mBillServiceView != null) {
                    mBillServiceView.getBillServiceIOListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBillServiceView != null) {
                    mBillServiceView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void searchBiller(String searchBillerNameOrCodeOfLocalService, String countryCode,  String channelCode) {

        AppClient.getInstance().getTransferManager().searchBillSearch(searchBillerNameOrCodeOfLocalService, countryCode,  channelCode, new LifecycleMVPResultCallback<BillServiceSearchResultEntity>(mBillServiceView, true) {
            @Override
            protected void onSuccess(BillServiceSearchResultEntity result) {
                if (mBillServiceView != null) {
                    mBillServiceView.searchBillerSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBillServiceView != null) {
                    mBillServiceView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }


    @Override
    public void attachView(PayBillContract.BillServiceView billServiceView) {
        mBillServiceView = billServiceView;
    }

    @Override
    public void detachView() {
        mBillServiceView = null;
    }


}
