package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrSetupConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrSetupInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrSetupContract;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrSetupPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;

public class ImrSetupActivity extends BaseCompatActivity<ImrSetupContract.IImrSetupPresenter> implements ImrSetupContract.ImrSetupView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.fl_source_of_fund)
    FrameLayout flSourceOfFund;
    @BindView(R.id.fl_profession)
    FrameLayout flProfession;
    @BindView(R.id.fl_company_type)
    FrameLayout flCompanyType;
    @BindView(R.id.fl_salary_range)
    FrameLayout flSalaryRange;
    @BindView(R.id.tv_source_of_fund)
    TextView tvSourceOfFund;
    @BindView(R.id.tv_profession)
    TextView tvProfession;
    @BindView(R.id.tv_company_type)
    TextView tvCompanyType;
    @BindView(R.id.et_company_name)
    CustomizeEditText etCompanyName;
    @BindView(R.id.tv_salary_range)
    TextView tvSalaryRange;
    @BindView(R.id.tv_source_of_fund_subtitle)
    TextView tvSourceOfFundSubtitle;
    @BindView(R.id.tv_profession_subtitle)
    TextView tvProfessionSubtitle;
    @BindView(R.id.tv_company_type_subtitle)
    TextView tvCompanyTypeSubtitle;
    @BindView(R.id.tv_salary_range_subtitle)
    TextView tvSalaryRangeSubtitle;
    @BindView(R.id.tv_save)
    TextView tvSave;

    ImrSetupInfoEntity infoEntity;
    UserInfoEntity userInfo;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_imr_setup;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        initView();

        initData();
    }

    private void initData() {
        infoEntity = new ImrSetupInfoEntity();
    }

    private void initView() {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        tvTitle.setText(R.string.IMR_3);
        etCompanyName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(64)});

        etCompanyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    infoEntity.companyName = etCompanyName.getText().toString().trim();
                    checkAllMsg();
                }
            }
        });
        etCompanyName.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                infoEntity.companyName = s.toString().trim();
                checkAllMsg();
            }
        });
        mPresenter.getUserInfo();
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkAllMsg();
                    }
                }
            });
        } else {
            content.setText(value);
            checkAllMsg();

        }
    }

    @Override
    public void showSourceOfFund(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_6));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                infoEntity.sourceOfFund = data.get(position).businessParamValue;
                infoEntity.sourceOfFundCode = data.get(position).businessParamKey;
                refreshItemView(data.get(position).businessParamValue, tvSourceOfFund, tvSourceOfFundSubtitle, false);

            }
        });
        dialog.showNow(getSupportFragmentManager(), "showSourceOfFund");
    }

    private void checkAllMsg() {
        tvSave.setEnabled(infoEntity.isFillAllInfo());
    }

    @Override
    public void showProfession(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_7));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                infoEntity.profession = data.get(position).businessParamValue;
                infoEntity.professionCode = data.get(position).businessParamKey;
                refreshItemView(data.get(position).businessParamValue, tvProfession,
                        tvProfessionSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showProfession");
    }

    @Override
    public void showCompanyType(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_8));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                infoEntity.companyType = data.get(position).businessParamValue;
                infoEntity.businessTypeCode = data.get(position).businessParamKey;
                refreshItemView(data.get(position).businessParamValue, tvCompanyType,
                        tvCompanyTypeSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showCompanyType");
    }

    @Override
    public void showSalaryRange(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.newhome_36));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                infoEntity.salaryRange = data.get(position).businessParamValue;
                infoEntity.salaryRangeCode = data.get(position).businessParamKey;
                refreshItemView(data.get(position).businessParamValue, tvSalaryRange,
                        tvSalaryRangeSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showSalaryRange");
    }

    @Override
    public void showIMRInfoSaveSuccess() {
        ImrSetupConfirmDialog dia = new ImrSetupConfirmDialog();
        dia.setOnConfirmClickListener(new ImrSetupConfirmDialog.OnDialogClickListener() {
            @Override
            public void onConfirmClick() {
                ActivitySkipUtil.startAnotherActivity(mContext, ImrMainActivity.class);
                if (!ImrSetupActivity.this.isFinishing()) {
                    finish();
                }
            }

            @Override
            public void onCancel() {
                ActivitySkipUtil.startAnotherActivity(mContext, ImrMainActivity.class);
                if (!ImrSetupActivity.this.isFinishing()) {
                    finish();
                }
            }
        });
        dia.showNow(getSupportFragmentManager(), "ImrSetupConfirm");
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getUserInfoSuccess(UserInfoEntity userInfoEntity) {
        userInfo = userInfoEntity;
        if (!TextUtils.isEmpty(userInfo.sourceOfFundCode)) {
            refreshItemView(userInfo.sourceOfFundDesc, tvSourceOfFund, tvSourceOfFundSubtitle, false);
            infoEntity.sourceOfFundCode = userInfo.sourceOfFundCode;
            infoEntity.sourceOfFund = userInfo.sourceOfFundDesc;
        }
        if (!TextUtils.isEmpty(userInfo.professionCode)) {
            refreshItemView(userInfo.professionDesc, tvProfession, tvProfessionSubtitle, false);
            infoEntity.professionCode = userInfo.professionCode;
            infoEntity.profession = userInfo.professionDesc;
        }
        if (!TextUtils.isEmpty(userInfo.businessTypeCode)) {
            refreshItemView(userInfo.businessTypeDesc, tvCompanyType, tvCompanyTypeSubtitle, false);
            infoEntity.businessTypeCode = userInfo.businessTypeCode;
            infoEntity.companyType = userInfo.businessTypeDesc;
        }
        if (!TextUtils.isEmpty(userInfo.companyName)) {
            etCompanyName.setText(userInfo.companyName);
            infoEntity.companyName = userInfo.companyName;
        }
        if (!TextUtils.isEmpty(userInfo.salaryRangeCode)) {
            refreshItemView(userInfo.salaryRangeDesc, tvSalaryRange, tvSalaryRangeSubtitle, false);
            infoEntity.salaryRangeCode = userInfo.salaryRangeCode;
            infoEntity.salaryRange = userInfo.salaryRangeDesc;
        }
        checkAllMsg();
    }

    @Override
    public void updateUserInfoSuccess(UserInfoEntity result) {
        showIMRInfoSaveSuccess();
    }

    @OnClick({R.id.iv_back, R.id.fl_source_of_fund, R.id.fl_profession,
            R.id.fl_company_type, R.id.fl_salary_range, R.id.tv_save})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                if (!ImrSetupActivity.this.isFinishing()) {
                    finish();
                }
                break;
            case R.id.fl_source_of_fund:
                mPresenter.getSourceOfFund();
                break;
            case R.id.fl_profession:
                mPresenter.getProfession();
                break;
            case R.id.fl_company_type:
                mPresenter.getCompanyType();
                break;
            case R.id.fl_salary_range:
                mPresenter.getSalaryRange();
                break;
            case R.id.tv_save:
                if (userInfo == null) {
                    return;
                }
                userInfo.sourceOfFundCode = infoEntity.sourceOfFundCode;
                userInfo.professionCode = infoEntity.professionCode;
                userInfo.businessTypeCode = infoEntity.businessTypeCode;
                userInfo.salaryRangeCode = infoEntity.salaryRangeCode;
                userInfo.companyName = infoEntity.companyName;
                userInfo.imrSetUpFlag = "Y";
                mPresenter.updateUserInfo(userInfo);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {//点击editText控件外部
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    assert v != null;
                    AndroidUtils.hideKeyboard(v);//软键盘工具类
                    if (etCompanyName != null) {
                        etCompanyName.clearFocus();
                    }
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
        }
        return false;
    }

    @Override
    public ImrSetupContract.IImrSetupPresenter getPresenter() {
        return new ImrSetupPresenter();
    }
}
