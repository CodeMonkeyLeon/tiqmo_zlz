package cn.swiftpass.wallet.tiqmo.module.chat.Contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;

public class ChatContract {

    public interface ChatView extends BaseView<ChatContract.ChatPresenter>{
        void contactInviteSuccess(ContactInviteEntity contactInviteEntity);

        void contactInviteFail(String errCode, String errMsg);
    }


    public interface ChatPresenter extends BasePresenter<ChatContract.ChatView>{
        void contactInvite(List<KycContactEntity> contactInviteDtos);
    }

    public interface ChatTransferView extends BaseView<ChatContract.ChatTransferPresenter>{
        void getLimitSuccess(TransferLimitEntity transferLimitEntity);
        void transferToContactSuccess(TransferEntity transferEntity);
        void transferSurePaySuccess(TransferEntity transferEntity);

        void showErrorMsg(String errCode, String errMsg);
    }


    public interface ChatTransferPresenter extends BasePresenter<ChatContract.ChatTransferView>{
        void getLimit(String type);

        void chatTransferToContact(String chatId,String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark,
                               String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName);

        void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                             String exchangeRate, String transCurrencyCode, String transFees, String vat);
    }
}
