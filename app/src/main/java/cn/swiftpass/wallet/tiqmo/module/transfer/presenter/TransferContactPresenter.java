package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class TransferContactPresenter implements TransferContract.TransferContactPresenter {

    private TransferContract.TransferContactView transferContactView;

    @Override
    public void getContactList(List<RequestContactEntity> phoneList) {
        AppClient.getInstance().getTransferContactList(phoneList, new LifecycleMVPResultCallback<TransferContactListEntity>(transferContactView, true) {
            @Override
            protected void onSuccess(TransferContactListEntity result) {
                transferContactView.getContactListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferContactView.getContactListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkContact(String callingCode, String phone, String contactsName, String sceneType) {
        AppClient.getInstance().checkContact(callingCode, phone, contactsName, sceneType, new LifecycleMVPResultCallback<KycContactEntity>(transferContactView, true) {
            @Override
            protected void onSuccess(KycContactEntity result) {
                transferContactView.checkContactSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferContactView.checkContactFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void contactInvite(List<KycContactEntity> contactInviteDtos) {
        for (int i = 0; i < contactInviteDtos.size(); i++) {
            contactInviteDtos.get(i).setCallingCode("+966");
            contactInviteDtos.get(i).setPhone(contactInviteDtos.get(i).getPhone().substring(5));
        }
        AppClient.getInstance().contactInvite(contactInviteDtos, new LifecycleMVPResultCallback<ContactInviteEntity>(transferContactView) {
            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferContactView != null) {
                    transferContactView.contactInviteFail(errorCode,errorMsg);
                }
            }

            @Override
            protected void onSuccess(ContactInviteEntity result) {
                if (transferContactView != null) {

                    transferContactView.contactInviteSuccess(result);
                }
            }
        });
    }

    @Override
    public void attachView(TransferContract.TransferContactView transferContactView) {
        this.transferContactView = transferContactView;
    }

    @Override
    public void detachView() {
        this.transferContactView = null;
    }
}
