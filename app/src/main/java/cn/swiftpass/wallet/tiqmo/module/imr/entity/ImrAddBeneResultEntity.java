package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrAddBeneResultEntity extends BaseEntity {
    //收款方信息id （ 保存成功的时候返回 ）
    public String payeeInfoId;
}
