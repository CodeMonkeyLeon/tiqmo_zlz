package cn.swiftpass.wallet.tiqmo.module.chat.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ChatConfirmPayDialog extends BottomDialog {

    private TextView tvError;
    private RequestCenterEntity requestCenterEntity;

    private String transferPurpose;
    private String transferPurposeDesc;

    public ChatConfirmPayDialog(Context context) {
        super(context);
        initViews(context);
    }

    private void initViews(Context mContext) {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_chat_confirm_pay, null);
        TextView tvConfirm = contentView.findViewById(R.id.tv_confirm);
        TextView tvCancel = contentView.findViewById(R.id.tv_cancel);
        TextView tvMoney = contentView.findViewById(R.id.tv_money);
        TextView tvNote = contentView.findViewById(R.id.tv_note);
        TextView tvName = contentView.findViewById(R.id.tv_name);
        TextView tvPhone = contentView.findViewById(R.id.tv_phone);
        LottieAnimationView ivSend = contentView.findViewById(R.id.iv_send);
        tvError = contentView.findViewById(R.id.tv_error);
        ConstraintLayout con_note = contentView.findViewById(R.id.con_note);
        RoundedImageView ivFromAvatar = contentView.findViewById(R.id.iv_from_avatar);
        RoundedImageView ivToAvatar = contentView.findViewById(R.id.iv_to_avatar);
        ConstraintLayout conPurpose = contentView.findViewById(R.id.con_purpose);
        EditTextWithDel etPurpose = contentView.findViewById(R.id.et_purpose);
        etPurpose.getTlEdit().setHint(mContext.getString(R.string.IMR_new_10));
        etPurpose.setFocusableFalse();
        LocaleUtils.viewRotationY(mContext, ivSend);

        String requestIcon = requestCenterEntity.requestIcon;
        String receiveIcon = requestCenterEntity.receiveIcon;
        String requestPhone = requestCenterEntity.requestPhone;
        String receivePhone = requestCenterEntity.receivePhone;
        String requestUserName = requestCenterEntity.requestUserName;
        String receiveUserName = requestCenterEntity.receiveUserName;
        String requestSex = requestCenterEntity.requestSex;
        String receiveSex = requestCenterEntity.receiveSex;
        String transferAmount = requestCenterEntity.transferAmount;
        String note = requestCenterEntity.transferRemark;
        String currencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferAmount = AndroidUtils.getTransferMoney(transferAmount);
        if (!TextUtils.isEmpty(transferAmount)) {
            tvMoney.setText(Spans.builder()
                    .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                    .build());
        }

        tvName.setText(AndroidUtils.getContactName(requestUserName));
        tvPhone.setText(AndroidUtils.getPhone(requestPhone));
        if (!TextUtils.isEmpty(note)) {
            con_note.setVisibility(View.VISIBLE);
            tvNote.setText(note);
        } else {
            con_note.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(receiveIcon)) {
            Glide.with(mContext).clear(ivFromAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(receiveIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, receiveSex))
                    .into(ivFromAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, receiveSex)).into(ivFromAvatar);
        }

        if (!TextUtils.isEmpty(requestIcon)) {
            Glide.with(mContext).clear(ivToAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(requestIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, requestSex))
                    .into(ivToAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, requestSex)).into(ivToAvatar);
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotifyEntity notifyEntity = new NotifyEntity();
                notifyEntity.msgSubject = "TRANSFER";
                notifyEntity.msgResult = "CONFIRM";
                notifyEntity.orderNo = requestCenterEntity.orderNo;
                List<NotifyEntity> receivers = new ArrayList<>();
                receivers.add(notifyEntity);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        etPurpose.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                AppClient.getInstance().getTransferPurpose("PURPOSE_OF_TRANSFER",new ResultCallback<BusinessParamEntity>(){

                    @Override
                    public void onResult(BusinessParamEntity businessParamEntity) {
                        showProgress(false);

                        if (businessParamEntity != null) {
                            List<SelectInfoEntity> selectInfoEntityList = businessParamEntity.businessParamList;
                            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, mContext.getString(R.string.IMR_new_10), false, true);
                            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    SelectInfoEntity  selectInfoEntity = selectInfoEntityList.get(position);
                                    if (selectInfoEntity != null) {
                                        transferPurpose = selectInfoEntity.businessParamKey;
                                        transferPurposeDesc = selectInfoEntity.businessParamValue;
                                        etPurpose.getEditText().setText(transferPurposeDesc);
                                    }
//                                    checkContinueStatus();
                                }
                            });
                            dialog.showNow(((BaseCompatActivity)mContext).getSupportFragmentManager(), "getNationalitySuccess");
                        }
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                    }
                });
            }
        });

        setContentView(contentView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
