package cn.swiftpass.wallet.tiqmo.sdk.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.BudgetCategoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AppManageEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ConfigEntity extends BaseEntity {
    public String codeLength;
    public String partnerNo;
    public String openId;
    public int splitReceiversLimit;
    //绑卡最大数
    public String bingCardNum = "15";
    public String verifyCodeType;
    public List<CurrencyEntity> currencyCds = new ArrayList<>();
    public List<AgreeEntity> agreementURLs = new ArrayList<>();
    public List<FilterCodeEntity> filterOrderConditions = new ArrayList<>();
    //前端CQ 码刷新的间隔时间(单位S)
    public int cqCodeRefreshGapTime;
    //是否展示货币   去币种时候用  0:代表不需要，其他：代表需要
    public String currencyShow;

    public AppManageEntity appManageInfo;
    //证书压缩包名
    public String certZipName;

    public String countTime;
    public String modifyTime;

    //支出分析列表数据
    public List<BudgetCategoryEntity> spendingAnalysisFilterConditions = new ArrayList<>();

    public String getAgreeUrl(String type){
        int size = agreementURLs.size();
        for(int i=0;i<size;i++){
            AgreeEntity agreeEntity = agreementURLs.get(i);
            if(agreeEntity != null){
                if(type.equals(agreeEntity.type)){
                    return agreeEntity.url;
                }
            }
        }
        return "";
    }
}
