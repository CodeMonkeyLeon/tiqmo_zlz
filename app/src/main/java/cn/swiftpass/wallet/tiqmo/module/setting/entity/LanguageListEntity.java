package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class LanguageListEntity extends BaseEntity {
    /**
     * {\"languageCodes\":[{\"name\":\"Arabic\",\"rTL\":false,\"key\":\"ar\"}
     * ,{\"name\":\"English\",\"rTL\":false,\"key\":\"en_US\"}],\"version\":11}}
     */
    public List<LanguageCodeEntity> languageCodes = new ArrayList<>();
    public String lang;
    //版本号，本地配置
    public String version;

    public List<Locale> getLocaleList(){
        List<Locale> localeList = new ArrayList<>();
        if(languageCodes != null && languageCodes.size()>0){
            int size = languageCodes.size();
            for(int i=0;i<size;i++){
                LanguageCodeEntity languageCodeEntity = languageCodes.get(i);
                if(languageCodeEntity != null) {
                    String key = languageCodeEntity.key;
                    if("en_US".equals(key)) {
                        localeList.add(Locale.ENGLISH);
                    }else{
                        localeList.add(new Locale(key));
                    }
                }
            }
        }
        return localeList;
    }
}
