package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferContactListEntity extends BaseEntity {

    public List<KycContactEntity> phoneList = new ArrayList<>();

    //别人给我转账的列表
    public List<KycContactEntity> payeeContactsList = new ArrayList<>();
    //我给别人转账的列表
    public List<KycContactEntity> payerContactsList = new ArrayList<>();
}
