package cn.swiftpass.wallet.tiqmo.module.chat.Presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.Contract.ChatContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ChatTransferPresenter implements ChatContract.ChatTransferPresenter {

    private ChatContract.ChatTransferView chatTransferView;

    @Override
    public void attachView(ChatContract.ChatTransferView chatTransferView) {
        this.chatTransferView = chatTransferView;
    }

    @Override
    public void detachView() {
        this.chatTransferView = null;
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(chatTransferView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                chatTransferView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                chatTransferView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void chatTransferToContact(String chatId,String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark, String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName) {
        AppClient.getInstance().chatTransferToContact(chatId,payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, payerName, new LifecycleMVPResultCallback<TransferEntity>(chatTransferView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        if (chatTransferView != null) {
                            chatTransferView.transferToContactSuccess(result);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (chatTransferView != null) {
                            chatTransferView.showErrorMsg(errorCode, errorMsg);
                        }
                    }
                });
    }

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(chatTransferView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        if (chatTransferView != null) {

                            chatTransferView.transferSurePaySuccess(result);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (chatTransferView != null) {
                            chatTransferView.showErrorMsg(errorCode, errorMsg);
                        }
                    }
                });
    }
}
