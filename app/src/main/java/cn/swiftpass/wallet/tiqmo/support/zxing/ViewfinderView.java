package cn.swiftpass.wallet.tiqmo.support.zxing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.google.zxing.ResultPoint;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.zxing.Camera.CameraManager;

/**
 * This view is overlaid on top of the camera preview. It adds the viewfinder rectangle and partial
 * transparency outside it, as well as the laser scanner animation and result points.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class ViewfinderView extends View {
    private static final int[] SCANNER_ALPHA = {0, 64, 128, 192, 255, 192, 128, 64};
    private static final int CURRENT_POINT_OPACITY = 0xA0;
    private static final int MAX_RESULT_POINTS = 20;
    private static final int POINT_SIZE = 6;
    private CameraManager cameraManager;
    private Bitmap resultBitmap;
    private final int maskColor;
    private final int resultColor;
    private final int resultPointColor;
    private final int frameColor;
    private List<ResultPoint> possibleResultPoints;
    private List<ResultPoint> lastPossibleResultPoints;

    /*************************onDraw绘制相关*************************************/
    //刷新界面的时间
    private static final long ANIMATION_DELAY = 20L;
    //四个绿色边角对应的长度
    private int screenRate;
    //四个绿色边角对应的宽度
    private static final int CORNER_WIDTH = 10;
    private static final int MIDDLE_LINE_PADDING = 5;
    private static final int SPEED = 15;
    //手机的屏幕密度
    private static float density;
    private static final int TEXT_SIZE = 12;
    private static final int TEXT_PADDING_TOP = 40;
    private Paint paint;
    private int slideTop;

    private boolean isFirst;
    private Paint framePaint;
    private Paint textPaint;
//    private String textHint;

    //手电筒相关
    private Bitmap flashLightBitmap;
    private Bitmap openFlashLightBitmap;
    private Bitmap scanLineBitmap;
    private String flashLightOpenText;
    private String flashLightCloseText;
    private Paint flashLightTextPaint;

    public void setOpenFlashLight(boolean openFlashLight) {
        isOpenFlashLight = openFlashLight;
    }

    private boolean isOpenFlashLight;
    private int UNIT = 50;
    private int TEXT_UNIT = 40;
    private float flashLightBottomDistance;
    private float flashTextBottomDistance;
    private Rect flashRect;
    private Rect flashOpenRect;
    private Rect frame;

    public void setTopViewTotalHeight(int topViewTotalHeight) {
        this.topViewTotalHeight = topViewTotalHeight;
    }

    /**
     * 导航栏+特定view顶部的高度
     */
    private int topViewTotalHeight = -1;

    public boolean isStopDraw() {
        return isStopDraw;
    }

    public void setStopDraw(boolean stopDraw) {
        isStopDraw = stopDraw;
    }

    private boolean isStopDraw = false;//是否停止扫描区域
    private onFlashLightStateChangeListener mOnFlashLightStateChangeListener;

    //自动放大标识 目前思路根据距离
//    private boolean isLongDistance;

    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        resultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.qr_code_scanner);
        density = context.getResources().getDisplayMetrics().density;
        screenRate = (int) (20 * density);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        framePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.color_40black);
        resultColor = resources.getColor(R.color.result_view);
        frameColor = resources.getColor(R.color.white);
        resultPointColor = resources.getColor(R.color.white);
        possibleResultPoints = new ArrayList<>(5);
        lastPossibleResultPoints = null;
        //frame边框画笔
        framePaint.setColor(frameColor);
        framePaint.setStyle(Paint.Style.STROKE);
        framePaint.setStrokeWidth(1);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, TEXT_SIZE * density, context.getResources().getDisplayMetrics()));
        textPaint.setTypeface(Typeface.create("System", Typeface.BOLD));
        //flashLight
        flashLightBitmap = ((BitmapDrawable) resources.getDrawable(R.mipmap.icon_button_light)).getBitmap();
        openFlashLightBitmap = ((BitmapDrawable) resources.getDrawable(R.mipmap.icon_button_light)).getBitmap();
        flashLightBitmap = AndroidUtils.scaleBitmap(flashLightBitmap, AndroidUtils.dip2px(context, 22f), AndroidUtils.dip2px(context, 20f));
        scanLineBitmap = ((BitmapDrawable) resources.getDrawable(R.mipmap.img_line)).getBitmap();
        flashRect = new Rect();
        flashOpenRect = new Rect();
        flashLightOpenText = resources.getString(R.string.stt_4_2);
        flashLightCloseText = resources.getString(R.string.stt_4_3);
        flashLightTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        flashLightTextPaint.setColor(Color.WHITE);
        flashLightTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, TEXT_SIZE * density, context.getResources().getDisplayMetrics()));
        flashLightBottomDistance = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, UNIT * density, context.getResources().getDisplayMetrics());
        flashTextBottomDistance = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, TEXT_UNIT * density, context.getResources().getDisplayMetrics());
    }

    //初始化cameraManager对象
    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    private Rect getRect() {
        Rect frame = new Rect();
        int frameWidth = (int) (AndroidUtils.getScreenWidth(getContext()) * 0.6);
        int margin = (AndroidUtils.getScreenWidth(getContext()) - frameWidth) / 2;
        int topMargin = margin * 2 + AndroidUtils.getStatusBarHeight(getContext());
        frame.left = margin;
        frame.top = topMargin;
        frame.right = margin + frameWidth;
        frame.bottom = topMargin + frameWidth;
        return frame;
    }

    private Bitmap drawBitmapPostScale(Bitmap bitmap) {
        // Matrix类进行图片处理（缩小或者旋转）
        Matrix matrix = new Matrix();
        // 缩小一倍
        matrix.postScale(0.92f, 0.98f);
        // 生成新的图片
        if (bitmap != null) {
            Bitmap dstbmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            return dstbmp;
        }
        return null;
    }


    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        //在这里只是绘制扫描线 以及矩形框
        Rect frame = getRect();
        //初始化中间线滑动的最上边和最下边
        if (!isFirst) {
            isFirst = true;
            slideTop = frame.top;
        }
        //获取屏幕的宽和高
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        //paint.setColor(resultBitmap != null ? resultColor : maskColor);
        paint.setColor(getResources().getColor(R.color.color_50black));
        //扫描框四周区域
        canvas.drawRect(0, 0, width, frame.top, paint);
        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
        canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1, paint);
        canvas.drawRect(0, frame.bottom + 1, width, height, paint);
        //绘制frame边框
//        canvas.drawRect(frame, framePaint);
//        if (resultBitmap != null) {
//            // Draw the opaque result bitmap over the scanning rectangle
//            paint.setAlpha(CURRENT_POINT_OPACITY);
//            canvas.drawBitmap(resultBitmap, frame.left, frame.top, paint);
//        } else {
//            //画扫描框边上的角，总共8个部分
//            paint.setColor(getResources().getColor(R.color.color_fc4f00));
//            canvas.drawRect(frame.left, frame.top, frame.left + screenRate, frame.top + CORNER_WIDTH, paint);
//            canvas.drawRect(frame.left, frame.top, frame.left + CORNER_WIDTH, frame.top + screenRate, paint);
//            canvas.drawRect(frame.right - screenRate, frame.top, frame.right, frame.top + CORNER_WIDTH, paint);
//            canvas.drawRect(frame.right - CORNER_WIDTH, frame.top, frame.right, frame.top + screenRate, paint);
//            canvas.drawRect(frame.left, frame.bottom - CORNER_WIDTH, frame.left + screenRate, frame.bottom, paint);
//            canvas.drawRect(frame.left, frame.bottom - screenRate, frame.left + CORNER_WIDTH, frame.bottom, paint);
//            canvas.drawRect(frame.right - screenRate, frame.bottom - CORNER_WIDTH, frame.right, frame.bottom, paint);
//            canvas.drawRect(frame.right - CORNER_WIDTH, frame.bottom - screenRate, frame.right, frame.bottom, paint);

        paint.setAlpha(CURRENT_POINT_OPACITY);
        if (resultBitmap != null) {
            canvas.drawBitmap(drawBitmapPostScale(resultBitmap), frame.left - 15, frame.top - 17, paint);
        }
        //绘制文字
        if (ZxingConst.isWeakLight) {
            if (!isOpenFlashLight) {
                flashRect.left = (width - flashLightBitmap.getWidth()) / 2;
                flashRect.right = (width + flashLightBitmap.getWidth()) / 2;
                flashRect.bottom = (int) (frame.bottom - flashLightBottomDistance + 40);
                flashRect.top = (int) (frame.bottom - flashLightBottomDistance - flashLightBitmap.getHeight() + 40);
//                canvas.drawBitmap(flashLightBitmap, null, flashRect, paint);

                Rect flashTextRect = new Rect();
                flashTextRect.left = (int) ((width - flashLightTextPaint.measureText(flashLightOpenText)) / 2);
                flashTextRect.right = (int) ((width + flashLightTextPaint.measureText(flashLightOpenText)) / 2);
                flashTextRect.bottom = (int) (frame.bottom - flashTextBottomDistance);
                flashTextRect.top = flashTextRect.bottom - 5;
//                canvas.drawText(flashLightOpenText, flashTextRect.left, flashRect.bottom + 50, flashLightTextPaint);
            }
            }
            //绘制中间的线
            slideTop += SPEED;
            if (slideTop >= frame.bottom) {
                slideTop = frame.top;
            }
            Rect lineRect = new Rect();
            lineRect.left = frame.left + MIDDLE_LINE_PADDING;
            lineRect.right = frame.right - MIDDLE_LINE_PADDING;
            lineRect.top = slideTop - scanLineBitmap.getHeight() / 2;
            lineRect.bottom = slideTop + scanLineBitmap.getHeight() / 2;
            canvas.drawBitmap(scanLineBitmap, null, lineRect, paint);


            if (isOpenFlashLight) {
                flashOpenRect.left = (width - flashLightBitmap.getWidth()) / 2;
                flashOpenRect.right = (width + flashLightBitmap.getWidth()) / 2;
                flashOpenRect.bottom = (int) (frame.bottom - flashLightBottomDistance + 40);
                flashOpenRect.top = (int) (frame.bottom - flashLightBottomDistance - flashLightBitmap.getHeight() + 40);
//                canvas.drawBitmap(flashLightBitmap, null, flashOpenRect, paint);

                Rect flashOpenTextRect = new Rect();
                flashOpenTextRect.left = (int) ((width - flashLightTextPaint.measureText(flashLightCloseText)) / 2);
                flashOpenTextRect.right = (int) ((width + flashLightTextPaint.measureText(flashLightCloseText)) / 2);
                flashOpenTextRect.bottom = (int) (frame.bottom - flashTextBottomDistance);
                flashOpenTextRect.top = flashOpenTextRect.bottom - 5;
//                canvas.drawText(flashLightCloseText, flashOpenTextRect.left, flashOpenRect.bottom + 50, flashLightTextPaint);
            }


            List<ResultPoint> currentPossible = possibleResultPoints;
            List<ResultPoint> currentLast = lastPossibleResultPoints;
            if (null != currentPossible && currentPossible.isEmpty()) {
                lastPossibleResultPoints = null;
            } else {
                possibleResultPoints = new ArrayList<>(5);
                lastPossibleResultPoints = currentPossible;
                paint.setAlpha(CURRENT_POINT_OPACITY);
                paint.setColor(resultPointColor);
                if (null != currentPossible && currentPossible.isEmpty()) {
                    for (ResultPoint point : currentPossible) {
//                        canvas.drawCircle(frame.left + point.getX(), frame.top + point.getY(), 6.0f, paint);
                    }
                }
            }
            if (currentLast != null) {
                paint.setAlpha(CURRENT_POINT_OPACITY / 2);
                paint.setColor(resultPointColor);
                for (ResultPoint point : currentLast) {
//                    canvas.drawCircle(frame.left + point.getX(), frame.top + point.getY(), 3.0f, paint);
                }
            }

            if (isStopDraw) {
                return;
            }
            //只刷新扫描框的内容，其他地方不刷新
            postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top, frame.right, frame.bottom);
//        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (!isOpenFlashLight) {
            isOpenFlashLight = openFlashLight(x, y);
        } else {
            isOpenFlashLight = false;
        }
        if (null != mOnFlashLightStateChangeListener) {
            mOnFlashLightStateChangeListener.openFlashLight(isOpenFlashLight);
        }
        return super.onTouchEvent(event);
    }


    private boolean openFlashLight(int x, int y) {
        Rect rect = flashRect;
        rect.left = flashRect.left - 10;
        rect.right = flashRect.right + 10;
        rect.top = flashRect.top - 10;
        rect.bottom = flashRect.bottom + 10;
        return rect.contains(x, y);
    }

    public void reOnDraw() {
        frame = getRect();
        postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top, frame.right, frame.bottom);
    }

    public void reErrorDraw() {
        resultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.qr_code_scanner_error);
        invalidate();
    }

    public void reRightDraw() {
        resultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.qr_code_scanner);
        invalidate();
    }

    public interface onFlashLightStateChangeListener {
        void openFlashLight(boolean open);
    }

    public void setOnFlashLightStateChangeListener(onFlashLightStateChangeListener onFlashLightStateChangeListener) {
        mOnFlashLightStateChangeListener = onFlashLightStateChangeListener;
    }


    public void drawViewfinder() {
        Bitmap resultBitmap = this.resultBitmap;
        this.resultBitmap = null;
        if (resultBitmap != null) {
            resultBitmap.recycle();
        }
        invalidate();
    }

    public void drawResultBitmap(Bitmap barcode) {
        resultBitmap = barcode;
        invalidate();
    }


    public void addPossibleResultPoint(ResultPoint point) {
        List<ResultPoint> points = possibleResultPoints;
        synchronized (points) {
            points.add(point);
            int size = points.size();
            if (size > MAX_RESULT_POINTS) {
                // trim it
                points.subList(0, size - MAX_RESULT_POINTS / 2).clear();
            }
        }
    }
}