package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RegPwdCheckEntity extends BaseEntity {
    public boolean isSelected;

    public RegPwdCheckEntity(final boolean isSelected) {
        this.isSelected = isSelected;
    }
}
