package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingCategoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingCategoryListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class SpendingCategoryAdapter extends BaseRecyclerAdapter<SpendingCategoryListEntity> {

    private DetailShowListener detailShowListener;

    private String totalSpendingAmount;

    public void setTotalSpendingAmount(final String totalSpendingAmount) {
        this.totalSpendingAmount = totalSpendingAmount;
    }

    public void setDetailShowListener(final DetailShowListener detailShowListener) {
        this.detailShowListener = detailShowListener;
    }

    public SpendingCategoryAdapter(@Nullable List<SpendingCategoryListEntity> data) {
        super(R.layout.item_spending_category, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SpendingCategoryListEntity spendingCategoryListEntity, int position) {
        String spendingCategoryBudgetAmount = spendingCategoryListEntity.spendingCategoryBudgetAmount;
        int spendingCategoryRatio = spendingCategoryListEntity.spendingCategoryRatio;
        RoundedImageView ivCategory = baseViewHolder.getView(R.id.iv_spending_category);
        int categoryResId = AndroidUtils.getSpendingCategoryDrawable(Integer.parseInt(spendingCategoryListEntity.spendingCategoryId));
        ivCategory.setImageResource(ThemeSourceUtils.getSourceID(mContext,categoryResId));
        ProgressBar progressBudget = baseViewHolder.getView(R.id.progress_budget);
        progressBudget.setProgress(spendingCategoryRatio);
        progressBudget.setProgressDrawable(mContext.getDrawable(AndroidUtils.getSpendingProgressColor(spendingCategoryListEntity.spendingCategoryId)));
        baseViewHolder.setText(R.id.tv_spending_title, spendingCategoryListEntity.spendingCategoryName);
        baseViewHolder.setTextColor(R.id.tv_spending_title,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_3a3b44)));
        baseViewHolder.setText(R.id.tv_spending_content, spendingCategoryListEntity.spendingCategoryTxnCount + " " + mContext.getString(R.string.analytics_9));
        baseViewHolder.setTextColor(R.id.tv_spending_content,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_60white_603a3b44)));
        baseViewHolder.setText(R.id.tv_spending_money, AndroidUtils.getTransferStringMoney(spendingCategoryListEntity.spendingCategoryAmount) + " " + LocaleUtils.getCurrencyCode("")
                +"/"+AndroidUtils.getTransferStringMoney(totalSpendingAmount)+" " + LocaleUtils.getCurrencyCode(""));
        baseViewHolder.setTextColor(R.id.tv_spending_money,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_3a3b44)));
        baseViewHolder.setTextColor(R.id.tv_line,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_495478_4c495478)));
        baseViewHolder.setBackgroundColor(R.id.ll_detail,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_10white_4ceaeef3)));

        RecyclerView rlDetailList = baseViewHolder.getView(R.id.rl_detail_list);
        LinearLayout llDetailList = baseViewHolder.getView(R.id.ll_detail_list);
        ConstraintLayout conBudget = baseViewHolder.getView(R.id.con_budget);
        LinearLayout llBackCategory = baseViewHolder.getView(R.id.ll_back_category);
        ImageView ivBackCategory = baseViewHolder.getView(R.id.iv_back_category);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
//        rlDetailList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rlDetailList.setLayoutManager(manager);
        SpendingCategroyTwoAdapter spendingCategroyTwoAdapter = new SpendingCategroyTwoAdapter(spendingCategoryListEntity.spendingCategoryTxnDetail);
        spendingCategroyTwoAdapter.bindToRecyclerView(rlDetailList);
        List<SpendingCategoryDetailEntity> detailList = spendingCategoryListEntity.spendingCategoryTxnDetail;
        spendingCategroyTwoAdapter.setDataList(detailList);
        LocaleUtils.viewRotationY(mContext,ivBackCategory);
        int showDetail = spendingCategoryListEntity.showDetail;
        if(showDetail == 1){
            baseViewHolder.setVisible(R.id.tv_line, false);
            llDetailList.setVisibility(View.VISIBLE);
        }else{
            baseViewHolder.setVisible(R.id.tv_line, true);
            llDetailList.setVisibility(View.GONE);
        }
        conBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailList.size() == 0) {
                    return;
                }
//                if (llDetailList.getVisibility() == View.VISIBLE) return;
//                baseViewHolder.setVisible(R.id.tv_line, false);
//                llDetailList.setVisibility(View.VISIBLE);
                if(spendingCategoryListEntity.showDetail != 1) {
                    spendingCategoryListEntity.showDetail = 1;
                    if (detailShowListener != null) {
                        detailShowListener.showDetail(position);
                    }
                }
            }
        });

        llBackCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                baseViewHolder.setVisible(R.id.tv_line, true);
//                llDetailList.setVisibility(View.GONE);
                spendingCategoryListEntity.showDetail = 0;
                if (detailShowListener != null) {
                    detailShowListener.hideDetail(position);
                }
            }
        });
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }

    public interface DetailShowListener {
        void showDetail(int position);

        void hideDetail(int position);
    }
}
