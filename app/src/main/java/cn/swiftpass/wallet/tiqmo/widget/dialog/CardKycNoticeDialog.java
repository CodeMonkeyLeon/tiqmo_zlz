package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class CardKycNoticeDialog extends BottomDialog{

    private Context mContext;

    private ContinueListener continueListener;

    public void setContinueListener(ContinueListener continueListener) {
        this.continueListener = continueListener;
    }

    public CardKycNoticeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface ContinueListener {
        void clickContinue();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_card_kyc_notice, null);
        TextView tvContinue = view.findViewById(R.id.tv_continue);

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (continueListener != null) {
                    continueListener.clickContinue();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
