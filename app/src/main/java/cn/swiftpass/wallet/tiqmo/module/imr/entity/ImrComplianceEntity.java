package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class ImrComplianceEntity extends BaseEntity {
    //当通道是TransFast且需要填写compliance的参数名
    public String paramName;
    public String paramValue;
    //当通道是TransFast且需要填写compliance的参数名的 数据类型:String 和 Date
    public String paramType;
    public String paramLabel;
    public String paramFormat;

    public int getParamLength(){
        try {
            if (!TextUtils.isEmpty(paramFormat) && paramFormat.contains("-")) {
                return Integer.parseInt(paramFormat.split("-")[1]);
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 100;
    }
}
