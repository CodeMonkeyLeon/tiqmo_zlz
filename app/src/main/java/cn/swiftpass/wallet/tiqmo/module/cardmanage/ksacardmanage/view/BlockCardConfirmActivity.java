package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardSetPinPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class BlockCardConfirmActivity extends BaseCompatActivity<CardKsaContract.SetCardPinPresener> implements CardKsaContract.SetCardPinView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    private String proxyCardNo,reason,blockCardReasonStatus;
    private KsaCardEntity ksaCardEntity;

    private RiskControlEntity riskControlEntity;

    private static BlockCardConfirmActivity blockCardConfirmActivity;

    public static BlockCardConfirmActivity getBlockCardConfirmActivity() {
        return blockCardConfirmActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        blockCardConfirmActivity = this;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_block_card_confirm;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        blockCardConfirmActivity = this;
        tvTitle.setText(R.string.DigitalCard_73);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
//        ivBack.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_card_close));
        if (getIntent().getExtras() != null) {
            proxyCardNo = getIntent().getExtras().getString(Constants.proxyCardNo);
            reason = getIntent().getExtras().getString(Constants.blockCardReason);
            blockCardReasonStatus = getIntent().getExtras().getString(Constants.blockCardReasonStatus);
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
        }
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);

                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(BlockCardConfirmActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.proxyCardNo,proxyCardNo);
        mHashMap.put(Constants.blockCardReason, reason);
        mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
        mHashMap.put(Constants.blockCardReasonStatus, blockCardReasonStatus);
        mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_BLOCK);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_KSA_CARD_BLOCK);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    private void requestTransfer(){
        mPresenter.setKsaCardStatus(Constants.TYPE_CARD_BLOCK,proxyCardNo,reason,blockCardReasonStatus);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void setKsaCardPinSuccess(Void result) {

    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_BLOCK));
        ProjectApp.removeAllTaskExcludeMainStack();
        finish();
    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {

    }

    @Override
    public void setKsaCardChannelSuccess(Void result) {

    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public CardKsaContract.SetCardPinPresener getPresenter() {
        return new CardSetPinPresenter();
    }

    @OnClick({R.id.iv_back, R.id.tv_confirm, R.id.tv_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                List<String> additionalData = new ArrayList<>();
                if(ksaCardEntity != null) {
                    additionalData.add(ksaCardEntity.maskedCardNo);
                }
                mPresenter.getOtpType("ActionType", "TCA05", additionalData);
                break;
            case R.id.tv_cancel:
                ProjectApp.removeAllTaskExcludeMainStack();
                finish();
                break;
            default:break;
        }
    }
}
