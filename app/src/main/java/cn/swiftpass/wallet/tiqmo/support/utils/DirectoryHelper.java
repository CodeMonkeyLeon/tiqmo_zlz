package cn.swiftpass.wallet.tiqmo.support.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import java.io.File;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;


public class DirectoryHelper {

    private static final String EXTERNAL_STORAGE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/EquicomWallet/";
    private static final String INTERNAL_STORAGE_PATH = ProjectApp.getContext().getFilesDir().getAbsolutePath() + "/Internal/";

    private static final String PATH_IMAGE = "image/";
    private static final String PATH_LOG = "log/";
    private static final String PATH_DOWNLOAD = "download/";
    private static final String PATH_WEB_CACHE = "web_cache/";

    private static String createPathIfDoesNotExist(String path) {
        File file;
        file = new File(path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                LogUtil.e("create path fail：" + path);
            }
        }
        return path;
    }

    public static Uri getUriFromPath(String path, @Nullable String fileName) {
        createPathIfDoesNotExist(path);
        File file = new File(path);
        if (!TextUtils.isEmpty(fileName)) {
            path = file.getAbsolutePath() + File.separator + fileName;
        }
        Context context = ProjectApp.getContext();
        return FileProvider.getUriForFile(context, context.getPackageName(), new File(path));
    }

    public static String getExternalImagePath() {
        return createPathIfDoesNotExist(EXTERNAL_STORAGE_PATH + PATH_IMAGE);
    }

    public static String getExternalLogPath() {
        return createPathIfDoesNotExist(EXTERNAL_STORAGE_PATH + PATH_LOG);
    }

    public static String getExternalDownloadPath() {
        return createPathIfDoesNotExist(EXTERNAL_STORAGE_PATH + PATH_DOWNLOAD);
    }

    public static String getInternalWebCachePath() {
        return createPathIfDoesNotExist(INTERNAL_STORAGE_PATH + PATH_WEB_CACHE);
    }
}
