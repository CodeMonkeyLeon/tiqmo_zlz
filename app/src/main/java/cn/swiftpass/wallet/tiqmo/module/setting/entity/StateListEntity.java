package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class StateListEntity extends BaseEntity {
    public List<StateEntity> statesInfoList = new ArrayList<>();
}
