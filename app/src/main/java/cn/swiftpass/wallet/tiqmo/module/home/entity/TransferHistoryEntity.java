package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferHistoryEntity extends BaseEntity {
    public String currencyType;
    public String orderType;
    public String tradeAmount = "";
    public String orderNo;
    public String createTime;
    public String orderStatus;
    public String logo;
    public String voucherBrandLightLogo;
    public String orderDirection;
    public String orderDesc;
    public String createDateDesc;
    public String sex;
    public int sceneType;
    public int subSceneType;
}
