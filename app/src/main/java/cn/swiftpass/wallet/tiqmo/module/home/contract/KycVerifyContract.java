package cn.swiftpass.wallet.tiqmo.module.home.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class KycVerifyContract {

    public interface View extends BaseView<KycVerifyContract.Presenter> {
        void kycVerifySuccess(UserInfoEntity result);

        void kycVerifyFail(String errorCode, String errorMsg);

        void showErrorMsg(String errorCode, String errorMsg);

        void showCityListDia(CityEntity result);

        void getStateListSuccess(StateListEntity result);

        void showSourceOfFund(List<SelectInfoEntity> data);

        void showProfession(List<SelectInfoEntity> data);

        void showSalaryRange(List<SelectInfoEntity> data);

        void getEmploymentSectorSuccess(List<SelectInfoEntity> data);
    }

    public interface Presenter extends BasePresenter<KycVerifyContract.View> {
        void kycVerify(String callingCode, String phone, String residentId, String birthday, String sceneType, String statesId, String citiesId, String address,
                       String employmentCode,
                       String professionCode,
                       String sourceOfFundCode,
                       String salaryRangeCode,String employerName,String beneficialOwner,String relativesExposedPerson,String referralCode,
                       String shareLink);

        void getCityList(String statesId);

        void getStateList();

        void getSourceOfFund();

        void getProfession(String businessType,String parentBussinessCode);

        void getSalaryRange();

        void getEmploymentSector(String businessType,String parentBussinessCode);
    }

    public interface ReferralView extends BaseView<KycVerifyContract.ReferralPresenter> {
        void referralVerifySuccess(Void result);

        void referralVerifyFail(String errorCode, String errorMsg);
    }

    public interface ReferralPresenter extends BasePresenter<KycVerifyContract.ReferralView> {
        void referralVerify(String referralCode);
    }
}
