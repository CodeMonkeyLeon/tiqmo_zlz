package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by YZX on 2019年09月20日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class JSResponseEntity implements Parcelable {
    private String result;
    private String msg;
    private String preId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPreId() {
        return preId;
    }

    public void setPreId(String preId) {
        this.preId = preId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.result);
        dest.writeString(this.msg);
        dest.writeString(this.preId);
    }

    public JSResponseEntity() {
    }

    protected JSResponseEntity(Parcel in) {
        this.result = in.readString();
        this.msg = in.readString();
        this.preId = in.readString();
    }

    public static final Creator<JSResponseEntity> CREATOR = new Creator<JSResponseEntity>() {
        @Override
        public JSResponseEntity createFromParcel(Parcel source) {
            return new JSResponseEntity(source);
        }

        @Override
        public JSResponseEntity[] newArray(int size) {
            return new JSResponseEntity[size];
        }
    };
}
