package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChatHistoryEntity extends BaseEntity {

    private List<ChatHistoryItemEntity> Individual;
    private List<ChatHistoryItemEntity> Groups;
    private List<ChatHistoryItemEntity> all;

    public List<ChatHistoryItemEntity> getAll() {
        return this.all;
    }

    public void setAll(final List<ChatHistoryItemEntity> all) {
        this.all = all;
    }

    public List<ChatHistoryItemEntity> getIndividual() {
        return this.Individual;
    }

    public void setIndividual(final List<ChatHistoryItemEntity> individual) {
        this.Individual = individual;
    }

    public List<ChatHistoryItemEntity> getGroups() {
        return this.Groups;
    }

    public void setGroups(final List<ChatHistoryItemEntity> groups) {
        this.Groups = groups;
    }
}
