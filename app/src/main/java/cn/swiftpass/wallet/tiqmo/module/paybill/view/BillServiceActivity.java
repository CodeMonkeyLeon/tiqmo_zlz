package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.BillServiceSearchResultAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillTypeAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.BillServiceSearchResultEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeIconEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.interfaces.OnBillServiceSearchResultItemClickListener;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillServicePresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class BillServiceActivity extends BaseCompatActivity<PayBillContract.BillServicePresenter> implements PayBillContract.BillServiceView {

    @Override
    protected int getLayoutID() {
        return R.layout.act_bill_service;
    }

    public static final String TAG = "BillServiceActivity";

    public static final String DATA_BILL_SERVICE = "DATA_BILL_SERVICE";

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    //    @BindView(R.id.iv_search)
//    ImageView ivSearch;
//    @BindView(R.id.con_tv_search)
//    ConstraintLayout conTvSearch;
//    @BindView(R.id.iv_close)
//    ImageView ivClose;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.iv_del)
    ImageView ivDel;
    //    @BindView(R.id.con_et_search)
//    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_type_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout mClBillService;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_country)
    RoundedImageView ivCountry;
    @BindView(R.id.tv_country_name)
    TextView tvCountryName;


    @BindView(R.id.id_ll_search_result)
    LinearLayout mLlSearchResult;
    @BindView(R.id.rv_country_list_search_result)
    RecyclerView mRvSearchResult;


    private StatusView typeStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private String searchString;
    private String mChannelCode = Constants.paybill_local;
    private String mCountryCode = Constants.SAU;
    private String mBillerType;

    private String billerId, billerName, billerLogo;

    private PayBillTypeAdapter payBillTypeAdapter;
    private BillServiceSearchResultAdapter mBillSearchResultAdapter;

    private PayBillTypeEntity mData;
    private List<PayBillTypeEntity> mDataList = new ArrayList<>();
    private List<PayBillTypeEntity> filterTypeList = new ArrayList<>();
    private List<PayBillTypeIconEntity> mChildFilterTypeList = new ArrayList<>();
    private List<PayBillerEntity> mSearchResultList = new ArrayList<>();


    public static void startBillServiceActivity(Activity fromActivity, PayBillTypeEntity data, String countryCode, String channelCode, String billerType) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(DATA_BILL_SERVICE, data);
        hashMap.put(Constants.CHANNEL_CODE, channelCode);
        hashMap.put(Constants.COUNTRY_CODE, countryCode);
        hashMap.put(Constants.BILLER_TYPE, billerType);
        ActivitySkipUtil.startAnotherActivity(fromActivity, BillServiceActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    @Override
    protected void init(Bundle savedInstanceState) {

        initView();

        if (getIntent() != null) {
            mData = (PayBillTypeEntity) getIntent().getSerializableExtra(DATA_BILL_SERVICE);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);
            mCountryCode = getIntent().getStringExtra(Constants.COUNTRY_CODE);
            mBillerType = getIntent().getStringExtra(Constants.BILLER_TYPE);

            tvTitle.setText(mData.billerTypeShow);

            mDataList.clear();
            mDataList.add(mData);
            payBillTypeAdapter.notifyDataSetChanged();
        }

    }


    private void initView() {
        tvCountryName.setText(R.string.Country_code_2);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setVisibility(View.GONE);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_31));
        typeStatusView = new StatusView.Builder(mContext, mRecyclerView).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();


        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        mRecyclerView.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(manager);


        payBillTypeAdapter = new PayBillTypeAdapter(mDataList);
        payBillTypeAdapter.bindToRecyclerView(mRecyclerView);


        // 搜索结果
        LinearLayoutManager searchResultManager = new LinearLayoutManager(mContext);
        MyItemDecoration searchResultItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        mRvSearchResult.addItemDecoration(searchResultItemDecoration);
        searchResultManager.setOrientation(RecyclerView.VERTICAL);
        mRvSearchResult.setLayoutManager(searchResultManager);

        mBillSearchResultAdapter = new BillServiceSearchResultAdapter(mSearchResultList);
        mBillSearchResultAdapter.bindToRecyclerView(mRvSearchResult);


        payBillTypeAdapter.setOnIconItemClickListener(new PayBillTypeAdapter.OnIconItemClick() {
            @Override
            public void onItemClick(PayBillTypeEntity payBillTypeEntity, PayBillTypeIconEntity item, int position) {
                if (item != null) {
                    LogUtils.i(TAG, "点击bill service 条目 : " + AndroidUtils.jsonToString(item));
                    HashMap<String, Object> mHashMap = new HashMap<>();
                    mHashMap.put(Constants.paybill_countryCode, mCountryCode);
                    mHashMap.put(Constants.paybill_billerType, mBillerType);
                    mHashMap.put(Constants.paybill_billerDescription, item.billerDescription);
                    mHashMap.put(Constants.CHANNEL_CODE, mChannelCode);
                    mHashMap.put(Constants.TITLE, item.billerDescriptionShow);
                    ActivitySkipUtil.startAnotherActivity(BillServiceActivity.this, BillServiceBillerListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });


        mBillSearchResultAdapter.setOnSearchResultItemClickListener(new OnBillServiceSearchResultItemClickListener() {
            @Override
            public void onBillServiceSearchResultItemClick(int position, PayBillerEntity item) {
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                if (item != null) {
                    billerId = item.billerId;
                    billerName = item.billerName;
                    billerLogo = item.imgUrl;
                    mPresenter.getBillServiceIOList(billerId, billerId, mChannelCode);
                }
            }
        });


        showSearchResult(false);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchString = s.toString();
                ivDel.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(searchString)) {
                    ivDel.setVisibility(View.GONE);
                    showSearchResult(false);
                } else if (searchString.length() >= 3) {
                    mSearchResultList.clear();
                    showSearchResult(true);
                    if (mPresenter != null) {
                        mPresenter.searchBiller(searchString, mCountryCode, mChannelCode);
                    }
                }
            }
        });

        ivDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });

    }


    @OnClick({R.id.iv_back})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }


    private void showSearchResult(boolean isShow) {
        if (isShow) {
            //展示搜索结果
            mLlSearchResult.setVisibility(View.VISIBLE);
            mClBillService.setVisibility(View.GONE);
        } else {
            //隐藏搜索结果
            mLlSearchResult.setVisibility(View.GONE);
            mClBillService.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public PayBillContract.BillServicePresenter getPresenter() {
        return new PayBillServicePresenter();
    }

    @Override
    public void searchBillerSuccess(BillServiceSearchResultEntity billServiceSearchResult) {
        if (billServiceSearchResult != null) {
            LogUtils.i(TAG, "获取到Bill Service Search 数据 : " + AndroidUtils.jsonToString(billServiceSearchResult));
            mSearchResultList.clear();
            mSearchResultList.addAll(billServiceSearchResult.getBillerList());
            mBillSearchResultAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void getBillServiceIOListSuccess(PayBillIOListEntity payBillIOListEntity) {
        if (payBillIOListEntity != null) {
            LogUtils.i(TAG, "获取到BillServiceIO数据 : " + AndroidUtils.jsonToString(payBillIOListEntity));
            PayBillFetchActivity.startPayBillFetchActivity(
                    BillServiceActivity.this,
                    payBillIOListEntity,
                    billerId,
                    billerName,
                    billerLogo,
                    mChannelCode);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
//        showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
        showTipDialog(errorMsg);
        showSearchResult(false);
    }
}