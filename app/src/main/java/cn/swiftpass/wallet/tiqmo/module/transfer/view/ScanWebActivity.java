package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseWebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.ScanOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferQrCodePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.HeightProvider;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class ScanWebActivity extends BaseWebViewActivity<TransferContract.TransferQrCodePresenter> implements TransferContract.TransferQrCodeView {
    @BindView(R.id.webView)
    WebView mWebView;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    String m_url;
//    private ProgressBar mProgressBar;

//    private String mWebUrl;

    private String mJSCallback;
    private String mJSData;

    private String  monthLimitAmt;
    private HeightProvider heightProvider;


    @Override
    protected int getLayoutID() {
        return R.layout.activity_scan_web;
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, headCircle, ivBack);
        setDarkBar();
        tvTitle.setText(R.string.stt_1);
        if (getIntent() != null && getIntent().getExtras() != null) {
            m_url = getIntent().getExtras().getString(Constants.M_URL);
        }
        if (!TextUtils.isEmpty(m_url)) {
            mWebView.setWebChromeClient(mWebChromeClient);
            mWebView.setWebViewClient(mWebViewClient);
            initWebView(mWebView, m_url, false);
        }

        heightProvider = new HeightProvider(ScanWebActivity.this);
//        heightProvider.init().setHeightListener(new HeightProvider.HeightListener() {
//            @Override
//            public void onHeightChanged(int height) {
//                if (height > 0) {
//                    llContent.setTranslationY(-height + AndroidUtils.dip2px(mContext, 40f));
//                } else {
//                    llContent.setTranslationY(-height);
//                }
//            }
//        });
    }

    //解析URI，判断是否是H5支付请求
    private boolean analyzeUrl(Uri uri) {
        String protocol = uri.getScheme();
        String host = uri.getHost();
        //判空，同时判断host是否是swallet开头的H5支付请求
        if (TextUtils.isEmpty(protocol) || TextUtils.isEmpty(host) || !AppConfig.H5_REQUEST_FLAG.equals(protocol)) {
            return false;
        }
        mJSCallback = uri.getQueryParameter(AppConfig.H5_PARAM_CALLBACK);//回调的JS接口
        mJSData = uri.getQueryParameter(AppConfig.H5_PARAM_DATA);//JS数据
        switch (host) {//host对应了不同的操作，本质上就是网页通过url调用APP的功能
            case AppConfig.H5_FUNCTION_PAY://支付
                mPresenter.checkOut("", mJSData);
                break;
            case AppConfig.H5_FUNCTION_CLOSE://关闭
                finish();
                break;
            default:
                break;
        }
        return true;
    }

    //回调JS的接口，成功
//    private void callJSSuccessful(OrderResultEntity result) {
//        JSResponseEntity response = new JSResponseEntity();
//        response.setResult(AppConfig.H5_RESPONSE_CODE_SUCCESSFUL);
//        response.setPreId(result.getOrderNo());
//        mWebView.loadUrl(String.format("javascript:%s('%s')", mJSCallback, ApiHelper.GSON.toJson(response)));
//        LogUtil.e(String.format("javascript:%s('%s')", mJSCallback, ApiHelper.GSON.toJson(response)));
//    }

    //回调JS的接口，失败，目前不需要调用失败，如果支付失败基本已经再收银台显示错误了
//    private void callJSFailure(String error) {
//        JSResponseEntity response = new JSResponseEntity();
//        response.setResult(AppConfig.H5_RESPONSE_CODE_FAILURE);
//        response.setMsg(error);
//        mWebView.loadUrl(String.format("javascript:%s('%s')", mJSCallback, ApiHelper.GSON.toJson(response)));
//        LogUtil.e(String.format("javascript:%s('%s')", mJSCallback, ApiHelper.GSON.toJson(response)));
//    }

    private final WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            String url = mWebView.getUrl();
            if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(title) && url.contains(title)) {//防止title显示网站
                return;
            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
//            if (newProgress == 100) {
//                mProgressBar.setVisibility(View.GONE);
//            } else {
//                mProgressBar.setVisibility(View.VISIBLE);
//                mProgressBar.setProgress(newProgress);
//            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
        }
    }

    private final WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //拦截URL并且解析
            return analyzeUrl(Uri.parse(url));
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
        mWebView.clearHistory();
        mWebView.destroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
//            callJSSuccessful(mOrderResultEntity);
        }
    }

    @Override
    public void getQrCodeSuccess(QrCodeEntity qrCodeEntity) {

    }

    @Override
    public void getQrCodeFail(String errCode, String errMsg) {

    }

    @Override
    public void getCrCodeSuccess(QrCodeEntity qrCodeEntity) {

    }

    @Override
    public void getCrCodeFail(String errCode, String errMsg) {

    }

    @Override
    public void checkQrCodeFailed(String errorCode, String errorMsg) {

    }

    @Override
    public void checkQrCodeSuccess(UserInfoEntity response) {

    }

    @Override
    public void checkOrderCodeFailed(String errorCode, String errorMsg) {

    }

    @Override
    public void checkOrderCodeSuccess(ScanOrderEntity response) {

    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        monthLimitAmt = transferLimitEntity.monthLimitAmt;
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {

    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        HashMap<String, Object> map = new HashMap<>(16);
        checkOutEntity.monthLimitAmt = monthLimitAmt;
        map.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        ActivitySkipUtil.startAnotherActivity(ScanWebActivity.this, ScanPayActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    @Override
    public void checkOutFail(String errCode, String errMsg) {

    }

    @Override
    public TransferContract.TransferQrCodePresenter getPresenter() {
        return new TransferQrCodePresenter();
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked() {
        finish();
    }
}
