package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MsgContentEntity extends BaseEntity {
    public String activityNameEnglish;
    public String activityNameArabic;
    public String activityNo;
    public String activityType;
    public String returnType;
}
