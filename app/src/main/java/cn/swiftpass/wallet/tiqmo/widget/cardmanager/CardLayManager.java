package cn.swiftpass.wallet.tiqmo.widget.cardmanager;

import android.view.View;
import android.view.ViewPropertyAnimator;

import androidx.recyclerview.widget.RecyclerView;

public class CardLayManager extends RecyclerView.LayoutManager {

//    private RecyclerView mRecyclerView;
//    private ItemTouchHelper mItemTouchHelper;
    private boolean isScrollEnabled = true;
    private boolean isRemove = false;

    public void setRemove(boolean remove) {
        isRemove = remove;
    }

    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }

    public CardLayManager() {
    }

//    private <T> T checkIsNull(T t) {
//        if (t == null) {
//            throw new NullPointerException();
//        }
//        return t;
//    }

    @Override
    public boolean canScrollVertically() {
        return isScrollEnabled && true;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }


    @Override
    public boolean isAutoMeasureEnabled() {
        return true;
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
//        detachAndScrapAttachedViews(recycler);
//
//        for (int i = 0; i < getItemCount(); i++) {
//            //这里就是从缓存里面取出
//            View view = recycler.getViewForPosition(i);
//            //将View加入到RecyclerView中
//            addView(view);
//            //对子View进行测量
//            measureChildWithMargins(view, 0, 0);
//
//            android.view.ViewGroup.LayoutParams params = view.getLayoutParams();
//            if (params == null) {
//                throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView.LayoutParams");
//            }
//
//            int left = (getItemCount() - 1 - i) * 15;
//            int top = (int) ((double) (view.getMeasuredHeight() * (getItemCount() - i)) * 0.03);
//            int right = view.getMeasuredWidth() - (getItemCount() - 1 - i) * 15;
//            int bottom = top + view.getMeasuredHeight();
//
//            this.layoutDecoratedWithMargins(view, left, top, right, bottom);
//
//        }
        // 在布局之前，将所有的子 View 先 Detach 掉，放入到 Scrap 缓存中

        detachAndScrapAttachedViews(recycler);

        int itemCount = getItemCount();
        if (itemCount == 1) {
            isScrollEnabled = false;
        }

        // 在这里，我们默认配置 CardConfig.DEFAULT_SHOW_ITEM = 3。即在屏幕上显示的卡片数为3

        // 当数据源个数大于最大显示数时

        if (itemCount > CardConfig.DEFAULT_SHOW_ITEM) {

            // 把数据源倒着循环，这样，第0个数据就在屏幕最上面了

            for (int position = itemCount - 1; position >= 0; position--) {

                final View view = recycler.getViewForPosition(position);

                // 将 Item View 加入到 RecyclerView 中

                addView(view);

                // 测量 Item View

                measureChildWithMargins(view, 0, 0);

                // getDecoratedMeasuredWidth(view) 可以得到 Item View 的宽度

                // 所以 widthSpace 就是除了 Item View 剩余的值

                int widthSpace = getWidth() - getDecoratedMeasuredWidth(view);

                // 同理

                int heightSpace = getHeight() - getDecoratedMeasuredHeight(view);

                // 将 Item View 放入 RecyclerView 中布局

                // 在这里默认布局是放在 RecyclerView 中心

                layoutDecoratedWithMargins(view, widthSpace / 2, heightSpace / 2,

                        widthSpace / 2 + getDecoratedMeasuredWidth(view),

                        heightSpace / 2 + getDecoratedMeasuredHeight(view));

                // 其实屏幕上有四张卡片，但是我们把第三张和第四张卡片重叠在一起，这样看上去就只有三张

                // 第四张卡片主要是为了保持动画的连贯性

                if (position > CardConfig.DEFAULT_SHOW_ITEM) {
                    // 按照一定的规则缩放，并且偏移Y轴。
                    ViewPropertyAnimator animation = view.animate();
                    animation.scaleX(1 - 10 * CardConfig.DEFAULT_SCALE)
                            .scaleY(1 - 10 * CardConfig.DEFAULT_SCALE)
                            .translationY(1 * view.getMeasuredHeight() / CardConfig.DEFAULT_TRANSLATE_Y).setDuration(500).start();

                } else {

                    ViewPropertyAnimator animation = view.animate();
                    animation.scaleX(1 - (position) * CardConfig.DEFAULT_SCALE)
                            .scaleY(1 - (position) * CardConfig.DEFAULT_SCALE)
                            .translationY((position) * view.getMeasuredHeight() / CardConfig.DEFAULT_TRANSLATE_Y).setDuration(500).start();

                }

            }

        } else {

            // 当数据源个数小于或等于最大显示数时，和上面的代码差不多

            for (int position = itemCount - 1; position >= 0; position--) {

                final View view = recycler.getViewForPosition(position);

                addView(view);

                measureChildWithMargins(view, 0, 0);

                int widthSpace = getWidth() - getDecoratedMeasuredWidth(view);

                int heightSpace = getHeight() - getDecoratedMeasuredHeight(view);


                layoutDecoratedWithMargins(view, widthSpace / 2, heightSpace / 2,

                        widthSpace / 2 + getDecoratedMeasuredWidth(view),

                        heightSpace / 2 + getDecoratedMeasuredHeight(view));


//                if (position > 0) {
                ViewPropertyAnimator animation = view.animate();
                animation.scaleX(1 - (position) * CardConfig.DEFAULT_SCALE)
                        .scaleY(1 - (position) * CardConfig.DEFAULT_SCALE)
                        .translationY((position) * view.getMeasuredHeight() / CardConfig.DEFAULT_TRANSLATE_Y).setDuration(500).start();
//                view.setScaleX(1 - (position) * CardConfig.DEFAULT_SCALE);
//
//                view.setScaleY(1 - (position) * CardConfig.DEFAULT_SCALE);
//
//                view.setTranslationY((position) * view.getMeasuredHeight() / CardConfig.DEFAULT_TRANSLATE_Y);

//                } else {
//
////                    view.setOnTouchListener(mOnTouchListener);
//
//                }

            }

        }
    }
}

