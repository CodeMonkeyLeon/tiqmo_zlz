package cn.swiftpass.wallet.tiqmo.support.utils;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SMSReceiver  extends BroadcastReceiver {
    private static final String TAG = "SMSReceiver";
    private static OnReceivedMessageListener mOnReceivedMessageListener;
    @Override
    public void onReceive(Context context, Intent intent) {
        //调用短信内容获取类
        getMsg(context, intent);
    }

    /**
     * 短信内容的获取
     * @param context
     * @param intent
     */
    private void getMsg(Context context, Intent intent) {
        //解析短信内容 pdus短信单位pdu
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Object[] pdus = (Object[]) intent.getExtras().get("pdus");
            assert pdus != null;
            for (Object pdu : pdus) {
                //封装短信参数的对象
                SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdu);
                String number = sms.getOriginatingAddress(); //获取发送短信的手机号
                String body = sms.getMessageBody(); //获取接收短信手机号的完整信息
                System.out.println("发短信的手机号："+number+" "+"监听到的短信息：" + body);

                //写处理逻辑，调用相关类 匹配获取验证码并复制到剪贴板
                getCode(context, body);
            }
        }
    }

    /**
     * 匹配出验证码并复制到剪贴板
     * @param context
     * @param body
     */
    private void getCode(Context context, String body) {
        //获取剪贴板管理器：
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        Pattern pattern1 = Pattern.compile("(\\d{4,6})");//正则匹配4-6位数字
        Matcher matcher1 = pattern1.matcher(body);//进行匹配
        if (matcher1.find()) {//匹配成功
            String code = matcher1.group(0);
            // 创建普通字符型ClipData
            mOnReceivedMessageListener.onReceived(code);
            ClipData mClipData = ClipData.newPlainText("Label", code);
            // 将mClipData内容放到手机系统剪贴板
            cm.setPrimaryClip(mClipData);
            LogUtils.d(TAG, "onReceive: " + code);
        } else {
        }
    }

    public interface OnReceivedMessageListener{
        void onReceived(String message);
    }

    public void setOnReceivedMessageListener(OnReceivedMessageListener onReceivedMessageListener){
       mOnReceivedMessageListener = onReceivedMessageListener;
    }
}
