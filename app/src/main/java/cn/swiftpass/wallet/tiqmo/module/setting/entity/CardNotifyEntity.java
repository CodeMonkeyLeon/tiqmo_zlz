package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.ExtendEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class CardNotifyEntity extends BaseEntity {

    public String cardNo;
    public String cardFaceMini;
    public String expire;
    public String custName;
    public String title;
    public String bankName;
    public String cardTypeDesc;
    public String cardType;
    public String directionDesc;
    public String popupInfo;

    //充值字段  {startTime=11:51 AM,04 Aug 2020, subject=ADD_MONEY,
    // paymentMethodDesc=************9399, orderNo=TU96601000000005731596513113145,
    // respCode=000000, msgId=MSG9660103005741596513134658,
    // title=Your Wallet balance has been credited with 5.00 SAR from ************9399,
    // orderCurrencyCode=SAR, respMsg=成功, orderAmount=500, msgType=NOTIFY}

    public String startTime;
    public String subject;
    public String paymentMethodDesc;
    public String payMethod;
    public String orderNo;
    public String respCode;
    public String msgId;
    public String orderCurrencyCode;
    public String respMsg;
    //请求转账付款人
    public String payerName;
    //原金额
    public String orderAmount;
    public String orderStatus;
    public String msgType;
    public String transferName;
    public String operateType;
    public String bizCode;
    public String bizMsg;
    public String payMethodDesc;


    public String requestIcon;
    public String receiveIcon;
    public String requestPhone;
    public String receivePhone;
    public String requestUserName;
    public String receiveUserName;
    public String requestSex;
    public String requestSubject;
    public String receiveSex;
    public String transferAmount;
    public String currencyCode;
    public String logo;
    public String orderDesc;
    public String sex;

    public String inAppTitle;
    public List<ExtendEntity> extendProperties = new ArrayList<>();
    public List<ExtendEntity> basicExtendProperties = new ArrayList<>();
    public int sceneType;
    public String orderDirection;

    public String merchantId;
    public String merchantName;
    public String orderType;
    public String outTradeNo;
    public String originalOrderNo;
    public String payerAmount;

    public String activityNo;
    public String activityType;
    public String returnType;
    /**
     * 充值类型
     */
    public String topUpType = "CREDIT/DEBIT CARD";

    public String discountTransFees;
    public String brandName;
    public String destinationCountry;

    public String transferType;
    public boolean fromHistory;

    //分享描述
    public String shareDesc = "";
    //优惠类型 1：返现 2：立减
    public String discountType;
    //返现订单号
    public String cashBackOrderNo;
    // 代金券的 pin 码
    public String voucherPinCode;
    //paybill 备注
    public String remark;

    public int subSceneType;
    //是否展示 发票 按钮 1-展示；0-不展示
    public int isShowEInvoice;

    //download按钮是否需要下载pdf   1-下载；0-不下载
    public int downloadFile;
    public String pdfFilePath;
    /**
     * pdf的名称
     */
    public String pdfFileName;
    /**
     * 发票类型
     */
    public String invoiceType;

    public boolean isNeedDownloadFile() {
        return downloadFile == 1;
    }

    public String getAppsflyerMoney() {
        try {
            if (!TextUtils.isEmpty(payerAmount)) {
                return AndroidUtils.getTransferMoney(payerAmount);
            } else if (!TextUtils.isEmpty(transferAmount)) {
                return AndroidUtils.getTransferMoney(transferAmount);
            }
            return AndroidUtils.getTransferMoney(orderAmount);
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "";
    }

}
