package cn.swiftpass.wallet.tiqmo.module.topup.presenter;

import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RechargeOrderListPresenter implements RechargeContract.RechargeOrderListPresenter{

    RechargeContract.RechargeOrderListView mRechargeOrderListView;

    @Override
    public void getRechargeOrderList() {
        AppClient.getInstance().getTransferManager().getRechargeOrderList(new LifecycleMVPResultCallback<RechargeOrderListEntity>(mRechargeOrderListView,true) {
            @Override
            protected void onSuccess(RechargeOrderListEntity result) {
                mRechargeOrderListView.getRechargeOrderListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeOrderListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getRechargeCountry() {

    }

    @Override
    public void getRechargeOrderDetail(String recordId) {
        AppClient.getInstance().getTransferManager().getRechargeOrderDetail(recordId, new LifecycleMVPResultCallback<RechargeOrderDetailEntity>(mRechargeOrderListView,true) {
            @Override
            protected void onSuccess(RechargeOrderDetailEntity result) {
                mRechargeOrderListView.getRechargeOrderDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeOrderListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void deleteRechargeOrder(String recordId) {
        AppClient.getInstance().getTransferManager().deleteRechargeOrder(recordId, new LifecycleMVPResultCallback<Void>(mRechargeOrderListView,true) {
            @Override
            protected void onSuccess(Void result) {
                mRechargeOrderListView.deleteRechargeOrderSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeOrderListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getRechargeProductList(String operatorId) {
        AppClient.getInstance().getTransferManager().getRechargeProductList(operatorId, new LifecycleMVPResultCallback<RechargeProductListEntity>(mRechargeOrderListView,true) {
            @Override
            protected void onSuccess(RechargeProductListEntity result) {
                mRechargeOrderListView.getRechargeProductListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeOrderListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(RechargeContract.RechargeOrderListView rechargeOrderListView) {
        this.mRechargeOrderListView = rechargeOrderListView;
    }

    @Override
    public void detachView() {
        this.mRechargeOrderListView = null;
    }
}
