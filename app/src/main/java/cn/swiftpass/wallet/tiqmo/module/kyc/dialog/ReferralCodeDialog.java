package cn.swiftpass.wallet.tiqmo.module.kyc.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycVerifyActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.AreaUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ReferralCodeDialog extends BottomDialog {

    private Context mContext;
    private static ReferralCodeDialog referralCodeDialog = null;
    private boolean isSkip = false;
    TextView tvCodeContent;
    private ReferralCodeDescEntity result;

    public boolean isSkip() {
        return isSkip;
    }

    public void setSkip(boolean skip) {
        isSkip = skip;
    }

    public ReferralCodeDialog(Context context, ReferralCodeDescEntity result) {
        super(context);
        this.mContext = context;
        this.result = result;
        initViews();
        showDesc(result);
    }

    public ReferralCodeDialog(Context context) {
        this(context, null);
    }

    public static void reset() {
        if (referralCodeDialog != null) {
            if (referralCodeDialog.isShowing()) {
                referralCodeDialog.dismiss();
            }
            referralCodeDialog = null;
        }
    }

    public static ReferralCodeDialog getInstance(Context context, ReferralCodeDescEntity result) {
        if (referralCodeDialog == null) {
            referralCodeDialog = new ReferralCodeDialog(context, result);
        } else {
            referralCodeDialog.mContext = context;
            referralCodeDialog.result = result;
        }
        return referralCodeDialog;
    }

    public static ReferralCodeDialog getInstance(Context context) {
        return getInstance(context, null);
    }

    private void initViews() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_bottom_referral_code, null);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        ImageView ivRefCode = view.findViewById(R.id.iv_ref_code);
        ImageView ivCode = view.findViewById(R.id.iv_code);
        tvCodeContent = view.findViewById(R.id.tv_code_content);
        EditTextWithDel etReferralCode = view.findViewById(R.id.et_referral_code);
        TextView tvSkip = view.findViewById(R.id.tv_skip);
        TextView tvVerifyCode = view.findViewById(R.id.tv_verify_code);
        ConstraintLayout llContent = view.findViewById(R.id.ll_content);

        etReferralCode.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_NUMBER)});

        etReferralCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    tvVerifyCode.setEnabled(false);
                } else {
                    tvVerifyCode.setEnabled(true);
                }
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startKyc("");
                isSkip = true;
            }
        });

        tvVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                String referralCode = etReferralCode.getEditText().getText().toString().trim().toUpperCase();
                AppClient.getInstance().referralVerify(referralCode, new ResultCallback<Void>() {
                    @Override
                    public void onResult(Void result) {
                        startKyc(referralCode);
                        showProgress(false);
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                        if (etReferralCode != null) {
                            etReferralCode.showErrorViewWithMsg(errorMsg);
                        }
                    }
                });
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (referralCodeDialog != null) {
                    referralCodeDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
            dialogWindow.setDimAmount(0.7f);
        }
    }

    private void startKyc(String referralCode){
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        switch (AreaUtils.getCurrentArea()) {
            case AreaUtils.AREA_SAUDI:
                mHashMaps.put(Constants.KYC_TYPE, Constants.KYC_REG);
                ActivitySkipUtil.startAnotherActivity((Activity) mContext,
                        KycVerifyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
        dismiss();
    }

    public void showDesc(ReferralCodeDescEntity result) {
        if (result == null || (result.useReferralCodeDesc.length == 0 &&
                result.amountAndCurrencyInfo.length == 0)) {
            tvCodeContent.setText("");
            return;
        }
        String[] desc = checkArray(result.useReferralCodeDesc),
                amount = checkArray(result.amountAndCurrencyInfo);
        tvCodeContent.setText(Spans.builder()
                .text(desc[0]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_6b6c73)))
                .text(amount[0]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"))
                .text(desc[1]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_6b6c73)))
                .text(amount[1]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"))
                .text(desc[2]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_6b6c73)))
                .build());
    }

    private String[] checkArray(String[] useReferralCodeDesc) {
        String[] temp;
        if (useReferralCodeDesc.length == 0) {
            temp = new String[]{" ", " ", " "};
        } else {
            temp = new String[3];
            for (int i = 0; i < 3; i++) {
                if (i < useReferralCodeDesc.length) {
                    temp[i] = isEmpty(useReferralCodeDesc[i]);
                } else {
                    temp[i] = " ";
                }
            }
        }
        return temp;
    }

    public String isEmpty(String s) {
        return TextUtils.isEmpty(s) ? " " : s;
    }
}
