package cn.swiftpass.wallet.tiqmo.support.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.google.zxing.Result;

/**
 * 异步解析二维码task
 */
public class ParseQrCodeAysnTask extends AsyncTask<Void, Void, String> {

    //二维码解析回调
    public interface onParseResultListener {
        void onResult(String res);
    }

    //回调
    private onParseResultListener mListener;

    //图片路径
    private String mPath;

    //图片
    private Bitmap mBitmap;

    public ParseQrCodeAysnTask(String path, onParseResultListener listener) {
        mPath = path;
        mListener = listener;
    }

    public ParseQrCodeAysnTask(Bitmap bitmap, onParseResultListener listener) {
        mBitmap = bitmap;
        mListener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        //处理二维码
        if (null == mBitmap && TextUtils.isEmpty(mPath)) {
            return null;
        }
        String res = null;
        try {
            Result result = QrcodePictureUtil.parseQRcodeBitmap(mBitmap);
            if (result != null) {
                res = result.getText();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    protected void onPostExecute(final String res) {
        if (null != mListener) {
            mListener.onResult(res);
        }
    }
}
