package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class NotificationCenterAdapter extends BaseRecyclerAdapter<MsgEntity> {

    public NotificationCenterAdapter(@Nullable List<MsgEntity> data) {
        super(R.layout.item_notification_center, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, MsgEntity msgEntity, int position) {
        String msgSubject = msgEntity.msgSubject;
        String msgTitle = msgEntity.inAppTitle;
        String msgFromNowTimeGap = msgEntity.msgFromNowTimeGap;

        if ("REQUEST_TRANSFER_REJECTED".equals(msgSubject) ||
                "SPLIT_BILL_REJECT".equals(msgSubject)) {
            baseViewHolder.setGone(R.id.iv_check, false);
        } else {
            baseViewHolder.setGone(R.id.iv_check, true);
        }

        if ("SPLIT_BILL_REJECT".equals(msgSubject)) {
            if (LocaleUtils.isRTL(mContext)) {
                msgTitle = "\u202B" + msgTitle;
            }
            baseViewHolder.setText(R.id.tv_notification, msgTitle);
        } else {
            baseViewHolder.setText(R.id.tv_notification, msgTitle);
        }
        baseViewHolder.setText(R.id.tv_time, msgFromNowTimeGap);
        baseViewHolder.setImageResource(R.id.iv_notification, AndroidUtils.getNotifyDrawable(mContext, msgSubject));
    }
}
