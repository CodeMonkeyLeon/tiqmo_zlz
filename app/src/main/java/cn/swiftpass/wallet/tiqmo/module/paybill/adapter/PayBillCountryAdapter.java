package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PayBillCountryAdapter extends BaseRecyclerAdapter<PayBillCountryEntity> {

    public PayBillCountryAdapter(@Nullable List<PayBillCountryEntity> data) {
        super(R.layout.item_paybill_country, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillCountryEntity payBillCountryEntity, int position) {
        String type = getSectionForPosition(position);
        if (TextUtils.isEmpty(type)) {
            return;
        }

        if (payBillCountryEntity != null) {
            String countryName = payBillCountryEntity.countryName;
            baseViewHolder.setText(R.id.tv_country_name, countryName);

            String areaCode = payBillCountryEntity.countryAreaCode;
            baseViewHolder.setText(R.id.tv_area_code, areaCode);

            RoundedImageView ivCountry = baseViewHolder.getView(R.id.iv_country);
            String countryLogo = payBillCountryEntity.countryLogo;
            Glide.with(mContext)
                    .load(countryLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivCountry);

            baseViewHolder.setGone(R.id.tv_country_type, false);
            if (position == getPositionForSection(type)) {
                baseViewHolder.setGone(R.id.tv_country_type, false);
                baseViewHolder.setText(R.id.tv_country_type, payBillCountryEntity.countryType);
                baseViewHolder.setTextColor(R.id.tv_country_type, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_list_title_text_color)));
            } else {
                baseViewHolder.setGone(R.id.tv_country_type, true);
            }
        }
    }

    /**
     * 根据当前位置获取时间值
     */
    public String getSectionForPosition(int position) {
        PayBillCountryEntity payBillCountryEntity = getItem(position);
        if (payBillCountryEntity != null) {
            return payBillCountryEntity.countryType;
        }
        return "";
    }

    public int getPositionForSection(String date) {
        for (int i = 0; i < getItemCount(); i++) {
            String dateStr = mDataList.get(i).countryType;
            if (!TextUtils.isEmpty(dateStr) && dateStr.equals(date)) {
                return i;
            }
        }
        return -1;
    }
}
