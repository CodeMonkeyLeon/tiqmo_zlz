package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 环形图表信息
 */
public class AnalysisGraphicEntity extends BaseEntity {
    //总支出金额
    public String totalSpendingAmount;
    //支出币种
    public String totalSpendingCategoryCurrency;
    //平均支出金额
    public String avgSpendingAmount;
    //平均支出的币种
    public String avgSpendingCategoryCurrency;
    //折线图数据
    public LineChartDataEntity lineChartData;
    //环色占比
    public List<AnalysisGraphicColorEntity> itemInfo = new ArrayList<>();
}
