package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class HelpOneEntity extends BaseEntity {
    public String id;
    public String parentId;
    public String title;
    public String content;
    public String nodeLevel;
    public List<HelpTwoEntity> children = new ArrayList<>();
}
