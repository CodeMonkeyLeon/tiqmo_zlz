package cn.swiftpass.wallet.tiqmo.sdk.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RegisterFinishEntity extends BaseEntity {
    public String partnerNo;
    public String openId;
    public String partnerUserId;
    public String userId;
}
