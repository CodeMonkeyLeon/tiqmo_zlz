package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.ChooseProductEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.ChooseProductListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class RechargeProductListAdapter extends BaseRecyclerAdapter<ChooseProductListEntity> {

    public RechargeProductListAdapter(@Nullable List<ChooseProductListEntity> data) {
        super(R.layout.item_recharge_product_list,data);
    }

    private ProductClickListener mProductClickListener;

    public void setProductClickListener(final ProductClickListener productClickListener) {
        this.mProductClickListener = productClickListener;
    }

    public interface ProductClickListener {
        void clickProduct(ChooseProductEntity chooseProductEntity);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, ChooseProductListEntity chooseProductListEntity, int position) {
        baseViewHolder.setText(R.id.tv_product_title,chooseProductListEntity.title);
        String value = chooseProductListEntity.value;
        RecyclerView rlProductList = baseViewHolder.getView(R.id.rl_product_list);
        LinearLayout llProductTitle = baseViewHolder.getView(R.id.ll_product_title);
        ImageView ivProductArrow = baseViewHolder.getView(R.id.iv_product_arrow);
        LocaleUtils.viewRotationY(mContext,ivProductArrow);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        manager.setOrientation(RecyclerView.VERTICAL);
        rlProductList.addItemDecoration(myItemDecoration);
        rlProductList.setLayoutManager(manager);

        List<ChooseProductEntity> productList = chooseProductListEntity.productList;
        RechargeProductAdapter rechargeProductAdapter = new RechargeProductAdapter(productList);
        rechargeProductAdapter.bindToRecyclerView(rlProductList);
        rechargeProductAdapter.setDataList(productList);
        rechargeProductAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                ChooseProductEntity chooseProductEntity = productList.get(position);
                if(mProductClickListener != null){
                    mProductClickListener.clickProduct(chooseProductEntity);
                }
            }
        });

        if("1".equals(value)){
            ivProductArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_down_arrow));
            rlProductList.setVisibility(View.VISIBLE);
        }else{
            ivProductArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_right_arrow));
            rlProductList.setVisibility(View.GONE);
        }

        llProductTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productList.size() == 0) {
                    return;
                }
                if("1".equals(chooseProductListEntity.value)) {
                    chooseProductListEntity.value = "0";
                    rlProductList.setVisibility(View.GONE);
                    ivProductArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_right_arrow));
                }else{
                    chooseProductListEntity.value = "1";
                    rlProductList.setVisibility(View.VISIBLE);
                    ivProductArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_down_arrow));
                }
            }
        });
    }
}
