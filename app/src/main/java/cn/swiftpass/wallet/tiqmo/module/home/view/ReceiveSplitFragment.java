package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SendSplitAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.SplitBillContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.SplitPresenter;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.NoBalanceDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class ReceiveSplitFragment extends BaseFragment<SplitBillContract.SplitPresenter> implements SplitBillContract.SplitView {
    @BindView(R.id.ll_no_send)
    LinearLayout llNoSend;
    @BindView(R.id.tv_split_1)
    TextView tvSplit1;
    @BindView(R.id.rl_send_split)
    RecyclerView rlSendSplit;

    private SendSplitAdapter adapter;
    private SplitListEntity entity;
    private List<SplitListEntity.SplitDetail> details = new ArrayList<>();
    private BottomDialog payDialog;
    public static final int receiveCode = 1001;
    public static final int rejectCode = 101;
    public static final int payCode = 102;
    private String sendType;
    private SendReceiveSplitDetailEntity detailEntity;

    private NoBalanceDialog noBalanceDialog;
    private RiskControlEntity riskControlEntity;
    private static ReceiveSplitFragment fragment;
    private BottomDialog bottomDialog;

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_send_split;
    }

    public static ReceiveSplitFragment getInstance(){
        if (fragment == null) {
           fragment = new ReceiveSplitFragment();
        }
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void initView(View parentView) {

        mPresenter.getSplit(Constants.receive);
    }

    private void showNoBalanceDialog(){
        noBalanceDialog = new NoBalanceDialog(mContext);
        noBalanceDialog.showWithBottomAnim();
    }


    @Override
    public SplitBillContract.SplitPresenter getPresenter() {
        return new SplitPresenter();
    }

    @Override
    public void ReceiveSplitBillDetailSuccess(RequestCenterEntity requestCenterEntity) {
        if (requestCenterEntity != null) {
            showPayDetailDialog(requestCenterEntity);
        }
    }

    @Override
    public void getSplitSuccess(SplitListEntity entity) {
        if (entity.getSplitBillSentReceivedRecordDtos() != null) {
            details = entity.getSplitBillSentReceivedRecordDtos();
            rlSendSplit.setVisibility(View.VISIBLE);
            initRecycleView();
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        if (transferLimitEntity != null) {
            HashMap<String, Object> limitMap = new HashMap<>();
            limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
            ActivitySkipUtil.startAnotherActivity(mActivity, CreateSplitBillActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getSplitDetailSuccess(SendReceiveSplitDetailEntity detailEntity) {
        if (detailEntity != null) {
            Intent intent = new Intent(mActivity,SendReceiveSplitDetailActivity.class);
            intent.putExtra("isReceive",true);
            this.detailEntity = detailEntity;
            intent.putExtra(Constants.SEND_RECEIVE_SPLIT_DETAIL, this.detailEntity);
            startActivityForResult(intent, receiveCode);
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == receiveCode) {
            switch (resultCode){
                //拒绝
                case rejectCode:
                    showTipDialog(mContext.getString(R.string.sprint17_42));
                    break;
                case payCode:
//                       mPresenter.ReceiveSplitBillDetail(detailEntity.getOrderNo());
                    break;
            }
        }
    }

    private void initRecycleView() {
        rlSendSplit.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        adapter = new SendSplitAdapter(details);
        adapter.bindToRecyclerView(rlSendSplit);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                String originalOrderNo = details.get(position).getOriginalOrderNo();
                String orderRelevancy = details.get(position).getOrderRelevancy();
                mPresenter.getSplitDetail(originalOrderNo,orderRelevancy,Constants.receive);
            }
        });
    }


    private void showPayDetailDialog(RequestCenterEntity requestCenterEntity) {
        payDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirm_transaction, null);
        TextView tvConfirm = contentView.findViewById(R.id.tv_confirm);
        TextView tvCancel = contentView.findViewById(R.id.tv_cancel);
        TextView tvMoney = contentView.findViewById(R.id.tv_money);
        TextView tvNote = contentView.findViewById(R.id.tv_note);
        TextView tvName = contentView.findViewById(R.id.tv_name);
        TextView tvPhone = contentView.findViewById(R.id.tv_phone);
        LottieAnimationView ivSend = contentView.findViewById(R.id.iv_send);
        TextView tvError = contentView.findViewById(R.id.tv_error);
        ConstraintLayout con_note = contentView.findViewById(R.id.con_note);
        RoundedImageView ivFromAvatar = contentView.findViewById(R.id.iv_from_avatar);
        RoundedImageView ivToAvatar = contentView.findViewById(R.id.iv_to_avatar);
        LocaleUtils.viewRotationY(mContext, ivSend);

        String requestIcon = requestCenterEntity.requestIcon;
        String receiveIcon = requestCenterEntity.receiveIcon;
        String requestPhone = requestCenterEntity.requestPhone;
        String receivePhone = requestCenterEntity.receivePhone;
        String requestUserName = requestCenterEntity.requestUserName;
        String receiveUserName = requestCenterEntity.receiveUserName;
        String requestSex = requestCenterEntity.requestSex;
        String receiveSex = requestCenterEntity.receiveSex;
        String transferAmount = requestCenterEntity.transferAmount;
        String note = requestCenterEntity.transferRemark;
        String currencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferAmount = AndroidUtils.getTransferMoney(transferAmount);
        if (!TextUtils.isEmpty(transferAmount)) {
            tvMoney.setText(Spans.builder()
                    .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                    .build());
        }

        tvName.setText(AndroidUtils.getContactName(requestUserName));
        tvPhone.setText(AndroidUtils.getPhone(requestPhone));
        if (!TextUtils.isEmpty(note)) {
            con_note.setVisibility(View.VISIBLE);
            tvNote.setText(note);
        } else {
            con_note.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(receiveIcon)) {
            Glide.with(mContext).clear(ivFromAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(receiveIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, receiveSex))
                    .into(ivFromAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, receiveSex)).into(ivFromAvatar);
        }

        if (!TextUtils.isEmpty(requestIcon)) {
            Glide.with(mContext).clear(ivToAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(requestIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, requestSex))
                    .into(ivToAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, requestSex)).into(ivToAvatar);
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendType = "TRANSFER";
                NotifyEntity notifyEntity = new NotifyEntity();
                notifyEntity.msgSubject = "TRANSFER";
                notifyEntity.msgResult = "CONFIRM";
                notifyEntity.orderNo = requestCenterEntity.orderNo;
                List<NotifyEntity> receivers = new ArrayList<>();
                receivers.add(notifyEntity);
                mPresenter.sendNotify(receivers);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payDialog != null) {
                    payDialog.dismiss();
                }
            }
        });

        payDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        payDialog.setContentView(contentView);
        Window dialogWindow = payDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        payDialog.showWithBottomAnim();
    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {
        if ("REJECT".equals(sendType)) {
            showTipDialog(errorMsg);
        }else {

            if ("010107".equals(errorCode)) {
                //余额不足
                showNoBalanceDialog();
                if (payDialog != null) {
                    payDialog.dismiss();
                }
            } else if ("030204".equals(errorCode) || "030207".equals(errorCode)) {
                showExcessBeneficiaryDialog(errorMsg);
            } else {
                showTipDialog(errorMsg);
            }
        }
    }

    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
//                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity payBillOrderInfoEntity) {
        if(payDialog != null){
            payDialog.dismiss();
        }
            if (payBillOrderInfoEntity == null) return;
            riskControlEntity = payBillOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(mActivity, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                } else if (riskControlEntity.isNeedIvr()) {
                    jumpToPwd(true);

                }

            }
        }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        TransferEntity transferEntity = new TransferEntity();
        transferEntity.orderNo = detailEntity.getOrderNo();
        transferEntity.orderAmount = detailEntity.getMyReceiveAmount();
        transferEntity.orderCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferEntity.remark = "vh";
        transferEntity.payMethod = "BAL";
        transferEntity.exchangeRate = "1.000000";
        mHashMap.put(Constants.TRANSFER_ENTITY, transferEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_RECEIVE_SPLIT_BILL);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_RECEIVE_PAY);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.splitbill_13));

    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(mActivity, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(mActivity, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }


    @OnClick({R.id.tv_split_1})
    public void onClickView(View view){
        switch (view.getId()){
            case R.id.tv_split_1:
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
        }
    }

    @Override
    public void showError(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {
        mPresenter.getSplit(Constants.receive);
    }
}
