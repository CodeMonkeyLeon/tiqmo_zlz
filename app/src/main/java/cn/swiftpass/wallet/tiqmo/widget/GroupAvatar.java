//package cn.swiftpass.wallet.tiqmo.widget;
//
//import android.content.Context;
//import android.text.TextUtils;
//import android.util.AttributeSet;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.constraintlayout.widget.ConstraintLayout;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.DecodeFormat;
//import com.makeramen.roundedimageview.RoundedImageView;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import cn.swiftpass.wallet.tiqmo.R;
//import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
//import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
//
//
//public class GroupAvatar extends RelativeLayout {
//
//    private List<String> avatars = new ArrayList<>();
//    private LinearLayout llLineAvatar1;
//    private LinearLayout llLineAvatar2;
//    private ConstraintLayout clAvatarMore;
//    private RoundedImageView imageView11;
//    private RoundedImageView imageView12;
//    private RoundedImageView imageView21;
//    private RoundedImageView imageView22;
//    private TextView textView;
//
//
//    public GroupAvatar(Context context) {
//        super(context);
//    }
//
//    public GroupAvatar(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public GroupAvatar(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        View view = View.inflate(context, R.layout.group_avatar,this);
//        initView(view ,context);
//    }
//
//    private void initView(View view, Context context) {
//        llLineAvatar1 = view.findViewById(R.id.ll_avatar_line_1);
//        llLineAvatar2 = view.findViewById(R.id.ll_avatar_line_2);
//        clAvatarMore = view.findViewById(R.id.cl_avatar_more);
//        imageView11 = view.findViewById(R.id.avatar_line1_1);
//        imageView12 = view.findViewById(R.id.avatar_line1_2);
//        imageView21 = view.findViewById(R.id.avatar_line2_1);
//        imageView22 = view.findViewById(R.id.avatar_line2_2);
//        textView = view.findViewById(R.id.tv_avatar_more);
//
//
//    }
//
//    private void loadAvatar(int size,Context context){
//        for (int i = 0; i < size; i++) {
//            String avatar = avatars.get(i);
//            if (i==0) {
//                if (!TextUtils.isEmpty(avatar)) {
//                    Glide.with(this).clear(imageView11);
//                    int round = (int) AndroidUtils.dip2px(context, 8);
//                    Glide.with(this)
//                            .load(avatar)
//                            .dontAnimate()
//                            .format(DecodeFormat.PREFER_RGB_565)
//                            .placeholder(ThemeSourceUtils.getDefAvatar(context, ""))
//                            .into(imageView11);
//                } else {
//                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(context, "")).into(imageView11);
//                }
//            }else if (i == 1){
//                if (!TextUtils.isEmpty(avatar)) {
//                    Glide.with(this).clear(imageView12);
//                    int round = (int) AndroidUtils.dip2px(context, 8);
//                    Glide.with(this)
//                            .load(avatar)
//                            .dontAnimate()
//                            .format(DecodeFormat.PREFER_RGB_565)
//                            .placeholder(ThemeSourceUtils.getDefAvatar(context, ""))
//                            .into(imageView12);
//                } else {
//                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(context, "")).into(imageView12);
//                }
//            }else if (i == 2){
//                if (!TextUtils.isEmpty(avatar)) {
//                    Glide.with(this).clear(imageView21);
//                    int round = (int) AndroidUtils.dip2px(context, 8);
//                    Glide.with(this)
//                            .load(avatar)
//                            .dontAnimate()
//                            .format(DecodeFormat.PREFER_RGB_565)
//                            .placeholder(ThemeSourceUtils.getDefAvatar(context, ""))
//                            .into(imageView21);
//                } else {
//                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(context, "")).into(imageView21);
//                }
//            } else if (i == 3){
//                if (!TextUtils.isEmpty(avatar)) {
//                    Glide.with(this).clear(imageView22);
//                    int round = (int) AndroidUtils.dip2px(context, 8);
//                    Glide.with(this)
//                            .load(avatar)
//                            .dontAnimate()
//                            .format(DecodeFormat.PREFER_RGB_565)
//                            .placeholder(ThemeSourceUtils.getDefAvatar(context, ""))
//                            .into(imageView22);
//                } else {
//                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(context, "")).into(imageView22);
//                }
//            }
//        }
//    }
//
//    public void SetAvatars(List<String> avatars, Context context){
//        this.avatars = avatars;
////        LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) llLineAvatar2.getLayoutParams();
////        int width = llLineAvatar2.getMeasuredWidth();
////        imageView21.setAdjustViewBounds(true);
////        imageView21.setMaxWidth(width/2);
////
////
////        clAvatarMore.setMaxWidth(width/2);
//
//        int size = avatars.size();
//        if (size == 2) {
//            llLineAvatar2.setVisibility(GONE);
//        }else if (size == 3){
//            imageView22.setVisibility(View.GONE);
//            clAvatarMore.setVisibility(GONE);
//        }else if (size == 4){
//            clAvatarMore.setVisibility(GONE);
//        }else{
//            textView.setText(size-4+"");
//            imageView22.setVisibility(GONE);
//        }
//        loadAvatar(size,context);
//    }
//}
