package cn.swiftpass.wallet.tiqmo.module.register.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterFinishEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class RegisterUserDetailContract {
    public interface View extends BaseView<Presenter> {
        void finishRegSuccess(RegisterFinishEntity response);
        void finishRegFail(String errorCode,String errorMsg);

        void loginAccountSuccess(UserInfoEntity response);

        void loginAccountError(String errorCode, String errorMsg);

        void forgetSetPwdSuccess(Void response);
        void forgetSetPwdFail(String errorCode,String errorMsg);

        void changePwdSuccess(UserInfoEntity response);
        void changePwdFail(String errorCode,String errorMsg);

        void preCheckPdSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity);
        void preCheckPdFail(String errorCode, String errorMsg);
    }


    public interface Presenter extends BasePresenter<View> {
        void loginAccount(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude);

        void finishRegister(String fullName, String callingCode, String phone, String password, String authType,
                            double longitude, double latitude,String noStr,String referralCode,
                            String shareLink);

        void forgetSetPwd(String callingCode, String phone, String password);

        void preCheckPd(String passwordType,
                        String operatorType,
                        String oldPassword);

        void changePwd(String pdType, String password, String oldPassword, String operatorType, String sceneType, String referralCode);
    }
}
