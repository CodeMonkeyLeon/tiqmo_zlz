package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillRefundListPresenter implements PayBillContract.BillRefundPresenter {

    private PayBillContract.BillRefundView billRefundView;

    @Override
    public void getNewPayBillIOList(String billerId, String sku, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillServiceIOList(billerId, sku, channelCode, new LifecycleMVPResultCallback<PayBillIOListEntity>(billRefundView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                if (billRefundView != null) {
                    billRefundView.getNewPayBillIOListSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (billRefundView != null) {
                    billRefundView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getPayBillIOList(String billerId, String sku) {
        AppClient.getInstance().getTransferManager().getPayBillIOList(billerId, sku, new LifecycleMVPResultCallback<PayBillIOListEntity>(billRefundView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                billRefundView.getPayBillIOListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billRefundView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getPayBillServiceDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data,String paymentType) {
        AppClient.getInstance().getTransferManager().getPayBillServiceDetail(aliasName, billerId, sku, channelCode, data,paymentType, new LifecycleMVPResultCallback<PayBillDetailEntity>(billRefundView, true) {
            @Override
            protected void onSuccess(PayBillDetailEntity result) {
                billRefundView.getPayBillDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billRefundView.getPayBillDetailFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(billRefundView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                billRefundView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billRefundView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillRefundView billRefundView) {
        this.billRefundView = billRefundView;
    }

    @Override
    public void detachView() {
        this.billRefundView = null;
    }

    @Override
    public void getPayBillRefundList(String billerId,String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillRefundList(billerId,channelCode, new LifecycleMVPResultCallback<PayBillRefundListEntity>(billRefundView,true) {
            @Override
            protected void onSuccess(PayBillRefundListEntity result) {
                billRefundView.getPayBillRefundListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billRefundView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getPayBillRefundDetail(String channelCode, String id) {
        AppClient.getInstance().getTransferManager().getPayBillRefundDetail(channelCode, id, new LifecycleMVPResultCallback<PayBillRefundEntity>(billRefundView,true) {
            @Override
            protected void onSuccess(PayBillRefundEntity result) {
                billRefundView.getPayBillRefundDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billRefundView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }
}
