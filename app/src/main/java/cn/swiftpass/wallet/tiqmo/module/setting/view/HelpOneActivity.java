package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.HelpOneAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.HelpContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpOneEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.HelpPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class HelpOneActivity extends BaseCompatActivity<HelpContract.HelpListPresenter> implements HelpContract.HelpListView{
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.ry_help)
    RecyclerView ryHelp;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private HelpOneAdapter helpOneAdapter;

    private List<HelpOneEntity> helpOneList = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_how_to_help;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.Help_support_1);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryHelp.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryHelp.setLayoutManager(manager);
        helpOneAdapter = new HelpOneAdapter(helpOneList);
        helpOneAdapter.bindToRecyclerView(ryHelp);

        mPresenter.getHelpContentList("");
    }

    @OnClick(R.id.iv_back)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:break;
        }
    }

    @Override
    public void getHelpContentListSuccess(HelpListEntity helpListEntity) {
        helpOneList.clear();
        if(helpListEntity != null){
            helpOneList.addAll(helpListEntity.tress);
            helpOneAdapter.setDataList(helpOneList);
        }
    }

    @Override
    public void submitHelpQuestionSuccess(Void result) {

    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public HelpContract.HelpListPresenter getPresenter() {
        return new HelpPresenter();
    }
}
