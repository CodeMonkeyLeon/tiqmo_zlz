package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SendReceiveTransferDetail extends BaseEntity {


    private String customerId;
    private String phone;
    private String firstName;
    private String headIcon;
    private String userId;
    private List<TransUserInfoList> transUserInfoList;

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(String headIcon) {
        this.headIcon = headIcon;
    }

    public List<TransUserInfoList> getTransUserInfoList() {
        return transUserInfoList;
    }

    public void setTransUserInfoList(List<TransUserInfoList> transUserInfoList) {
        this.transUserInfoList = transUserInfoList;
    }

    public static class TransUserInfoList extends BaseEntity {
        private String transUserCustomerId;
        private String transUserPhone;
        private String transName;
        private String transUserHeadIcon;
        private String transUserId;

        public String getTransUserId() {
            return this.transUserId;
        }

        public void setTransUserId(final String transUserId) {
            this.transUserId = transUserId;
        }

        public String getTransUserCustomerId() {
            return transUserCustomerId;
        }

        public void setTransUserCustomerId(String transUserCustomerId) {
            this.transUserCustomerId = transUserCustomerId;
        }

        public String getTransUserPhone() {
            return transUserPhone;
        }

        public void setTransUserPhone(String transUserPhone) {
            this.transUserPhone = transUserPhone;
        }

        public String getTransName() {
            return transName;
        }

        public void setTransName(String transName) {
            this.transName = transName;
        }

        public String getTransUserHeadIcon() {
            return transUserHeadIcon;
        }

        public void setTransUserHeadIcon(String transUserHeadIcon) {
            this.transUserHeadIcon = transUserHeadIcon;
        }
    }
}
