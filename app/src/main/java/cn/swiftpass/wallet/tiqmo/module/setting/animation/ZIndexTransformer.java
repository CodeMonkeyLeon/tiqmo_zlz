package cn.swiftpass.wallet.tiqmo.module.setting.animation;

import cn.swiftpass.wallet.tiqmo.module.setting.animation.entry.CardItem;

public interface ZIndexTransformer {
    void transformAnimation(CardItem card, float fraction, int cardWidth, int cardHeight, int
            fromPosition, int toPosition);

    void transformInterpolatedAnimation(CardItem card, float fraction, int cardWidth, int
            cardHeight, int fromPosition, int toPosition);
}
