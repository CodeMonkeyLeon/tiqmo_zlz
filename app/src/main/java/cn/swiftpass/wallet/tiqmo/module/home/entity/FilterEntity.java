package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class FilterEntity extends BaseEntity {

    public String startDate = "";
    public String showStartDate = "";
    public String showEndDate = "";
    public String endDate = "";
    public Date fromDate;
    public Date toDate;
    public List<String> paymentType = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public HashMap<Integer, String> selectedArray = new HashMap<>();
//    public SparseArray<String> selectedArray = new SparseArray<>();
}
