package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMoreInfoAdapter;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.ImrSendMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrMoreInfoPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class ImrMoreInfoActivity extends BaseCompatActivity<ImrSendMoneyContract.MoreInfoPresenter> implements ImrSendMoneyContract.MoreInfoView{
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_beneficiary_desc)
    TextView tvBeneficiaryDesc;
    @BindView(R.id.rv_info_list)
    RecyclerView rvInfoList;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private ImrMoreInfoAdapter imrMoreInfoAdapter;
    private ImrOrderInfoEntity imrOrderInfoEntity;

    private List<ImrComplianceEntity> infoList = new ArrayList<>();
    private List<ImrComplianceEntity> sendInfoList = new ArrayList<>();
    private HashMap<Integer, String> dataMap;

    private String transferDestinationCountryCode;

    private ImrOrderInfoEntity mImrOrderInfoEntity;
    private TransferLimitEntity transferLimitEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_imr_more_info;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvTitle.setText(R.string.IMR_22);
        if (getIntent() != null && getIntent().getExtras() != null) {
            imrOrderInfoEntity = (ImrOrderInfoEntity) getIntent().getExtras().getSerializable(Constants.ImrOrderInfoEntity);
            transferDestinationCountryCode = getIntent().getExtras().getString(Constants.transferDestinationCountryCode);
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);

            if(imrOrderInfoEntity != null){
                infoList = imrOrderInfoEntity.compliance;
                dataMap = new HashMap<>();
                for(int i=0;i<infoList.size();i++) {
                    dataMap.put(i,"");
                }
            }
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 12));
        rvInfoList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvInfoList.setLayoutManager(manager);
        imrMoreInfoAdapter = new ImrMoreInfoAdapter(infoList);
        imrMoreInfoAdapter.bindToRecyclerView(rvInfoList);
        imrMoreInfoAdapter.setDataList(infoList);
    }

    @OnClick({R.id.iv_back, R.id.tv_continue})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_continue:
                if (dataMap.size() > 0) {
                    for (Integer k : dataMap.keySet()) {
                        String value = dataMap.get(k);
                        ImrComplianceEntity imrComplianceEntity = infoList.get(k);
                        imrComplianceEntity.paramValue = value;
                        sendInfoList.add(imrComplianceEntity);
                    }
                    mPresenter.sendMoreInfo(sendInfoList);
                }
                break;
            default:break;
        }
    }

    public void saveEditData(int position, String content) {
        dataMap.put(position, content);

        for (Integer k : dataMap.keySet()) {
            String value = dataMap.get(k);
            if (TextUtils.isEmpty(value)) {
                tvContinue.setEnabled(false);
                return;
            }
        }

        tvContinue.setEnabled(true);
    }

    @Override
    public void sendMoreInfoSuccess(ImrOrderInfoEntity imrOrderInfoEntity) {
        this.mImrOrderInfoEntity = imrOrderInfoEntity;
        String orderInfo = imrOrderInfoEntity.orderInfo;
        if (!TextUtils.isEmpty(orderInfo)) {
            mPresenter.checkOut("", orderInfo);
        }
    }

    @Override
    public void sendMoreInfoFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        checkOutEntity.destinationCountry = transferDestinationCountryCode;
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.mImrOrderInfoEntity, mImrOrderInfoEntity);
        ActivitySkipUtil.startAnotherActivity(ImrMoreInfoActivity.this,
                ImrSendMoneySummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkOutFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public ImrSendMoneyContract.MoreInfoPresenter getPresenter() {
        return new ImrMoreInfoPresenter();
    }
}
