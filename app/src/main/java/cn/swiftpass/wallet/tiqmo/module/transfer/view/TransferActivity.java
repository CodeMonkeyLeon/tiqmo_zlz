package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;

public class TransferActivity extends BaseCompatActivity {

    private TransferFragment transferFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        transferFragment = new TransferFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fl_transfer, transferFragment);
        ft.commit();
    }

    @Override
    public void notifyByThemeChanged() {
        super.notifyByThemeChanged();
        if (transferFragment != null) {
            transferFragment.noticeThemeChange();
        }
    }
}
