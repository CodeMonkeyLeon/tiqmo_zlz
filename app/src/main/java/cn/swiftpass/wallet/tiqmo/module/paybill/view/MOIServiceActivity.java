package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.MOIServiceAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeIconEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.interfaces.OnMoiServiceItemClickListener;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayMoiServicePresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class MOIServiceActivity extends BaseCompatActivity<PayBillContract.MoiServicePresenter> implements PayBillContract.MoiServiceView {

    @Override
    protected int getLayoutID() {
        return R.layout.act_moi_service;
    }

    public static final String TAG = "MOIServiceActivity";
    public static final String DATA_MOI_SERVICE = "DATA_MOI_SERVICE";


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_country_list)
    RecyclerView rvCountryList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;


    private List<PayBillerEntity> mBillList = new ArrayList<>();
    private List<PayBillerEntity> filterCountryList = new ArrayList<>();


    private MOIServiceAdapter mMoiServiceAdapter;

    private String searchString;

    private StatusView typeStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private String mChannelCode = Constants.paybill_local;
    private String mCountryCode = Constants.SAU;
    private String mBillerType;

    private String billerId, billerName, billerLogo;
    private int supportRefundFlag;


    public static void startMOIServiceActivity(Activity fromActivity,
                                               PayBillTypeEntity data,
                                               String countryCode,
                                               String channelCode,
                                               String billerType) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(DATA_MOI_SERVICE, data);
        hashMap.put(Constants.CHANNEL_CODE, channelCode);
        hashMap.put(Constants.COUNTRY_CODE, countryCode);
        hashMap.put(Constants.BILLER_TYPE, billerType);
        ActivitySkipUtil.startAnotherActivity(fromActivity, MOIServiceActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    @Override
    protected void init(Bundle savedInstanceState) {
        initView();


        if (getIntent() != null) {
            PayBillTypeEntity data = (PayBillTypeEntity) getIntent().getSerializableExtra(DATA_MOI_SERVICE);
            List<PayBillTypeIconEntity> list = data.billerDescriptionList;
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);
            mCountryCode = getIntent().getStringExtra(Constants.COUNTRY_CODE);
            mBillerType = getIntent().getStringExtra(Constants.BILLER_TYPE);

            tvTitle.setText(data.billerTypeShow);

            mPresenter.getPayBillerList(mCountryCode,mBillerType,"",mChannelCode);
        }


    }


    private void initView() {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setVisibility(View.GONE);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_37));
        typeStatusView = new StatusView.Builder(mContext, rvCountryList).setNoContentMsg(getString(R.string.BillPay_37))
                .setNoContentView(noHistoryView).build();


        etSearch.setHint(getString(R.string.BillPay_11));
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvCountryList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvCountryList.setLayoutManager(manager);
        mMoiServiceAdapter = new MOIServiceAdapter(mBillList);
        mMoiServiceAdapter.bindToRecyclerView(rvCountryList);

        mMoiServiceAdapter.setOnMoiItemClickListener(new OnMoiServiceItemClickListener() {
            @Override
            public void onMoiServiceItemClick(int position, PayBillerEntity item) {
                if (item != null) {
                    billerId = item.billerId;
                    billerName = item.billerName;
                    billerLogo = item.imgUrl;
                    supportRefundFlag = item.supportRefundFlag;
                    mPresenter.getMoiServiceIOList(billerId, billerId, mChannelCode);
                }
            }
        });


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchString = s.toString();
                if (mBillList != null && mBillList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });
    }


    private void searchFilterWithAllList(String filterStr) {
        try {
            filterCountryList.clear();
            //去除空格的匹配规则
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
//                sideBar.setVisibility(View.VISIBLE);
                filterCountryList.clear();
                mMoiServiceAdapter.setDataList(mBillList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (mBillList != null && mBillList.size() > 0) {
                int size = mBillList.size();
                for (int i = 0; i < size; i++) {
                    PayBillerEntity payBillerEntity = mBillList.get(i);
                    String name = payBillerEntity.billerName;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterCountryList.add(payBillerEntity);
                    }
                }
                mMoiServiceAdapter.setDataList(filterCountryList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }


    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }


    @Override
    public PayBillContract.MoiServicePresenter getPresenter() {
        return new PayMoiServicePresenter();
    }

    @Override
    public void getPayBillerListSuccess(PayBillerListEntity payBillerListEntity) {
        mBillList.clear();
        if(payBillerListEntity != null){
            List<PayBillerEntity> billList = payBillerListEntity.billerList;
            mBillList.addAll(billList);
            mMoiServiceAdapter.setDataList(mBillList);
        }
    }

    @Override
    public void getMoiServiceIOListSuccess(PayBillIOListEntity result) {
        if (result != null) {
            LogUtils.i(TAG, "获取到MoiServiceIO数据 : " + AndroidUtils.jsonToString(result));
            PayBillMoiFetchActivity.startBillMoiServiceFetchActivity(
                    MOIServiceActivity.this,
                    result,
                    billerId,
                    billerName,
                    billerLogo,
                    mChannelCode,supportRefundFlag);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }
}