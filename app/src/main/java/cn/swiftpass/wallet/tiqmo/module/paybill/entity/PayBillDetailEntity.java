package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillDetailEntity extends BaseEntity {

    /**
     * {\"billerName\":\"NTC Postpaid\",\"billerImgUrl\":\"\",
     * \"billReferenceName\":\"Mobile Number\",\"billReferenceValue\":\"1286768689\",
     * \"payAmount\":2100,\"payCurrencyCode\":\"SAR\",\"payAmountEditable\":\"N\",
     * \"billAmount\":10000,\"billCurrencyCode\":\"NPR\",\"partnerNo\":\"96601\"}}
     */

    //账单Biller名称
    public String billerName;
    //账单参考标识名称
    public String billReferenceName;
    //账单参考标识值
    public String billReferenceValue;
    //账单结算日
    public String billDueDate;
    //付款金额
    public String payAmount;
    public String billerImgUrl;
    //付款币种
    public String payCurrencyCode;
    //付款金额是否可编辑
    public String payAmountEditable;
    //付款金额是否可编辑
    public String billAmountEditable;
    //非定额账单时不返回
    public String billAmount;
    //	账单币种
    public String billCurrencyCode;
    //是否支持部分支付，定额账单时不返回
    public String exchangeRate;
    //是否支持超额支付，定额账单时不返回
    public String partialPayAmountEnabled;
    //是否支持超期支付，定额账单时不返回
    public String pastDuePaymentAllowed;
    //账单处理日期
    public String daysToPost;
    //账单处理日期是否按工作日
    public String isPostByBusinessDays;

    //最小可支付金额，非定额账单时可用
    public String minPayableAmount;
    //最大可支付金额，非定额账单时可用
    public String maxPayableAmount;
    //是否支持超额支付，定额账单时不返回
    public String excessPayAmountEnabled;
    public String billAmountCustomLabel;
    //账单手续费实付金额
    public String billProcessingFeePayAmount;
    public String billProcessingFeeAmount;

    public List<String> invalidInputList = new ArrayList<>();


    public List<ExtendPropertiesEntity> getExtendProperties() {
        return this.extendProperties;
    }

    public PayBillDetailEntity setExtendProperties(final List<ExtendPropertiesEntity> extendProperties) {
        this.extendProperties = extendProperties;
        return this;
    }

    private List<ExtendPropertiesEntity> extendProperties;

    public boolean isPayAmountEditable(String payAmountEditable) {
        return !"N".equals(payAmountEditable);
    }

    public boolean isBillAmountEditable(String billAmountEditable) {
        return !"N".equals(billAmountEditable);
    }
}
