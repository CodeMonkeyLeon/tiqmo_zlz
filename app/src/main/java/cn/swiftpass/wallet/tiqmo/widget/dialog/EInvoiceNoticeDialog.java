package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class EInvoiceNoticeDialog extends BottomDialog {

    private Context mContext;

//    private AcceptListener acceptListener;


    private String mTitle;

    private String mContent;


    public EInvoiceNoticeDialog(Context context, String title, String content) {
        super(context);
        mContext = context;
        mTitle = title;
        mContent = content;
        initViews(mContext);
    }


    public interface AcceptListener {
        void clickAccept();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_einvoice_notice, null);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvContent = view.findViewById(R.id.tv_content_one);

        tvTitle.setText(mTitle);
        tvContent.setText(mContent);

        TextView tvOk = view.findViewById(R.id.tv_ok);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
