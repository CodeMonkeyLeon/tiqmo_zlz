package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeCountryListEntity extends BaseEntity {

    public List<RechargeCountryEntity> countryList = new ArrayList<>();
}
