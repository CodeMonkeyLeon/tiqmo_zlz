package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class DownloadFileUrlEntity extends BaseEntity {


    //S生成成功P处理中F生成失败
    public static final String PDF_CREATE_SUCCESS = "S";
    public static final String PDF_CREATE_WAITING = "P";
    public static final String PDF_CREATE_FAIL = "F";


    public String status;
    public String url;
}
