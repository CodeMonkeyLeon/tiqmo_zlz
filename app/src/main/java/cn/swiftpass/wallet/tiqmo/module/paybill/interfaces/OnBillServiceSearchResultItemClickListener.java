package cn.swiftpass.wallet.tiqmo.module.paybill.interfaces;

import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;

public interface OnBillServiceSearchResultItemClickListener {
    void onBillServiceSearchResultItemClick(int position, PayBillerEntity item);
}
