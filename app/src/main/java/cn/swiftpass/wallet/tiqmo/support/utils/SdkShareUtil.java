package cn.swiftpass.wallet.tiqmo.support.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.util.List;

public class SdkShareUtil {

    public static final String WHATS_APP = "com.whatsapp";

    public static void openWhatsApp(Context context, String text, String phoneNumber) {
        //直接跳转到对应phoneNumber whatsApp会话窗口
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://wa.me/00971521349192";
//            String url = "https://api.whatsapp.com/send?phone=" + phoneNumber + "&text=" + URLEncoder.encode(text, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    /**
     * 发短信
     */
    public static void sendSMS(Context context, String shareInfo, String phoneNumber) {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
            sendIntent.setData(Uri.parse("smsto:" + phoneNumber));
            sendIntent.putExtra("sms_body", shareInfo);
            context.startActivity(sendIntent);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    /**
     * 拨电话
     *
     * @param context
     * @param phoneNum
     */
    public static void callPhoneNumber(Context context, String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        context.startActivity(intent);
    }

    /**
     * 发email
     *
     * @param context
     */
    public static void sendEmail(Context context, String email) {
        try {
            Intent data = new Intent(Intent.ACTION_SENDTO);
            data.setData(Uri.parse("mailto:" + email));
            context.startActivity(data);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public static boolean shareWechatFriend(Context context, Uri imageUri) {
        String packageName = "com.tencent.mm";
        if (isAppInstalled(context, packageName) && imageUri != null) {
            Intent intent = new Intent();
            ComponentName cop = new ComponentName(packageName, "com.tencent.mm.ui.tools.ShareImgUI");
            intent.setComponent(cop);
            intent.setAction(Intent.ACTION_SEND);
            //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(Intent.createChooser(intent, ""));
            return true;
        } else {
            return false;
        }
    }


    /**
     * 判断APP是否安装
     *
     * @param context
     * @param packageName
     * @return
     */
    public static boolean isAppInstalled(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(packageName);
        if (intent == null) {
            return false;
        }
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return !list.isEmpty();
    }


    public static boolean shareToLine(Activity mActivity, Uri imageUri) {
        String line_package_name = "jp.naver.line.android";
        if (!isAppInstalled(mActivity, line_package_name) || imageUri == null) return false;
        try {
            ComponentName cn = new ComponentName(line_package_name, "jp.naver.line.android.activity.selectchat.SelectChatActivity");
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
            //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
            shareIntent.setType("image/*");
            shareIntent.setComponent(cn);
            mActivity.startActivity(shareIntent);
        } catch (Exception e) {
            try {
                ComponentName cn = new ComponentName(line_package_name, "jp.naver.line.android.activity.selectchat.SelectChatActivityLaunchActivity");
                Intent shareIntent = new Intent();
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
                shareIntent.setType("image/*");
                shareIntent.setComponent(cn);
                mActivity.startActivity(shareIntent);
            } catch (Exception e1) {
                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
                    intent.setType("image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setPackage(line_package_name);
                    intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    mActivity.startActivity(intent);
                } catch (Exception e2) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }

        }
        return true;
    }


    /**
     * whatsapp 分享
     *
     * @param mActivity
     * @param picture_text
     * @param imageUri
     */
    public static boolean shareToWhatsApp(Activity mActivity, String picture_text, Uri imageUri) {
        String packageName = "com.whatsapp";
        if (!isAppInstalled(mActivity, packageName) || imageUri == null) return false;
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setPackage(packageName);
        shareIntent.putExtra(Intent.EXTRA_TEXT, picture_text);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        mActivity.startActivity(Intent.createChooser(shareIntent, ""));
        return true;
    }

}
