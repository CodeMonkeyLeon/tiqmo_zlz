package cn.swiftpass.wallet.tiqmo.module.kyc.dialog;

import android.app.Activity;
import android.content.Context;

import java.util.HashMap;

import cn.swiftpass.wallet.tiqmo.base.view.Loading;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycVerifyActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.AreaUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class ReferralCodeDialogUtils {

    public static void showReferralCodeDialog(Loading loading, Context context) {
        String referralCode = SpUtils.getInstance().getReferralCode();
        startKyc(context, referralCode);
//        loading.showProgress(true);
//        AppClient.getInstance().requestRefCodeDesc("", new ResultCallback<ReferralCodeDescEntity>() {
//            @Override
//            public void onResult(ReferralCodeDescEntity response) {
//                ReferralCodeDialog.getInstance(context, response).showWithBottomAnim();
//                loading.showProgress(false);
//            }
//
//            @Override
//            public void onFailure(String errorCode, String errorMsg) {
//                ReferralCodeDialog.getInstance(context, null).showWithBottomAnim();
//                loading.showProgress(false);
//            }
//        });
    }


    public static void showReferralCodeNotSupportGooglePlayDialog(Loading loading, Context context) {
        loading.showProgress(true);
        AppClient.getInstance().requestRefCodeDesc("", new ResultCallback<ReferralCodeDescEntity>() {
            @Override
            public void onResult(ReferralCodeDescEntity response) {
                ReferralCodeDialog.getInstance(context, response).showWithBottomAnim();
                loading.showProgress(false);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                ReferralCodeDialog.getInstance(context, null).showWithBottomAnim();
                loading.showProgress(false);
            }
        });
    }


    private static void startKyc(Context mContext, String referralCode) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        switch (AreaUtils.getCurrentArea()) {
            case AreaUtils.AREA_SAUDI:
                mHashMaps.put(Constants.KYC_TYPE, Constants.KYC_REG);
                ActivitySkipUtil.startAnotherActivity((Activity) mContext,
                        KycVerifyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }

    public static void resetKeyDialog() {
//        ReferralCodeDialog.reset();
    }
}
