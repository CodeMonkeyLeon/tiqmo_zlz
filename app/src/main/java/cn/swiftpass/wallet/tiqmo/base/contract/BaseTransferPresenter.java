package cn.swiftpass.wallet.tiqmo.base.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;

public interface BaseTransferPresenter<V extends BaseView> extends BasePresenter<V> {
    void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                         String exchangeRate, String transCurrencyCode, String transFees, String vat);

    void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark,
                           String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName);

    void confirmPay(String orderNo, String paymentMethodNo, String orderType);

    void splitBill(String oldOrderNo,
                   String receivableAmount,
                   String currencyCode,
                   String orderRemark,
                   List<SplitBillPayerEntity> receiverInfoList);

    void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName,
                           String nickName, String relationshipCode, String callingCode, String phone,
                           String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace,
                           String birthDate, String sex, String cityName, String districtName,
                           String poBox, String buildingNo, String street, String idNo, String idExpiry,
                           String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                           String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,String channelPayeeId,String channelCode,String branchId);

    void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo);

    void getKsaCardPayResult(String vat, String totalAmount,
                             OpenCardReqEntity appNiOpenCardReq);

    void setKsaCardStatus(String status,String proxyCardNo, String reason,String reasonStatus);

    void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue,String proxyCardNo,String up);

    void getKsaCardDetails(String proxyCardNo);

    void activateCard(String proxyCardNo, String cardNo, String cardExpire);
}
