package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class CropImageView extends AppCompatImageView
        implements View.OnTouchListener,
        View.OnLayoutChangeListener {

    private static int MAX_CROP_SIZE = 200;

    private final float[] mMatrixValue = new float[9];
    private ScaleGestureDetector mScaleDetector;
    private GestureDetector mScrollScaleDetector;
    private Matrix mMatrix = new Matrix();
    private RectF mSpaceRectF;
    private Paint mPaint;
    private PorterDuffXfermode mXfermode;
    private float mCropRadius;
    private float mMinScale;
    private float mMaxScale;
    private boolean isZoomMode;
    private RectF mDrawableRectF;

    private int mStrokeColor;
    private float mStrokeWidth;
    private int mMaskColor;
    private float mCropPadding;
    private boolean isPreviewMode;
    private boolean isCircle;

    public CropImageView(Context context) {
        this(context, null);
    }

    public CropImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CropImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        super.setScaleType(ScaleType.MATRIX);
        super.setPadding(0, 0, 0, 0);

        setOnTouchListener(this);
        addOnLayoutChangeListener(this);

        mSpaceRectF = new RectF();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mXfermode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
        mStrokeColor = Color.WHITE;
        mMaskColor = Color.argb(96, 0, 0, 0);
        mStrokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, context.getResources().getDisplayMetrics());
        mCropPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics());
        setupGesture();
    }

    private void setupGesture() {
        mScaleDetector = new ScaleGestureDetector(getContext().getApplicationContext(), new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                Drawable drawable = getDrawable();
                if (drawable == null) {
                    return true;
                }
                float scaleFactor = detector.getScaleFactor();
                if (scaleFactor == 1) {
                    return false;
                }
                mMatrix.getValues(mMatrixValue);
                float scale = mMatrixValue[Matrix.MSCALE_X];
                if ((scaleFactor > 1 && scale >= mMaxScale) || (scaleFactor < 1 && scale <= mMinScale)) {
                    return true;
                }
                if (scaleFactor * scale > mMaxScale) {
                    scaleFactor = mMaxScale / scale;
                } else if (scaleFactor * scale < mMinScale) {
                    scaleFactor = mMinScale / scale;
                }
                mMatrix.postScale(scaleFactor, scaleFactor, detector.getFocusX(), detector.getFocusY());
                checkTrans();
                setImageMatrix(mMatrix);
                return true;
            }
        });
        mScrollScaleDetector = new GestureDetector(getContext().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                mMatrix.postTranslate(-distanceX, -distanceY);
                checkTrans();
                setImageMatrix(mMatrix);
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }
        });
    }

    private void reset() {
        mMatrix.reset();
        Drawable drawable = getDrawable();
        if (drawable != null) {
            int width = getWidth();
            int height = getHeight();
            int dw = drawable.getIntrinsicWidth();
            int dh = drawable.getIntrinsicHeight();
            if (isPreviewMode) {
                mMinScale = Math.min((float) width / dw, (float) height / dh);
                mMaxScale = mMinScale > 1 ? mMinScale * 3 : Math.max((float) width / dw, (float) height / dh) * 2;
            } else {
                mCropRadius = (float) Math.ceil(Math.min(width, height) / 2f - mStrokeWidth - mCropPadding);
                mMinScale = Math.max(mCropRadius * 2 / dw, mCropRadius * 2 / dh);
                mMaxScale = mMinScale > 1 ? mMinScale * 3 : Math.max((float) width / dw, (float) height / dh) * 2;
            }
            mMatrix.postTranslate((width - dw) / 2f, (height - dh) / 2f);
            mMatrix.postScale(mMinScale, mMinScale, width / 2f, height / 2f);
        }
        setImageMatrix(mMatrix);
    }


    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        reset();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isPreviewMode) {
            return;
        }

        int width = getWidth();
        int height = getHeight();


        mSpaceRectF.left = mCropPadding;
        mSpaceRectF.right = mSpaceRectF.left + mCropRadius * 2;
        mSpaceRectF.top = height / 2f - mCropRadius;
        mSpaceRectF.bottom = mSpaceRectF.top + mCropRadius * 2;

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mMaskColor);
        canvas.drawRect(0, 0, width, mSpaceRectF.top, mPaint);
        canvas.drawRect(0, mSpaceRectF.bottom, width, height, mPaint);
        canvas.drawRect(0, mSpaceRectF.top, mSpaceRectF.left, mSpaceRectF.bottom, mPaint);
        canvas.drawRect(mSpaceRectF.right, mSpaceRectF.top, width, mSpaceRectF.bottom, mPaint);

        int layerId = canvas.saveLayer(mSpaceRectF, null);
        canvas.drawColor(mMaskColor);
        mPaint.setXfermode(mXfermode);
        if (isCircle) {
            canvas.drawCircle(mSpaceRectF.centerX(), mSpaceRectF.centerY(), mCropRadius, mPaint);
        } else {
            canvas.drawRect(
                    mSpaceRectF.centerX() - mCropRadius,
                    mSpaceRectF.centerY() - mCropRadius,
                    mSpaceRectF.centerX() + mCropRadius,
                    mSpaceRectF.centerY() + mCropRadius,
                    mPaint);
        }
        mPaint.setXfermode(null);
        canvas.restoreToCount(layerId);

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mStrokeColor);
        mPaint.setStrokeWidth(mStrokeWidth);
        if (isCircle) {
            canvas.drawCircle(mSpaceRectF.centerX(), mSpaceRectF.centerY(), mCropRadius, mPaint);
        } else {
            canvas.drawRect(
                    mSpaceRectF.centerX() - mCropRadius,
                    mSpaceRectF.centerY() - mCropRadius,
                    mSpaceRectF.centerX() + mCropRadius,
                    mSpaceRectF.centerY() + mCropRadius,
                    mPaint);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return false;
        }
        mScaleDetector.onTouchEvent(event);
        if (event.getPointerCount() == 2) {
            isZoomMode = true;
        } else if (isZoomMode) {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    isZoomMode = false;
                    break;
            }
        } else {
            mScrollScaleDetector.onTouchEvent(event);
        }
        return true;
    }

    private void checkTrans() {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }
        if (mDrawableRectF == null) {
            mDrawableRectF = new RectF();
        }
        mDrawableRectF.set(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        mMatrix.mapRect(mDrawableRectF);
        float offsetX = 0;
        float offsetY = 0;
        float width = getWidth();
        float height = getHeight();
        float cx = width / 2f;
        float cy = height / 2f;
        float leftBound;
        float rightBound;
        float topBound;
        float bottomBound;
        if (isPreviewMode) {
            leftBound = 0;
            rightBound = width;
            topBound = 0;
            bottomBound = height;
        } else {
            leftBound = cx - mCropRadius;
            rightBound = cx + mCropRadius;
            topBound = cy - mCropRadius;
            bottomBound = cy + mCropRadius;
        }
        if (mDrawableRectF.width() > rightBound - leftBound) {
            if (mDrawableRectF.left > leftBound) {
                offsetX = leftBound - mDrawableRectF.left;
            } else if (mDrawableRectF.right < rightBound) {
                offsetX = rightBound - mDrawableRectF.right;
            }
        } else {
            if (mDrawableRectF.left < leftBound) {
                offsetX = leftBound - mDrawableRectF.left;
            } else if (mDrawableRectF.right > rightBound) {
                offsetX = rightBound - mDrawableRectF.right;
            }
        }
        if (mDrawableRectF.height() > bottomBound - topBound) {
            if (mDrawableRectF.top > topBound) {
                offsetY = topBound - mDrawableRectF.top;
            } else if (mDrawableRectF.bottom < bottomBound) {
                offsetY = bottomBound - mDrawableRectF.bottom;
            }
        } else {
            if (mDrawableRectF.top < topBound) {
                offsetY = topBound - mDrawableRectF.top;
            } else if (mDrawableRectF.bottom > bottomBound) {
                offsetY = bottomBound - mDrawableRectF.bottom;
            }
        }
        mMatrix.postTranslate(offsetX, offsetY);
    }

    @Override
    public void setScaleType(ScaleType scaleType) {

    }

    @Override
    public ScaleType getScaleType() {
        return ScaleType.CENTER_INSIDE;
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {

    }

    @Override
    public void setPaddingRelative(int start, int top, int end, int bottom) {

    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        reset();
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        reset();
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        reset();
    }

    public void setCircle(boolean circle) {
        isCircle = circle;
    }

    public void setEnablePreviewMode(boolean isEnable) {
        if (isPreviewMode == isEnable) {
            return;
        }
        isPreviewMode = isEnable;
        reset();
    }

    public void setStrokeWidth(float strokeWidth) {
        mStrokeWidth = strokeWidth;
        reset();
    }

    public void setCropPadding(float cropPadding) {
        mCropPadding = cropPadding;
        reset();
    }

    public void setStrokeColor(int strokeColor) {
        mStrokeColor = strokeColor;
        invalidate();

    }

    public void setMaskColor(int maskColor) {
        mMaskColor = maskColor;
        invalidate();
    }

    public Bitmap crop() {
        if (mCropRadius == 0 || isPreviewMode) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
        super.draw(new Canvas(bitmap));
        MAX_CROP_SIZE = (int) (mCropRadius- AndroidUtils.dip2px(ProjectApp.getContext(),2));//减去2dp 避免把边框也绘制上去
        Bitmap crop = Bitmap.createBitmap(MAX_CROP_SIZE, MAX_CROP_SIZE, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(crop);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        if (isCircle) {
            canvas.drawCircle(MAX_CROP_SIZE / 2f, MAX_CROP_SIZE / 2f, MAX_CROP_SIZE / 2f, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        }
        Rect src = new Rect((int) mSpaceRectF.left, (int) mSpaceRectF.top, (int) mSpaceRectF.right, (int) mSpaceRectF.bottom);
        canvas.drawBitmap(bitmap, src, new Rect(0, 0, MAX_CROP_SIZE, MAX_CROP_SIZE), paint);
        bitmap.recycle();
        return crop;
    }
}