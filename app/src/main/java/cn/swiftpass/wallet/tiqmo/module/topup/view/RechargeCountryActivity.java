package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.adapter.RechargeCountryAdapter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeContract;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeCountryPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class RechargeCountryActivity extends BaseCompatActivity<RechargeContract.RechargeCountryPresenter> implements RechargeContract.RechargeCountryView{
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_country_list)
    RecyclerView rvCountryList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_no_result_found)
    LinearLayout llNoResultFound;
//    @BindView(R.id.sc_content)
//    NestedScrollView scContent;

    private RechargeCountryAdapter rechargeCountryAdapter;

    private List<RechargeCountryEntity> showCountryList = new ArrayList<>();
    private List<RechargeCountryEntity> filterCountryList = new ArrayList<>();

    private String searchString;

    private StatusView typeStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;
    private RechargeOrderListEntity rechargeOrderListEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_select_country;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_2);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if (getIntent().getExtras() != null) {
            rechargeOrderListEntity = (RechargeOrderListEntity) getIntent().getExtras().getSerializable(Constants.rechargeOrderListEntity);
        }
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setVisibility(View.GONE);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_37));
        typeStatusView = new StatusView.Builder(mContext, rvCountryList).setNoContentMsg(getString(R.string.BillPay_37))
                .setNoContentView(noHistoryView).build();

        etSearch.setHint(getString(R.string.BillPay_3));
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvCountryList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvCountryList.setLayoutManager(manager);
        rechargeCountryAdapter = new RechargeCountryAdapter(showCountryList);
        rechargeCountryAdapter.bindToRecyclerView(rvCountryList);

        rechargeCountryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                RechargeCountryEntity rechargeCountryEntity = rechargeCountryAdapter.getDataList().get(position);
                if (rechargeCountryEntity != null) {
                    HashMap<String, Object> mHashMap = new HashMap<>();
                    String isDefault = rechargeCountryEntity.isdefault;
                    if("1".equals(isDefault)){
                        mHashMap.put(Constants.rechargeOrderListEntity, rechargeOrderListEntity);
                        ActivitySkipUtil.startAnotherActivity(RechargeCountryActivity.this, RechargeOperatorActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }else{
                        mHashMap.put(Constants.rechargeCountryEntity, rechargeCountryEntity);
                        ActivitySkipUtil.startAnotherActivity(RechargeCountryActivity.this, RechargeContactActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etSearch.hideErrorView();
                searchString = s.toString();
                if (showCountryList != null && showCountryList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });

        mPresenter.getRechargeCountry();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String s = etSearch.getEditText().getText().toString();
        if(!TextUtils.isEmpty(s)){
            AndroidUtils.showKeyboard(this,etSearch.getEditText());
        }
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            llNoResultFound.setVisibility(View.GONE);
            filterCountryList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
//                sideBar.setVisibility(View.VISIBLE);
                filterCountryList.clear();
                rechargeCountryAdapter.setDataList(showCountryList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (showCountryList != null && showCountryList.size() > 0) {
                int size = showCountryList.size();
                for (int i = 0; i < size; i++) {
                    RechargeCountryEntity rechargeCountryEntity = showCountryList.get(i);
                    String name = rechargeCountryEntity.countryName;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterCountryList.add(rechargeCountryEntity);
                    }
                }
                rechargeCountryAdapter.setDataList(filterCountryList);
                if (filterCountryList.size() == 0) {
                    llNoResultFound.setVisibility(View.VISIBLE);
                }else {
                    llNoResultFound.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void getRechargeOperatorSuccess(RechargeOperatorListEntity rechargeOperatorListEntity) {

    }

    @Override
    public void getRechargeCountrySuccess(RechargeCountryListEntity rechargeCountryListEntity) {
        if (rechargeCountryListEntity != null) {
            List<RechargeCountryEntity> countryList = rechargeCountryListEntity.countryList;
            if (countryList != null && countryList.size() > 0) {
                showStatusView(typeStatusView, StatusView.CONTENT_VIEW);
                showCountryList.clear();
                int size = countryList.size();
                for (int i = 0; i < size; i++) {
                    RechargeCountryEntity rechargeCountryEntity = countryList.get(i);
                    if (i == 0) {
                        //后台返回默认第一个是本国数据
                        rechargeCountryEntity.countryType = getString(R.string.BillPay_4);
                    } else {
                        rechargeCountryEntity.countryType = getString(R.string.BillPay_5);
                    }
                    showCountryList.add(rechargeCountryEntity);
                }
                rechargeCountryAdapter.setDataList(showCountryList);
                if (showCountryList.size() == 0) {
                    llNoResultFound.setVisibility(View.VISIBLE);
                }else {
                    llNoResultFound.setVisibility(View.GONE);
                }
            } else {
                showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
        showTipDialog(errorMsg);
    }

    @Override
    public RechargeContract.RechargeCountryPresenter getPresenter() {
        return new RechargeCountryPresenter();
    }
}
