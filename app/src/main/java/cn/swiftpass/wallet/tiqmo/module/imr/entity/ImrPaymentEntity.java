package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrPaymentEntity extends BaseEntity {
    public String paymentMethodName;
    public String paymentMethodLogo;
    public String defaultPaymentOrgCode;
    public String channelCode;
    public List<String> supportCurrencyList = new ArrayList<>();
    public String paymentMode;
}
