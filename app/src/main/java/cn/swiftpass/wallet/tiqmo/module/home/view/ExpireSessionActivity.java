package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class ExpireSessionActivity extends BaseCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.tv_log_out)
    Button tvLogOut;
    @BindView(R.id.tv_yes)
    Button tvYes;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_expire_session;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        Window window = getWindow();
        //取消设置Window半透明的Flag
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //添加Flag把状态栏设为可绘制模式
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        //设置状态栏为透明/或者需要的颜色
//        window.setStatusBarColor(mContext.getColor(R.color.color_4c686969));

        getWindow().setGravity(Gravity.CENTER);//设置显示在底部 默认在中间
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = AndroidUtils.getScreenWidth(mContext)-AndroidUtils.dip2px(mContext,72f);//设置宽度满屏
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(lp);
        setFinishOnTouchOutside(false);//允许点击空白处关闭

        if(getIntent() != null && getIntent().getExtras() != null){
            String title = mContext.getString(R.string.sprint11_56)+"\n"+mContext.getString(R.string.sprint11_57);
            String content = mContext.getString(R.string.sprint11_58);
            String countTime = getIntent().getExtras().getString("countTime");
            if(tvTitle != null) {
                tvTitle.setText(title);
            }
            AdvancedCountdownTimer.getInstance().countDownEvent(!TextUtils.isEmpty(countTime)?Integer.parseInt(countTime):60, new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if(millisUntilFinished == 0){
                        AdvancedCountdownTimer.getInstance().cancel();
                        logout();
                    }
                    if(tvMessage != null) {
                        tvMessage.setText(content.replace("XXX", millisUntilFinished + ""));
                    }
                }

                @Override
                public void onFinish() {
                    AdvancedCountdownTimer.getInstance().cancel();
                    logout();
                }
            });
        }
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
//        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @OnClick({R.id.tv_log_out, R.id.tv_yes})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_log_out:
                finish();
                AdvancedCountdownTimer.getInstance().cancel();
                logout();
                break;
            case R.id.tv_yes:
                AdvancedCountdownTimer.getInstance().cancel();
                finish();

                AppClient.getInstance().longTimeNoUse(null);
                break;
            default:break;
        }
    }
}
