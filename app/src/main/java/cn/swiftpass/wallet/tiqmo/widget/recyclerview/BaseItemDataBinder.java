package cn.swiftpass.wallet.tiqmo.widget.recyclerview;

import android.content.Context;

import java.util.List;

/**
 * 数据绑定基类
 * @param <T>
 * @author shican
 */
public abstract class BaseItemDataBinder<T> {

    public Context mContext;
    public List<T> mDataList;

    /**不同条目的itemType*/
    public abstract int getViewType();

    /**不同条目的layoutId*/
    public abstract int getLayoutId();

    /**数据绑定*/
    public abstract void bindData(BaseViewHolder baseViewHolder,T item,int position);

    /**点击事件 子类可重写该方法*/
    public void onItemClick(BaseViewHolder baseViewHolder,T item,int position){

    }

}
