package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SendReceiveDetailListEntity  extends BaseEntity {


    private List<SendReceiveDetailEntity> detailList;

    public List<SendReceiveDetailEntity> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<SendReceiveDetailEntity> detailList) {
        this.detailList = detailList;
    }


}
