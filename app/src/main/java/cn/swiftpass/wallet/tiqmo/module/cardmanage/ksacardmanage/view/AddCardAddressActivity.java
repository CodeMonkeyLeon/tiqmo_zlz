package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.MapResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.AddCardAddressPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;

public class AddCardAddressActivity extends BaseCompatActivity<CardKsaContract.AddCardAddressPresenter> implements CardKsaContract.AddCardAddressView, OnMapReadyCallback {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_province_subtitle)
    TextView tvProvinceSubtitle;
    @BindView(R.id.tv_province)
    TextView tvProvince;
    @BindView(R.id.ll_province)
    FrameLayout llProvince;
    @BindView(R.id.tv_city_choose_subtitle)
    TextView tvCityChooseSubtitle;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.ll_city_choose)
    FrameLayout llCityChoose;
    @BindView(R.id.et_address)
    CustomizeEditText etAddress;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_save)
    TextView tvSave;
    @BindView(R.id.con_map)
    CardView conMap;

    private GoogleMap mMap;
    private String phoneNo;

    private OpenCardReqEntity openCardReqEntity;

    private String province,city,address;

    private CityEntity.City cityEntity;
    private StateEntity stateEntity;

    private String mapAddress;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_card_address;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        userInfoEntity = getUserInfoEntity();
        if (userInfoEntity != null) {
            phoneNo = userInfoEntity.phone;
        }
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if(status == ConnectionResult.SUCCESS) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }else{
            conMap.setVisibility(View.GONE);
        }
        tvPhone.setText(AndroidUtils.addStrToPhone(SpUtils.getInstance().getCallingCode() + " " + AndroidUtils.addStarPhone(phoneNo)));
        etAddress.setFilters(new InputFilter[]{new InputFilter.LengthFilter(64),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPACE)});
        etAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                address = etAddress.getText().toString().trim();
                if (!hasFocus) {
                    checkCompleteEnable();
                    searchMap();
                }
            }
        });
        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                address = s.toString().trim();
                checkCompleteEnable();
            }
        });
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void showCityListDia(CityEntity result) {
        ArrayList<SelectInfoEntity> cityNameList = new ArrayList<>();
        for (int i = 0; i < result.citiesInfoList.size(); i++) {
            cityNameList.add(new SelectInfoEntity(result.citiesInfoList.get(i).citiesName));
        }
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(cityNameList, getString(R.string.IMR_72), true, true,getString(R.string.sprint11_90));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (cityEntity == null) {
                    cityEntity = result.citiesInfoList.get(position);
                } else {
                    if (!cityEntity.citiesId.equals(result.citiesInfoList.get(position).citiesId)) {
                        cityEntity = result.citiesInfoList.get(position);
                    }
                }
                if (tvCity.getVisibility() == View.GONE) {
                    ViewAnim.startViewUp(mContext, tvCityChooseSubtitle, new ViewAnim.AnimStateListener() {
                        @Override
                        public void onAnimEnd() {
                            tvCity.setText(cityEntity.citiesName);
                            tvCity.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    tvCity.setText(cityEntity.citiesName);
                }
                city = cityEntity.citiesName;
                checkCompleteEnable();
                searchMap();
            }
        });
        dialog.showNow(getSupportFragmentManager(), "city");
    }

    private void checkCompleteEnable() {
        tvSave.setEnabled(!TextUtils.isEmpty(city)&&!TextUtils.isEmpty(address)&&!TextUtils.isEmpty(province));
    }

    @Override
    public void getStateListSuccess(StateListEntity stateListEntity) {
        ArrayList<SelectInfoEntity> stateNameList = new ArrayList<>();
        for (int i = 0; i < stateListEntity.statesInfoList.size(); i++) {
            stateNameList.add(new SelectInfoEntity(stateListEntity.statesInfoList.get(i).statesName));
        }
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(stateNameList, getString(R.string.IMR_new_4), true, true,getString(R.string.sprint11_89));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (stateEntity == null) {
                    stateEntity = stateListEntity.statesInfoList.get(position);
                } else {
                    if (!stateEntity.statesId.equals(stateListEntity.statesInfoList.get(position).statesId)) {
                        tvCity.setText("");
                        city = "";
                        tvCity.setVisibility(View.GONE);
                        ViewAnim.resetViewParams(mContext, tvCityChooseSubtitle);
                        stateEntity = stateListEntity.statesInfoList.get(position);
                    }
                }
//                LatLng sydney = new LatLng(24.7135517, 46.6752957);
//                mMap.addMarker(new MarkerOptions()
//                        .position(sydney));
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,15.0f));
                if (tvProvince.getVisibility() == View.GONE) {
                    ViewAnim.startViewUp(mContext, tvProvinceSubtitle, new ViewAnim.AnimStateListener() {
                        @Override
                        public void onAnimEnd() {
                            tvProvince.setText(stateEntity.statesName);
                            tvProvince.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    tvProvince.setText(stateEntity.statesName);
                }
                province = stateEntity.statesName;
                checkCompleteEnable();
                searchMap();
            }
        });

        dialog.showNow(getSupportFragmentManager(), "state");
    }

    private void searchMap(){
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if(status == ConnectionResult.SUCCESS) {
            //Success! Do what you want
            address = etAddress.getText().toString().trim();
            mapAddress = "";
            StringBuffer stringBuffer = new StringBuffer(mapAddress);
            if(!TextUtils.isEmpty(province)){
                stringBuffer.append(province);
                if(!TextUtils.isEmpty(city)){
                    stringBuffer.append(",");
                    stringBuffer.append(city);
                }

                if(!TextUtils.isEmpty(address)){
                    stringBuffer.append(",");
                    stringBuffer.append(address);
                }
            }else{
                if(!TextUtils.isEmpty(address)){
                    stringBuffer.append(address);
                }
            }
            mapAddress = stringBuffer.toString();
            mPresenter.mapSearch(mapAddress, BuildConfig.googleMapKey);
        }
    }

    @Override
    public void mapSearchSuccess(MapResultEntity mapResultEntity) {
        try {
            if(mapResultEntity.results != null && mapResultEntity.results.size()>0) {
                String lat = mapResultEntity.results.get(0).geometry.location.lat;
                String lng = mapResultEntity.results.get(0).geometry.location.lng;
                Bitmap unselectedMarker = setMarker(R.drawable.icon_map);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(mMap != null) {
                            LatLng sydney = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(unselectedMarker))
                                    .position(sydney));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15.0f));
                        }
                    }
                });
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public Bitmap setMarker(int resId) {
        View markerView = LayoutInflater.from(this).inflate(R.layout.view_map_marker_normal, null, false);
        ImageView ivBranchMarker = markerView.findViewById(R.id.iv_marker);
        ivBranchMarker.setImageResource(resId);
        Bitmap bmp = ((BitmapDrawable) ivBranchMarker.getDrawable()).getBitmap();
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        float scaleWidth = 1.0f;
        float scaleHeight = 1.0f;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        bmp = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, true);
        return bmp;
    }

    @Override
    public CardKsaContract.AddCardAddressPresenter getPresenter() {
        return new AddCardAddressPresenter();
    }

    @OnClick({R.id.ll_province, R.id.ll_city_choose, R.id.tv_save})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.ll_province:
                mPresenter.getStateList();
                break;
            case R.id.ll_city_choose:
                if (stateEntity == null) return;
                mPresenter.getCityList(stateEntity.statesId);
                break;
            case R.id.tv_save:
                openCardReqEntity.address = address;
                openCardReqEntity.city = city;
                openCardReqEntity.province = province;
                Intent intent = new Intent();
                intent.putExtra("openCardReqEntity",openCardReqEntity);
                setResult(RESULT_OK,intent);
                finish();
                break;
            default:break;
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        try {
            mMap = googleMap;

            // Add a marker in Sydney and move the camera
            Bitmap unselectedMarker = setMarker(R.drawable.icon_map);
            LatLng sydney = new LatLng(ProjectApp.getLatitude(), ProjectApp.getLongitude());
            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(unselectedMarker))
                    .position(sydney));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15f));
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }
}
