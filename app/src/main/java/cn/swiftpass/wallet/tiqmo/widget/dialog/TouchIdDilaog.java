package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class TouchIdDilaog extends BottomDialog{

    private Context mContext;

    private AcceptListener acceptListener;

    public void setAcceptListener(AcceptListener acceptListener) {
        this.acceptListener = acceptListener;
    }

    public TouchIdDilaog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface AcceptListener {
        void clickAccept();
        void clickCancel();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_touch_id, null);
        TextView tvAccept = view.findViewById(R.id.tv_accept);
        TextView tvThanks = view.findViewById(R.id.tv_thanks);

        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (acceptListener != null) {
                    acceptListener.clickAccept();
                }
            }
        });

        tvThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(acceptListener != null){
                    acceptListener.clickCancel();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        setCanceledOnTouchOutside(false);
    }
}
