package cn.swiftpass.wallet.tiqmo.module.addmoney.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanBaseActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TokenDataEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AddMoneyPresenter implements AddMoneyContract.Presenter {

    AddMoneyContract.View baseView;

    @Override
    public void getTransferFee(String type, String orderAmount, String orderCurrencyCode) {
        AppClient.getInstance().getTransferFee(type, orderAmount, orderCurrencyCode, "", new LifecycleMVPResultCallback<TransFeeEntity>(baseView, true,false) {
            @Override
            protected void onSuccess(TransFeeEntity result) {
                baseView.getTransferFeeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.getTransferFeeFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getCardList(String cardType) {
        AppClient.getInstance().getCardManager().getBankCardList(cardType,new LifecycleMVPResultCallback<AllCardEntity>(baseView,true,false) {
            @Override
            protected void onSuccess(AllCardEntity result) {
                baseView.getCardListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(baseView, true,false) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                baseView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void googlePay(String type, String orderAmount, String orderCurrencyCode, TokenDataEntity tokenData, String transFees, String vat) {
        AppClient.getInstance().googlePay(type, orderAmount, orderCurrencyCode, tokenData, transFees, vat, new LifecycleMVPResultCallback<CardNotifyEntity>(baseView, true) {
            @Override
            protected void onSuccess(CardNotifyEntity result) {
                baseView.googlePaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.googlePayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getIbanActivityDetail() {
        AppClient.getInstance().getTransferManager().getIbanActivityDetail(new LifecycleMVPResultCallback<IbanBaseActivityEntity>(baseView,true) {
            @Override
            protected void onSuccess(IbanBaseActivityEntity result) {
                baseView.getIbanActivityDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddMoneyContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
