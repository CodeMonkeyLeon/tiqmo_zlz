package cn.swiftpass.wallet.tiqmo.support.locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.LanguageEntity;

public class LocaleUtils {
    /**
     * 英文
     */
    public static final String LOCALE_ENGLISH = "en";
    /**
     * 阿拉伯语
     */
    public static final String LOCALE_ARABIC = "ar";
    /**
     * 保存SharedPreferences的文件名
     */
    private static final String LOCALE_FILE = "LOCALE_FILE";
    /**
     * 保存Locale的key
     */
    private static final String LOCALE_KEY = "LOCALE_KEY";
    private static final String RTL_KEY = "RTL";
    /**
     * SAR货币 英语
     */
    private static final String CURRENCY_SAR_EN = "SAR";
    private static final String CURRENCY_AED_EN = "AED";
    /**
     * SAR货币 阿拉伯语
     */
    private static final String CURRENCY_SAR_AR = "ريال";
    /**
     * AED货币 阿拉伯语
     */
    private static final String CURRENCY_AED_AR = "د.إ";

    /**
     * 获取用户设置的Locale
     *
     * @param context Context
     * @return Locale
     */
    public static String getUserLocale(Context context) {
        if (context == null || context.getApplicationContext() == null) {
            return "";
        }
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(LOCALE_FILE, Context.MODE_PRIVATE);
        // follow the system locale if the user does not set locale
//        if (LOCALE_ARABIC.equals(sharedPreferences.getString(LOCALE_KEY, getSystemLocale(context)))) {
//            AppClient.setLanguage("ar");
//        } else {
//            AppClient.setLanguage("en_US");
//        }
        AppClient.setLanguage(sharedPreferences.getString(LOCALE_KEY, getSystemLocale(context)));
        return sharedPreferences.getString(LOCALE_KEY, "");
    }

    public static boolean getIsRTL(Context context) {
        if (context == null || context.getApplicationContext() == null) {
            return false;
        }
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(LOCALE_FILE, Context.MODE_PRIVATE);
        // follow the system locale if the user does not set locale
//        if (LOCALE_ARABIC.equals(sharedPreferences.getString(LOCALE_KEY, getSystemLocale(context)))) {
//            AppClient.setLanguage("ar");
//        } else {
//            AppClient.setLanguage("en_US");
//        }
//        AppClient.setLanguage(sharedPreferences.getString(LOCALE_KEY, getSystemLocale(context)));
        return sharedPreferences.getBoolean(RTL_KEY, false);
    }

    /**
     * 保存用户设置的Locale
     *
     * @param context Context
     * @param lang    String
     */
    public static void saveUserLocale(Context context, String lang,boolean isRtl) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(LOCALE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(LOCALE_KEY, lang);
        edit.putBoolean(RTL_KEY,isRtl);
        edit.apply();
        AppClient.setLanguage(lang);
//        if (LOCALE_ARABIC.equals(lang)) {
//            AppClient.setLanguage("ar");
//        } else {
//            AppClient.setLanguage("en_US");
//        }
    }

    /**
     * 获取当前的Locale
     *
     * @param context Context
     * @return Locale
     */
    public static String getSystemLocale(Context context) {
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { //7.0有多语言设置获取顶部的语言
            locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = context.getResources().getConfiguration().locale;
        }
        return locale.getLanguage();
    }

    /**
     * 更新locale中的语言设置
     * @param context
     * @return
     */
    public static Context updateResources(Context context) {
        if (context == null || TextUtils.isEmpty(getUserLocale(context))) {
            return context;
        }
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        Locale locale = new Locale(getUserLocale(context));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleList defaultList = LocaleList.forLanguageTags(getUserLocale(context));
            LocaleList.setDefault(defaultList);
            conf.setLocales(defaultList);
        } else {
            conf.locale = locale;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(locale);
            context = context.createConfigurationContext(conf);
        }

        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        return context;
    }

    /**
     * 判断需不需要更新
     *
     * @param pContext Context
     * @param lang     String
     * @return true / false
     */
    public static boolean needUpdateLocale(Context pContext, String lang) {
        return !TextUtils.isEmpty(lang) && !getUserLocale(pContext).equals(lang);
    }

    /**
     * 更新语言
     *
     * @param context Context
     * @return true / false
     */
    public static boolean switchLocale(Context context, String lang,boolean isRTL) {
        if (needUpdateLocale(context, lang)) {
            saveUserLocale(context, lang,isRTL);
            return true;
        }
        return false;
    }

    /**
     * 判断显示方向是否右到左
     *
     * @param context Context
     * @return true / false
     */
    public static boolean isRTL(Context context) {
//        return getUserLocale(context).equals(LOCALE_ARABIC);
        return getIsRTL(context);
    }

    /**
     * 绕着竖直中心线旋转
     *
     * @param views   Views
     * @param context Context
     */
    public static void viewRotationY(Context context, View... views) {
        if (isRTL(context)) {
            int length = views.length;
            for (int i = 0; i < length; i++) {
                if (views[i] != null) {
                    views[i].setRotationY(180f);
                }
            }
        }
    }

    /**
     * 绕着竖直中心线旋转
     *
     * @param views   Views
     * @param context Context
     */
    public static void viewRotation360Y(Context context, View... views) {
        if (isRTL(context)) {
            int length = views.length;
            for (int i = 0; i < length; i++) {
                if (views[i] != null) {
                    views[i].setRotationY(180f);
                }
            }
        }
    }

    public static String getCurrencyShow() {
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        String currencyShow = "";
        if (configEntity != null) {
            currencyShow = configEntity.currencyShow;
        }
        return currencyShow;
    }

    /**
     * 获取请求后台的币种单位
     * @return
     */
    public static String getRequestCurrencyCode(){
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            if (configEntity.currencyCds != null && configEntity.currencyCds.size() > 0) {
                LanguageEntity languageEntity = configEntity.currencyCds.get(0).currencyLang;
                if (languageEntity != null) {
                    return languageEntity.en_US;
                }
            }
        }
        return CURRENCY_SAR_EN;
    }

    /**
     * 获取多语言下的货币单位
     *
     * @param enCurrency
     * @return
     */

    public static String getCurrencyCode(String enCurrency) {
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            //是否去币种
            if ("0".equals(configEntity.currencyShow)) {
                return "";
            }
            if (configEntity.currencyCds != null && configEntity.currencyCds.size() > 0) {
                LanguageEntity languageEntity = configEntity.currencyCds.get(0).currencyLang;
                if (languageEntity != null) {
                    if (getUserLocale(ProjectApp.getContext()).equals(LOCALE_ARABIC)) {
                        return languageEntity.ar;
                    }
                    return languageEntity.en_US;
                }
            }
        }

        if (getUserLocale(ProjectApp.getContext()).equals(LOCALE_ARABIC)) {
            return CURRENCY_SAR_AR;
        }
        return CURRENCY_SAR_EN;
//        if (TextUtils.isEmpty(enCurrency)) {return "";}
//        if (getUserLocale(ProjectApp.getContext()).equals(LOCALE_ARABIC) && enCurrency.equals(CURRENCY_SAR_EN)){
//            return CURRENCY_SAR_AR;
//        }
//        return enCurrency;
    }

}
