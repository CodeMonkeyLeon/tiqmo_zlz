package cn.swiftpass.wallet.tiqmo.module.register.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class NafathUrlEntity extends BaseEntity {

    /**
     * 认证url
     */
    public String authUrl;
    /**
     * hashState用来查询认证状态
     */
    public String hashedState;

    /**
     * 跳转url用来监听
     */
    public String redirectUrl;
    /**
     * 1跳过认证流程 0不跳过  默认为0
     */
    public String nafathAuthFlag = "0";
    public String firstName;
    /**
     * 认证数字
     */
    public String random;
    /**
     * 倒计时
     */
    public long countSeconds;

    /**
     * 是否跳过Nafath
     * @return
     */
    public boolean isSkipNafath(){
        return "1".equals(nafathAuthFlag);
    }
}
