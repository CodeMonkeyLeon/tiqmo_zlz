package cn.swiftpass.wallet.tiqmo.widget;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;

/**
 * 圆形进度条控件
 * 起点为90度
 */

public class CircularProgressView1 extends View {

    private Paint mBackPaint, mProgPaint;   // 绘制画笔
    private RectF mRectF;       // 绘制区域
    private int[] mColorArray;  // 圆环渐变色
    private int mProgress;      // 圆环进度(0-100)
    //圆环之间的间距占比角度
    private int spaceProgressAngle = 0;

    private int startAngle = 270,endAngle;

    private List<ColorItem> items = new ArrayList<>();
    //所有item总占比
    private int totalProgress = 360,totalSpace;

    int viewWide;
    int viewHigh;
    int mRectLength;
    int mRectL;
    int mRectT;
    private float downX, downY;

    private RectF pieInRectF;
    private RectF pieOutRectF;
    private float mRoundWidth = 40;

    private double mSumData = 0;

    private List<Region> regionList = new ArrayList<>();

    private ColorItemClickListener colorItemClickListener;

    public void setColorItemClickListener(final ColorItemClickListener colorItemClickListener) {
        this.colorItemClickListener = colorItemClickListener;
    }

    public void setItems(List<ColorItem> items) {
        regionList.clear();
        totalProgress = 360;
        this.items = items;
        if(items.size()>1) {
            totalSpace = spaceProgressAngle * items.size();
            totalProgress = 360-totalSpace;
        }
//        totalProgress = 0;
//        for (ColorItem item : items) {
//            totalProgress += item.getValue();
//        }
        invalidate();
    }

    public CircularProgressView1(Context context) {
        this(context, null);
    }

    public CircularProgressView1(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularProgressView1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        @SuppressLint("Recycle")
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircularProgressView);

        // 初始化背景圆环画笔
        mBackPaint = new Paint();
        mBackPaint.setStyle(Paint.Style.STROKE);    // 只描边，不填充
        mBackPaint.setStrokeCap(Paint.Cap.ROUND);   // 设置圆角
        mBackPaint.setAntiAlias(true);              // 设置抗锯齿
        mBackPaint.setDither(true);                 // 设置抖动
        mBackPaint.setStrokeWidth(typedArray.getDimension(R.styleable.CircularProgressView_backWidth, 5));
        mBackPaint.setColor(typedArray.getColor(R.styleable.CircularProgressView_backColor, Color.LTGRAY));

        // 初始化进度圆环画笔
        mProgPaint = new Paint();
        mProgPaint.setStyle(Paint.Style.STROKE);    // 只描边，不填充
        mProgPaint.setStrokeCap(Paint.Cap.ROUND);   // 设置圆角
        mProgPaint.setAntiAlias(true);              // 设置抗锯齿
        mProgPaint.setDither(true);                 // 设置抖动
        mProgPaint.setStrokeWidth(typedArray.getDimension(R.styleable.CircularProgressView_progWidth, 10));
        mProgPaint.setColor(typedArray.getColor(R.styleable.CircularProgressView_progColor, Color.BLUE));

        // 初始化进度圆环渐变色
        int startColor = typedArray.getColor(R.styleable.CircularProgressView_progStartColor, -1);
        int firstColor = typedArray.getColor(R.styleable.CircularProgressView_progFirstColor, -1);
        if (startColor != -1 && firstColor != -1) mColorArray = new int[]{startColor, firstColor};
        else mColorArray = null;

        // 初始化进度
        mProgress = typedArray.getInteger(R.styleable.CircularProgressView_progressCir, 0);
        typedArray.recycle();
        pieInRectF = new RectF();
        pieOutRectF = new RectF();
        spaceProgressAngle = 360*2/100;
        startAngle = 270+spaceProgressAngle;
    }

    public void setSpaceProgressAngle(final int spaceProgressAngle) {
        this.spaceProgressAngle = spaceProgressAngle;
        startAngle = 270+spaceProgressAngle;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWide = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        viewHigh = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        mRectLength = (int) ((viewWide > viewHigh ? viewHigh : viewWide) - (mBackPaint.getStrokeWidth() > mProgPaint.getStrokeWidth() ? mBackPaint.getStrokeWidth() : mProgPaint.getStrokeWidth()));
        mRectL = getPaddingLeft() + (viewWide - mRectLength) / 2;
        mRectT = getPaddingTop() + (viewHigh - mRectLength) / 2;
        mRectF = new RectF(mRectL, mRectT, mRectL + mRectLength, mRectT + mRectLength);

        //设置绘制内圆的矩形
        pieInRectF.left = mRectF.left + mRoundWidth;
        pieInRectF.top = mRectF.top + mRoundWidth;
        pieInRectF.right = mRectF.right -mRoundWidth;
        pieInRectF.bottom = mRectF.bottom -mRoundWidth;
        //设置绘制外圆的矩形
        pieOutRectF.left = mRectF.left - mRoundWidth;
        pieOutRectF.top = mRectF.top - mRoundWidth;
        pieOutRectF.right = mRectF.right + mRoundWidth;
        pieOutRectF.bottom = mRectF.bottom + mRoundWidth;

        // 设置进度圆环渐变色
        if (mColorArray != null && mColorArray.length > 1)
            mProgPaint.setShader(new LinearGradient(0, 0, 0, getMeasuredWidth(), mColorArray, null, Shader.TileMode.MIRROR));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        startAngle = 270+spaceProgressAngle;
        int startAngle = 90;
        canvas.drawArc(mRectF, 0, 360, false, mBackPaint);
        int size = items.size();
        for (int i=0;i<size;i++) {
            ColorItem item = items.get(i);
            mProgPaint.setColor(item.getColor());
            int mProgress = item.getValue();
            endAngle = totalProgress * mProgress / 100;

            Path path = new Path();
            path.addArc(mRectF, startAngle, endAngle);

            Path pathRegion = getArcPath(pieInRectF, pieOutRectF, startAngle, endAngle);
            item.setRegion(pathRegion);
            canvas.drawPath(path, mProgPaint);
//            canvas.drawArc(mRectF, startAngle, endAngle, false, mProgPaint);
            startAngle = startAngle+endAngle+spaceProgressAngle;
        }
    }

    private Path getArcPath(RectF in, RectF out, int startAngle, int angle) {
        Path path1 = new Path();
        path1.moveTo(in.centerX(), in.centerY());
        path1.arcTo(in, startAngle, angle);
        Path path2 = new Path();
        path2.moveTo(out.centerX(), out.centerY());
        path2.arcTo(out, startAngle, angle);
        Path path = new Path();
        path.op(path2, path1, Path.Op.DIFFERENCE);
        return path;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
                downY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                for (ColorItem colorItem : items) {
//                    colorItem.setColor(getContext().getColor(AndroidUtils.getSpendingColorId(colorItem.colorId)));
                    if (colorItem.isInRegion(downX, downY)) {
                        if(colorItem.isSelect == 0){
                            colorItem.isSelect = 1;
                        }else{
                            colorItem.isSelect = 0;
                        }
                        if(colorItemClickListener != null){
                            colorItemClickListener.clickColor(colorItem);
                        }
                    }else{
//                        colorItem.isSelect = 0;
//                        colorItem.setColor(getContext().getColor(R.color.color_50dcdcdc));
                    }
                }
//                invalidate();
                break;
        }
        return true;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * 获取当前进度
     *
     * @return 当前进度（0-100）
     */
    public int getProgress() {
        return mProgress;
    }

    /**
     * 设置当前进度
     *
     * @param progress 当前进度（0-100）
     */
    public void setProgress(int progress) {
        this.mProgress = progress;
        invalidate();
    }

    /**
     * 设置当前进度，并展示进度动画。如果动画时间小于等于0，则不展示动画
     *
     * @param progress 当前进度（0-100）
     * @param animTime 动画时间（毫秒）
     */
    public void setProgress(int progress, long animTime) {
        if (animTime <= 0) setProgress(progress);
        else {
            ValueAnimator animator = ValueAnimator.ofInt(mProgress, progress);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    mProgress = (int) animation.getAnimatedValue();
                    invalidate();
                }
            });
            animator.setInterpolator(new OvershootInterpolator());
            animator.setDuration(animTime);
            animator.start();
        }
    }

    /**
     * 设置背景圆环宽度
     *
     * @param width 背景圆环宽度
     */
    public void setBackWidth(int width) {
        mBackPaint.setStrokeWidth(width);
        invalidate();
    }

    /**
     * 设置背景圆环颜色
     *
     * @param color 背景圆环颜色
     */
    public void setBackColor(@ColorRes int color) {
        mBackPaint.setColor(ContextCompat.getColor(getContext(), color));
        invalidate();
    }

    /**
     * 设置进度圆环宽度
     *
     * @param width 进度圆环宽度
     */
    public void setProgWidth(int width) {
        mProgPaint.setStrokeWidth(width);
        invalidate();
    }

    /**
     * 设置进度圆环颜色
     *
     * @param color 景圆环颜色
     */
    public void setProgColor(@ColorRes int color) {
        mProgPaint.setColor(ContextCompat.getColor(getContext(), color));
        mProgPaint.setShader(null);
        invalidate();
    }

    /**
     * 设置进度圆环颜色(支持渐变色)
     *
     * @param startColor 进度圆环开始颜色
     * @param firstColor 进度圆环结束颜色
     */
    public void setProgColor(@ColorRes int startColor, @ColorRes int firstColor) {
        mColorArray = new int[]{ContextCompat.getColor(getContext(), startColor), ContextCompat.getColor(getContext(), firstColor)};
        mProgPaint.setShader(new LinearGradient(0, 0, 0, getMeasuredWidth(), mColorArray, null, Shader.TileMode.MIRROR));
        invalidate();
    }

    /**
     * 设置进度圆环颜色(支持渐变色)
     *
     * @param colorArray 渐变色集合
     */
    public void setProgColor(@ColorRes int[] colorArray) {
        if (colorArray == null || colorArray.length < 2) return;
        mColorArray = new int[colorArray.length];
        for (int index = 0; index < colorArray.length; index++)
            mColorArray[index] = ContextCompat.getColor(getContext(), colorArray[index]);
        mProgPaint.setShader(new LinearGradient(0, 0, 0, getMeasuredWidth(), mColorArray, null, Shader.TileMode.MIRROR));
        invalidate();
    }

    public interface ColorItemClickListener{
        void clickColor(ColorItem colorItem);
    }
}
