package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;

/**
 * 只能输入中文和数字（中文，英文，字符）
 */
public class ChineseInputFilter implements InputFilter {
    private Pattern mPattern;

    public ChineseInputFilter() {
        String reg = InputConst.CHINESE;
        reg += InputConst.ENGLISH;
        reg += InputConst.NUMBER;
        reg += InputConst.SPECIAL_CHAR;
        reg += InputConst.EMPTY;
        mPattern = Pattern.compile("[" + reg + "]*");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (mPattern.matcher(temp).matches()) {
            return source;
        }
        return "";
    }
}
