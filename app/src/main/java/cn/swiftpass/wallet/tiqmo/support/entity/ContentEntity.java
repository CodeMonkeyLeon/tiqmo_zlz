package cn.swiftpass.wallet.tiqmo.support.entity;


public class ContentEntity extends BaseEntity {

    /**
     * result_msg : OK
     * data : EsPfHfowD8EgfjTFPnHUnVFtzY9tRwjBnHSSKYkCBa7LOZMb0jQivPejnJhVhcuX91X1HsSUUpVzBKzsYacPrw==
     * sign : uYuRObHLY0SQ43qLG+LDsmlqSFEJge7CESIR1wEGt9nbBZweas6qKna3zODaSc/5Hyzvk0nz7UwWDd62ZcqXz2KYO7PXz8oHb8GIBeWqhJwBSWf0rUNHGdm/vD/nQH/07RTuvvkoVYtoyfUrCtANwMrRqcxfTPhslQbL1vh8ddI=
     * result_code : 0
     * token : NqFTtxNJgqFXPI6txg1utoPZhT2jUDR+yCa8q1nAuBiTounyRnmbMOpmRu1QbBnMnGWW/vBcqRc7pG5zTqD3H59WMYSd08SWpaK7PQsN9Q24yC6XC4npC6AFhJdd0bqmI2HCUyuZBWf00WmTRyNX9CbLUIDcpBEfNdpqYHDQ9vA=
     */

    private String result_msg;
    private String data;
    private String sign;
    private String result_code;
    private String token;
    private String timestamp;

    public String getResult_msg() {
        return result_msg;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
