package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 输入金额的产品信息
 */
public class RechargeInputProductEntity extends BaseEntity {
    /**
     \"inputProduct\":{\"serviceType\":\"RANGED_VALUE_RECHARGE\",\"productId\":\"18951\",
     \"destinationAmountIncrement\":\"1\",\"description\":\"Enter amount between 5 and 1000 in multiple of 1.\",
     \"title\":\"Custom Amount\",\"operatorName\":\"TNT Philippines\",\"productName\":\"Open_Range\",
     \"destinationMaxAmount\":\"1000\",\"exchangeRate\":13.1504140000,\"destinationCurrencyCode\":\"PHP\",
     \"destinationMinAmount\":\"5\",\"operatorId\":\"2897\",\"productType\":\"Custom Amount\",\"productDescription\":\"\"}}}"
     */
    //服务类型
    public String serviceType;
    //产品ID
    public String productId;
    //目标金额强制增量
    public String destinationAmountIncrement;
    //	展示的标题，已经做过国际化
    public String title;
    //产品名称 也做过国际化
    public String productName;
    //最大目标金额
    public String destinationMaxAmount;
    //等值本币金额汇率
    public String exchangeRate;
    //目标币种
    public String destinationCurrencyCode;
    //最小目标金额
    public String destinationMinAmount;
    //运营商ID
    public String operatorId;
    //产品类型
    public String productType;
    //产品描述
    public String productDescription;
    //描述，展示的标题，已做国际化
    public String description;
    //运营商名称，已做国际化
    public String operatorName;
    public String operatorImgUrl;

    public String amount;

    public String destinationAmount;

}
