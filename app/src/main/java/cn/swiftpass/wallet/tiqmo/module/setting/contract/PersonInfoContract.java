package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RegionEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class PersonInfoContract {

    public interface View extends BaseView<PersonInfoContract.Presenter> {
        void getUserInfoSuccess(UserInfoEntity userInfoEntity);

        void getUserInfoFail(String errorCode, String errorMsg);

        void updateUserInfoSuccess(UserInfoEntity result);

        void updateUserInfoFail(String errorCode, String errorMsg);

        //展示上传成功提示
        void showUploadSuccessfulHint();

        //展示上传失败提示
        void showUploadFailedHint(String error);

        //展示新头像
        void showNewAvatar(String avatarUrl);

        //展示用户信息
        void showUserInfo(UserInfoEntity userInfo);

        void getStateListSuccess(StateListEntity result);

        void showCityListDia(CityEntity result);

        void showErrorMsg(String errorCode, String errorMsg);

        void showRegionListDia(RegionEntity result);

        void showDeactivationDia();

        void showCannotDeactivationDia();

        void showSourceOfFund(List<SelectInfoEntity> data);

        void showProfession(List<SelectInfoEntity> data);

        void showCompanyType(List<SelectInfoEntity> data);

        void showSalaryRange(List<SelectInfoEntity> data);

        void renewIdSuccess(ResultEntity resultEntity);

        void getEmploymentSectorSuccess(List<SelectInfoEntity> data);

        void renewIdFail(String errorCode, String errorMsg);

        void verifyEmailSuccess(Void result);
        void getChangePhoneFeeSuccess(ChangePhoneFeeEntity result);

        void verifyEmailFail(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);
    }

    public interface Presenter extends BasePresenter<PersonInfoContract.View> {
        void getUserInfo();

        void updateUserInfo(UserInfoEntity userInfoEntity);

        //上传头像
        void uploadAvatar(String localPath);

        void getStateList();

        void getCityList(String statesId);

        void getDistrictList(String citiesId);

        void getSourceOfFund();

        void getEmploymentSector(String businessType,String parentBussinessCode);

        void getProfession(String businessType,String parentBussinessCode);

        void getCompanyType();

        void getSalaryRange();

        void getBalance();

        void renewId();

        void verifyEmail(String email);
        void getChangePhoneFee();
        void getLimit(String type);
    }

    public interface StopView extends BaseView<PersonInfoContract.StopPresenter> {
        void stopAccountSuccess(Void result);

        void stopAccountFail(String errorCode, String errorMsg);
    }

    public interface StopPresenter extends BasePresenter<PersonInfoContract.StopView> {
        void stopAccount();
    }

    public interface ChangePhoneView extends BaseView<PersonInfoContract.ChangePhonePresenter> {
        void checkPhoneSuccess(Void result);

        void checkPhoneFail(String errorCode, String errorMsg);
    }

    public interface ChangePhonePresenter extends BasePresenter<PersonInfoContract.ChangePhoneView> {
        void checkPhone(String callingCode, String phone, String checkIdNumber);
    }
}
