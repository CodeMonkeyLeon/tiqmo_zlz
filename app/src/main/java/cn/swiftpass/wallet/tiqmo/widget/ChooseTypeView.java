package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;

public class ChooseTypeView extends FrameLayout {

    @BindView(R.id.tv_content_title)
    TextView tvContentTitle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.iv_content)
    ImageView ivContent;
    @BindView(R.id.fl_content)
    FrameLayout flContent;

    private String content;
//    private String contentTitle;
    private Context context;

    private OnTypeClickListener onTypeClickListener;

    public void setOnTypeClickListener(OnTypeClickListener onTypeClickListener) {
        this.onTypeClickListener = onTypeClickListener;
    }

    public void setContentTitle(String contentTitle, int imgResId) {
        tvContentTitle.setText(contentTitle);
        ivContent.setImageResource(imgResId);
    }

    public String getContent() {
        return content;
    }

    public void setEnable(boolean enable) {
        flContent.setEnabled(enable);
        this.setAlpha(0.4f);
    }

    public void setContent(String content) {
        this.content = content;
        if (tvContent.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(context, tvContentTitle, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    tvContent.setText(content);
                    tvContent.setVisibility(View.VISIBLE);
                }
            });
        } else {
            tvContent.setText(content);
            tvContent.setVisibility(View.VISIBLE);
        }
    }

    public ChooseTypeView(@NonNull Context context) {
        super(context);
        initViews(context);
    }

    public ChooseTypeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    private void initViews(Context context) {
        this.context = context;
        inflate(context, R.layout.edittext_choose_type, this);
        ButterKnife.bind(this);
        setFocusable(true);
    }

    @OnClick(R.id.fl_content)
    public void onViewClicked() {
        if (onTypeClickListener != null) {
            onTypeClickListener.onTypeClick();
        }
    }

    public interface OnTypeClickListener {
        void onTypeClick();
    }
}
