package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;

import javax.crypto.Cipher;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.login.view.ChooseLanguageActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.TouchIDContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.TouchIDPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AboutTiqmoActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CardManageActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CustomerInfoActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycSuccessActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.widget.SettingItemView;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.MyQrCodeActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.manager.BiometricManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.FingerPrintHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;

public class ProfileFragment extends BaseFragment<TouchIDContract.Presenter> implements TouchIDContract.View {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_theme_switch)
    TextView tvThemeSwitch;
    @BindView(R.id.cb_theme_switch)
    CheckBox cbThemeSwitch;
    @BindView(R.id.ll_setting_normal)
    LinearLayout llSettingNormal;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.profile_bg)
    ConstraintLayout rootView;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_version_name)
    TextView tvVersionName;

    private CheckBox cbLogin, cbPay;
    private ImageView ivArrowPwd, ivIconPwd, ivArrowAcc, ivIconAcc, ivBaseAcc, ivBaseAccArrow, ivShareAcc, ivShareAccArrow, ivLoginFace, ivPayFace, ivChangeLoginPwd, ivChangeLoginPwdArr, ivChangePayPwd, ivChangePayPwdArr,
            ivForgetLoginPwd, ivForgetLoginPwdArr, ivForgetPayPwd, ivForgetPayPwdArr;
    private TextView tvPwdTitlePwd, tvPwdTitleAcc, tvBaseAcc, tvShareAcc, tvPwdSubTitleBio, tvLoginFace,
            tvPayFace, tvPwdSubTitleMag, tvChangeLoginPwd, tvChangePayPwd, tvForgetLoginPwd, tvForgetPayPwd;

    private List<BaseEntity> settingIconList = new ArrayList<>();

    private SettingItemView myQrCodeView;
    private SettingItemView referralCodeView;
    private SettingItemView customInfoView;
    private SettingItemView cardManageView;
    private View accountManageView;
    private SettingItemView termsConditionView;
    private SettingItemView contactUsView;
    private SettingItemView languageView;
    private SettingItemView aboutView;
    private SettingItemView logoutView;
    private View settingPwdView;

    private FingerPrintHelper mFingerPrintHelper;
    private UserInfoEntity userInfoEntity;
    private String phone;

    private LinearLayout llPayFace, llSettingPwd, llSettingAccountR,
            llSettingAccountDetail, llFace, llPwdManage, ll_setting_pwd;

    public static ProfileFragment getInstance() {
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, headCircle, ivBack);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        mFingerPrintHelper = FingerPrintHelper.newInstance(ProjectApp.getContext());
        myQrCodeView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_my_qr_code), R.string.sprint19_52);
        referralCodeView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_referral_code), R.string.sprint11_69);
        customInfoView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_profile), R.string.Profile_CLP_0010_L_3);
        cardManageView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_card_management), R.string.Profile_CLP_0010_L_4);
        accountManageView = addAccountManageView();
        settingPwdView = addSettingPwdView();
        termsConditionView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_terms_conditions), R.string.Profile_CLP_0010_L_15);
        contactUsView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_contact_us), R.string.Help_support_1);
        languageView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_language), R.string.Profile_CLP_0010_L_16);
        aboutView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_customer_information), R.string.sprint11_133);
        logoutView = addRecordItemView(ThemeSourceUtils.getSourceID(mContext, R.attr.setting_logout), R.string.Profile_CLP_0010_L_17);

        cbPay.setOnCheckedChangeListener(mOnTouchIDPaymentChangeListener);
        cbLogin.setOnCheckedChangeListener(mOnTouchIDLoginChangeListener);
        cbThemeSwitch.setChecked(ThemeUtils.isCurrentDark(mContext));

        tvVersionName.setText(getString(R.string.sprint11_63)+" "+AndroidUtils.getCurrentAppVersionName(mContext));

        cbThemeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ProjectApp.changeTheme = true;
                ThemeUtils.switchCurrentThemeTag(mContext);
                ReferralCodeDialogUtils.resetKeyDialog();
            }
        });

        termsConditionView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                HashMap<String,Object> hashMap = new HashMap<>();
                hashMap.put(Constants.terms_condition_type,"1");
                ActivitySkipUtil.startAnotherActivity(mActivity, WebViewActivity.class, hashMap,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        contactUsView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("isHideTitle", false);
                hashMap.put("hideTitle", getString(R.string.newhome_44));
                hashMap.put("topTitle", getString(R.string.Help_support_1));
                ActivitySkipUtil.startAnotherActivity(mActivity, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        myQrCodeView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                if (isKyc(Constants.KYC_TYPE_NULL)) {
                    HashMap<String,Object> hashMap = new HashMap<>();
                    String qrCodeUrl = SpUtils.getInstance().getQrCode();
                    if (TextUtils.isEmpty(qrCodeUrl) || qrCodeUrl.startsWith("swiftpass://")) {
                        showProgress(true);
                        AppClient.getInstance().getQrCode(UserInfoManager.getInstance().getCurrencyCode(), new ResultCallback<QrCodeEntity>() {
                            @Override
                            public void onResult(QrCodeEntity qrCodeEntity) {
                                showProgress(false);
                                if (qrCodeEntity != null) {
                                    String qrCodeUrl = qrCodeEntity.qRCodeInfo;
                                    SpUtils.getInstance().setQrCode(qrCodeUrl);

                                    HashMap<String,Object> hashMap = new HashMap<>();
                                    hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                                    ActivitySkipUtil.startAnotherActivity(mActivity,MyQrCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }
                            }

                            @Override
                            public void onFailure(String errorCode, String errorMsg) {
                                showProgress(false);
                            }
                        });
                    } else {
                        hashMap.put(Constants.qrCodeUrl,qrCodeUrl);
                        ActivitySkipUtil.startAnotherActivity(mActivity,MyQrCodeActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });
        referralCodeView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_rewards))return;
                if (isKyc(Constants.KYC_TYPE_REFERRAL_CODE)) {
                    getInviteDetail();
                }
            }
        });

        customInfoView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_myProfile))return;
                ActivitySkipUtil.startAnotherActivity(mActivity, CustomerInfoActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        cardManageView.setVisibility(View.GONE);
        cardManageView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                if (isNotExpireId()) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, CardManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });

        aboutView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                HashMap<String,Object> mHashMap = new HashMap<>();
                mHashMap.put("title",getString(R.string.sprint11_133));
                ActivitySkipUtil.startAnotherActivity(mActivity, AboutTiqmoActivity.class, mHashMap,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        languageView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                HashMap<String,Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.IS_FROM_PROFILE,true);
                ActivitySkipUtil.startAnotherActivity(mActivity, ChooseLanguageActivity.class, mHashMap,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        logoutView.setOnItemClickListener(new SettingItemView.OnItemClick() {
            @Override
            public void onItemClick() {
                AppClient.getInstance().logout(new ResultCallback<Void>() {
                    @Override
                    public void onResult(Void result) {

                    }

                    @Override
                    public void onFailure(String code, String error) {

                    }
                });
                AppClient.getInstance().clearLastSessionInfo();
                UserInfoEntity user = AppClient.getInstance().getUserManager().getUserInfo();
                if (user != null) {
                    LoginFastNewActivity.startActivityOfNewTaskType(mContext, user, null);
                } else {
                    RegisterActivity.startActivityOfNewTaskType(mContext);
                }
            }
        });
    }

    @Override
    protected void initData() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            String userName = userInfoEntity.userName;
            if (!TextUtils.isEmpty(userName)) {
                if (userName.length() > 16) {
                    userName = userName.substring(0, 16) + "...";
                }
                tvName.setText(userName.trim());
            }
            phone = userInfoEntity.phone;
            if (!TextUtils.isEmpty(userInfoEntity.avatar)) {
                Glide.with(this).clear(ivAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(userInfoEntity.avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender))
                        .into(ivAvatar);
            } else {
                Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender)).into(ivAvatar);
            }
        }
        checkTouchIDState();
    }

    private void getInviteDetail(){
        showProgress(true);
        AppClient.getInstance().getInviteDetail(new ResultCallback<InviteEntity>() {
            @Override
            public void onResult(InviteEntity inviteEntity) {
                showProgress(false);
                if(inviteEntity != null){
                    HashMap<String, Object> mHashMap = new HashMap<>();
                    mHashMap.put(Constants.INVITE_ENTITY, inviteEntity);
                    String badges = userInfoEntity.badges;
                    mHashMap.put(Constants.badges, badges);
                    ActivitySkipUtil.startAnotherActivity(mActivity, InviteFriendsActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                showProgress(false);
                showTipDialog(errorMsg);
            }
        });
    }

    @Override
    protected void restart() {
        checkTouchIDState();
    }


    private SettingItemView addRecordItemView(int imgId, int textId) {
        SettingItemView settingItemView = new SettingItemView(mContext);
        settingItemView.setText(textId);
        settingItemView.setImage(imgId);
        llSettingNormal.addView(settingItemView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return settingItemView;
    }

    private View addAccountManageView() {
        View settingPwdView = LayoutInflater.from(getContext()).inflate(R.layout.item_setting_account, null);
        llSettingAccountR = settingPwdView.findViewById(R.id.ll_setting_account_r);
        LinearLayout llSettingAccount = settingPwdView.findViewById(R.id.ll_setting_account);
        LinearLayout llWalletBasicAccount = settingPwdView.findViewById(R.id.ll_wallet_basic_account);
        LinearLayout llShareAccount = settingPwdView.findViewById(R.id.ll_share_account);
        llSettingAccountDetail = settingPwdView.findViewById(R.id.ll_setting_account_detail);
        ivArrowAcc = settingPwdView.findViewById(R.id.iv_setting_account_arrow);
        ivIconAcc = settingPwdView.findViewById(R.id.iv_acc_icon);
        tvPwdTitleAcc = settingPwdView.findViewById(R.id.tv_acc_title);
        ivBaseAcc = settingPwdView.findViewById(R.id.iv_setting_base_acc);
        tvBaseAcc = settingPwdView.findViewById(R.id.tv_setting_base_acc);
        ivBaseAccArrow = settingPwdView.findViewById(R.id.iv_base_acc_arrow);
        ivShareAcc = settingPwdView.findViewById(R.id.iv_setting_share_acc);
        tvShareAcc = settingPwdView.findViewById(R.id.tv_setting_share_acc);
        ivShareAccArrow = settingPwdView.findViewById(R.id.iv_share_acc_arrow);
        LocaleUtils.viewRotationY(mContext, ivArrowAcc, ivBaseAccArrow, ivShareAccArrow);
        llWalletBasicAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_accountDetails))return;
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (isKyc(Constants.KYC_TYPE_ACCOUNT)) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, AccountDetailActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });

        llShareAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        llSettingAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick(-1, 300)) {
                    if (isKyc(Constants.KYC_TYPE_ACCOUNT)) {
                        ActivitySkipUtil.startAnotherActivity(mActivity, AccountDetailActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });

        llSettingNormal.addView(settingPwdView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return settingPwdView;
    }

    private View addSettingPwdView() {
        View settingPwdView = LayoutInflater.from(getContext()).inflate(R.layout.item_setting_password, null);
        LinearLayout llSettingPrivacy = settingPwdView.findViewById(R.id.ll_setting_privacy);
        ll_setting_pwd = settingPwdView.findViewById(R.id.ll_setting_pwd);
        LinearLayout llForgetLoginPwd = settingPwdView.findViewById(R.id.ll_forget_login_pwd);
        LinearLayout llChangeLoginPwd = settingPwdView.findViewById(R.id.ll_change_login_pwd);
        llSettingPwd = settingPwdView.findViewById(R.id.ll_setting_password);
        LinearLayout llLoginFace = settingPwdView.findViewById(R.id.ll_login_face);
        llFace = settingPwdView.findViewById(R.id.ll_face);
        llPwdManage = settingPwdView.findViewById(R.id.ll_pwd_menage);
        llPayFace = settingPwdView.findViewById(R.id.ll_pay_face);
        cbLogin = settingPwdView.findViewById(R.id.cb_login_switch);
        cbPay = settingPwdView.findViewById(R.id.cb_pay_switch);
        ivArrowPwd = settingPwdView.findViewById(R.id.iv_setting_pwd_arrow);
        ivArrowPwd.setImageResource(ThemeSourceUtils.getSourceID(getActivity(), getArrow(ll_setting_pwd)));
        ivIconPwd = settingPwdView.findViewById(R.id.iv_pwd_icon);
        tvPwdTitlePwd = settingPwdView.findViewById(R.id.tv_pwd_title);
        tvPwdSubTitleBio = settingPwdView.findViewById(R.id.tv_pwd_subtitle_bio);
        ivLoginFace = settingPwdView.findViewById(R.id.iv_login_face);
        tvLoginFace = settingPwdView.findViewById(R.id.tv_login_face);
        ivPayFace = settingPwdView.findViewById(R.id.iv_pay_face);
        tvPayFace = settingPwdView.findViewById(R.id.tv_pay_face);
        tvPwdSubTitleMag = settingPwdView.findViewById(R.id.tv_pwd_subtitle_manage);
        ivChangeLoginPwd = settingPwdView.findViewById(R.id.iv_change_login_pwd);
        tvChangeLoginPwd = settingPwdView.findViewById(R.id.tv_change_login_pwd);
        ivChangeLoginPwdArr = settingPwdView.findViewById(R.id.iv_change_login_pwd_arr);
        ivForgetLoginPwd = settingPwdView.findViewById(R.id.iv_forget_login_pwd);
        tvForgetLoginPwd = settingPwdView.findViewById(R.id.tv_forget_login_pwd);
        ivForgetLoginPwdArr = settingPwdView.findViewById(R.id.iv_forget_login_pwd_arr);
        ivChangePayPwd = settingPwdView.findViewById(R.id.iv_change_pay_pwd);
        tvChangePayPwd = settingPwdView.findViewById(R.id.tv_change_pay_pwd);
        ivChangePayPwdArr = settingPwdView.findViewById(R.id.iv_change_pay_pwd_arr);
        ivForgetPayPwd = settingPwdView.findViewById(R.id.iv_forget_pay_pwd);
        tvForgetPayPwd = settingPwdView.findViewById(R.id.tv_forget_pay_pwd);
        ivForgetPayPwdArr = settingPwdView.findViewById(R.id.iv_forget_pay_pwd_arr);
        LocaleUtils.viewRotationY(mContext, ivArrowPwd, ivChangeLoginPwdArr, ivForgetLoginPwdArr,
                ivChangePayPwdArr, ivForgetPayPwdArr);

        cbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_loginFace))return;
                if (!ProjectApp.isPhoneNotSupportedFace()) {
                    if (!ProjectApp.isBiometricSupported()) {
                        showErrorDialog(getString(R.string.common_22));
                    }
                }
            }
        });

        cbPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_payFace))return;
                if (isNotExpireId()) {
                    if (!ProjectApp.isPhoneNotSupportedFace()) {
                        if (!ProjectApp.isBiometricSupported()) {
                            showErrorDialog(getString(R.string.common_22));
                        }
                    }
                }
            }
        });
        userInfoEntity = getUserInfoEntity();
        if (userInfoEntity != null && userInfoEntity.isKyc()) {
            llPayFace.setVisibility(View.VISIBLE);
        } else {
            llPayFace.setVisibility(View.GONE);
        }

        if (ProjectApp.isPhoneNotSupportedFace()) {
            llFace.setVisibility(View.GONE);
        } else {
            llFace.setVisibility(View.VISIBLE);
        }
        llChangeLoginPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_changePayPd))return;
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                HashMap<String, Object> mHashMap = new HashMap<>(16);
                mHashMap.put(Constants.sceneType, Constants.TYPE_CHANGE_LOGIN_PD);
//                ActivitySkipUtil.startAnotherActivity(mActivity, ChangeLoginPwdActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                ActivitySkipUtil.startAnotherActivity(mActivity, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        llForgetLoginPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_forgotLoginPd))return;
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                mHashMaps.put(Constants.DATA_PHONE_NUMBER, ProjectApp.getPhone());
                mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_FORGET_LOGIN_PD);
                ActivitySkipUtil.startAnotherActivity(mActivity, RegisterActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        llSettingPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse(mActivity,Constants.profile_security))return;
                if (!ButtonUtils.isFastDoubleClick(-1, 300)) {
                    ll_setting_pwd.setVisibility(ll_setting_pwd.getVisibility() == View.VISIBLE
                            ? View.GONE : View.VISIBLE);
                    ivArrowPwd.setImageResource(ThemeSourceUtils.getSourceID(mContext,
                            getArrow(ll_setting_pwd)));
                }
            }
        });

        llSettingNormal.addView(settingPwdView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return settingPwdView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }

        if (ProjectApp.kycSuccess) {
            ActivitySkipUtil.startAnotherActivity(mActivity, KycSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.kycSuccess = false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_OPEN_ACCOUNT == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, AccountDetailActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.KYC_TYPE_REFERRAL_CODE == event.getEventType()) {
            getInviteDetail();
//            ActivitySkipUtil.startAnotherActivity(getActivity(), ReferralCodeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.KYC_TYPE_ACCOUNT == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, AccountDetailActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.KYC_TYPE_CUSTOMER_INFO == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, CustomerInfoActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }


    private void checkTouchIDState() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            if (userInfoEntity.isKycHigh()) {
                showLogoutDialog();
                return;
            }
            if (userInfoEntity.isKyc()) {
                if (llPayFace != null) {
                    llPayFace.setVisibility(View.VISIBLE);
                }
            } else {
                if (llPayFace != null) {
                    llPayFace.setVisibility(View.GONE);
                }
            }
            String avatar = userInfoEntity.avatar;
            String userName = userInfoEntity.userName;
            String gender = userInfoEntity.gender;
            if (!TextUtils.isEmpty(userName)) {
                if (userName.length() > 16) {
                    userName = userName.substring(0, 16) + "...";
                }
                tvName.setText(userName.trim());
            } else {
                tvName.setText("");
            }
            if (!TextUtils.isEmpty(avatar)) {
                Glide.with(this).clear(ivAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                        .into(ivAvatar);
            } else {
                Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivAvatar);
            }
        }
        if (ProjectApp.isBiometricSupported() && AndroidUtils.isOpenBiometricLogin()) {
            cbLogin.setChecked(true);
        } else {
            cbLogin.setChecked(false);
        }

        if (ProjectApp.isBiometricSupported() && AndroidUtils.isOpenBiometricPay()) {
            cbPay.setChecked(true);
        } else {
            cbPay.setChecked(false);
        }
    }

    @Override
    public void switchTouchIDPayment(boolean isOpen) {
        cbPay.setChecked(isOpen);
        if (isOpen) {
            showToast(getString(R.string.Hint_FingerprintOpenSuccessful));
        } else {
            showTipDialog(getString(R.string.common_35));
        }
    }

    @Override
    public void switchTouchIDLogin(boolean isOpen) {
        cbLogin.setChecked(isOpen);
        if (isOpen) {
            showToast(getString(R.string.Hint_FingerprintOpenSuccessful));
        } else {
            showTipDialog(getString(R.string.common_35));
        }
    }

    @Override
    public TouchIDContract.Presenter getPresenter() {
        return new TouchIDPresenter();
    }

    private void tryOpenTouchIDLogin() {
        if (AndroidUtils.isOpenBiometricLogin()) {
            cbLogin.setChecked(true);
            return;
        }
        if (!mFingerPrintHelper.hasEnrolledFingerprints()) {
//            showErrorDialog(getString(R.string.Hint_PleaseRegisterFingerprint));
            cbLogin.setChecked(false);
            return;
        }
        showBiometricPrompt(true);
    }

    private Handler handler = new Handler();

    private Executor executor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };

    //生物认证的setting
    private void showBiometricPrompt(boolean isLogin) {
        BiometricPrompt.PromptInfo promptInfo =
                new BiometricPrompt.PromptInfo.Builder()
                        .setTitle(isLogin? getString(R.string.common_40) :  getString(R.string.common_37)) //设置大标题
                        .setSubtitle(isLogin? getString(R.string.common_41) :  getString(R.string.common_38)) // 设置标题下的提示
                        .setNegativeButtonText(getString(R.string.common_6)) //设置取消按钮
                        .build();
        //需要提供的参数callback
        BiometricPrompt biometricPrompt = new BiometricPrompt(getActivity(),
                executor, new BiometricPrompt.AuthenticationCallback() {
            //各种异常的回调
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(ProjectApp.mContext,
                        getString(R.string.common_42) + ": " + errString, Toast.LENGTH_SHORT)
                        .show();
                if (isLogin) {
                    cbLogin.setChecked(false);
                } else {
                    cbPay.setChecked(false);
                }
            }

            //认证成功的回调
            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                BiometricPrompt.CryptoObject authenticatedCryptoObject =
                        result.getCryptoObject();
                if (isLogin) {
                    mPresenter.setEnableTouchIDLogin(true);
                } else {
                    mPresenter.setEnableTouchIDPayment(true);
                }
            }

            //认证失败的回调
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(ProjectApp.mContext, getString(R.string.common_44),
                        Toast.LENGTH_SHORT)
                        .show();
                if (isLogin) {
                    cbLogin.setChecked(false);
                } else {
                    cbPay.setChecked(false);
                }
            }
        });
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            FingerPrintHelper mFingerPrintHelper = FingerPrintHelper.newInstance(mContext);
            if (mFingerPrintHelper == null) {
                return;
            }
            String mSecretKeyName = AppClient.getInstance().getUserID() + (isLogin ? BiometricManager.FINGERPRINT_SECRET_KEY_LOGIN : BiometricManager.FINGERPRINT_SECRET_KEY_PAYMENT);
            Cipher cipher = mFingerPrintHelper.getCipherIfFingerPrintNoChange(mSecretKeyName);
            if (cipher == null) {
                cipher = mFingerPrintHelper.generateNewSecretKey(mSecretKeyName);
                if (cipher == null) {
                    LogUtil.e("cipher == null");
                    return;
                }
            }
            BiometricPrompt.CryptoObject crypto = new BiometricPrompt.CryptoObject(cipher);
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo, crypto);
        } else {
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo);
        }
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        myQrCodeView.changeTheme(R.attr.icon_my_qr_code);
        referralCodeView.changeTheme(R.attr.setting_referral_code);
        customInfoView.changeTheme(R.attr.icon_profile);
        cardManageView.changeTheme(R.attr.setting_card_management);
        termsConditionView.changeTheme(R.attr.setting_terms_conditions);
        contactUsView.changeTheme(R.attr.setting_contact_us);
        languageView.changeTheme(R.attr.setting_language);
        logoutView.changeTheme(R.attr.setting_logout);
        aboutView.changeTheme(R.attr.setting_customer_information);
        helper.setTextColorByAttr(tvTitle, R.attr.wallet_setting_item_text);
        helper.setTextColorByAttr(tvThemeSwitch, R.attr.wallet_setting_item_text);
        helper.setTextColorByAttr(tvName, R.attr.wallet_setting_text_color);
        helper.setTextColorByAttr(tvVersionName, R.attr.color_white_3a3b44);
        helper.setImageBgResourceByAttr(headCircle, R.attr.icon_circle);
        helper.setImageResourceByAttr(ivBack, R.attr.back_arrow);
        helper.setBackgroundColorByAttr(rootView, R.attr.splash_bg_color);
        changePwdTheme(helper);
        changeAccTheme(helper);
        if (userInfoEntity == null || TextUtils.isEmpty(userInfoEntity.avatar)) {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender)).into(ivAvatar);
        }
    }

    private void changePwdTheme(ResourceHelper helper) {
        helper.setBackgroundResourceByAttr(llSettingPwd, R.attr.setting_item_bg);
        helper.setBackgroundResourceByAttr(llFace, R.attr.setting_wallet_sub_item_bg);
        helper.setBackgroundResourceByAttr(llPwdManage, R.attr.setting_wallet_sub_item_bg);
        helper.setDividerDrawableByAttr(llFace, R.attr.color_10white_10black);
        helper.setDividerDrawableByAttr(llPwdManage, R.attr.color_10white_10black);
        if (ll_setting_pwd != null) {
            helper.setImageResourceByAttr(ivArrowPwd, getArrow(ll_setting_pwd));
        }
        helper.setImageResourceByAttr(ivIconPwd, R.attr.setting_security_privacy);
        helper.setImageResourceByAttr(ivLoginFace, R.attr.setting_login_fingerprint_id);
        helper.setImageResourceByAttr(ivPayFace, R.attr.setting_pay_fingerprint_id);
        helper.setCheckBoxResourceByAttr(cbLogin, R.attr.bg_btn_selector);
        helper.setCheckBoxResourceByAttr(cbPay, R.attr.bg_btn_selector);

        helper.setImageResourceByAttr(ivChangeLoginPwd, R.attr.setting_change_login_password);
        helper.setImageResourceByAttr(ivChangeLoginPwdArr, R.attr.icon_right_arrow);
        helper.setImageResourceByAttr(ivForgetLoginPwd, R.attr.forgot_login_password);
        helper.setImageResourceByAttr(ivForgetLoginPwdArr, R.attr.icon_right_arrow);
        helper.setImageResourceByAttr(ivChangePayPwd, R.attr.change_payment_password_copy);
        helper.setImageResourceByAttr(ivChangePayPwdArr, R.attr.icon_right_arrow);
        helper.setImageResourceByAttr(ivForgetPayPwd, R.attr.forgot_payment_password_copy);
        helper.setImageResourceByAttr(ivForgetPayPwdArr, R.attr.icon_right_arrow);

        helper.setTextColorByAttr(tvPwdTitlePwd, R.attr.wallet_setting_item_text);
        helper.setTextColorByAttr(tvPwdSubTitleBio, R.attr.setting_wallet_sub_item_text_color1);
        helper.setTextColorByAttr(tvLoginFace, R.attr.setting_wallet_sub_item_text_color);
        helper.setTextColorByAttr(tvPayFace, R.attr.setting_wallet_sub_item_text_color);
        helper.setTextColorByAttr(tvPwdSubTitleMag, R.attr.setting_wallet_sub_item_text_color1);
        helper.setTextColorByAttr(tvChangeLoginPwd, R.attr.setting_wallet_sub_item_text_color);
        helper.setTextColorByAttr(tvChangePayPwd, R.attr.setting_wallet_sub_item_text_color);
        helper.setTextColorByAttr(tvForgetLoginPwd, R.attr.setting_wallet_sub_item_text_color);
        helper.setTextColorByAttr(tvForgetPayPwd, R.attr.setting_wallet_sub_item_text_color);

    }

    private void changeAccTheme(ResourceHelper helper) {
        helper.setBackgroundResourceByAttr(llSettingAccountR, R.attr.setting_item_bg);
        helper.setBackgroundResourceByAttr(llSettingAccountDetail, R.attr.setting_wallet_sub_item_bg);
        if (llSettingAccountDetail != null) {
            helper.setImageResourceByAttr(ivArrowAcc, getArrow(llSettingAccountDetail));
            helper.setDividerDrawableByAttr(llSettingAccountDetail, R.attr.color_10white_10black);
        }
        helper.setImageResourceByAttr(ivIconAcc, R.attr.icon_account_details);
        helper.setImageResourceByAttr(ivBaseAcc, R.attr.setting_wallet_basic_account);
        helper.setImageResourceByAttr(ivBaseAccArrow, R.attr.icon_right_arrow);
        helper.setImageResourceByAttr(ivShareAcc, R.attr.setting_shared_account);
        helper.setImageResourceByAttr(ivShareAccArrow, R.attr.icon_right_arrow);
        helper.setTextColorByAttr(tvPwdTitleAcc, R.attr.wallet_setting_item_text);
        helper.setTextColorByAttr(tvBaseAcc, R.attr.setting_wallet_sub_item_text_color);
        helper.setTextColorByAttr(tvShareAcc, R.attr.setting_wallet_sub_item_text_color);

    }

    private int getArrow(View view) {
        return view.getVisibility() == View.VISIBLE ? R.attr.icon_down_arrow : R.attr.icon_right_arrow;
    }

    private void tryOpenTouchIDPayment() {
        if (AndroidUtils.isOpenBiometricPay()) {
            cbPay.setChecked(true);
            return;
        }
        if (!mFingerPrintHelper.hasEnrolledFingerprints()) {
//            showErrorDialog(getString(R.string.Hint_PleaseRegisterFingerprint));
            cbPay.setChecked(false);
            return;
        }
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        mHashMap.put(Constants.checkPdType, Constants.checkPdType_face_id);
        mHashMap.put(Constants.isOpen, true);
        ActivitySkipUtil.startAnotherActivity(mActivity, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//        showBiometricPrompt(false);
    }

    private final CompoundButton.OnCheckedChangeListener mOnTouchIDPaymentChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            cbPay.setChecked(false);
            if (ProjectApp.isBiometricSupported()) {
                if (isChecked) {
                    tryOpenTouchIDPayment();
                } else {
                    HashMap<String, Object> mHashMap = new HashMap<>(16);
                    mHashMap.put(Constants.checkPdType, Constants.checkPdType_face_id);
                    mHashMap.put(Constants.isOpen, false);
                    ActivitySkipUtil.startAnotherActivity(mActivity, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        }
    };

    private final CompoundButton.OnCheckedChangeListener mOnTouchIDLoginChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            cbLogin.setChecked(false);
            if (ProjectApp.isBiometricSupported()) {
                if (isChecked) {
                    tryOpenTouchIDLogin();
                } else {
                    mPresenter.setEnableTouchIDLogin(false);
                }
            }
        }
    };
}
