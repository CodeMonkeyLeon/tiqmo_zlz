//package cn.swiftpass.wallet.tiqmo.sdk.net.api;
//
//import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
//import cn.swiftpass.wallet.tiqmo.sdk.entity.UpdateEntity;
//import cn.swiftpass.wallet.tiqmo.sdk.net.RequestCall;
//import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Headers;
//
///**
// * Created by 叶智星 on 2018年09月27日.
// * 每一个不曾起舞的日子，都是对生命的辜负。
// */
//public interface ConfigurationApi {
//
//    @Headers({"Service-Id:1048"})
//    RequestCall<JsonResponse<UpdateEntity>> checkAppUpdate();
//
//    @Headers({"Service-Id:1059"})
//    RequestCall<JsonResponse<GetInternationalCodeEntity>> getCountryPhoneCode();
//
//
//    @Headers({"Service-Id:1040"})
//    RequestCall<JsonResponse<ConfigurationDataEntity>> getCurrencyConfiguration();
//
//
//    @Headers({"Service-Id:1030"})
//    RequestCall<JsonResponse<ProtocolUrlEntity>> getProtocol(@Param("urlType") String urlType);
//
//    @Headers({"Service-Id:1028"})
//    RequestCall<JsonResponse<Void>> agreeAgreement(@Param("functionType") String functionType, @Param("enable") String isEnable);
//
//    @Headers({"Service-Id:1046"})
//    RequestCall<JsonResponse<AllAdvertisementEntity>> getAdvertisementList();
//}
