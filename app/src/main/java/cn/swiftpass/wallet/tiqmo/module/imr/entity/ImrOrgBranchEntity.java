package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrOrgBranchEntity extends BaseEntity {
    public String receiptOrgBranchCode;
    public String receiptOrgBranchName;
}
