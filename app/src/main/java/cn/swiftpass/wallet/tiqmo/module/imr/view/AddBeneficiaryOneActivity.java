package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrModeAdapter;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPaymentEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.AddBeneOnePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class AddBeneficiaryOneActivity extends BaseCompatActivity<AddBeneficiaryContract.AddOnePresenter> implements AddBeneficiaryContract.AddOneView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_destination)
    EditTextWithDel etDestination;
    @BindView(R.id.iv_bank_check)
    ImageView ivBankCheck;
    @BindView(R.id.ll_bank_account)
    LinearLayout llBankAccount;
    @BindView(R.id.iv_agent_check)
    ImageView ivAgentCheck;
    @BindView(R.id.ll_agent_cashout)
    LinearLayout llAgentCashout;
    @BindView(R.id.tv_next_step)
    TextView tvNextStep;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.ry_all_mode)
    MaxHeightRecyclerView ryAllMode;
    @BindView(R.id.ll_default_mode)
    LinearLayout ll_default_mode;

    private String transferDestinationCountryCode, receiptMethod, destinationName, callingCode, countryLogo, currencyCode,
            bankAddrIdType, agentAddrIdType, ibanPreCode, ibanLength, ibanSupport, accountSupport,channelPayeeId;
    private ImrCountryEntity imrCountryEntity;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    private boolean isEditBeneficiary = false;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;// edit data

    public List<String> bankSupportCurrencyCodes = new ArrayList<>();
    //机构模式支持币种
    public List<String> agentSupportCurrencyCodes = new ArrayList<>();

    private String bankMode;
    private String agentMode;

    private String receiptOrgCode;

    public List<ImrPaymentEntity> paymentMethodList = new ArrayList<>();
    private ImrModeAdapter imrModeAdapter;
    private int chooseModePosition = -1;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_beneficiary_one;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvTitle.setText(getString(R.string.IMR_20));
        etDestination.getTlEdit().setHint(getString(R.string.IMR_57));
        ivKycStep.setVisibility(View.VISIBLE);
        ivKycStep.setImageResource(R.drawable.kyc_step_1);
//        ivKycStep.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_imr_add_step_1));
        isEditBeneficiary = getIntent().getBooleanExtra(Constants.IS_FROM_EDIT_BENEFICIARY, false);
        if (isEditBeneficiary) {
            ll_default_mode.setVisibility(View.GONE);
            tvNextStep.setEnabled(true);
            beneficiaryInfo = (ImrBeneficiaryDetails.ImrBeneficiaryDetail) getIntent().getSerializableExtra("BeneficiaryInfo");
            transferDestinationCountryCode = beneficiaryInfo.transferDestinationCountryCode;
            destinationName = beneficiaryInfo.transferDestinationCountryName;
            receiptMethod = beneficiaryInfo.receiptMethod;
            callingCode = beneficiaryInfo.callingCode;
            countryLogo = beneficiaryInfo.countryLogo;
            receiptMethod = beneficiaryInfo.receiptMethod;
            channelPayeeId = beneficiaryInfo.channelPayeeId;
            receiptOrgCode = beneficiaryInfo.receiptOrgCode;
            bankAddrIdType = beneficiaryInfo.bankAddrIdType;
            agentAddrIdType = beneficiaryInfo.agentAddrIdType;
            ibanPreCode = beneficiaryInfo.ibanPreCode;
            ibanLength = beneficiaryInfo.ibanLength;
            agentAddrIdType = beneficiaryInfo.agentAddrIdType;
            currencyCode = beneficiaryInfo.countrySupportedCurrencyName;
            ibanSupport = beneficiaryInfo.ibanSupport;
            accountSupport = beneficiaryInfo.accountSupport;
            agentMode = beneficiaryInfo.agentMode;
            bankMode = beneficiaryInfo.bankMode;
            agentSupportCurrencyCodes = beneficiaryInfo.agentSupportCurrencyCodes;
            bankSupportCurrencyCodes = beneficiaryInfo.bankSupportCurrencyCodes;
            paymentMethodList = beneficiaryInfo.paymentMethodList;
            if(paymentMethodList.size()>0){
                for (int i=0;i<paymentMethodList.size();i++){
                    ImrPaymentEntity imrPaymentEntity = paymentMethodList.get(i);
                    if(!TextUtils.isEmpty(channelPayeeId) && channelPayeeId.equals(imrPaymentEntity.defaultPaymentOrgCode)){
                        beneficiaryInfo.imrPaymentEntity = imrPaymentEntity;
                        chooseModePosition = i;
                    }
                }
            }
            if (!TextUtils.isEmpty(beneficiaryInfo.transferDestinationCountryName)) {
                etDestination.getEditText().setText(beneficiaryInfo.transferDestinationCountryName);
            }
//            if (Constants.imr_BT.equals(receiptMethod)) {
//                llBankAccount.setVisibility(View.VISIBLE);
//                llBankAccount.setAlpha(1.0f);
//                ivBankCheck.setVisibility(View.VISIBLE);
//                if (!TextUtils.isEmpty(beneficiaryInfo.agentMode)) {
//                    llAgentCashout.setVisibility(View.VISIBLE);
//                    llAgentCashout.setAlpha(0.4f);
//                    ivAgentCheck.setVisibility(View.GONE);
//                } else {
//                    llAgentCashout.setVisibility(View.GONE);
//                }
//            } else{
//                llAgentCashout.setVisibility(View.VISIBLE);
//                llAgentCashout.setAlpha(1.0f);
//                ivAgentCheck.setVisibility(View.VISIBLE);
//                if (!TextUtils.isEmpty(beneficiaryInfo.bankMode)) {
//                    llBankAccount.setVisibility(View.VISIBLE);
//                    llBankAccount.setAlpha(0.4f);
//                    ivBankCheck.setVisibility(View.GONE);
//                } else {
//                    llBankAccount.setVisibility(View.GONE);
//                }
//            }
        } else {
            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            llBankAccount.setAlpha(0.4f);
            llAgentCashout.setAlpha(0.4f);
            llBankAccount.setEnabled(false);
            llAgentCashout.setEnabled(false);
            ivAgentCheck.setVisibility(View.GONE);
            ivBankCheck.setVisibility(View.GONE);
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryAllMode.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryAllMode.setLayoutManager(manager);
        imrModeAdapter = new ImrModeAdapter(paymentMethodList);
        imrModeAdapter.setChoosePosition(chooseModePosition);
        imrModeAdapter.bindToRecyclerView(ryAllMode);
        imrModeAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                ImrPaymentEntity imrPaymentEntity = imrModeAdapter.getDataList().get(position);
                if (imrPaymentEntity != null) {
                    receiptMethod = imrPaymentEntity.paymentMode;
                    if(isEditBeneficiary) {
                        beneficiaryInfo.imrPaymentEntity = imrPaymentEntity;
                        beneficiaryInfo.channelCode = imrPaymentEntity.channelCode;
                        beneficiaryInfo.channelPayeeId = imrPaymentEntity.defaultPaymentOrgCode;
                    }else{
                        imrBeneficiaryEntity.imrPaymentEntity = imrPaymentEntity;
                        imrBeneficiaryEntity.channelCode = imrPaymentEntity.channelCode;
                        imrBeneficiaryEntity.channelPayeeId = imrPaymentEntity.defaultPaymentOrgCode;
                    }
                    checkNextStatus();
                    if (isEditBeneficiary) {
                        resetOrgInfo();
                    }
                    imrModeAdapter.setChoosePosition(position);
                    imrModeAdapter.notifyDataSetChanged();
                }
            }
        });

        etDestination.setFocusableFalse();
        etDestination.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.getDestination(Constants.imr_destination,"");
            }
        });
    }

    private void checkNextStatus() {
        if (!TextUtils.isEmpty(transferDestinationCountryCode) && !TextUtils.isEmpty(receiptMethod)) {
            tvNextStep.setEnabled(true);
        } else {
            tvNextStep.setEnabled(false);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_next_step, R.id.ll_bank_account, R.id.ll_agent_cashout})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.ll_bank_account:
                if (Constants.imr_BT.equals(receiptMethod)) return;
                llBankAccount.setAlpha(1.0f);
                ivBankCheck.setVisibility(View.VISIBLE);
                llAgentCashout.setAlpha(0.4f);
                ivAgentCheck.setVisibility(View.GONE);
                receiptMethod = Constants.imr_BT;
                checkNextStatus();
                if (isEditBeneficiary) {
                    resetOrgInfo();
                }
                break;
            case R.id.ll_agent_cashout:
                if (agentMode.equals(receiptMethod)) return;
                llAgentCashout.setAlpha(1.0f);
                ivAgentCheck.setVisibility(View.VISIBLE);
                llBankAccount.setAlpha(0.4f);
                ivBankCheck.setVisibility(View.GONE);
                receiptMethod = agentMode;
                checkNextStatus();
                if (isEditBeneficiary) {
                    resetOrgInfo();
                }
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_next_step:
                if (isEditBeneficiary) {
                    beneficiaryInfo.transferDestinationCountryCode = transferDestinationCountryCode;
                    beneficiaryInfo.receiptMethod = receiptMethod;
                    beneficiaryInfo.transferDestinationCountryName = destinationName;
                    beneficiaryInfo.callingCode = callingCode;
                    beneficiaryInfo.countryLogo = countryLogo;
                    beneficiaryInfo.bankAddrIdType = bankAddrIdType;
                    beneficiaryInfo.agentAddrIdType = agentAddrIdType;
                    beneficiaryInfo.ibanPreCode = ibanPreCode;
                    beneficiaryInfo.ibanLength = ibanLength;
                    beneficiaryInfo.accountSupport = accountSupport;
                    beneficiaryInfo.ibanSupport = ibanSupport;
                    beneficiaryInfo.countrySupportedCurrencyName = currencyCode;
                    beneficiaryInfo.agentSupportCurrencyCodes = agentSupportCurrencyCodes;
                    beneficiaryInfo.bankSupportCurrencyCodes = bankSupportCurrencyCodes;
                    Intent intent = new Intent(AddBeneficiaryOneActivity.this, AddBeneficiaryTwoActivity.class);
                    intent.putExtra(Constants.IS_FROM_EDIT_BENEFICIARY, true);
                    intent.putExtra("BeneficiaryInfo", beneficiaryInfo);
                    AddBeneficiaryOneActivity.this.startActivity(intent);
                } else {
                    HashMap<String, Object> hashMap = new HashMap<>(16);
                    imrBeneficiaryEntity.transferDestinationCountryCode = transferDestinationCountryCode;
                    imrBeneficiaryEntity.receiptMethod = receiptMethod;
                    imrBeneficiaryEntity.destinationName = destinationName;
                    imrBeneficiaryEntity.callingCode = callingCode;
                    imrBeneficiaryEntity.countryLogo = countryLogo;
                    imrBeneficiaryEntity.ibanPreCode = ibanPreCode;
                    imrBeneficiaryEntity.ibanLength = ibanLength;
                    imrBeneficiaryEntity.accountSupport = accountSupport;
                    imrBeneficiaryEntity.ibanSupport = ibanSupport;
                    imrBeneficiaryEntity.showSaveList = false;
                    imrBeneficiaryEntity.bankSupportCurrencyCodes = bankSupportCurrencyCodes;
                    imrBeneficiaryEntity.agentSupportCurrencyCodes = agentSupportCurrencyCodes;
                    AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(imrBeneficiaryEntity);
                    ActivitySkipUtil.startAnotherActivityForResult(AddBeneficiaryOneActivity.this, AddBeneficiaryTwoActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, 100);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
        if (imrBeneficiaryEntity != null) {
            etDestination.getEditText().setText(imrBeneficiaryEntity.destinationName);
        }
    }

    @Override
    public void getDestinationSuccess(ImrCountryListEntity imrCountryListEntity) {
        if (imrCountryListEntity != null) {
            String channelCode = imrCountryListEntity.channelCode;
            SpUtils.getInstance().setChannelCode(channelCode);
            List<ImrCountryEntity> imrCountrysList = imrCountryListEntity.imrCountrysList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = imrCountrysList.size();
            for (int i = 0; i < size; i++) {
                ImrCountryEntity imrCountryEntity = imrCountrysList.get(i);
                if (imrCountryEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.imgUrl = imrCountryEntity.countryLogo;
                    selectInfoEntity.businessParamValue = imrCountryEntity.countryName + "  " + "+" + imrCountryEntity.callingCode;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_57), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    ll_default_mode.setVisibility(View.GONE);
                    if (isEditBeneficiary) {
                        resetOrgInfo();
                    }
                    imrCountryEntity = imrCountrysList.get(position);
                    if (imrCountryEntity != null) {
                        paymentMethodList = imrCountryEntity.paymentMethodList;
                        if(isEditBeneficiary) {
                            beneficiaryInfo.paymentMethodList = imrCountryEntity.paymentMethodList;
                        }else{
                            imrBeneficiaryEntity.paymentMethodList = imrCountryEntity.paymentMethodList;
                        }
                        receiptMethod = "";
                        if(paymentMethodList.size() == 1){
                            receiptMethod = paymentMethodList.get(0).paymentMode;
                            imrModeAdapter.setChoosePosition(0);
                        }else {
                            imrModeAdapter.setChoosePosition(-1);
                        }
                        imrModeAdapter.setDataList(paymentMethodList);
                        destinationName = imrCountryEntity.countryName;
                        etDestination.getEditText().setText(destinationName);
                        transferDestinationCountryCode = imrCountryEntity.countryCode;
                        callingCode = imrCountryEntity.callingCode;
                        countryLogo = imrCountryEntity.countryLogo;
                        ibanPreCode = imrCountryEntity.ibanPreCode;
                        ibanLength = imrCountryEntity.ibanLength;
                        currencyCode = imrCountryEntity.currencyCode;
                        ibanSupport = imrCountryEntity.ibanSupport;
                        accountSupport = imrCountryEntity.accountSupport;

                        bankMode = imrCountryEntity.bankMode;
                        agentMode = imrCountryEntity.agentMode;
                        bankSupportCurrencyCodes = imrCountryEntity.bankSupportCurrencyCodes;
                        agentSupportCurrencyCodes = imrCountryEntity.agentSupportCurrencyCodes;

//                        llBankAccount.setEnabled(true);
//                        llAgentCashout.setEnabled(true);

//                        if (TextUtils.isEmpty(bankMode)) {
//                            llBankAccount.setVisibility(View.GONE);
//                            if (!TextUtils.isEmpty(agentMode)) {
//                                receiptMethod = agentMode;
//                                llAgentCashout.setAlpha(1.0f);
//                                ivAgentCheck.setVisibility(View.VISIBLE);
//                            }
//                        } else {
//                            if (!TextUtils.isEmpty(agentMode)) {
//                                ivAgentCheck.setVisibility(View.GONE);
//                                ivBankCheck.setVisibility(View.GONE);
//                            }
//                            llBankAccount.setAlpha(1.0f);
//                            llBankAccount.setVisibility(View.VISIBLE);
//                        }
//                        if (TextUtils.isEmpty(agentMode)) {
//                            llAgentCashout.setVisibility(View.GONE);
//                            if (!TextUtils.isEmpty(bankMode)) {
//                                receiptMethod = bankMode;
//                                llBankAccount.setAlpha(1.0f);
//                                ivBankCheck.setVisibility(View.VISIBLE);
//                            }
//                        } else {
//                            if (!TextUtils.isEmpty(bankMode)) {
//                                ivAgentCheck.setVisibility(View.GONE);
//                                ivBankCheck.setVisibility(View.GONE);
//                            }
//                            llAgentCashout.setAlpha(1.0f);
//                            llAgentCashout.setVisibility(View.VISIBLE);
//                        }
                    }
                    checkNextStatus();
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getDestinationSuccess");
        }
    }

    private void resetOrgInfo() {
        beneficiaryInfo.receiptOrgCode = "";
        beneficiaryInfo.receiptOrgName = "";
        beneficiaryInfo.receiptOrgBranchCode = "";
        beneficiaryInfo.receiptOrgBranchName = "";
        beneficiaryInfo.bankAccountType = "";
        beneficiaryInfo.bankAccountNo = "";
        beneficiaryInfo.ibanNo = "";
        beneficiaryInfo.currencyCode = "";
        beneficiaryInfo.cityName = "";
    }

    @Override
    public void getDestinationFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddBeneficiaryContract.AddOnePresenter getPresenter() {
        return new AddBeneOnePresenter();
    }
}
