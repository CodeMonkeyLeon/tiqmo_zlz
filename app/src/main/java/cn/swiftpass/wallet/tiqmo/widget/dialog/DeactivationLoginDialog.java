package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.FragmentActivity;

import java.util.HashMap;
import java.util.concurrent.Executor;

import javax.crypto.Cipher;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.module.login.PwdIconUtils;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.manager.BiometricManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.FingerPrintHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;

public class DeactivationLoginDialog extends BottomDialog {

    private Context mContext;

    CustomizeEditText etPassword;
    private String password;
    private boolean isHidePwd = true;

    private CustomDialog customDialog;

    private Handler handler = new Handler();

    private Executor executor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };

    public DeactivationLoginDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_deactivation_login, null);
        etPassword = view.findViewById(R.id.et_password);
        TextView tvForgetPwd = view.findViewById(R.id.tv_forget_pwd);
        TextView tvLogin = view.findViewById(R.id.tv_login);
        TextView tvFio = view.findViewById(R.id.tv_fio);
        ImageView ivSetting = view.findViewById(R.id.iv_setting);
        TextView tvSetting = view.findViewById(R.id.tv_setting);
        LinearLayout llLoginFace = view.findViewById(R.id.ll_login_face);
        LinearLayout llContent = view.findViewById(R.id.ll_content);

        if (ProjectApp.isBiometricSupported()) {
            tvFio.setVisibility(View.VISIBLE);
            llLoginFace.setVisibility(View.VISIBLE);
            if (AndroidUtils.isOpenBiometricLogin()) {
                showBiometricPrompt();
            }
        } else {
            tvFio.setVisibility(View.GONE);
            llLoginFace.setVisibility(View.GONE);
        }

//        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                password = etPassword.getText().toString().trim();
//                if (!hasFocus && !TextUtils.isEmpty(password)) {
//                    if (!RegexUtil.isLoginPasswordLength(password) ||
//                            !RegexUtil.isLoginOneChar(password) ||
//                            !RegexUtil.isLoginOneNumber(password)) {
//                        etPassword.setError(mContext.getString(R.string.Login_SU_0005_4_D_5));
//                    }
//                }
//            }
//        });

        etPassword.addTextChangedListener(new SimpleTextWatcher(){
            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    tvLogin.setEnabled(false);
                } else {
                    if (s.toString().length() == 6) {
                        tvLogin.setEnabled(true);
                    } else {
                        tvLogin.setEnabled(false);
                    }
                }
            }
        });

        etPassword.setClickListener(new CustomizeEditText.OnViewClickListener() {
            @Override
            public void onRightViewClick() {
                isHidePwd = PwdIconUtils.setPwdHide(mContext, isHidePwd, etPassword);
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginRequest();
            }
        });

        tvForgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AndroidUtils.isCloseUse((Activity) mContext,Constants.profile_forgotLoginPd))return;
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_FORGET_LOGIN_PD);
                ActivitySkipUtil.startAnotherActivity((Activity) mContext, RegisterActivity.class,
                        mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        llLoginFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AndroidUtils.isOpenBiometricLogin()) {
                    showBiometricPrompt();
                } else {
                    showTouchIdDialog();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private void showTouchIdDialog() {
        if (ProjectApp.isBiometricSupported()) {
            customDialog = showCustomDialog(mContext, R.string.FaceID_SU_0006_D_2, R.string.FaceID_SU_0006_D_4, R.string.FaceID_SU_0006_D_6, R.string.FaceID_SU_0006_D_5,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (customDialog != null && customDialog.isShowing()) {
                                customDialog.dismiss();
                            }
                            AppClient.getInstance().getBiometricManager().registerFingerprintPayment(new ResultCallback<Void>() {
                                @Override
                                public void onResult(Void result) {
                                    if (customDialog != null && customDialog.isShowing()) {
                                        customDialog.dismiss();
                                    }
                                    showBiometricPrompt();
                                }

                                @Override
                                public void onFailure(String code, String error) {
//                                    showErrorDialog(error);
                                }
                            });
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            customDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                }
            });
        } else if (!ProjectApp.isPhoneNotSupportedFace()) {
            showErrorDialog(mContext, mContext.getString(R.string.common_22));
        }
    }

    private void showDeactivationResultDialog(){
        DeactivationResultDialog deactivationResultDialog = new DeactivationResultDialog(mContext);
        deactivationResultDialog.showWithBottomAnim();
    }

    private void loginRequest() {
        password = etPassword.getText().toString().trim();
        showProgress(true);
        AppClient.getInstance().getUserManager().checkLoginPassword(Constants.LOGIN_PD, password, new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {
                showProgress(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showDeactivationResultDialog();
                    }
                }, 100);
                dismiss();
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                showProgress(false);
                etPassword.setError(errorMsg);
            }
        });
    }

    //生物认证的setting
    private void showBiometricPrompt() {
        BiometricPrompt.PromptInfo promptInfo =
                new BiometricPrompt.PromptInfo.Builder()
                        .setTitle(mContext.getString(R.string.common_40)) //设置大标题
                        .setSubtitle(mContext.getString(R.string.common_41)) // 设置标题下的提示
                        .setNegativeButtonText(mContext.getString(R.string.common_6)) //设置取消按钮
                        .build();
        //需要提供的参数callback
        BiometricPrompt biometricPrompt = new BiometricPrompt((FragmentActivity) mContext,
                executor, new BiometricPrompt.AuthenticationCallback() {
            //各种异常的回调
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(ProjectApp.mContext,
                        mContext.getString(R.string.common_42) + ": " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            //认证成功的回调
            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                BiometricPrompt.CryptoObject authenticatedCryptoObject =
                        result.getCryptoObject();
                showToast(mContext.getString(R.string.Hint_FingerprintOpenSuccessful));
                AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(true);
                dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showDeactivationResultDialog();
                    }
                }, 100);
            }

            //认证失败的回调
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(ProjectApp.mContext, mContext.getString(R.string.common_44),
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            FingerPrintHelper mFingerPrintHelper = FingerPrintHelper.newInstance(mContext);
            if (mFingerPrintHelper == null) {
                return;
            }
            String mSecretKeyName = AppClient.getInstance().getUserID() + BiometricManager.FINGERPRINT_SECRET_KEY_LOGIN;
            Cipher cipher = mFingerPrintHelper.getCipherIfFingerPrintNoChange(mSecretKeyName);
            if (cipher == null) {
                cipher = mFingerPrintHelper.generateNewSecretKey(mSecretKeyName);
                if (cipher == null) {
                    LogUtil.e("cipher == null");
                    return;
                }
            }
            BiometricPrompt.CryptoObject crypto = new BiometricPrompt.CryptoObject(cipher);
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo, crypto);
        } else {
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo);
        }
    }
}
