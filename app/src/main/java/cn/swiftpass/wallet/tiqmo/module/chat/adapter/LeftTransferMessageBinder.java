package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseItemDataBinder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class LeftTransferMessageBinder extends BaseItemDataBinder<MessageEntity> {
    private String searchTerm = "";
    @Override
    public int getViewType() {
        return MessageAdapter.LeftTransferMessage;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_chat_left_send_money;
    }

    @Override
    public void bindData(BaseViewHolder baseViewHolder, MessageEntity messageEntity, int position) {
        baseViewHolder.setText(R.id.tv_right_time, TimeUtils.getChatTime(messageEntity.getTimestamp()));
        baseViewHolder.setText(R.id.tv_right_name, messageEntity.getUser().getName());
        baseViewHolder.setText(R.id.tv_right_message, messageEntity.getSimpleMessageToDisplay(searchTerm));
        baseViewHolder.setBackgroundRes(R.id.con_reply_msg, ThemeSourceUtils.getSourceID(mContext,R.attr.shape_071b55_white_16));
        if(!TextUtils.isEmpty(messageEntity.getReplyMessage())){
            baseViewHolder.setGone(R.id.view_reply_message,false);
            baseViewHolder.setText(R.id.tv_send_name, messageEntity.getReplyName());
            baseViewHolder.setText(R.id.tv_send_message, messageEntity.getReplyMessage());
            baseViewHolder.addOnClickListener(R.id.tv_send_show_details);
            ImageView ivReply = baseViewHolder.getView(R.id.iv_reply_image);
        }else{
            baseViewHolder.setGone(R.id.view_reply_message,true);
        }
    }
}
