package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillCheckoutPresenter implements PayBillContract.BillOrderCheckoutPresenter {

    PayBillContract.BillOrderCheckoutView billOrderCheckoutView;

    @Override
    public void fetchBill(String payAmount, String billAmount, String billProcessingFeePayAmount, String billProcessingFeeAmount, String channelCode) {
        AppClient.getInstance().getTransferManager().fetchBill(payAmount, billAmount, billProcessingFeePayAmount, billProcessingFeeAmount, channelCode, new LifecycleMVPResultCallback<PayBillOrderInfoEntity>(billOrderCheckoutView, true) {
            @Override
            protected void onSuccess(PayBillOrderInfoEntity result) {
                billOrderCheckoutView.fetchBillSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billOrderCheckoutView.fetchBillFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(billOrderCheckoutView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                billOrderCheckoutView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billOrderCheckoutView.checkOutFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillOrderCheckoutView billOrderCheckoutView) {
        this.billOrderCheckoutView = billOrderCheckoutView;
    }

    @Override
    public void detachView() {
        this.billOrderCheckoutView = null;
    }
}
