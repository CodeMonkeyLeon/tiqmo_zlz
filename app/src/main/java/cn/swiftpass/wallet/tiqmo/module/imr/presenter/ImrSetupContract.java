package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class ImrSetupContract {

    public interface ImrSetupView extends BaseView<IImrSetupPresenter> {
        void showSourceOfFund(List<SelectInfoEntity> data);
        void showProfession(List<SelectInfoEntity> data);
        void showCompanyType(List<SelectInfoEntity> data);
        void showSalaryRange(List<SelectInfoEntity> data);
        void showIMRInfoSaveSuccess();
        void showErrorMsg(String errorCode, String errorMsg);
        void getUserInfoSuccess(UserInfoEntity userInfoEntity);
        void updateUserInfoSuccess(UserInfoEntity result);
    }

    public interface IImrSetupPresenter extends BasePresenter<ImrSetupView> {
        void getSourceOfFund();
        void getProfession();
        void getCompanyType();
        void getSalaryRange();
        void getUserInfo();
        void updateUserInfo(UserInfoEntity userInfo);

    }
}
