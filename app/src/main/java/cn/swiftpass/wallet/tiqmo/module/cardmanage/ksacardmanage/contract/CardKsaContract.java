package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardListEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.MapResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;

public class CardKsaContract {

    public interface GetOtpTypePresenter<V extends BaseView> extends BasePresenter<V>{
        void getOtpType(String category, String categoryId, List<String> additionalData);
    }

    public interface GetOtpTypeView<V extends BasePresenter> extends BaseView<V>{
        void getOtpTypeSuccess(RechargeOrderInfoEntity riskControlEntity);

        void getOtpTypeFail(String errCode, String errMsg);
    }

    public interface MapSearchPresenter<V extends BaseView> extends BasePresenter<V>{
        void mapSearch(String address,String key);
    }

    public interface MapSearchView<V extends BasePresenter> extends BaseView<V>{
        void mapSearchSuccess(MapResultEntity result);

        void showErrorMsg(String errCode, String errMsg);
    }

    public interface OpenCardView extends BaseView<CardKsaContract.OpenCardPresenter> {
        void showErrorMsg(String errorCode, String errorMsg);

        void openKsaCardAccountSuccess(Void result);

        void getKsaCardSummarySuccess(KsaCardSummaryEntity result);
    }

    public interface OpenCardPresenter extends BasePresenter<CardKsaContract.OpenCardView> {
        void openKsaCardAccount(String productType);
        void getKsaCardSummary(String cardType,String cardProductType);
    }

    public interface AddCardAddressView extends MapSearchView<CardKsaContract.AddCardAddressPresenter> {
        void showCityListDia(CityEntity result);

        void getStateListSuccess(StateListEntity result);
    }

    public interface AddCardAddressPresenter extends MapSearchPresenter<CardKsaContract.AddCardAddressView> {
        void getCityList(String statesId);

        void getStateList();
    }

    public interface GetCardSummaryView extends GetOtpTypeView<CardKsaContract.GetCardSummaryPresenter> {
        void showErrorMsg(String errorCode, String errorMsg);

        void getKsaCardSummarySuccess(KsaCardSummaryEntity result);

        void getKsaCardPayResultSuccess(KsaPayResultEntity result);

        void setKsaCardPinSuccess(Void result);
    }

    public interface GetCardSummaryPresenter extends GetOtpTypePresenter<CardKsaContract.GetCardSummaryView> {
        void getKsaCardSummary(String cardType,String cardProductType);

        void getKsaCardPayResult(String vat, String totalAmount,
                                 OpenCardReqEntity appNiOpenCardReq);

        void setKsaCardPin(String pin,String proxyCardNo);
    }

    public interface CardListView extends GetOtpTypeView<CardListPresenter> {
        void getCardListSuccess(KsaCardListEntity ksaCardListEntity);

        void getCardListFail(String errorCode, String errorMsg);

        void getHistoryListSuccess(TransferHistoryMainEntity result);

        void getHistoryDetailSuccess(TransferHistoryDetailEntity result);

        void deleteTransferHistorySuccess(Void result);

        void getHistoryListFail(String errorCode, String errorMsg);

        void getKsaCardDetailsSuccess(CardDetailEntity cardDetailEntity);

        void setKsaCardStatusSuccess(Void result);

        void getKsaLimitChannelSuccess(KsaLimitChannelEntity result);

        void showErrorMsg(String errorCode, String errorMsg);

        void activateCardSuccess(Void result);
        void activateCardResultSuccess(Void result);
    }

    public interface CardListPresenter extends GetOtpTypePresenter<CardListView> {

        void getCardList();

        void getHistoryList(String protocolNo, String accountNo, List<String> paymentType,
                            List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum,
                            String direction, String orderNo);

        void getHistoryDetail(String orderNo, String orderType,String queryType);

        void deleteTransferHistory(String orderNo);

        void getKsaCardDetails(String proxyCardNo);

        void setKsaCardStatus(String status,
                              String proxyCardNo,
                              String reason,
                              String reasonStatus);

        void getKsaLimitChannel(String proxyCardNo);

        void activateCard(String proxyCardNo, String cardNo, String cardExpire);

        void activateCardResult();
    }

    public interface ActivateCardView extends GetOtpTypeView<CardKsaContract.ActivateCardPresener> {
        void activateCardSuccess(Void result);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface ActivateCardPresener extends GetOtpTypePresenter<CardKsaContract.ActivateCardView> {
        void activateCard(String proxyCardNo, String cardNo, String cardExpire);
    }

    public interface SetCardLimitView extends GetOtpTypeView<CardKsaContract.SetCardLimitPresener> {
        void getKsaLimitChannelSuccess(KsaLimitChannelEntity result);
        void setKsaCardLimitSuccess(Void result);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface SetCardLimitPresener extends GetOtpTypePresenter<CardKsaContract.SetCardLimitView> {
        void getKsaLimitChannel(String proxyCardNo);

        void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue,String proxyCardNo,String up);
    }

    public interface SetCardPinView extends GetOtpTypeView<CardKsaContract.SetCardPinPresener> {
        void setKsaCardPinSuccess(Void result);
        void setKsaCardStatusSuccess(Void result);
        void setKsaCardLimitSuccess(Void result);
        void setKsaCardChannelSuccess(Void result);
        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface SetCardPinPresener extends GetOtpTypePresenter<CardKsaContract.SetCardPinView> {
        void setKsaCardPin(String pin,String proxyCardNo);
        void setKsaCardStatus(String status,
                              String proxyCardNo,
                              String reason,String reasonStatus);
        void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue,String proxyCardNo,String up);
        void setKsaCardChannel(String txnType,String txnTypeEnabled,String proxyCardNo);
    }
}
