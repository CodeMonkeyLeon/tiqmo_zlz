package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class CardFilterDialog extends BottomDialog {

    private Context mContext;

    //1 store purchase  2 online purchase
    private int channelType = 1;
    //1 Last 30 days  2 Last 60 days
    private int timeType = 1;


    private ApplyFilterListener mApplyFilterListener;

    public CardFilterDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(context);
    }

    public void setType(final int channelType, int timeType) {
        this.channelType = channelType;
        this.timeType = timeType;
    }

    public interface ApplyFilterListener {
        void changeFilter(int channelType, int timeType);
    }

    public void setApplyFilterListener(ApplyFilterListener applyFilterListener) {
        this.mApplyFilterListener = applyFilterListener;
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_card_filter, null);
        TextView tvThirtyDay = mView.findViewById(R.id.tv_thirty_day);
        TextView tvSixtyDay = mView.findViewById(R.id.tv_sixty_day);
        TextView tvOnlinePurchase = mView.findViewById(R.id.tv_online_purchase);
        TextView tvStorePurchase = mView.findViewById(R.id.tv_store_purchase);
        TextView tvCancel = mView.findViewById(R.id.tv_cancel);
        TextView tvConfirm = mView.findViewById(R.id.tv_confirm);

        tvThirtyDay.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                tvThirtyDay.setSelected(true);
                tvSixtyDay.setSelected(false);
            }
        });

        tvSixtyDay.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                tvThirtyDay.setSelected(false);
                tvSixtyDay.setSelected(true);
            }
        });

        tvOnlinePurchase.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                tvOnlinePurchase.setSelected(true);
                tvStorePurchase.setSelected(false);
            }
        });

        tvStorePurchase.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                tvOnlinePurchase.setSelected(false);
                tvStorePurchase.setSelected(true);
            }
        });

        tvCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });

        tvConfirm.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mApplyFilterListener != null) {
                    mApplyFilterListener.changeFilter(channelType, timeType);
                }
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
