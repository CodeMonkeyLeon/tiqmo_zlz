package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class TransactionHistoryFragment extends BaseHistoryFragment {


    public static TransactionHistoryFragment getInstance(String isFrom) {
        TransactionHistoryFragment historyFragment = new TransactionHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.isFrom, isFrom);
        bundle.putBoolean(Constants.HISTORY_TYPE, false);
        historyFragment.setArguments(bundle);
        return historyFragment;
    }


    @Override
    protected void initData() {
        noticeThemeChange();
        EventBus.getDefault().postSticky(
                new EventEntity(
                        EventEntity.EVENT_HISTORY_INIT_DATA,
                        "transaction initData"
                )
        );
    }
}
