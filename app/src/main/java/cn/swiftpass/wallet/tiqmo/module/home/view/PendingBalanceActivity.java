package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class PendingBalanceActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_your_balance)
    TextView tvYourBalance;
    @BindView(R.id.tv_pending_balance)
    TextView tvPendingBalance;
    @BindView(R.id.tv_got_it)
    TextView tvGotIt;
    @BindView(R.id.tv_content_three)
    TextView tvContentThree;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_content_two)
    TextView tvContentTwo;
    @BindView(R.id.tv_content_four)
    TextView tvContentFour;

    private BalanceEntity pendingBalance;
    private String totalMonthLimitAmt;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_pending_balance;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.sprint11_48);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        try {
            tvContentThree.setText(Spans.builder()
                    .text(mContext.getString(R.string.sprint11_53_3)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_6b6c73))).size(14)
                    .build());

            if (getIntent() != null && getIntent().getExtras() != null) {
                pendingBalance = (BalanceEntity) getIntent().getExtras().getSerializable(Constants.balanceEntity);
                if (pendingBalance != null) {
                    totalMonthLimitAmt = pendingBalance.totalMonthLimitAmt;
                    totalMonthLimitAmt = AmountUtil.dataFormatNumber(BigDecimalFormatUtils.add(totalMonthLimitAmt, "0", 0));
                    tvContentOne.setText(getString(R.string.sprint11_51).replace("20,000", totalMonthLimitAmt));
                    tvContentTwo.setText(getString(R.string.sprint11_52).replace("20,000", totalMonthLimitAmt));
                    tvContentFour.setText(getString(R.string.sprint11_53).replace("20,000", totalMonthLimitAmt));
                    tvYourBalance.setText(getUserInfoEntity().getBalance() + " " + LocaleUtils.getCurrencyCode(""));
                    tvPendingBalance.setText(pendingBalance.getBalance() + " " + LocaleUtils.getCurrencyCode(""));
                }
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_got_it})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_got_it:
                ProjectApp.closePendingBalance = true;
                finish();
                break;
            default:break;
        }
    }
}
