package cn.swiftpass.wallet.tiqmo.module.chat.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SplitCheckedAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SplitContactAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class ChatContactDialog extends BottomDialog {

    TextView tvTitle;
    RecyclerView rlContacts;
    TextView idDialog;
    SideBar idSideBar;
    ConstraintLayout conRecycler;
    LinearLayout llGroupName;
    TextView tvGroupName;
    RecyclerView rlCheckedContacts;
    ConstraintLayout conChecked;
    TextView tvCreate;
    LinearLayout llContent;
    CustomizeEditText etPhone;
    TextView tvNoPermission;
    TextView tvContacts;
    ImageView ivNoPermission;
    ConstraintLayout conInvite;
    RecyclerView rlInvite;
    TextView tvNewGroup;
    private Context mContext;
    private static ChatContactDialog chatContactDialog = null;
    //窗口类型,1 =all,2= individual 3 = groups
    private int chatType = 0;

    private SplitContactAdapter splitContactAdapter;
    private SplitCheckedAdapter splitCheckedAdapter;

    private ChatContactDialog.OnAddClickListener onAddClickListener;
    private ChatContactDialog.TvInviteClickListener tvInviteClickListener;

    private ChatContactDialog.CreateChatRoom createChatRoom;

    private ChatContactDialog.CreateNewGroupListener createNewGroupListener;

    private LinearLayoutManager mLayoutManager, mCheckedLayoutManager;

    private StatusView statusView;

    public List<KycContactEntity> phoneList = new ArrayList<>();
    public List<KycContactEntity> inviteList = new ArrayList<>();
    public List<KycContactEntity> checkedPhoneList;
    private List<KycContactEntity> filterContactList = new ArrayList<>();
    private int splitReceiversLimit;
    private boolean meChecked;

    private boolean isAdd;
    private String phone;
    private UserInfoEntity userInfoEntity;
    private boolean isSearchEmpty;
    private TextView tvContinue;
    private ImageView ivBack;
    private String groupName;

    public void setGroupName(final String groupName) {
        this.groupName = groupName;
        if(tvGroupName != null){
            tvGroupName.setText(groupName);
        }
    }

    public void setMeChecked(boolean meChecked) {
        this.meChecked = meChecked;
    }

    public void setSplitReceiversLimit(int splitReceiversLimit) {
        this.splitReceiversLimit = splitReceiversLimit;
    }

    public void setPhoneList(List<KycContactEntity> phoneList) {
        this.phoneList = phoneList;
        checkedPhoneList = new ArrayList<>();
//        checkedPhoneList.addAll(peopleList);
        initRecyclerView(phoneList);
    }

    public void setInviteList(List<KycContactEntity> inviteList){
        this.inviteList = inviteList;
    }

    private void initRecyclerView(List<KycContactEntity> phoneList) {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        View noContentView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ImageView iv_no_content = noContentView.findViewById(R.id.iv_no_content);
        TextView tv_no_content = noContentView.findViewById(R.id.tv_no_content);
        iv_no_content.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
        tv_no_content.setText(mContext.getString(R.string.wtw_18_1));
        statusView = new StatusView.Builder(mContext, conRecycler)
                .setNoContentView(noContentView).build();

        idSideBar.setTextView(idDialog);
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rlContacts.setLayoutManager(mLayoutManager);
        splitContactAdapter = new SplitContactAdapter(phoneList);
        splitContactAdapter.bindToRecyclerView(rlContacts);

        mCheckedLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 2f);
        rlCheckedContacts.addItemDecoration(MyItemDecoration.createHorizontal(mContext.getColor(R.color.transparent), ryLineSpace));
        rlCheckedContacts.setLayoutManager(mCheckedLayoutManager);
        splitCheckedAdapter = new SplitCheckedAdapter(checkedPhoneList);
        splitCheckedAdapter.bindToRecyclerView(rlCheckedContacts);

        if (checkedPhoneList.size() > 0) {
            conChecked.setVisibility(View.VISIBLE);
            tvCreate.setEnabled(true);
        } else {
            tvCreate.setEnabled(false);
            conChecked.setVisibility(View.GONE);
        }

        splitCheckedAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                switch (view.getId()) {
                    case R.id.iv_delete:
                        KycContactEntity kycContactEntity = splitCheckedAdapter.getDataList().get(position);
                        if (phoneList.contains(kycContactEntity)) {
                            kycContactEntity.isChecked = false;
                            splitContactAdapter.notifyDataSetChanged();
                        }
                        if (checkedPhoneList.contains(kycContactEntity)) {
                            checkedPhoneList.remove(kycContactEntity);
                        }
                        if (checkedPhoneList.size() > 0) {
                            conChecked.setVisibility(View.VISIBLE);
                            tvCreate.setEnabled(true);
                            splitCheckedAdapter.notifyDataSetChanged();
                        } else {
                            tvCreate.setEnabled(false);
                            conChecked.setVisibility(View.GONE);
                        }
                        break;

                    default:
                        break;
                }
            }
        });

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = splitContactAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (mLayoutManager != null) {
                        mLayoutManager.scrollToPositionWithOffset(position, 0);
                    }
                }
            }
        });

        splitContactAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                KycContactEntity kycContactEntity = splitContactAdapter.getDataList().get(position);
                switch (view.getId()) {
                    case R.id.tv_invite:
                        if (tvInviteClickListener != null) {
//                            if ("invited".equals(kycContactEntity.getInviteStatus())) {
//                                return;
//                            }
                            tvInviteClickListener.sendInvite(kycContactEntity.getRequestPhoneNumber());
//                            kycContactEntity.setInviteStatus("invited");
//                            splitContactAdapter.notifyItemChanged(position);
                        }
                        break;
                    case R.id.iv_avatar:
                    case R.id.tv_user_name:
                    case R.id.tv_phone:
                        if(!kycContactEntity.isRegistered())return;
                        if (chatType == 1  || chatType ==2){
                            createChatRoom.createChatRoom(kycContactEntity.getUserId());
                            return;
                        }
                        if (kycContactEntity.isChecked) {
                            if (checkedPhoneList.contains(kycContactEntity)) {
                                kycContactEntity.isChecked = false;
                                checkedPhoneList.remove(kycContactEntity);
                            }
                        } else {
                            kycContactEntity.isChecked = true;
                            if (!checkedPhoneList.contains(kycContactEntity)) {
                                checkedPhoneList.add(kycContactEntity);
                            }
                        }
                        splitContactAdapter.notifyItemChanged(position);
                        if (checkedPhoneList.size() > 0) {
                            conChecked.setVisibility(View.VISIBLE);
                            tvCreate.setEnabled(true);
                            splitCheckedAdapter.notifyDataSetChanged();
                        } else {
                            tvCreate.setEnabled(false);
                            conChecked.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        });


        if (phoneList.size() == 0) {
            statusView.showEmpty();
        } else {
            statusView.dismissLoading();
        }
    }

    public void setOnAddClickListener(ChatContactDialog.OnAddClickListener onAddClickListener) {
        this.onAddClickListener = onAddClickListener;
    }

    public void setTvInviteClickListener(ChatContactDialog.TvInviteClickListener tvInviteClickListener){
        this.tvInviteClickListener = tvInviteClickListener;
    }

    public void setCreateChatRoom(ChatContactDialog.CreateChatRoom createChatRoom){
        this.createChatRoom = createChatRoom;
    }

    public void setCreateNewGroupListener(ChatContactDialog.CreateNewGroupListener createNewGroupListener){
        this.createNewGroupListener = createNewGroupListener;
    }

    public ChatContactDialog(Context context,int chatType) {
        super(context);
        this.mContext = context;
        this.chatType = chatType;
        initViews(mContext);
    }

    public static ChatContactDialog getInstance(Context context,int type) {
        chatContactDialog = new ChatContactDialog(context,type);
        return chatContactDialog;
    }

    public interface OnAddClickListener {
        void getSplitList(List<KycContactEntity> peopleList);
    }

    public interface TvInviteClickListener {
        void sendInvite(String phone);
    }

    public interface CreateChatRoom{
        void createChatRoom(String phone);
    }

    public interface CreateNewGroupListener{
        void createNewGroup();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_chat_contact, null);

        tvTitle = view.findViewById(R.id.tv_title);
        rlContacts = view.findViewById(R.id.rl_contacts);
        idDialog = view.findViewById(R.id.id_dialog);
        idSideBar = view.findViewById(R.id.id_sideBar);
        conRecycler = view.findViewById(R.id.con_recycler);
        llGroupName = view.findViewById(R.id.ll_group_name);
        tvGroupName = view.findViewById(R.id.tv_group_name);
        rlCheckedContacts = view.findViewById(R.id.rl_checked_contacts);
        conChecked = view.findViewById(R.id.con_checked);
        tvCreate = view.findViewById(R.id.tv_create);
        llContent = view.findViewById(R.id.ll_content);
        tvContinue = view.findViewById(R.id.tv_continue);
        etPhone = view.findViewById(R.id.et_phone);
        tvNoPermission = view.findViewById(R.id.tv_no_permission);
        tvContacts = view.findViewById(R.id.tv_contacts);
        ivNoPermission = view.findViewById(R.id.iv_no_permission);
        tvNewGroup = view.findViewById(R.id.tv_new_group);
        ivBack = view.findViewById(R.id.iv_back);
        LocaleUtils.viewRotationY(mContext,ivBack);
        if (chatType == 1) {
            tvNewGroup.setVisibility(View.VISIBLE);
            tvCreate.setVisibility(View.GONE);
        }else if (chatType == 2){
            tvNewGroup.setVisibility(View.INVISIBLE);
            tvCreate.setVisibility(View.GONE);
        }else if (chatType == 3){
            tvNewGroup.setVisibility(View.INVISIBLE);
            tvCreate.setVisibility(View.VISIBLE);
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvNewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewGroupListener.createNewGroup();
            }
        });

        etPhone.addTextChangedListener(new SimpleTextWatcher(){
            @Override
            public void afterTextChanged(Editable s) {
                phone = s.toString();
                etPhone.setError("");
                if (TextUtils.isEmpty(phone)) {
                    tvContinue.setVisibility(View.GONE);
                } else {
                    if (phone.equals(userInfoEntity.getPhone())) {
                        tvContinue.setVisibility(View.GONE);
                    } else if (RegexUtil.isPhone(phone)) {
                        tvContinue.setVisibility(View.GONE);
                    } else {
                        tvContinue.setVisibility(View.GONE);
                    }
                }
                if (phoneList != null && phoneList.size() > 0) {
                    searchFilterWithAllList(phone);
                }
            }
        });

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KycContactEntity kycContactEntity = new KycContactEntity();
                if (filterContactList.size() ==1) {
                    kycContactEntity = filterContactList.get(0);
                }else {
                    kycContactEntity.setPhone(etPhone.getText().toString());
                    kycContactEntity.setContactsName(etPhone.getText().toString());
                }

                if (chatType == 1  || chatType ==2){
                    createChatRoom.createChatRoom(kycContactEntity.getRequestPhoneNumber());
                    return;
                }

                if (kycContactEntity.isChecked) {
                    kycContactEntity.isChecked = false;
                    if (checkedPhoneList.contains(kycContactEntity)) {
                        checkedPhoneList.remove(kycContactEntity);
                    }
                } else {
                    kycContactEntity.isChecked = true;
                    if (!checkedPhoneList.contains(kycContactEntity)) {
                        checkedPhoneList.add(kycContactEntity);
                    }
                }
                splitContactAdapter.notifyDataSetChanged();
                if (checkedPhoneList.size() > 0) {
                    conChecked.setVisibility(View.VISIBLE);
                    tvCreate.setEnabled(true);
                    splitCheckedAdapter.notifyDataSetChanged();
                } else {
                    tvCreate.setEnabled(false);
                    conChecked.setVisibility(View.GONE);
                }
                etPhone.handleEndIconClick();
            }
        });

        tvCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onAddClickListener != null) {
                    isAdd = true;
                    onAddClickListener.getSplitList(checkedPhoneList);
                }
//                dismiss();
            }
        });



        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (chatContactDialog != null) {
                    chatContactDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }

    }

    private void searchFilterWithAllList(String filterStr) {

        try {
            filterContactList.clear();
            //去除空格的匹配规则
            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            if (TextUtils.isEmpty(filterStr)) {
                idSideBar.setVisibility(View.VISIBLE);
                filterContactList.clear();
                tvNoPermission.setVisibility(View.GONE);
                ivNoPermission.setVisibility(View.GONE);
                rlContacts.setVisibility(View.VISIBLE);
                tvContacts.setVisibility(View.VISIBLE);
                splitContactAdapter.setDataList(phoneList);
                return;
            } else {
                idSideBar.setVisibility(View.GONE);
            }
            filterContactList.clear();
            if (phoneList != null && phoneList.size() > 0) {
                int size = phoneList.size();
                for (int i = 0; i < size; i++) {
                    KycContactEntity kycContactEntity = phoneList.get(i);
                    String name = kycContactEntity.getNameNoMapping() + kycContactEntity.getPhoneNoMapping();
                    String pinyinName = Pinyin.toPinyin(name, "");
                    String phoneNumber = kycContactEntity.getPhone();
                    if (pinyinName.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterContactList.add(kycContactEntity);
                    } else if (phoneNumber.startsWith("+966 " + filterStr)) {
                        filterContactList.add(kycContactEntity);
                    }
                }

                if (filterContactList.size() == 0) {
                    isSearchEmpty = true;
                    tvNoPermission.setVisibility(View.VISIBLE);
                    ivNoPermission.setVisibility(View.VISIBLE);
                    rlContacts.setVisibility(View.GONE);
                    tvContacts.setVisibility(View.GONE);
                } else {
                    isSearchEmpty = false;
                    tvNoPermission.setVisibility(View.GONE);
                    ivNoPermission.setVisibility(View.GONE);
                    rlContacts.setVisibility(View.VISIBLE);
                    tvContacts.setVisibility(View.VISIBLE);
                }
                splitContactAdapter.setDataList(filterContactList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
