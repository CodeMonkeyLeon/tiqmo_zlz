package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class EVoucherHistoryActivity extends BaseCompatActivity {


    @BindView(R.id.fl_evoucher_history)
    FrameLayout flEvoucherHistory;

    private HistoryFragment historyFragment;

    private FilterEntity filterEntity;
    public List<String> tradeType = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_evoucher_history;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null) {
            tradeType.add("15");
            filterEntity = new FilterEntity();
            filterEntity.tradeType = tradeType;
            historyFragment = HistoryFragment.getInstance(filterEntity, Constants.from_evoucher);

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_evoucher_history, historyFragment);
            ft.commit();
        } else {
            historyFragment = HistoryFragment.getInstance(filterEntity, Constants.from_notification_center);

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_evoucher_history, historyFragment);
            ft.commit();
        }
    }

}
