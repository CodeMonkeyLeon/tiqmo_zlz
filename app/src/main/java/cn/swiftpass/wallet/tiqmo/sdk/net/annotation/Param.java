package cn.swiftpass.wallet.tiqmo.sdk.net.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by YZX on 2018年12月17日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */

//参数注解
@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Param {
    String value() ;

}