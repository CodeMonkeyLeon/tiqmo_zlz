package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillCountryEntity extends BaseEntity {
    public String countryName;
    public String countryCode;
    public String countryLogo;
    public String countryAreaCode;

    public String countryType;

    public String selected = "0";
}
