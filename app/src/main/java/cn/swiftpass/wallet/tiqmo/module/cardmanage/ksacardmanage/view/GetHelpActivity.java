package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;

public class GetHelpActivity extends BaseCompatActivity {

    @BindView(R.id.fl_get_help)
    FrameLayout flGetHelp;


    private boolean isHideTitle,isActivity;
    private String hideTitle, topTitle;
    private GetHelpFragment getHelpFragment;
    @Override
    protected int getLayoutID() {
        return R.layout.activity_get_help;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();

        if (getIntent() != null && getIntent().getExtras() != null) {
            isHideTitle = getIntent().getBooleanExtra("isHideTitle", false);
            hideTitle = getIntent().getStringExtra("hideTitle");
            topTitle = getIntent().getStringExtra("topTitle");
            getHelpFragment = GetHelpFragment.getInstance(isHideTitle,hideTitle,topTitle,true);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_get_help,getHelpFragment);
            ft.commit();

        }
    }

}
