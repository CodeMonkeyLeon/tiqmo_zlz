package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

public class KsaCardFilterDialog extends BottomDialog {
    private Context mContext;

    LinearLayout llDateTitle;
    EditTextWithDel etDateFrom;
    ImageView ivDateFrom;
    ImageView ivDateFromDelete;
    ImageView ivDateToDelete;
    ImageView ivCategoryArrow;
    ImageView ivDateArrow;
    EditTextWithDel etDateTo;
    ImageView ivDateTo;
    LinearLayout llDate;
    TextView tvCancel;
    TextView tvConfirm;
    TextView tvError;

    private Date mSelectedFromDate;
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedFromDate = Calendar.getInstance();
    Calendar startFromDate = Calendar.getInstance();
    Calendar endFromDate = Calendar.getInstance();

    private Date mSelectedToDate;
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedToDate = Calendar.getInstance();
    Calendar startToDate = Calendar.getInstance();
    Calendar endToDate = Calendar.getInstance();

    private ApplyFilterListener mApplyFilterListener;

    private String startDate = "", endDate = "";
    private boolean isShowDateError;

    private FilterEntity filterEntity = new FilterEntity();

    public HashMap<String, String> charMap = new HashMap<>();

    public KsaCardFilterDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(context);
    }

    public void setFilterEntity(FilterEntity filterEntity) {
        this.filterEntity = filterEntity;
        setFilter(filterEntity);
    }

    public interface ApplyFilterListener {
        void changeFilter(FilterEntity filterEntity);
    }

    public void setApplyFilterListener(ApplyFilterListener applyFilterListener) {
        this.mApplyFilterListener = applyFilterListener;
    }

    private void setFilter(FilterEntity filterEntity) {
        if (filterEntity != null) {

            mSelectedFromDate = filterEntity.fromDate;
            mSelectedToDate = filterEntity.toDate;

            startDate = filterEntity.startDate;
            String showStartDate = filterEntity.showStartDate;
            String showEndDate = filterEntity.showEndDate;
            if (!TextUtils.isEmpty(showStartDate)) {
                etDateFrom.getEditText().setText(showStartDate);
                ivDateFrom.setVisibility(View.GONE);
                ivDateFromDelete.setVisibility(View.VISIBLE);
            } else {
                etDateFrom.getEditText().setText("");
                mSelectedFromDate = null;
                startDate = "";
            }

            endDate = filterEntity.endDate;
            if (!TextUtils.isEmpty(showEndDate)) {
                etDateTo.getEditText().setText(showEndDate);
                ivDateTo.setVisibility(View.GONE);
                ivDateToDelete.setVisibility(View.VISIBLE);
            } else {
                etDateTo.getEditText().setText("");
                mSelectedToDate = null;
                endDate = "";
            }
        }
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_filter_ksacard_history, null);
        llDateTitle = mView.findViewById(R.id.ll_date_title);
        etDateFrom = mView.findViewById(R.id.et_date_from);
        ivDateFrom = mView.findViewById(R.id.iv_date_from);
        ivDateFromDelete = mView.findViewById(R.id.iv_date_from_delete);
        ivDateToDelete = mView.findViewById(R.id.iv_date_to_delete);
        ivCategoryArrow = mView.findViewById(R.id.iv_category_arrow);
        ivDateArrow = mView.findViewById(R.id.iv_date_arrow);
        etDateTo = mView.findViewById(R.id.et_date_to);
        ivDateTo = mView.findViewById(R.id.iv_date_to);
        llDate = mView.findViewById(R.id.ll_date);
        tvCancel = mView.findViewById(R.id.tv_cancel);
        tvConfirm = mView.findViewById(R.id.tv_confirm);
        tvError = mView.findViewById(R.id.tv_error_date);

        etDateFrom.getTlEdit().setHint(mContext.getString(R.string.History_filter_8));
        etDateTo.getTlEdit().setHint(mContext.getString(R.string.History_filter_9));

        charMap.put("January", "Jan");
        charMap.put("February", "Feb");
        charMap.put("March", "Mar");
        charMap.put("April", "Apr");
        charMap.put("May", "May");
        charMap.put("June", "Jun");
        charMap.put("July", "Jul");
        charMap.put("August", "Aug");
        charMap.put("September", "Sept");
        charMap.put("October", "Oct");
        charMap.put("November", "Nov");
        charMap.put("December", "Dec");

        etDateFrom.getEditText().setCursorVisible(false);
        etDateFrom.getEditText().setInputType(InputType.TYPE_NULL);
        etDateFrom.getEditText().setFocusable(false);
        etDateFrom.getEditText().setFocusableInTouchMode(false);
        etDateTo.getEditText().setCursorVisible(false);
        etDateTo.getEditText().setInputType(InputType.TYPE_NULL);
        etDateTo.getEditText().setFocusable(false);
        etDateTo.getEditText().setFocusableInTouchMode(false);

        startFromDate.set(Calendar.YEAR, selectedFromDate.get(Calendar.YEAR) - 90);
        endFromDate.set(Calendar.YEAR, selectedFromDate.get(Calendar.YEAR));

        startToDate.set(Calendar.YEAR, selectedToDate.get(Calendar.YEAR) - 90);
        endToDate.set(Calendar.YEAR, selectedToDate.get(Calendar.YEAR));

        ivDateFromDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDateFrom.getEditText().setText(null);
                mSelectedFromDate = null;
                startDate = "";
                tvConfirm.setEnabled(true);
                tvError.setVisibility(View.GONE);
                isShowDateError = false;
                ivDateFromDelete.setVisibility(View.GONE);
                ivDateFrom.setVisibility(View.VISIBLE);
            }
        });

        ivDateToDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDateTo.getEditText().setText(null);
                tvConfirm.setEnabled(true);
                mSelectedToDate = null;
                endDate = "";
                tvError.setVisibility(View.GONE);
                isShowDateError = false;
                ivDateToDelete.setVisibility(View.GONE);
                ivDateTo.setVisibility(View.VISIBLE);
            }
        });

        etDateFrom.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFromDatetime();
            }
        });

        etDateTo.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectToDatetime();
            }
        });

        tvCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                dismiss();
            }
        });

        tvConfirm.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                filterEntity.startDate = startDate;
                filterEntity.fromDate = mSelectedFromDate;
                filterEntity.toDate = mSelectedToDate;
                filterEntity.showStartDate = etDateFrom.getEditText().getText().toString().trim();
                filterEntity.endDate = endDate;
                filterEntity.showEndDate = etDateTo.getEditText().getText().toString().trim();
                if (mApplyFilterListener != null) {
                    mApplyFilterListener.changeFilter(filterEntity);
                }
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private void selectFromDatetime() {
        if (mSelectedFromDate == null) {
            selectedFromDate.setTime(endFromDate.getTime());
        } else {
            selectedFromDate.setTime(mSelectedFromDate);
        }
        TimePickerDialogUtils.showTimePicker(getContext(), selectedFromDate, startFromDate, endFromDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedFromDate = date;
                ivDateFromDelete.setVisibility(View.VISIBLE);
                ivDateFrom.setVisibility(View.GONE);
                startDate = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(date);
                if (!TextUtils.isEmpty(endDate)) {
                    boolean big = AndroidUtils.compareDate(mSelectedFromDate, mSelectedToDate);
                    if (!big) {
                        tvError.setVisibility(View.VISIBLE);
                        isShowDateError = true;
                        tvConfirm.setEnabled(false);
                    } else {
                        tvConfirm.setEnabled(true);
                        tvError.setVisibility(View.GONE);
                        isShowDateError = false;
                    }
                }
                try {
                    String monthKey = startDate.split(" ")[0];
                    String month = startDate.replace(monthKey, charMap.get(monthKey));
                    etDateFrom.getEditText().setText(month);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        });
    }

    private void selectToDatetime() {
        if (mSelectedToDate == null) {
            selectedToDate.setTime(endToDate.getTime());
        } else {
            selectedToDate.setTime(mSelectedToDate);
        }
        TimePickerDialogUtils.showTimePicker(getContext(), selectedToDate, startToDate, endToDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedToDate = date;
                ivDateToDelete.setVisibility(View.VISIBLE);
                ivDateTo.setVisibility(View.GONE);
                endDate = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(date);
                if (!TextUtils.isEmpty(startDate)) {
                    boolean big = AndroidUtils.compareDate(mSelectedFromDate, mSelectedToDate);
                    if (!big) {
                        tvError.setVisibility(View.VISIBLE);
                        isShowDateError = true;
                        tvConfirm.setEnabled(false);
                    } else {
                        tvError.setVisibility(View.GONE);
                        isShowDateError = false;
                        tvConfirm.setEnabled(true);
                    }
                }
                try {
                    String monthKey = endDate.split(" ")[0];
                    String month = endDate.replace(monthKey, charMap.get(monthKey));
                    etDateTo.getEditText().setText(month);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        });
    }
}
