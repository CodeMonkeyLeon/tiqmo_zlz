package cn.swiftpass.wallet.tiqmo.sdk.listener;

/**
 * Created by YZX on 2018年10月24日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class CancelableResultCallback<T> implements ResultCallback<T>, Cancelable {

    private volatile boolean isCancel;
    private ResultCallback<T> mResultCallback;

    public CancelableResultCallback(ResultCallback<T> resultCallback) {
        mResultCallback = resultCallback;
    }

    @Override
    public void onResult(T result) {
        if (!isCancel && mResultCallback != null) {
            mResultCallback.onResult(result);
        }
    }

    @Override
    public void onFailure(String code, String error) {
        if (!isCancel && mResultCallback != null) {
            mResultCallback.onFailure(code, error);
        }
    }

    @Override
    public void cancel() {
        isCancel = true;
    }
}
