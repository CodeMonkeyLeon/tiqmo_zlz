package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;


/**
 * @author lizheng.zhao
 * @date 2022/07/13
 */
public class PayBillTypeListNewEntity extends BaseEntity {

    private List<PayBillTypeEntity> billTypeList;

    public void setBillTypeList(List<PayBillTypeEntity> billTypeList) {
        this.billTypeList = billTypeList;
    }

    public List<PayBillTypeEntity> getBillTypeList() {
        return billTypeList;
    }
}
