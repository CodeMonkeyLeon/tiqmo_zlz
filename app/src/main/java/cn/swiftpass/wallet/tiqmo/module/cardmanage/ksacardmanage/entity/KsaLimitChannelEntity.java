package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class KsaLimitChannelEntity extends BaseEntity {

    /**
     * instore ：POS和NFC
     * online: ECOM
     * ATM: ATM（开通了实体卡才有）
     */

    /**
     * 是否开启ecom交易	0=关闭,1=开启
     */
    public String ecom;
    /**
     * 是否开启atm交易	0=关闭,1=开启
     */
    public String atm;
    /**
     * 是否开启instore交易	0=关闭,1=开启
     */
    public String instore;
    //是否开启国际交易 0=关闭,1=开启
    public String nationtms;
    /**
     * 限额货币代码	用于显示(如有需要)
     */
    public String currAlphabeticCode;
    /**
     * 限额货币符号	用于显示(如有需要)
     */
    public String currSymbol;
    /**
     * 限额最小货币单位转换系数
     */
    public String convertUnit;

    public String proxyCardNo;
    /**
     * 开启状态的交易渠道
     */
    public List<String> enabledChannels = new ArrayList<>();
    /**
     * 关闭状态的交易渠道
     */
    public List<String> disabledChannels = new ArrayList<>();

    public List<CardLimitConfigEntity> limitConfig = new ArrayList<>();

    public class CardLimitConfigEntity extends BaseEntity {

        //	月限额最小值	5000.0
        public String monthlyLimitMin;
        //月限额最大值	10000.0
        public String monthlyLimitMax;
        //月限额范围国际化提示信息
        public String monthlyLimitRangeNote;
        //当前月限额	5000.0
        public String monthlyLimitValue;
        //日限额最小值	100.0
        public String dailyLimitMin;
        //	日限额最大值	1000.0
        public String dailyLimitMax;
        //	日限额范围国际化提示信息
        public String dailyLimitRangeNote;
        //交易类型	ATM/NFC/POS/ECOM
        public String txnType;
        //当前日限额	200.0
        public String dailyLimitValue;
        //已使用的日限额
        public String dailyLimitCnt;
        //已使用的月限额
        public String monthlyLimitCnt;

        public String proxyCardNo;

        public boolean isDaily;

        public String up;

        public String currency;

        public boolean isOnlineLimit(String txnType) {
            return "ecom".equals(txnType);
        }

        public boolean isAtmLimit(String txnType) {
            return "atm".equals(txnType);
        }

        public boolean isInstoreLimit(String txnType) {
            return "instore".equals(txnType);
        }
    }
}
