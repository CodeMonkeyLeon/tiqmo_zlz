package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import cn.swiftpass.wallet.tiqmo.module.home.contract.KycVerifyContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ReferralPresener implements KycVerifyContract.ReferralPresenter {

    KycVerifyContract.ReferralView baseView;

    @Override
    public void referralVerify(String referralCode) {
        AppClient.getInstance().referralVerify(referralCode, new LifecycleMVPResultCallback<Void>(baseView, true) {
            @Override
            protected void onSuccess(Void result) {
                baseView.referralVerifySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.referralVerifyFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(KycVerifyContract.ReferralView referralView) {
        this.baseView = referralView;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
