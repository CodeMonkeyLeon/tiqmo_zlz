package cn.swiftpass.wallet.tiqmo.module.transfer.contract;

import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.contract.GetOtpTypePresenter;
import cn.swiftpass.wallet.tiqmo.base.contract.GetOtpTypeView;
import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.QrCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.ScanOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferPwdTimeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class TransferContract {

    public interface TransferFeeView extends BaseView<TransferContract.TransferFeePresenter> {
        void getTransferFeeSuccess(TransFeeEntity transFeeEntity);

        void getTransferFeeFail(String errCode, String errMsg);

        void transferPaySuccess(TransferPayEntity transferPayEntity);

        void transferPayFail(String errCode, String errMsg);

        void addMoneyBindCardSuccess(CardBind3DSEntity cardBind3DSEntity);

        void addMoneyBindCardFail(String errCode, String errMsg);
    }

    public interface TransferFeePresenter extends BasePresenter<TransferContract.TransferFeeView> {
        void getTransferFee(String type, String orderAmount, String orderCurrencyCode);

        void transferPay(String type, String orderAmount, String orderCurrencyCode, String cvv, String protocolNo, String transFees, String vat,
                         String shopperResultUrl);

        void addMoneyBindCard(String custName, String cardNo, String expire, String cvv, String type, String orderAmount,
                              String orderCurrencyCode, String transFees, String vat,String shopperResultUrl,String creditType);
    }

    public interface TransferToContactView extends BaseView<TransferContract.TransferToContactPresenter> {
        void transferToContactSuccess(TransferEntity transferEntity);

        void contactInviteSuccess(ContactInviteEntity contactInviteEntity);

        void contactInviteFail(String errCode, String errMsg);

        void transferToContactFail(String errCode, String errMsg);

        void transferSurePaySuccess(TransferEntity transferEntity);

        void transferSurePayFail(String errCode, String errMsg);

        void getTransferFeeSuccess(TransFeeEntity transFeeEntity);

        void getTransferFeeFail(String errCode, String errMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void getOtpTypeSuccess(RechargeOrderInfoEntity riskControlEntity);

        void getOtpTypeFail(String errCode, String errMsg);

        void splitBillSuccess(TransferEntity transferEntity);

        void splitBillFail(String errorCode, String errorMsg);

        void sendNotifySuccess(PayBillOrderInfoEntity result);

        void sendNotifyFail(String errorCode, String errorMsg);

    }

    public interface TransferToContactPresenter extends BasePresenter<TransferContract.TransferToContactView> {

        void sendNotify(List<NotifyEntity> receivers);

        void contactInvite(List<KycContactEntity> contactInviteDtos);

        void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark,
                               String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName);

        void chatTransferToContact(RequestTransferEntity requestTransferEntity);
        void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                             String exchangeRate, String transCurrencyCode, String transFees, String vat);

        void getTransferFee(String type, String orderAmount, String orderCurrencyCode, String transTimeType);

        void getLimit(String type);

        void getOtpType(String category,String categoryId, List<String> additionalData);

        void splitBill(String oldOrderNo,
                       String receivableAmount,
                       String currencyCode,
                       String orderRemark,
                       List<SplitBillPayerEntity> receiverInfoList);
    }

    public interface TransferContactView extends BaseView<TransferContract.TransferContactPresenter> {
        void getContactListSuccess(TransferContactListEntity transferContactListEntity);

        void getContactListFail(String errCode, String errMsg);

        void checkContactSuccess(KycContactEntity kycContactEntity);

        void checkContactFail(String errCode, String errMsg);

        void contactInviteSuccess(ContactInviteEntity contactInviteEntity);

        void contactInviteFail(String errCode, String errMsg);
    }

    public interface TransferContactPresenter extends BasePresenter<TransferContract.TransferContactView> {
        void getContactList(List<RequestContactEntity> phoneList);

        void contactInvite(List<KycContactEntity> contactInviteDtos);

        void checkContact(String callingCode, String phone, String contactsName, String sceneType);
    }


    public interface TransferSurePayView extends BaseView<TransferContract.TransferSurePayPresenter> {
        void transferSurePaySuccess(TransferEntity transferEntity);

        void transferSurePayFail(String errCode, String errMsg);

        void switchTouchIDPayment(boolean isOpen);

        void checkPayPwdSuccess(TransferPwdTimeEntity result);

        void checkPayPwdFail(String errorCode, String errorMsg);

        void transferToContactSuccess(TransferEntity transferEntity);

        void transferToContactFail(String errCode, String errMsg);

        void confirmPaySuccess(TransferEntity transferEntity);

        void confirmPayFail(String errCode, String errMsg);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void checkOutFail(String errCode, String errMsg);

        void splitBillSuccess(TransferEntity transferEntity);

        void splitBillFail(String errorCode, String errorMsg);

        void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity);

        void imrAddBeneficiaryFail(String errorCode, String errorMsg);

        void imrEditBeneficiarySuccess(ResponseEntity result);

        void imrEditBeneficiaryFail(String errorCode, String errorMsg);

        void setKsaCardStatusSuccess(Void result);

        void showErrorMsg(String errorCode, String errorMsg);

        void getKsaCardPayResultSuccess(KsaPayResultEntity result);

        void activateCardSuccess(Void result);

        void setKsaCardLimitSuccess(Void result);

        void getKsaCardDetailsSuccess(CardDetailEntity result);

        void payChangePhoneFeeSuccess(ChangePhoneResultEntity result);
    }

    public interface TransferSurePayPresenter extends BasePresenter<TransferContract.TransferSurePayView> {
        void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                             String exchangeRate, String transCurrencyCode, String transFees, String vat);

        //校验一下指纹信息有没有过期
        void checkTouchIDPaymentStateFromServer(String type);

        //设置启动指纹解锁
        void setEnableTouchIDLogin(boolean isEnable);

        //设置启动指纹支付
        void setEnableTouchIDPayment(boolean isEnable);

        void checkPayPassword(String pdType, String sceneType, @Nullable String oldPassword,String orderNo);

        void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark,
                               String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName);

        void confirmPay(String orderNo, String paymentMethodNo, String orderType);

        void checkOut(String orderNo, String orderInfo);

        void splitBill(String oldOrderNo,
                       String receivableAmount,
                       String currencyCode,
                       String orderRemark,
                       List<SplitBillPayerEntity> receiverInfoList);

        void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName,
                               String nickName, String relationshipCode, String callingCode, String phone,
                               String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace,
                               String birthDate, String sex, String cityName, String districtName,
                               String poBox, String buildingNo, String street, String idNo, String idExpiry,
                               String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                               String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,String channelPayeeId,String channelCode,String branchId);

        void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo);

        void setKsaCardStatus(String status,String proxyCardNo, String reason,String reasonStatus);

        void getKsaCardPayResult(String vat, String totalAmount,
                                 OpenCardReqEntity appNiOpenCardReq);

        void activateCard(String proxyCardNo, String cardNo, String cardExpire);

        void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue,String proxyCardNo,String up);

        void getKsaCardDetails(String proxyCardNo);

        void payChangePhoneFee(String totalAmount,String vat);
    }

    public interface TransferQrCodeView extends BaseView<TransferContract.TransferQrCodePresenter> {
        void getQrCodeSuccess(QrCodeEntity qrCodeEntity);

        void getQrCodeFail(String errCode, String errMsg);

        void getCrCodeSuccess(QrCodeEntity qrCodeEntity);

        void getCrCodeFail(String errCode, String errMsg);

        void checkQrCodeFailed(String errorCode, String errorMsg);

        void checkQrCodeSuccess(UserInfoEntity response);

        void checkOrderCodeFailed(String errorCode, String errorMsg);

        void checkOrderCodeSuccess(ScanOrderEntity response);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void checkOutFail(String errCode, String errMsg);
    }

    public interface TransferQrCodePresenter extends BasePresenter<TransferContract.TransferQrCodeView> {
        void getQrCode(String currencyCode);

        void getCrCode(String paymentMethodNo);

        void checkQrCode(String qrCode);

        void getLimit(String type);

        void checkOrderCode(String qrCode);

        void checkOut(String orderNo, String orderInfo);
    }


    public interface AddBeneficiaryView extends GetOtpTypeView<AddBeneficiaryPresenter> {

        void AddBeneficiaryFailed(String errorCode, String errorMsg);

        void AddBeneficiarySuccess(BeneficiaryEntity response);

        void getTimeFailed(String errorCode, String errorMsg);

        void getTimeSuccess(TimeEntity response);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);
    }

    public interface AddBeneficiaryPresenter extends GetOtpTypePresenter<AddBeneficiaryView> {
        void addBeneficiary(String beneficiaryName, String beneficiaryIbanNo, String chooseRelationship, String saveFlag);

        void getTime(String ibanNo);

        void getLimit(String type);
    }


    public interface BeneficiaryListView extends BaseView<TransferContract.BeneficiaryListPresenter> {

        void getBeneficiaryListFailed(String errorCode, String errorMsg);

        void getBeneficiaryListSuccess(BeneficiaryListEntity response);

        void deleteBeneficiaryFailed(String errorCode, String errorMsg);

        void deleteBeneficiarySuccess(Void response);

        void getTimeFailed(String errorCode, String errorMsg);

        void getTimeSuccess(TimeEntity response);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);
    }

    public interface BeneficiaryListPresenter extends BasePresenter<TransferContract.BeneficiaryListView> {
        void getBeneficiaryList();

        void deleteBeneficiary(String id);

        void getTime(String ibanNo);

        void getLimit(String type);

    }

    public interface GetTimeView extends BaseView<TransferContract.GetTimePresenter> {

        void getTimeFailed(String errorCode, String errorMsg);

        void getTimeSuccess(TimeEntity response);


        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);
    }

    public interface GetTimePresenter extends BasePresenter<TransferContract.GetTimeView> {
        void getTime(String ibanNo);

        void getLimit(String type);
    }

    public interface RecentContactView extends BaseView<TransferContract.RecentContactPresenter> {

        void getRecentContactFailed(String errorCode, String errorMsg);

        void getRecentContactSuccess(TransferContactListEntity response);

        void getQrCodeSuccess(QrCodeEntity qrCodeEntity);

        void getQrCodeFail(String errCode, String errMsg);

        void getBeneficiaryListFailed(String errorCode, String errorMsg);

        void getBeneficiaryListSuccess(BeneficiaryListEntity response);
    }

    public interface RecentContactPresenter extends BasePresenter<TransferContract.RecentContactView> {
        void getRecentContact();

        void getBeneficiaryList();

        void getQrCode(String currencyCode);
    }

}
