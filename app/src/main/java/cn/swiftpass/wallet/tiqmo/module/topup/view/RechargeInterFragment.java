package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.adapter.RechargeInterAdapter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInterOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeContract;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeOrderListPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.RechargeFilterDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class RechargeInterFragment extends BaseFragment<RechargeContract.RechargeOrderListPresenter> implements RechargeContract.RechargeOrderListView {
    @BindView(R.id.tv_inter_title)
    TextView tvInterTitle;
    @BindView(R.id.iv_filter)
    ImageView ivFilter;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.ll_top)
    LinearLayout llTop;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;
    @BindView(R.id.ry_recharge_inter)
    RecyclerView ryRechargeInter;
    @BindView(R.id.sw_inter)
    SwipeRefreshLayout swInter;
    @BindView(R.id.view_bottom)
    View viewBottom;

    private FilterEntity filterEntity;

    private RechargeInterAdapter rechargeInterAdapter;

    private List<String> countryCodeList = new ArrayList<>();

    private List<RechargeInterOrderEntity> internationalList = new ArrayList<>();

    private int historyPosition;

    private RechargeOrderListEntity rechargeOrderListEntity;

    private StatusView historyStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;
    private String recordId;

    //是否正在筛选
    private boolean isFilter;
    private List<RechargeInterOrderEntity> countryList = new ArrayList<>();

    private RechargeInterOrderEntity mRechargeInterOrderEntity;
    private RechargeOrderDetailEntity mRechargeOrderDetailEntity;

    public static RechargeInterFragment getInstance(RechargeOrderListEntity rechargeOrderListEntity) {
        RechargeInterFragment rechargeInterFragment = new RechargeInterFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.rechargeOrderListEntity, rechargeOrderListEntity);
        rechargeInterFragment.setArguments(bundle);
        return rechargeInterFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_recharge_inter;
    }

    @Override
    protected void initView(View parentView) {
        swInter.setColorSchemeResources(R.color.color_B00931);
        filterEntity = new FilterEntity();

        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }

        swInter.setVisibility(View.VISIBLE);
        initRecyclerView();
    }

    private void initRecyclerView() {
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_31));
        historyStatusView = new StatusView.Builder(mContext, ryRechargeInter).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();

        swInter.setEnabled(false);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryRechargeInter.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryRechargeInter.setLayoutManager(manager);
        rechargeInterAdapter = new RechargeInterAdapter(internationalList);
        rechargeInterAdapter.bindToRecyclerView(ryRechargeInter);

        if (getArguments() != null) {
            rechargeOrderListEntity = (RechargeOrderListEntity) getArguments().getSerializable(Constants.rechargeOrderListEntity);
            setOrderList(rechargeOrderListEntity);
        }

        rechargeInterAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                try {
                    RechargeInterOrderEntity orderEntity = rechargeInterAdapter.getDataList().get(position);
                    historyPosition = position;
                    switch (view.getId()) {
                        case R.id.tv_pay:
                            if (orderEntity != null) {
                                mRechargeInterOrderEntity = orderEntity;
                                recordId = mRechargeInterOrderEntity.recordId;
                                mPresenter.getRechargeOrderDetail(recordId);
                            }
                            break;
                        default:
                            break;
                    }
                }catch (Throwable e){
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        });
    }

    private void setOrderList(RechargeOrderListEntity rechargeOrderListEntity) {
        internationalList.clear();
        if (rechargeOrderListEntity != null) {
//            List<RechargeInterOrderEntity> list = rechargeOrderListEntity.getTestList();
            List<RechargeInterOrderEntity> list = rechargeOrderListEntity.internationalList;
            if (!isFilter) {
                countryList.clear();
                countryList.addAll(list);
            }
            isFilter = false;
            int size = list.size();
            tvInterTitle.setText(getString(R.string.BillPay_30) + " (" + size + ")");
            if (size == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                internationalList.addAll(list);
                rechargeInterAdapter.setDataList(internationalList);
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }

    @Override
    public void getRechargeOrderListSuccess(RechargeOrderListEntity rechargeOrderListEntity) {
        if(rechargeOrderListEntity != null){
            setOrderList(rechargeOrderListEntity);
        }
    }

    @Override
    public void deleteRechargeOrderSuccess(Void result) {
        mPresenter.getRechargeOrderList();
    }

    @Override
    public void getRechargeOrderDetailSuccess(RechargeOrderDetailEntity rechargeOrderDetailEntity) {
        if (rechargeOrderDetailEntity != null) {
            this.mRechargeOrderDetailEntity = rechargeOrderDetailEntity;
            String isValidBiller = rechargeOrderDetailEntity.isValidBiller;
            if ("N".equals(isValidBiller)) {
                showCustomDialog(mContext, R.string.Topup_new_17, R.string.common_18, R.string.common_19, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPresenter.deleteRechargeOrder(recordId);
                        dialog.dismiss();
                    }
                });
            } else {
                mPresenter.getRechargeProductList(rechargeOrderDetailEntity.operatorId);
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getRechargeOrderListFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getRechargeProductListSuccess(RechargeProductListEntity rechargeProductListEntity) {
        if(rechargeProductListEntity != null){
            HashMap<String,Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.rechargeProductListEntity,rechargeProductListEntity);
            mHashMap.put(Constants.rechargeInterOrderEntity,mRechargeInterOrderEntity);
            mRechargeOrderDetailEntity.countryName = mRechargeInterOrderEntity.countryName;
            mHashMap.put(Constants.rechargeOrderDetailEntity,mRechargeOrderDetailEntity);
//            mRechargeOperatorEntity.mobileNumber = phoneNumber;
//            mRechargeOperatorEntity.shortName = shortName;
            ActivitySkipUtil.startAnotherActivity(mActivity, RechargeProductListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public RechargeContract.RechargeOrderListPresenter getPresenter() {
        return new RechargeOrderListPresenter();
    }

    @OnClick({R.id.tv_filter, R.id.iv_filter})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_filter:
            case R.id.iv_filter:
                if (countryList.size() > 0) {
                    showFilterDialog(countryList);
                } else {
                    mPresenter.getRechargeOrderList();
                }
                break;
            default:
                break;
        }
    }

    private void showFilterDialog(List<RechargeInterOrderEntity> countryList) {
        //这里国家用列表里面存在的国家
        RechargeFilterDialog paymentFilterDialog = new RechargeFilterDialog(mContext, countryList);
        paymentFilterDialog.setApplyFilterListener(new RechargeFilterDialog.ApplyFilterListener() {
            @Override
            public void changeFilter(List<String> selectCountryCodeList, List<RechargeInterOrderEntity> selectCountryList) {
                countryCodeList.clear();
                countryCodeList.addAll(selectCountryCodeList);
                rechargeInterAdapter.setDataList(selectCountryList);
                tvInterTitle.setText(getString(R.string.BillPay_30) + " (" + selectCountryList.size() + ")");
                isFilter = true;
            }
        });
        paymentFilterDialog.showWithBottomAnim();
    }
}
