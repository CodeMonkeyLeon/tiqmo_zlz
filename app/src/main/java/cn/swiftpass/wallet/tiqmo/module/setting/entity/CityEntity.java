package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CityEntity extends BaseEntity {

    public ArrayList<City> citiesInfoList;

    public class City extends BaseEntity{
        public String citiesName;
        public String citiesId;
    }
}
