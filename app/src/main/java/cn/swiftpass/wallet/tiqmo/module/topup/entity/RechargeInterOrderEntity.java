package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeInterOrderEntity extends BaseEntity {

    //记录id
    public String recordId;
    //国家图片
    public String countryImgUrl;
    //国家名称
    public String countryName;
    //国家代码
    public String countryCode;
    //用户简称
    public String userShortName;
    //运营商名称
    public String operatorName;
    //运营商图片 url
    public String operatorImgUrl;

    public String selected = "0";
}
