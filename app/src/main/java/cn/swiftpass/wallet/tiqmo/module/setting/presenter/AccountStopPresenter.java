package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.PersonInfoContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AccountStopPresenter implements PersonInfoContract.StopPresenter {

    private PersonInfoContract.StopView baseView;

    @Override
    public void stopAccount() {
        AppClient.getInstance().getUserManager().stopAccount(new LifecycleMVPResultCallback<Void>(baseView,true) {
            @Override
            protected void onSuccess(Void result) {
                baseView.stopAccountSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.stopAccountFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(PersonInfoContract.StopView view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
