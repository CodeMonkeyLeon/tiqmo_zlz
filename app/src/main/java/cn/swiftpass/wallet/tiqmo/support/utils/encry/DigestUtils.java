package cn.swiftpass.wallet.tiqmo.support.utils.encry;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Author: jamy
 * @Description:
 * @Date: Create in 17:04 2018/7/15
 * @Modified By：
 */
public class DigestUtils {
    public static MessageDigest getDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static MessageDigest getMd5Digest() {
        return getDigest("MD5");
    }

    public static MessageDigest getSha256Digest() {
        return getDigest("SHA-256");
    }

}
