package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import com.gc.gcchat.GcChatMessage;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatUser;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChatHistoryItemEntity extends BaseEntity {

    private String sessionId;
    private String title;
    private boolean groupChat;
    private String fileId;
    private ArrayList<GcChatParticipant> participantUsers;
    private GcChatMessage lastMessage;
    private String createdAt;
    private GcChatUser createdByUser;
    private boolean blocked;
    private String blockedAt;


    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public boolean isGroupChat() {
        return this.groupChat;
    }

    public void setGroupChat(final boolean groupChat) {
        this.groupChat = groupChat;
    }

    public String getFileId() {
        return this.fileId;
    }

    public void setFileId(final String fileId) {
        this.fileId = fileId;
    }

    public ArrayList<GcChatParticipant> getParticipantUsers() {
        return this.participantUsers;
    }

    public void setParticipantUsers(final ArrayList<GcChatParticipant> participantUsers) {
        this.participantUsers = participantUsers;
    }

    public GcChatMessage getLastMessage() {
        return this.lastMessage;
    }

    public void setLastMessage(final GcChatMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(final String createdAt) {
        this.createdAt = createdAt;
    }

    public GcChatUser getCreatedByUser() {
        return this.createdByUser;
    }

    public void setCreatedByUser(final GcChatUser createdByUser) {
        this.createdByUser = createdByUser;
    }

    public boolean isBlocked() {
        return this.blocked;
    }

    public void setBlocked(final boolean blocked) {
        this.blocked = blocked;
    }

    public String getBlockedAt() {
        return this.blockedAt;
    }

    public void setBlockedAt(final String blockedAt) {
        this.blockedAt = blockedAt;
    }
}
