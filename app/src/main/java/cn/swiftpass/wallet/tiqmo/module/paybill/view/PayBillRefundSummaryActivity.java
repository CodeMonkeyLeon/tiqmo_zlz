package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillRefundOrderPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class PayBillRefundSummaryActivity extends BaseCompatActivity<PayBillContract.BillRefundOrderPresenter> implements PayBillContract.BillRefundOrderView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_store_logo)
    ImageView ivStoreLogo;
    @BindView(R.id.tv_store_name)
    TextView tvStoreName;
    @BindView(R.id.iv_split_line)
    ImageView ivSplitLine;
    @BindView(R.id.tv_key)
    TextView tvKey;
    @BindView(R.id.tv_value)
    TextView tvValue;
    @BindView(R.id.tv_pay_confirm)
    TextView tvPayConfirm;

    private PayBillDetailEntity payBillDetailEntity;
    private String mChannelCode;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_refund_summary;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        tvTitle.setText(R.string.BillPay_38);
        LocaleUtils.viewRotationY(this, ivHeadCircle, ivBack);

        if (getIntent() != null && getIntent().getExtras() != null) {
            payBillDetailEntity = (PayBillDetailEntity) getIntent().getExtras().getSerializable(Constants.paybill_billerDetail);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);
            if(payBillDetailEntity != null){
                tvKey.setText(payBillDetailEntity.billReferenceName);
                tvValue.setText(payBillDetailEntity.billReferenceValue);
                tvStoreName.setText(payBillDetailEntity.billerName);
                Glide.with(mContext)
                        .load(payBillDetailEntity.billerImgUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.utilities))
                        .into(ivStoreLogo);
            }
        }
    }

    @Override
    public void payBillRefundOrderSuccess(PayBillRefundEntity payBillRefundEntity) {
        if(payBillRefundEntity != null){
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
            cardNotifyEntity.paymentMethodDesc = payBillRefundEntity.paymentMethod;
            cardNotifyEntity.orderAmount = payBillRefundEntity.tradeAmount;
            cardNotifyEntity.orderNo = payBillRefundEntity.orderNo;
            cardNotifyEntity.orderDesc = payBillRefundEntity.orderDesc;
            cardNotifyEntity.logo = payBillRefundEntity.logo;
            cardNotifyEntity.startTime = payBillRefundEntity.createTime;
            cardNotifyEntity.basicExtendProperties = payBillRefundEntity.basicExtendProperties;
            cardNotifyEntity.orderStatus = payBillRefundEntity.orderStatus;
            cardNotifyEntity.orderDirection = payBillRefundEntity.orderDirection;
            cardNotifyEntity.sceneType = payBillRefundEntity.sceneType;
            cardNotifyEntity.remark = payBillRefundEntity.remark;
            mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            ProjectApp.removeAllTaskExcludeMainStack();
            ActivitySkipUtil.startAnotherActivity(this, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);

        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillRefundOrderPresenter getPresenter() {
        return new PayBillRefundOrderPresenter();
    }

    @OnClick({R.id.iv_back, R.id.tv_pay_confirm})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_pay_confirm:
                mPresenter.payBillRefundOrder(mChannelCode);
                break;
            default:break;
        }
    }
}
