package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class BudgetCategoryEntity extends BaseEntity {
    /**
     * //\"spendingAnalysisFilterConditions\":[{\"title\":\"General\",\"value\":\"1\"},{\"title\":\"Bills\",\"value\":\"2\"},{\"title\":\"Entertainment\",\"value\":\"3\"}]
     */
    public String title;
    public String value;

    public BudgetCategoryEntity() {
    }

    public BudgetCategoryEntity(final String title, final String value) {
        this.title = title;
        this.value = value;
    }
}
