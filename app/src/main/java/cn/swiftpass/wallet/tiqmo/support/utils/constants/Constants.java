package cn.swiftpass.wallet.tiqmo.support.utils.constants;

import cn.swiftpass.wallet.tiqmo.BuildConfig;

public class Constants {
    //沙特
    public static String CALLINGCODE;
    public static final String CALLINGCODE_KSA = BuildConfig.DefaultInternationalAreaCode;
    public static final String KsaPartnerNo = BuildConfig.partnerNo;
    public static final String KsaServerPublicKey = BuildConfig.serverPublicKey;
    public static final String call_phone = "8001110800";
    public static final String call_email = "CustomerCare.KSA@Tiqmo.com";
    //阿联酋
    public static final String ACCOUNT_TYPE_BASE = "100";
    public static final String ID_CARD_INFO_FRONT = "cardInfoFront";
    public static final String ID_CARD_INFO = "cardInfo";
    public static final String OCR_RESULT = "ocr_result";
    public static final String OCR_RESULT_MESSAGE = "ocr_result_message";
    public static final String changePhoneResult = "changePhoneResult";

    //检查更新
    public static final String appManageEntity = "appManageEntity";

    public static final String AMOUNT_100 = "100";
    public static final String AMOUNT_200 = "200";
    public static final String AMOUNT_500 = "500";
    public static final String AMOUNT_1000 = "1000";
    public static final String chatTransferEntity = "chatTransferEntity";

    //预校验统一使用S
    public static final String FIO_TYPE_LOGIN = "S";
    public static final String FIO_TYPE_PAYMENT = "P";
    //手机号注册
    public static final String REG_PHONE = "TYPE_1";
    public static final String SCENE_TYPE_REG_PHONE = "R";
    public static final String SCENE_TYPE_FORGET_PD = "FP";
    public static final String REG_OTP_TYPE = "M";
    public static final String FORGET_OTP_TYPE = "F";
    public static final String OTP_KYC_REG = "AT";
    public static final String OTP_KYC_REG_KSA = "SP";
    public static final String OTP_KYC_PAY_PD = "FP";
    public static final String OTP_IMRP = "IMRP";
    public static final String OTP_TYPE_CHANGE_PHONE = "A";
    public static final String OTP_TYPE_FIRST_LOGIN = "02";
    public static final String DEACTIVATION_OTP_TYPE = "T";
    public static final String LOGIN_PD = "L";
    public static final String LAST_USER = "LAST_USER";
    public static final String NO_USE_FINGER_LOGIN = "active";
    public static final String NO_USE_FINGER_PAY = "pay";

    public static final int KYC_TYPE_NULL = -1;
    public static final int KYC_TYPE_ACCOUNT = 1001;
    public static final int KYC_TYPE_ADD_CARD = 1002;
    public static final int KYC_TYPE_TRANSFER = 1003;
    public static final int KYC_TYPE_HOME_SCAN = 1004;
    public static final int KYC_TYPE_ADD_MONEY_SCAN = 1005;
    public static final int KYC_TYPE_SPLIT_BILL = 1006;
    public static final int KYC_TYPE_E_VOUCHER = 1007;
    public static final int KYC_TYPE_TOP_UP = 1008;
    public static final int KYC_TYPE_REFERRAL_CODE = 1009;
    public static final int KYC_TYPE_CUSTOMER_INFO = 1010;
    public static final int KYC_TYPE_ACCOUNT_DETAIL = 1011;
    public static final int KYC_TYPE_IMR = 1012;
    public static final int KYC_GO_TO_CARD_MANAGE = 1013;
    public static final int KYC_TYPE_PAY_BILL = 1014;
    public static final int KYC_TYPE_HOME_ADD_MONEY = 1015;
    public static final int KYC_TYPE_HOME_SEND_MONEY = 1016;
    public static final int KYC_TYPE_KSA_ADD_CARD = 1017;
    public static final int KYC_TYPE_SEND_RECEIVE = 1018;
    public static final int KYC_CHANGE_PHONE_NUMBER = 1019;

    public static final String sceneType_EVoucher = "15";
    public static final String sceneType_TopUp = "5";
    public static final String CASH_BACK = "CASH_BACK";
    public static final String CASH_REDUCE = "CASH_REDUCE";

    public static final String from_evoucher = "from_evoucher";
    public static final String from_notification_center = "from_notification_center";

    public static final String NOTIFY_SUBJECT_CQR_PAYMENT = "CQR_PAYMENT";
    public static final String NOTIFY_SUBJECT_MQR_PAYMENT = "MQR_PAYMENT";
    public static final String NOTIFY_SUBJECT_H5_PAYMENT = "H5_PAYMENT";
    public static final String NOTIFY_SUBJECT_APP_PAYMENT = "APP_PAYMENT";
    public static final String NOTIFY_SUBJECT_PAYMENT_REFUND = "PAYMENT_REFUND";
    public static final String NOTIFY_SUBJECT_UPDATE_USER_ID = "UPDATE_USER_ID";
    //    public static final String NOTIFY_SUBJECT_BILL_PAYMENT_REFUND = "PAYMENT_REFUND1";
    public static final String NOTIFY_SUBJECT_BILL_PAYMENT = "BILL_PAYMENT";
    public static final String NOTIFY_SUBJECT_TRANSFER = "TRANSFER";
    public static final String NOTIFY_SUBJECT_TRANSFER_BANK = "TRANSFER_BANK";
    public static final String NOTIFY_SUBJECT_RECIEVE_REQUEST_TRANSFER = "RECIEVE_REQUEST_TRANSFER";
    public static final String NOTIFY_SUBJECT_REQUEST_TRANSFER_APPROVED = "REQUEST_TRANSFER_APPROVED";
    public static final String NOTIFY_SUBJECT_REQUEST_TRANSFER_REJECTED = "REQUEST_TRANSFER_REJECTED";
    public static final String NOTIFY_SUBJECT_LOGIN_PD_CHANGE = "LOGIN_PWD_CHANGE";
    public static final String NOTIFY_SUBJECT_LOGIN_PD_RESET = "LOGIN_PWD_RESET";
    public static final String NOTIFY_SUBJECT_PAYMENT_PD_CHANGE = "PAYMENT_PWD_CHANGE";
    public static final String NOTIFY_SUBJECT_PAYMENT_PD_RESET = "PAYMENT_PWD_RESET";
    public static final String NOTIFY_SUBJECT_REGISTERATION = "REGISTERATION";
    public static final String NOTIFY_SUBJECT_CHANGE_LOGIN_DEVICE = "CHANGE_LOGIN_DEVICE";
    public static final String NOTIFY_SUBJECT_KYC = "KYC";
    public static final String NOTIFY_SUBJECT_ADD_MONEY = "ADD_MONEY";
    public static final String NOTIFY_SUBJECT_BindCard = "BindCard";
    public static final String NOTIFY_SUBJECT_SPLIT_BILL_REJECT = "SPLIT_BILL_REJECT";
    public static final String NOTIFY_SUBJECT_SPLIT_BILL_CONFIRM = "SPLIT_BILL_CONFIRM";
    public static final String NOTIFY_SUBJECT_MOBILE_TOP_UP = "MOBILE_TOP_UP";
    public static final String NOTIFY_SUBJECT_PURCHASE_E_VOUCHER = "PURCHASE_E_VOUCHER";
    public static final String NOTIFY_SUBJECT_MARKETING_ACTIVITY = "MARKETING_ACTIVITY";
    public static final String NOTIFY_SUBJECT_PORTAL_PUSH = "PORTAL_PUSH";
    public static final String NOTIFY_SUBJECT_CASHBACK_MARKET_ACTIVITY = "CASHBACK_MARKET_ACTIVITY";
    public static final String NOTIFY_SUBJECT_REFERRALCODE_CASHBACK = "REFERRALCODE_CASHBACK";
    public static final String NOTIFY_SUBJECT_I18N_REMTTIANCE_ACCEPT = "I18N_REMTTIANCE_ACCEPT";
    public static final String NOTIFY_SUBJECT_NI_CARD_EXPIRED = "NI_CARD_EXPIRED";
    public static final String NOTIFY_SUBJECT_USER_KYC_STATUS_UNPD = "USER_KYC_STATUS_UNPWD";
    public static final String SEND_RECEIVE_SPLIT_DETAIL = "SEND_RECEIVE_SPLIT_DETAIL";

    //查询限额
    public static final String LIMIT_ADD_MONEY = "I";
    public static final String LIMIT_TRANSFER = "O";
    //查询手续费
    public static final String TYPE_ADDMONEY = "R";
    public static final String TYPE_TRANSFER = "T";
    //转账 RT：发起请求转账 ST：扫码转账 WW：钱包内部转账 BT：银行账户转账 RC:处理请求转账
    public static final String TYPE_TRANSFER_RT = "RT";
    public static final String TYPE_TRANSFER_RC = "RC";
    public static final String TYPE_TRANSFER_ST = "ST";
    public static final String TYPE_TRANSFER_WW = "WW";
    public static final String TYPE_TRANSFER_BT = "BT";
    public static final String TYPE_TRANSFER_QR = "QR";
    //提现
    public static final String TYPE_TRANSFER_WT = "WT";
    //UL解冻卡 L冻结卡 BL注销卡
    public static final String TYPE_CARD_FREEZE = "L";
    public static final String TYPE_CARD_UNFREEZE = "UL";
    public static final String TYPE_CARD_BLOCK = "BL";
    public static final String TYPE_TXN_INSTORE = "instore";
    public static final String TYPE_TXN_ECOM = "ecom";
    public static final String TYPE_TXN_ATM = "atm";
    public static final String TYPE_TXN_INTER = "nationtms";
    public static final String history_detail = "history_detail";
    public static final String TYPE_SCAN_MERCHANT = "TYPE_SCAN_MERCHANT";
    public static final String TYPE_IMR_PAY = "TYPE_IMR_Pay";
    public static final String TYPE_PAY_BILL = "TYPE_PAY_BILL";
    public static final String TYPE_INTER_TOP_UP = "TYPE_INTER_TOP_UP";
    public static final String TYPE_CHARITY = "TYPE_CHARITY";
    public static final String TYPE_PAY_CARD = "TYPE_PAY_CARD";
    public static final String TYPE_PAY_CARD_NO_FEE = "TYPE_PAY_CARD_NO_FEE";
    public static final String TYPE_KSA_CARD_SET_CHANNEL = "TYPE_KSA_CARD_SET_CHANNEL";
    public static final String TYPE_KSA_CARD_ACTIVATE = "TYPE_PAY_CARD_ACTIVATE";
    public static final String TYPE_SEND_BENEFICIARY = "TYPE_SEND_BENEFICIARY";
    public static final String TYPE_RECEIVE_SPLIT_BILL = "TYPE_RECEIVE_SPLIT_BILL";
    public static final String TRANSFER_ENTITY = "TRANSFER_ENTITY";
    public static final String TYPE_SPLIT = "TYPE_SPLIT";
    public static final String TYPE_KSA_CARD_FREEZE = "TYPE_KSA_CARD_FREEZE";
    public static final String TYPE_KSA_CARD_DETAIL = "TYPE_KSA_CARD_DETAIL";
    public static final String TYPE_KSA_CARD_UNFREEZE = "TYPE_KSA_CARD_UNFREEZE";
    public static final String TYPE_KSA_CARD_BLOCK = "TYPE_KSA_CARD_BLOCK";
    public static final String TYPE_KSA_CARD_SET_PIN = "TYPE_KSA_CARD_SET_PIN";
    public static final String TYPE_KSA_CARD_SET_LIMIT = "TYPE_KSA_CARD_SET_LIMIT";
    public static final String TYPE_IMR_ADD_BENEFICIARY = "TYPE_IMR_ADD_BENEFICIARY";
    public static final String TYPE_WITHDRAW_ADD_BENEFICIARY = "TYPE_WITHDRAW_ADD_BENEFICIARY";
    public static final String IS_FROM_SDK = "isFromSDK";
    public static final String IS_FROM_EDIT_BENEFICIARY = "isEditBeneficiary";
    public static final String showSaveList = "showSaveList";
    public static final String sceneType = "sceneType";
    public static final String chatId = "chatId";
    public static final String TRADE_ORDER = "TRADE_ORDER";
    public static final String ACTIVITY_REFERRAL_CODE = "REFERRAL_CODE";
    public static final String checkPdType = "checkPwdType";
    public static final String isOpen = "isOpen";
    public static final String checkPdType_face_id = "checkPwdType_face_id";
    public static final String checkPdType_reason = "checkPwdType_reason";
    public static final String fromHistory = "fromHistory";
    public static final String orderNo = "orderNo";
    public static final String splitMoney = "splitMoney";
    public static final String pd_sceneType = "pwd_sceneType";
    public static final String TransferLimitEntity = "transferLimitEntity";
    public static final String cardLimitChannelEntity = "cardLimitChannelEntity";
    public static final String transferDestinationCountryCode = "transferDestinationCountryCode";
    public static final String imrExchangeRateEntity = "imrExchangeRateEntity";
    public static final String TimeEntity = "TimeEntity";
    public static final String BeneficiaryEntity = "beneficiaryEntity";
    public static final String EVoucherDetailEntity = "EVoucherDetailEntity";
    public static final String cardSettingEntity = "cardSettingEntity";
    public static final String seeCardType = "seeCardType";
    public static final String openAnalyticsPage = "openAnalyticsPage";
    public static final String TYPE_CHANGE_LOGIN_PD = "TYPE_CHANGE_LOGIN_PD";
    public static final String TYPE_HyperPay_ADD_MONEY = "TYPE_HyperPay_ADD_MONEY";
    public static final String TYPE_ADD_SEND_BENE = "TYPE_ADD_SEND_BENE";
    public static final String TYPE_CHANGE_PHONE_FEE = "TYPE_CHANGE_PHONE_FEE";

    public static final String mChangePhoneFeeEntity = "mChangePhoneFeeEntity";
    public static final String mChangePhoneOrderNo = "mChangePhoneOrderNo";
    public static final String qrCodeUrl = "qrCodeUrl";
    public static final String paymentMethod = "paymentMethod";
    public static final String isFrom = "isFrom";
    public static final String HISTORY_TYPE = "history_type";
    public static final String isChangeLoginPd = "isChangeLoginPd";

    //TransferDetail 交易详情
    public static final int sceneType_Mobile_Topup = 5;
    public static final int sceneType_Bills_Payment = 6;
    public static final int sceneType_Wallet_Transfer = 7;
    public static final int sceneType_Request_Transfer = 8;
    public static final int sceneType_Bank_Transfer = 9;
    public static final int sceneType_Add_Money = 10;
    public static final int sceneType_Int_Remittance = 11;
    public static final int sceneType_QR_Purchase = 12;
    public static final int sceneType_Add_Money_Refund = 13;
    public static final int sceneType_QR_Transfer = 14;
    public static final int sceneType_Evouchers_Purchase = 15;
    public static final int sceneType_Split_Bill = 18;
    public static final int sceneType_Cash_Back = 19;
    public static final int sceneType_Instant_Discount = 20;
    public static final int sceneType_Referral_Code = 21;
    public static final int sceneType_Refund_QR_Purchase = 22;
    public static final int sceneType_Int_Mobile_Topup = 23;

    //BillPayment
    public static final String paybill_countryCode = "paybill_countryCode";
    public static final String paybill_countryLogo = "paybill_countryLogo";
    public static final String paybill_countryName = "paybill_countryName";
    public static final String payBillCountryEntity = "payBillCountryEntity";
    public static final String payBillTypeEntity = "payBillTypeEntity";
    public static final String paybill_billerType = "paybill_billerType";
    public static final String paybill_billerDescription = "paybill_billerDescription";
    public static final String paybill_billerId = "paybill_billerId";
    public static final String paybill_billerSku = "paybill_billerSku";
    public static final String paybill_billerSkuListEntity = "paybill_billerSkuListEntity";
    public static final String paybill_billerOrderListEntity = "paybill_billerOrderListEntity";
    public static final String paybill_billerIOListEntity = "paybill_billerIOListEntity";
    public static final String paybill_billerOrderEntity = "paybill_billerOrderEntity";
    public static final String paybill_billerDetail = "paybill_billerDetail";
    public static final String paybill_billerLogo = "paybill_billerLogo";
    public static final String paybill_billerName = "paybill_billerName";
    public static final String paybill_supportRefundFlag = "paybill_supportRefundFlag";
    public static final String sent = "sent";
    public static final String receive = "receive";
    public static final String paybill_inter = "Paykii";
    public static final String paybill_local = "Sadad";
    //Recharge
    public static final String recharge_countryCode = "recharge_countryCode";
    public static final String recharge_countryLogo = "recharge_countryLogo";
    public static final String recharge_countryName = "recharge_countryName";
    public static final String rechargeOrderListEntity = "rechargeOrderListEntity";
    public static final String rechargeCountryEntity = "rechargeCountryEntity";
    public static final String rechargeContactEntity = "rechargeContactEntity";
    public static final String rechargeProductListEntity = "rechargeProductListEntity";
    public static final String rechargeOperatorEntity = "rechargeOperatorEntity";
    public static final String rechargeOperatorListEntity = "rechargeOperatorListEntity";
    public static final String rechargeOrderInfoEntity = "rechargeOrderInfoEntity";
    public static final String operator_name = "operator_name";
    public static final String productName = "productName";
    public static final String rechargeInputProductEntity = "rechargeInputProductEntity";
    public static final String rechargeChooseProductEntity = "rechargeChooseProductEntity";
    public static final String rechargeInterOrderEntity = "rechargeInterOrderEntity";
    public static final String rechargeOrderDetailEntity = "rechargeOrderDetailEntity";
    //KSA 卡管理
    public static final String card_type_Standard = "Standard";
    public static final String card_type_Platinum = "Platinum";
    public static final String ksa_cardFaceId = "ksa_cardFaceId";
    public static final String ksa_cardType = "ksa_cardType";
    public static final String ksa_deliveryTime = "ksa_deliveryTime";
    public static final String ksa_card_VIRTUAL = "VIRTUAL";
    public static final String ksa_card_PHYSICAL = "PHYSICAL";
    public static final String ksaCardSummaryEntity = "ksaCardSummaryEntity";
    public static final String ksaPayResultEntity = "ksaPayResultEntity";
    public static final String openCardReqEntity = "openCardReqEntity";
    public static final String ksaCardEntity = "KsaCardEntity";
    public static final String ksaLimitChannelEntity = "ksaLimitChannelEntity";
    public static final String ksaCardLimitConfigEntity = "ksaCardLimitConfigEntity";
    public static final String blockCardReason = "blockCardReason";
    public static final String blockCardReasonStatus = "blockCardReasonStatus";
    public static final String proxyCardNo = "proxyCardNo";
    public static final String cardResultType = "cardResultType";
    public static final String cardResultErrorMsg = "cardResultErrorMsg";
    public static final String transferPdfEntity = "transferPdfEntity";
    public static final String cardSuccessType = "cardSuccessType";
    public static final String isFromHome = "isFromHome";
    //Hyperpay充值
    public static final String allCardEntity = "allCardEntity";
    public static final String transferCardType = "transferCardType";
    public static final String transFeeEntity = "transFeeEntity";
    public static final String transferPayEntity = "transferPayEntity";
    public static final String cardEntity = "cardEntity";
    public static final String INVITE_ENTITY = "inviteEntity";
    public static final String badges = "badges";
    public static final String shopperResultURL = "tiqmo://tiqmoappURL.com";
    public static final String Hyperpay_orderNo = "Hyperpay_orderNo";
    public static final String balanceEntity = "balanceEntity";
    //IVR
    //扩展字段功能类型
    //复制
    public static final String extend_copy = "C";
    //返现订单
    public static final String extend_cash_back_order = "TO_CASH_BACK_ORDER";
    public static final String extend_order_percent_icon = "ORDER_PERCENT_ICON";

    //绑新卡充值
    public static final String ADD_MONEY_BIND_NEW_CARD = "ADD_MONEY_BIND_NEW_CARD";

    //IMR相关  	汇款地区：DESTINATION，国籍：NATIONALITY
    public static final String imr_destination = "DESTINATION";
    public static final String imr_nationality = "NATIONALITY";
    //IMR相关 WWCE / TransFast，未指定则取机构默认
    public static final String imr_WWCE = "WWCE";
    public static final String imr_TransFast = "TransFast";
    public static final String imr_Thunes = "Thunes";
    //银行/代理类型:AT表示代理 BT表示银行
    public static final String imr_AT = "AT";
    public static final String imr_BT = "BT";
    public static final String imr_CPA = "CPA";

    public static final String ImrBeneficiaryBean = "ImrBeneficiaryBean";
    public static final String ImrBeneficiaryEntity = "ImrBeneficiaryEntity";
    public static final String ImrOrderInfoEntity = "imrOrderInfoEntity";

    //通知消息类型
    public static final String MSGTYPE = "msgType";
    public static final String MSGTYPE_TRANSFER_WW = "MSGTYPE_TRANSFER_WW";
    public static final String SUBJECT_ADD_MONEY = "ADD_MONEY";
    public static final String SUBJECT_NOTIFY = "NOTIFY";
    public static final String SUBJECT_SPLIT_BILL = "SPLIT_BILL";
    public static final String SUBJECT_CQR_PAYMENT = "CQR_PAYMENT";
    public static final String SUBJECT_MQR_PAYMENT = "MQR_PAYMENT";
    public static final String SUBJECT_H5_PAYMENT = "H5_PAYMENT";
    public static final String SUBJECT_APP_PAYMENT = "APP_PAYMENT";
    public static final String SUBJECT_PURCHASE_E_VOUCHER = "PURCHASE_E_VOUCHER";
    public static final String SUBJECT_MOBILE_TOP_UP = "MOBILE_TOP_UP";
    public static final String SUBJECT_PAYMENT_REFUND = "PAYMENT_REFUND";
    public static final String SUBJECT_CHANGE_LOGIN_DEVICE = "CHANGE_LOGIN_DEVICE";
    public static final String SUBJECT_REQUEST_TRANSFER_REJECTED = "REQUEST_TRANSFER_REJECTED";
    public static final String SUBJECT_TRANSFER_WW = "TRANSFER_WW";
    public static final String SUBJECT_BIND_CARD = "BindCard";
    public static final String ADD_CARD_ENTITY = "ADD_CARD_ENTITY";
    public static final String NOTIFY_ENTITY = "NOTIFY_ENTITY";
    public static final String ADD_MONEY_ENTITY = "ADD_MONEY_ENTITY";
    public static final String REQUEST_TRANSFER_ENTITY = "REQUEST_TRANSFER_ENTITY";
    public static final String MSG_ENTITY = "MSG_ENTITY";
    public static final String CONTACT_ENTITY = "CONTACT_ENTITY";
    public static final String CHECK_OUT_ENTITY = "CHECK_OUT_ENTITY";
    public static final String mImrOrderInfoEntity = "mImrOrderInfoEntity";
    public static final String mEVoucherOrderInfoEntity = "mEVoucherOrderInfoEntity";
    public static final String mPayBillOrderInfoEntity = "mPayBillOrderInfoEntity";
    public static final String SEND_RECEIVE_ENTITY = "SEND_RECEIVE_ENTITY";
    public static final String ORDER_ENTITY = "ORDER_ENTITY";
    public static final String ADD_MONEY = "ADD_MONEY";
    public static final String CARD_ENTITY = "CARD_ENTITY";
    public static final String CARD_FROM = "CARD_FROM";
    public static final String CARD_FROM_ADD_MONEY = "CARD_FROM_ADD_MONEY";
    public static final String CARD_FROM_MANAGE = "CARD_FROM_MANAGE";
    public static final String CARD_COLOR = "CARD_COLOR";

    public static final String KYC_FORGET_PAY_PD = "KYC_FORGET_PAY_PD";
    public static final String KYC_REG = "KYC_REG";
    public static final String KYC_TYPE = "KYC_TYPE";

    //卡支付
    public static final String PAY_TYPE_CARD = "CP";
    public static final String PAY_TYPE_GooglePay = "GooglePay";

    public static final String APP_PRIVATEKEY = "privateKey";
    public static final String APP_PUBLICKEY = "publicKey";

    public static final String DATA_STR_UUID_NEW = "DATA_STR_UUID_NEW";

    public static final String CACHE_IS_LOGIN = "CACHE_IS_LOGIN";
    public static final String LOGIN_FROM = "login_from";
    public static final String CACHE_LAST_LOGIN_TIME = "CACHE_LAST_LOGIN_TIME";
    public static final String CACHE_FINGER_PRINT = "CACHE_FINGER_PRINT";


    public static final String LANG_CODE_ZH_CN_NEW = "zh_CN";

    public static final String LANG_CODE_ZH_MO_NEW_NORMAL = "zh-MO";

    public static final String LANG_CODE_ZH_HK_NEW = "zh_HK";


    public static final String LANG_CODE_ZH_HK_NORMAL = "zh-HK";
    /**
     * 英语
     */
    public static final String LANG_CODE_EN_US_NORMAL = "en-US";
    /**
     * 简体中文
     */
    public static final String LANG_CODE_ZH_CN_NORMAL = "zh-CN";

    public static final String LANG_CODE_ZH_TW_NORMAL = "zh-TW";

    /**
     * 繁体中文
     */
    public static final String LANG_CODE_ZH_TW = "zh_TW";

    public static final String LANG_CODE_ZH_MO = "zh_MO";

    public static final String LANG_CODE_ZH_HK = "zh_HK";
    /**
     * 英语
     */
    public static final String LANG_CODE_EN_US = "en_US";
    /**
     * 简体中文
     */
    public static final String LANG_CODE_ZH_CN = "zh_CN";


    public static final String CACHE_CARDLIST_FILENAME = "cardlist.txt";


    public static final String DATA_COUNTRY_NAME = "DATA_COUNTRY_NAME";
    public static final String DYNAMICQRCODEENTITY = "DynamicQrCodeEntity";
    public static final String IS_DEEP_LINK = "IS_DEEP_LINK";
    public static final String IS_SWITCH_ACCOUNT = "IS_SWITCH_ACCOUNT";
    public static final String IS_FROM_PROFILE = "IS_FROM_PROFILE";

    public static final String DATA_PHONE_NUMBER = "DATA_PHONE_NUMBER";
    public static final String DATA_PHONE_PD = "DATA_PHONE_PASSWORD";
    public static final String DATA_AREA_ENTITY = "DATA_AREA_ENTITY";
    public static final String DATA_OTP_TYPE = "DATA_OTP_TYPE";
    public static final String LOGIN_TYPE = "LOGIN_TYPE";
    public static final String DATA_FIRST_TITLE = "DATA_FIRST_TITLE";
    public static final String DATA_OTP_LENGTH = "DATA_OTP_LENGTH";
    public static final String DATA_OTP_TIME = "DATA_OTP_TIME";
    public static final String OTP_REG = "OTP_REG";
    public static final String OTP_CHANGE_PHONE = "OTP_CHANGE_PHONE";
    public static final String OTP_LOGIN_NEW_DEVICE = "OTP_LOGIN_NEW_DEVICE";
    public static final String OTP_LOGIN_OLD_DEVICE = "OTP_LOGIN_OLD_DEVICE";
    public static final String OTP_riskControlEntity = "riskControlEntity";
    public static final String OTP_FORGET_LOGIN_PD = "OTP_FORGET_LOGIN_PWD";
    public static final String OTP_FORGET_PAY_PD = "OTP_FORGET_PAY_PWD";
    public static final String OTP_KYC = "OTP_KYC";
    //激活用户
    public static final String OTP_ACTIVATE_USER = "OTP_ACTIVATE_USER";
    public static final String OTP_FIRST_LOGIN = "OTP_FIRST_LOGIN";
    public static final String OTP_FIRST_FAST_LOGIN = "OTP_FIRST_FAST_LOGIN";
    public static final String OTP_FIRST_TRANSFER_CONTACT = "OTP_FIRST_TRANSFER_CONTACT";
    public static final String OTP_FIRST_TRANSFER_IBANK = "OTP_FIRST_TRANSFER_IBANK";
    public static final String OTP_FIRST_TRANSFER_REQUEST = "OTP_FIRST_TRANSFER_REQUEST";
    public static final String OTP_FIRST_TOP_UP = "OTP_FIRST_TOP_UP";
    public static final String OTP_FIRST_PAY_BILL = "OTP_FIRST_PAY_BILL";
    public static final String OTP_FIRST_IMR = "OTP_FIRST_IMR";
    public static final String OTP_FIRST_ADD_IMR = "OTP_FIRST_ADD_IMR";
    public static final String OTP_FIRST_WITHDRAW_ADD_IMR = "OTP_FIRST_WITHDRAW_ADD_IMR";
    public static final String OTP_FIRST_REQUEST_TRANSFER = "OTP_FIRST_REQUEST_TRANSFER";
    public static final String OTP_FIRST_SPLIT_BILL = "OTP_FIRST_SPLIT_BILL";
    public static final String OTP_FIRST_PAY_CARD = "OTP_FIRST_PAY_CARD";
    public static final String OTP_FIRST_PAY_CARD_NO_FEE = "OTP_FIRST_PAY_CARD_NO_FEE";
    public static final String OTP_FIRST_PAY_CARD_ACTIVATE = "OTP_FIRST_PAY_CARD_ACTIVATE";
    public static final String OTP_FIRST_FREEZE_CARD = "OTP_FIRST_FREEZE_CARD";
    public static final String OTP_FIRST_CARD_DETAIL = "OTP_FIRST_CARD_DETAIL";
    public static final String OTP_FIRST_UNFREEZE_CARD = "OTP_FIRST_UNFREEZE_CARD";
    public static final String OTP_FIRST_BLOCK_CARD = "OTP_FIRST_BLOCK_CARD";
    public static final String OTP_FIRST_INTER_TOP_UP = "OTP_FIRST_INTER_TOP_UP";
    public static final String OTP_FIRST_CHARITY = "OTP_FIRST_CHARITY";
    public static final String OTP_FIRST_SET_KSA_CARD_LIMIT = "OTP_FIRST_SET_KSA_CARD_LIMIT";
    public static final String OTP_FIRST_KSA_CARD_BLOCK = "OTP_FIRST_KSA_CARD_BLOCK";
    public static final String OTP_FIRST_KSA_CARD_SET_PIN = "OTP_FIRST_KSA_CARD_SET_PIN";
    public static final String OTP_FIRST_KSA_CARD_SET_CHANNEL = "OTP_FIRST_KSA_CARD_SET_CHANNEL";
    public static final String OTP_FIRST_CHANGE_LOGIN_PD = "OTP_FIRST_CHANGE_LOGIN_PD";
    public static final String OTP_FIRST_HyperPay_ADD_MONEY = "OTP_FIRST_HyperPay_ADD_MONEY";
    public static final String OTP_FIRST_ADD_SEND_BENE = "OTP_FIRST_ADD_SEND_BENE";
    public static final String OTP_FIRST_RECEIVE_PAY = "OTP_FIRST_RECEIVE_PAY";
    //需要手动发送OTP
    public static final String OTP_SEND_CODE_000002 = "000002";
    //无需手动发送OTP
    public static final String OTP_SEND_CODE_000001 = "000001";
    public static final String DEVICE_ID_CHANGE = "DEVICE_ID_CHANGE";
    public static final String DEVICE_TOKEN_CHANGE = "DEVICE_TOKEN_CHANGE";
    public static final String USER_STATUS_S_LOGIN = "USER_STATUS_S_LOGIN";
    public static final String DATA_USER = "DATA_USER";


    public static final String BIND_CARD_3DS = "BIND_CARD_3DS";
    public static final String M_URL = "M_URL";
    public static final String terms_condition_type = "terms_condition_type";

    public static final String CARDDETAILS = "CARD_DETAILS";
    public static final String ONLYCARD = "CARD_ONLY";


    public static final String SAU = "SAU";
    public static final String DOMESTIC = "Domestic";
    public static final String CHANNEL_CODE = "channelCode";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String BILLER_TYPE = "billerType";

    public static final String TITLE = "TITLE";

    /**
     * 模块功能开放设置
     */
    public static final String tab_budget = "tab_budget";
    public static final String tab_cards = "tab_cards";
    public static final String tab_chat = "tab_chat";
    public static final String cards_addNew = "cards_addNew";
    public static final String cards_limit = "cards_limit";
    public static final String cards_limit_change = "cards_limit_change";
    public static final String cards_freeze = "cards_freeze";
    public static final String cards_settings = "cards_settings";
    public static final String home_addMoney = "home_addMoney";
    public static final String home_send = "home_send";
    public static final String home_request = "home_request";
    public static final String home_mobileTopup = "home_mobileTopup";
    public static final String home_intTransfer = "home_intTransfer";
    public static final String home_billPayments = "home_billPayments";
    public static final String home_splitBill = "home_splitBill";
    public static final String home_charity = "home_charity";
    public static final String home_marketplace = "home_marketplace";
    public static final String home_todaySpending = "home_todaySpending";
    public static final String home_leftSpend = "home_leftSpend";
    public static final String home_rewards = "home_rewards";
    public static final String home_analytics = "home_analytics";
    public static final String profile_rewards = "profile_rewards";
    public static final String profile_myProfile = "profile_myProfile";
    public static final String profile_accountDetails = "profile_accountDetails";
    public static final String profile_security = "profile_security";
    public static final String profile_loginFace = "profile_loginFace";
    public static final String profile_payFace = "profile_payFace";
    public static final String profile_changeLoginPd = "profile_changeLoginPwd";
    public static final String profile_changePayPd = "profile_changePayPwd";
    public static final String profile_forgotLoginPd = "profile_forgotLoginPwd";
    public static final String profile_forgotPayPd = "profile_forgotPayPwd";
    public static final String addMoney_applePay = "addMoney_applePay";
    public static final String change_mobile = "change_mobile";
    public static final String card_detail = "card_detail";
    public static final String card_history = "card_history";
    public static final String tab_history = "tab_history";
    public static final String confirm_request = "confirm_request";
    public static final String confirm_spilit_request = "confirm_spilit_request";
    public static final String addMoney_cards = "addMoney_cards";
    public static final String addMoney_bank = "addMoney_bank";
    public static final String send_contacts = "send_contacts";
    public static final String send_scan = "send_scan";
    public static final String send_localTransfer = "send_localTransfer";
    public static final String send_intTransfer = "send_intTransfer";
    public static final String send_withdraw = "send_withdraw";
    public static final String send_myCode = "send_myCode";
    public static final String registered = "registered";
    public static final String unregistered = "unregistered";

    /**
     * chat
     */
    public static final String gcChatSession = "gcChatSession";
}
