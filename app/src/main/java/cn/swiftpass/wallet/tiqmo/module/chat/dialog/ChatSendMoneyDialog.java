package cn.swiftpass.wallet.tiqmo.module.chat.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ChatSendMoneyDialog extends BottomDialog {

    private String money;
    private TransferLimitEntity transferLimitEntity;

    private int transferType;

    private UserInfoEntity userInfoEntity;
    private String transferPurpose;
    private String transferPurposeDesc;

    private Context mContext;

    private ConfirmListener confirmListener;

    private EditTextWithDel etAmount;
    private CustomizeEditText etNote;
    private TextView tvSendMoney;
    private String valiableMoney,monthLimitMoney;
    private String note;

    private ChatTransferEntity chatTransferEntity;

    public ChatSendMoneyDialog(Context context,TransferLimitEntity transferLimitEntity,int transferType) {
        super(context);
        this.mContext = context;
        this.transferLimitEntity = transferLimitEntity;
        this.transferType = transferType;
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        initViews(context);
    }

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public interface ConfirmListener {
        void clickConfirm(ChatTransferEntity chatTransferEntity);
        void clickCancel();
    }

    private void initViews(Context context) {
        chatTransferEntity = new ChatTransferEntity();
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_chat_send_money, null);
        tvSendMoney = contentView.findViewById(R.id.tv_send_money);
        etAmount = contentView.findViewById(R.id.et_amount);
        TextView tvMoneyCurrency = contentView.findViewById(R.id.tv_money_currency);
        TextView tv100 = contentView.findViewById(R.id.tv_100);
        TextView tv200 = contentView.findViewById(R.id.tv_200);
        TextView tv500 = contentView.findViewById(R.id.tv_500);
        TextView tv1000 = contentView.findViewById(R.id.tv_1000);
        etNote = contentView.findViewById(R.id.et_note);
        tv100.setText(Constants.AMOUNT_100);
        tv200.setText(Constants.AMOUNT_200);
        tv500.setText(Constants.AMOUNT_500);
        tv1000.setText(Constants.AMOUNT_1000);
        EditTextWithDel etPurpose = contentView.findViewById(R.id.et_purpose);
        etPurpose.setFocusableFalse();
        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        etAmount.getEditText().setHint("0.00");

        valiableMoney = userInfoEntity.getBalanceNumber();
        etPurpose.getTlEdit().setHint(mContext.getString(R.string.IMR_new_10));

        monthLimitMoney = transferLimitEntity.monthLimitAmt;

        //两位小数过滤
        etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                money = amount.replace(",", "");
                if (TextUtils.isEmpty(money)) {
                    tvMoneyCurrency.setVisibility(View.GONE);
                    tvSendMoney.setEnabled(false);
                } else {
                    try {
                        checkContinueStatus();
                    } catch (Exception e) {
                        tvSendMoney.setEnabled(false);
                    }
                    if (!Constants.AMOUNT_100.equals(money)) {
                        tv100.setSelected(false);
                    }
                    if (!Constants.AMOUNT_200.equals(money)) {
                        tv200.setSelected(false);
                    }
                    if (!Constants.AMOUNT_500.equals(money)) {
                        tv500.setSelected(false);
                    }
                    if (!Constants.AMOUNT_1000.equals(money)) {
                        tv1000.setSelected(false);
                    }
                    tvMoneyCurrency.setVisibility(View.VISIBLE);
                }
            }
        });

        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                note = editable.toString();
                checkContinueStatus();
            }
        });

        etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (!hasFocus) {
                    checkContinueStatus();
                }
            }
        });

        etPurpose.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                AppClient.getInstance().getTransferPurpose("PURPOSE_OF_TRANSFER",new ResultCallback<BusinessParamEntity>(){

                    @Override
                    public void onResult(BusinessParamEntity businessParamEntity) {
                        showProgress(false);

                        if (businessParamEntity != null) {
                            List<SelectInfoEntity> selectInfoEntityList = businessParamEntity.businessParamList;
                            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, mContext.getString(R.string.IMR_new_10), false, true);
                            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    SelectInfoEntity  selectInfoEntity = selectInfoEntityList.get(position);
                                    if (selectInfoEntity != null) {
                                        transferPurpose = selectInfoEntity.businessParamKey;
                                        transferPurposeDesc = selectInfoEntity.businessParamValue;
                                        etPurpose.getEditText().setText(transferPurposeDesc);
                                    }
                                    checkContinueStatus();
                                }
                            });
                            dialog.showNow(((BaseCompatActivity)mContext).getSupportFragmentManager(), "getNationalitySuccess");
                        }
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                    }
                });
            }
        });

        tv100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv100.setSelected(true);
                tv200.setSelected(false);
                tv500.setSelected(false);
                tv1000.setSelected(false);

                etAmount.setContentText(Constants.AMOUNT_100);
                money = Constants.AMOUNT_100;
                checkContinueStatus();
            }
        });

        tv200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv200.setSelected(true);
                tv100.setSelected(false);
                tv500.setSelected(false);
                tv1000.setSelected(false);

                etAmount.setContentText(Constants.AMOUNT_200);
                money = Constants.AMOUNT_200;
                checkContinueStatus();
            }
        });

        tv500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv500.setSelected(true);
                tv200.setSelected(false);
                tv100.setSelected(false);
                tv1000.setSelected(false);

                etAmount.setContentText(Constants.AMOUNT_500);
                money = Constants.AMOUNT_500;
                checkContinueStatus();
            }
        });

        tv1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1000.setSelected(true);
                tv200.setSelected(false);
                tv500.setSelected(false);
                tv100.setSelected(false);

                etAmount.setContentText(Constants.AMOUNT_1000);
                money = Constants.AMOUNT_1000;
                checkContinueStatus();
            }
        });

        tvSendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(confirmListener != null){
                    note = etNote.getText().toString().trim();
//                    chatTransferEntity.note = note;
//                    chatTransferEntity.transferPurpose = transferPurpose;
//                    chatTransferEntity.transferPurposeDesc = transferPurposeDesc;
//                    chatTransferEntity.orderAmount = money;
                    confirmListener.clickConfirm(chatTransferEntity);
                }
            }
        });
        setContentView(contentView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private void checkContinueStatus() {

        tvSendMoney.setEnabled(false);
        if(BigDecimalFormatUtils.isZero(money))return;
        if(TextUtils.isEmpty(note))return;
        if(BigDecimalFormatUtils.compareBig(money,valiableMoney)){
            etAmount.showErrorViewWithMsg(mContext.getString(R.string.sprint20_84));
            return;
        }
        if(BigDecimalFormatUtils.compareBig(money,monthLimitMoney)){
            etAmount.showErrorViewWithMsg(mContext.getString(R.string.add_money_12));
            return;
        }
        if(TextUtils.isEmpty(transferPurpose))return;
        tvSendMoney.setEnabled(true);
    }
}
