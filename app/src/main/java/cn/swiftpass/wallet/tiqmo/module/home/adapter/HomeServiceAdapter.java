package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.HomeIconEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class HomeServiceAdapter extends BaseRecyclerAdapter<HomeIconEntity> {

    public HomeServiceAdapter(@Nullable List<HomeIconEntity> data) {
        super(R.layout.item_home_service, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, HomeIconEntity homeIconEntity, int position) {
        baseViewHolder.setImageResource(R.id.iv_icon, ResourceHelper.getInstance(mContext).getIdentifierByAttrId(homeIconEntity.imgId));
        baseViewHolder.setBackgroundRes(R.id.rl_service, ThemeSourceUtils.getSourceID(mContext, R.attr.shape_091b57_white));
        baseViewHolder.setText(R.id.tv_icon, homeIconEntity.textId);
        baseViewHolder.setTextColor(R.id.tv_icon, ResourceHelper.getInstance(mContext).getColorByAttr(R.attr.color_white_3a3b44));
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }
}
