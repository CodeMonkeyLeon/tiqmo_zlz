package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class RootCheckDialog extends BottomDialog {

    private Context mContext;
    private static RootCheckDialog rootCheckDialog = null;

    private CloseListener closeListener;

    public void setCloseListener(CloseListener closeListener) {
        this.closeListener = closeListener;
    }

    public RootCheckDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface CloseListener {
        void clickClose();
    }

    public static RootCheckDialog getInstance(Context context) {
        rootCheckDialog = new RootCheckDialog(context);
        return rootCheckDialog;
    }

    public void reset() {
        rootCheckDialog = null;
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_root_check, null);
        TextView tvClose = view.findViewById(R.id.tv_root_close);

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (closeListener != null) {
                    closeListener.clickClose();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (rootCheckDialog != null) {
                    rootCheckDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
