package cn.swiftpass.wallet.tiqmo.module.imr.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.contract.GetOtpTypePresenter;
import cn.swiftpass.wallet.tiqmo.base.contract.GetOtpTypeView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;

public class BeneficiaryListContract {

    public interface View extends GetOtpTypeView<Presenter> {
        // 显示联系人列表
        void showBeneficiaryList(List<ImrBeneficiaryListEntity.ImrBeneficiaryInfo> targetAccList);

        void showErrorMsg(String errorCode, String errorMsg);

        void showRemoveBeneficiarySuccess();

        void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity);

        void imrExchangeRateFail(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void showSendMoneyFragment();

        void showNoBeneficiary();

        void activateBeneficiarySuccess(Void result);

        void startEditView(ImrBeneficiaryDetails result);
    }


    public interface Presenter extends GetOtpTypePresenter<View> {
        // 获取联系人列表
        void getBeneficiaryList(boolean isInit);
        // remove imr 收款人
        void removeBeneficiary(String payeeInfoId);
        // 查询 imr 收款人详情
        void getImrBeneficiaryDetail(ImrBeneficiaryListEntity.ImrBeneficiaryBean payeeInfoId);

        void imrExchangeRate(String payeeInfoId,String sourceAmount,
                             String destinationAmount,String channelCode);

        void activateBeneficiary(String payeeInfoId);

        void getLimit(String type);
    }
}
