package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class LogOutDialog extends BottomDialog{

    private Context mContext;

    private LogoutClickListener logoutClickListener;

    public void setLogoutClickListener(LogoutClickListener logoutClickListener) {
        this.logoutClickListener = logoutClickListener;
    }

    public LogOutDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public interface LogoutClickListener {
        void clickLogout();
        void clickKeepSign();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_log_out, null);
        TextView tvLogOut = view.findViewById(R.id.tv_log_out);
        TextView tvKeepSign = view.findViewById(R.id.tv_keep_sign);

        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (logoutClickListener != null) {
                    logoutClickListener.clickLogout();
                }
            }
        });

        tvKeepSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
