package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillRefundListEntity extends BaseEntity {
    public List<PayBillRefundEntity> refundInfoList = new ArrayList<>();
}
