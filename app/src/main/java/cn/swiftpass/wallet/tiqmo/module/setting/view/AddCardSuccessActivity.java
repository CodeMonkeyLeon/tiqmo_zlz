package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class AddCardSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.tv_dialog_title)
    TextView tvDialogTitle;
    @BindView(R.id.iv_mini_card)
    ImageView ivMiniCard;
    @BindView(R.id.tv_card_no)
    TextView tvCardNo;
    @BindView(R.id.fl_card_small_face)
    FrameLayout flCardSmallFace;
    @BindView(R.id.tv_dialog_message)
    TextView tvDialogMessage;
    @BindView(R.id.tv_dialog_button)
    TextView tvDialogButton;

    private CardNotifyEntity cardNotifyEntity;
    private HashMap<String, Object> mHashMaps_mini = new HashMap<>();

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_add_card_success;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();

        mHashMaps_mini.put("424242", R.drawable.card_middle_new);
        mHashMaps_mini.put("454347", R.drawable.card_middle_new);
        mHashMaps_mini.put("543603", R.drawable.card_middle_new);
        mHashMaps_mini.put("222300", R.drawable.card_middle_new);
        mHashMaps_mini.put("519999", R.drawable.card_middle_new);
        mHashMaps_mini.put("500018", R.drawable.card_middle_new);
        mHashMaps_mini.put("465858", R.drawable.card_middle_new);
        if (getIntent() != null && getIntent().getExtras() != null) {
            cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_CARD_ENTITY);
            String CardNo = cardNotifyEntity.cardNo;
            String regex = "(.{4})";
            String CardNo_with_black = CardNo.replaceAll(regex, "$1  ");
            tvCardNo.setText(AndroidUtils.forceL2R(CardNo_with_black));

            if (!TextUtils.isEmpty(cardNotifyEntity.cardFaceMini)) {
                String card_url = (String) mHashMaps_mini.get(cardNotifyEntity.cardFaceMini);
                String cardId = cardNotifyEntity.cardNo.substring(0, 6);
                Glide.with(this)
                        .load(card_url)
                        .placeholder((int) mHashMaps_mini.get(cardId))
                        .error((int) mHashMaps_mini.get(cardId))
                        .into(ivMiniCard);
            } else {
                ivMiniCard.setImageResource((int) mHashMaps_mini.get(cardNotifyEntity.cardNo.substring(0, 6)));
            }
        }
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @OnClick(R.id.tv_dialog_button)
    public void onViewClicked() {
        finish();
    }
}
