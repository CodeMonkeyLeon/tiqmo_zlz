package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.imr.contract.ImrSendMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ImrMoreInfoPresenter implements ImrSendMoneyContract.MoreInfoPresenter {

    ImrSendMoneyContract.MoreInfoView moreInfoView;

    @Override
    public void sendMoreInfo(List<ImrComplianceEntity> compliance) {
        AppClient.getInstance().sendImrCompliance(compliance, new LifecycleMVPResultCallback<ImrOrderInfoEntity>(moreInfoView,true) {
            @Override
            protected void onSuccess(ImrOrderInfoEntity result) {
                moreInfoView.sendMoreInfoSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                moreInfoView.sendMoreInfoFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(moreInfoView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                moreInfoView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                moreInfoView.checkOutFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(ImrSendMoneyContract.MoreInfoView moreInfoView) {
        this.moreInfoView = moreInfoView;
    }

    @Override
    public void detachView() {
        this.moreInfoView = null;
    }
}
