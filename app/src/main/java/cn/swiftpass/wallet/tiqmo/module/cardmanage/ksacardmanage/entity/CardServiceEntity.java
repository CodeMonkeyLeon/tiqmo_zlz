package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CardServiceEntity extends BaseEntity {
    public int imgId;
    public int textId;

    public CardServiceEntity(int imgId, int textId) {
        this.imgId = imgId;
        this.textId = textId;
    }
}
