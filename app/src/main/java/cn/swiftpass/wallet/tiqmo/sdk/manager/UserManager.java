package cn.swiftpass.wallet.tiqmo.sdk.manager;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.JsonSyntaxException;

import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AccountInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AvatarUrlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.AuthApi;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.UserApi;
import cn.swiftpass.wallet.tiqmo.sdk.util.CallbackUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.CertsDownloadManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.LanguageDownloadManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.Sha256Util;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import dev.b3nedikt.app_locale.AppLocale;
import dev.b3nedikt.restring.Restring;

public class UserManager {

    private static final String STORAGE_KEY_LAST_USER_INFO = "UserInfo";
    //本地保存两份配置文件  防止接口获取失败
    private static final String STORAGE_KEY_LAST_CONFIG_INFO_KSA = "ConfigInfo_KSA";

    private AppClient mAppClient;
    private UserInfoEntity mUserInfo;
    private ConfigEntity mConfigEntity;
    private final List<OnUserInfoChangeListener> mUserInfoChangeListeners;
    private UserApi mUserApi;
    private AuthApi mAuthApi;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;

    public UserManager(AppClient client) {
        mAppClient = client;
        mUserApi = ApiHelper.getApi(UserApi.class);
        mAuthApi = ApiHelper.getApi(AuthApi.class);
        mUserInfoChangeListeners = Collections.synchronizedList(new LinkedList<OnUserInfoChangeListener>());
    }

    //登录成功之后就要绑定用户信息
    public void bindUserInfo(UserInfoEntity userInfoEntity, boolean isLogin) {
        mUserInfo = userInfoEntity;
        //登录才绑定
        if (isLogin) {
            mAppClient.getStorageHelper().putToConfigurationPreferences(STORAGE_KEY_LAST_USER_INFO, ApiHelper.GSON.toJson(mUserInfo));
        }
    }

    //获取用户信息
    public UserInfoEntity getUserInfo() {
        if (mUserInfo == null) {
            return getLastUserInfo();
        }
        return mUserInfo;
    }

    //获取配置信息
    public ConfigEntity getConfig() {
        if (mConfigEntity == null) {
            return getKsaConfigEntity();
        }
        return mConfigEntity;
    }

    public ImrBeneficiaryEntity getImrBeneficiaryEntity() {
        if (imrBeneficiaryEntity == null) {
            return new ImrBeneficiaryEntity();
        }
        return imrBeneficiaryEntity;
    }

    public void setImrBeneficiaryEntity(ImrBeneficiaryEntity imrBeneficiaryEntity) {
        this.imrBeneficiaryEntity = imrBeneficiaryEntity;
    }

    //更新用户ID到期日
    public void renewId(LifecycleMVPResultCallback<ResultEntity> callback) {
        mUserApi.renewId().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //上次头像
    public void uploadAvatar(String localAvatarPath, final ResultCallback<AvatarUrlEntity> callback) {
        mUserApi.uploadAvatar(localAvatarPath)
                .enqueue(new ResponseCallbackWrapper<>(new ResultCallback<AvatarUrlEntity>() {
                    @Override
                    public void onResult(AvatarUrlEntity result) {
                        mUserInfo.setAvatar(result.getUrl());
                        mAppClient.getStorageHelper().putToConfigurationPreferences(STORAGE_KEY_LAST_USER_INFO, ApiHelper.GSON.toJson(mUserInfo));
                        UserInfoEntity userInfo = getUserInfo();
                        for (OnUserInfoChangeListener listener : mUserInfoChangeListeners) {
                            listener.onUserInfoChange(userInfo);
                        }
                        CallbackUtil.callResult(result, callback);
                    }

                    @Override
                    public void onFailure(String code, String error) {
                        CallbackUtil.callFailure(code, error, callback);
                    }
                }));
    }

    //修改用户基本信息
    public void updateUserInfo(UserInfoEntity userInfoEntity,
                               final ResultCallback<UserInfoEntity> callback) {
        mUserApi.updateUserInfo(userInfoEntity.fullName, userInfoEntity.email, userInfoEntity.gender, userInfoEntity.birthday, userInfoEntity.statesId, userInfoEntity.citiesId, userInfoEntity.address,
                userInfoEntity.sourceOfFundCode,userInfoEntity.employmentCode, userInfoEntity.professionCode, userInfoEntity.businessTypeCode, userInfoEntity.companyName, userInfoEntity.salaryRangeCode, userInfoEntity.imrSetUpFlag,
                        userInfoEntity.phone,userInfoEntity.orderNo)
                .enqueue(new ResponseCallbackWrapper<>(new ResultCallback<UserInfoEntity>() {
                    @Override
                    public void onResult(UserInfoEntity result) {
                        CallbackUtil.callResult(result, callback);
                    }

                    @Override
                    public void onFailure(String code, String error) {
                        CallbackUtil.callFailure(code, error, callback);
                    }
                }));
    }

    public void getAccountInfo(String type,final ResultCallback<AccountInfoEntity> callback){
        mUserApi.getAccountInfo(type).enqueue(new ResponseCallbackWrapper<>(new ResultCallback<AccountInfoEntity>(){

            @Override
            public void onResult(AccountInfoEntity response) {
                CallbackUtil.callResult(response, callback);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                CallbackUtil.callFailure(errorCode, errorMsg, callback);
            }
        }));
    }

    public ConfigEntity getKsaConfigEntity() {
        String configInfo = mAppClient.getStorageHelper().getFromConfigurationPreferences(STORAGE_KEY_LAST_CONFIG_INFO_KSA);

        if (TextUtils.isEmpty(configInfo)) {
            return null;
        } else {
            try {
                return ApiHelper.GSON.fromJson(configInfo, ConfigEntity.class);
            } catch (JsonSyntaxException e) {
                return null;
            }

        }
    }

    public void verifyEmail(String email,final ResultCallback<Void> callback) {
        mUserApi.verifyEmail(email).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    public void checkLanguageAgain(final ResultCallback<LanguageListEntity> callback) {
        mUserApi.checkLanguageUpdate().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取多语言包信息
    public void checkLanguageUpdate(final ResultCallback<LanguageListEntity> callback) {
        mUserApi.checkLanguageUpdate().enqueue(new ResponseCallbackWrapper<>(new ResultCallback<LanguageListEntity>() {
            @Override
            public void onResult(LanguageListEntity languageListEntity) {

                if(languageListEntity != null){
                    List<Locale> localeList = languageListEntity.getLocaleList();
                    if(localeList != null && localeList.size()>0) {
                        AppLocale.setSupportedLocales(localeList);
                    }else{
                        AppLocale.setSupportedLocales(Arrays.asList(Locale.ENGLISH, new Locale("ar")));
                    }

                    String version = languageListEntity.version;
                    String lastVersion = SpUtils.getInstance().getLanguageVersion();
                    if(!TextUtils.isEmpty(version) && !lastVersion.equals(version)) {
                        LanguageDownloadManager.getInstance().initCertificate(ProjectApp.getContext(), version, new LanguageDownloadManager.OnCertDownLoadListener() {
                            @Override
                            public void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates) {
                                if(isSuccess){
                                    List<LanguageCodeEntity> languageCodes = new ArrayList<>();
                                    if(languageListEntity != null) {
                                        languageCodes = languageListEntity.languageCodes;
                                    }else{
                                        LanguageCodeEntity languageCodeEntity1 = new LanguageCodeEntity("English","en_US");
                                        LanguageCodeEntity languageCodeEntity2 = new LanguageCodeEntity("العربية","ar");
                                        languageCodes.add(languageCodeEntity1);
                                        languageCodes.add(languageCodeEntity2);
                                    }
                                    int size = localeList.size();
                                    for (int i=0;i<size;i++) {
                                        Locale locale = localeList.get(i);
                                        LanguageCodeEntity languageCodeEntity = languageCodes.get(i);
                                        Restring.putStrings(locale, AndroidUtils.getLanguageMap(languageCodeEntity));
                                    }
                                }
                            }
                        });
                    }
                    SpUtils.getInstance().setLanguageListEntity(languageListEntity);
                    CallbackUtil.callResult(languageListEntity, callback);
//                    List<Locale> localeList = AppLocale.getSupportedLocales();
//                    LanguageListEntity languageListEntity = SpUtils.getInstance().getLanguageVersionguageListEntity();
                }
            }

            @Override
            public void onFailure(String code, String error) {

                List<Locale> localeList = Arrays.asList(Locale.ENGLISH, new Locale("ar"));
                AppLocale.setSupportedLocales(localeList);
                List<LanguageCodeEntity> languageCodes = new ArrayList<>();
                LanguageCodeEntity languageCodeEntity1 = new LanguageCodeEntity("English","en_US");
                LanguageCodeEntity languageCodeEntity2 = new LanguageCodeEntity("العربية","ar");
                languageCodes.add(languageCodeEntity1);
                languageCodes.add(languageCodeEntity2);
                int size = localeList.size();
                for (int i=0;i<size;i++) {
                    Locale locale = localeList.get(i);
                    LanguageCodeEntity languageCodeEntity = languageCodes.get(i);
                    Restring.putStrings(locale, AndroidUtils.getLanguageMap(languageCodeEntity));
                }
//                CallbackUtil.callFailure(code, error, callback);
            }
        }));
    }

    //获取配置信息
    public void getConfig(final ResultCallback<ConfigEntity> callback) {
//        if (mConfigEntity != null) {
//            return;
//        }
        mUserApi.getConfig().enqueue(new ResponseCallbackWrapper<>(new ResultCallback<ConfigEntity>() {
            @Override
            public void onResult(ConfigEntity configInfo) {
                mAppClient.getStorageHelper().putToConfigurationPreferences(STORAGE_KEY_LAST_CONFIG_INFO_KSA, ApiHelper.GSON.toJson(configInfo));
                mConfigEntity = configInfo;
                if (mConfigEntity != null) {
                    String certZipName = SpUtils.getInstance().getCertZipName();
                    //如果证书压缩包名称更新  则重新下载证书压缩包
                    if (!TextUtils.isEmpty(certZipName) && !certZipName.equals(mConfigEntity.certZipName)) {
                        CertsDownloadManager.getInstance().initCertificate(ProjectApp.getContext());
                        SpUtils.getInstance().setCertZipName(mConfigEntity.certZipName);
                    }
                }
                CallbackUtil.callResult(configInfo, callback);
            }

            @Override
            public void onFailure(String code, String error) {
//                CallbackUtil.callFailure(code, error, callback);
            }
        }));
    }

    //获取用户基本信息
    public void getUserInfo(final ResultCallback<UserInfoEntity> callback) {
        mUserApi.getUserInfo().enqueue(new ResponseCallbackWrapper<>(new ResultCallback<UserInfoEntity>() {
            @Override
            public void onResult(UserInfoEntity mUserInfo) {
                mAppClient.getStorageHelper().putToConfigurationPreferences(STORAGE_KEY_LAST_USER_INFO, ApiHelper.GSON.toJson(mUserInfo));
                UserInfoEntity userInfo = getUserInfo();
                for (OnUserInfoChangeListener listener : mUserInfoChangeListeners) {
                    listener.onUserInfoChange(userInfo);
                }
                CallbackUtil.callResult(mUserInfo, callback);
            }

            @Override
            public void onFailure(String code, String error) {
                CallbackUtil.callFailure(code, error, callback);
            }
        }));
    }

    //校验登录密码
    public void checkLoginPassword(String pdType, @Nullable String oldPassword, ResultCallback<Void> callback) {
        mUserApi.checkPassword(pdType, Sha256Util.sha256WithSalt(oldPassword)).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账户停用
    public void stopAccount(ResultCallback<Void> callback) {
        mUserApi.stopAccount().enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //账户停用
    public void getStateList(ResultCallback<StateListEntity> callback) {
        mUserApi.getStateList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取nafath认证Url(3150)
    public void getNafathUrl(String callingCode, String phone, String recipientId,String sceneType,ResultCallback<NafathUrlEntity> callback) {
        mUserApi.getNafathUrl(callingCode,phone,recipientId,sceneType).enqueue(new ResponseCallbackWrapper<>(callback));
    }
    //认证状态查询(3151)
    public void getNafathResult(String callingCode, String phone,String hashedState,String recipientId,String random,String sceneType,ResultCallback<NafathResultEntity> callback) {
        mUserApi.getNafathResult(callingCode,phone,hashedState,recipientId,random,sceneType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //判断手机号是否已注册(3157)
    public void checkUserExist(String callingCode, String phone, double longitude, double latitude,LifecycleMVPResultCallback<CheckPhoneEntity> callback) {
        mUserApi.checkUserExist(callingCode, phone, longitude, latitude).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //用户信息校验用于新设备登录(3159)
    public void checkNewDevice(String callingCode, String phone, String residentId,String birthday,String sceneType,String userId, LifecycleMVPResultCallback<CheckPhoneEntity> callback) {
        mUserApi.checkNewDevice(callingCode, phone, residentId, birthday,sceneType,userId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //激活用户状态(3134)
    public void activateUserStatus(String callingCode,String loginId,String lockAcc,ResultCallback<Void> callback) {
        mUserApi.activateUserStatus(lockAcc, callingCode, loginId).enqueue(new ResponseCallbackWrapper<>(callback));
    }
    //更新用户ID到期日(3118)
    public void renewId(String callingCode,String loginId,String lockAcc,ResultCallback<ResultEntity> callback) {
        mUserApi.renewId(callingCode, loginId,lockAcc).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //校验支付密码
    public void checkPaymentPassword(String password, ResultCallback<Void> callback) {
        mUserApi.checkPassword(Sha256Util.sha256WithSalt(password), UserApi.SECRET_TYPE_TRANSACTION, null, null).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询用户修改手机号费用(3161)
    public void getChangePhoneFee(ResultCallback<ChangePhoneFeeEntity> callback) {
        mUserApi.getChangePhoneFee().enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //查询用户修改手机号费用(3162)
    public void payChangePhoneFee(String totalAmount,String vat,ResultCallback<ChangePhoneResultEntity> callback) {
        mUserApi.payChangePhoneFee(totalAmount,vat).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询用户禁用功能（3280）
    public void getUserDisableAppFuncCodeList(String callingCode,
                                  String phone, ResultCallback<UserUnableUseEntity> callback) {
        mUserApi.getUserDisableAppFuncCodeList(callingCode,phone).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //实名认证接口，目前没有用到，如果后期用到了，估计serverID会改，请注意了
    public void authentication(final String firstName, final String lastName, final String idNumberType, final String idNumber, final ResultCallback<Void> callback) {
        mUserApi.authentication(firstName, lastName, idNumberType, idNumber).enqueue(new ResponseCallbackWrapper<>(new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {
                mUserInfo.setAuthentication(AppConfig.TRUE);
                mUserInfo.setUserName(firstName + lastName);
                CallbackUtil.callResult(result, callback);
            }

            @Override
            public void onFailure(String code, String error) {
                CallbackUtil.callFailure(code, error, callback);
            }
        }));
    }

    //解绑微信，目前没有用到，如果后期用到了，估计serverID会改，请注意了
    public void unbindThirdPartyOfWeChat(ResultCallback<Void> callback) {
        mUserApi.unbindThirdPartAuthorization(UserApi.THIRD_PARTY_TYPE_WE_CHAT)
                .enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除最近一个的登录信息
    public void clearLastUserInfo() {
        mAppClient.getStorageHelper().putToConfigurationPreferences(STORAGE_KEY_LAST_USER_INFO, "");
    }

    //获取最近一个的登录信息

    @Nullable
    public UserInfoEntity getLastUserInfo() {
        String lastInfo = mAppClient.getStorageHelper().getFromConfigurationPreferences(STORAGE_KEY_LAST_USER_INFO);
        if (TextUtils.isEmpty(lastInfo)) {
            return null;
        } else {
            try {
                return ApiHelper.GSON.fromJson(lastInfo, UserInfoEntity.class);
            } catch (JsonSyntaxException e) {
                return null;
            }

        }
    }

    public void reset() {
        mUserInfoChangeListeners.clear();
        mUserInfo = null;
    }

    //当用户信息发生改变的时候
    public interface OnUserInfoChangeListener {
        void onUserInfoChange(UserInfoEntity info);
    }
}
