package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class InviteEntity extends BaseEntity {
    //	邀请人数
    public String countRecord = "0";
    //	邀请人用户收到返现金额
    public String userAmount;
    //	邀请人返现金额
    public String inviterBenefit;
    //	被邀请人返现金额
    public String inviteeBenefit;
    //	币种
    public String currency;
    //	活动开始时间 格式： dd/MM/yyyy
    public String activityStartTime;
    //	活动结束时间 格式： dd/MM/yyyy
    public String activityEndTime;
    //	分享链接
    public String shareUrl;
    //	是否有邀请码活动 1: 有 0: 无
    public String hasReferralCodeActivityFlag;
    public String link;
    public String firebaseDynamicLink;
    public String referralCode;


}
