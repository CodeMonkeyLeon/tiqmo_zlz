package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class AddBeneficiarySuccessDialog extends BottomDialog {

    private Context mContext;
    private static AddBeneficiarySuccessDialog addBeneficiarySuccessDialog = null;

    private AddBeneficiaryDoneListener addBeneficiaryDoneListener;

    public void setAddBeneficiaryDoneListener(AddBeneficiaryDoneListener addBeneficiaryDoneListener) {
        this.addBeneficiaryDoneListener = addBeneficiaryDoneListener;
    }

    public AddBeneficiarySuccessDialog(Context context, ImrBeneficiaryEntity imrBeneficiaryEntity) {
        super(context);
        this.mContext = context;
        initViews(mContext, imrBeneficiaryEntity);
    }


    public interface AddBeneficiaryDoneListener {
        void addBeneficiaryDone();
    }

    public static AddBeneficiarySuccessDialog getInstance(Context context, ImrBeneficiaryEntity imrBeneficiaryEntity) {
        addBeneficiarySuccessDialog = new AddBeneficiarySuccessDialog(context, imrBeneficiaryEntity);
        return addBeneficiarySuccessDialog;
    }

    public void reset() {
        addBeneficiarySuccessDialog = null;
    }

    private void initViews(Context mContext, ImrBeneficiaryEntity imrBeneficiaryEntity) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_imr_remove_beneficiary, null);
        TextView name = view.findViewById(R.id.tv_remove_beneficiary_name);
        ImageView flag = view.findViewById(R.id.iv_remove_beneficiary_flag);
        TextView country = view.findViewById(R.id.tv_remove_beneficiary_country);
        ImageView genderAvatar = view.findViewById(R.id.iv_remove_gender_avatar);
        TextView button = view.findViewById(R.id.tv_imr_remove_beneficiary);
//        TextView currency = view.findViewById(R.id.tv_remove_beneficiary_currency);
        button.setText(R.string.IMR_19_1);
        TextView title = view.findViewById(R.id.tv_imr_setup_notice_main_title);
        TextView notice = view.findViewById(R.id.tv_imr_remove_notice);
        ImageView ivImrRemove = view.findViewById(R.id.iv_imr_remove);
        ivImrRemove.setVisibility(View.GONE);
        title.setText(R.string.IMR_94);
        notice.setVisibility(View.GONE);

        if (imrBeneficiaryEntity != null) {
            String sex = imrBeneficiaryEntity.sex;
            String countryLogo = imrBeneficiaryEntity.countryLogo;
            String fullName = imrBeneficiaryEntity.payeeFullName;
            String nickName = imrBeneficiaryEntity.nickName;
            String countryName = imrBeneficiaryEntity.destinationName;
            if (!TextUtils.isEmpty(countryLogo)) {
                Glide.with(mContext).clear(flag);
                Glide.with(mContext)
                        .load(countryLogo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(flag);
            }
            genderAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, sex));
            if (!TextUtils.isEmpty(nickName)) {
                name.setText(nickName);
            } else {
                name.setText(fullName);
            }
            country.setText(countryName);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addBeneficiaryDoneListener != null) {
                    addBeneficiaryDoneListener.addBeneficiaryDone();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (addBeneficiarySuccessDialog != null) {
                    addBeneficiarySuccessDialog = null;
                }
            }
        });

        setContentView(view);
        setCanceledOnTouchOutside(false);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
