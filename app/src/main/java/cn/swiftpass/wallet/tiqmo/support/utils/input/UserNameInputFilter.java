package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

import java.util.regex.Pattern;

/**
 * 用户名匹配过滤器
 * 最少一位字母 ，长度8到16位，允许大小写字母_-. 这些字符
 * created by junhua on 2019/7/24 13:54
 */
public class UserNameInputFilter implements InputFilter {
    public static final Pattern M_RESULT_PATTERN = Pattern.compile("^(?=.*[a-zA-Z])[\\.\\da-zA-Z_-]{8,16}$");
    private Pattern mPattern = Pattern.compile("^[\\.\\da-zA-Z_-]{0,16}$");

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        Log.d("filter", "filter() called with: source = [" + source + "], start = [" + start + "], end = [" + end + "], dest = [" + dest + "], dstart = [" + dstart + "], dend = [" + dend + "]");
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (mPattern.matcher(temp).matches()) {
            return source;
        }
        return "";
    }
}
