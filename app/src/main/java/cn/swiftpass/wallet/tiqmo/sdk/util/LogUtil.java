package cn.swiftpass.wallet.tiqmo.sdk.util;

import android.util.Log;

/**
 * Created by YZX on 2017年10月17日.
 * 生命太短暂,不要去做一些根本没有人想要的东西
 */


public class LogUtil {

    private static boolean isEnableV;
    private static boolean isEnableD;
    private static boolean isEnableI;
    private static boolean isEnableW;
    private static boolean isEnableE;
    private static boolean isEnableWTF;

    public static void setEnableLog(boolean isEnable) {
        isEnableV = isEnable;
        isEnableD = isEnable;
        isEnableI = isEnable;
        isEnableW = isEnable;
        isEnableE = isEnable;
        isEnableWTF = isEnable;
    }


    private static String[] generateStackInfo() {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[4];
        String className = stackTraceElement.getClassName();
        className = className.substring(className.lastIndexOf('.') + 1);
        return new String[]{
                className,
                stackTraceElement.getMethodName(),
                String.valueOf(stackTraceElement.getLineNumber())};
    }

    public static void v(String msg) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableV) {
            String[] stackInfo = generateStackInfo();
            Log.v(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg));
        }
    }

    public static void v(Number number) {
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.v(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], number == null ? "null" : number.toString()));
        }
    }

    public static void v(String msg, Throwable tr) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableV) {
            String[] stackInfo = generateStackInfo();
            Log.v(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg), tr);
        }
    }

    public static void d(String msg) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableD) {
            String[] stackInfo = generateStackInfo();
            Log.d(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg));
        }
    }

    public static void d(Number number) {
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.d(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], number == null ? "null" : number.toString()));
        }
    }

    public static void d(String msg, Throwable tr) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableD) {
            String[] stackInfo = generateStackInfo();
            Log.d(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg), tr);
        }
    }

    public static void i(String msg) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableI) {
            String[] stackInfo = generateStackInfo();
            Log.i(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg));
        }
    }

    public static void i(Number number) {
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.i(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], number == null ? "null" : number.toString()));
        }
    }

    public static void i(String msg, Throwable tr) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableI) {
            String[] stackInfo = generateStackInfo();
            Log.i(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg), tr);
        }
    }

    public static void w(String msg) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableW) {
            String[] stackInfo = generateStackInfo();
            Log.w(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg));
        }
    }

    public static void w(Number number) {
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.w(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], number == null ? "null" : number.toString()));
        }
    }

    public static void w(String msg, Throwable tr) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableW) {
            String[] stackInfo = generateStackInfo();
            Log.w(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg), tr);
        }
    }

    public static void e(String msg) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.e(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg));
        }
    }

    public static void e(Number number) {
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.e(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], number == null ? "null" : number.toString()));
        }
    }

    public static void e(String msg, Throwable tr) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.e(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg), tr);
        }
    }

    public static void wtf(String msg) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableWTF) {
            String[] stackInfo = generateStackInfo();
            Log.wtf(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg));
        }
    }

    public static void wtf(Number number) {
        if (isEnableE) {
            String[] stackInfo = generateStackInfo();
            Log.wtf(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], number == null ? "null" : number.toString()));
        }
    }

    public static void wtf(String msg, Throwable tr) {
        if (msg == null) {
            msg = "null";
        }
        if (isEnableWTF) {
            String[] stackInfo = generateStackInfo();
            Log.wtf(stackInfo[0], String.format("%s(%s.java:%s) ==>  %s", stackInfo[1], stackInfo[0], stackInfo[2], msg), tr);
        }
    }
}
