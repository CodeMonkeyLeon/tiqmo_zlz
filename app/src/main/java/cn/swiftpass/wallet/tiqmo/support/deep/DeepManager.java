package cn.swiftpass.wallet.tiqmo.support.deep;

import android.content.Context;

public class DeepManager {
    private static DeepManager sInstance = new DeepManager();

    public static DeepManager getInstance() {
        return sInstance;
    }

    public DeepBuilder with(Context mContext) {
        return new DeepBuilder(mContext);
    }
}
