package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.adapter.RechargeProductListAdapter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.ChooseProductEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.ChooseProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInputProductEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInterOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.RechargeCustomDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class RechargeProductListActivity extends BaseCompatActivity{
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_biller)
    RoundedImageView ivBiller;
    @BindView(R.id.tv_biller_name)
    TextView tvBillerName;
    @BindView(R.id.ll_sku_head)
    LinearLayout llSkuHead;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.con_custom)
    ConstraintLayout conCustom;
    @BindView(R.id.ry_all_product)
    RecyclerView ryAllProduct;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_custom_name)
    TextView tvCustomName;

    private RechargeProductListEntity rechargeProductListEntity;
    private RechargeOperatorEntity rechargeOperatorEntity;
    private RechargeInterOrderEntity rechargeInterOrderEntity;
    private RechargeOrderDetailEntity rechargeOrderDetailEntity;

    //选择套餐的产品信息
    public List<ChooseProductListEntity> changeProductList = new ArrayList<>();
    //输入金额的产品信息
    public RechargeInputProductEntity inputProduct;

    private RechargeProductListAdapter rechargeProductListAdapter;

    private StatusView historyStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private String operatorImgUrl,operatorName;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_product_list;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, ivHeadCircle);

        if(getIntent() != null && getIntent().getExtras() != null) {
            rechargeProductListEntity = (RechargeProductListEntity) getIntent().getExtras().getSerializable(Constants.rechargeProductListEntity);
            rechargeOperatorEntity = (RechargeOperatorEntity) getIntent().getExtras().getSerializable(Constants.rechargeOperatorEntity);
            rechargeInterOrderEntity = (RechargeInterOrderEntity) getIntent().getExtras().getSerializable(Constants.rechargeInterOrderEntity);
            rechargeOrderDetailEntity = (RechargeOrderDetailEntity) getIntent().getExtras().getSerializable(Constants.rechargeOrderDetailEntity);
            if(rechargeInterOrderEntity != null) {
                operatorImgUrl = rechargeInterOrderEntity.operatorImgUrl;
                operatorName = rechargeInterOrderEntity.operatorName;
                tvBillerName.setText(operatorName);
                Glide.with(mContext)
                        .load(operatorImgUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.mobile_topup))
                        .into(ivBiller);
            }

            if(rechargeOperatorEntity != null){
                operatorName = rechargeOperatorEntity.operatorName;
            }
        }

        initRecyclerView();
    }

    private void initRecyclerView() {
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_31));
        historyStatusView = new StatusView.Builder(mContext, ryAllProduct).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryAllProduct.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryAllProduct.setLayoutManager(manager);
        rechargeProductListAdapter = new RechargeProductListAdapter(changeProductList);
        rechargeProductListAdapter.setProductClickListener(new RechargeProductListAdapter.ProductClickListener() {
            @Override
            public void clickProduct(ChooseProductEntity chooseProductEntity) {
                HashMap<String,Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.rechargeChooseProductEntity,chooseProductEntity);
                if(rechargeOperatorEntity != null) {
                    rechargeOperatorEntity.operatorImgUrl = operatorImgUrl;
                }
                if(rechargeOrderDetailEntity != null) {
                    rechargeOrderDetailEntity.operatorImgUrl = operatorImgUrl;
                }
                mHashMap.put(Constants.rechargeOperatorEntity,rechargeOperatorEntity);
                mHashMap.put(Constants.rechargeOrderDetailEntity,rechargeOrderDetailEntity);
                ActivitySkipUtil.startAnotherActivity(RechargeProductListActivity.this, RechargeOrderDetailActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        rechargeProductListAdapter.bindToRecyclerView(ryAllProduct);
        if(rechargeProductListEntity != null){
            operatorImgUrl = rechargeProductListEntity.logo;
            operatorName = rechargeProductListEntity.operatorName;
            tvBillerName.setText(operatorName);
            Glide.with(mContext)
                    .load(operatorImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.mobile_topup))
                    .into(ivBiller);
            changeProductList = rechargeProductListEntity.changeProductList;
            inputProduct = rechargeProductListEntity.inputProduct;
//            inputProduct = rechargeProductListEntity.getProductEntity();
            rechargeProductListAdapter.setDataList(changeProductList);
            if(inputProduct != null){
                conCustom.setVisibility(View.VISIBLE);
                tvCustomName.setText(inputProduct.title);
            }else{
                conCustom.setVisibility(View.GONE);
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_get_help, R.id.con_custom})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_help:
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.con_custom:
                if(inputProduct != null) {
                    showCustomDialog();
                }
                break;
            default:break;
        }
    }

    private void showCustomDialog(){
        RechargeCustomDialog rechargeCustomDialog = new RechargeCustomDialog(mContext,inputProduct);
        rechargeCustomDialog.setPayMoneyListener(new RechargeCustomDialog.PayMoneyListener() {
            @Override
            public void payMoney(String destinationAmount,String money) {
                HashMap<String,Object> mHashMap = new HashMap<>();
                inputProduct.amount = money;
                inputProduct.destinationAmount = destinationAmount;
                inputProduct.operatorName = operatorName;
                inputProduct.operatorImgUrl = operatorImgUrl;
                mHashMap.put(Constants.rechargeInputProductEntity,inputProduct);
                mHashMap.put(Constants.rechargeOperatorEntity,rechargeOperatorEntity);
                mHashMap.put(Constants.rechargeOrderDetailEntity,rechargeOrderDetailEntity);
                ActivitySkipUtil.startAnotherActivity(RechargeProductListActivity.this, RechargeOrderDetailActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        rechargeCustomDialog.showWithBottomAnim();
    }

}
