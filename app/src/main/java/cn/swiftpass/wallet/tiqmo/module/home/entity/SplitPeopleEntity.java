package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SplitPeopleEntity extends BaseEntity {
    public String receiverName;
    public String receiverPhone;
    public String receiverIcon;
    public String receiverAmount;
    public String transferStatus;
    public String transferOrderNo;
    public String orderType;
    public String sex;
}
