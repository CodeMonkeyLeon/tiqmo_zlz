package cn.swiftpass.wallet.tiqmo.widget.adapter;

import android.graphics.Canvas;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private ItemTouchHelperListener mAdapter;
    private int type;

    public SimpleItemTouchHelperCallback(ItemTouchHelperListener adapter, int type) {
        mAdapter = adapter;
        this.type = type;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;        //允许上下的拖动
        int swipeFlags = ItemTouchHelper.START;   //只允许从右向左侧滑
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        //onItemMove是接口方法
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        //onItemDissmiss是接口方法
        mAdapter.onItemDissmiss(viewHolder.getAdapterPosition(), type);
    }

    //限制ImageView长度所能增加的最大值
    private double ICON_MAX_SIZE = 50;
    //ImageView的初始长宽
    private int fixedWidth = 150;

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        //重置改变，防止由于复用而导致的显示问题
        BaseViewHolder baseViewHolder = (BaseViewHolder) viewHolder;
        LinearLayout llContent = baseViewHolder.getView(R.id.ll_content);
        llContent.setTranslationX(0);
//        viewHolder.itemView.setScrollX(0);
//        ((MyAdapter.NormalItem)viewHolder).tv.setText("左滑删除");
//        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) ((MyAdapter.NormalItem) viewHolder).iv.getLayoutParams();
//        params.width = 150;
//        params.height = 150;
//        ((MyAdapter.NormalItem) viewHolder).iv.setLayoutParams(params);
//        ((MyAdapter.NormalItem) viewHolder).iv.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        BaseViewHolder baseViewHolder = (BaseViewHolder) viewHolder;
        LinearLayout llContent = baseViewHolder.getView(R.id.ll_content);
        //仅对侧滑状态下的效果做出改变
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            //如果dX小于等于删除方块的宽度，那么我们把该方块滑出来
//            if (Math.abs(dX) <= getSlideLimitation(viewHolder)){
//                viewHolder.itemView.scrollTo(-(int) dX,0);
//            }
//            viewHolder.itemView.scrollTo(-(int) dX,0);
            llContent.setTranslationX(dX);
//            if (Math.abs(dX) <= getSlideLimitation(viewHolder)){
//            }
//            //如果dX还未达到能删除的距离，此时慢慢增加“眼睛”的大小，增加的最大值为ICON_MAX_SIZE
//            else if (Math.abs(dX) <= recyclerView.getWidth() / 2){
//                double distance = (recyclerView.getWidth() / 2 -getSlideLimitation(viewHolder));
//                double factor = ICON_MAX_SIZE / distance;
//                double diff =  (Math.abs(dX) - getSlideLimitation(viewHolder)) * factor;
//                if (diff >= ICON_MAX_SIZE) {
//                    diff = ICON_MAX_SIZE;
//                }
////                ((MyAdapter.NormalItem)viewHolder).tv.setText("");   //把文字去掉
////                ((MyAdapter.NormalItem) viewHolder).iv.setVisibility(View.VISIBLE);  //显示眼睛
////                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) ((MyAdapter.NormalItem) viewHolder).iv.getLayoutParams();
////                params.width = (int) (fixedWidth + diff);
////                params.height = (int) (fixedWidth + diff);
////                ((MyAdapter.NormalItem) viewHolder).iv.setLayoutParams(params);
//            }
        } else {
            //拖拽状态下不做改变，需要调用父类的方法
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    }

    /**
     * 获取删除方块的宽度
     */
    public int getSlideLimitation(RecyclerView.ViewHolder viewHolder) {
        ViewGroup viewGroup = (ViewGroup) viewHolder.itemView;
        return viewGroup.getChildAt(1).getLayoutParams().width;
    }
}
