package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.CardServiceAdapter;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardServiceEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.OpenKsaCardPresenter;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class GetNewCardTwoActivity extends BaseCompatActivity<CardKsaContract.OpenCardPresenter> implements CardKsaContract.OpenCardView {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_card_image)
    ImageView ivCardImage;
    @BindView(R.id.rv_card_service)
    RecyclerView rvCardService;
    @BindView(R.id.tv_show_more)
    TextView tvShowMore;
    @BindView(R.id.iv_show_more_up)
    ImageView ivShowMoreUp;
    @BindView(R.id.iv_show_more_down)
    ImageView ivShowMoreDown;
    @BindView(R.id.ll_show_more)
    LinearLayout llShowMore;
    @BindView(R.id.iv_standard)
    ImageView ivStandard;
    @BindView(R.id.con_standard)
    ConstraintLayout conStandard;
    @BindView(R.id.iv_platinum)
    ImageView ivPlatinum;
    @BindView(R.id.tv_platinum_title)
    TextView tvPlatinumTitle;
    @BindView(R.id.tv_platinum_price)
    TextView tvPlatinumPrice;
    @BindView(R.id.con_platinum)
    ConstraintLayout conPlatinum;
    @BindView(R.id.tv_choose_card)
    TextView tvChooseCard;

    private CardServiceAdapter cardServiceAdapter;
    private List<CardServiceEntity> allIconList = new ArrayList<>();
    private List<CardServiceEntity> showIconList = new ArrayList<>();

    private boolean isShowMore;
    //Standard表示标准卡,Platinum表示白金卡
    private String cardProductType = Constants.card_type_Standard;

    private OpenCardReqEntity openCardReqEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_new_card_two;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.DigitalCard_0);
        setDarkBar();
        fixedSoftKeyboardSliding();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();

        allIconList.add(new CardServiceEntity(R.attr.icon_card_service_1, R.string.DigitalCard_11));
        allIconList.add(new CardServiceEntity(R.attr.icon_card_service_2, R.string.DigitalCard_12));
        allIconList.add(new CardServiceEntity(R.attr.icon_card_service_3, R.string.DigitalCard_13));
        allIconList.add(new CardServiceEntity(R.attr.icon_card_service_4, R.string.DigitalCard_14));
        allIconList.add(new CardServiceEntity(R.attr.icon_card_service_5, R.string.DigitalCard_15));
        allIconList.add(new CardServiceEntity(R.attr.icon_card_service_6, R.string.DigitalCard_16));
        showIconList.add(new CardServiceEntity(R.attr.icon_card_service_1, R.string.DigitalCard_11));
        showIconList.add(new CardServiceEntity(R.attr.icon_card_service_2, R.string.DigitalCard_12));
        showIconList.add(new CardServiceEntity(R.attr.icon_card_service_3, R.string.DigitalCard_13));
        GridLayoutManager manager = new GridLayoutManager(mContext, 3);
        MyItemDecoration myItemDecoration = MyItemDecoration.createHorizontal(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        MyItemDecoration myItemDecoration1 = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        myItemDecoration1.setGrid(true);
        rvCardService.addItemDecoration(myItemDecoration);
        rvCardService.addItemDecoration(myItemDecoration1);
        rvCardService.setLayoutManager(manager);
        cardServiceAdapter = new CardServiceAdapter(showIconList);
        cardServiceAdapter.bindToRecyclerView(rvCardService);
        cardServiceAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                showToast(getString(allIconList.get(position).textId));
                switch (position) { // case
                    default:
                        break;
                }
            }
        });

        //默认选中普通卡
        chooseCardType(cardProductType);

        mPresenter.getKsaCardSummary("Fee",null);
    }

    private void chooseCardType(String mCardType){
        if(Constants.card_type_Standard.equals(mCardType)){
            //普通卡
            conStandard.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_0f67b9_eef3f9_10));
            conPlatinum.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_1da0f0_white_10));
            ivCardImage.setImageResource(R.drawable.digital_stand_face);
            this.cardProductType = mCardType;
            cardServiceAdapter.setDataList(showIconList);
            llShowMore.setVisibility(View.GONE);
        }else if(Constants.card_type_Platinum.equals(mCardType)){
            //白金卡
            conStandard.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_1da0f0_white_10));
            conPlatinum.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_0f67b9_eef3f9_10));
            this.cardProductType = mCardType;
            ivCardImage.setImageResource(R.drawable.digital_green_face);
            llShowMore.setVisibility(View.VISIBLE);
            isShowMore = false;
            tvShowMore.setText(getString(R.string.DigitalCard_20));
            ivShowMoreUp.setVisibility(View.GONE);
            ivShowMoreDown.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.iv_back, R.id.ll_show_more, R.id.con_standard, R.id.con_platinum, R.id.tv_choose_card})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_show_more:
                if (isShowMore) {
                    isShowMore = false;
                    cardServiceAdapter.setDataList(showIconList);
                    tvShowMore.setText(getString(R.string.DigitalCard_20));
                    ivShowMoreUp.setVisibility(View.GONE);
                    ivShowMoreDown.setVisibility(View.VISIBLE);
                } else {
                    isShowMore = true;
                    cardServiceAdapter.setDataList(allIconList);
                    tvShowMore.setText(getString(R.string.DigitalCard_21));
                    ivShowMoreUp.setVisibility(View.VISIBLE);
                    ivShowMoreDown.setVisibility(View.GONE);
                }
                break;
            case R.id.con_standard:
                chooseCardType(Constants.card_type_Standard);
                break;
            case R.id.con_platinum:
                chooseCardType(Constants.card_type_Platinum);
                break;
            case R.id.tv_choose_card:
                openCardReqEntity.cardProductType = cardProductType;
                UserInfoManager.getInstance().setOpenCardReqEntity(openCardReqEntity);
                ActivitySkipUtil.startAnotherActivity(this, GetNewCardThreeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                mPresenter.openKsaCardAccount(cardProductType);
                break;
            default:break;
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void openKsaCardAccountSuccess(Void result) {
        ActivitySkipUtil.startAnotherActivity(this, GetNewCardThreeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getKsaCardSummarySuccess(KsaCardSummaryEntity ksaCardSummaryEntity) {
        if(ksaCardSummaryEntity != null){
            String platinumCardAmount = ksaCardSummaryEntity.platinumCardAmount;
            if(!TextUtils.isEmpty(platinumCardAmount)){
                tvPlatinumPrice.setVisibility(View.VISIBLE);
                tvPlatinumPrice.setText(AndroidUtils.getTransferMoney(platinumCardAmount)+LocaleUtils.getCurrencyCode(""));
            }else{
                tvPlatinumPrice.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public CardKsaContract.OpenCardPresenter getPresenter() {
        return new OpenKsaCardPresenter();
    }
}
