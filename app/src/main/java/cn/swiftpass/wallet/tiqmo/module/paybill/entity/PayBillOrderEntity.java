package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillOrderEntity extends BaseEntity {

    /**
     * {\"countryImgUrl\":\"http://wallet-saas-dev.wallyt.net/tiqmo/cms/file/download?imr/country/pk.png\",\"billerId\":\"586000000000015\",\"alaisName\":\"mobile prepaid\",\"countryCode\":\"PAK\",\"countryName\":\"Pakistan\",\"sku\":\"00000000000000000800\",\"billerName\":\"Warid Prepaid\"}
     */

    //别名
    public String alaisName;
    //商户名
    public String billerName;
    //商户Logo Url
    public String billerImgUrl;
    //商户ID
    public String billerId;
    //sku
    public String sku;
    public String countryCode;

    public String countryName;
    public String countryImgUrl;

    public int sceneType;

    public String selected = "0";

    //0表示私企; 1表示MOI
    public String billTypeFlag;

    //当前biller是否支持退款(当sadad的MOI的biller时候，必返回此值: 0表示不支持，1表示支持退款)
    public int supportRefundFlag;

    public boolean isMoiBill(){
        return "1".equals(billTypeFlag);
    }
}
