package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PayBillPaymentsAdapter extends BaseRecyclerAdapter<PayBillOrderEntity> {
    public PayBillPaymentsAdapter(@Nullable List<PayBillOrderEntity> data) {
        super(R.layout.item_paybill_payments, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillOrderEntity payBillOrderEntity, int position) {
        RoundedImageView ivPayment = baseViewHolder.getView(R.id.iv_payment);
        ImageView ivCountry = baseViewHolder.getView(R.id.iv_country);
        if (payBillOrderEntity != null) {
            String billerImgUrl = payBillOrderEntity.billerImgUrl;
            String countryImgUrl = payBillOrderEntity.countryImgUrl;
            Glide.with(mContext)
                    .load(countryImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivCountry);
            Glide.with(mContext)
                    .load(billerImgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext,R.attr.utilities))
                    .into(ivPayment);

            baseViewHolder.setText(R.id.tv_payment_name, payBillOrderEntity.alaisName);
            baseViewHolder.setText(R.id.tv_payment_desc, payBillOrderEntity.billerName);
            baseViewHolder.setText(R.id.tv_country_name, payBillOrderEntity.countryName);
            baseViewHolder.addOnClickListener(R.id.tv_pay);
        }
    }
}
