package cn.swiftpass.wallet.tiqmo.module.addmoney.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class BindCardPresenter implements AddMoneyContract.AddCardPresenter {

    AddMoneyContract.AddCardView mAddCardView;

    @Override
    public void bindCardPreOrder(String custName, String cardNo, String expire, String cvv, String type, String orderAmount, String orderCurrencyCode,
                                 String creditType) {
        AppClient.getInstance().getTransferManager().bindCardPreOrder(custName, cardNo, expire, cvv, type, orderAmount,
                orderCurrencyCode,creditType, new LifecycleMVPResultCallback<TransferPayEntity>(mAddCardView, true) {
                    @Override
                    protected void onSuccess(TransferPayEntity result) {
                        mAddCardView.bindCardPreOrderSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mAddCardView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void verifyCard(String cardNo) {
        AppClient.getInstance().getCardManager().verifyCard(cardNo, new LifecycleMVPResultCallback<verifyCardEntity>(mAddCardView,true) {
            @Override
            protected void onSuccess(verifyCardEntity result) {
                mAddCardView.verifyCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddCardView.verifyCardFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddMoneyContract.AddCardView addCardView) {
        this.mAddCardView = addCardView;
    }

    @Override
    public void detachView() {
        this.mAddCardView = null;
    }
}
