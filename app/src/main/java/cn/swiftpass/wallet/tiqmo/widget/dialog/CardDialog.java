package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

/**
 * Created by aijingya on 2020/7/14.
 *
 * @Package cn.swiftpass.wallet.tiqmo.widget.dialog
 * @Description:
 * @date 2020/7/14.13:46.
 */

public class CardDialog extends Dialog {
    private Context context;

    public CardDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    public static class Builder{
        /**
         * 都是内容数据
         */
//        private String title;
//        private int ImageRes;
//        private String message;

        TextView tv_dialog_title;
        ImageView iv_dialog_image;
        TextView tv_card_no;
        TextView tv_dialog_message;
        TextView tv_dialog_button;
        private CardDialog mDialog;

//        private View.OnClickListener mButtonClickListener;

        public Builder(Context context) {
         /*   mDialog = new CardDialog(context, R.style.DialogBottomIn);
             LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            View mLayout = inflater.inflate(R.layout.layout_card_dialog, null, false);
            //添加布局文件到 Dialog
            CardDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            tv_dialog_title = mLayout.findViewById(R.id.tv_dialog_title);
            iv_dialog_image = mLayout.findViewById(R.id.iv_dialog_image);
            tv_card_no = mLayout.findViewById(R.id.tv_card_no);
            tv_dialog_message = mLayout.findViewById(R.id.tv_dialog_message);
            tv_dialog_button = mLayout.findViewById(R.id.tv_dialog_button);*/
        }

    }




    /**
     * 如果用户提出需要点击dialog 后面的按钮点击事件不被满屏的dialog消费
     * 则初始化的构造函数里面调用下面的代码。但是需要自己维护关闭的事件
     **/
//    private void closeOutSideTouch(){
//        Window window = getWindow();
//        if (window != null) {
//            int flag = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
//            window.setFlags(flag, flag);
//        }
//    }

//    private void initStyle(int themeResId) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        if (getWindow()!=null){
//            getWindow().getAttributes().windowAnimations = themeResId;
//        }
//        setCanceledOnTouchOutside(true);
//        getWindow().setBackgroundDrawableResource(R.color.color_040f33);
////        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
////                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//        setCancelable(true);
//    }

    /**
     * 宽度全屏显示在底部
     */
    public void showMatchParentWidthAndGravityBottom() {
        if (context == null || getWindow() == null) {
            return;
        }
        getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = AndroidUtils.getScreenWidth(getContext());
        getWindow().setAttributes(lp);
        show();
    }

    /**
     * 宽度全屏显示在底部，从底部动画冒出
     */
    public void showWithBottomAnim() {
        getWindow().setWindowAnimations(R.style.popup_style);
        showMatchParentWidthAndGravityBottom();
    }
}
