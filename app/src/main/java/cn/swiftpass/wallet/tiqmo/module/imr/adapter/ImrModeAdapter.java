package cn.swiftpass.wallet.tiqmo.module.imr.adapter;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPaymentEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class ImrModeAdapter extends BaseRecyclerAdapter<ImrPaymentEntity> {

    private int choosePosition = -1;

    public void setChoosePosition(final int choosePosition) {
        this.choosePosition = choosePosition;
        notifyDataSetChanged();
    }

    public ImrModeAdapter(@Nullable List<ImrPaymentEntity> data) {
        super(R.layout.item_imr_mode,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, ImrPaymentEntity imrPaymentEntity, int position) {
        ImageView ivMode = baseViewHolder.getView(R.id.iv_mode);
        ImageView ivModeCheck = baseViewHolder.getView(R.id.iv_mode_check);
        LinearLayout llMode = baseViewHolder.getView(R.id.ll_mode);
        baseViewHolder.setText(R.id.tv_mode_name,imrPaymentEntity.paymentMethodName);
        String paymentMode = imrPaymentEntity.paymentMode;
        if("BT".equals(paymentMode)){
            baseViewHolder.setText(R.id.tv_mode_desc,mContext.getString(R.string.sprint19_44));
        }else if("CPA".equals(paymentMode)){
            baseViewHolder.setText(R.id.tv_mode_desc,mContext.getString(R.string.sprint19_45));
        }else{
            baseViewHolder.setText(R.id.tv_mode_desc,mContext.getString(R.string.sprint19_46));
        }

        if (choosePosition == position) {
            baseViewHolder.setGone(R.id.iv_mode_check,false);
        } else {
            baseViewHolder.setGone(R.id.iv_mode_check,true);
        }
        String paymentMethodLogo = imrPaymentEntity.paymentMethodLogo;
        if (!TextUtils.isEmpty(paymentMethodLogo)) {
            Glide.with(mContext).clear(ivMode);
            Glide.with(mContext)
                    .load(paymentMethodLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivMode);
        }else{
            Glide.with(mContext).clear(ivMode);
        }
    }
}
