package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ExtendEntity extends BaseEntity {

    public String extendKey;
    public String extendValue;
    public String extendSubValue;
    public String logoUrl;
    /**
     * 额外功能类型 C:可复制  TO_CASH_BACK_ORDER:可跳转到返现订单
     */
    public String extraFunctionType;

    public ExtendEntity() {
    }

    public ExtendEntity(final String extendKey, final String extendValue) {
        this.extendKey = extendKey;
        this.extendValue = extendValue;
    }
}
