package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class AppManageEntity extends BaseEntity {
    public static final int NO_UPDATE = 1;
    public static final int ADVICE_UPDATE = 2;
    public static final int FORCE_UPDATE = 3;
    /**
     * 标识位 1:不强制更新，2：建议更新，3：强制更新
     */
    public String flag;
    public String createTime;
    public String appName;
    public String appType;
    public String updateTime;
    public String version;
    public String installUrl;
}
