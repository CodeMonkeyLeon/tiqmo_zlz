package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;

public class CustomDialog extends Dialog {

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    private static TimeOverListener timeOverListener;

    public void setTimeOverListener(TimeOverListener timeOverListener) {
        this.timeOverListener = timeOverListener;
    }

    public interface TimeOverListener {
        void timeOver();
    }

    public static class Builder {
        private Context context;
        private String title;
        private String message;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;
        private DialogInterface.OnClickListener positiveButtonClickListener;
        private DialogInterface.OnClickListener negativeButtonClickListener;
        private int leftColor;
        private int rightColor;
        private boolean showTitle;
        private boolean isShowTime;
        private String countTime;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }
        public Builder setCountTime(String countTime) {
            this.countTime = countTime;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        public Builder setLeftColor(int color) {
            leftColor = color;
            return this;
        }

        public Builder setRightColor(int color) {
            rightColor = color;
            return this;
        }

        public Builder setShowTime(final boolean showTime) {
            this.isShowTime = showTime;
            return this;
        }

        /**
         * Set the Dialog title from resource
         *
         * @param title
         * @return
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        public Builder showTitle(boolean show) {
            this.showTitle = show;
            return this;
        }

        /**
         * Set the Dialog title from String
         *
         * @param title
         * @return
         */

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        //gravity
        int gravity;

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText, DialogInterface.OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, DialogInterface.OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText, DialogInterface.OnClickListener listener) {
            this.negativeButtonText = (String) context.getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText, DialogInterface.OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomDialog dialog = new CustomDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_normal_layout, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title

            TextView titleTV = layout.findViewById(R.id.title_dl);
            TextView messageTV = layout.findViewById(R.id.message_dl);

            Button posBtn = layout.findViewById(R.id.positive_dl);
            Button negBtn = layout.findViewById(R.id.negative_dl);

            if (!TextUtils.isEmpty(title)) {
                titleTV.setVisibility(View.VISIBLE);
                titleTV.setText(title);
            } else {


                titleTV.setVisibility(showTitle ? View.INVISIBLE : View.GONE);
            }
            // set the confirm button
            if (positiveButtonText != null) {
                posBtn.setText(positiveButtonText);
//                ((Button) layout.findViewById(R.id.negative_dl)).setTextColor(rightColor);
                if (positiveButtonClickListener != null) {
                    posBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                posBtn.setVisibility(View.GONE);
            }
            // set the cancel button
            if (negativeButtonText != null) {
                negBtn.setText(negativeButtonText);
//                ((Button) layout.findViewById(R.id.negative_dl)).setTextColor(leftColor);
                if (negativeButtonClickListener != null) {
                    negBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                negBtn.setVisibility(View.GONE);
                posBtn.setBackgroundResource(R.drawable.dialog_buttom_total_bg);
            }
            // set the content message
            if (message != null) {
                messageTV.setText(message.trim());
                if (0 != gravity) {
                    messageTV.setGravity(gravity);
                }
                if(isShowTime){
                    AdvancedCountdownTimer.getInstance().countDownEvent(!TextUtils.isEmpty(countTime)?Integer.parseInt(countTime):60, new AdvancedCountdownTimer.OnCountDownListener() {
                        @Override
                        public void onTick(int millisUntilFinished) {
                            if(millisUntilFinished == 0){
                                if(timeOverListener != null){
                                    timeOverListener.timeOver();
                                }
                            }
                            messageTV.setText(message.replace("XXX",millisUntilFinished+""));
                        }

                        @Override
                        public void onFinish() {
                            if(timeOverListener != null){
                                timeOverListener.timeOver();
                            }
                        }
                    });
                }
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }
}

