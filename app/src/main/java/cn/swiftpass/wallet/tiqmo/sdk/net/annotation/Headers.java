package cn.swiftpass.wallet.tiqmo.sdk.net.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by YZX on 2018年12月17日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */

//方法注解，声明http的header
@Documented
@Target(METHOD)
@Retention(RUNTIME)
public @interface Headers {
    String[] value();
}