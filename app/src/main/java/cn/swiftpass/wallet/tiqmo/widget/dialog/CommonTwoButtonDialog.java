package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class CommonTwoButtonDialog extends BottomDialog{

    private Context mContext;

    private onTwoButtonListener twoButtonListener;

    private TextView tvTitle,tvContent;
    private ImageView ivContent;
    private TextView tvConfirm,tvCancel;

    public void setTwoButtonListener(String confirmMsg, onTwoButtonListener onTwoButtonListener) {
        this.twoButtonListener = onTwoButtonListener;
        if(tvConfirm != null) {
            tvConfirm.setText(confirmMsg);
        }
    }

    public CommonTwoButtonDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public void setViewContent(String title,String content,int imageId){
        if(tvTitle != null && !TextUtils.isEmpty(title)){
            tvTitle.setText(title);
        }

        if(tvContent != null && !TextUtils.isEmpty(content)){
            tvContent.setText(content);
        }

        if(ivContent != null && imageId != -1){
            ivContent.setImageResource(imageId);
        }
    }

    public interface onTwoButtonListener {
        void clickConfirm();
        void clickCancel();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_two_button_common, null);
        tvTitle = view.findViewById(R.id.tv_title);
        tvContent = view.findViewById(R.id.tv_content);
        ivContent = view.findViewById(R.id.iv_content);
        tvConfirm = view.findViewById(R.id.tv_confirm);
        tvCancel = view.findViewById(R.id.tv_cancel);

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (twoButtonListener != null) {
                    twoButtonListener.clickConfirm();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
