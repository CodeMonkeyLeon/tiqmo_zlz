package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.ImrSendMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPurposeEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPurposeListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrSendMoneyPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.TransferRelationAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.NoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class ImrSendMoneyActivity extends BaseCompatActivity<ImrSendMoneyContract.Presenter> implements ImrSendMoneyContract.View {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.ll_error)
    LinearLayout llError;
    @BindView(R.id.iv_from_avatar)
    RoundedImageView ivFromAvatar;
    @BindView(R.id.iv_send)
    LottieAnimationView ivSend;
    @BindView(R.id.iv_to_avatar)
    RoundedImageView ivToAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.et_amount)
    EditTextWithDel etAmount;
    @BindView(R.id.tv_money_currency)
    TextView tvMoneyCurrency;
    @BindView(R.id.tv_rate_desc)
    TextView tvRateDesc;
    @BindView(R.id.et_change_amount)
    EditTextWithDel etChangeAmount;
    @BindView(R.id.tv_change_money_currency)
    TextView tvMoneyChangeCurrency;
    @BindView(R.id.cb_transfer_fee)
    CheckBox cbTransferFee;
    @BindView(R.id.rl_purpose)
    RecyclerView rlPurpose;
    @BindView(R.id.tv_agree)
    TextView tvAgree;
    @BindView(R.id.tv_error_edit)
    TextView tvErrorEdit;
    @BindView(R.id.tv_discount_title)
    TextView tvDiscountTitle;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;
    @BindView(R.id.tv_discount)
    TextView tvDiscount;
    @BindView(R.id.con_discount)
    ConstraintLayout conDiscount;
    @BindView(R.id.tv_send)
    TextView tvSend;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.et_purpose)
    EditTextWithDel etPurpose;
    @BindView(R.id.con_purpose)
    ConstraintLayout conPurpose;
    @BindView(R.id.ll_purpose)
    LinearLayout llPurpose;
    @BindView(R.id.tv_fee_title)
    TextView tvFeeTitle;
    @BindView(R.id.tv_fee_money)
    TextView tvFeeMoney;
    @BindView(R.id.tv_fee)
    TextView tvFee;
    @BindView(R.id.ll_fee)
    LinearLayout llFee;
    @BindView(R.id.con_switch_fee)
    ConstraintLayout conSwitchFee;

    private String monthLimitMoney,valiableMoney;

    private String transferPurpose, transferPurposeDesc;

    private TransferRelationAdapter transferRelationAdapter;
    private LinearLayoutManager mLayoutManager;
    public List<KycContactEntity> contactList = new ArrayList<>();

    private NoticeDialog noticeDialog;

    private ImrBeneficiaryListEntity.ImrBeneficiaryBean imrBeneficiaryBean;
    private ImrExchangeRateEntity imrExchangeRateEntity;
    private TransferLimitEntity transferLimitEntity;
    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    //Y-可切换是否包含，N-不可切换
    private String chargesSwitchFlag;

    private String payeeFullName, payeeNickName, sex, payeeInfoId, receiptMethod;

//    private String vatMoney;
    private String transferFees;

    private String originatingCurrencyCode, destinationCurrencyCode, exchangeRate, orginatingAmount, destinationAmount
            ,minDestinationAmount = "0.01",maxDestinationAmount,
            minOrginatingAmount,destinationAmountDecimalPoints;
//    private String maxOrginatingAmount;
    //是否包含手续费 1:包含，2：不包含
    private String includeFee = "2";

    private double rate;

    private String orderAmount;
    private String payerAmount;

    private boolean isOriginMoneyChange, isDestionMoneyChange;

    private String transferDestinationCountryCode;

    private ImrOrderInfoEntity mImrOrderInfoEntity;

    private String purposeId, purposeDesc;

    private String channelCode;

    private String requestSourceAmount,requestDestinationAmount;

    private boolean secondGetExchange;
    /**
     * 输入框显示向上取整位数
     */
    private int roundUpLength = 2;

    private boolean isFirstInit = true;
    private BottomDialog bottomDialog;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_imr_send_money;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                setBanlance(userInfoEntity);
                if(isChannelThunes(channelCode)){
                    checkMoneyW2W();
                }else{
                    checkMoney();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        try{
            setDarkBar();
            LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle, ivSend);
            tvTitle.setText(R.string.IMR_22);
            etPurpose.getTlEdit().setHint(getString(R.string.IMR_new_10));
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                checkUser(userInfoEntity);
            }
            etChangeAmount.setLineVisible(false);
            etPurpose.setFocusableFalse();
            if(isChannelTF()){
                conPurpose.setVisibility(View.VISIBLE);
                llPurpose.setVisibility(View.GONE);
            }else{
                conPurpose.setVisibility(View.GONE);
                llPurpose.setVisibility(View.VISIBLE);
            }
            if (getIntent().getExtras() != null) {
                imrBeneficiaryBean = (ImrBeneficiaryListEntity.ImrBeneficiaryBean) getIntent().getExtras().getSerializable(Constants.ImrBeneficiaryBean);
                imrExchangeRateEntity = (ImrExchangeRateEntity) getIntent().getExtras().getSerializable(Constants.imrExchangeRateEntity);
                transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
                imrBeneficiaryEntity = (ImrBeneficiaryEntity) getIntent().getExtras().getSerializable(Constants.ImrBeneficiaryEntity);
                if (imrBeneficiaryBean != null) {
                    payeeFullName = imrBeneficiaryBean.payeeFullName;
                    sex = imrBeneficiaryBean.sex;
                    payeeInfoId = imrBeneficiaryBean.payeeInfoId;
                    receiptMethod = imrBeneficiaryBean.receiptMethod;
                    channelCode = imrBeneficiaryBean.channelCode;
                    transferDestinationCountryCode = imrBeneficiaryBean.transferDestinationCountryCode;
                    tvName.setText(payeeFullName);

                    Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, sex)).into(ivToAvatar);
                } else if (imrBeneficiaryEntity != null) {
                    payeeFullName = imrBeneficiaryEntity.payeeFullName;
                    payeeNickName = imrBeneficiaryEntity.nickName;
                    sex = imrBeneficiaryEntity.sex;
                    channelCode = imrBeneficiaryEntity.channelCode;
                    payeeInfoId = imrBeneficiaryEntity.payeeInfoId;
                    receiptMethod = imrBeneficiaryEntity.receiptMethod;
                    transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;

                    if (!TextUtils.isEmpty(payeeNickName)) {
                        tvName.setText(payeeNickName);
                    } else {
                        tvName.setText(payeeFullName);
                    }
                    Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, sex)).into(ivToAvatar);
                }

                if (imrExchangeRateEntity != null) {
                    setImrExchangeRate(imrExchangeRateEntity);
                }

                etPurpose.getEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPresenter.getImrPurposeList(transferDestinationCountryCode,channelCode);
                    }
                });

                if (transferLimitEntity != null) {
                    String monthLimitAmt = transferLimitEntity.monthLimitAmt;
                    try {
                        monthLimitMoney = AndroidUtils.getTransferMoney(monthLimitAmt);
                    } catch (Exception e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                    }
                }
            }
            mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            rlPurpose.setLayoutManager(mLayoutManager);
            contactList.add(new KycContactEntity("18", getString(R.string.IMR_sendMoney_7), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_gift_purpose)));
            contactList.add(new KycContactEntity("21", getString(R.string.IMR_sendMoney_8), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_installments_purpose)));
            contactList.add(new KycContactEntity("16", getString(R.string.IMR_sendMoney_9), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_family_purpose)));
            contactList.add(new KycContactEntity("24", getString(R.string.IMR_sendMoney_10), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_living_expenses_purpose)));
            contactList.add(new KycContactEntity("25", getString(R.string.IMR_sendMoney_11), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_medical_purpose)));
            contactList.add(new KycContactEntity("27", getString(R.string.IMR_sendMoney_12), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_savings_purpose)));
            contactList.add(new KycContactEntity("33", getString(R.string.IMR_sendMoney_13), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_others_purpose_imr)));
            transferRelationAdapter = new TransferRelationAdapter(contactList);
            transferRelationAdapter.bindToRecyclerView(rlPurpose);

            transferRelationAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
                @Override
                public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                    switch (view.getId()) {
                        case R.id.con_choose:
                            transferRelationAdapter.setPosition(position);
                            KycContactEntity kycContactEntity = contactList.get(position);
                            transferPurpose = kycContactEntity.purposeCode;
                            transferPurposeDesc = kycContactEntity.getContactsName();
                            if (isChannelTF() || isChannelThunes(channelCode)) {
                                purposeId = transferPurpose;
                                purposeDesc = transferPurposeDesc;
                            }
                            checkMoney();
                            break;
                        default:
                            break;
                    }
                }
            });
            noticeDialog = NoticeDialog.getInstance(mContext);
            tvAgree.setText(Spans.builder()
                    .text(mContext.getString(R.string.IMR_sendMoney_4)).color(mContext.getColor(R.color.color_676a72)).size(12)
                    .text(" " + mContext.getString(R.string.IMR_sendMoney_5)).color(mContext.getColor(R.color.color_fc4f00)).size(12).click(tvAgree, new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View widget) {
                            HashMap<String,Object> hashMap = new HashMap<>();
                            hashMap.put(Constants.terms_condition_type,"1");
                            ActivitySkipUtil.startAnotherActivity(ImrSendMoneyActivity.this, WebViewActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                            noticeDialog.setTitle(mContext.getString(R.string.IMR_sendMoney_5));
//                            noticeDialog.setContent(mContext.getString(R.string.IMR_sendMoney_5));
//                            noticeDialog.showWithBottomAnim();
                        }

                        @Override
                        public void updateDrawState(@NonNull TextPaint ds) {
                            ds.setColor(mContext.getColor(R.color.color_fc4f00));
                            ds.setUnderlineText(false);
                        }
                    }).build());

            cbTransferFee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        includeFee = "1";
                    } else {
                        includeFee = "2";
                        tvErrorEdit.setVisibility(View.GONE);
                    }
                    checkMoney();
                }
            });
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void setImrExchangeRate(ImrExchangeRateEntity imrExchangeRateEntity){
        try {
            this.imrExchangeRateEntity = imrExchangeRateEntity;
            chargesSwitchFlag = imrExchangeRateEntity.chargesSwitchFlag;
//            if("Y".equals(chargesSwitchFlag)){
//                conSwitchFee.setVisibility(View.GONE);
//                llFee.setVisibility(View.GONE);
//            }else{
//                conSwitchFee.setVisibility(View.GONE);
//                llFee.setVisibility(View.GONE);
//                includeFee = "2";
//            }
            includeFee = "2";
            originatingCurrencyCode = imrExchangeRateEntity.originatingCurrencyCode;
            destinationCurrencyCode = imrExchangeRateEntity.destinationCurrencyCode;
            exchangeRate = imrExchangeRateEntity.exchangeRate;
            destinationAmountDecimalPoints = imrExchangeRateEntity.destinationAmountDecimalPoints;
            if("0".equals(destinationAmountDecimalPoints)){
                roundUpLength = 0;
                etAmount.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(8), new NormalInputFilter(NormalInputFilter.NUMBER)});
                etChangeAmount.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(8), new NormalInputFilter(NormalInputFilter.NUMBER)});
            }else{
                //两位小数过滤
                roundUpLength = 2;
                etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
                etChangeAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(18)});
            }

            orginatingAmount = BigDecimalFormatUtils.forMatWithDigs(imrExchangeRateEntity.orginatingAmount,roundUpLength);
            destinationAmount = BigDecimalFormatUtils.forMatWithDigs(imrExchangeRateEntity.destinationAmount,roundUpLength);
            if(!TextUtils.isEmpty(imrExchangeRateEntity.minDestinationAmount)) {
                minDestinationAmount = BigDecimalFormatUtils.forMatWithDigs(imrExchangeRateEntity.minDestinationAmount, roundUpLength);
                minOrginatingAmount = BigDecimalFormatUtils.div(imrExchangeRateEntity.minDestinationAmount,exchangeRate,roundUpLength);
            }
            if(!TextUtils.isEmpty(imrExchangeRateEntity.maxDestinationAmount)) {
                maxDestinationAmount = BigDecimalFormatUtils.forMatWithDigs(imrExchangeRateEntity.maxDestinationAmount, roundUpLength);
            }
            transferFees = imrExchangeRateEntity.charges;
//            vatMoney = imrExchangeRateEntity.vatCharges;
            double vat = 0d;
            double fee = 0d;

//            if(!TextUtils.isEmpty(vatMoney)) {
//                vat = Double.parseDouble(vatMoney);
//            }
//            if(!TextUtils.isEmpty(transferFees)) {
//                fee = Double.parseDouble(transferFees);
//            }
            String fees = BigDecimalFormatUtils.forMatWithDigsDown(String.valueOf(vat+fee), 2);
            tvFeeMoney.setText(Spans.builder()
                    .text(fees+" ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(10)
                    .build());
            tvFee.setText(Spans.builder()
                    .text(fees+" ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(10)
                    .build());
            exchangeRate = exchangeRate.replace(",", "");
            rate = Double.parseDouble(exchangeRate);

            if (!TextUtils.isEmpty(originatingCurrencyCode)) {
                tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(originatingCurrencyCode));
            }

            if (!TextUtils.isEmpty(destinationCurrencyCode)) {
                tvMoneyChangeCurrency.setText(destinationCurrencyCode);
            }
            if(isChannelThunes(channelCode)){
                String formatOrginatingAmount = "1";
                payerAmount = BigDecimalFormatUtils.add(orginatingAmount,"0",2);
                if (!TextUtils.isEmpty(orginatingAmount) && BigDecimalFormatUtils.compareBig(orginatingAmount,"0") && !isFirstInit) {
                    etAmount.getEditText().setText(orginatingAmount);
                    tvSend.setText(getString(R.string.IMR_sendMoney_6) + " " + payerAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
                }
//
                String showDestinationAmount = exchangeMoneyW2W("1", true, exchangeRate,4);
                if(!TextUtils.isEmpty(destinationAmount) && BigDecimalFormatUtils.compareBig(destinationAmount,"0") && !isFirstInit) {
                    etChangeAmount.getEditText().setText(destinationAmount);
                }
                if (rate < 1) {
                    try {
                        String destinaMoney = exchangeMoney("1", false, exchangeRate,4);

                        tvRateDesc.setText(formatOrginatingAmount + " " + destinationCurrencyCode + " ≈ " + destinaMoney + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
                    } catch (Exception e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                        tvRateDesc.setText(formatOrginatingAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode) + " ≈ " + showDestinationAmount + " " + destinationCurrencyCode);
                    }
                } else {
                    tvRateDesc.setText(formatOrginatingAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode) + " ≈ " + showDestinationAmount + " " + destinationCurrencyCode);
                }
            }else {
                String formatOrginatingAmount = "1";
                String formatDestinationAmount = destinationAmount;
                if (!TextUtils.isEmpty(orginatingAmount) && BigDecimalFormatUtils.compareBig(orginatingAmount,"0") && !isFirstInit) {
                    etAmount.getEditText().setText(orginatingAmount);
                    tvSend.setText(getString(R.string.IMR_sendMoney_6) + " " + formatOrginatingAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
                }

                String showDestinationAmount = exchangeMoney("1", true, exchangeRate,4);
                if(!TextUtils.isEmpty(destinationAmount) && BigDecimalFormatUtils.compareBig(destinationAmount,"0") && !isFirstInit) {
                    etChangeAmount.getEditText().setText(destinationAmount);
                }

                if (rate < 1) {
                    try {
                        String destinaMoney = exchangeMoney("1", false, exchangeRate,4);

                        tvRateDesc.setText(formatOrginatingAmount + " " + destinationCurrencyCode + " ≈ " + destinaMoney + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
                    } catch (Exception e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                        tvRateDesc.setText(formatOrginatingAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode) + " ≈ " + showDestinationAmount + " " + destinationCurrencyCode);
                    }
                } else {
                    tvRateDesc.setText(formatOrginatingAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode) + " ≈ " + showDestinationAmount + " " + destinationCurrencyCode);
                }
            }

            isFirstInit = false;

            etAmount.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String amount = s.toString();
                    amount = amount.replace(",", "");
                    if (TextUtils.isEmpty(amount)) {
                        tvSend.setEnabled(false);
                    }
                    if(isChannelThunes(channelCode))return;
                    if (TextUtils.isEmpty(amount)) {
                        isOriginMoneyChange = true;
                        etChangeAmount.getEditText().setText("");
                        isOriginMoneyChange = false;
                        tvSend.setEnabled(false);
                    } else {
                        try {
                            orderAmount = amount;
                            checkMoney();
                            if (isDestionMoneyChange) {
                                isDestionMoneyChange = false;
                            } else {
                                isOriginMoneyChange = true;
                                String changeAmount = exchangeMoney(amount, true, exchangeRate,roundUpLength);
                                etChangeAmount.getEditText().setText(changeAmount);
                                isOriginMoneyChange = false;
                            }
                            orginatingAmount = AmountUtil.dataFormat(amount);
                            tvSend.setText(getString(R.string.IMR_sendMoney_6) + " " + AmountUtil.dataFormat(amount) + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
                        } catch (Exception e) {
                            tvSend.setEnabled(false);
                        }
                    }
                }
            });

            etChangeAmount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String amount = s.toString();
                    amount = amount.replace(",", "");
                    if (TextUtils.isEmpty(amount)) {
                        tvSend.setEnabled(false);
                    }
                    if(isChannelThunes(channelCode))return;
                    if (isOriginMoneyChange) {
                        isOriginMoneyChange = false;
                    } else {
                        if (TextUtils.isEmpty(amount)) {
                            tvSend.setEnabled(false);
                            setLeftEditMoney("");
                        } else {
                            try {
                                double money = Double.parseDouble(amount);
                                String originAmount = exchangeMoney(amount, false, exchangeRate,roundUpLength);
                                setLeftEditMoney(originAmount);
                                checkMoney();
                            } catch (Exception e) {
                                tvSend.setEnabled(false);
                            }
                        }
                    }
                }
            });

            etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
                @Override
                public void onFocusChange(boolean hasFocus) {
                    requestSourceAmount = etAmount.getEditText().getText().toString().trim();
                    requestSourceAmount = requestSourceAmount.replace(",", "");
                    String fomatAmount = AmountUtil.dataFormat(requestSourceAmount);

                    String changeAmount = etChangeAmount.getEditText().getText().toString().trim();
                    changeAmount = changeAmount.replace(",", "");
                    String fomatChangeAmount = AmountUtil.dataFormat(changeAmount);
                    if (!hasFocus) {
                        if(TextUtils.isEmpty(requestSourceAmount))return;
                        if(isChannelThunes(channelCode)){
                            if(compareMoneyW2W(false)) {
                                mPresenter.imrExchangeRate(payeeInfoId, requestSourceAmount, "", channelCode);
                            }
                        }else {
                            etChangeAmount.getEditText().setText(fomatChangeAmount);
                            setLeftEditMoney(fomatAmount);
                            checkMoney();
                        }
                    } else {
                        // 获取焦点时清空输入框
//                        etChangeAmount.clearFocus();
                        etChangeAmount.getEditText().setText("");
                        setLeftEditMoney("");
                        tvErrorEdit.setVisibility(View.GONE);
                        AndroidUtils.showKeyboard((Activity) mContext,etAmount.getEditText());
                        AndroidUtils.showKeyboardDelay(etAmount.getEditText());
                        AndroidUtils.showKeyboardView(etAmount.getEditText());
                    }
                }
            });
            etChangeAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
                @Override
                public void onFocusChange(boolean hasFocus) {
                    String amount = etAmount.getEditText().getText().toString().trim();
                    amount = amount.replace(",", "");
                    String fomatAmount = AmountUtil.dataFormat(amount);
                    requestDestinationAmount = etChangeAmount.getEditText().getText().toString().trim();
                    requestDestinationAmount = requestDestinationAmount.replace(",", "");
                    String fomatChangeAmount = AmountUtil.dataFormat(requestDestinationAmount);
                    if (!hasFocus) {
                        if(TextUtils.isEmpty(requestDestinationAmount))return;
                        if(isChannelThunes(channelCode)){
                            if(compareMoneyW2W(true)) {
                                mPresenter.imrExchangeRate(payeeInfoId, "", requestDestinationAmount, channelCode);
                            }
                        }else {
                            setRightEditMoney(fomatChangeAmount);
                            setLeftEditMoney(fomatAmount);
                            checkMoney();
                        }
                    } else {
                        // 获取焦点时清空输入框
//                        etAmount.clearFocus();
                        setRightEditMoney("");
                        setLeftEditMoney("");
                        tvErrorEdit.setVisibility(View.GONE);
                        AndroidUtils.showKeyboard((Activity) mContext,etChangeAmount.getEditText());
                        AndroidUtils.showKeyboardDelay(etChangeAmount.getEditText());
                        AndroidUtils.showKeyboardView(etChangeAmount.getEditText());
                    }
                }
            });
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void setChangeListener(){

    }

    private void setLeftEditMoney(String amount) {
        isDestionMoneyChange = true;
        etAmount.getEditText().setText(amount);
        isDestionMoneyChange = false;
    }

    private void setRightEditMoney(String amount) {
        isOriginMoneyChange = true;
        etChangeAmount.getEditText().setText(amount);
        isOriginMoneyChange = false;
    }

    private String exchangeMoneyW2W(String money,boolean isOriginMoney, String exchangeRate,int roundUpLength){
        try {
            String resultMoney;
//            if (isOriginMoney) {
                resultMoney = BigDecimalFormatUtils.mul(money,exchangeRate,roundUpLength, BigDecimal.ROUND_DOWN);
//            } else {
//                resultMoney = BigDecimalFormatUtils.div(money,exchangeRate,roundUpLength,BigDecimal.ROUND_UP);
//            }

            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "";
    }

    private String exchangeMoney(String money, boolean isOriginMoney, String exchangeRate,int roundUpLength) {
        try {
            String resultMoney;
            if (isOriginMoney) {
                resultMoney = BigDecimalFormatUtils.mul(money,exchangeRate,roundUpLength,BigDecimal.ROUND_DOWN);
            } else {
                resultMoney = BigDecimalFormatUtils.div(money,exchangeRate,roundUpLength);
            }

            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
//        try {
//            if (etAmount != null) {
//                if(isChannelThunes(channelCode)){
//                    checkMoneyW2W();
//                }else {
//                    checkMoney();
//                }
//            }
//        }catch (Throwable e){
//            LogUtils.d("errorMsg", "---"+e+"---");
//        }
    }

    private void setBanlance(UserInfoEntity userInfoEntity) {
        String balance = userInfoEntity.getBalance();
        String balanceNumber = userInfoEntity.getBalanceNumber();
        if (!TextUtils.isEmpty(balanceNumber)) {
            try {
                balanceNumber = balanceNumber.replace(",", "");
                valiableMoney = balanceNumber;
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        tvBalance.setText(balance);
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.avatar;
            String gender = userInfoEntity.gender;
            setBanlance(userInfoEntity);
            tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
            if (!TextUtils.isEmpty(avatar)) {
                Glide.with(this).clear(ivFromAvatar);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                        .into(ivFromAvatar);
            } else {
                Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivFromAvatar);
            }
        }
    }

    private boolean compareMoneyW2W(boolean isDestination){
        //判断是否输入目标币种后失焦
        requestSourceAmount = etAmount.getEditText().getText().toString().trim();
        requestDestinationAmount = etChangeAmount.getEditText().getText().toString().trim();
        if (!TextUtils.isEmpty(requestSourceAmount)) {
            requestSourceAmount = requestSourceAmount.replace(",", "");
            orderAmount = requestSourceAmount;
        }
        if (!TextUtils.isEmpty(requestDestinationAmount)) {
            requestDestinationAmount = requestDestinationAmount.replace(",", "");
        }
        if(!isDestination){
            requestDestinationAmount = exchangeMoneyW2W(requestSourceAmount,true,exchangeRate,roundUpLength);
        }
        if (TextUtils.isEmpty(requestDestinationAmount)) {
            tvErrorEdit.setVisibility(View.VISIBLE);
            tvErrorEdit.setText(getString(R.string.IMR_new_13).replace("XXX", minDestinationAmount + " " + destinationCurrencyCode));
            tvSend.setEnabled(false);
            etChangeAmount.setContentText(AmountUtil.dataFormat(requestDestinationAmount));
            return false;
        }

        //最小值必须要大于0才判断
        if (minDestinationAmount != null && BigDecimalFormatUtils.compareBig(minDestinationAmount,"0") && !BigDecimalFormatUtils.compare(requestDestinationAmount, minDestinationAmount)) {
            tvErrorEdit.setVisibility(View.VISIBLE);
            tvErrorEdit.setText(getString(R.string.IMR_new_13).replace("XXX", minDestinationAmount + " " + destinationCurrencyCode));
            tvSend.setEnabled(false);
            etChangeAmount.setContentText(AmountUtil.dataFormat(requestDestinationAmount));
            return false;
        }

        //最大值必须要大于0才判断
        if (maxDestinationAmount != null && BigDecimalFormatUtils.compareBig(maxDestinationAmount,"0") && !BigDecimalFormatUtils.compare(maxDestinationAmount, requestDestinationAmount)) {
            tvErrorEdit.setVisibility(View.VISIBLE);
            tvErrorEdit.setText(getString(R.string.IMR_new_16).replace("XXX", maxDestinationAmount + " " + destinationCurrencyCode));
            tvSend.setEnabled(false);
            etChangeAmount.setContentText(AmountUtil.dataFormat(requestDestinationAmount));
            return false;
        }

        return true;
    }

    private void checkMoneyW2W(){
        try {
            String amount = etAmount.getEditText().getText().toString().trim();
            requestDestinationAmount = etChangeAmount.getEditText().getText().toString().trim();
            if (!TextUtils.isEmpty(amount)) {
                amount = amount.replace(",", "");
                orderAmount = amount;
            }
            if (!TextUtils.isEmpty(requestDestinationAmount)) {
                requestDestinationAmount = requestDestinationAmount.replace(",", "");
            }
            tvAddMoney.setEnabled(false);
            tvErrorEdit.setVisibility(View.GONE);
            llError.setVisibility(View.GONE);

            if (TextUtils.isEmpty(requestDestinationAmount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.IMR_new_13).replace("XXX", minDestinationAmount + " " + destinationCurrencyCode));
                tvSend.setEnabled(false);
                return;
            }

            //最小值必须要大于0才判断
            if (minDestinationAmount != null && BigDecimalFormatUtils.compareBig(minDestinationAmount,"0") && !BigDecimalFormatUtils.compare(requestDestinationAmount, minDestinationAmount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.IMR_new_13).replace("XXX", minDestinationAmount + " " + destinationCurrencyCode));
                tvSend.setEnabled(false);
                return;
            }

            //最大值必须要大于0才判断
            if (maxDestinationAmount != null && BigDecimalFormatUtils.compareBig(maxDestinationAmount,"0") && !BigDecimalFormatUtils.compare(maxDestinationAmount, requestDestinationAmount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.IMR_new_16).replace("XXX", maxDestinationAmount + " " + destinationCurrencyCode));
                tvSend.setEnabled(false);
                return;
            }

            if (!BigDecimalFormatUtils.compare(monthLimitMoney,payerAmount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.wtw_25_3));
                tvSend.setEnabled(false);
                return;
            } else if (BigDecimalFormatUtils.compare(valiableMoney,payerAmount)) {
                checkContinueStatus(orderAmount);
            } else {
                tvAddMoney.setEnabled(true);
                llError.setVisibility(View.VISIBLE);
                tvSend.setEnabled(false);
                return;
            }

            if (Double.parseDouble(valiableMoney) == 0) {
                tvAddMoney.setEnabled(true);
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void checkMoney() {
        try {
            String amount = etAmount.getEditText().getText().toString().trim();
            if (!TextUtils.isEmpty(amount)) {
                amount = amount.replace(",", "");
            } else {
                amount = "0.00";
            }
            payerAmount = BigDecimalFormatUtils.add(amount, "0", 2);
            if ("1".equals(includeFee)) {
                tvSend.setText(getString(R.string.IMR_sendMoney_6) + " " + AmountUtil.dataFormat(amount) + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
            } else {
                tvSend.setText(getString(R.string.IMR_sendMoney_6) + " " + payerAmount + " " + LocaleUtils.getCurrencyCode(originatingCurrencyCode));
            }
            tvAddMoney.setEnabled(false);
            tvErrorEdit.setVisibility(View.GONE);
            llError.setVisibility(View.GONE);

            if (TextUtils.isEmpty(amount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.IMR_notice_8).replace("XXX", LocaleUtils.getCurrencyCode("")));
                return;
            }

            orderAmount = amount;

            if (BigDecimalFormatUtils.compareBig("1",orderAmount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.IMR_notice_8).replace("XXX", LocaleUtils.getCurrencyCode("")));
                tvSend.setEnabled(false);
                return;
            }

            if (!BigDecimalFormatUtils.compare(monthLimitMoney,amount)) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.wtw_25_3));
                tvSend.setEnabled(false);
                return;
            } else if (BigDecimalFormatUtils.compare(valiableMoney,amount)) {
                checkContinueStatus(orderAmount);
            } else {
                tvAddMoney.setEnabled(true);
                llError.setVisibility(View.VISIBLE);
                tvSend.setEnabled(false);
                return;
            }

            if (Double.parseDouble(valiableMoney) == 0) {
                tvAddMoney.setEnabled(true);
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void checkContinueStatus(String orderAmount) {
//        vatMoney = imrExchangeRateEntity.vatCharges;
//        transferFees = imrExchangeRateEntity.charges;
//        double vat = 0d;
//        if(!TextUtils.isEmpty(vatMoney)) {
//            vatMoney = vatMoney.replace(",", "");
//            vat = Double.parseDouble(vatMoney);
//        }
//        transferFees = transferFees.replace(",", "");
//        double fee = Double.parseDouble(transferFees);
        if (TextUtils.isEmpty(transferPurpose)) {
            tvSend.setEnabled(false);
            if ("1".equals(includeFee)) {
//                if (orderMoney <= (vat + fee)) {
//                    tvErrorEdit.setText(getString(R.string.IMR_notice_7));
//                    tvErrorEdit.setVisibility(View.VISIBLE);
//                } else {
//                    tvErrorEdit.setVisibility(View.GONE);
//                }
            } else {
                if (BigDecimalFormatUtils.compareBig(orderAmount,valiableMoney)) {
                    tvAddMoney.setEnabled(true);
                    llError.setVisibility(View.VISIBLE);
                } else {
                    tvAddMoney.setEnabled(false);
                    llError.setVisibility(View.GONE);
                }
            }
        } else {
            if ("1".equals(includeFee)) {
//                if (orderMoney <= (vat + fee)) {
//                    tvErrorEdit.setText(getString(R.string.IMR_notice_7));
//                    tvErrorEdit.setVisibility(View.VISIBLE);
//                    tvSend.setEnabled(false);
//                } else {
//                    tvErrorEdit.setVisibility(View.GONE);
//                    tvAddMoney.setEnabled(false);
//                    llError.setVisibility(View.GONE);
//                    tvSend.setEnabled(true);
//                }
            } else {
                if (BigDecimalFormatUtils.compareBig(orderAmount,valiableMoney)) {
                    tvAddMoney.setEnabled(true);
                    llError.setVisibility(View.VISIBLE);
                    tvSend.setEnabled(false);
                } else {
                    tvAddMoney.setEnabled(false);
                    llError.setVisibility(View.GONE);
                    tvSend.setEnabled(true);
                }
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_add_money, R.id.tv_send})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            case R.id.tv_send:
                if(isChannelThunes(channelCode)){
                    if(imrExchangeRateEntity == null){
                        secondGetExchange = true;
                        mPresenter.imrExchangeRate(payeeInfoId,requestSourceAmount,requestDestinationAmount,channelCode);
                        return;
                    }
                }
                mPresenter.imrCheckoutOrder(AndroidUtils.getReqTransferMoney(orderAmount), UserInfoManager.getInstance().getCurrencyCode(), payeeInfoId, transferPurpose, receiptMethod, includeFee,
                        purposeId, purposeDesc,channelCode, transferFees,
                        AndroidUtils.getReqTransferMoney(destinationAmount),
                        destinationCurrencyCode,
                        AndroidUtils.getReqTransferMoney(payerAmount),exchangeRate);
                break;
            default:
                break;
        }
    }

    @Override
    public void imrCheckoutOrderSuccess(ImrOrderInfoEntity imrOrderInfoEntity) {
        if (imrOrderInfoEntity != null) {
            List<ImrComplianceEntity> compliance = imrOrderInfoEntity.compliance;
            if (compliance != null && !compliance.isEmpty()) {
                HashMap<String, Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.ImrOrderInfoEntity, imrOrderInfoEntity);
                mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
                mHashMap.put(Constants.transferDestinationCountryCode, transferDestinationCountryCode);
                ActivitySkipUtil.startAnotherActivity(this, ImrMoreInfoActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                this.mImrOrderInfoEntity = imrOrderInfoEntity;
                String orderInfo = imrOrderInfoEntity.orderInfo;
                if (!TextUtils.isEmpty(orderInfo)) {
                    mPresenter.checkOut("", orderInfo);
                }
            }
        }
    }

    @Override
    public void getImrPurposeListSuccess(ImrPurposeListEntity imrPurposeListEntity) {
        if (imrPurposeListEntity != null) {
            List<ImrPurposeEntity> imrRemittancePurposeList = imrPurposeListEntity.imrRemittancePurposeList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = imrRemittancePurposeList.size();
            for (int i = 0; i < size; i++) {
                ImrPurposeEntity imrPurposeEntity = imrRemittancePurposeList.get(i);
                if (imrPurposeEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = imrPurposeEntity.desc;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_new_10), false, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    ImrPurposeEntity  imrPurposeEntity = imrRemittancePurposeList.get(position);
                    if (imrPurposeEntity != null) {
                        purposeId = imrPurposeEntity.code;
                        transferPurpose = purposeId;
                        purposeDesc = imrPurposeEntity.desc;
                        etPurpose.getEditText().setText(purposeDesc);
                    }
                    if(isChannelThunes(channelCode)){
                        checkMoneyW2W();
                    }else {
                        checkMoney();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getNationalitySuccess");
        }
    }

    @Override
    public void imrCheckoutOrderFail(String errorCode, String errorMsg) {
        if ("030204".equals(errorCode) || "030207".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void getImrPurposeListFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity) {
        if(imrExchangeRateEntity != null){
            setImrExchangeRate(imrExchangeRateEntity);
            checkMoneyW2W();
            if(secondGetExchange) {
                secondGetExchange = false;
                mPresenter.imrCheckoutOrder(AndroidUtils.getReqTransferMoney(orderAmount), UserInfoManager.getInstance().getCurrencyCode(), payeeInfoId, transferPurpose, receiptMethod, includeFee,
                        purposeId, purposeDesc, channelCode, transferFees,
                        AndroidUtils.getReqTransferMoney(destinationAmount),
                        destinationCurrencyCode,
                        AndroidUtils.getReqTransferMoney(payerAmount), exchangeRate);
            }
        }
    }

    @Override
    public void imrExchangeRateFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        imrExchangeRateEntity = null;
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        checkOutEntity.destinationCountry = transferDestinationCountryCode;
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.mImrOrderInfoEntity, mImrOrderInfoEntity);
        ActivitySkipUtil.startAnotherActivity(ImrSendMoneyActivity.this,
                ImrSendMoneySummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkOutFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public ImrSendMoneyContract.Presenter getPresenter() {
        return new ImrSendMoneyPresenter();
    }

}
