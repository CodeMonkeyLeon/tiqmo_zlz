package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class TransferToContactPresenter implements TransferContract.TransferToContactPresenter {

    private TransferContract.TransferToContactView transferToContactView;


    @Override
    public void attachView(TransferContract.TransferToContactView transferToContactView) {
        this.transferToContactView = transferToContactView;
    }

    @Override
    public void detachView() {
        this.transferToContactView = null;
    }

    @Override
    public void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark, String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName) {
        AppClient.getInstance().transferToContact(payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, payerName, new LifecycleMVPResultCallback<TransferEntity>(transferToContactView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        if (transferToContactView != null) {
                            transferToContactView.transferToContactSuccess(result);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (transferToContactView != null) {

                            transferToContactView.transferToContactFail(errorCode, errorMsg);
                        }
                    }
                });
    }

    @Override
    public void chatTransferToContact(RequestTransferEntity requestTransferEntity) {
        AppClient.getInstance().chatTransferToContact(requestTransferEntity, new LifecycleMVPResultCallback<TransferEntity>(transferToContactView,true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                if (transferToContactView != null) {
                    transferToContactView.transferToContactSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {
                    transferToContactView.transferToContactFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(transferToContactView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        if (transferToContactView != null) {

                            transferToContactView.transferSurePaySuccess(result);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (transferToContactView != null) {

                            transferToContactView.transferSurePayFail(errorCode, errorMsg);
                        }
                    }
                });
    }

    @Override
    public void getTransferFee(String type, String orderAmount, String orderCurrencyCode, String transTimeType) {
        AppClient.getInstance().getTransferFee(type, orderAmount, orderCurrencyCode, transTimeType, new LifecycleMVPResultCallback<TransFeeEntity>(transferToContactView, true) {
            @Override
            protected void onSuccess(TransFeeEntity result) {
                if (transferToContactView != null) {

                    transferToContactView.getTransferFeeSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {

                    transferToContactView.getTransferFeeFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(transferToContactView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (transferToContactView != null) {

                    transferToContactView.getLimitSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {

                    transferToContactView.getLimitFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(transferToContactView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                if (transferToContactView != null) {

                    transferToContactView.getOtpTypeSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {

                    transferToContactView.getOtpTypeFail(errorCode,errorMsg);
                }
            }
        });
    }

    @Override
    public void splitBill(String oldOrderNo, String receivableAmount, String currencyCode, String orderRemark, List<SplitBillPayerEntity> receiverInfoList) {
        AppClient.getInstance().splitBill(oldOrderNo, receivableAmount, currencyCode, orderRemark, receiverInfoList, new LifecycleMVPResultCallback<TransferEntity>(transferToContactView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                if (transferToContactView != null) {

                    transferToContactView.splitBillSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {

                    transferToContactView.splitBillFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void sendNotify(List<NotifyEntity> receivers) {
        AppClient.getInstance().sendNotify(receivers, new LifecycleMVPResultCallback<PayBillOrderInfoEntity>(transferToContactView, true) {
            @Override
            protected void onSuccess(PayBillOrderInfoEntity result) {
                if (transferToContactView != null) {

                    transferToContactView.sendNotifySuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {

                    transferToContactView.sendNotifyFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void contactInvite(List<KycContactEntity> contactInviteDtos) {
        for (int i = 0; i < contactInviteDtos.size(); i++) {
            contactInviteDtos.get(i).setCallingCode("+966");
            contactInviteDtos.get(i).setPhone(contactInviteDtos.get(i).getPhone().substring(5));
        }
        AppClient.getInstance().contactInvite(contactInviteDtos, new LifecycleMVPResultCallback<ContactInviteEntity>(transferToContactView) {
            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (transferToContactView != null) {
                    transferToContactView.contactInviteFail(errorCode,errorMsg);
                }
            }

            @Override
            protected void onSuccess(ContactInviteEntity result) {
                if (transferToContactView != null) {

                    transferToContactView.contactInviteSuccess(result);
                }
            }
        });
    }
}
