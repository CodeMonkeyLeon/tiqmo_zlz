package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.QRCodeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ShareQrCodeDialog;

public class MyQrCodeActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_qr_code)
    TextView tvQrCode;
    @BindView(R.id.ll_qr_code)
    LinearLayout llQrCode;

    private String isFrom;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_my_qr_code;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivHeadCircle,ivBack);
        tvTitle.setText(getString(R.string.stt_4_4));

        if(getIntent() != null && getIntent().getExtras() != null){
            isFrom = getIntent().getExtras().getString(Constants.isFrom);
            String qrCodeUrl = getIntent().getExtras().getString(Constants.qrCodeUrl);
            if(!TextUtils.isEmpty(qrCodeUrl)){
                Bitmap logo = BitmapFactory.decodeResource(getResources(),R.drawable.l_qr_logo);
                Bitmap bitmap = QRCodeUtils.createQRCodeWithLogo(qrCodeUrl, AndroidUtils.dip2px(mContext, 300),logo);
                ivQrCode.setImageBitmap(bitmap);
            }
        }

        ivKycStep.setVisibility(View.VISIBLE);
        ivKycStep.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_qr_share));

        if(userInfoEntity != null){
            String userName = userInfoEntity.userName;
            String avatarUrl = userInfoEntity.avatar;
            tvUserName.setText(userName);
            if (!TextUtils.isEmpty(avatarUrl)) {
                Glide.with(mContext)
                        .load(avatarUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ivAvatar.getDrawable())
                        .into(ivAvatar);
            } else {
                ivAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender));
            }
        }
    }

    private void showShareDialog(){
        ShareQrCodeDialog shareQrCodeDialog = new ShareQrCodeDialog(mContext);
        shareQrCodeDialog.setConfirmListener(new ShareQrCodeDialog.ConfirmListener() {
            @Override
            public void clickShare() {
                downShareImage(llQrCode,"");
            }

            @Override
            public void clickSave() {
                saveImage(llQrCode);
            }
        });
        shareQrCodeDialog.showWithBottomAnim();
    }

    @OnClick({R.id.iv_back, R.id.tv_qr_code,R.id.iv_kyc_step})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_kyc_step:
                showShareDialog();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_qr_code:
                if(AndroidUtils.isCloseUse(MyQrCodeActivity.this, Constants.send_scan))return;
                if (isKyc(Constants.KYC_TYPE_NULL)) {
                    if("send".equals(isFrom)){
                        finish();
                    }else {
                        ActivitySkipUtil.startAnotherActivity(MyQrCodeActivity.this, ScanQrActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
                break;
            default:break;
        }
    }
}
