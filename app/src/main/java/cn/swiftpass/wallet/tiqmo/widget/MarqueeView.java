package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

/**
 * des:水平跑马灯滚动视图
 */
public class MarqueeView extends HorizontalScrollView implements Runnable {

    private Context context;
    private LinearLayout mainLayout;//跑马灯滚动部分

    private View parentView;//自定义父类滚动容器
    private boolean hasParent = false;//默认没有父容器

    private int scrollSpeed = 5;//滚动速度
    private int scrollDirection = LEFT_TO_RIGHT;//滚动方向
    private int currentX;//当前x坐标
    private int newCurrentX;//停止滑动时缓存x坐标
    private int viewMargin = 20;//View间距
    private int viewWidth;//View总宽度
    private int screenWidth;//屏幕宽度
    private int multiple = 2;// 显示view与源数据的倍数
    private ArrayList<Integer> viewWidths = new ArrayList<>();//每个view的宽度，用来计算点击事件的位置

    public static final int LEFT_TO_RIGHT = 1;
    public static final int RIGHT_TO_LEFT = 2;

    private OnItemClickListener mOnClickListener;

    public MarqueeView(Context context) {
        this(context, null);
    }

    public MarqueeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MarqueeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initView();
    }

    public void setOnItemClickListener(OnItemClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    void initView() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        screenWidth = wm.getDefaultDisplay().getWidth();
        if (hasParent) {
            this.addView(parentView);
        } else {
            mainLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.scroll_content, null);
            this.addView(mainLayout);
        }
    }

    /**
     * 添加自定义滚动视图
     *
     * @param parentView
     */
    public void setParentView(View parentView) {
        this.parentView = parentView;
        hasParent = true;
    }

    /**
     * 添加本地图片res
     *
     * @param res
     */
    public void addResInQueue(Context c, List<Integer> res) {
        ArrayList<View> views = new ArrayList<>();
        List<Integer> newRes = new ArrayList<>();
        for (int i = 0; i < res.size(); i++) {
            newRes.add(res.get(i % res.size()));
        }
        for (int i = 0; i < newRes.size(); i++) {
            ImageView imageView = new ImageView(c);
            imageView.setBackgroundResource(newRes.get(i));
            views.add(imageView);
        }
        addViewInQueue(views);
    }

    /**
     * 添加视图数组
     *
     * @param view
     */
    public void addViewInQueue(List<View> view) {
        if (!hasParent && mainLayout.getChildCount() > 0) {
            mainLayout.removeAllViews();
        }
        if (LEFT_TO_RIGHT == scrollDirection) {
            Collections.reverse(view);
        }
        for (int i = 0; i < view.size(); i++) {
            if (view.get(i) != null) {
                addViewInQueue(view.get(i));
            }
        }
    }

    /**
     * 添加单个视图  如TextView imageView等等
     *
     * @param view
     */
    private void addViewInQueue(View view) {
        if (view == null) {
            return;
        }
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(viewMargin, 0, 0, 0);
        view.setLayoutParams(lp);
        if (!hasParent) {
            mainLayout.addView(view);
        }
        view.measure(0, 0);//测量view
        viewWidth = viewWidth + view.getMeasuredWidth() + viewMargin;
        viewWidths.add(view.getMeasuredWidth() + viewMargin);
    }

    //开始滚动
    public void startScroll() {
        if (viewWidth / 2 < screenWidth) { // 如果传进来的view的总宽度的一半小于屏幕宽度则不滚动
            return;
        }
        removeCallbacks(this);
        currentX = (scrollDirection == LEFT_TO_RIGHT ? viewWidth - screenWidth : 0);
        post(this);
    }

    //处理完点击事件后重新开始滚动
    public void restartScroll() {
        removeCallbacks(this);
        currentX = newCurrentX;
        post(this);
    }

    //停止滚动
    public void stopScroll() {
        newCurrentX = currentX;
        removeCallbacks(this);
    }

    //设置View间距
    public void setViewMargin(int viewMargin) {
        this.viewMargin = viewMargin;
    }

    //设置滚动速度
    public void setScrollSpeed(int scrollSpeed) {
        this.scrollSpeed = scrollSpeed;
    }

    public void setMultiple(int multiple) {
        this.multiple = multiple;
    }

    //设置滚动方向 默认从左向右
    public void setScrollDirection(int scrollDirection) {
        this.scrollDirection = scrollDirection;
    }

    @Override
    public void run() {
        switch (scrollDirection) {
            case LEFT_TO_RIGHT:
                if (currentX <= viewWidth / 2 - screenWidth) {
                    currentX = viewWidth - screenWidth;
                } else {
                    currentX--;
                }
                scrollToX(currentX);
                break;
            case RIGHT_TO_LEFT:
                if (currentX >= viewWidth / 2) {
                    currentX = 0;
                } else {
                    currentX++;
                }
                scrollToX(currentX);
                break;
            default:
                break;
        }
        postDelayed(this, 100 / scrollSpeed);
    }

    float oldX = -1;
    float diffX;
    float oldTouch = -1;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                oldX = ev.getX();
                oldTouch = ev.getX();
                stopScroll();
                LogUtils.e("MarqueeView", "ACTION_DOWN: ");
                break;
            case MotionEvent.ACTION_MOVE:
                diffX = ev.getX() - oldX;
                newCurrentX = Math.round(newCurrentX - diffX);
                if (scrollDirection == LEFT_TO_RIGHT) {
                    if (newCurrentX <= viewWidth / 2 - screenWidth) {
                        newCurrentX = viewWidth - screenWidth;
                    }
                    if (newCurrentX > viewWidth - screenWidth) {
                        newCurrentX = viewWidth / 2 - screenWidth;
                    }
                } else {
                    if (newCurrentX >= viewWidth / 2) {
                        newCurrentX = 0;
                    }
                    if (newCurrentX < 0) {
                        newCurrentX = viewWidth / 2;
                    }
                }
                scrollToX(newCurrentX);
                oldX = ev.getX();
                LogUtils.e("MarqueeView", "ACTION_MOVE: ");
                break;
            case MotionEvent.ACTION_UP:
                restartScroll();
                LogUtils.e("MarqueeView", "ACTION_UP: ");
                if (Math.abs(ev.getX() - oldTouch) < 2 && mOnClickListener != null) {
                    mOnClickListener.onItemClick(getPosition(Math.round(oldTouch) + currentX));
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = (int) ev.getX();
                startY = (int) ev.getY();
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                int dX = (int) Math.abs(ev.getX() - startX);
                int dY = (int) Math.abs(ev.getY() - startY);
                if (dX < 3 && dY < 3) {
                    return false;
                }
                if (Math.abs(dX) + 50 > Math.abs(dY)) {//左右滑动
                    getParent().requestDisallowInterceptTouchEvent(true);
                } else {//上下滑动
                    getParent().requestDisallowInterceptTouchEvent(false);
                    restartScroll();
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_CANCEL:
                restartScroll();
                break;
            default:
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    private int startX;
    private int startY;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = (int) ev.getX();
                startY = (int) ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:

                int dX = (int) Math.abs(ev.getX() - startX);
                int dY = (int) Math.abs(ev.getY() - startY);
                if (dX < 3 && dY < 3) {
                    return false;
                }
                if (Math.abs(dX) + 50 > Math.abs(dY)) {//左右滑动
                    return false;
                } else {//上下滑动
                    return true;
                }
            case MotionEvent.ACTION_UP:
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    private void scrollToX(int toX) {
        if (hasParent) {
            parentView.scrollTo(toX, 0);
        } else {
            mainLayout.scrollTo(toX, 0);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    // 根据当前点击的位置计算点击的position
    private int getPosition(int currentTouch) {
        for (int i = 0; i < viewWidths.size(); i++) {
            currentTouch = currentTouch - viewWidths.get(i);
            if (currentTouch <= 0) {
                return (LEFT_TO_RIGHT == scrollDirection ? (viewWidths.size() - 1 - i) : i)
                        % (viewWidths.size() / multiple);
            }
        }
        return viewWidths.size();
    }

}
