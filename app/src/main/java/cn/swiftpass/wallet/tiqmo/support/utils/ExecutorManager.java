package cn.swiftpass.wallet.tiqmo.support.utils;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public final class ExecutorManager {

    private final ExecutorService executorService;
    private final ExecutorService singleExecutorService;
    private final Handler mHandler;

    private ExecutorManager() {
        int numberOfCPUCores = Runtime.getRuntime().availableProcessors();
        executorService = Executors.newFixedThreadPool(numberOfCPUCores);
        singleExecutorService = Executors.newSingleThreadExecutor();
        mHandler = new Handler(Looper.myLooper());
    }

    private static final class SingletonHolder {
        private static final ExecutorManager INSTANCE = new ExecutorManager();
    }

    public static ExecutorManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void execute(Runnable task) {
        try {
            executorService.execute(task);
        } catch (RejectedExecutionException e) {
            new Thread(task).start();
        }
    }

    /**
     * 串行执行任务
     *
     * @param task Runnable
     */
    public void executeBySerial(Runnable task) {
        try {
            singleExecutorService.execute(task);
        } catch (RejectedExecutionException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public void runOnUIThread(Runnable task) {
        mHandler.post(task);
    }

    public void shutdown() {
        executorService.shutdown();
        singleExecutorService.shutdown();
        try {
            executorService.awaitTermination(10, TimeUnit.MINUTES);
            singleExecutorService.awaitTermination(10, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
