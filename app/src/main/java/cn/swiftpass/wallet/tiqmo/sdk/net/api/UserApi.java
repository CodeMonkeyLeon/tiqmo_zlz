package cn.swiftpass.wallet.tiqmo.sdk.net.api;


import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AccountInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AvatarUrlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.net.RequestCall;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Headers;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Param;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.UploadPart;

public interface UserApi {

    String SECRET_TYPE_LOGIN = "L";
    String SECRET_TYPE_TRANSACTION = "T";
    String SECRET_OPERATOR_SETUP = "S";
    String SECRET_OPERATOR_MODIFY = "U";

    String THIRD_PARTY_TYPE_WE_CHAT = "TYPE_4";

    String CHECK_PD_VALIDATE_TYPE_DEFAULT = "00";
    String CHECK_PD_VALIDATE_TYPE_PICTURE = "01";

    @Headers({"Service-Id:1020"})
    RequestCall<JsonResponse<Void>> setupPassword(@Param("oldPassword") String oldPassword,
                                                  @Param("password") String password,
                                                  @Param("passwordType") String pdType,
                                                  @Param("operatorType") String operatorType);

    @Headers({"Service-Id:1021"})
    RequestCall<JsonResponse<Void>> checkPassword(@Param("oldPassword") String oldPassword,
                                                  @Param("passwordType") String pdType,
                                                  @Param("verifyCode") String verifyCode,
                                                  @Param("validateType") String validateType);


    @Headers({"Service-Id:1022"})
    RequestCall<JsonResponse<Void>> authentication(@Param("firstName") String firstName,
                                                   @Param("lastName") String lastName,
                                                   @Param("idNumberType") String certificateType,
                                                   @Param("idNumber") String cardNo);

    @Headers({"Service-Id:3037"})
    RequestCall<JsonResponse<UserInfoEntity>> updateUserInfo(@Param("fullName") String fullName,
                                                             @Param("email") String email,
                                                             @Param("gender") String gender,
                                                             @Param("birthday") String birthday,
                                                             @Param("statesId") String statesId,
                                                             @Param("citiesId") String citiesId,
                                                             @Param("address") String address,
                                                             @Param("sourceOfFundCode") String sourceOfFundCode,
                                                             @Param("employmentCode") String employmentCode,
                                                             @Param("professionCode") String professionCode,
                                                             @Param("businessTypeCode") String businessTypeCode,
                                                             @Param("companyName") String companyName,
                                                             @Param("salaryRangeCode") String salaryRangeCode,
                                                             @Param("imrSetUpFlag") String imrSetUpFlag,
                                                             @Param("phone") String phone,
                                                             @Param("orderNo") String orderNo);


    @Headers({"Service-Id:3045"})
    RequestCall<JsonResponse<Void>> checkPassword(@Param("passwordType") String pdType,
                                                  @Param("oldPassword") String oldPassword);

    @Headers({"Service-Id:3232"})
    RequestCall<JsonResponse<StateListEntity>> getStateList();

    @Headers({"Service-Id:3041"})
    RequestCall<JsonResponse<AccountInfoEntity>> getAccountInfo(@Param("type") String type);

    @Headers({"Service-Id:3040"})
    RequestCall<JsonResponse<Void>> stopAccount();

    @Headers({"Service-Id:3036"})
    RequestCall<JsonResponse<UserInfoEntity>> getUserInfo();

    @Headers({"Service-Id:1027"})
    RequestCall<JsonResponse<Void>> unbindThirdPartAuthorization(@Param("authType") String authType);

    @Headers({"Service-Id:1008"})
    RequestCall<JsonResponse<AvatarUrlEntity>> uploadAvatar(@UploadPart("file") String avatar);

    @Headers({"Service-Id:3032"})
    RequestCall<JsonResponse<ConfigEntity>> getConfig();

    @Headers({"Service-Id:3118"})
    RequestCall<JsonResponse<ResultEntity>> renewId();

    @Headers({"Service-Id:3150"})
    RequestCall<JsonResponse<NafathUrlEntity>> getNafathUrl(@Param("callingCode") String callingCode,
                                                            @Param("phone") String phone,
                                                            @Param("recipientId") String recipientId,
                                                            @Param("sceneType") String sceneType);

    @Headers({"Service-Id:3151"})
    RequestCall<JsonResponse<NafathResultEntity>> getNafathResult(@Param("callingCode") String callingCode,
                                                                  @Param("phone") String phone,
                                                                  @Param("hashedState") String hashedState,
                                                                  @Param("recipientId") String recipientId,
                                                                  @Param("random") String random,
                                                                  @Param("sceneType") String sceneType);

    @Headers({"Service-Id:3134"})
    RequestCall<JsonResponse<Void>> activateUserStatus(@Param("lockAcc") String lockAcc,
                                                       @Param("callingCode") String callingCode,
                                                       @Param("loginId") String loginId);

    @Headers({"Service-Id:3118"})
    RequestCall<JsonResponse<ResultEntity>> renewId(@Param("callingCode") String callingCode,
                                                    @Param("loginId") String loginId,
                                                    @Param("lockAcc") String lockAcc);

    @Headers({"Service-Id:3135"})
    RequestCall<JsonResponse<LanguageListEntity>> checkLanguageUpdate();

    @Headers({"Service-Id:3156"})
    RequestCall<JsonResponse<Void>> verifyEmail(@Param("email") String email);

    @Headers({"Service-Id:3157"})
    RequestCall<JsonResponse<CheckPhoneEntity>> checkUserExist(@Param("callingCode") String callingCode,
                                                               @Param("phone") String phone,
                                                               @Param("longitude") double longitude,
                                                               @Param("latitude") double latitude);

    @Headers({"Service-Id:3159"})
    RequestCall<JsonResponse<CheckPhoneEntity>> checkNewDevice(@Param("callingCode") String callingCode,
                                                             @Param("phone") String phone,
                                                             @Param("residentId") String residentId,
                                                             @Param("birthday") String birthday,
                                                   @Param("sceneType") String sceneType,
                                                   @Param("userId") String userId);

    @Headers({"Service-Id:3161"})
    RequestCall<JsonResponse<ChangePhoneFeeEntity>> getChangePhoneFee();

    @Headers({"Service-Id:3162"})
    RequestCall<JsonResponse<ChangePhoneResultEntity>> payChangePhoneFee(@Param("totalAmount") String totalAmount,
                                                                         @Param("vat") String vat);

    @Headers({"Service-Id:3280"})
    RequestCall<JsonResponse<UserUnableUseEntity>> getUserDisableAppFuncCodeList(@Param("callingCode") String callingCode,
                                                                                 @Param("phone") String phone);

}