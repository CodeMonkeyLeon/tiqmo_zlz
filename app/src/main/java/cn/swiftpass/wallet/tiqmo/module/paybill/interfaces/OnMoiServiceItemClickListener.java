package cn.swiftpass.wallet.tiqmo.module.paybill.interfaces;

import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;

public interface OnMoiServiceItemClickListener {
    void onMoiServiceItemClick(int position, PayBillerEntity item);
}
