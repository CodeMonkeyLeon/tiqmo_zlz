package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SelectStoreAdapter extends BaseRecyclerAdapter<VouSupCountryEntity.VouSupCountry> {

    public SelectStoreAdapter(@Nullable List<VouSupCountryEntity.VouSupCountry> data) {
        super(R.layout.select_store_item, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VouSupCountryEntity.VouSupCountry item, int position) {
        if (item == null) {
            return;
        }
        ImageView ivStore = baseViewHolder.getView(R.id.iv_select_store);
        TextView tvStore = baseViewHolder.getView(R.id.tv_select_store);

        if (!TextUtils.isEmpty(item.countryLogo.toString())) {
            Glide.with(mContext).clear(ivStore);
            Glide.with(mContext)
                    .load(item.countryLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivStore);
        }
        if (!TextUtils.isEmpty(item.countryName)) {
            tvStore.setText(item.countryName);
        }
    }

    @Override
    public OnItemClickListener getOnItemClickListener() {
        return super.getOnItemClickListener();
    }
}
