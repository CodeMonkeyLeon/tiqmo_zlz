package cn.swiftpass.wallet.tiqmo.support.zxing.Decoding;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;

import cn.swiftpass.wallet.tiqmo.support.zxing.Camera.CameraManager;
import cn.swiftpass.wallet.tiqmo.support.zxing.ViewfinderView;


public interface QrcodeScanListener {

    void scanResult(boolean isOk, String result);

    void submitData(String result, boolean status);

    void drawViewfinder();

    void handleDecode(Result obj, Bitmap barcode);

    void setResult(int resultOk, Intent obj);

    ViewfinderView getViewfinderView();

    Handler getHandler();

    CameraManager getCameraManager();

    Context getContext();
}
