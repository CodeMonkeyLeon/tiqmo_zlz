package cn.swiftpass.wallet.tiqmo.sdk.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RegisterOtpEntity extends BaseEntity {

    /**
     * "codeLength": 4,
     *             "phone": "1827632727",
     *             "type": "M",
     *             "verifyCodeType": 0,
     * "callingCode": "966"
     */

    public String codeLength;
    public String phone;
    public String type;
    public String verifyCodeType;
    public String callingCode;
    public Boolean verifyCodeExpired;
    public long expiredTime;
}
