package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrOrderInfoEntity extends BaseEntity {

    public List<ImrComplianceEntity> compliance = new ArrayList<>();
    //订单信息
    public String orderInfo;

    public RiskControlEntity riskControlInfo;
}
