package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Bitmap;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SplitPeopleAdapter extends BaseRecyclerAdapter<KycContactEntity> {

    private String peopleMoney, lastPeopleMoney, allMoney;
    private TextWatcher textWatcher;
//    private int currentPosition;
    //是否是初始化金额
    private boolean isInitMoney;

    public void setInitMoney(final boolean initMoney) {
        this.isInitMoney = initMoney;
    }

    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }

    public SplitPeopleAdapter(@Nullable List<KycContactEntity> data) {
        super(R.layout.item_split_people, data);
    }

    public void setEditMoney(String money) {
        this.peopleMoney = money;
    }

    public void setAllMoney(String allMoney) {
        this.allMoney = allMoney;
    }

    public void setLastPeopleMoney(String money) {
        this.lastPeopleMoney = money;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, KycContactEntity kycContactEntity, int position) {

        EditText etBillAmount = baseViewHolder.getView(R.id.et_amount);
        //两位小数过滤
        etBillAmount.setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etBillAmount.setTag(position);
        etBillAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String amount = etBillAmount.getText().toString().trim();
                amount = amount.replace(",", "");
                String fomatAmount = AmountUtil.dataFormat(amount);
                if (!hasFocus) {
                    etBillAmount.setText(fomatAmount);
                }
            }
        });

        etBillAmount.addTextChangedListener(new TextSwitcher(baseViewHolder));

        String name = kycContactEntity.getContactsName();
        if (TextUtils.isEmpty(name)) {
            baseViewHolder.setGone(R.id.tv_user_name, true);
        } else {
            baseViewHolder.setGone(R.id.tv_user_name, false);
            baseViewHolder.setText(R.id.tv_user_name, name);
        }
        baseViewHolder.setText(R.id.tv_phone, kycContactEntity.getPhone());
        baseViewHolder.addOnClickListener(R.id.iv_plus, R.id.iv_minus);

        etBillAmount.setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});

        etBillAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    String amount = etBillAmount.getText().toString();
                    amount = amount.replace(",", "");
                    String money = "0";
                    if (!TextUtils.isEmpty(amount)) {
                        money = amount;
                    }
                    etBillAmount.setText(AmountUtil.dataFormat(money));
                    ((CreateSplitBillActivity) mContext).changeMoney(position, money);
                }
            }
        });

//        if (position == mDataList.size() - 1) {
//            String fomatAmount = AmountUtil.dataFormat(String.valueOf(lastPeopleMoney));
//            etBillAmount.setText(fomatAmount);
//        } else {
        String fomatAmount = AmountUtil.dataFormat(String.valueOf(kycContactEntity.splitMoney));
        etBillAmount.setText(fomatAmount);
//        }

        ImageView ivAvatar = baseViewHolder.getView(R.id.iv_avatar);
        String avatarUrl = kycContactEntity.getHeadIcon();
        String gender = kycContactEntity.getSex();
        if (!TextUtils.isEmpty(avatarUrl)) {
            Glide.with(mContext).clear(ivAvatar);
            Glide.with(mContext)
                    .asBitmap()
                    .load(avatarUrl)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .dontAnimate()
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(new BitmapImageViewTarget(ivAvatar) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                            ivAvatar.setImageBitmap(bitmap);
                        }
                    });
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivAvatar);
        }

        if (position == mDataList.size() - 1) {
            isInitMoney = false;
        }
    }

    class TextSwitcher implements TextWatcher {
        private BaseViewHolder mHolder;

        public TextSwitcher(BaseViewHolder mHolder) {
            this.mHolder = mHolder;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(isInitMoney)return;
            int position = (int) mHolder.getView(R.id.et_amount).getTag();//取tag值
            String amount = s.toString();
            amount = amount.replace(",", "");
            String money = "0";
            if (!TextUtils.isEmpty(amount)) {
                money = amount;
                if (BigDecimalFormatUtils.isZero(money)) {
                    mHolder.getView(R.id.ll_line).setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_70fd1d2d));
                } else {
                    mHolder.getView(R.id.ll_line).setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_6b6c73_light));
                }
            } else {
                mHolder.getView(R.id.ll_line).setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_70fd1d2d));
            }
            ((CreateSplitBillActivity) mContext).saveEditData(position, money);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
