package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.callback.UploadFileCallback;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.PersonInfoContract;
import cn.swiftpass.wallet.tiqmo.module.setting.dialog.DeActNoticeDialog;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RegionEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.PersonInfoPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ViewAnimUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CommonTwoButtonDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ConfirmPayDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.DeactivationLoginDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.EnterEmailDialog;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusInterface;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class CustomerInfoActivity extends BaseCompatActivity<PersonInfoContract.Presenter> implements PersonInfoContract.View {

    private static final int PERMISSION_REQUEST_CODE_CAMERA = ActivityHelper.generateRequestCode();

    private static final int REQUEST_CODE_CAMERA = ActivityHelper.generateRequestCode();
    private static final int REQUEST_CODE_ALBUM = ActivityHelper.generateRequestCode();
    @BindView(R.id.tv_second_title)
    TextView tvSecondTitle;
    @BindView(R.id.tv_iban_num)
    TextView tvIbanNum;
    @BindView(R.id.ll_personal_info_title)
    LinearLayout llPersonalInfoTitle;
    @BindView(R.id.ll_address_title)
    LinearLayout llAddressTitle;
    @BindView(R.id.tv_state_subtitle)
    TextView tvStateSubtitle;
    @BindView(R.id.tv_state)
    TextView tvState;
    @BindView(R.id.ll_state_choose)
    FrameLayout llStateChoose;
    @BindView(R.id.ll_city_choose)
    FrameLayout llCityChoose;
    @BindView(R.id.et_kyc_address)
    CustomizeEditText etKycAddress;
    @BindView(R.id.ll_additional_details_title)
    LinearLayout llAdditionalDetailsTitle;

    private String timeFormat = "MMMM dd,yy";
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.con_kyc)
    ConstraintLayout conKyc;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.iv_take_photo)
    ImageView ivTakePhoto;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_change_mobile)
    TextView tvChangeMobile;
    @BindView(R.id.tv_wallet_account)
    TextView tvWalletAccount;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_bank_number)
    TextView tvBankNumber;
    @BindView(R.id.iv_copy_bank)
    ImageView ivCopyBank;
    @BindView(R.id.et_email_id)
    CustomizeEditText etEmailId;
    @BindView(R.id.et_birthday)
    CustomizeEditText etBirthday;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.cb_male)
    CheckBox cbMale;
    @BindView(R.id.iv_male_checked)
    ImageView ivMaleChecked;
    @BindView(R.id.con_male)
    ConstraintLayout conMale;
    @BindView(R.id.cb_female)
    CheckBox cbFemale;
    @BindView(R.id.iv_female_checked)
    ImageView ivFemaleChecked;
    @BindView(R.id.con_female)
    ConstraintLayout conFemale;
    @BindView(R.id.tv_stop_user)
    TextView tvStopUser;
    @BindView(R.id.con_content)
    ConstraintLayout conContent;
    @BindView(R.id.iv_head_circle)
    ImageView headView;
    @BindView(R.id.ll_personal_info_content)
    LinearLayout llPersonalInfoContent;
    @BindView(R.id.ll_address_content)
    LinearLayout llAddressContent;
    @BindView(R.id.ll_additional_details_content)
    LinearLayout llAdditionalDetailsContent;
    @BindView(R.id.iv_personal_arrow)
    ImageView ivPersonalArrow;
    @BindView(R.id.iv_address_arrow)
    ImageView ivAddressArrow;
    @BindView(R.id.iv_additional_arrow)
    ImageView ivAdditionalArrow;
    @BindView(R.id.tv_city_choose_subtitle)
    TextView tvCityChooseSub;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_save)
    TextView tvSave;
    @BindView(R.id.ll_address)
    LinearLayout llAddress;
    @BindView(R.id.fl_source_of_fund)
    FrameLayout flSourceOfFund;
    @BindView(R.id.fl_profession)
    FrameLayout flProfession;
    @BindView(R.id.fl_company_type)
    FrameLayout flCompanyType;
    @BindView(R.id.fl_salary_range)
    FrameLayout flSalaryRange;
    @BindView(R.id.tv_source_of_fund)
    TextView tvSourceOfFund;
    @BindView(R.id.tv_profession)
    TextView tvProfession;
    @BindView(R.id.tv_company_type)
    TextView tvCompanyType;
    @BindView(R.id.et_company_name)
    CustomizeEditText etCompanyName;
    @BindView(R.id.tv_salary_range)
    TextView tvSalaryRange;
    @BindView(R.id.tv_source_of_fund_subtitle)
    TextView tvSourceOfFundSubtitle;
    @BindView(R.id.tv_profession_subtitle)
    TextView tvProfessionSubtitle;
    @BindView(R.id.tv_company_type_subtitle)
    TextView tvCompanyTypeSubtitle;
    @BindView(R.id.tv_salary_range_subtitle)
    TextView tvSalaryRangeSubtitle;
    @BindView(R.id.ll_renew_id)
    LinearLayout llRenewId;
    @BindView(R.id.tv_renew_id)
    TextView tvRenewId;
    @BindView(R.id.et_employer_name)
    CustomizeEditText etEmployerName;
    @BindView(R.id.tv_employment_subtitle)
    TextView tvEmploymentSubtitle;
    @BindView(R.id.tv_employment)
    TextView tvEmployment;
    @BindView(R.id.fl_employment)
    FrameLayout flEmployment;
    @BindView(R.id.tv_verify)
    TextView tvVerify;
    @BindView(R.id.tv_verified)
    TextView tvVerified;

    private BottomDialog bottomDialog;

    private Uri mImageSaveUri;

    private StatusView statusView;

    private String employmentCode;

    private double balanceMoney;

    //证件是否过期 1：过期，0：未过期
    private String isExpiryDate;
    //邮箱验证标识 Y:已验证 N:未验证
    public String emailVerifyFlag;
    //证件过期日期 （dd-MM-yyyy）
//    private String expiryDate;

    private EnterEmailDialog enterEmailDialog;

    private String orderNo;
    private String changePhoneMoney = "";

    private ChangePhoneFeeEntity mChangePhoneFeeEntity;
    private ConfirmPayDialog confirmPayDialog;
    private CommonTwoButtonDialog commonTwoButtonDialog;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_person_info;
    }

    private UserInfoEntity userInfoEntity;

    private Date mSelectedDate;
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedDate = Calendar.getInstance();
    Calendar startDate = Calendar.getInstance();
    Calendar endDate = Calendar.getInstance();

    //处理失焦
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (AndroidUtils.isShouldHideInput(v, ev)) {//点击editText控件外部
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    assert v != null;
                    AndroidUtils.hideKeyboard(v);//软键盘工具类
                    if (etEmailId != null) {
                        etEmailId.clearFocus();
                    }
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(tvVerify.getVisibility() == View.VISIBLE || tvVerified.getVisibility() == View.VISIBLE){
            etEmailId.setPadding(LocaleUtils.isRTL(mContext) ? AndroidUtils.dip2px(mContext,80):0,0,LocaleUtils.isRTL(mContext) ? 0:AndroidUtils.dip2px(mContext,80),0);
        }else{
            etEmailId.setPadding(0,0,0,0);
        }

        if (ProjectApp.kycSuccess) {
            ActivitySkipUtil.startAnotherActivity(this, KycSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.kycSuccess = false;
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack, headView, ivPersonalArrow, ivAddressArrow, ivAdditionalArrow);
        UxcamHelper.getInstance().hideScreen(true);
        fixedSoftKeyboardSliding();
        tvTitle.setText(R.string.Profile_CLP_0010_L_3);
        tvSalaryRangeSubtitle.setText(R.string.newhome_36);
        etBirthday.setEnabled(false);
        etEmployerName.setVisibility(View.GONE);
        startDate.set(Calendar.YEAR, selectedDate.get(Calendar.YEAR) - 90);
        endDate.set(Calendar.YEAR, selectedDate.get(Calendar.YEAR) - 16);
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        etCompanyName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(64)});

        etCompanyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    userInfoEntity.companyName = etCompanyName.getText().toString().trim();
                    checkUpdateState();
                }
            }
        });
        etCompanyName.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                userInfoEntity.companyName = s.toString().trim();
                checkUpdateState();
            }
        });

        try {
            if (userInfoEntity != null) {
                //加载图片
                Glide.with(this)
                        .load(userInfoEntity.avatar)
                        .error(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender))
                        .into(ivAvatar);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }

        etEmailId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && userInfoEntity != null) {
                    checkUpdateState();
                }
            }
        });

        etEmailId.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                tvVerify.setVisibility(View.GONE);
                tvVerified.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(userInfoEntity.email)) {
                    if (TextUtils.isEmpty(s.toString())) {
                        tvSave.setEnabled(false);
                    }else if(userInfoEntity.email.equals(s.toString())){
                        if(userInfoEntity.isEmailVerify()){
                            tvVerify.setVisibility(View.GONE);
                            tvVerified.setVisibility(View.VISIBLE);
                        }else{
                            tvVerify.setVisibility(View.VISIBLE);
                            tvVerified.setVisibility(View.GONE);
                        }
                    }
                }
                if(tvVerify.getVisibility() == View.VISIBLE || tvVerified.getVisibility() == View.VISIBLE){
                    etEmailId.setPadding(LocaleUtils.isRTL(mContext) ? AndroidUtils.dip2px(mContext,80):0,0,LocaleUtils.isRTL(mContext) ? 0:AndroidUtils.dip2px(mContext,80),0);
                }else{
                    etEmailId.setPadding(0,0,0,0);
                }
            }
        });
        etName.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                if (TextUtils.isEmpty(s.toString())) {
                    tvSave.setEnabled(false);
                }
            }
        });

        etKycAddress.setFilters(new InputFilter[]{new InputFilter.LengthFilter(64),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPACE)});
        etKycAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && userInfoEntity != null) {
                    userInfoEntity.address = etKycAddress.getText().toString().trim();
                    checkUpdateState();
                }
            }
        });

        etKycAddress.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (userInfoEntity == null) return;
                userInfoEntity.address = s.toString().trim();
                checkUpdateState();
            }
        });

        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AndroidUtils.hideKeyboard(v);
                    etName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(60),
                            new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPECIAL_SPACE)});
                    String userName = etName.getText().toString().trim();


                    if (!TextUtils.isEmpty(userName)) {
                        if (!userName.equals(userInfoEntity.userName)) {
                            userInfoEntity.userName = userName;
                        }

                        if (userName.length() > 16) {
                            userName = userName.substring(0, 16) + "...";
                        }
                        etName.setText(userName.trim());
                    } else {
                        etName.setText(userInfoEntity.userName);
                    }
                    checkUpdateState();
                } else {
                    if (!TextUtils.isEmpty(userInfoEntity.userName)) {
                        etName.setText(userInfoEntity.userName);
                        etName.post(new Runnable() {
                            @Override
                            public void run() {
                                etName.setSelection(userInfoEntity.userName.length());
                            }
                        });
                        showSoftKeyboard(etName);
                    }
                    etName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(60), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_SPACE)});
                }
            }
        });

        statusView = new StatusView.Builder(mContext, conContent)
                .setNetErrorListener(new StatusInterface.OnClickListener() {
                    @Override
                    public void onClick() {
                        if (isConnected()) {
                            mPresenter.getUserInfo();
                        }
                    }
                }).build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
        mPresenter.getUserInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_CHANGE_PHONE_PAY_SUCCESS == event.getEventType()) {
            if(confirmPayDialog != null){
                confirmPayDialog.dismiss();
            }
        }else if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                if(confirmPayDialog != null){
                    confirmPayDialog.updateBalance();
                }
            }
        }else if(Constants.KYC_CHANGE_PHONE_NUMBER == event.getEventType()){
            mPresenter.getChangePhoneFee();
        }
    }


    private void checkUpdateState() {
        String email = etEmailId.getText().toString().trim();
        String userName = etName.getText().toString().trim();

        if (!TextUtils.isEmpty(email)) {
            if (!RegexUtil.isEmail(email)) {
                etEmailId.setError(getString(R.string.Customer_information_PD_0011_1_L_10));
                tvSave.setEnabled(false);
                return;
            }
        } else {
            //如果用户之前设置了email,则清空后不能保存
            if (!TextUtils.isEmpty(userInfoEntity.email)) {
                tvSave.setEnabled(false);
                return;
            }
        }

        //userName不能为空
        if (TextUtils.isEmpty(userName)) {
            tvSave.setEnabled(false);
            return;
        }
        tvSave.setEnabled(true);
        if (userInfoEntity.isKyc()) {
            tvSave.setEnabled((!TextUtils.isEmpty(userInfoEntity.citiesId)
                            && !TextUtils.isEmpty(userInfoEntity.address))
                            && ((!TextUtils.isEmpty(userInfoEntity.sourceOfFundCode) && !TextUtils.isEmpty(userInfoEntity.professionCode)
                            && !TextUtils.isEmpty(userInfoEntity.employmentCode)
//                                    && !TextUtils.isEmpty(userInfoEntity.businessTypeCode) && !TextUtils.isEmpty(userInfoEntity.companyName)
                            && !TextUtils.isEmpty(userInfoEntity.salaryRangeCode))));
        }
    }

    private void updateUserInfo() {
        userInfoEntity.email = etEmailId.getText().toString().trim();
        userInfoEntity.phone = "";
        mPresenter.updateUserInfo(userInfoEntity);
    }

    @Override
    public void getUserInfoSuccess(UserInfoEntity userInfo) {
        this.userInfoEntity = userInfo;
        isExpiryDate = userInfoEntity.isExpiryDate;
        emailVerifyFlag = userInfoEntity.emailVerifyFlag;
        if(userInfoEntity.isEmailVerify()){
            tvVerified.setVisibility(View.VISIBLE);
            tvVerify.setVisibility(View.GONE);
        }else{
            tvVerified.setVisibility(View.GONE);
            tvVerify.setVisibility(View.VISIBLE);
        }

        if (userInfoEntity.isExpiryId(isExpiryDate)) {
            llRenewId.setVisibility(View.VISIBLE);
        } else {
            llRenewId.setVisibility(View.GONE);
        }
        balanceMoney = userInfoEntity.getBalanceSumMoney();
//        showStatusView(statusView, StatusView.CONTENT_VIEW);
        AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
        if (!TextUtils.isEmpty(userInfoEntity.userName)) {
            if (userInfoEntity.userName.length() > 16) {
                String showUserName = userInfoEntity.userName.substring(0, 16) + "...";
                etName.setText(showUserName.trim());
            } else {
                etName.setText(userInfoEntity.userName.trim());
            }
        }
        tvBankName.setText(userInfoEntity.accountName);
        String ibanNum = userInfoEntity.ibanNum;
        if (!TextUtils.isEmpty(ibanNum)) {
            tvBankNumber.setText(AndroidUtils.changeBanNum(ibanNum));
        }
        if (!TextUtils.isEmpty(userInfoEntity.phone)) {
            tvPhone.setText(new StringBuffer("+").append(SpUtils.getInstance().getCallingCode()).append(userInfoEntity.phone));
        }
        if (!TextUtils.isEmpty(userInfoEntity.email)) {
            etEmailId.setText(userInfoEntity.email);
        }else{
            tvVerify.setVisibility(View.GONE);
            tvVerified.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(userInfoEntity.birthday)) {
            etBirthday.setText(userInfoEntity.birthday);
        }
        if (!TextUtils.isEmpty(userInfoEntity.gender)) {
            selectGender();
        } else {
            cbMale.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_9c9da1)));
            cbFemale.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_9c9da1)));
        }
        if (TextUtils.isEmpty(userInfoEntity.avatar)) {
            Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender)).into(ivAvatar);
        } else {
            showNewAvatar(userInfoEntity.avatar);
        }

        if (!TextUtils.isEmpty(userInfoEntity.citiesId)) {
            refreshItemView(userInfoEntity.cities, tvCity, tvCityChooseSub, true);
        }
        if (!TextUtils.isEmpty(userInfoEntity.statesId)) {
            refreshItemView(userInfoEntity.getStatesName(), tvState, tvStateSubtitle, true);
        }
        if (!TextUtils.isEmpty(userInfoEntity.address)) {
            etKycAddress.setText(userInfoEntity.address);
        }
        if (!TextUtils.isEmpty(userInfoEntity.sourceOfFundCode)) {
            refreshItemView(userInfoEntity.sourceOfFundDesc, tvSourceOfFund, tvSourceOfFundSubtitle, true);
        }
        if (!TextUtils.isEmpty(userInfoEntity.professionCode)) {
            refreshItemView(userInfoEntity.professionDesc, tvProfession, tvProfessionSubtitle, true);
        }
        if (!TextUtils.isEmpty(userInfoEntity.businessTypeCode)) {
            refreshItemView(userInfoEntity.businessTypeDesc, tvCompanyType, tvCompanyTypeSubtitle, true);
        }
        if (!TextUtils.isEmpty(userInfoEntity.companyName)) {
            etCompanyName.setText(userInfoEntity.companyName);
        }
        if (!TextUtils.isEmpty(userInfoEntity.employmentCode)) {
            employmentCode = userInfoEntity.employmentCode;
            refreshItemView(userInfoEntity.employmentDesc, tvEmployment, tvEmploymentSubtitle, true);
        }
        if (!TextUtils.isEmpty(userInfoEntity.salaryRangeCode)) {
            refreshItemView(userInfoEntity.salaryRangeDesc, tvSalaryRange, tvSalaryRangeSubtitle, true);
        }
        etName.setEnabled(false);
        cbFemale.setEnabled(false);
        cbMale.setEnabled(false);
        etBirthday.setOnUnableClickListener(null);
        //KYC用户只能修改邮箱
        if (userInfoEntity.isKyc()) {
            llAddress.setVisibility(View.VISIBLE);
            tvSave.setEnabled(true);
            conKyc.setVisibility(View.VISIBLE);
        } else {
            etBirthday.setOnUnableClickListener(null);
            etName.setEnabled(false);
            cbFemale.setEnabled(false);
            cbMale.setEnabled(false);
            conKyc.setVisibility(View.GONE);

            llAddress.setVisibility(View.GONE);
            if (userInfoEntity.isKycLow()) {
                tvSave.setEnabled(true);
            } else {
                checkUpdateState();
            }
        }
    }

    private void selectGender() {
        ivMaleChecked.setVisibility("0".equals(userInfoEntity.gender) ? View.VISIBLE : View.GONE);
        ivFemaleChecked.setVisibility("1".equals(userInfoEntity.gender) ? View.VISIBLE : View.GONE);
        conMale.setAlpha("0".equals(userInfoEntity.gender) ? 1f : 0.4f);
        conFemale.setAlpha("1".equals(userInfoEntity.gender) ? 1f : 0.4f);
        cbMale.setTextColor(getColor(
                ThemeSourceUtils.getSourceID(mContext, "0".equals(userInfoEntity.gender) ?
                        R.attr.color_gender_selected : R.attr.color_gender_unselected)));
        cbFemale.setTextColor(getColor(
                ThemeSourceUtils.getSourceID(mContext, "1".equals(userInfoEntity.gender) ?
                        R.attr.color_gender_selected : R.attr.color_gender_unselected)));
    }

    @Override
    public void getUserInfoFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void updateUserInfoSuccess(UserInfoEntity userInfoEntity) {
        showTipDialog(userInfoEntity.message);
        getUserInfoSuccess(userInfoEntity);
    }

    @Override
    public void updateUserInfoFail(String errorCode, String errorMsg) {
        mPresenter.getUserInfo();
    }

    @Override
    public void showUploadSuccessfulHint() {
    }

    @Override
    public void showUploadFailedHint(String error) {
    }

    @Override
    public void showNewAvatar(String avatarUrl) {
        if (!TextUtils.isEmpty(avatarUrl)) {//特殊处理 不需要设置placeHolder  不然修改成功后先变成默认头像 再显示新的

            Glide.with(this).clear(ivAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(this)
                    .asBitmap()
                    .load(avatarUrl)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .dontAnimate()
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender))
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .into(new BitmapImageViewTarget(ivAvatar) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                            ivAvatar.setImageBitmap(bitmap);
                        }
                    });
        }
    }

    @Override
    public void showUserInfo(UserInfoEntity userInfo) {

    }

    @Override
    public void getStateListSuccess(StateListEntity stateListEntity) {
        if (stateListEntity.statesInfoList.size() == 0) {
            return;
        }
        ArrayList<SelectInfoEntity> stateNameList = new ArrayList<>();
        for (int i = 0; i < stateListEntity.statesInfoList.size(); i++) {
            stateNameList.add(new SelectInfoEntity(stateListEntity.statesInfoList.get(i).statesName));
        }
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(stateNameList, getString(R.string.IMR_new_4), true, true);
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                StateEntity stateEntity = stateListEntity.statesInfoList.get(position);
                if (stateEntity == null) return;
                if (TextUtils.isEmpty(userInfoEntity.statesId) || !userInfoEntity.statesId.equals(stateEntity.statesId)) {
                    tvCity.setText("");
                    tvCity.setVisibility(View.GONE);
                    ViewAnim.resetViewParams(mContext, tvCityChooseSub);
                    userInfoEntity.statesId = stateEntity.statesId;
                    userInfoEntity.statesName = stateEntity.statesName;
                    userInfoEntity.citiesId = null;
                    userInfoEntity.cities = null;
                }
                refreshItemView(userInfoEntity.statesName, tvState, tvStateSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "state");
    }

    @Override
    public void showCityListDia(CityEntity result) {
        ArrayList<SelectInfoEntity> cityNameList = new ArrayList<>();
        for (int i = 0; i < result.citiesInfoList.size(); i++) {
            cityNameList.add(new SelectInfoEntity(result.citiesInfoList.get(i).citiesName));
        }
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(cityNameList, getString(R.string.IMR_72), true, true);
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (TextUtils.isEmpty(userInfoEntity.citiesId) ||
                        !result.citiesInfoList.get(position).citiesId.equals(userInfoEntity.citiesId)) {
                    userInfoEntity.cities = result.citiesInfoList.get(position).citiesName;
                    userInfoEntity.citiesId = result.citiesInfoList.get(position).citiesId;
//                    userInfoEntity.neighborhoodsId = null;
//                    userInfoEntity.neighborhoods = null;
                }
                refreshItemView(userInfoEntity.cities, tvCity, tvCityChooseSub, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "city");
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkUpdateState();
                    }
                }
            });
        } else {
            content.setText(value);
            if (!isInit) {
                checkUpdateState();
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void showRegionListDia(RegionEntity result) {
//        ArrayList<SelectInfoEntity> regionNameList = new ArrayList<>();
//        for (int i = 0; i < result.neighborhoodsInfoList.size(); i++) {
//            regionNameList.add(new SelectInfoEntity(result.neighborhoodsInfoList.get(i).neighborhoodsName));
//        }
//        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(regionNameList, getString(R.string.IMR_73), true, true);
//        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
//            @Override
//            public void onItemClick(int position) {
//                if (position >= result.neighborhoodsInfoList.size() ||
//                        result.neighborhoodsInfoList.get(position) == null ||
//                        result.neighborhoodsInfoList.get(position).neighborhoodsId.equals(userInfoEntity.neighborhoodsId)) {
//                    return;
//                }
//                userInfoEntity.neighborhoods = result.neighborhoodsInfoList.get(position).neighborhoodsName;
//                userInfoEntity.neighborhoodsId = result.neighborhoodsInfoList.get(position).neighborhoodsId;
////                refreshItemView(userInfoEntity.neighborhoods, tvDistrict, tvDistrictChooseSub, false);
//            }
//        });
//        dialog.showNow(getSupportFragmentManager(), "district");
    }

    @Override
    public void showDeactivationDia() {

    }

    @Override
    public void showCannotDeactivationDia() {

    }

    @Override
    public void showSourceOfFund(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_6));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(userInfoEntity.sourceOfFundCode)) {
                    return;
                }
                userInfoEntity.sourceOfFundDesc = data.get(position).businessParamValue;
                userInfoEntity.sourceOfFundCode = data.get(position).businessParamKey;
                refreshItemView(userInfoEntity.sourceOfFundDesc, tvSourceOfFund, tvSourceOfFundSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showSourceOfFund");
    }

    @Override
    public void showProfession(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_7));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(userInfoEntity.professionCode)) {
                    return;
                }
                userInfoEntity.professionDesc = data.get(position).businessParamValue;
                userInfoEntity.professionCode = data.get(position).businessParamKey;
                refreshItemView(userInfoEntity.professionDesc, tvProfession, tvProfessionSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showProfession");
    }

    @Override
    public void showCompanyType(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.IMR_8));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(userInfoEntity.businessTypeCode)) {
                    return;
                }
                userInfoEntity.businessTypeDesc = data.get(position).businessParamValue;
                userInfoEntity.businessTypeCode = data.get(position).businessParamKey;
                refreshItemView(userInfoEntity.businessTypeDesc, tvCompanyType, tvCompanyTypeSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showCompanyType");
    }

    @Override
    public void showSalaryRange(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.newhome_36));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(userInfoEntity.businessTypeCode)) {
                    return;
                }
                userInfoEntity.salaryRangeDesc = data.get(position).businessParamValue;
                userInfoEntity.salaryRangeCode = data.get(position).businessParamKey;
                refreshItemView(userInfoEntity.salaryRangeDesc, tvSalaryRange, tvSalaryRangeSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "showSalaryRange");
    }

    @Override
    public void renewIdSuccess(ResultEntity resultEntity) {
        if (resultEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            if (resultEntity.renewIdSuccess(resultEntity)) {
                mHashMap.put(Constants.changePhoneResult, ChangePhoneResultActivity.status_renew_id_success);
            } else {
                mHashMap.put(Constants.changePhoneResult, ChangePhoneResultActivity.status_renew_id_fail);
            }
            ActivitySkipUtil.startAnotherActivity(this, ChangePhoneResultActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        mPresenter.getUserInfo();
    }

    @Override
    public void getEmploymentSectorSuccess(List<SelectInfoEntity> data) {
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(data, getString(R.string.sprint19_2));
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position >= data.size() || data.get(position) == null
                        || TextUtils.isEmpty(data.get(position).businessParamKey)
                        || data.get(position).businessParamKey.equals(userInfoEntity.businessTypeCode)) {
                    return;
                }
                if(!data.get(position).businessParamKey.equals(employmentCode)) {
                    userInfoEntity.professionCode = "";
                    tvProfession.setText("");
                    tvProfession.setVisibility(View.GONE);
                    ViewAnim.resetViewParams(mContext, tvProfessionSubtitle);
                }
                userInfoEntity.employmentDesc = data.get(position).businessParamValue;
                userInfoEntity.employmentCode = data.get(position).businessParamKey;
                employmentCode = data.get(position).businessParamKey;
                refreshItemView(userInfoEntity.employmentDesc, tvEmployment, tvEmploymentSubtitle, false);
            }
        });
        dialog.showNow(getSupportFragmentManager(), "employmentSector");
    }

    @Override
    public void renewIdFail(String errorCode, String errorMsg) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.changePhoneResult, ChangePhoneResultActivity.status_renew_id_fail);
        ActivitySkipUtil.startAnotherActivity(this, ChangePhoneResultActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void verifyEmailSuccess(Void result) {
        if(enterEmailDialog != null) {
            enterEmailDialog.dismiss();
        }
        mPresenter.getUserInfo();
    }

    @Override
    public void getChangePhoneFeeSuccess(ChangePhoneFeeEntity changePhoneFeeEntity) {
        if(changePhoneFeeEntity != null){
            mChangePhoneFeeEntity = changePhoneFeeEntity;
            //是否需要收费 Y/N Y需要
             String chargeFee = changePhoneFeeEntity.chargeFee;
            //提示的词条
             String comment = changePhoneFeeEntity.comment;
             String orderNo = changePhoneFeeEntity.orderNo;
            //需要支付的金额
            changePhoneMoney = changePhoneFeeEntity.totalAmount;
             if(changePhoneFeeEntity.needChargeFee()){
                 mPresenter.getLimit(Constants.LIMIT_TRANSFER);
//                 showChangePhoneFeeDialog(comment);
             }else{
                 HashMap<String, Object> mHashMaps = new HashMap<>(16);
                 mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_CHANGE_PHONE);
                 if(!TextUtils.isEmpty(orderNo)){
                     mHashMaps.put(Constants.mChangePhoneOrderNo, orderNo);
                 }
                 ActivitySkipUtil.startAnotherActivity(this, RegisterActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
             }
        }
    }

    private void showChangePhoneFeeDialog(String comment){
        commonTwoButtonDialog = new CommonTwoButtonDialog(mContext);
        commonTwoButtonDialog.setViewContent(getString(R.string.sprint20_6),comment,ThemeSourceUtils.getSourceID(mContext,R.attr.icon_error_mobile_number));
        commonTwoButtonDialog.setTwoButtonListener(getString(R.string.Deactivation_PD_0012_1_L_6), new CommonTwoButtonDialog.onTwoButtonListener() {
            @Override
            public void clickConfirm() {
                mPresenter.getLimit(Constants.LIMIT_TRANSFER);
            }

            @Override
            public void clickCancel() {

            }
        });
        commonTwoButtonDialog.showWithBottomAnim();
    }

    private void showConfirmPayDialog(TransferLimitEntity mTransferLimitEntity){
        confirmPayDialog = new ConfirmPayDialog(mContext,mChangePhoneFeeEntity,mTransferLimitEntity);
        confirmPayDialog.setPayMoneyListener(new ConfirmPayDialog.PayMoneyListener() {
            @Override
            public void payMoney(ChangePhoneFeeEntity changePhoneFeeEntity) {
                HashMap<String, Object> mHashMap = new HashMap<>(16);
                mHashMap.put(Constants.sceneType, Constants.TYPE_CHANGE_PHONE_FEE);
                mHashMap.put(Constants.mChangePhoneFeeEntity, changePhoneFeeEntity);
                ActivitySkipUtil.startAnotherActivity(CustomerInfoActivity.this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void addMoney() {
                AndroidUtils.jumpToAddMoneyActivity(CustomerInfoActivity.this);
            }
        });
        confirmPayDialog.showWithBottomAnim();
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity mTransferLimitEntity) {
        showConfirmPayDialog(mTransferLimitEntity);
    }

    @Override
    public void verifyEmailFail(String errorCode, String errorMsg) {
        if(enterEmailDialog != null) {
            enterEmailDialog.dismiss();
        }
        showTipDialog(errorMsg);
    }

    @Override
    public PersonInfoContract.Presenter getPresenter() {
        return new PersonInfoPresenter();
    }

    private void showDeactivationLoginDialog(){
        DeactivationLoginDialog deactivationLoginDialog = new DeactivationLoginDialog(mContext);
        deactivationLoginDialog.showWithBottomAnim();
    }

    private void showEmailVerifyDialog(String email) {
        enterEmailDialog = new EnterEmailDialog(mContext,email);
        enterEmailDialog.setContinueListener(new EnterEmailDialog.ContinueListener() {
            @Override
            public void clickContinue(String email) {
                mPresenter.verifyEmail(email);
            }
        });
        enterEmailDialog.showWithBottomAnim();
    }

    @OnClick({R.id.iv_back, R.id.iv_take_photo, R.id.iv_copy_bank, R.id.tv_stop_user,
            R.id.ll_personal_info_title, R.id.ll_address_title, R.id.ll_additional_details_title,
            R.id.ll_city_choose, R.id.ll_state_choose, R.id.tv_save, R.id.fl_source_of_fund,
            R.id.fl_profession, R.id.fl_company_type, R.id.fl_salary_range, R.id.con_male, R.id.con_female,
            R.id.tv_renew_id,R.id.fl_employment,R.id.tv_verify,R.id.tv_change_mobile})
    public void onViewClicked(View view) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        switch (view.getId()) {
            case R.id.tv_change_mobile:
                if(AndroidUtils.isCloseUse(this,Constants.change_mobile))return;
                if (isKyc(Constants.KYC_CHANGE_PHONE_NUMBER)) {
                    mPresenter.getChangePhoneFee();
                }
                break;
            case R.id.tv_verify:
                showEmailVerifyDialog(etEmailId.getText().toString());
                break;
            case R.id.tv_renew_id:
                mPresenter.renewId();
                break;
            case R.id.iv_back:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                finish();
                break;
            case R.id.iv_take_photo:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                showUploadDialog();
                break;
            case R.id.iv_copy_bank:
                if (ButtonUtils.isFastDoubleClick(-1, 2500)) {
                    return;
                }
                String bankNumber = tvBankNumber.getText().toString().trim();
                AndroidUtils.setClipboardText(mContext, bankNumber, true);
                showTipDialog(getString(R.string.iban_copy));
                break;
            case R.id.tv_stop_user:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (balanceMoney > 0) {
                    DeActNoticeDialog dialog = DeActNoticeDialog.getInstance();
                    dialog.setBackToHomeClickListener(new DeActNoticeDialog.OnBackToHomeClickListener() {
                        @Override
                        public void onBTHClick() {
                            ActivitySkipUtil.clearTaskToMainActivity(CustomerInfoActivity.this);
                        }
                    });
                    dialog.showNow(getSupportFragmentManager(), "DeActNotice");
                } else {
                    showDeactivationLoginDialog();
                }
                break;
            case R.id.ll_personal_info_title:
                llPersonalInfoContent.measure(0, 0);
                if (llPersonalInfoContent.getVisibility() == View.GONE) {
                    ViewAnimUtils.showViewWithAnim(llPersonalInfoContent, 300,
                            llPersonalInfoContent.getMeasuredHeight());
                } else {
                    ViewAnimUtils.hideViewWithAnim(llPersonalInfoContent, 300,
                            llPersonalInfoContent.getMeasuredHeight(), null);
                }
                break;
            case R.id.ll_address_title:
                llAddressContent.measure(0, 0);
                if (llAddressContent.getVisibility() == View.GONE) {
                    ViewAnimUtils.showViewWithAnim(llAddressContent, 300,
                            llAddressContent.getMeasuredHeight());
                } else {
                    ViewAnimUtils.hideViewWithAnim(llAddressContent, 300,
                            llAddressContent.getMeasuredHeight(), null);
                }
                break;
            case R.id.ll_additional_details_title:
                llAdditionalDetailsContent.measure(0, 0);
                if (llAdditionalDetailsContent.getVisibility() == View.GONE) {
                    ViewAnimUtils.showViewWithAnim(llAdditionalDetailsContent, 300,
                            llAdditionalDetailsContent.getMeasuredHeight());
                } else {
                    ViewAnimUtils.hideViewWithAnim(llAdditionalDetailsContent, 300,
                            llAdditionalDetailsContent.getMeasuredHeight(), null);
                }
                break;
            case R.id.ll_city_choose:
                if (TextUtils.isEmpty(userInfoEntity.statesId)) return;
                mPresenter.getCityList(userInfoEntity.statesId);
                break;
            case R.id.ll_state_choose:
                mPresenter.getStateList();
                break;
            case R.id.fl_source_of_fund:
                mPresenter.getSourceOfFund();
                break;
            case R.id.fl_employment:
                mPresenter.getEmploymentSector("EMPLOYMENT_SECTOR","");
                break;
            case R.id.fl_profession:
                if(!TextUtils.isEmpty(employmentCode)) {
                    mPresenter.getProfession("PROFESSION", employmentCode);
                }
                break;
            case R.id.fl_company_type:
                mPresenter.getCompanyType();
                break;
            case R.id.fl_salary_range:
                mPresenter.getSalaryRange();
                break;
            case R.id.tv_save:
                if (tvSave.isEnabled()) {
                    updateUserInfo();
//                    tvSave.setEnabled(false);
                }
                break;
//            case R.id.con_male:
//                if (userInfoEntity.isKyc() || "0".equals(userInfoEntity.gender)) {
//                    return;
//                }
//                userInfoEntity.gender = "0";
//                selectGender();
//                checkUpdateState();
//                break;
//            case R.id.con_female:
//                if (userInfoEntity.isKyc() || "1".equals(userInfoEntity.gender)) {
//                    return;
//                }
//                userInfoEntity.gender = "1";
//                selectGender();
//                checkUpdateState();
//                break;
            default:
                break;
        }
    }

    public void selectDatetime() {
        if (mSelectedDate == null) {
            selectedDate.setTime(endDate.getTime());
        } else {
            selectedDate.setTime(mSelectedDate);
        }
        TimePickerDialogUtils.showTimePicker(mContext, selectedDate, startDate, endDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedDate = date;
                etBirthday.setText(DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(date));
                if (!etBirthday.getText().toString().trim().equals(userInfoEntity.birthday)) {
                    userInfoEntity.birthday = etBirthday.getText().toString().trim();
                    checkUpdateState();
                }
            }
        });
    }

    private void showUploadDialog() {
        bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_bottom_avatar, null);
        LinearLayout llPhoto = contentView.findViewById(R.id.ll_take_photo);
        LinearLayout llFromAlbum = contentView.findViewById(R.id.ll_from_album);

        llPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    PermissionInstance.getInstance().getPermission(CustomerInfoActivity.this, new PermissionInstance.PermissionCallback() {
                        @Override
                        public void acceptPermission() {
                            openCameraCapture();
                        }

                        @Override
                        public void rejectPermission() {
                            showLackOfPermissionDialog(" ", getString(R.string.common_29));
                        }
                    }, PermissionInstance.getPermissons(1));
                }
            }
        });

        llFromAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    PermissionInstance.getInstance().getPermission(CustomerInfoActivity.this, new PermissionInstance.PermissionCallback() {
                        @Override
                        public void acceptPermission() {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/*");
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            startActivityForResult(intent, REQUEST_CODE_ALBUM);
                        }

                        @Override
                        public void rejectPermission() {
                            showLackOfPermissionDialog(" ", getString(R.string.common_32));
                        }
                    }, PermissionInstance.getPermissons(2));
                }
            }
        });


        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.height = AndroidUtils.dip2px(CustomerInfoActivity.this, 190);
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == REQUEST_CODE_ALBUM) {//从相册回来
            Uri imagePath = data.getData();
            if (imagePath != null) {
                CropImageActivity.startActivityForResult(this, imagePath);
            }
        } else if (data != null && resultCode == CropImageActivity.RESULT_CODE) {//从裁剪回来
            String avatarPath = data.getStringExtra(CropImageActivity.INTENT_EXTRA_IMAGE_PATH);
            if (!TextUtils.isEmpty(avatarPath)) {
                mPresenter.uploadAvatar(avatarPath);
                if (bottomDialog != null && bottomDialog.isShowing()) {
                    bottomDialog.dismiss();
                }
            }
            GcChatSDK.uploadFile(new File(avatarPath), new UploadFileCallback() {
                @Override
                public void onSuccessful(String profilePicFileId) {
                    SpUtils.getInstance().setProfilePicFileId(profilePicFileId);
                }

                @Override
                public void onFailure(int errorCode) {
                }
            });
        } else if (requestCode == REQUEST_CODE_CAMERA) {//从相机回来
            if (resultCode == Activity.RESULT_OK) {
                CropImageActivity.startActivityForResult(this, mImageSaveUri);
            } else {
                captureFailure();
            }
        }
    }

    private void captureFailure() {
        //如果取消拍照，就删除提前保存的uri
        getContentResolver().delete(mImageSaveUri, null, null);
    }

    private void openCameraCapture() {
        //打开相机拍照，指定文件名字
        try {
            String fileName = String.format(Locale.getDefault(), "capture_%d%s", System.currentTimeMillis() / 1000, ".jpg");
            ContentValues values = new ContentValues();
            long now = System.currentTimeMillis() / 1000;
            values.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, fileName);
            values.put(MediaStore.Images.ImageColumns.MIME_TYPE, "image/jpg");
            values.put(MediaStore.Images.ImageColumns.DATE_ADDED, now);
            values.put(MediaStore.Images.ImageColumns.DATE_MODIFIED, now);
            values.put(MediaStore.Images.ImageColumns.DATE_TAKEN, now);
            mImageSaveUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageSaveUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

}
