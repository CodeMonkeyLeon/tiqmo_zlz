package cn.swiftpass.wallet.tiqmo.module.addmoney.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.presenter.BindCardPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

public class AddNewCardActivity extends BaseCompatActivity<AddMoneyContract.AddCardPresenter> implements AddMoneyContract.AddCardView{
    private static final String TAG = AddNewCardActivity.class.getSimpleName();
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.iv_card)
    ImageView ivCard;
    @BindView(R.id.et_card_number)
    EditTextWithDel etCardNumber;
    @BindView(R.id.tv_error_card)
    TextView tvError;
    @BindView(R.id.tv_expiry_subtitle)
    TextView tvExpirySubtitle;
    @BindView(R.id.tv_expiry_date)
    TextView tvExpiryDate;
    @BindView(R.id.fl_expiry_date)
    FrameLayout flExpiryDate;
    @BindView(R.id.et_cvv)
    EditTextWithDel etCvv;
    @BindView(R.id.et_card_nickname)
    EditTextWithDel etCardNickname;
    @BindView(R.id.tv_add_new_card)
    TextView tvAddNewCard;
    @BindView(R.id.tv_delete_card)
    TextView tvDeleteCard;

    private String cardNo,cvv;
    private String validIDNotices = "";
    private Date mSelectedDate = null;

    String resourcePath = "0";

    private String addMoney;

    private String topupAmount, transferFee, wrongTransferFee, wrongVat, vat,
            totalAmount, orderCurrencyCode, protocolNo, custName, expire, showCardNo;
    //卡有效期月份
    private String month,year;
    private  String checkcredit;
    private String cardType;

    private String orderNo;
    private CardEntity mCardEntity;
    private RiskControlEntity mRiskControlEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_new_card;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.sprint11_34);
        fixedSoftKeyboardSliding();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        if (getIntent() != null && getIntent().getExtras() != null) {
            addMoney = getIntent().getExtras().getString(Constants.ADD_MONEY);
        }

        etCardNumber.getTlEdit().setHint(getString(R.string.sprint11_30));
        etCardNickname.getTlEdit().setHint(getString(R.string.sprint18_5));
        etCvv.getTlEdit().setHint(getString(R.string.DigitalCard_53));
        etCardNumber.setLineVisible(false);
        etCvv.setLineVisible(false);
        etCardNickname.setLineVisible(false);


        etCardNumber.addTextChangedListener(new TextWatcher() {
            char space = ' ';
            //改变之前text长度
            int beforeTextLength = 0;
            //改变之前的文字
            private CharSequence beforeChar;
            //改变之后text长度
            int onTextLength = 0;
            //是否改变空格或光标
            boolean isChanged = false;
            // 记录光标的位置
            int location = 0;
            private char[] tempChar;
            private StringBuffer buffer = new StringBuffer();
            //已有空格数量
            int spaceNumberB = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeTextLength = s.length();
                if (buffer.length() > 0) {
                    buffer.delete(0, buffer.length());
                }
                spaceNumberB = 0;
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == ' ') {
                        spaceNumberB++;
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onTextLength = s.length();
                buffer.append(s.toString());
                if (onTextLength == beforeTextLength || onTextLength <= 3
                        || isChanged) {
                    isChanged = false;
                    return;
                }
                isChanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvError.setVisibility(View.GONE);
                if (isChanged) {
                    location = etCardNumber.getEditText().getSelectionEnd();
                    int index = 0;
                    while (index < buffer.length()) {
                        if (buffer.charAt(index) == space) {
                            buffer.deleteCharAt(index);
                        } else {
                            index++;
                        }
                    }
                    index = 0;
                    int spaceNumberC = 0;
                    while (index < buffer.length()) {
                        if ((index == 4 || index == 9 || index == 14 || index == 19)) {
                            buffer.insert(index, space);
                            spaceNumberC++;
                        }
                        index++;
                    }

                    if (spaceNumberC > spaceNumberB) {
                        location += (spaceNumberC - spaceNumberB);
                    }

                    tempChar = new char[buffer.length()];
                    buffer.getChars(0, buffer.length(), tempChar, 0);
                    String str = buffer.toString();
                    if (location > str.length()) {
                        location = str.length();
                    } else if (location < 0) {
                        location = 0;
                    }

                    etCardNumber.getEditText().setText(str);
                    Editable etable = etCardNumber.getEditText().getText();
                    Selection.setSelection(etable, location);
                    isChanged = false;
                }
                checkButtonStatus();
            }
        });

        etCardNumber.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    //卡号输入框失去焦点则调用卡号校验接口
                    String number = etCardNumber.getEditText().getText().toString().trim();
                    if (number.contains(" ")) {
                        cardNo = number.replace(" ", "").trim().toString();
                        if (!TextUtils.isEmpty(cardNo) && cardNo.length() > 8) {
                            String resNum = cardNo.substring(0, 8);
                            mPresenter.verifyCard(cardNo);
                        }
                    } else {
                        etCardNumber.showErrorViewWithMsg("");
                    }
                    if(!TextUtils.isEmpty(cardNo) && cardNo.length() == 16){
                        etCardNumber.setContentText("**** **** ****"+cardNo.substring(12,16));
                    }
                }else{
                    AndroidUtils.showKeyboardView(etCardNumber.getEditText());
                    etCardNumber.setContentText(cardNo);
                }
            }
        });

        etCvv.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cvv = s.toString();
                checkButtonStatus();
            }
        });

        etCardNickname.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                custName = s.toString();
                checkButtonStatus();
            }
        });
    }

    @OnClick({R.id.iv_back,R.id.fl_expiry_date, R.id.tv_add_new_card})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fl_expiry_date:
                selectDatetime();
                break;
            case R.id.tv_add_new_card:
                mPresenter.bindCardPreOrder(custName, cardNo, expire, cvv, "R", AndroidUtils.getReqTransferMoney(addMoney),
                        UserInfoManager.getInstance().getCurrencyCode(),checkcredit);
                break;
            default:break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAlpha(mContext,1f);
    }

    public void selectDatetime() {
        GregorianCalendar startDate = new GregorianCalendar();
        GregorianCalendar endDate = new GregorianCalendar();
        startDate.set(Calendar.YEAR, startDate.get(Calendar.YEAR));
        startDate.set(Calendar.MONTH, startDate.get(Calendar.MONTH));
        endDate.set(Calendar.YEAR, endDate.get(Calendar.YEAR) + 50);

        GregorianCalendar selectedDate = new GregorianCalendar();
        if (mSelectedDate == null) {
            mSelectedDate = startDate.getTime();
            selectedDate.setTime(startDate.getTime());
        } else {
            selectedDate.setTime(mSelectedDate);
        }

        TimePickerDialogUtils.showTimePickerWithoutDay(mContext, selectedDate, startDate, endDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedDate = date;

//                SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
                SimpleDateFormat df = new SimpleDateFormat("MM/YY", Locale.ENGLISH);
                expire = df.format(date).replace("/","");
                refreshItemView(df.format(date), tvExpiryDate, tvExpirySubtitle, false);
            }
        });
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkButtonStatus();
                    }
                }
            });
        } else {
            content.setText(value);
            checkButtonStatus();

        }
    }

    private void checkButtonStatus() {
        String cardNo = etCardNumber.getEditText().getText().toString().trim().replace(" ","");
        tvAddNewCard.setEnabled(!TextUtils.isEmpty(cardNo) && cardNo.length()==16 && !TextUtils.isEmpty(expire) && !TextUtils.isEmpty(cvv) && cvv.length() == 3 && !TextUtils.isEmpty(custName)
        && !TextUtils.isEmpty(checkcredit));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void openAddMoney(TransferPayEntity transferPayEntity){
        mCardEntity = new CardEntity();
        mCardEntity.setCvv(cvv);
        mCardEntity.setCardNo(cardNo);
        mCardEntity.setExpire(expire);
        mCardEntity.setCustName(custName);
        mCardEntity.setCardOrganization(checkcredit);
        HashMap<String,Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.transferPayEntity, transferPayEntity);
        mHashMap.put(Constants.cardEntity, mCardEntity);
        mHashMap.put("fromAddCard", true);
        ActivitySkipUtil.startAnotherActivity(AddNewCardActivity.this, ConfirmAddMoneyActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        setAlpha(mContext,0.5f);
    }

    @Override
    public void bindCardPreOrderSuccess(TransferPayEntity transferPayEntity) {
        if(transferPayEntity != null){
            orderNo = transferPayEntity.getOrderNo();
            mRiskControlEntity = transferPayEntity.riskControlInfo;
            AppsFlyerHelper.getInstance().sendAddCardEvent();
            openAddMoney(transferPayEntity);
        }
    }

    @Override
    public void verifyCardSuccess(verifyCardEntity verifyCardEntity) {
        if(verifyCardEntity != null){
            String cardOrganization = verifyCardEntity.cardOrganization;
            if(!TextUtils.isEmpty(cardOrganization)) {
                checkcredit = cardOrganization.toUpperCase();
                ivCard.setImageResource(ThemeSourceUtils.getSourceID(mContext,AndroidUtils.getCardLogo(checkcredit)));
            }
            checkButtonStatus();
        }
    }

    @Override
    public void verifyCardFail(String errorCode, String errorMsg) {
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(errorMsg);
        checkButtonStatus();
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddMoneyContract.AddCardPresenter getPresenter() {
        return new BindCardPresenter();
    }

}
