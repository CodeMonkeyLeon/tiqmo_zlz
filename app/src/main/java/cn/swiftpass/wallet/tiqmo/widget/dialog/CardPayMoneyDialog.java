package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;

public class CardPayMoneyDialog extends BottomDialog {

    private Context mContext;

    private PayMoneyListener payMoneyListener;

    public interface PayMoneyListener {
        void payMoney(String money);
    }

    public void setPayMoneyListener(final PayMoneyListener payMoneyListener) {
        this.payMoneyListener = payMoneyListener;
    }

    public CardPayMoneyDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(context);
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_card_add_money, null);
        TextView tvAddMoney = mView.findViewById(R.id.tv_add_money);
        TextView tvCardBalance = mView.findViewById(R.id.tv_card_balance);

        String money = "15";
        tvCardBalance.setText(Spans.builder().text(money).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(24)
                .text(" " + LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(12)
                .build());

        tvCardBalance.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (payMoneyListener != null) {
                    payMoneyListener.payMoney(money);
                }
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
