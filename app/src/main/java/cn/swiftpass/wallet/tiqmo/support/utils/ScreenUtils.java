package cn.swiftpass.wallet.tiqmo.support.utils;

import android.os.Build;

public class ScreenUtils {

    /**
     * 判断是否是全面屏
     */
    public static boolean isFullScreenDevice() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }
}
