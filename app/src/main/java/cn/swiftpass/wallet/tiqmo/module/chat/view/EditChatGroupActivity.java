package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.gc.gcchat.GcChatUser;
import com.gc.gcchat.callback.AddUsersToGroupCallback;
import com.gc.gcchat.callback.GcChatSimpleCallback;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.chat.Contract.ChatContract;
import cn.swiftpass.wallet.tiqmo.module.chat.Presenter.ChatPresenter;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.GroupChatMemberAdapter;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatContactDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.NewGroupDialog;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.GcChatErrorUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ContactUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class EditChatGroupActivity extends BaseCompatActivity<ChatContract.ChatPresenter> implements ChatContract.ChatView {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    MyTextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.iv_take_photo)
    ImageView ivTakePhoto;
    @BindView(R.id.con_group_avatar)
    ConstraintLayout conGroupAvatar;
    @BindView(R.id.tv_name)
    MyTextView tvName;
    @BindView(R.id.tv_create_date)
    MyTextView tvCreateDate;
    @BindView(R.id.rl_group_member)
    MaxHeightRecyclerView rlGroupMember;
    @BindView(R.id.ll_add_member)
    LinearLayout llAddMember;
    @BindView(R.id.ll_leave_group)
    LinearLayout llLeaveGroup;
    @BindView(R.id.iv_admin_user)
    RoundedImageView ivAdminUser;
    @BindView(R.id.tv_admin_name)
    TextView tvAdminName;
    @BindView(R.id.tv_admin_phone)
    TextView tvAdminPhone;
    @BindView(R.id.con_admin)
    ConstraintLayout conAdmin;

    private GcChatSession gcChatSession;

    private GroupChatMemberAdapter groupChatMemberAdapter;

    private NewGroupDialog newGroupDialog;
    public boolean firstRejectContacts = true;
    //通讯录联系人List
    public List<KycContactEntity> phoneList = new ArrayList<>();
    private ChatContactDialog chatContactDialog;
    private List<KycContactEntity> contactInviteDtos;

    private GcChatUser createUser;

    private final List<GcChatParticipant> groupParticipantList = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_edit_chat_group;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, ivHeadCircle);
        userInfoEntity = getUserInfoEntity();
        if (getIntent() != null && getIntent().getExtras() != null) {
            gcChatSession = (GcChatSession) getIntent().getSerializableExtra(Constants.gcChatSession);
            if (gcChatSession != null) {
                groupParticipantList.addAll(gcChatSession.getParticipantUsers());
                tvName.setText(gcChatSession.getTitle());
                tvCreateDate.setText(getString(R.string.sprint20_55).replace("XXX", TimeUtils.getGroupCreateDay(gcChatSession.getCreatedAt())));
                createUser = gcChatSession.getCreatedByUser();
//                Collections.sort(groupParticipantList, new TimeComparator());
                if(createUser != null){
                    for (GcChatParticipant gcChatParticipant : groupParticipantList) {
                        if (createUser.getExternalId().equals(gcChatParticipant.getUser().getExternalId())) {
                            conAdmin.setVisibility(View.VISIBLE);
                            groupParticipantList.remove(gcChatParticipant);
                            break;
                        }
                    }
                    if(!createUser.getExternalId().equals(userInfoEntity.userId)){
                        llAddMember.setVisibility(View.GONE);
                    }
                    tvAdminName.setText(createUser.getName()+" ("+mContext.getString(R.string.sprint20_57)+")");
                    tvAdminPhone.setText(AndroidUtils.getPhone(createUser.getPhoneNumber()));
                    String displayPic = createUser.getFileId();
                    if (displayPic != null && !displayPic.isEmpty()) {
                        Glide.with(mContext).load(ChatHelper.getInstance().getChatUserAvatar(displayPic)).centerCrop().autoClone().into(ivAdminUser);
                    } else {
                        Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivAdminUser);
                    }
                }
            }
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_030b28_white)), AndroidUtils.dip2px(mContext, 0));
        manager.setOrientation(RecyclerView.VERTICAL);
        rlGroupMember.setLayoutManager(manager);
        groupChatMemberAdapter = new GroupChatMemberAdapter(groupParticipantList);
        groupChatMemberAdapter.setCreateUser(createUser);
        groupChatMemberAdapter.bindToRecyclerView(rlGroupMember);

        groupChatMemberAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {

            }
        });
        groupChatMemberAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                GcChatParticipant gcChatParticipant = (GcChatParticipant) baseRecyclerAdapter.getDataList().get(position);
                switch (view.getId()) {
                    case R.id.iv_delete_member:
                        if(gcChatParticipant != null){
                            showChatConfirmDialog(getString(R.string.sprint20_47).replace("XXX", gcChatParticipant.getUser().getName()),getString(R.string.common_6), getString(R.string.sprint20_50),
                                    "",
                                    new ChatConfirmDialog.ConfirmListener() {
                                        @Override
                                        public void clickLeft() {
                                            dissmissChatConfirmDialog();
                                        }

                                        @Override
                                        public void clickRight() {
                                            showProgressCancelable(true,true);
                                            GcChatSDK.removeParticipantFromGroup(gcChatParticipant.getId(), gcChatSession.getSessionId(), new GcChatSimpleCallback() {
                                                @Override
                                                public void onSuccessful() {
                                                    showProgress(false);
                                                    dissmissChatConfirmDialog();
                                                    ChatRoomActivity.refreshOnResume = true;
                                                    groupParticipantList.remove(position);
                                                    groupChatMemberAdapter.notifyDataSetChanged();
                                                }

                                                @Override
                                                public void onFailure(int errorCode) {
                                                    showProgress(false);
                                                    dissmissChatConfirmDialog();
                                                    showTipDialog("Problem in Removing User!! - "+ GcChatErrorUtils.getErrorMessage(errorCode));
                                                }
                                            });
                                        }
                                    });
                        }

                        break;
                    default:
                        break;
                }
            }
        });
    }

    public class TimeComparator implements Comparator<GcChatParticipant> {

        @Override
        public int compare(GcChatParticipant o1, GcChatParticipant o2) {
            return (int)(o1.getJoinDate() - o2.getJoinDate());
        }
    }

    @OnClick({R.id.iv_back, R.id.con_group_avatar, R.id.ll_add_member, R.id.ll_leave_group})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.con_group_avatar:
                break;
            case R.id.ll_add_member:
                checkPermission();
                break;
            case R.id.ll_leave_group:
                showChatConfirmDialog(getString(R.string.sprint20_48).replace("XXX",gcChatSession.getTitle()),getString(R.string.common_6), getString(R.string.sprint20_54),
                        getString(R.string.sprint20_49),
                        new ChatConfirmDialog.ConfirmListener() {
                            @Override
                            public void clickLeft() {
                                dissmissChatConfirmDialog();
                            }

                            @Override
                            public void clickRight() {
                                showProgressCancelable(true,true);
                                GcChatSDK.leaveGroup(gcChatSession.getSessionId(), new GcChatSimpleCallback() {
                                    @Override
                                    public void onSuccessful() {
                                        ChatRoomActivity.finishOnResume = true;
                                        showProgress(false);
                                        dissmissChatConfirmDialog();
                                        showTipDialog("You have left the group successfully!!");
                                        finish();
                                    }

                                    @Override
                                    public void onFailure(int errorCode) {
                                        showProgress(false);
                                        dissmissChatConfirmDialog();
                                        showTipDialog("Error in leaving group. Please try again after some time!! - "+ GcChatErrorUtils.getErrorMessage(errorCode));
                                    }
                                });
                            }
                        });
                break;
            default:
                break;
        }
    }

    private void showNewGroupDialog() {
        newGroupDialog = new NewGroupDialog(mContext);
        newGroupDialog.setNewGroupListener(new NewGroupDialog.NewGroupListener() {
            @Override
            public void createNewGroupChat(String groupName) {
                checkPermission();
                newGroupDialog.dismiss();
            }
        });

        newGroupDialog.showWithBottomAnim();
    }

    private void checkPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(EditChatGroupActivity.this, Manifest.permission.READ_CONTACTS)) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
            PermissionInstance.getInstance().getPermission(EditChatGroupActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                        @Override
                        public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                            phoneList.clear();
                            phoneList.addAll(contactEntities);
                            mPresenter.contactInvite(phoneList);
                        }
                    });
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(EditChatGroupActivity.this, Manifest.permission.READ_CONTACTS)) {
                        if (!firstRejectContacts) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_30));
                        } else {
                            firstRejectContacts = false;
                        }
                    }
                }
            }, Manifest.permission.READ_CONTACTS);
        } else {
            ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                @Override
                public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                    phoneList.clear();
                    phoneList.addAll(contactEntities);
                    mPresenter.contactInvite(phoneList);
                }
            });
        }
    }

    @Override
    public ChatContract.ChatPresenter getPresenter() {
        return new ChatPresenter();
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {
        if (contactInviteEntity != null) {
            contactInviteDtos = contactInviteEntity.getContactInviteDtos();

            for (int i = 0; i < contactInviteDtos.size(); i++) {
                KycContactEntity kycContactEntity = contactInviteDtos.get(i);
                for(int j = 0;j<groupParticipantList.size();j++){
                    GcChatParticipant gcChatParticipant = groupParticipantList.get(j);
                    if(kycContactEntity.getUserId().equals(gcChatParticipant.getUser().getExternalId())){
                        kycContactEntity.isChecked = true;
                    }
                }
                if (kycContactEntity.getContactsName() != null) {
                    kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                }
            }

            Collections.sort(contactInviteDtos, new registerComparator());
            showContactDialog(contactInviteDtos);
        }
    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    /**
     * 根据是否注册来排序
     */
    public class registerComparator implements Comparator<KycContactEntity> {

        @Override
        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getUserStatus().equals(Constants.unregistered) || o2.getUserStatus().equals(Constants.registered)) {
                return 1;
            } else if (o1.getUserStatus().equals(Constants.registered) || o2.getUserStatus().equals(Constants.unregistered)) {
                return -1;
            }
            return 0;
        }
    }

    private void showContactDialog(List<KycContactEntity> phoneList) {
        chatContactDialog = ChatContactDialog.getInstance(mContext, 3);
        chatContactDialog.setPhoneList(phoneList);

        chatContactDialog.setCreateNewGroupListener(new ChatContactDialog.CreateNewGroupListener() {
            @Override
            public void createNewGroup() {
            }
        });

        chatContactDialog.setOnAddClickListener(new ChatContactDialog.OnAddClickListener() {
            @Override
            public void getSplitList(List<KycContactEntity> peopleList) {
                //创建群聊
                checkAndCreateGroup(peopleList);
            }
        });

        chatContactDialog.setTvInviteClickListener(new ChatContactDialog.TvInviteClickListener() {
            @Override
            public void sendInvite(String phone) {

                try {
                    String msg = mContext.getString(R.string.sprint20_67);
                    String[] s = getUserInfoEntity().userName.split(" ");
                    String body = msg.replace("XXX", s[0]);
                    AndroidUtils.sendSmsWithBody(mContext, BuildConfig.DefaultInternationalAreaCode + phone, body);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
//
            }
        });

        chatContactDialog.showWithBottomAnim();
    }

    private void checkAndCreateGroup(List<KycContactEntity> peopleList) {
        ArrayList<String> memberList = new ArrayList<>();
        int size = peopleList.size();
        int currentSize = groupParticipantList.size();
        if(size+currentSize>99){
            showChatConfirmDialog(getString(R.string.sprint20_90), "", getString(R.string.common_7), getString(R.string.sprint20_91),
                    new ChatConfirmDialog.ConfirmListener() {
                        @Override
                        public void clickLeft() {

                        }

                        @Override
                        public void clickRight() {
                            dissmissChatConfirmDialog();
                        }
                    });
            return;
        }
        if(chatContactDialog != null){
            chatContactDialog.dismiss();
        }
        showProgressCancelable(true, true);
        for (int i = 0; i < size; i++) {
            KycContactEntity kycContactEntity = peopleList.get(i);
            String userId = kycContactEntity.getUserId();
            String name = kycContactEntity.getContactsName();
            boolean isNumberAlreadyAdded = false;
            for (GcChatParticipant gcChatParticipant : groupParticipantList) {
                if (userId.equals(gcChatParticipant.getUser().getExternalId())) {
                    isNumberAlreadyAdded = true;
                    break;
                }
            }
            if (!isNumberAlreadyAdded) {
                memberList.add(userId);
            } else {
//                showTipDialog("Member with number " + userId + " already added!!");
            }
        }
        GcChatSDK.addUsersToGroup(memberList, gcChatSession.getSessionId(), new AddUsersToGroupCallback() {
            @Override
            public void onSuccessful(@NotNull List<GcChatParticipant> newParticipant) {
                showProgress(false);
                ChatRoomActivity.refreshOnResume = true;
                groupParticipantList.addAll(newParticipant);
                groupChatMemberAdapter.notifyItemInserted(groupParticipantList.size() - 1);
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                showTipDialog("Problem in adding new user!! - " + GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }
}
