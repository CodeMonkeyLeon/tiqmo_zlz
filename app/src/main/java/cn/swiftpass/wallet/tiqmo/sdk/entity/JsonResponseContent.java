package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by 叶智星 on 2018年09月17日.
 * 每一个不曾起舞的日子，都是对生命的辜负。
 */
public class JsonResponseContent<T> {

    private T serviceBody;

    public T getServiceBody() {
        return serviceBody;
    }

    public void setServiceBody(T serviceBody) {
        this.serviceBody = serviceBody;
    }

}