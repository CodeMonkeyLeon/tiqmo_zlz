package cn.swiftpass.wallet.tiqmo.module.register.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;

public class NafathContract {

    public interface View extends BaseView<NafathContract.Presenter> {

        void getNafathResultSuccess(NafathResultEntity result);

        void getNafathUrlSuccess(NafathUrlEntity result);

        void getNafathUrlFail(String errorCode, String errorMsg);
        void getNafathResultFail(String errorCode, String errorMsg);
    }


    public interface Presenter extends BasePresenter<NafathContract.View> {
        void getNafathUrl(String callingCode, String phone, String recipientId,String sceneType);
        void getNafathResult(String callingCode, String phone,String hashedState,String recipientId,String random,String sceneType);
    }
}
