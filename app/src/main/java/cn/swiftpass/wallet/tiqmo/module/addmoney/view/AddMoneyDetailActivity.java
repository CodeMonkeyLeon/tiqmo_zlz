package cn.swiftpass.wallet.tiqmo.module.addmoney.view;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferFeePresenter;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class AddMoneyDetailActivity extends BaseCompatActivity<TransferContract.TransferFeePresenter> implements TransferContract.TransferFeeView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_front_card_image)
    ImageView ivFrontCardImage;
    @BindView(R.id.tv_card_num)
    TextView tvCardNum;
    @BindView(R.id.tv_card_holder_name)
    TextView tvCardHolderName;
    @BindView(R.id.tv_valid_thru)
    TextView tvValidThru;
    @BindView(R.id.ll_card_face_front)
    LinearLayout llCardFaceFront;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_topup_amount)
    TextView tvTopupAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_wrong_trans_money)
    TextView tvWrongTransMoney;
    @BindView(R.id.tv_trans_money)
    TextView tvTransMoney;
    @BindView(R.id.tv_wrong_vat_money)
    TextView tvWrongVatMoney;
    @BindView(R.id.tv_vat_money)
    TextView tvVatMoney;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.iv_visa)
    ImageView iv_visa;
    @BindView(R.id.iv_mastercard)
    ImageView iv_mastercard;
    @BindView(R.id.ll_transfer_vat)
    ConstraintLayout llTransferVat;
    @BindView(R.id.ll_transfer_fee)
    ConstraintLayout llTransferFee;

    private BottomDialog bottomDialog;

    private String addMoney;
    private boolean bindNewCard;
    private CardEntity cardEntity;
    HashMap<String, Object> mHashMaps = new HashMap<>();
    HashMap<String, Object> mHashMaps_back = new HashMap<>();
    private String topupAmount, transferFee, wrongTransferFee, wrongVat, vat,
            totalAmount, orderCurrencyCode, cvv, custName, cardNo, expire, showCardNo;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_money_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        tvTitle.setText(R.string.add_money_14);

        mHashMaps.put("424242", R.drawable.card_front_new);
        mHashMaps.put("454347", R.drawable.card_front_new);
        mHashMaps.put("543603", R.drawable.card_front_new);
        mHashMaps.put("222300", R.drawable.card_front_new);
        mHashMaps.put("519999", R.drawable.card_front_new);
        mHashMaps.put("500018", R.drawable.card_front_new);
        mHashMaps.put("465858", R.drawable.card_front_new);

        mHashMaps_back.put("424242", R.drawable.card_back_new);
        mHashMaps_back.put("454347", R.drawable.card_back_new);
        mHashMaps_back.put("543603", R.drawable.card_back_new);
        mHashMaps_back.put("222300", R.drawable.card_back_new);
        mHashMaps_back.put("519999", R.drawable.card_back_new);
        mHashMaps_back.put("500018", R.drawable.card_back_new);
        mHashMaps_back.put("465858", R.drawable.card_back_new);

        if (getIntent() != null && getIntent().getExtras() != null) {
            addMoney = getIntent().getExtras().getString(Constants.ADD_MONEY);
            cardEntity = (CardEntity) getIntent().getExtras().getSerializable(Constants.CARD_ENTITY);
            bindNewCard = getIntent().getExtras().getBoolean(Constants.ADD_MONEY_BIND_NEW_CARD);
            if (cardEntity != null) {
                cardNo = cardEntity.getCardNo();
                showCardNo = cardEntity.showCardNo;
                if (!TextUtils.isEmpty(showCardNo)) {
                    tvCardNum.setText(AndroidUtils.convertBankCardNoWithBlank(showCardNo));
                } else {
                    tvCardNum.setText(AndroidUtils.convertBankCardNoWithBlank(cardNo));
                }
                AndroidUtils.setCardLogoVisible(cardEntity.getCardNo(), iv_visa, iv_mastercard);
                custName = cardEntity.getCustName();
                expire = cardEntity.getExpire();
                cvv = cardEntity.getCvv();
                tvCardHolderName.setText(cardEntity.getCustName());
                int resId = 0;
                //本地有图先取本地的图
                if (mHashMaps.containsKey(cardEntity.getCardNo().substring(0, 6))) {
                    resId = (int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6));
                }
                if (resId != 0 && resId != -1) {
                    ivFrontCardImage.setImageResource(resId);
                } else {
                    if (cardEntity.getCardFace() != null && !TextUtils.isEmpty(cardEntity.getCardFace())) {
                        String card_url = (String) mHashMaps.get(cardEntity.getCardFace());
                        Glide.with(this)
                                .load(card_url)
                                .placeholder(resId)
                                .error(resId)
                                .into(ivFrontCardImage);
                    } else {
                        ivFrontCardImage.setImageResource(resId);
                    }
                }

                tvValidThru.setText(AndroidUtils.converExpireDate(cardEntity.getExpire()));
            }
        }
        tvWrongTransMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        mPresenter.getTransferFee(Constants.TYPE_ADDMONEY, AndroidUtils.getReqTransferMoney(addMoney), UserInfoManager.getInstance().getCurrencyCode());

        LocaleUtils.viewRotationY(this, ivBack, headCircle);
    }


    @OnClick({R.id.tv_cancel, R.id.tv_confirm, R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_cancel:
                ProjectApp.removeAddMoneyTask();
                finish();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                if (cardEntity != null) {
                    if (bindNewCard) {
                        mPresenter.addMoneyBindCard(custName, cardNo, expire, cvv, "R", topupAmount,
                                orderCurrencyCode, AndroidUtils.getReqTransferMoney(transferFee), AndroidUtils.getReqTransferMoney(vat),"","");
                    } else {
//                        mPresenter.transferPay(Constants.PAY_TYPE_CARD, topupAmount, orderCurrencyCode, "", cardEntity.getProtocolNo(),
//                                AndroidUtils.getReqTransferMoney(transferFee), AndroidUtils.getReqTransferMoney(vat));
                        showCardBackDialog(cardEntity);
                    }
                }
                break;
            default:
                break;
        }
    }

    private void showCardBackDialog(CardEntity cardEntity) {
        bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_card_cvv, null);
        ImageView iv_back_card_image = contentView.findViewById(R.id.iv_back_card_image);
        EditTextWithDel et_cvv_text = contentView.findViewById(R.id.et_cvv_text);
        TextView tv_submit = contentView.findViewById(R.id.tv_submit);
        LocaleUtils.viewRotationY(mContext, iv_back_card_image);

        String cardNum = cardEntity.getCardNo();
        int resId_back = (int) mHashMaps_back.get(cardNum.substring(0, 6));
        if (resId_back != 0 && resId_back != -1) {
            iv_back_card_image.setImageResource((int) mHashMaps_back.get(cardNum.substring(0, 6)));
        } else {
            if (cardEntity.getCardFaceBack() != null && !TextUtils.isEmpty(cardEntity.getCardFaceBack())) {
                Glide.with(this)
                        .load(cardEntity.getCardFaceBack())
                        .placeholder((int) mHashMaps_back.get(cardNum.substring(0, 6)))
                        .error((int) mHashMaps_back.get(cardNum.substring(0, 6)))
                        .into(iv_back_card_image);
            } else {
                iv_back_card_image.setImageResource((int) mHashMaps_back.get(cardNum.substring(0, 6)));
            }
        }
        et_cvv_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString()) && s.length() == 3) {
                    tv_submit.setEnabled(true);
                } else {
                    tv_submit.setEnabled(false);
                }
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                cvv = et_cvv_text.getEditText().getText().toString().trim();
                mPresenter.transferPay(Constants.PAY_TYPE_CARD, topupAmount, orderCurrencyCode, cvv, cardEntity.getProtocolNo(),
                        AndroidUtils.getReqTransferMoney(transferFee), AndroidUtils.getReqTransferMoney(vat),"");
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
            dialogWindow.setDimAmount(0f);
        }
        setAlpha(mContext, 0.7f);
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {
        if (transFeeEntity != null) {
            topupAmount = transFeeEntity.getOrderAmount();
            transferFee = transFeeEntity.getDiscountTransFees();
            wrongTransferFee = transFeeEntity.getTransFees();
            totalAmount = transFeeEntity.getTotalAmount();
            wrongVat = transFeeEntity.getVat();
            vat = transFeeEntity.getDiscountVat();
            orderCurrencyCode = transFeeEntity.orderCurrencyCode;
            if (!TextUtils.isEmpty(topupAmount)) {
                tvTopupAmount.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(topupAmount) + " ").color(getTextColor(R.attr.color_80white_090a15))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(getTextColor(R.attr.color_80white_090a15)).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(transferFee) && BigDecimalFormatUtils.compareBig(transferFee,"0")) {
                tvTransMoney.setText(Spans.builder()
                        .text(transferFee + " ").color(getTextColor(R.attr.color_80white_090a15)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(getTextColor(R.attr.color_80white_090a15)).size(10)
                        .build());
            }else{
                llTransferFee.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(vat) && BigDecimalFormatUtils.compareBig(vat,"0")) {
                tvVatMoney.setText(Spans.builder()
                        .text(vat + " ").color(getTextColor(R.attr.color_80white_090a15)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(getTextColor(R.attr.color_80white_090a15)).size(10)
                        .build());
            }else{
                llTransferVat.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(totalAmount)) {
                tvTotalAmount.setText(Spans.builder()
                        .text(totalAmount + " ").color(getTextColor(R.attr.color_white_090a15)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(getTextColor(R.attr.color_white_090a15)).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(wrongTransferFee)) {
                if (!wrongTransferFee.equals(transferFee)||transferFee.equals(0)) {
                    tvWrongTransMoney.setVisibility(View.GONE);
                }
                tvWrongTransMoney.setText(Spans.builder()
                        .text(wrongTransferFee + " ").color(getTextColor(R.attr.color_50white_95979d)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());
            }
            if (!TextUtils.isEmpty(wrongVat)) {
                if (!BigDecimalFormatUtils.compareBig(wrongVat,"0")) {
                    tvWrongVatMoney.setVisibility(View.GONE);
                }
                tvWrongVatMoney.setText(Spans.builder()
                        .text(wrongVat + " ").color(getTextColor(R.attr.color_50white_95979d)).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());
            }
        }
    }

    private int getTextColor(int attr){
        return getColor(ThemeSourceUtils.getSourceID(AddMoneyDetailActivity.this, attr));
    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void transferPaySuccess(TransferPayEntity transferPayEntity) {
        if (bottomDialog != null) {
            bottomDialog.dismiss();
        }
        if (transferPayEntity != null) {
            String authPage = transferPayEntity.getAuthPage();
            if (!TextUtils.isEmpty(authPage)) {
                //跳转到3ds验证页面
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.BIND_CARD_3DS, authPage);

                ActivitySkipUtil.startAnotherActivity(AddMoneyDetailActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                ProjectApp.removeAddMoneyTaskExcludeWeb();
            }
        }
    }

    @Override
    public void transferPayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void addMoneyBindCardSuccess(CardBind3DSEntity cardBind3DSEntity) {
        if (cardBind3DSEntity != null) {
            String authPage = cardBind3DSEntity.getRedirectUrl();
            if (!TextUtils.isEmpty(authPage)) {
                //跳转到3ds验证页面
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.BIND_CARD_3DS, authPage);

                ActivitySkipUtil.startAnotherActivity(AddMoneyDetailActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                ProjectApp.removeAddMoneyTaskExcludeWeb();
            }
        }
    }

    @Override
    public void addMoneyBindCardFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public TransferContract.TransferFeePresenter getPresenter() {
        return new TransferFeePresenter();
    }
}
