package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.TransferPdfEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.widget.PdfDownloadFile;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.DownloadUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;

public class ShowPdfActivity extends BaseCompatActivity implements DownloadFile.Listener {

    @Override
    protected int getLayoutID() {
        return R.layout.activity_show_pdf;
    }


    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.pdfView)
    WebView pdfView;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_share_pdf)
    TextView tvSharePdf;
    @BindView(R.id.tv_down_pdf)
    TextView tvDownPdf;

    RemotePDFViewPager remotePDFViewPager;

    private TransferPdfEntity transferPdfEntity;
    private String pdfUrl;
    private boolean isShare;
    private File pdfFile;

    /**
     * 1是 invoice发票  2是receipt收据
     */
    private int pdfType = 1;

    private PDFPagerAdapter pdfPagerAdapter;


    public static void startShowPdfActivity(Activity fromActivity,
                                            TransferPdfEntity transferPdfEntity) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(Constants.transferPdfEntity, transferPdfEntity);
        ActivitySkipUtil.startAnotherActivity(fromActivity, ShowPdfActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.DigitalCard_92);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        if (getIntent() != null && getIntent().getExtras() != null) {
            transferPdfEntity = (TransferPdfEntity) getIntent().getExtras().getSerializable(Constants.transferPdfEntity);
            if (transferPdfEntity != null) {
                pdfUrl = transferPdfEntity.url;
                pdfType = transferPdfEntity.pdfType;
                if(pdfType == 2){
                    tvTitle.setText(R.string.sprint11_70);
                }
                if (!TextUtils.isEmpty(pdfUrl)) {
                    final DownloadFile.Listener listener = this;
                    remotePDFViewPager = new RemotePDFViewPager(mContext,new PdfDownloadFile(mContext,new Handler(),listener), pdfUrl, listener);
                    remotePDFViewPager.setId(R.id.pdf_pager);

//                    remotePDFViewPager =
//                            new RemotePDFViewPager(mContext, pdfUrl, this);
//                    initWebView(pdfView,pdfUrl);
                }
            }
        }
        getNotifyPermission();
    }

//    private void initWebView(WebView pdfView, String pdfUrl) {
//        WebSettings settings = pdfView.getSettings();
//        settings.setSupportZoom(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setUseWideViewPort(true);
//        settings.setJavaScriptEnabled(true);  //开启JS支持
//        settings.setAllowContentAccess(true);
//        settings.setAppCacheEnabled(false);
//        settings.setBuiltInZoomControls(false);
//        /**
//         * 安全相关配置
//         */
//        settings.setAllowFileAccess(false);
//        settings.setAllowFileAccessFromFileURLs(false);
//        settings.setAllowUniversalAccessFromFileURLs(false);
//
//        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
//
//        pdfView.setWebChromeClient(mWebChromeClient);
//        pdfView.setWebViewClient(mWebViewClient);
//
//        if (pdfUrl.contains("pdf")) {
//            //打开pdf处理
//            String encode_url;
//            try {
//                encode_url = URLEncoder.encode(pdfUrl, "UTF-8"); //Url Convert to UTF-8
//                pdfView.loadUrl("http://docs.google.com/gview?embedded=true&url="
//                        + encode_url);
//            } catch (UnsupportedEncodingException e) {
//                LogUtils.d("errorMsg", "---"+e+"---");
//            }
//        } else {
//            pdfView.loadUrl(pdfUrl);
//        }
//    }

    private final WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            String url = pdfView.getUrl();
            if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(title) && url.contains(title)) {//防止title显示网站
                return;
            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress == 100) {
            } else {
            }
            super.onProgressChanged(view, newProgress);
        }
    };

    private final WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("http://") || url.startsWith("https://")) { //加载的url是http/https协议地址
                view.loadUrl(url);
                return false; //返回false表示此url默认由系统处理,url未加载完成，会继续往下走
            } else { //加载的url是自定义协议地址
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
                return true;
            }
        }
    };

    @OnClick({R.id.iv_back, R.id.tv_share_pdf, R.id.tv_down_pdf})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_share_pdf:
                isShare = true;
                downLoadFile();
                break;
            case R.id.tv_down_pdf:
                isShare = false;
                downLoadFile();
                break;
            default:
                break;
        }
    }

    private void downLoadFile(){
        if (!isGranted_(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            PermissionInstance.getInstance().getPermission(ShowPdfActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    downFile((pdfType == 1?"invoicePdfFile_":"receiptPdfFile_") + AndroidUtils.localImageStr(System.currentTimeMillis()), pdfUrl);
                }

                @Override
                public void rejectPermission() {

                }
            }, PermissionInstance.getPermissons(2));
        } else {
            downFile((pdfType == 1?"invoicePdfFile_":"receiptPdfFile_") + AndroidUtils.localImageStr(System.currentTimeMillis()), pdfUrl);
        }
    }

    /**
     * 下載文件
     *
     * @param name
     * @param urlString
     */
    public void downFile(String name, String urlString) {
        Log.d("info", "filename==" + name + ".pdf");
//        String destFileDir = ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + "pdfFile";
//        String destFileDir = ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator;
        String destFileDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator;
        showProgress(true);
        DownloadUtil.get().download(urlString, destFileDir, name + ".pdf", new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(File file) {
                showProgress(false);
                pdfFile = file;
                Log.v("info", "download success" + " " + destFileDir);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isShare) {
                            AndroidUtils.shareFile(mContext, pdfFile);
                        }
                        showTipDialog(getString(R.string.DigitalCard_105));
//                        showToast("download success"+" "+file.getAbsolutePath());
                    }
                });

                Looper.prepare();//增加部分
                Looper.loop();//增加部分
            }

            @Override
            public void onDownloading(int progress) {
                Log.v("info", "下载進度" + progress);
//                progressDialog.setProgress(progress);
            }

            @Override
            public void onDownloadFailed(Exception e) {
                showProgress(false);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast("download failed");
                    }
                });

                Log.d("info", "下载失败了 " + e.toString());
//                if (progressDialog != null && progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }

            }
        });
    }

    public void updateLayout() {
        rootView.removeAllViewsInLayout();
        rootView.addView(remotePDFViewPager,
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        try {
            pdfPagerAdapter = new PDFPagerAdapter(this, extractFileNameFromURL(url));
            remotePDFViewPager.setAdapter(pdfPagerAdapter);
            updateLayout();
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public String extractFileNameFromURL(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

    @Override
    public void onFailure(Exception e) {

    }

    @Override
    public void onProgressUpdate(int progress, int total) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pdfPagerAdapter != null) {
            pdfPagerAdapter.close();
        }
    }
}
