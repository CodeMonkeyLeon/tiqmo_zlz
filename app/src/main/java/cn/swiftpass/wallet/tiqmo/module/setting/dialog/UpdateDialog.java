package cn.swiftpass.wallet.tiqmo.module.setting.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AppManageEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class UpdateDialog extends BottomDialog {

    private UpdateListener updateListener;
    private static UpdateDialog updateDialog = null;

    public void setUpdateListener(UpdateListener updateListener) {
        this.updateListener = updateListener;
    }

    public interface UpdateListener {
        void update();

        void logout();
    }

    public UpdateDialog(Context context, AppManageEntity appManageEntity) {
        super(context);
        initViews(context);
    }

    public static UpdateDialog getInstance(Context context, AppManageEntity appManageEntity) {
        updateDialog = new UpdateDialog(context, appManageEntity);
        return updateDialog;
    }

    private void initViews(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_update, null);
        TextView tvUpdate = view.findViewById(R.id.tv_update);
        TextView tvLogout = view.findViewById(R.id.tv_log_out);

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updateListener != null) {
                    updateListener.update();
                }
            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updateListener != null) {
                    updateListener.logout();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (updateDialog != null) {
                    updateDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.width = AndroidUtils.getScreenWidth(context);
            layoutParams.height = AndroidUtils.getScreenHeight(context);

            //设置dialog沉浸式效果
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                dialogWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                dialogWindow.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                dialogWindow.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            }
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
