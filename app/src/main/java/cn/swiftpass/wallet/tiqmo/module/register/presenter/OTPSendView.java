package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;

public interface OTPSendView<V extends BasePresenter> extends BaseView<V> {
    void sendOTPFailed(String errorCode, String errorMsg);

    void sendOTPSuccess(RegisterOtpEntity response);


}
