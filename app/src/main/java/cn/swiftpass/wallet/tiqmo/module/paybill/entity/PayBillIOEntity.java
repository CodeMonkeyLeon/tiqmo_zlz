package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillIOEntity extends BaseEntity {

    public static final String TYPE_SELECT = "Select";

    public int ioId;
    //字段数据类型 目前已知道两种 Numeric，Alphanumeric
    public String datatype;
    //字段名称
    public String name;
    //字段描述
    public String description;

    public String value;

    public String errorMsg;

    public List<SelectOptionListEntity> selectOptionList;
}
