package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.zrq.spanbuilder.Spans;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;

public class MarketDetailsForRefCodeDia extends BaseDialogFragmentN implements View.OnClickListener {

    private TextView tv_active_name;
    private TextView tv_active_des;
    private TextView tv_conditions_name;
    private TextView tv_conditions_des1;
    private TextView tv_conditions_des2;
//    private TextView tv_conditions_des3;
//    private TextView tv_merchants;
    private ImageView iv_active_picture;
//    private RecyclerView rv_merchants_list;
//    private View v_market_line;
    private TextView tv_confirm_button;
    private TextView tv_no_activity_content;
    private LinearLayout ll_activity_content;

    private ReferralCodeDescEntity marketDetails;
    private OnConfirmClickListener confirmClickListener;

    public void setConfirmClickListener(OnConfirmClickListener confirmClickListener) {
        this.confirmClickListener = confirmClickListener;
    }

    public static MarketDetailsForRefCodeDia newInstance(ReferralCodeDescEntity marketDetails) {
        MarketDetailsForRefCodeDia fragment = new MarketDetailsForRefCodeDia();
        Bundle bundle = new Bundle();
        bundle.putSerializable("marketDetails", marketDetails);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_market_details_rel_code;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);

        if (getArguments() != null) {
            marketDetails = (ReferralCodeDescEntity) getArguments().getSerializable("marketDetails");
            showDetails();
        }
    }

    private void showDetails() {
        if (marketDetails == null) {
            return;
        }
        if (marketDetails.haveReferralActivityFlag.equals("Y")) {
            ll_activity_content.setVisibility(View.VISIBLE);
            tv_no_activity_content.setVisibility(View.GONE);
            refreshView(marketDetails);
        } else {
            ll_activity_content.setVisibility(View.GONE);
            tv_no_activity_content.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(marketDetails.activityPicUrl)) {
            Glide.with(mContext).clear(iv_active_picture);
            Glide.with(mContext)
                    .load(marketDetails.activityPicUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(iv_active_picture);
        }
    }

    private void initView(View parentView) {
        ll_activity_content = parentView.findViewById(R.id.ll_activity_content);
        tv_no_activity_content = parentView.findViewById(R.id.tv_no_activity_content);
        iv_active_picture = parentView.findViewById(R.id.vp_active_picture);
        tv_active_name = parentView.findViewById(R.id.tv_active_name);
        tv_active_des = parentView.findViewById(R.id.tv_active_des);
        tv_conditions_name = parentView.findViewById(R.id.tv_conditions_name);
        tv_conditions_des1 = parentView.findViewById(R.id.tv_conditions_des1);
        tv_conditions_des2 = parentView.findViewById(R.id.tv_conditions_des2);
        tv_confirm_button = parentView.findViewById(R.id.tv_confirm_button);

        tv_confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmClickListener != null) {
                    confirmClickListener.onConfirmClick();
                }
                dismiss();
            }
        });
    }

    private void refreshView(ReferralCodeDescEntity result) {
        if (result == null) {
            return;
        }
        tv_conditions_des1.setText(Spans.builder()
                .text("· ").size(18)
                .text(getString(R.string.Loyalty_6) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .text(isEmpty(result.activityStartTime) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_eb5c14_fa6400)))
                .text(getString(R.string.Loyalty_7) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .text(isEmpty(result.activityEndTime)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_eb5c14_fa6400)))
                .build());
        tv_conditions_des2.setText(Spans.builder()
                .text("· ").size(18)
                .text(getString(R.string.Loyalty_8) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .build());
        if (result.amountAndCurrencyInfo == null || result.amountAndCurrencyInfo.length == 0) {
            return;
        }
        String[] amount = checkArray(result.amountAndCurrencyInfo);
        tv_active_des.setText(Spans.builder()
                .text(getString(R.string.Loyalty_2) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .text(amount[0] + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_eb5c14_fc4f00)))
                .text(getString(R.string.Loyalty_3) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .text(amount[1] + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_eb5c14_fc4f00)))
                .text(getString(R.string.Loyalty_4)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                .build());
    }

    @Override
    public void onClick(View v) {

    }

    private String[] checkArray(String[] useReferralCodeDesc) {
        String[] temp;
        if (useReferralCodeDesc.length == 0) {
            temp = new String[]{" ", " ", " "};
        } else {
            temp = new String[3];
            for (int i = 0; i < 3; i++) {
                if (i < useReferralCodeDesc.length) {
                    temp[i] = isEmpty(useReferralCodeDesc[i]);
                } else {
                    temp[i] = " ";
                }
            }
        }
        return temp;
    }

    public interface OnConfirmClickListener {
        void onConfirmClick();
    }

    public String isEmpty(String s) {
        return TextUtils.isEmpty(s) ? " " : s;
    }
}
