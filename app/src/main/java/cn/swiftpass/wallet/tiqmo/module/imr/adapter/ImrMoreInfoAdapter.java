package cn.swiftpass.wallet.tiqmo.module.imr.adapter;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrComplianceEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrMoreInfoActivity;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class ImrMoreInfoAdapter extends BaseRecyclerAdapter<ImrComplianceEntity> {

    private Date mSelectedFromDate;
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedFromDate = Calendar.getInstance();
    Calendar startFromDate = Calendar.getInstance();
    Calendar endFromDate = Calendar.getInstance();

    public ImrMoreInfoAdapter(@Nullable List<ImrComplianceEntity> data) {
        super(R.layout.item_imr_more_info,data);
        startFromDate.set(Calendar.YEAR, selectedFromDate.get(Calendar.YEAR) - 90);
        endFromDate.set(Calendar.YEAR, selectedFromDate.get(Calendar.YEAR));
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, ImrComplianceEntity imrComplianceEntity, int position) {
        CustomizeEditText etMoreInfo = baseViewHolder.getView(R.id.et_more_info);
        FrameLayout flBirthday = baseViewHolder.getView(R.id.fl_birthday);
        TextView tvBirthday = baseViewHolder.getView(R.id.tv_birthday);
        TextView tvBirthdaySubtitle = baseViewHolder.getView(R.id.tv_birthday_subtitle);

        String paramType = imrComplianceEntity.paramType;
        String paramName = imrComplianceEntity.paramName;
        String paramLabel = imrComplianceEntity.paramLabel;
        int length = imrComplianceEntity.getParamLength();
        if("String".equals(paramType)){
            etMoreInfo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
            etMoreInfo.setHint(paramLabel);
            etMoreInfo.setVisibility(View.VISIBLE);
            flBirthday.setVisibility(View.GONE);
            etMoreInfo.setTag(position);
            etMoreInfo.addTextChangedListener(new TextSwitcher(baseViewHolder));
        }else if("Date".equals(paramType)){
            tvBirthdaySubtitle.setText(paramLabel);
            etMoreInfo.setVisibility(View.GONE);
            flBirthday.setVisibility(View.VISIBLE);
            flBirthday.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectDatetime(true,imrComplianceEntity,tvBirthday,tvBirthdaySubtitle,position);
                }
            });
        }else if("Alphanumeric".equals(paramType)){
            etMoreInfo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPACE)});
            etMoreInfo.setHint(paramLabel);
            etMoreInfo.setVisibility(View.VISIBLE);
            flBirthday.setVisibility(View.GONE);
            etMoreInfo.setTag(position);
            etMoreInfo.addTextChangedListener(new TextSwitcher(baseViewHolder));
        }
    }

    public void selectDatetime(boolean isGeo,ImrComplianceEntity imrComplianceEntity,TextView tvBirthday,TextView tvBirthdaySubtitle,int position) {
        if (mSelectedFromDate == null) {
            selectedFromDate.setTime(endFromDate.getTime());
        } else {
            selectedFromDate.setTime(mSelectedFromDate);
        }

        TimePickerDialogUtils.showTimePicker(mContext, selectedFromDate, startFromDate, endFromDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedFromDate = date;
                SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
                imrComplianceEntity.paramValue = df.format(date);
                ((ImrMoreInfoActivity) mContext).saveEditData(position, df.format(date));
                refreshItemView(imrComplianceEntity.paramValue, tvBirthday, tvBirthdaySubtitle, false);
            }
        });
    }


    class TextSwitcher implements TextWatcher {
        private BaseViewHolder mHolder;

        public TextSwitcher(BaseViewHolder mHolder) {
            this.mHolder = mHolder;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int position = (int) mHolder.getView(R.id.et_more_info).getTag();//取tag值
            String content = s.toString();
            ((ImrMoreInfoActivity) mContext).saveEditData(position, content);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                }
            });
        } else {
            content.setText(value);
        }
    }
}
