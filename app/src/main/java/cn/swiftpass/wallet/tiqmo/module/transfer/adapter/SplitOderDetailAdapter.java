package cn.swiftpass.wallet.tiqmo.module.transfer.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransactionListInfos;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SplitOderDetailAdapter extends BaseRecyclerAdapter<TransactionListInfos.SplitOderDetail> {

    private List<TransactionListInfos.SplitOderDetail> data;

    public SplitOderDetailAdapter(@Nullable List<TransactionListInfos.SplitOderDetail> data) {
        super(R.layout.item_send_receive_info, data);
        this.data = data;

    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, TransactionListInfos.SplitOderDetail item, int position) {
        RoundedImageView ivAvatar = holder.getView(R.id.iv_avatar);
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        int size = data.size();
        switch (item.getOrderStatus()){
            case "SUCCESS":
                ivAvatar.setBackgroundResource(R.drawable.l_split_detail_success);
                holder.setTextColor(R.id.tv_money,mContext.getColor(R.color.green2));
                holder.setTextColor(R.id.tv_currencyType,mContext.getColor(R.color.green2));
                break;
            case "PROCESS":
                helper.setBackgroundResourceByAttr(ivAvatar,R.attr.split_process);
                holder.setTextColor(R.id.tv_money,mContext.getColor(R.color.color_f1a13c));
                holder.setTextColor(R.id.tv_currencyType,mContext.getColor(R.color.color_f1a13c));
                break;
            case "REFUSED":
                helper.setBackgroundResourceByAttr(ivAvatar,R.attr.split_reject);
                holder.setTextColor(R.id.tv_money,mContext.getColor(R.color.red2));
                holder.setTextColor(R.id.tv_currencyType,mContext.getColor(R.color.red2));
                break;

        }
        holder.setText(R.id.tv_name,item.getOrderDesc());
        holder.setText(R.id.tv_time, item.getTradeTime());
        holder.setText(R.id.tv_money, AndroidUtils.getTransferMoney(item.getAmount()));
        holder.setText(R.id.tv_currencyType, LocaleUtils.getCurrencyCode(item.getOrderCurrency()));
    }
}
