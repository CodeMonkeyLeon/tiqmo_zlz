package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageCodeEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class ChooseLanguageAdapter extends BaseRecyclerAdapter<LanguageCodeEntity> {
    public ChooseLanguageAdapter(@Nullable List<LanguageCodeEntity> data) {
        super(R.layout.item_choose_language,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, LanguageCodeEntity languageCodeEntity, int position) {
        ImageView ivLanguage = baseViewHolder.getView(R.id.iv_language);
        ImageView ivArrow = baseViewHolder.getView(R.id.iv_arr_language);
        String name = languageCodeEntity.name;
        baseViewHolder.setText(R.id.tv_language,name);
        LocaleUtils.viewRotationY(mContext, ivArrow);
    }
}
