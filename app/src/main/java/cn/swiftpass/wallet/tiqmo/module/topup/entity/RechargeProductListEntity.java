package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeProductListEntity extends BaseEntity {
    //选择套餐的产品信息
    public List<ChooseProductListEntity> changeProductList = new ArrayList<>();
    //输入金额的产品信息
    public RechargeInputProductEntity inputProduct;

    public String logo;
    public String operatorName;

    public RechargeInputProductEntity getProductEntity(){
        RechargeInputProductEntity rechargeInputProductEntity = new RechargeInputProductEntity();
        rechargeInputProductEntity.serviceType = "RANGED_VALUE_RECHARGE";
        rechargeInputProductEntity.description = "Enter amount between 50 and 100 in multiple of 50";
        rechargeInputProductEntity.productId = "11210";
        rechargeInputProductEntity.destinationAmountIncrement = "50.00";
        rechargeInputProductEntity.title = "Custom Amount";
        rechargeInputProductEntity.productName = "Open_Range";
        rechargeInputProductEntity.destinationMaxAmount = "100.00";
        rechargeInputProductEntity.exchangeRate = "0.2834199340";
        rechargeInputProductEntity.destinationCurrencyCode = "USD";
        rechargeInputProductEntity.destinationMinAmount = "50.00";
        rechargeInputProductEntity.operatorId = "1506";
        rechargeInputProductEntity.productType = "Custom Amount";
        rechargeInputProductEntity.productDescription = "";
        return rechargeInputProductEntity;
    }
}
