package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RequestCenterEntity extends BaseEntity {

    /**
     * {receivedSex=1, subject=NOTIFY, receivedIcon=http://192.168.1.19:9092/cms/file/download?96601000861595319169217_1596786940463.jpg, respCode=000000, requestSex=1, msgId=MSG9660103015641598431712780, title=You received a request to transfer money., requestCallingCode=966, orderCurrencyCode=SAR, inAppTitle=You received a request to transfer 5 SAR to Sunny., respMsg=成功, orderAmount=500, requestName=Sunny Sunny, requestPhone=532345678, msgType=NOTIFY}
     */

    public String requestSubject;
    public String requestId;
    public String callingCode;
    public String requestUserName;
    public String requestPhone;
    public String requestSex;
    public String requestIcon;
    public String receiveUserName;
    public String receivePhone;
    public String receiveSex;
    public String receiveIcon;
    public String transferAmount;
    public String transferRemark;
    public String currencyCode;
    public String orderNo;
    public String exchangeRate;
    public String payMethod;

    public String chatUserId;
    public String chatPhone;
    public String groupName;
    public String groupInviter;
    public String chatUserAvatar;
}
