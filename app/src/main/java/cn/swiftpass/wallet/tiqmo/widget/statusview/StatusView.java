package cn.swiftpass.wallet.tiqmo.widget.statusview;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

/**
 * 页面状态view
 */
public class StatusView implements StatusInterface {

    public static final int CONTENT_VIEW = 1;
    public static final int LOADING_VIEW = 2;
    public static final int NO_CONTENT_VIEW = 3;
    public static final int NET_ERROR_VIEW = 4;
    public static final int OTHER_VIEW = 5;

    private Context context;
    //内容布局
    private View contentView;
    //加载中
    private View loadingView;
    //网络问题相关
    private View netErrorView;
    private String errorMsg;
    //无内容
    private View noContentView;
    private int noImageRes;
    private String noContent;
    //刷新按钮
    private StatusInterface.OnClickListener noContentListener;
    private StatusInterface.OnClickListener netErrorListener;

    private LayoutInflater inflater;

    //内容布局的父布局
    private ViewGroup parentView;
    //当前显示的view在父布局中的位置
    private int currentViewIndex;
    //内容布局属性
    private ViewGroup.LayoutParams params;

    private StatusView(Builder builder) {
        context = builder.context;
        contentView = builder.contentView;
        loadingView = builder.loadingView;
        netErrorView = builder.netErrorView;
        errorMsg = builder.netErrorMsg;
        noContent = builder.noContentMsg;
        noImageRes = builder.ivContentResId;
        noContentView = builder.noContentView;

        noContentListener = builder.noContentListener;
        netErrorListener = builder.netErrorListener;

        init();
    }

    private void init() {
        inflater = LayoutInflater.from(context);
        params = contentView.getLayoutParams();
        if (contentView.getParent() != null) {
            parentView = (ViewGroup) contentView.getParent();
        } else {
            parentView = contentView.getRootView().findViewById(android.R.id.content);
        }
        int count = parentView.getChildCount();
        for (int i = 0; i < count; i++) {
            if (contentView == parentView.getChildAt(i)) {
                currentViewIndex = i;
                break;
            }
        }
    }

    //显示布局
    private void showView(View view) {
        if (parentView.getChildAt(currentViewIndex) != view) {
            ViewGroup viewParent = (ViewGroup) view.getParent();
            if (viewParent != null) {
                viewParent.removeView(view);
            }
            parentView.removeViewAt(currentViewIndex);
            parentView.addView(view, currentViewIndex, params);
        }
    }

    @Override
    public void showLoading() {
        if (loadingView != null) {
            showView(loadingView);
            return;
        }
        loadingView = inflater.inflate(R.layout.view_kyc_loading, null);
        LinearLayout loading_view = loadingView.findViewById(R.id.loading_view);
//        ImageView ivKycLoading = loadingView.findViewById(R.id.iv_kyc_loading);
//        Glide.with(context).load(R.drawable.kyc_loading).diskCacheStrategy(DiskCacheStrategy.DATA).into(ivKycLoading);

        if (loading_view != null) {
            loading_view.setVisibility(View.VISIBLE);
        }

        showView(loadingView);
    }

    @Override
    public void showNetError() {
        if (netErrorView != null) {
            showView(netErrorView);
            return;
        }

        netErrorView = inflater.inflate(R.layout.view_network_error, null);
        LinearLayout netErrorLayout = netErrorView.findViewById(R.id.ll_no_network);
        ImageView iv_no_network = netErrorView.findViewById(R.id.iv_no_network);
        TextView tvNetwordContent = netErrorView.findViewById(R.id.tv_network_content);
        TextView tv_netword_title = netErrorView.findViewById(R.id.tv_network_title);
        TextView tvNetwordRetry = netErrorView.findViewById(R.id.tv_network_retry);
        if (TextUtils.isEmpty(errorMsg)) {
            errorMsg = context.getResources().getString(R.string.offline_2);
        }
        tvNetwordContent.setText(errorMsg);
        if (netErrorLayout != null) {
            netErrorLayout.setVisibility(View.VISIBLE);
        }

        if (netErrorListener != null) {
            tvNetwordRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    netErrorListener.onClick();
                }
            });
        }

        showView(netErrorView);
    }

    @Override
    public void showEmpty() {
        if (noContentView != null) {
            showView(noContentView);
            return;
        }
        noContentView = inflater.inflate(R.layout.view_no_content, null);
        ImageView iv_no_content = noContentView.findViewById(R.id.iv_no_content);
        TextView tv_no_content = noContentView.findViewById(R.id.tv_no_content);
        TextView tv_no_title = noContentView.findViewById(R.id.tv_no_title);
        TextView tvRetry = noContentView.findViewById(R.id.tv_retry);

        iv_no_content.setImageResource(noImageRes);
        tv_no_content.setText(context.getResources().getString(R.string.offline_2));
        if (!TextUtils.isEmpty(noContent)) {
            tv_no_content.setText(noContent);
        }
        if (noContentListener != null) {
            tvRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noContentListener.onClick();
                }
            });
        }

        showView(noContentView);
    }

    @Override
    public void dismissLoading() {
        showView(contentView);
    }

    public static class Builder {
        private Context context;
        private View contentView;
        private View loadingView;
        private View netErrorView;
        private View noContentView;

        private String netErrorMsg;
        private String noContentMsg;
        private int ivContentResId;
        private StatusInterface.OnClickListener netErrorListener;
        private StatusInterface.OnClickListener noContentListener;

        public Builder(Context context, View contentView) {
            this.context = context;
            this.contentView = contentView;
        }

        public Builder setIvContentResId(int ivContentResId) {
            this.ivContentResId = ivContentResId;
            return this;
        }

        public Builder setContext(Context context) {
            this.context = context;
            return this;
        }

        public Builder setLoadingView(View loadingView) {
            this.loadingView = loadingView;
            return this;
        }

        public Builder setNetErrorView(View netErrorView) {
            this.netErrorView = netErrorView;
            return this;
        }

        public Builder setNoContentView(View noContentView) {
            this.noContentView = noContentView;
            return this;
        }

        public Builder setNetErrorMsg(String netErrorMsg) {
            this.netErrorMsg = netErrorMsg;
            return this;
        }

        public Builder setNoContentMsg(String noContentMsg) {
            this.noContentMsg = noContentMsg;
            return this;
        }

        public Builder setNetErrorListener(OnClickListener netErrorListener) {
            this.netErrorListener = netErrorListener;
            return this;
        }

        public Builder setNoContentListener(OnClickListener noContentListener) {
            this.noContentListener = noContentListener;
            return this;
        }

        public StatusView build() {
            return new StatusView(this);
        }
    }
}
