package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillTypePresenter implements PayBillContract.BillTypePresenter {

    PayBillContract.BillTypeView billTypeView;

    @Override
    public void getPayBillTypeList(String countryCode,String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillTypeList(countryCode,channelCode, new LifecycleMVPResultCallback<PayBillTypeListEntity>(billTypeView, true) {
            @Override
            protected void onSuccess(PayBillTypeListEntity result) {
                billTypeView.getPayBillTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billTypeView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillTypeView billTypeView) {
        this.billTypeView = billTypeView;
    }

    @Override
    public void detachView() {
        this.billTypeView = null;
    }
}
