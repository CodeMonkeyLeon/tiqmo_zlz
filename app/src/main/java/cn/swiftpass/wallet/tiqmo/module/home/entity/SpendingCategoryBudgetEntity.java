package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 支出预算数据信息
 */
public class SpendingCategoryBudgetEntity extends BaseEntity {
    //是否设置预算的标识
    public boolean setBudgetFlag;
    //剩余预算金额
    public String leftSpendableAmount;
    //剩余预算币种
    public String leftSpendableTxnCurrency;
    //预算耗尽提醒
    public String budgetSpendingReminder = "";
}
