package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillerListAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.interfaces.OnBillerListItemClickListener;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillServicePayBillerListPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class BillServiceBillerListActivity extends BaseCompatActivity<PayBillContract.BillServiceBillerListPresenter> implements PayBillContract.BillServiceBillerListView {

    @Override
    protected int getLayoutID() {
        return R.layout.act_bill_service_biller_list;
    }


    public static final String TAG = "BillServiceBillerListActivity";


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_country_list)
    RecyclerView rvCountryList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.id_tv_item_title)
    TextView tvItemTitle;


    private PayBillerListAdapter payBillerListAdapter;

    private List<PayBillerEntity> showBillerList = new ArrayList<>();
    private List<PayBillerEntity> filterBillerList = new ArrayList<>();


    private String countryCode, billerType, billerDescription, channelCode;

    private String billerId, billerName, billerLogo;

    private String mTitle;


    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        if (getIntent() != null && getIntent().getExtras() != null) {
            countryCode = getIntent().getExtras().getString(Constants.paybill_countryCode);
            billerType = getIntent().getExtras().getString(Constants.paybill_billerType);
            billerDescription = getIntent().getExtras().getString(Constants.paybill_billerDescription);
            channelCode = getIntent().getExtras().getString(Constants.CHANNEL_CODE);

            mTitle = getIntent().getStringExtra(Constants.TITLE);
            tvTitle.setText(mTitle);
            tvItemTitle.setText(mTitle);
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvCountryList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvCountryList.setLayoutManager(manager);
        payBillerListAdapter = new PayBillerListAdapter(showBillerList);
        payBillerListAdapter.bindToRecyclerView(rvCountryList);


        payBillerListAdapter.setOnBillerListItemClickListener(new OnBillerListItemClickListener() {
            @Override
            public void onBillServiceBillerListItemClick(int position, PayBillerEntity item) {
                if (item != null) {
                    billerId = item.billerId;
                    billerName = item.billerName;
                    billerLogo = item.imgUrl;
                    mPresenter.getBillServiceIOList(billerId, billerId, channelCode);
                }
            }
        });

        mPresenter.getPayBillerList(countryCode, billerType, billerDescription, channelCode);
    }


    @OnClick({R.id.iv_back})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }


    @Override
    public void getPayBillerListSuccess(PayBillerListEntity result) {
        if (result != null) {
            showBillerList.clear();
            showBillerList.addAll(result.billerList);
            payBillerListAdapter.setDataList(showBillerList);
        }
    }

    @Override
    public void getBillServiceIOListSuccess(PayBillIOListEntity result) {
        if (result != null) {
            LogUtils.i(TAG, "获取到BillServiceIO数据 : " + AndroidUtils.jsonToString(result));
            PayBillFetchActivity.startPayBillFetchActivity(
                    BillServiceBillerListActivity.this,
                    result,
                    billerId,
                    billerName,
                    billerLogo,
                    channelCode);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillServiceBillerListPresenter getPresenter() {
        return new PayBillServicePayBillerListPresenter();
    }
}