package cn.swiftpass.wallet.tiqmo.sdk.net.api;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanBaseActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HomeAnalysisEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.BillServiceSearchResultEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListNewEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
import cn.swiftpass.wallet.tiqmo.sdk.net.RequestCall;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Headers;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Param;

public interface PaymentApi {

    @Headers({"Service-Id:3220"})
    RequestCall<JsonResponse<PayBillCountryListEntity>> getPayBillCountry();

    @Headers({"Service-Id:3221"})
    RequestCall<JsonResponse<PayBillTypeListNewEntity>> getPayBillCategory(@Param("countryCode") String countryCode,
                                                                           @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3221"})
    RequestCall<JsonResponse<PayBillTypeListEntity>> getPayBillTypeList(@Param("countryCode") String countryCode,
                                                                        @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3222"})
    RequestCall<JsonResponse<BillServiceSearchResultEntity>> searchBillService(@Param("searchBillerNameOrCodeOfLocalService") String searchBillerNameOrCodeOfLocalService,
                                                                               @Param("countryCode") String countryCode,
                                                                               @Param("channelCode") String channelCode);


    @Headers({"Service-Id:3222"})
    RequestCall<JsonResponse<PayBillerListEntity>> getPayBillerList(@Param("countryCode") String countryCode,
                                                                    @Param("billerType") String billerType,
                                                                    @Param("billerDescription") String billerDescription,
                                                                    @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3223"})
    RequestCall<JsonResponse<PayBillSkuListEntity>> getPayBillSkuList(@Param("billerId") String billerId,
                                                                      @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3224"})
    RequestCall<JsonResponse<PayBillIOListEntity>> getPayBillIOList(@Param("billerId") String billerId,
                                                                    @Param("sku") String sku);

    @Headers({"Service-Id:3224"})
    RequestCall<JsonResponse<PayBillIOListEntity>> getPayBillServiceIOList(@Param("billerId") String billerId,
                                                                           @Param("sku") String sku,
                                                                           @Param("channelCode") String channelCode);

//    @Headers({"Service-Id:3225"})
//    RequestCall<JsonResponse<PayBillDetailEntity>> getPayBillDetail(@Param("aliasName") String aliasName,
//                                                                    @Param("billerId") String billerId,
//                                                                    @Param("sku") String sku,
//                                                                    @Param("data") List<String> data);

    @Headers({"Service-Id:3225"})
    RequestCall<JsonResponse<PayBillDetailEntity>> getPayBillServiceDetail(@Param("aliasName") String aliasName,
                                                                           @Param("billerId") String billerId,
                                                                           @Param("sku") String sku,
                                                                           @Param("channelCode") String channelCode,
                                                                           @Param("data") List<String> data,
                                                                           @Param("paymentType") String paymentType);

    @Headers({"Service-Id:3226"})
    RequestCall<JsonResponse<PayBillOrderInfoEntity>> fetchBill(@Param("payAmount") String payAmount,
                                                                @Param("billAmount") String billAmount,
                                                                @Param("billProcessingFeePayAmount") String billProcessingFeePayAmount,
                                                                @Param("billProcessingFeeAmount") String billProcessingFeeAmount,
                                                                @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3227"})
    RequestCall<JsonResponse<PayBillOrderListEntity>> getBillOrderHistory(@Param("countryCodes") List<String> countryCodeList,
                                                                          @Param("domesticFlag") String domesticFlag);

    @Headers({"Service-Id:3227"})
    RequestCall<JsonResponse<PayBillOrderListEntity>> getHomeBillOrderList();

    @Headers({"Service-Id:3228"})
    RequestCall<JsonResponse<PayBillIOListEntity>> getPayBillOrderIOList(@Param("billerId") String billerId,
                                                                         @Param("sku") String sku,
                                                                         @Param("aliasName") String aliasName,
                                                                         @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3229"})
    RequestCall<JsonResponse<Void>> deletePayBillOrder(@Param("billerId") String billerId,
                                                       @Param("aliasName") String aliasName);

    @Headers({"Service-Id:3230"})
    RequestCall<JsonResponse<VoucherBrandListEntity>> getVoucherBrandList();

    @Headers({"Service-Id:3231"})
    RequestCall<JsonResponse<VoucherBrandListEntity>> getRecentVoucherBrandList();

    @Headers({"Service-Id:3120"})
    RequestCall<JsonResponse<HomeAnalysisEntity>> getHomeAnalysis();

    @Headers({"Service-Id:3119"})
    RequestCall<JsonResponse<SpendingDetailEntity>> getAnalysisDetail(@Param("periodType") String periodType,
                                                                      @Param("spendingAnalysisQueryDate") String spendingAnalysisQueryDate);

    @Headers({"Service-Id:3234"})
    RequestCall<JsonResponse<SpendingBudgetListEntity>> getSpendingBudgetList();

    @Headers({"Service-Id:3233"})
    RequestCall<JsonResponse<Void>> saveUserSpending(@Param("userSpendingBudgetSaveOrUpdateInfoList") List<SpendingBudgetEntity> userSpendingBudgetSaveOrUpdateInfoList,
                                                     @Param("deleteBudgetIdList") List<String> deleteBudgetIdList);

    @Headers({"Service-Id:3235"})
    RequestCall<JsonResponse<RechargeOrderInfoEntity>> rechargePreOrder(@Param("productId") String productId,
                                                                        @Param("operatorId") String operatorId,
                                                                        @Param("country") String country,
                                                                        @Param("mobileNumber") String mobileNumber,
                                                                        @Param("amount") String amount,
                                                                        @Param("targetAmount") String targetAmount,
                                                                        @Param("shortName") String shortName,
                                                                        @Param("operatorName") String operatorName);

    @Headers({"Service-Id:3236"})
    RequestCall<JsonResponse<RechargeCountryListEntity>> getRechargeCountryList();

    @Headers({"Service-Id:3237"})
    RequestCall<JsonResponse<RechargeOperatorListEntity>> getRechargeOperatorList(@Param("phoneNubmer") String phoneNubmer);

    @Headers({"Service-Id:3238"})
    RequestCall<JsonResponse<RechargeProductListEntity>> getRechargeProductList(@Param("operatorId") String operatorId);

    @Headers({"Service-Id:3239"})
    RequestCall<JsonResponse<RechargeOrderListEntity>> getRechargeOrderList();

    @Headers({"Service-Id:3240"})
    RequestCall<JsonResponse<Void>> saveRechargeOrder(@Param("countryCode") String countryCode,
                                                      @Param("userShortName") String userShortName,
                                                      @Param("operatorId") String operatorId,
                                                      @Param("operatorName") String operatorName,
                                                      @Param("callingCode") String callingCode,
                                                      @Param("phone") String phone);

    @Headers({"Service-Id:3241"})
    RequestCall<JsonResponse<RechargeOrderDetailEntity>> getRechargeOrderDetail(@Param("recordId") String recordId);

    @Headers({"Service-Id:3242"})
    RequestCall<JsonResponse<Void>> deleteRechargeOrder(@Param("recordId") String recordId);

    @Headers({"Service-Id:3271"})
    RequestCall<JsonResponse<RechargeOrderInfoEntity>> preCharityOrder(@Param("channelCode") String channelCode,
                                                                       @Param("payAmount") String payAmount,
                                                                       @Param("domainDonation") String domainDonation);

    @Headers({"Service-Id:3274"})
    RequestCall<JsonResponse<PayBillRefundEntity>> payBillRefundOrder(@Param("channelCode") String channelCode);

    @Headers({"Service-Id:3275"})
    RequestCall<JsonResponse<PayBillRefundListEntity>> getPayBillRefundList(@Param("billerId") String billerId,
            @Param("channelCode") String channelCode);

    @Headers({"Service-Id:3276"})
    RequestCall<JsonResponse<PayBillRefundEntity>> getPayBillRefundDetail(@Param("channelCode") String channelCode,
                                                                          @Param("id") String id);

    @Headers({"Service-Id:3277"})
    RequestCall<JsonResponse<IbanBaseActivityEntity>> getIbanActivityDetail();

    @Headers({"Service-Id:3257"})
    RequestCall<JsonResponse<TransferPayEntity>> bindCardPreOrder(@Param("custName") String custName,
                                                                  @Param("cardNo") String cardNo,
                                                                  @Param("expire") String expire,
                                                                  @Param("cvv") String cvv,
                                                                  @Param("type") String type,
                                                                  @Param("orderAmount") String orderAmount,
                                                                  @Param("orderCurrencyCode") String orderCurrencyCode,
                                                                  @Param("creditType") String creditType);

    @Headers({"Service-Id:3258"})
    RequestCall<JsonResponse<TransferPayEntity>> cardAddMoneyPreOrder(@Param("type") String type,
                                                                      @Param("orderAmount") String orderAmount,
                                                                      @Param("orderCurrencyCode") String orderCurrencyCode,
                                                                      @Param("protocolNo") String protocolNo);

    @Headers({"Service-Id:3259"})
    RequestCall<JsonResponse<CardBind3DSEntity>> bindCardConfirmPay(@Param("orderNo") String orderNo,
                                                                  @Param("cvv") String cvv,
                                                                  @Param("type") String type,
                                                                  @Param("shopperResultUrl") String shopperResultUrl,
                                                                  @Param("creditType") String creditType);


    @Headers({"Service-Id:3260"})
    RequestCall<JsonResponse<CardBind3DSEntity>> cardAddMoneyConfirmPay(@Param("orderNo") String orderNo,
                                                                        @Param("cvv") String cvv,
                                                                        @Param("type") String type,
                                                                        @Param("shopperResultUrl") String shopperResultUrl,
                                                                        @Param("creditType") String creditType);
}
