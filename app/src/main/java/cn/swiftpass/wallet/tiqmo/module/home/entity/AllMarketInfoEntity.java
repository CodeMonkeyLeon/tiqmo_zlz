package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;

public class AllMarketInfoEntity {

//    "partnerNo": "96601",
//            "marketActivityInfos": [
//    {
//        "activityPicUrl": "http://www.baidu.com/22.jpg",
//            "activityNo": "111"
//    },
//    {
//        "activityPicUrl": "http://www.baidu.com/22.jpg",
//            "activityNo": "222"
//    }
//            ]
    public String partnerNo;
    public ArrayList<MarketInfo> marketActivityInfos;

    public class MarketInfo {
        public String activityPicUrl;
        public String activityNo;
        public String referralCodeActivityFlag;
    }
}
