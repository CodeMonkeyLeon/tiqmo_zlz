package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class KycFailActivity extends BaseCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_ref_code)
    ImageView ivRefCode;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_content_two)
    TextView tvContentTwo;
    @BindView(R.id.tv_accept)
    TextView tvAccept;
    @BindView(R.id.ll_content)
    ConstraintLayout llContent;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_kyc_under_review;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
    }

    @OnClick(R.id.tv_accept)
    public void onViewClicked() {
        finish();
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        SpUtils.getInstance().setKycChecking(false);
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }
}
