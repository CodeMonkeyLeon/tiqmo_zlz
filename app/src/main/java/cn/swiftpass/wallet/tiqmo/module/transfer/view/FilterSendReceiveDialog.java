package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class FilterSendReceiveDialog extends BottomDialog {
    private Context mContext;
    private FilterEntity filterEntity = new FilterEntity();
    public List<String> typeList = new ArrayList<>();

    public FilterSendReceiveDialog(Context context, Bitmap bitmap) {
        super(context, bitmap);
        this.mContext = context;
        initViews(context);
    }

    private void initViews(Context context) {

    }

    public void setFilterEntity(FilterEntity filterEntity) {
        this.filterEntity = filterEntity;
        setFilter(filterEntity);
    }

    private void setFilter(FilterEntity filterEntity) {
        if (filterEntity != null) {
            typeList = filterEntity.paymentType;
        }
    }
}
