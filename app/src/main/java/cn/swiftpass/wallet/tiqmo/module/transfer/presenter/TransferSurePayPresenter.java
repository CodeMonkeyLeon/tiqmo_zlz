package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferPwdTimeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.PreVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class TransferSurePayPresenter implements TransferContract.TransferSurePayPresenter {

    TransferContract.TransferSurePayView transferSurePayView;

    @Override
    public void payChangePhoneFee(String totalAmount,String vat) {
        AppClient.getInstance().getUserManager().payChangePhoneFee(totalAmount,vat, new LifecycleMVPResultCallback<ChangePhoneResultEntity>(transferSurePayView,true) {
            @Override
            protected void onSuccess(ChangePhoneResultEntity result) {
                transferSurePayView.payChangePhoneFeeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardLimit(String txnType, String dailyLimitValue, String monthlyLimitValue, String proxyCardNo,String up) {
        AppClient.getInstance().getCardManager().setKsaCardLimit(txnType,dailyLimitValue, monthlyLimitValue, proxyCardNo,up, new LifecycleMVPResultCallback<Void>(transferSurePayView,true) {
            @Override
            protected void onSuccess(Void result) {
                transferSurePayView.setKsaCardLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaCardDetails(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaCardDetails(proxyCardNo, new LifecycleMVPResultCallback<CardDetailEntity>(transferSurePayView,true) {
            @Override
            protected void onSuccess(CardDetailEntity result) {
                transferSurePayView.getKsaCardDetailsSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardStatus(String status, String proxyCardNo, String reason,String reasonStatus) {
        AppClient.getInstance().getCardManager().setKsaCardStatus(status, proxyCardNo, reason,reasonStatus, new LifecycleMVPResultCallback<Void>(transferSurePayView,true) {
            @Override
            protected void onSuccess(Void result) {
                transferSurePayView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire) {
        AppClient.getInstance().getCardManager().activateCard(proxyCardNo, cardNo, cardExpire, new LifecycleMVPResultCallback<Void>(transferSurePayView,true) {
            @Override
            protected void onSuccess(Void result) {
                transferSurePayView.activateCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaCardPayResult(String vat, String totalAmount, OpenCardReqEntity appNiOpenCardReq) {
        AppClient.getInstance().getCardManager().getKsaCardPayResult(vat, totalAmount, appNiOpenCardReq, new LifecycleMVPResultCallback<KsaPayResultEntity>(transferSurePayView,true) {
            @Override
            protected void onSuccess(KsaPayResultEntity result) {
                transferSurePayView.getKsaCardPayResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(transferSurePayView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        transferSurePayView.transferSurePaySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        transferSurePayView.transferSurePayFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark, String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName) {
        AppClient.getInstance().transferToContact(payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, payerName, new LifecycleMVPResultCallback<TransferEntity>(transferSurePayView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        transferSurePayView.transferToContactSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        transferSurePayView.transferToContactFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType) {
        AppClient.getInstance().confirmPay(orderNo, paymentMethodNo, orderType, new LifecycleMVPResultCallback<TransferEntity>(transferSurePayView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                transferSurePayView.confirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.confirmPayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(transferSurePayView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                transferSurePayView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.checkOutFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void splitBill(String oldOrderNo, String receivableAmount, String currencyCode, String orderRemark, List<SplitBillPayerEntity> receiverInfoList) {
        AppClient.getInstance().splitBill(oldOrderNo, receivableAmount, currencyCode, orderRemark, receiverInfoList, new LifecycleMVPResultCallback<TransferEntity>(transferSurePayView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                transferSurePayView.splitBillSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.splitBillFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName, String nickName, String relationshipCode, String callingCode, String phone, String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace, String birthDate, String sex, String cityName, String districtName, String poBox, String buildingNo, String street, String idNo, String idExpiry, String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                                  String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,String channelPayeeId,String channelCode,String branchId) {
        AppClient.getInstance().imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag,receiptOrgName,
                receiptOrgBranchName,cityId,currencyCode,channelPayeeId,channelCode,branchId, new LifecycleMVPResultCallback<ImrAddBeneResultEntity>(transferSurePayView, true) {
                    @Override
                    protected void onSuccess(ImrAddBeneResultEntity result) {
                        transferSurePayView.imrAddBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        transferSurePayView.imrAddBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo) {
        AppClient.getInstance().saveImrBeneficiaryInfo(beneficiaryInfo.payeeInfoId,
                beneficiaryInfo.transferDestinationCountryCode, beneficiaryInfo.receiptMethod,
                beneficiaryInfo.receiptOrgCode, beneficiaryInfo.receiptOrgBranchCode,
                beneficiaryInfo.payeeFullName, beneficiaryInfo.nickName, beneficiaryInfo.relationshipCode,
                beneficiaryInfo.callingCode, beneficiaryInfo.phone, beneficiaryInfo.payeeInfoCountryCode,
                beneficiaryInfo.birthPlace, beneficiaryInfo.birthDate, beneficiaryInfo.sex,
                beneficiaryInfo.cityName, beneficiaryInfo.districtName, beneficiaryInfo.poBox,
                beneficiaryInfo.buildingNo, beneficiaryInfo.street, beneficiaryInfo.idNo,
                beneficiaryInfo.idExpiry, beneficiaryInfo.bankAccountType, beneficiaryInfo.ibanNo,
                beneficiaryInfo.bankAccountNo,beneficiaryInfo.receiptOrgName,beneficiaryInfo.receiptOrgBranchName,
                beneficiaryInfo.cityId,beneficiaryInfo.currencyCode,beneficiaryInfo.channelPayeeId,beneficiaryInfo.channelCode,beneficiaryInfo.branchId,
                new LifecycleMVPResultCallback<ResponseEntity>(transferSurePayView, true) {
                    @Override
                    protected void onSuccess(ResponseEntity result) {
                        transferSurePayView.imrEditBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        transferSurePayView.imrEditBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }


    @Override
    public void attachView(TransferContract.TransferSurePayView transferSurePayView) {
        this.transferSurePayView = transferSurePayView;
    }

    @Override
    public void detachView() {
        this.transferSurePayView = null;
    }

    @Override
    public void checkTouchIDPaymentStateFromServer(String type) {
        AppClient.getInstance().getBiometricManager().preVerify(type, new LifecycleMVPResultCallback<PreVerifyEntity>(transferSurePayView) {
            @Override
            protected void onSuccess(PreVerifyEntity result) {
                transferSurePayView.switchTouchIDPayment(true);
            }

            @Override
            protected void onFail(String code, String error) {
                if (ResponseCallbackWrapper.RESPONSE_CODE_BIOMETRIC_NOT_MATCH.equals(code)) {
                    AppClient.getInstance().getBiometricManager().unregisterFingerprintPayment();
                    transferSurePayView.switchTouchIDPayment(false);
                } else {
                    transferSurePayView.switchTouchIDPayment(true);
                }
            }
        });
    }

    @Override
    public void setEnableTouchIDLogin(boolean isEnable) {
        if (isEnable) {
            AppClient.getInstance().getBiometricManager().registerFingerprintPayment(new LifecycleMVPResultCallback<Void>(transferSurePayView) {
                @Override
                protected void onSuccess(Void result) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(true);
                }

                @Override
                protected void onFail(String code, String error) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(false);
                }
            });
        } else {
            if (AndroidUtils.isOpenBiometricLogin() && !AndroidUtils.isOpenBiometricPay()) {
                AppClient.getInstance().getBiometricManager().unregisterFingerprintPayment();
            }
            AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(false);
        }
    }

    @Override
    public void setEnableTouchIDPayment(boolean isEnable) {
        if (isEnable) {
            AppClient.getInstance().getBiometricManager().registerFingerprintPayment(new LifecycleMVPResultCallback<Void>(transferSurePayView) {
                @Override
                protected void onSuccess(Void result) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(true);
                    transferSurePayView.switchTouchIDPayment(true);
                }

                @Override
                protected void onFail(String code, String error) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(false);
                    transferSurePayView.switchTouchIDPayment(false);
                }
            });
        } else {
            if (!AndroidUtils.isOpenBiometricLogin() && AndroidUtils.isOpenBiometricPay()) {
                AppClient.getInstance().getBiometricManager().unregisterFingerprintPayment();
            }
            AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(false);
        }
    }

    @Override
    public void checkPayPassword(String pdType, String sceneType, @Nullable String oldPassword,String orderNo) {
        AppClient.getInstance().checkTransferPwd(pdType, sceneType, oldPassword,orderNo, new LifecycleMVPResultCallback<TransferPwdTimeEntity>(transferSurePayView, true) {
            @Override
            protected void onSuccess(TransferPwdTimeEntity result) {
                transferSurePayView.checkPayPwdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferSurePayView.checkPayPwdFail(errorCode, errorMsg);
            }
        });
    }
}
