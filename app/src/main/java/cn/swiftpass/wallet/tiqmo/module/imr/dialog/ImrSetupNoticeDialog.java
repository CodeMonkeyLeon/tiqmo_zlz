package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrSetupActivity;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;

public class ImrSetupNoticeDialog extends BaseDialogFragmentN {

    TextView tvNoticeContent;
    TextView continueButton;

    public ImrSetupNoticeDialog() {

    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_imr_check;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
        initData();
    }

    private void initData() {
    }

    private void initView(View parentView) {
        tvNoticeContent = parentView.findViewById(R.id.tv_imr_setup_notice_content);
        continueButton = parentView.findViewById(R.id.tv_imr_setup_notice_continue);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivitySkipUtil.startAnotherActivity(mContext, ImrSetupActivity.class);
                ImrSetupNoticeDialog.this.dismiss();
            }
        });
    }
}
