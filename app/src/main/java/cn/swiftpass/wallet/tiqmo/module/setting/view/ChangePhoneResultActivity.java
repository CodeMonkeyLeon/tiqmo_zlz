package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class ChangePhoneResultActivity extends BaseCompatActivity {

    public static final int status_renew_id_success = 3;
    public static final int status_renew_id_fail = 4;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_result_image)
    ImageView ivResultImage;
    @BindView(R.id.tv_result_content)
    TextView tvResultContent;
    @BindView(R.id.tv_result_desc)
    TextView tvResultDesc;
    @BindView(R.id.tv_net)
    TextView tvNet;
    @BindView(R.id.tv_okay)
    TextView tvOkay;

    private int status;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_change_phone_result;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        ivBack.setVisibility(View.GONE);
        if (getIntent().getExtras() != null) {
            status = getIntent().getExtras().getInt(Constants.changePhoneResult);
            switch (status) {
                case status_renew_id_success:
                    tvNet.setVisibility(View.GONE);
                    ivResultImage.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_renew_id_success));
                    tvResultContent.setText(getString(R.string.RENEW_3));
                    tvResultDesc.setText(getString(R.string.RENEW_4));
                    break;
                case status_renew_id_fail:
                    tvNet.setVisibility(View.GONE);
                    ivResultImage.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_renew_id_fail));
                    tvResultContent.setText(getString(R.string.RENEW_5));
                    tvResultDesc.setText(getString(R.string.RENEW_6));
                    break;
                default:
                    break;
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_okay})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMaps = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_okay:
                switch (status) {
                    case status_renew_id_success:
                        finish();
                        break;
                    case status_renew_id_fail:
                        finish();
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}
