package cn.swiftpass.wallet.tiqmo.sdk.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * Created by aijingya on 2020/7/11.
 *
 * @Package cn.swiftpass.wallet.tiqmo.sdk.entity
 * @Description:
 * @date 2020/7/11.14:33.
 */
public class CardBind3DSEntity extends BaseEntity {
    private String redirectUrl;
    //hyper pay 通道 checkoutId
    public String checkoutId;

    public String orderNo;
    public String protocolNo;
    public String creditType;
    public String paymentId;

    public RiskControlEntity riskControlEntity;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}
