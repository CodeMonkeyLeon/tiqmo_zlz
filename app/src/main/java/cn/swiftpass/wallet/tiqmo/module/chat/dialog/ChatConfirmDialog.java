package cn.swiftpass.wallet.tiqmo.module.chat.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ChatConfirmDialog extends BottomDialog {
    private Context mContext;

    private ConfirmListener confirmListener;
    private TextView tvMessage;
    private TextView tvLeft;
    private TextView tvRight;
    private TextView tvTitle;
    private TextView tvMiddle;

    public void setMessage(String message){
        if(tvMessage != null){
            if(!TextUtils.isEmpty(message)) {
                tvMessage.setVisibility(View.VISIBLE);
                tvMessage.setText(message);
            }else {
                tvMessage.setVisibility(View.GONE);
            }
        }
    }

    public void setLeft(String left){
        if(tvLeft != null){
            if(!TextUtils.isEmpty(left)) {
                tvLeft.setVisibility(View.VISIBLE);
                tvLeft.setText(left);
            }else{
                tvLeft.setVisibility(View.GONE);
                tvMiddle.setVisibility(View.GONE);
            }
        }
    }

    public void setRight(String right){
        if(tvRight != null){
            if(!TextUtils.isEmpty(right)) {
                tvRight.setVisibility(View.VISIBLE);
                tvRight.setText(right);
            }else{
                tvRight.setVisibility(View.GONE);
                tvMiddle.setVisibility(View.GONE);
            }
        }
    }
    public void setTitle(String title){
        if(tvTitle != null){
            if(!TextUtils.isEmpty(title)) {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(title);
            }
        }
    }

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public interface ConfirmListener {
        void clickLeft();
        void clickRight();
    }

    public ChatConfirmDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_chat_confirm, null);
        tvLeft = view.findViewById(R.id.tv_left);
        tvRight = view.findViewById(R.id.tv_right);
        tvMessage = view.findViewById(R.id.tv_message);
        tvTitle = view.findViewById(R.id.tv_title);
        tvMiddle = view.findViewById(R.id.tv_middle);

        tvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickLeft();
                }
            }
        });

        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickRight();
                }
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
