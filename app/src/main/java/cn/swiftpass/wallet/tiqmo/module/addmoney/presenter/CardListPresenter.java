package cn.swiftpass.wallet.tiqmo.module.addmoney.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class CardListPresenter implements AddMoneyContract.CardListPresenter {

    AddMoneyContract.CardListView mCardListView;

    @Override
    public void getCardList(String cardType) {
        AppClient.getInstance().getCardManager().getBankCardList(cardType,new LifecycleMVPResultCallback<AllCardEntity>(mCardListView,true,false) {
            @Override
            protected void onSuccess(AllCardEntity result) {
                mCardListView.getCardListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void bindNewCard(String cardHolderName,String cardNum, String expireDate,String CVV,String type) {
        AppClient.getInstance().getCardManager().bindCard(cardHolderName, cardNum, expireDate, CVV, type, new LifecycleMVPResultCallback<CardBind3DSEntity>(mCardListView,true,false) {
            @Override
            protected void onSuccess(CardBind3DSEntity result) {
                mCardListView.bindNewCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void unbindCard(String protocolNo) {
        AppClient.getInstance().getCardManager().unBindCard(protocolNo, new LifecycleMVPResultCallback<unBindCardEntity>(mCardListView,true,false) {
            @Override
            protected void onSuccess(unBindCardEntity result) {
                mCardListView.unbindCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void cardAddMoneyPreOrder(String type, String orderAmount, String orderCurrencyCode, String protocolNo) {
        AppClient.getInstance().getTransferManager().cardAddMoneyPreOrder(type, orderAmount, orderCurrencyCode,protocolNo, new LifecycleMVPResultCallback<TransferPayEntity>(mCardListView,true) {
            @Override
            public void onSuccess(TransferPayEntity response) {
                mCardListView.cardAddMoneyPreOrderSuccess(response);
            }

            @Override
            public void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddMoneyContract.CardListView cardListView) {
        this.mCardListView = cardListView;
    }

    @Override
    public void detachView() {
        this.mCardListView = null;
    }
}
