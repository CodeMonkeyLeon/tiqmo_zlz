package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ImrEditBeneficiarySuccessDialog extends BottomDialog {

    private OnDialogClickListener listener;

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        this.listener = listener;
    }

    private ImrEditBeneficiarySuccessDialog(Context context, ImrBeneficiaryDetails.ImrBeneficiaryDetail data) {
        super(context);
        initViews(context, data);
    }

    public static ImrEditBeneficiarySuccessDialog getInstance(Context context, ImrBeneficiaryDetails.ImrBeneficiaryDetail data) {
        return new ImrEditBeneficiarySuccessDialog(context, data);
    }
    private void initViews(Context mContext, ImrBeneficiaryDetails.ImrBeneficiaryDetail imrBeneficiaryEntity) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_imr_remove_beneficiary, null);
        TextView name = view.findViewById(R.id.tv_remove_beneficiary_name);
        ImageView flag = view.findViewById(R.id.iv_remove_beneficiary_flag);
        TextView country = view.findViewById(R.id.tv_remove_beneficiary_country);
        ImageView genderAvatar = view.findViewById(R.id.iv_remove_gender_avatar);
        TextView removeBeneficiary = view.findViewById(R.id.tv_imr_remove_beneficiary);
//        TextView currency = view.findViewById(R.id.tv_remove_beneficiary_currency);
        removeBeneficiary.setText(R.string.IMR_19_1);
        TextView title = view.findViewById(R.id.tv_imr_setup_notice_main_title);
        TextView notice = view.findViewById(R.id.tv_imr_remove_notice);
        ImageView ivImrRemove = view.findViewById(R.id.iv_imr_remove);
        ivImrRemove.setVisibility(View.GONE);
        title.setText(R.string.IMR_95);
        notice.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(imrBeneficiaryEntity.transferDestinationCountryName)) {
            country.setText(imrBeneficiaryEntity.transferDestinationCountryName);
        }
//        if (!TextUtils.isEmpty(imrBeneficiaryEntity.countrySupportedCurrencyName)) {
//            currency.setText(imrBeneficiaryEntity.countrySupportedCurrencyName);
//        }
        if (!TextUtils.isEmpty(imrBeneficiaryEntity.nickName)) {
            name.setText(imrBeneficiaryEntity.nickName);
        } else {
            name.setText(imrBeneficiaryEntity.payeeFullName);
        }
        genderAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, imrBeneficiaryEntity.sex));
        if (!TextUtils.isEmpty(imrBeneficiaryEntity.countryLogo)) {
            Glide.with(mContext).clear(flag);
            Glide.with(mContext)
                    .load(imrBeneficiaryEntity.countryLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(flag);
        }

        removeBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener !=  null) {
                    listener.onRemoveClick();
                }
                dismiss();
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (listener !=  null) {
                    listener.onCancelClick();
                }
            }
        });
        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    public interface OnDialogClickListener {
        void onRemoveClick();
        void onCancelClick();
    }
}
