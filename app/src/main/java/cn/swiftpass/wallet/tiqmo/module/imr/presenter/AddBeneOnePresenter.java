package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AddBeneOnePresenter implements AddBeneficiaryContract.AddOnePresenter {

    private AddBeneficiaryContract.AddOneView addOneView;

    @Override
    public void getDestination(String countryType,String countryCode) {
        AppClient.getInstance().imrCountryList(countryType,countryCode, new LifecycleMVPResultCallback<ImrCountryListEntity>(addOneView, true) {
            @Override
            protected void onSuccess(ImrCountryListEntity result) {
                addOneView.getDestinationSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addOneView.getDestinationFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddBeneficiaryContract.AddOneView addFourView) {
        this.addOneView = addFourView;
    }

    @Override
    public void detachView() {
        this.addOneView = null;
    }
}
