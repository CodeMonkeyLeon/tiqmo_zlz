package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterPhoneCheckContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RegNafathUrlPresenter implements RegisterPhoneCheckContract.RegisterNafathPresenter {

    private RegisterPhoneCheckContract.RegisterNafathView baseView;

    @Override
    public void getNafathUrl(String callingCode, String phone, String recipientId,String sceneType) {
        AppClient.getInstance().getUserManager().getNafathUrl(callingCode, phone, recipientId,sceneType, new LifecycleMVPResultCallback<NafathUrlEntity>(baseView,true) {
            @Override
            protected void onSuccess(NafathUrlEntity result) {
                baseView.getNafathUrlSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.getNafathUrlFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void checkPhoneReg(String callingCode, String phone, double longitude, double latitude, String type, String checkIdNumber, String birthday,String needCheckIdNumber) {
        if(baseView == null){
            return;
        }
        baseView.showProgress(true);
        AppClient.getInstance().checkPhoneReg(callingCode, phone, longitude, latitude,type,checkIdNumber,birthday, needCheckIdNumber, new LifecycleMVPResultCallback<CheckPhoneEntity>(baseView,true) {
            @Override
            public void onSuccess(CheckPhoneEntity result) {
                baseView.showProgress(false);
                baseView.checkPhoneRegSuccess(result);
            }

            @Override
            public void onFail(String errorCode, String errorMsg) {
                baseView.showProgress(false);
                baseView.checkPhoneRegError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void checkNewDevice(String callingCode, String phone,String checkIdNumber, String birthday,String sceneType,String userId) {
        if(baseView == null){
            return;
        }
        baseView.showProgress(true);
        AppClient.getInstance().getUserManager().checkNewDevice(callingCode, phone,checkIdNumber,birthday,sceneType,userId,new LifecycleMVPResultCallback<CheckPhoneEntity>(baseView,true) {
            @Override
            public void onSuccess(CheckPhoneEntity result) {
                baseView.showProgress(false);
                baseView.checkNewDeviceSuccess(result);
            }

            @Override
            public void onFail(String errorCode, String errorMsg) {
                baseView.showProgress(false);
                baseView.checkNewDeviceError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(RegisterPhoneCheckContract.RegisterNafathView registerView) {
        this.baseView = registerView;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
