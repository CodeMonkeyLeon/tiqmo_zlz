package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * SendReceive详情实体类
 */
public class SendReceiveDetailEntity extends BaseEntity {

    private String dateDesc;
    private List<TransactionListInfos> transactionListInfos;

    public String getDateDesc() {
        return dateDesc;
    }

    public void setDateDesc(String dateDesc) {
        this.dateDesc = dateDesc;
    }

    public List<TransactionListInfos> getTransactionListInfos() {
        return transactionListInfos;
    }

    public void setTransactionListInfos(List<TransactionListInfos> transactionListInfos) {
        this.transactionListInfos = transactionListInfos;
}

}
