package cn.swiftpass.wallet.tiqmo.support.entity;

public class AutoLoginSucEntity extends BaseEntity {

    private String sId;
    private String walletId;

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }
}
