package cn.swiftpass.wallet.tiqmo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.callback.InitCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.common.ApiException;

import java.util.Iterator;
import java.util.Map;

import cn.swiftpass.wallet.tiqmo.base.view.OnMsgClickCallBack;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.AddMoneyDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.AddNewCardActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.ConfirmAddMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.SelectCardListActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.CardIntroductionActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.MainActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryFiveActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryOneActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiarySixActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryTwoActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrMainActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrMoreInfoActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrSendMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrSendMoneySummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.NafathAuthActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterIdActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AreaEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CardManageActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycVerifyActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.TopUpSelectActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeConfirmActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeCountryActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeListActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeOperatorActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeProductListActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.AddBeneficiaryActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.AddMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.BeneficiaryListActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.BeneficiaryMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.RequestTransferActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanPayActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanQrActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanWebActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferContactActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.WithDrawActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherPayReviewActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherSelectActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.VoucherMarketActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.CertsDownloadManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.ServerConfigHelper;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.deviceinfo.DeviceUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityLifeManager;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.CrashHandler;
import cn.swiftpass.wallet.tiqmo.support.utils.ExecutorManager;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomMsgDialog;
import dev.b3nedikt.restring.Restring;
import dev.b3nedikt.reword.RewordInterceptor;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import me.jessyan.autosize.AutoSizeConfig;


public class ProjectApp extends Application {
    private static final String TAG = "ProjectApp";
    public static Context mContext;
    private static ProjectApp projectApp;

    private static final String AF_DEV_KEY = "BSTyhFsCJPM7k26Jya8yLA";

    private static double longitude, latitude;

    public static boolean kycSuccess;
    public static boolean changePhoneSuccess;
    public static boolean addCardSuccess;
    public static boolean closePendingBalance;
    public static boolean requestTransferSuccess;
    public static boolean addMoneySuccessOnMain;
    public static boolean showMarketActivity;
    public static boolean changeTheme;
    public static Handler mainHandler;

    private static CardNotifyEntity cardNotifyEntity;

    public static CardNotifyEntity getCardNotifyEntity() {
        return cardNotifyEntity;
    }

    public static void setCardNotifyEntity(CardNotifyEntity cardNotifyEntity) {
        ProjectApp.cardNotifyEntity = cardNotifyEntity;
    }

    public Handler getMainHandler() {
        if (mainHandler == null) {
            return (mainHandler = new Handler(Looper.getMainLooper()));
        }
        return mainHandler;
    }

    public static int getFrontActivityNumber() {
        return frontActivityNumber;
    }

    private static int frontActivityNumber = 0;

    public static void setLongitude(double longitude) {
        ProjectApp.longitude = longitude;
    }

    public static void setLatitude(double latitude) {
        ProjectApp.latitude = latitude;
    }

    public static double getLongitude() {
        return longitude;
    }

    public static double getLatitude() {
        return latitude;
    }

    public static BiometricManager biometricManager;

    /**
     * 是否支持并且已经录入了生物识别
     */
    public static boolean isBiometricSupported() {
        return BiometricManager.BIOMETRIC_SUCCESS == biometricManager.canAuthenticate();
    }

    /**
     * 手机硬件不支持生物识别
     */
    public static boolean isPhoneNotSupportedFace() {
        return BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE == biometricManager.canAuthenticate();
    }

    public static ProjectApp getInstance() {
        return projectApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (projectApp == null) {
            projectApp = this;
        }
        mainHandler = new Handler(Looper.getMainLooper());
        mContext = getApplicationContext();
        initTools();
        initWalletSDK();
        ActivityHelper.init(this);
        initAppsFlyer();
        setUpListener();
        initFont();
        setFioFace();
        initPush();
        initChat();
        registerActivityCallBack();

        //在这里为应用设置异常处理程序，然后我们的程序才能捕获未处理的异常
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(this);

        //解决 整体解决屏幕缩放问题 华为mate10 400
//        FirebaseMessaging.getInstance().subscribeToTopic("news");
        AutoSizeConfig.getInstance().setDesignWidthInDp(360).setDesignHeightInDp(640).setExcludeFontScale(true);
        initPhotoError();

        if (BuildConfig.DOWNLOAD_CER_SERVER) {
            CertsDownloadManager.getInstance().initCertificate(getContext());
        }
    }

    private void initChat() {
        try {
            GcChatSDK.init(this, new InitCallback() {
                @Override
                public void onSuccessful() {
                    LogUtils.d("gc_chat", "GcChatSDK init onSuccessful");
                }

                @Override
                public void onFailure(int i) {
                    LogUtils.d("gc_chat", "GcChatSDK init onFailure - " + i);
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void initPush() {
        if (DeviceUtils.isEMUI()) {
            // 创建一个新线程
            getHWPushToken();
        } else {
            Log.i(TAG, "get Google play token: start");
            //Google 推送
            FirebaseApp.initializeApp(this);
            //UserInfoManager.getInstance().setmGcmDeviceToken(FirebaseInstanceId.getInstance().getToken());
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            Log.i(TAG, "get Google play token filed!");
                            if (!task.isSuccessful()) {
                                Log.i(TAG, "get Google play token filed!");
                                return;
                            }
                            Log.i(TAG, "get Google play token success");
                            // Get new Instance ID token
                            if (task.getResult() != null) {
                                String token = task.getResult().getToken();
                                sendRegTokenToServer(token);
                            }
                        }
                    });
        }
    }

    private void getHWPushToken() {
        new Thread() {
            @Override
            public void run() {
                try {
                    Log.i(TAG, "get huawei token: start");
                    // 从agconnect-service.json文件中读取appId
//                    String appId = "104633875";
                    String appId = BuildConfig.HW_PUSH_ID;

                    // 输入token标识"HCM"
                    String tokenScope = "HCM";
                    String token = HmsInstanceId.getInstance(ProjectApp.this).getToken(appId, tokenScope);
                    Log.i(TAG, "get token: " + token);

                    // 判断token是否为空
                    if (!TextUtils.isEmpty(token)) {
                        token = BuildConfig.hwOS + token;
                        sendRegTokenToServer(token);
                    }
                } catch (ApiException e) {
                    Log.e(TAG, "get token failed, " + e);
                }
            }
        }.start();
    }

    public void sendRegTokenToServer(String token) {
        UserInfoManager.getInstance().setmGcmDeviceToken(token);
        if (!TextUtils.isEmpty(token)) {
            SpUtils.getInstance().setDeviceToken(token);
        }
        LogUtils.i(TAG, "onMessageReceived: " + token);
    }

    private void initAppsFlyer() {
        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {
            @Override
            public void onConversionDataSuccess(Map<String, Object> conversionData) {

                for (String attrName : conversionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + conversionData.get(attrName));
                }
            }

            @Override
            public void onConversionDataFail(String errorMessage) {
                Log.d("LOG_TAG", "error getting conversion data: " + errorMessage);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> attributionData) {
                for (String attrName : attributionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + attributionData.get(attrName));
                }
            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d("LOG_TAG", "error onAttributionFailure : " + errorMessage);
            }
        };

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionListener, this);
        AppsFlyerLib.getInstance().start(this);
    }

    private void initFont() {
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath(LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Regular.ttf" : "fonts/SFProDisplay-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build())).build());

//        if(AppClient.getInstance().getUserManager() != null) {
//            AppClient.getInstance().getUserManager().checkLanguageUpdate(null);
//        }
    }

    private void setFioFace() {
        biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                LogUtils.d(TAG, "应用可以进行生物识别技术进行身份验证。");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                LogUtils.e(TAG, "该设备上没有搭载可用的生物特征功能。");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                LogUtils.e(TAG, "生物识别功能当前不可用。");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                LogUtils.e(TAG, "用户没有录入生物识别数据。");
                break;
            default:
                break;
        }
    }

    //设置全局的监听
    private void setUpListener() {
        AppClient.getInstance().setOnLoginExpiredListener(new AppClient.OnLoginExpiredListener() {
            @Override
            public void onExpired(String code, String message) {
                Context context = ActivityHelper.getStackTopActivityInstance();
                if (context == null) {
                    context = ProjectApp.this;
                }
                UserInfoEntity user = AppClient.getInstance().getUserManager().getLastUserInfo();
                if (user != null) {
                    LoginFastNewActivity.startActivityOfNewTaskType(context, user, "");
                } else {
                    RegisterActivity.startActivityOfNewTaskType(context);
                }
//                switch (code) {
//                    //判断是否是过期的CODE
//                    case ResponseCallbackWrapper.RESPONSE_CODE_EXPIRED:
//                        message = getString(R.string.common_1);
//                        break;
//                    case ResponseCallbackWrapper.RESPONSE_CODE_INVALID:
//                        message = getString(R.string.common_1);
//                        break;
//                    case ResponseCallbackWrapper.RESPONSE_CODE_LOG_IN_ON_OTHER_DEVICES:
//                        message = getString(R.string.common_2);
//                        break;
//                    default:break;
//                }
//
//                showErrorMsgDialog(mContext, message, new OnMsgClickCallBack() {
//                    @Override
//                    public void onBtnClickListener() {
//                        UserInfoEntity user = AppClient.getInstance().getUserManager().getLastUserInfo();
//                        if (user != null) {
//                            LoginFastActivity.startActivityOfNewTaskType(mContext, user, "");
//                        } else {
//                            LoginActivity.startActivityOfNewTaskType(mContext);
//                        }
//                    }
//                });
            }
        });
        ActivityHelper.addAppFrontAndBackListener(new ActivityHelper.OnAppFrontAndBackListener() {
            private long mEnterBackgroundTime;

            @Override
            public void onAppForeground() {
                AppClient client = AppClient.getInstance();
                if (client.isLogged() && client.getBiometricManager().isEnableFingerprintLogin()) {
                    long nowTime = SystemClock.elapsedRealtime();
                    if (nowTime - mEnterBackgroundTime < AppConfig.APP_UNLOCK_INTERVAL) {
                        return;
                    }
                    Activity activity = ActivityHelper.getStackTopActivityInstance();
//                    if (activity != null &&
//                            activity.getClass() != SplashActivity.class &&
//                            !ActivityHelper.isExistInActivityStack(UnlockActivity.class)) {
//                        activity.startActivity(new Intent(activity, UnlockActivity.class));
//                    }
                }
            }

            @Override
            public void onAppBackground() {
                mEnterBackgroundTime = SystemClock.elapsedRealtime();
            }
        });
    }

    public void showErrorMsgDialog(Context mContext, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.wtw_11), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    private void initWalletSDK() {
        AreaEntity areaEntity = SpUtils.getInstance().getAreaEntity();
        if (areaEntity == null) {
            areaEntity = new AreaEntity(Constants.CALLINGCODE_KSA, Constants.KsaServerPublicKey, Constants.KsaPartnerNo, "Saudi Arabia", R.drawable.icon_ksa);
        }
        if (BuildConfig.DEBUG) {
            ServerConfigHelper.Model model = ServerConfigHelper.getConfig(this);
            AppClient.init(this,
                    model.getUrl() + BuildConfig.RequestPath,
                    model.getUrl() + BuildConfig.UploadRequestPath,
                    areaEntity.partnerNo,
                    BuildConfig.OSType,
                    BuildConfig.DefaultLanguage,
                    areaEntity.callingCode,
                    AndroidUtils.getCurrentAppVersionName(mContext),
                    model.isEncodeData(),
                    areaEntity.publicKey);
        } else {
            AppClient.init(this,
                    BuildConfig.ServerUrl + BuildConfig.RequestPath,
                    BuildConfig.ServerUrl + BuildConfig.UploadRequestPath,
                    areaEntity.partnerNo,
                    BuildConfig.OSType,
                    BuildConfig.DefaultLanguage,
                    areaEntity.callingCode,
                    AndroidUtils.getCurrentAppVersionName(mContext),
                    BuildConfig.isEnableEncryptMode,
                    areaEntity.publicKey);
        }

        LogUtil.setEnableLog(BuildConfig.isLogDebug);

        Restring.init(ProjectApp.mContext);
        dev.b3nedikt.viewpump.ViewPump.init(RewordInterceptor.INSTANCE);
    }

    /**
     * 初始化工具包
     */
    private void initTools() {
        SpUtils.init(this);
    }

    private void initPhotoError() {
        // android 7.0系统解决拍照的问题
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
    }

    /**
     * activity全部finish 除了 MainActivity
     */
    public static void removeAllTaskExcludeMainStack() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof MainActivity)) {
                    activity.finish();
                }
                iterator.remove();
            }
        }
    }

    /**
     * 删除本地保存的用户信息
     */
    public static void clearUserInfo() {
        AppClient.getInstance().clearLastSessionInfo();
        AppClient.getInstance().getUserManager().clearLastUserInfo();
    }

    public static void removeRegTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && (activity instanceof RegisterActivity || activity instanceof RegisterOtpActivity)) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeKycTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && (activity instanceof WebViewActivity ||
                        activity instanceof KycVerifyActivity ||
                        activity instanceof RegisterOtpActivity)) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeImrAddBeneTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof AddBeneficiaryOneActivity || activity instanceof AddBeneficiaryTwoActivity
                        || activity instanceof AddBeneficiaryFiveActivity || activity instanceof AddBeneficiarySixActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
        AppsFlyerHelper.getInstance().sendAddImrBeneEvent();
    }

    public static void removeImrSendMoneyTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof ImrSendMoneyActivity || activity instanceof ImrMoreInfoActivity || activity instanceof ImrSendMoneySummaryActivity || activity instanceof ImrMainActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeTransferTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof ScanQrActivity || activity instanceof RequestTransferActivity || activity instanceof BeneficiaryMoneyActivity || activity instanceof BeneficiaryListActivity || activity instanceof AddBeneficiaryActivity || activity instanceof BeneficiaryListActivity || activity instanceof TransferSummaryActivity || activity instanceof TransferMoneyActivity || activity instanceof TransferPwdActivity || activity instanceof TransferContactActivity
                        || activity instanceof WithDrawActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeScanTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof ScanQrActivity || activity instanceof ScanPayActivity || activity instanceof ScanWebActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeRechargeTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof RechargeConfirmActivity || activity instanceof RechargeProductListActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeApp2App() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof TransferPwdActivity
                        || activity instanceof ScanPayActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeSplitTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof TransferPwdActivity || activity instanceof CreateSplitBillActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeEVoucherTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof EVoucherSelectActivity || activity instanceof EVoucherPayReviewActivity || activity instanceof TopUpSelectActivity
                        || activity instanceof VoucherMarketActivity || activity instanceof RechargeListActivity || activity instanceof RechargeOperatorActivity
                        || activity instanceof RechargeCountryActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeHyperpayTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof AddNewCardActivity || activity instanceof SelectCardListActivity || activity instanceof ConfirmAddMoneyActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeAddMoneyTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof CardManageActivity || activity instanceof AddMoneyActivity || activity instanceof AddMoneyDetailActivity || activity instanceof WebViewActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeGetNewCardTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (!(activity instanceof MainActivity) && !(activity instanceof CardIntroductionActivity)) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeTransferDetailTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof AccountPayDetailActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeNafathTask() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof NafathAuthActivity || activity instanceof RegisterIdActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    public static void removeAddMoneyTaskExcludeWeb() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity == null) {
                    return;
                }
                if (activity instanceof CardManageActivity || activity instanceof AddMoneyActivity || activity instanceof AddMoneyDetailActivity) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }

    /**
     * activity全部finish 除了 LoginActivity
     */
    public static void removeAllTaskExcludeLoginStack() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof LoginFastNewActivity)) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }
    }


    /**
     * 全局activity 生命周期的监听
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void registerActivityCallBack() {
        ActivityLifecycleCallbacks mActivityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                ActivityLifeManager.getInstance().add(activity);
            }

            @Override
            public void onActivityStarted(Activity activity) {
                frontActivityNumber++;
                ActivityLifeManager.getInstance().addFront(activity);
            }

            @Override
            public void onActivityResumed(Activity activity) {
                ActivityLifeManager.getInstance().setCurrentActivity(activity);
            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                frontActivityNumber--;
                ActivityLifeManager.getInstance().removeFront(activity);
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                ActivityLifeManager.getInstance().remove(activity);
            }
        };
        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks);
    }

//    /**
//     * activity全部finish 除了 MainActivity
//     */
//    public static void removeAllTaskExcludeMainStack() {
//        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
//            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
//            while (iterator.hasNext()) {
//                Activity activity = iterator.next();
//                if (activity != null && !(activity instanceof MainActivity)) {
//                    activity.finish();
//                }
//                iterator.remove();
//            }
//        }
//    }


    /**
     * activity全部finish
     */
    public static void removeAllTaskIncludeMainStack() {
        if (ActivityLifeManager.getInstance().getActivities() != null && ActivityLifeManager.getInstance().getActivities().size() > 0) {
            Iterator<Activity> iterator = ActivityLifeManager.getInstance().getActivities().iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null) {
                    activity.finish();
                    iterator.remove();
                }
            }
        }

    }

    /**
     * 注册流程需要一个recordId
     */
    private static String registerRecordId;
    private static long checkIdvTime;
    /**
     * 登录成功后phone
     */
    private static String phone = BuildConfig.DefaultInternationalAreaCode;

    public static String getPhone() {
        if (AppClient.getInstance().getUserManager().getUserInfo() != null) {
            return AppClient.getInstance().getUserManager().getUserInfo().phone;
        }
        return phone;
    }

    public static void setPhone(String phone) {
        ProjectApp.phone = phone;
    }

    public static void setCheckIdStartTime(long time) {
        checkIdvTime = time;
    }


    public static String getRegisterRecordId() {
        return registerRecordId == null ? "" : registerRecordId;
    }

    public static long getCheckIdvStartTime() {
        return checkIdvTime;
    }


    public static void setRegisterRecordId(String registerRecordIdIn) {

        registerRecordId = registerRecordIdIn;

    }

    /**
     * @return 全局的上下文
     */
    public static Context getContext() {
        return mContext;
    }


    @Override
    protected void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(base);
        androidx.multidex.MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        LogUtils.d("TAG", "chang onTerminate");
        ExecutorManager.getInstance().shutdown();
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateResources(getApplicationContext());
    }
}
