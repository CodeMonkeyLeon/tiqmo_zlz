package cn.swiftpass.wallet.tiqmo.module.voucher.presenter;

import android.app.Activity;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandListEntity;

public class EVoucherContract {

    public interface View extends BaseView<EVoucherContract.Presenter> {

        void getLimitSuccess(TransferLimitEntity transferLimitEntity, CheckOutEntity checkOutEntity);

        void eVoucherOrderSuccess(EVoucherOrderInfoEntity orderInfo);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void showErrorMsg(String errCode, String errMsg);

        void showVoucherList(VouTypeAndInfoEntity result);

    }

    public interface Presenter extends BasePresenter<EVoucherContract.View> {

        void getLimit(String type, CheckOutEntity checkOutEntity);


        void eVoucherOrder(String voucherCode, String voucherAmount);

        void checkOut(String orderNo, String orderInfo);

        void requestVoucherInfo(String providerCode, String brandCode, String countryCode, String sceneType);

    }

    public interface EVoucherPayView extends BaseView<EVoucherContract.EVoucherPayPresenter> {
        void confirmPaySuccess(TransferEntity transferEntity);

        void confirmPayFail(String errCode, String errMsg);
    }

    public interface EVoucherPayPresenter extends BasePresenter<EVoucherContract.EVoucherPayView> {
        void confirmPay(String orderNo, String paymentMethodNo, String orderType);
    }

    public interface VoucherBrandListView extends BaseView<EVoucherContract.VoucherBrandListPresenter> {
        void getVoucherBrandListSuccess(VoucherBrandListEntity result);

        void getRecentVoucherBrandListSuccess(VoucherBrandListEntity result);

        void getVoucherBrandListFail(String errCode, String errMsg);

        void getRecentVoucherBrandListFail(String errCode, String errMsg);

        void showErrorMsg(String errCode, String errMsg);

        void showOrderRepeatDia(VoucherBrandEntity voucherBrandEntity, String sceneType);

        void checkOrderStatusSuccess(VoucherBrandEntity voucherBrandEntity, String sceneType);

        void eVoucherOrderSuccess(EVoucherOrderInfoEntity orderInfo);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity, CheckOutEntity checkOutEntity);

        void showEVouCountryDia(VouSupCountryEntity result, String providerCode, String brandCode, String brandName);
    }

    public interface VoucherBrandListPresenter extends BasePresenter<EVoucherContract.VoucherBrandListView> {
        void getVoucherBrandList();

        void getRecentVoucherBrandList();

        void getLimit(String type, CheckOutEntity checkOutEntity);

        void checkOut(String orderNo, String orderInfo);

        void eVoucherOrder(String voucherCode, String voucherAmount);

        void checkOrderExist(VoucherBrandEntity voucherBrandEntity, String sceneType);

        void skipToVouTypeAndInfo(Activity activity, String providerCode, String brandCode, String brandName, String countryCode);

        void continueToSelectTickets(Activity activity, VoucherBrandEntity voucherBrandEntity, String sceneType);

        void requestSupCountry(String providerCode, String brandCode, String brandName);
    }
}
