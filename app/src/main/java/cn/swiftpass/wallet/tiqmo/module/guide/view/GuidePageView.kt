package cn.swiftpass.wallet.tiqmo.module.guide.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.module.guide.OnNextClickListener
import cn.swiftpass.wallet.tiqmo.widget.MyTextView

class GuidePageView : ConstraintLayout {


    private lateinit var mContext: Context


    private lateinit var mImgView: ImageView
    private lateinit var mTvTitle: MyTextView
    private lateinit var mTvContent: MyTextView
    private lateinit var mBtnNext: MyTextView

    private var mIndex: Int = 0

    private var mOnNextClickListener: OnNextClickListener? = null

    fun setOnNextClickListener(listener: OnNextClickListener?) {
        listener?.let {
            mOnNextClickListener = it
        }
    }


    constructor(context: Context) : super(context) {
        mContext = context
        initView()
    }


    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        mContext = context
        initView()
    }


    constructor(context: Context, attributeSet: AttributeSet, def: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        initView()
    }


    private val initView = {
        //加载布局
        LayoutInflater.from(mContext).inflate(R.layout.layout_view_page, this)
        //绑定控件
        mImgView = findViewById(R.id.id_img)
        mTvTitle = findViewById(R.id.id_tv_title)
        mTvContent = findViewById(R.id.id_tv_content)
        mBtnNext = findViewById(R.id.id_btn_next)


        mBtnNext.setOnClickListener {
            mOnNextClickListener?.run {
                onNextClick(mIndex)
            }
        }
    }


    fun setIndex(index: Int) {
        mIndex = index
    }


    fun setTitle(text: String) {
        mTvTitle.text = text
    }

    fun setContent(text: String) {
        mTvContent.text = text
    }

    fun setImage(resId: Int) {
        mImgView.setImageResource(resId)
    }


    fun setNext(text: String) {
        mBtnNext.text = text
    }

}