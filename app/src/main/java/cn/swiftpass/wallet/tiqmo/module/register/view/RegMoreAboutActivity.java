package cn.swiftpass.wallet.tiqmo.module.register.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.zrq.spanbuilder.Spans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterPhoneCheckContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.RegMoreAboutPresenter;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

public class RegMoreAboutActivity extends BaseCompatActivity<RegisterPhoneCheckContract.RegisterMorePresenter> implements RegisterPhoneCheckContract.RegisterMoreView{

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_content_title)
    TextView tvContentTitle;
    @BindView(R.id.et_id_number)
    CustomizeEditText etIdNumber;
    @BindView(R.id.tv_birthday_subtitle)
    TextView tvBirthdaySubtitle;
    @BindView(R.id.tv_birthday)
    TextView tvBirthday;
    @BindView(R.id.fl_birthday)
    FrameLayout flBirthday;
    @BindView(R.id.ll_kyc)
    LinearLayout llKyc;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_notice)
    TextView tvNotice;

    private CheckPhoneEntity checkPhoneEntity;
    private String phone,callingCode,userId;
    private String checkIdNumber = "",birthday = "";
    private String validIDNotices = "";
    private Date mSelectedDate = null;
    // 设置日历的显示的地区（根据自己的需要写）
    private GregorianCalendar chooseCal;
    private int maxIdLength;
    /**
     * 是否选择公历
     */
    private boolean isChooseGeo = true;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_reg_more_about;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if(checkPhoneEntity != null){
            phone = checkPhoneEntity.phone;
            userId = checkPhoneEntity.userId;
        }
        callingCode = Constants.CALLINGCODE_KSA;
        maxIdLength = 10;
        etIdNumber.setHint(getString(R.string.forget_ppw_5));
        validIDNotices = getString(R.string.kyc_37);
        etIdNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxIdLength),
                new NormalInputFilter(NormalInputFilter.NUMBER)});
        etIdNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkIdNumber = s.toString();
                if (TextUtils.isEmpty(checkIdNumber)) {
                    clearBirthday();
                    birthday = "";
                    tvContinue.setEnabled(false);
                    etIdNumber.setError("");
                } else {
                    if (!checkIdNumber.startsWith("1") && !checkIdNumber.startsWith("2")) {
                        clearBirthday();
                        birthday = "";
                        etIdNumber.setError(validIDNotices);
                    } else {
                        if (checkIdNumber.startsWith("1") && isChooseGeo) {
                            clearBirthday();
                            birthday = "";
                        }

                        if (checkIdNumber.startsWith("2") && !isChooseGeo) {
                            clearBirthday();
                            birthday = "";
                        }
                    }
                    checkButtonStatus();
                }
            }
        });

        etIdNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkIdNumber = etIdNumber.getText().toString().trim();
                if (!hasFocus && !TextUtils.isEmpty(checkIdNumber)) {
                    if (checkIdNumber.length() < maxIdLength || (!checkIdNumber.startsWith("1") &&
                            !checkIdNumber.startsWith("2"))) {
                        etIdNumber.setError(validIDNotices);
                    } else {
                        etIdNumber.setError("");
                    }
                }
            }
        });

        tvNotice.setText(Spans.builder()
                .text(mContext.getString(R.string.MobileValidation_SU_0002_2_D_6)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.login_botton_notice_text_color))).size(12)
                .text(" " + mContext.getString(R.string.IMR_sendMoney_5)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.login_forgot_pwd_color))).size(12).click(tvNotice, new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        HashMap<String,Object> hashMap = new HashMap<>();
                        hashMap.put(Constants.terms_condition_type,"1");
                        ActivitySkipUtil.startAnotherActivity(RegMoreAboutActivity.this, WebViewActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setColor(mContext.getColor(R.color.color_fc4f00));
                        ds.setUnderlineText(false);
                    }
                }).build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(checkIdNumber) && etIdNumber != null){
            etIdNumber.setText(checkIdNumber);
            etIdNumber.setSelection(checkIdNumber.length());
        }
    }

    private void clearBirthday() {
        try {
            tvBirthday.setText("");
            tvBirthday.setVisibility(View.GONE);
            ViewAnim.resetViewParams(mContext, tvBirthdaySubtitle);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    public void selectDatetime(boolean isGeo) {
        if (isGeo) {
            GregorianCalendar startDate = new GregorianCalendar();
            GregorianCalendar endDate = new GregorianCalendar();
            startDate.set(Calendar.YEAR, startDate.get(Calendar.YEAR) - 90);
            endDate.set(Calendar.YEAR, endDate.get(Calendar.YEAR) - 16);

            GregorianCalendar selectedDate = new GregorianCalendar();
            if (mSelectedDate == null) {
                mSelectedDate = endDate.getTime();
                selectedDate.setTime(endDate.getTime());
            } else {
                selectedDate.setTime(mSelectedDate);
            }

            TimePickerDialogUtils.showTimePicker(mContext, selectedDate, startDate, endDate, new OnTimeSelectListener() {
                @Override
                public void onTimeSelect(Date date, View v) {
                    isChooseGeo = true;
                    mSelectedDate = date;

                    SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
                    birthday = df.format(date);
                    etIdNumber.setError("");
                    refreshItemView(birthday, tvBirthday, tvBirthdaySubtitle, false);
                }
            });
        } else {
            UmmalquraCalendar startHijDate = new UmmalquraCalendar();
            UmmalquraCalendar endHijDate = new UmmalquraCalendar();

            GregorianCalendar calendar = new GregorianCalendar();
            GregorianCalendar startCal = new GregorianCalendar(calendar.get(Calendar.YEAR) - 80,
                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            startHijDate.setTime(startCal.getTime());
            GregorianCalendar endCal = new GregorianCalendar(calendar.get(Calendar.YEAR) - 16,
                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            endHijDate.setTime(endCal.getTime());
            UmmalquraCalendar selectedHijDate = new UmmalquraCalendar();
            if (chooseCal != null) {
                selectedHijDate.setTime(chooseCal.getTime());
            } else {
                selectedHijDate.setTime(endHijDate.getTime());
            }
            TimePickerDialogUtils.showTimePicker(mContext, selectedHijDate, startHijDate, endHijDate, new OnTimeSelectListener() {
                @Override
                public void onTimeSelect(Date date, View v) {
                    isChooseGeo = false;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
                    ummalquraCalendar.set(UmmalquraCalendar.YEAR, year);
                    ummalquraCalendar.set(UmmalquraCalendar.MONTH, month);
                    ummalquraCalendar.set(UmmalquraCalendar.DAY_OF_MONTH, day);

                    String bir = TimeUtils.getEnUmmalqura(ummalquraCalendar);

                    birthday = TimeUtils.getGregorianToUmmalqura(ummalquraCalendar);
                    refreshItemView(bir, tvBirthday, tvBirthdaySubtitle, false);
                    etIdNumber.setError("");
                }
            });
        }
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkButtonStatus();
                    }
                }
            });
        } else {
            content.setText(value);
            checkButtonStatus();

        }
    }

    private void checkButtonStatus() {
        tvContinue.setEnabled(false);
        if (!TextUtils.isEmpty(checkIdNumber) && checkIdNumber.length() == 10 && !TextUtils.isEmpty(birthday)) {
            tvContinue.setEnabled(true);
        }
    }

    @OnClick({R.id.iv_back, R.id.fl_birthday,R.id.tv_continue})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fl_birthday:
                if (TextUtils.isEmpty(checkIdNumber) || checkIdNumber.length() != maxIdLength) {
                    return;
                }
                if(!checkIdNumber.startsWith("1") && !checkIdNumber.startsWith("2")){
                    return;
                }
                selectDatetime(!checkIdNumber.startsWith("1"));
                break;
            case R.id.tv_continue:
                mPresenter.checkNewDevice(callingCode,phone,checkIdNumber,birthday,"DC",userId);
                break;
            default:break;
        }
    }

    @Override
    public void checkNewDeviceSuccess(CheckPhoneEntity result) {
        if(result != null){
            checkPhoneEntity.riskControlInfo = result.riskControlInfo;
        }
        ActivitySkipUtil.startAnotherActivity(this, LoginFastNewActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkNewDeviceError(String errorCode, String errorMsg) {
        if("030212".equals(errorCode)){
            ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
            errorBottomDialog.setErrorMsg(mContext.getString(R.string.sprint19_37),errorMsg,ThemeSourceUtils.getSourceID(mContext,R.attr.icon_error_mobile_number));
            errorBottomDialog.setCloseListener(mContext.getString(R.string.sprint19_38),new ErrorBottomDialog.onConfirmListener() {
                @Override
                public void clickConfirm() {
                    contactHelp();
                }
            });
            errorBottomDialog.showWithBottomAnim();
        }else {
            etIdNumber.setError(errorMsg);
        }
    }

    @Override
    public RegisterPhoneCheckContract.RegisterMorePresenter getPresenter() {
        return new RegMoreAboutPresenter();
    }
}
