package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public class ImrMainContract {

    public interface ImrMainView extends BaseView<IImrMainPresenter> {

    }

    public interface IImrMainPresenter extends BasePresenter<ImrMainView> {

    }
}
