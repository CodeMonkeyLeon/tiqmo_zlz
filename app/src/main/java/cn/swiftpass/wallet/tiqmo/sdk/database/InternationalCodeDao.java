package cn.swiftpass.wallet.tiqmo.sdk.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.sdk.entity.InternationalCodeEntity;


//目前APP就国际冠码这个地方用到了，不知道以后还是否会用得到更多，可以参考InternationalCodeDao例子去写
public class InternationalCodeDao extends AbstractDao<InternationalCodeEntity> {

    static final String TABLE_NAME = "InternationalCod";

    //以下是列名
    private static final String COLUMN_NAME_Code = "Code";
    private static final String COLUMN_NAME_Country = "Country";
    private static final String COLUMN_NAME_National = "National";

    //列明对饮的索引
    private static final int COLUMN_INDEX_Code = 0;
    private static final int COLUMN_INDEX_Country = 1;
    private static final int COLUMN_INDEX_National = 2;

    //创建表的sql语句，AppSQLiteOpenHelper用到
    public static final String CREATE_TABLE_SQL =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                    + COLUMN_NAME_Code + " TEXT NOT NULL , "
                    + COLUMN_NAME_Country + " TEXT NOT NULL,"
                    + COLUMN_NAME_National + " TEXT,"
                    + "PRIMARY KEY (" + COLUMN_NAME_Code + ", " + COLUMN_NAME_Country + ")"
                    + ")";

    public InternationalCodeDao(ReadWriteHelper readWriteHelper) {
        super(readWriteHelper);
    }

    //如果需要特殊的查询或其方法，可以自己实现，某人父类已经有了增删查改方法了
    public ArrayList<InternationalCodeEntity> getAllInternationalCode() {
        SQLiteDatabase database = mReadWriteHelper.openReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        ArrayList<InternationalCodeEntity> list = new ArrayList<>(cursor.getCount());
        while (cursor.moveToNext()) {
            list.add(toEntity(cursor));
        }
        cursor.close();
        mReadWriteHelper.closeReadableDatabase();
        return list;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getWhereClauseOfKey() {
        return COLUMN_NAME_Code + "=? and " + COLUMN_NAME_Country + "=?";
    }

    @Override
    protected String[] toWhereArgsOfKey(InternationalCodeEntity entity) {
        return new String[]{entity.getCallingCode(), entity.getCountry()};
    }

    @Override
    protected void parseToContentValues(InternationalCodeEntity entity, ContentValues values) {
        values.put(COLUMN_NAME_Code, entity.getCallingCode());
        values.put(COLUMN_NAME_Country, entity.getCountry());
        values.put(COLUMN_NAME_National, entity.getNationalFlag());
    }

    @Override
    protected InternationalCodeEntity toEntity(Cursor cursor) {
        InternationalCodeEntity entity = new InternationalCodeEntity();
        entity.setCallingCode(cursor.getString(COLUMN_INDEX_Code));
        entity.setCountry(cursor.getString(COLUMN_INDEX_Country));
        entity.setNationalFlag(cursor.getString(COLUMN_INDEX_National));
        return entity;
    }
}
