package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.QRCodeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

/**
 * 我的付款码页面
 */
public class MyPaymentCodeActivity extends BaseCompatActivity {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_qr_code)
    TextView tvQrCode;
    @BindView(R.id.ll_wallet_balance)
    LinearLayout llWalletBalance;
    @BindView(R.id.tv_payment_one)
    TextView tvPaymentOne;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_my_payment_code;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivHeadCircle);
        tvTitle.setText(getString(R.string.sprint11_141));

        if(getIntent() != null && getIntent().getExtras() != null){
            String qrCodeUrl = getIntent().getExtras().getString(Constants.qrCodeUrl);
            String paymentMethod = getIntent().getExtras().getString(Constants.paymentMethod);
            tvPaymentOne.setText(paymentMethod);
            if(!TextUtils.isEmpty(qrCodeUrl)){
                Bitmap logo = BitmapFactory.decodeResource(getResources(),R.drawable.l_qr_logo);
                Bitmap bitmap = QRCodeUtils.createQRCodeWithLogo(qrCodeUrl, AndroidUtils.dip2px(mContext, 300),logo);
                ivQrCode.setImageBitmap(bitmap);
            }
        }

        if(userInfoEntity != null){
            String userName = userInfoEntity.userName;
            String avatarUrl = userInfoEntity.avatar;
            tvUserName.setText(userName);
            if (!TextUtils.isEmpty(avatarUrl)) {
                Glide.with(mContext)
                        .load(avatarUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ivAvatar.getDrawable())
                        .into(ivAvatar);
            } else {
                ivAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender));
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_qr_code})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_qr_code:
                finish();
                break;
            default:break;
        }
    }
}
