package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.ActivateCardPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

public class ActivateCardActivity extends BaseCompatActivity<CardKsaContract.ActivateCardPresener> implements CardKsaContract.ActivateCardView {


    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_card_number)
    CustomizeEditText etCardNumber;
    @BindView(R.id.tv_expiry_subtitle)
    TextView tvExpirySubtitle;
    @BindView(R.id.tv_expiry_date)
    TextView tvExpiryDate;
    @BindView(R.id.fl_expiry_date)
    FrameLayout flExpiryDate;
    @BindView(R.id.tv_activate)
    TextView tvActivate;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private String expiryTime,cardNumber;
    private String validIDNotices = "";
    private Date mSelectedDate = null;
    // 设置日历的显示的地区（根据自己的需要写）
//    private GregorianCalendar chooseCal;

    private KsaCardEntity ksaCardEntity;

    private String proxyCardNo;

    private RiskControlEntity riskControlEntity;

    private static ActivateCardActivity activateCardActivity;

    public static ActivateCardActivity getActivateCardActivity() {
        return activateCardActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activateCardActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_activate_card;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activateCardActivity = this;
        tvTitle.setText(R.string.DigitalCard_58);
        setDarkBar();
        fixedSoftKeyboardSliding();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if(getIntent() != null && getIntent().getExtras() != null){
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            if(ksaCardEntity != null){
                proxyCardNo = ksaCardEntity.proxyCardNo;
            }
        }
        etCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cardNumber = s.toString();
                checkButtonStatus();
            }
        });
    }

    @OnClick({R.id.iv_back, R.id.fl_expiry_date, R.id.tv_activate})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fl_expiry_date:
                selectDatetime();
                break;
            case R.id.tv_activate:
                cardNumber = etCardNumber.getText().toString().trim();
                List<String> additionalData = new ArrayList<>();
                if(ksaCardEntity != null){
                    additionalData.add(ksaCardEntity.maskedCardNo);
                }
                mPresenter.getOtpType("ActionType", "TCA10", additionalData);
                break;
            default:break;
        }
    }

    public void selectDatetime() {
        GregorianCalendar startDate = new GregorianCalendar();
        GregorianCalendar endDate = new GregorianCalendar();
        startDate.set(Calendar.YEAR, startDate.get(Calendar.YEAR)-10);
        endDate.set(Calendar.YEAR, endDate.get(Calendar.YEAR) + 50);

        GregorianCalendar selectedDate = new GregorianCalendar();
        if (mSelectedDate == null) {
            mSelectedDate = endDate.getTime();
            selectedDate.setTime(endDate.getTime());
        } else {
            selectedDate.setTime(mSelectedDate);
        }

        TimePickerDialogUtils.showTimePickerWithoutDay(mContext, selectedDate, startDate, endDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedDate = date;

//                SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
                SimpleDateFormat df = new SimpleDateFormat("MM/YY", Locale.ENGLISH);
                expiryTime = df.format(date).replace("/","");
                refreshItemView(df.format(date), tvExpiryDate, tvExpirySubtitle, false);
            }
        });
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkButtonStatus();
                    }
                }
            });
        } else {
            content.setText(value);
            checkButtonStatus();

        }
    }

    private void checkButtonStatus() {
        tvActivate.setEnabled(!TextUtils.isEmpty(cardNumber) && !TextUtils.isEmpty(expiryTime));
    }

    @Override
    public void activateCardSuccess(Void result) {
        HashMap<String,Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);
        mHashMap.put(Constants.cardSuccessType,3);
        ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public CardKsaContract.ActivateCardPresener getPresenter() {
        return new ActivateCardPresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(ActivateCardActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        ksaCardEntity.expiryTime = expiryTime;
        ksaCardEntity.cardNumber = cardNumber;
        mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_ACTIVATE);
        mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_PAY_CARD_ACTIVATE);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    private void requestTransfer(){
        mPresenter.activateCard(proxyCardNo,cardNumber,expiryTime);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
