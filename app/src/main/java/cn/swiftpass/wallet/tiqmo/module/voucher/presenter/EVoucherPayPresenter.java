package cn.swiftpass.wallet.tiqmo.module.voucher.presenter;

import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class EVoucherPayPresenter implements EVoucherContract.EVoucherPayPresenter {

    private EVoucherContract.EVoucherPayView mView;

    @Override
    public void attachView(EVoucherContract.EVoucherPayView eVoucherPayView) {
        this.mView = eVoucherPayView;
    }

    @Override
    public void detachView() {
        this.mView = null;
    }

    @Override
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType) {
        AppClient.getInstance().confirmPay(orderNo, paymentMethodNo, orderType, new LifecycleMVPResultCallback<TransferEntity>(mView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mView.confirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.confirmPayFail(errorCode, errorMsg);
            }
        });
    }
}
