package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.tiqmo.R;

public class ErrorDialog extends Dialog {

    private TextView mBtnCancel;
    private TextView mTvError;

    private View.OnClickListener mOnCancelClickListener;

    public ErrorDialog(@NonNull Context context) {
        super(context, R.style.AlertDialogStyle);
        init();
    }

    private void init() {
        setContentView(R.layout.dialog_error);
        mBtnCancel = findViewById(R.id.mBtnCancel);
        mTvError = findViewById(R.id.mTvHint);

        mBtnCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mOnCancelClickListener != null) {
                    mOnCancelClickListener.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                }
            }
        });
    }

    public ErrorDialog setContentText(String content) {
        mTvError.setText(content);
        return this;
    }

    public ErrorDialog setNegativeButton(String title, View.OnClickListener listener) {
        mBtnCancel.setText(title);
        mOnCancelClickListener = listener;
        return this;
    }

}
