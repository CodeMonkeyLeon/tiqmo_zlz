package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.SpendingBudgetFragment;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NumberMoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class BudgetEditAdapter extends BaseRecyclerAdapter<SpendingBudgetEntity> {

    public BudgetEditAdapter(@Nullable List<SpendingBudgetEntity> data) {
        super(R.layout.item_edit_budget_category, data);
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SpendingBudgetEntity spendingBudgetEntity, int position) {
        try {
            baseViewHolder.setImageResource(R.id.iv_budget_category, AndroidUtils.getBudgetCategoryDrawable(mContext, spendingBudgetEntity.categoryCode));
            baseViewHolder.setText(R.id.tv_category_name, spendingBudgetEntity.categoryName);
            baseViewHolder.setTextColor(R.id.tv_category_name,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_803a3b44)));
            baseViewHolder.setImageResource(R.id.iv_minus,ThemeSourceUtils.getSourceID(mContext,R.attr.icon_budget_delete));
            EditText etBillAmount = baseViewHolder.getView(R.id.et_amount);
            baseViewHolder.setTextColor(R.id.et_amount,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_80white_3a3b44)));
            baseViewHolder.setBackgroundColor(R.id.ll_line,mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_70white_703a3b44)));
            //两位小数过滤
            etBillAmount.setFilters(new InputFilter[]{new NumberMoneyInputFilter(), new InputFilter.LengthFilter(6)});
            etBillAmount.setTag(position);
            etBillAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    String amount = etBillAmount.getText().toString().trim();
                    if(hasFocus){
                        if(!BigDecimalFormatUtils.compareBig(amount,"0")){
                            etBillAmount.setText("");
                        }
                    }else{
                        if(TextUtils.isEmpty(amount)){
                            etBillAmount.setText("0");
                        }
                    }
                }
            });
            etBillAmount.setText(AndroidUtils.getTransferIntNumber(spendingBudgetEntity.budgetLimitAmount));
            etBillAmount.addTextChangedListener(new TextSwitcher(baseViewHolder));

            baseViewHolder.addOnClickListener(R.id.iv_minus);
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    class TextSwitcher implements TextWatcher {
        private BaseViewHolder mHolder;

        public TextSwitcher(BaseViewHolder mHolder) {
            this.mHolder = mHolder;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int position = (int) mHolder.getView(R.id.et_amount).getTag();//取tag值
            String amount = s.toString();
            amount = amount.replace(",", "");
            double money;
            if (!TextUtils.isEmpty(amount)) {
                money = Double.parseDouble(amount);
//                if (money == 0) {
//                    mHolder.getView(R.id.ll_line).setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_70fd1d2d));
//                } else {
//                    mHolder.getView(R.id.ll_line).setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_6b6c73_light));
//                }
            } else {
                money = 0;
//                mHolder.getView(R.id.ll_line).setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_70fd1d2d));
            }
            SpendingBudgetEntity mSpendingBudgetEntity = getDataList().get(position);
            mSpendingBudgetEntity.budgetLimitAmount = AndroidUtils.getReqTransferMoney(String.valueOf(money));
            SpendingBudgetFragment.spendingBudgetFragment.saveEditData(getDataList());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
