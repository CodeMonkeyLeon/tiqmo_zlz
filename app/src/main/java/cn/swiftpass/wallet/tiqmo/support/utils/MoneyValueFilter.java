package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;


public class MoneyValueFilter extends DigitsKeyListener {

    private static final String TAG = "MoneyValueFilter";

    public MoneyValueFilter() {
        super(false, true);
    }

    private int digits = 2;

    public MoneyValueFilter setDigits(int d) {
        digits = d;
        return this;
    }

    /**
     * @param source 输入的文字 ，即将输入的字符串
     * @param start  开始位置 ，source的start
     * @param end    结束位置 ，source的end，或理解为长度
     * @param dest   当前显示的内容 ，输入框中原来的内容
     * @param dstart 当前开始位置，要替换或者添加的起始位置，即光标所在的位置
     * @param dend   当前的结束位置，要替换或者添加的终止始位置，若为选择一串字符串进行更改，则为选中字符串 最后一个字符在dest中的位置
     */
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        CharSequence out = super.filter(source, start, end, dest, dstart, dend);

        LogUtils.i(TAG, "%%source: " + source + "out: " + out + " start:" + start + " end" + end + " dstart：" + dstart + " dend " + dend);

//        // if changed, replace the source
        if (out != null) {
            source = out;
            start = 0;
            end = out.length();
        }


        int len = end - start;
        if (len == 0) {
            return source;
        }

        //以点开始的时候，自动在前面添加0
        if (source.toString().equals(".") && dstart == 0) {
            return "0.";
        }
        LogUtils.i(TAG, "source: " + source + "start pos: " + dstart + " point pos:" + dest.toString().indexOf("."));
        LogUtils.i(TAG, "dest: " + dest.toString());
        //如果起始位置为0,且第二位跟的不是".",则无法后续输入
        if (!source.toString().equals(".") && dest.toString().equals("0")) {
            return "";
        }

        //.前头只能有一个0
        //source.toString().equals("0") &&
        if (dest.toString().contains(".")) {
            String firstCharStr = dest.toString().charAt(0) + "";
            int pointPos = dest.toString().indexOf(".");
            //dstart>0&&d
            if (firstCharStr.equals("0") && dstart <= pointPos) {
                if (source.toString().equals("0")) {
                    //如果之前小数点前 是0开始 这个时候再加进去0 非法
                    return "";
                } else if (dstart != 0) {
                    //09.00 非法
                    return "";
                }
            }
        }

        //如果起始位置为[0],且第二位跟的不是".",则无法后续输入
        if (source.toString().equals("0") && dest.toString().toString().length() > 0 && dstart == 0) {
            //009
            return "";
        }

        int dlen = dest.length();
        // Find the position of the decimal .
        for (int i = 0; i < dstart; i++) {
            if (dest.charAt(i) == '.') {
                // being here means, that a number has
                // been inserted after the dot
                // check if the amount of digits is right
                return (dlen - (i + 1) + len > digits) ? "" : new SpannableStringBuilder(source, start, end);
            }
        }

        for (int i = start; i < end; ++i) {
            if (source.charAt(i) == '.') {
                // being here means, dot has been inserted
                // check if the amount of digits is right
                if ((dlen - dend) + (end - (i + 1)) > digits) return "";
                else break;  // return new SpannableStringBuilder(source, start, end);
            }
        }


        // if the dot is after the inserted part,
        // nothing can break
        return new SpannableStringBuilder(source, start, end);
    }
}

