package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.EventListener;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SplitContactAdapter extends BaseRecyclerAdapter<KycContactEntity> {

    public SplitContactAdapter(@Nullable List<KycContactEntity> data) {
        super(R.layout.item_split_contact, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, KycContactEntity kycContactEntity, int position) {
        int section = getSectionForPosition(position);
        String userStatus = getUserStatusForPosition(position);
        if (TextUtils.isEmpty(userStatus)) {
            return;
        }
        if (section == -1) {
            return;
        }

        TextView tvIntive = baseViewHolder.getView(R.id.tv_invite);


        if ("registered".equals(userStatus) &&position == getPositionForSection(section)) {
            baseViewHolder.setGone(R.id.ll_invite_with,true);
            baseViewHolder.setGone(R.id.ll_letter, false);
            baseViewHolder.setText(R.id.tv_letter, kycContactEntity.getSortLetter());
        } else {
            baseViewHolder.setGone(R.id.ll_letter, true);
        }
        if ("unregistered".equals(userStatus) && position == getPositionForUserStatus(userStatus)) {
            baseViewHolder.setGone(R.id.ll_invite_with,false);
            baseViewHolder.setGone(R.id.ll_letter, true);
        }else{
            baseViewHolder.setGone(R.id.ll_invite_with,true);
        }

        if ("unregistered".equals(kycContactEntity.getUserStatus())){
            tvIntive.setVisibility(View.VISIBLE);
//            if ("invited".equals(kycContactEntity.getInviteStatus())) {
//                tvIntive.setTextColor(mContext.getColor(R.color.color_dcdcdc));
//                tvIntive.setEnabled(false);
//            }else if ("invite".equals(kycContactEntity.getInviteStatus())){
//                tvIntive.setTextColor(mContext.getColor(R.color.red2));
//                tvIntive.setEnabled(true);
//            }
        }else {
            tvIntive.setVisibility(View.GONE);
        }


        if (kycContactEntity.isChecked) {
            baseViewHolder.setGone(R.id.iv_checked, false);
        } else {
            baseViewHolder.setGone(R.id.iv_checked, true);
        }

        String name = kycContactEntity.getContactsName();
        if (TextUtils.isEmpty(name)) {
            baseViewHolder.setGone(R.id.tv_user_name, true);
        } else {
            baseViewHolder.setGone(R.id.tv_user_name, false);
            baseViewHolder.setText(R.id.tv_user_name, name);
        }
        baseViewHolder.setText(R.id.tv_phone, kycContactEntity.getPhone());
        ImageView ivAvatar = baseViewHolder.getView(R.id.iv_avatar);
        String avatarUrl = kycContactEntity.getHeadIcon();
        String gender = kycContactEntity.getSex();
        if (!TextUtils.isEmpty(avatarUrl)) {
            Glide.with(mContext).clear(ivAvatar);
            Glide.with(mContext)
                    .asBitmap()
                    .load(avatarUrl)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .dontAnimate()
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(new BitmapImageViewTarget(ivAvatar) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                            ivAvatar.setImageBitmap(bitmap);
                        }
                    });
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivAvatar);
        }
        setItemStatus(baseViewHolder, true, true);

        baseViewHolder.addOnClickListener(R.id.iv_avatar);
        baseViewHolder.addOnClickListener(R.id.tv_user_name);
        baseViewHolder.addOnClickListener(R.id.tv_phone);
        baseViewHolder.addOnClickListener(R.id.tv_invite);
    }


    private void setItemStatus(BaseViewHolder baseViewHolder, boolean showText, boolean showWhatApp) {
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        KycContactEntity kycContactEntity = getItem(position);
        if (kycContactEntity != null) {
            return kycContactEntity.getSortLetter().charAt(0);
        }
        return -1;
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mDataList.get(i).getSortLetter();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 根据当前位置获取是否注册 -- 未注册 - unregistered  已注册-registered
     */
    public String getUserStatusForPosition(int position) {
        KycContactEntity kycContactEntity = getItem(position);
        if (kycContactEntity != null) {
            return kycContactEntity.getUserStatus();
        }
        return "";
    }

    public int getPositionForUserStatus(String userStatus) {
        for (int i = 0; i < getItemCount(); i++) {
            String dateStr = mDataList.get(i).getUserStatus();
            if (!TextUtils.isEmpty(dateStr) && dateStr.equals(userStatus)) {
                return i;
            }
        }
        return -1;
    }
}
