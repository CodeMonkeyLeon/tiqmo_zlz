package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatSendMoneyEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseItemDataBinder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class RightTransferMessageBinder extends BaseItemDataBinder<MessageEntity> {
    private String searchTerm = "";
    @Override
    public int getViewType() {
        return MessageAdapter.RightTransferMessage;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_chat_right_send_money;
    }

    @Override
    public void bindData(BaseViewHolder baseViewHolder, MessageEntity messageEntity, int position) {
        baseViewHolder.setText(R.id.tv_right_time, TimeUtils.getChatTime(messageEntity.getTimestamp()));
        baseViewHolder.setText(R.id.tv_right_name, messageEntity.getUser().getName());
        baseViewHolder.setText(R.id.tv_right_message, messageEntity.getSimpleMessageToDisplay(searchTerm));
        baseViewHolder.setBackgroundRes(R.id.con_reply_msg, ThemeSourceUtils.getSourceID(mContext,R.attr.shape_071b55_white_16));
        ChatTransferEntity chatTransferEntity = messageEntity.getChatTransferEntity();
        if(chatTransferEntity != null){
            ChatSendMoneyEntity chatSendMoneyEntity = chatTransferEntity.transferMoney;
            if(chatSendMoneyEntity != null) {
                String sceneType = chatSendMoneyEntity.sceneType;
                if("8".equals(sceneType)){
                    baseViewHolder.setGone(R.id.view_request_money_message,false);
                    setRequestTransfer(baseViewHolder,chatSendMoneyEntity);
                }else if("7".equals(sceneType)){
                    baseViewHolder.setGone(R.id.view_send_money_message,false);
                    baseViewHolder.setText(R.id.tv_send_name, mContext.getString(R.string.sprint21_10));
                    baseViewHolder.setText(R.id.tv_send_message, chatSendMoneyEntity.orderAmount +" "+ chatSendMoneyEntity.orderCurrencyCode);
                }
            }
            baseViewHolder.addOnClickListener(R.id.tv_send_show_details);
        }else{
            baseViewHolder.setGone(R.id.view_request_money_message,true);
            baseViewHolder.setGone(R.id.view_send_money_message,true);
        }
    }

    private void setRequestTransfer(BaseViewHolder baseViewHolder,ChatSendMoneyEntity chatSendMoneyEntity){
        TextView tvRight = baseViewHolder.getView(R.id.tv_right);
        TextView tvLeft = baseViewHolder.getView(R.id.tv_left);
        TextView tvRequestStatus = baseViewHolder.getView(R.id.tv_request_status);
        LinearLayout llBottom = baseViewHolder.getView(R.id.ll_bottom);
        baseViewHolder.setText(R.id.tv_send_name, mContext.getString(R.string.sprint21_11));
        String remindStatus = chatSendMoneyEntity.remindStatus;
        String payStatus = chatSendMoneyEntity.payStatus;
        String declineStatus = chatSendMoneyEntity.declineStatus;
        baseViewHolder.setText(R.id.tv_request_status, mContext.getString(R.string.sprint21_18));
        AndroidUtils.setDrawableStart(mContext,tvRequestStatus,R.drawable.l_chat_request_waiting);
        tvRight.setVisibility(View.INVISIBLE);
        baseViewHolder.addOnClickListener(R.id.tv_left);
        if("1".equals(remindStatus)){
            tvLeft.setEnabled(false);
            tvLeft.setText(mContext.getText(R.string.sprint21_16));
        }else {
            tvLeft.setEnabled(true);
            tvLeft.setText(mContext.getText(R.string.sprint21_15));
        }

        if("1".equals(payStatus)){
            baseViewHolder.setText(R.id.tv_request_status, mContext.getString(R.string.sprint17_16));
            AndroidUtils.setDrawableStart(mContext,tvRequestStatus,R.drawable.l_chat_request_paid);
            llBottom.setVisibility(View.GONE);
        }else {
            llBottom.setVisibility(View.VISIBLE);
        }

        if("1".equals(declineStatus)){
        }else {
        }
        baseViewHolder.setText(R.id.tv_send_message, chatSendMoneyEntity.orderAmount +" "+ chatSendMoneyEntity.orderCurrencyCode);
    }
}
