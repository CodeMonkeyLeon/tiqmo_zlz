package cn.swiftpass.wallet.tiqmo.base.view;

public interface BasePresenter<V extends BaseView> {

    void attachView(V v);

    void detachView();
}
