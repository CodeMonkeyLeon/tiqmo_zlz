package cn.swiftpass.wallet.tiqmo.module.voucher.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class EVoucherPresenter implements EVoucherContract.Presenter {

    private EVoucherContract.View mView;

    @Override
    public void getLimit(String type, CheckOutEntity checkOutEntity) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mView, false) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (mView != null) {
                    mView.getLimitSuccess(result, checkOutEntity);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void requestVoucherInfo(String providerCode, String brandCode, String countryCode, String sceneType) {
        AppClient.getInstance().requestVoucherTypeAndInfo(providerCode, brandCode, countryCode, sceneType,
                new LifecycleMVPResultCallback<VouTypeAndInfoEntity>(mView, true) {
                    @Override
                    protected void onSuccess(VouTypeAndInfoEntity result) {
                        if (mView != null) {
                            mView.showVoucherList(result);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void eVoucherOrder(String voucherCode, String voucherAmount) {
        AppClient.getInstance().eVoucherOrder(voucherCode, voucherAmount, new LifecycleMVPResultCallback<EVoucherOrderInfoEntity>(mView, true) {

            @Override
            protected void onSuccess(EVoucherOrderInfoEntity result) {
                if (mView != null) {
                    mView.eVoucherOrderSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(mView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                mView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(EVoucherContract.View view) {
        this.mView = view;
    }

    @Override
    public void detachView() {
        this.mView = null;
    }
}
