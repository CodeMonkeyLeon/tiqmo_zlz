package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import android.view.View;

import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;

public interface MessageLongPressListener {
    void onMessageLongPress(View view, MessageEntity msg);
}
