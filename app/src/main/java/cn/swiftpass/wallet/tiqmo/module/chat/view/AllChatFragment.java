package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gc.gcchat.GcChatError;
import com.gc.gcchat.GcChatGetSessionsRequest;
import com.gc.gcchat.GcChatMessage;
import com.gc.gcchat.GcChatParticipant;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.gc.gcchat.GcChatUser;
import com.gc.gcchat.callback.CreateGroupCallback;
import com.gc.gcchat.callback.CreateOneToOneChatCallback;
import com.gc.gcchat.callback.GcChatEventListener;
import com.gc.gcchat.callback.GetSessionListCallback;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.chat.Contract.ChatContract;
import cn.swiftpass.wallet.tiqmo.module.chat.Presenter.ChatPresenter;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.ChatHistoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatContactDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.NewGroupDialog;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.GcChatErrorUtils;
import cn.swiftpass.wallet.tiqmo.support.chat.UserUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ContactUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class AllChatFragment extends BaseFragment<ChatContract.ChatPresenter> implements ChatContract.ChatView {
    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;
    @BindView(R.id.ll_content_empty)
    LinearLayout llContentEmpty;
    @BindView(R.id.rl_chat_history)
    RecyclerView rlChatHistory;
    @BindView(R.id.iv_no_chat_list)
    ImageView ivNoChatList;
    @BindView(R.id.tv_no_chat_list)
    TextView tvNoChatList;
    @BindView(R.id.tv_no_chat_list_two)
    TextView tvNoChatListTwo;
    @BindView(R.id.iv_no_chat_line)
    ImageView ivNoChatLine;
    @BindView(R.id.iv_add_chat_new)
    ImageView ivAddChatNew;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private List<KycContactEntity> contactInviteDtos;
    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;
    //通讯录联系人List
    public List<KycContactEntity> phoneList = new ArrayList<>();
    private ChatContactDialog chatContactDialog;
    private final ArrayList<GcChatSession> chatSessionArrayList = new ArrayList<>();
    private ChatHistoryAdapter chatHistoryAdapter;
    private NewGroupDialog groupDialog;

    /**
     * 群聊
     */
    private String mGroupName;
    private final List<GcChatParticipant> groupParticipantList = new ArrayList<>();

    private GcChatEventListener gcChatEventListener = null;
    //1为所有聊天 2为单聊 3为群聊
    private int chatType = 1;

    private int chooseContactType = 1;


    public static AllChatFragment newInstance(int chatType) {
        AllChatFragment fragment = new AllChatFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("chatType",chatType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_chat_history;
    }

    @Override
    protected void initView(View parentView) {
        if(getArguments() != null){
            chatType = getArguments().getInt("chatType");
            chooseContactType = chatType;
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.color_B00931);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setDistanceToTriggerSync(200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getChatSessions();
            }
        });
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_030b28_white)), AndroidUtils.dip2px(mContext, 0));
//        rlChatHistory.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rlChatHistory.setLayoutManager(manager);
        chatHistoryAdapter = new ChatHistoryAdapter(chatSessionArrayList);
        chatHistoryAdapter.bindToRecyclerView(rlChatHistory);
        chatHistoryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                GcChatSession gcChatSession = (GcChatSession) baseRecyclerAdapter.getDataList().get(position);
                if(gcChatSession != null){
                    HashMap<String,Object> hashMap = new HashMap<>();
                    hashMap.put(Constants.gcChatSession, gcChatSession);
                    ActivitySkipUtil.startAnotherActivity(mActivity,ChatRoomActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
        gcChatEventListener = new GcChatEventListener() {
            @Override
            public void onNewMessageReceived(GcChatMessage chatMessage) {
                LogUtils.d("onNewMessageReceived for %s and %s", chatMessage.getSessionId()+" "+new Gson().toJson(chatMessage));
                for (int i = 0; i < chatSessionArrayList.size(); i++) {
                    GcChatSession gcChatSession = chatSessionArrayList.get(i);
                    if (chatMessage.getSessionId().equals(gcChatSession.getSessionId())) {
                        gcChatSession.setLastMessage(chatMessage);
                        chatHistoryAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }

            @Override
            public void onMessageUpdated(GcChatMessage chatMessage) {

            }

            @Override
            public void onMessageDeleted(String messageId) {
                for (int i = 0; i < chatSessionArrayList.size(); i++) {
                    GcChatSession gcChatSession = chatSessionArrayList.get(i);
                    if (gcChatSession.getLastMessage() != null && gcChatSession.getLastMessage().getMessageId().equals(messageId)) {
                        gcChatSession.setLastMessage(null);
                        chatHistoryAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }

            @Override
            public void onNewChatInvitation(String s, GcChatUser invitedByUser, @Nullable String groupName) {
                String messageText = "You got an invitation to join chat from "+ invitedByUser.getName();
                if(groupName!=null){
                    messageText+=" for "+groupName+" group";
                }
//                showTipDialog(messageText);
                getChatSessions();
            }

            @Override
            public void onInvitationResponded(boolean accepted, @NonNull String sessionId, @NonNull GcChatUser respondedByUser, @NonNull GcChatUser invitedByUser, @Nullable String groupName) {
                if(invitedByUser.getExternalId().equals(UserUtils.loggedInUserId)){
                    String messageText = "";
                    if (accepted) {
                        messageText = "Your invitation was accepted by " + respondedByUser.getName();
                    } else {
                        messageText = "Your invitation was rejected by " + respondedByUser.getName();
                    }
                    if (groupName != null) {
                        messageText += " for " + groupName + " group";
                    }
                    showTipDialog(messageText);
                }
                else {
                    String messageText = "";
                    if (accepted) {
                        messageText = invitedByUser.getName() +" added  " + respondedByUser.getName();
                        if (groupName != null) {
                            messageText += " in " + groupName + " group";
                        }
                        showTipDialog(messageText);
                    }
                }
                for (GcChatSession gcChatSession : chatSessionArrayList){
                    if(gcChatSession.getSessionId().equals(sessionId)){
                        GcChatParticipant respondedByGcChatParticipant = null;
                        for (GcChatParticipant gcChatParticipant : gcChatSession.getParticipantUsers()){
                            if(gcChatParticipant.getUser().getExternalId().equals(respondedByUser.getExternalId())){
                                respondedByGcChatParticipant = gcChatParticipant;
                            }
                        }
                        if(respondedByGcChatParticipant != null){
                            if(accepted){
                                respondedByGcChatParticipant.setInvited(false);
                            }
                            else {
                                gcChatSession.getParticipantUsers().remove(respondedByGcChatParticipant);
                            }
                            chatHistoryAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onUserLeftGroup(@NonNull GcChatUser leftUser, @NonNull String sessionId, @NonNull String groupName) {
                for (GcChatSession gcChatSession : chatSessionArrayList){
                    if(gcChatSession.getSessionId().equals(sessionId)){
                        GcChatParticipant participantToRemoved = null;
                        for (GcChatParticipant gcChatParticipant : gcChatSession.getParticipantUsers()){
                            if(gcChatParticipant.getUser().getExternalId().equals(leftUser.getExternalId())) {
                                participantToRemoved = gcChatParticipant;
                            }
                        }
                        if(participantToRemoved != null){
                            gcChatSession.getParticipantUsers().remove(participantToRemoved);
                        }
                    }
                }
                showTipDialog(leftUser.getName()+ " left "+groupName);
            }

            @Override
            public void onUserRemovedFromGroup(@NonNull GcChatUser removedUser, @NonNull GcChatUser removedByUser, @NonNull String sessionId, @NonNull String groupName) {
                for (GcChatSession gcChatSession : chatSessionArrayList){
                    if(gcChatSession.getSessionId().equals(sessionId)){
                        GcChatParticipant participantToRemoved = null;
                        for (GcChatParticipant gcChatParticipant : gcChatSession.getParticipantUsers()){
                            if(gcChatParticipant.getUser().getExternalId().equals(removedUser.getExternalId())) {
                                participantToRemoved = gcChatParticipant;
                            }
                        }
                        if(participantToRemoved != null){
                            gcChatSession.getParticipantUsers().remove(participantToRemoved);
                        }
                    }
                }
                if(removedUser.getExternalId().equals(UserUtils.loggedInUserId)){
                    showTipDialog(removedByUser.getName() + " removed you from " + groupName);
                } else {
                    showTipDialog(removedByUser.getName() + " removed " + removedUser.getName() + " from " + groupName);
                }
            }

            @Override
            public void onUserInfoUpdated(@NonNull String externalId, @Nullable String name, @Nullable String imageId) {
                for (GcChatSession chatSession : chatSessionArrayList){
                    for (GcChatParticipant gcChatParticipant: chatSession.getParticipantUsers()){
                        if(gcChatParticipant.getUser().getExternalId().equals(externalId)){
                            if(name != null){
                                gcChatParticipant.getUser().setName(name);
                            }
                            if(imageId != null){
                                gcChatParticipant.getUser().setFileId(imageId);
                            }
                        }
                    }
                }
                chatHistoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onGroupSessionInfoUpdated(@NonNull String sessionId, @Nullable String groupName, @Nullable String imageId) {
                for (GcChatSession chatSession : chatSessionArrayList){
                    if (sessionId.equals(chatSession.getSessionId())){
                        if(groupName != null){
                            chatSession.setTitle(groupName);
                        }
                        if(imageId != null){
                            chatSession.setFileId(imageId);
                        }
                    }
                }
                chatHistoryAdapter.notifyDataSetChanged();
            }
        };
        GcChatSDK.addChatEventListener(gcChatEventListener);
        if(chatType == 1) {
            showProgressCancelable(true, true);
        }
        getChatSessions();
    }

    private void getChatSessions() {
        GcChatGetSessionsRequest request = new GcChatGetSessionsRequest().setTake(100).setSkip(0);
        GcChatSDK.getSessionList(request, new GetSessionListCallback() {
            @Override
            public void onSuccessful(List<GcChatSession> list, int totalCount) {
                showProgress(false);
                swipeRefreshLayout.setRefreshing(false);
                chatSessionArrayList.clear();
                if(list != null) {
                    int size = list.size();
                    if (chatType == 1) {
                        chatSessionArrayList.addAll(list);
                    } else if (chatType == 2) {
                        for(int i = 0;i<size;i++){
                            GcChatSession gcChatSession = list.get(i);
                            if(!gcChatSession.isGroupChat()){
                                chatSessionArrayList.add(gcChatSession);
                            }
                        }
                    } else {
                        for(int i = 0;i<size;i++){
                            GcChatSession gcChatSession = list.get(i);
                            if(gcChatSession.isGroupChat()){
                                chatSessionArrayList.add(gcChatSession);
                            }
                        }
                    }
                    if (chatSessionArrayList.size() == 0) {
                        swipeRefreshLayout.setVisibility(View.GONE);
                        llContentEmpty.setVisibility(View.VISIBLE);
                    } else {
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        llContentEmpty.setVisibility(View.GONE);
                        chatHistoryAdapter.setDataList(chatSessionArrayList);
                    }
                }
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                swipeRefreshLayout.setRefreshing(false);
                showTipDialog(GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gcChatEventListener != null) {
            GcChatSDK.removeChatEventListener(gcChatEventListener);
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {
        if (gcChatEventListener != null) {
            GcChatSDK.addChatEventListener(gcChatEventListener);
        }
        getChatSessions();
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundColorByAttr(bgMain,R.attr.color_030b28_f4f6f9);
        helper.setImageResourceByAttr(ivNoChatList, R.attr.icon_chat_history_empty);
        helper.setImageResourceByAttr(ivNoChatLine, R.attr.icon_chat_history_line_empty);
        helper.setImageResourceByAttr(ivAddChatNew, R.attr.icon_chat_add_new);
        helper.setTextColorByAttr(tvNoChatList, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvNoChatListTwo, R.attr.color_white_090a15);

        if(chatHistoryAdapter != null){
            chatHistoryAdapter.changeTheme();
        }
    }

    @OnClick({R.id.iv_add_chat_new})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_add_chat_new:
                if(chatType == 3){
                    showNewGroupDialog();
                }else {
                    chooseContactType = chatType;
                    checkPermission();
                }
                break;
            default:break;
        }
    }

    private void checkPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
            PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                        @Override
                        public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                            phoneList.clear();
                            phoneList.addAll(contactEntities);
                            mPresenter.contactInvite(phoneList);
                        }
                    });
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {
                        if (!firstRejectContacts) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_30));
                        } else {
                            firstRejectContacts = false;
                        }
                    }
                }
            }, Manifest.permission.READ_CONTACTS);
        } else {
            ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                @Override
                public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                    phoneList.clear();
                    phoneList.addAll(contactEntities);
                    mPresenter.contactInvite(phoneList);
                }
            });
        }
    }

    @Override
    public ChatContract.ChatPresenter getPresenter() {
        return new ChatPresenter();
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {
        if (contactInviteEntity != null) {
            contactInviteDtos = contactInviteEntity.getContactInviteDtos();

            for (int i = 0; i < contactInviteDtos.size(); i++) {
                KycContactEntity kycContactEntity = contactInviteDtos.get(i);
                if (kycContactEntity.getContactsName() != null) {
                    kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                }
            }

            Collections.sort(contactInviteDtos, new registerComparator());
            showContactDialog(contactInviteDtos);
        }
    }

    private void jumpToOneChatRoom(String chatSessionId,boolean isGroupChat){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("chatSessionId", chatSessionId);
        hashMap.put("isGroupChat", isGroupChat);
        ActivitySkipUtil.startAnotherActivity(mActivity,ChatRoomActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void showContactDialog(List<KycContactEntity> phoneList) {
        chatContactDialog = ChatContactDialog.getInstance(mContext, chooseContactType);
        chatContactDialog.setPhoneList(phoneList);
        chatContactDialog.setGroupName(mGroupName);

        chatContactDialog.setCreateNewGroupListener(new ChatContactDialog.CreateNewGroupListener() {
            @Override
            public void createNewGroup() {
                chatContactDialog.dismiss();
                showNewGroupDialog();
            }
        });

        chatContactDialog.setCreateChatRoom(new ChatContactDialog.CreateChatRoom() {
            @Override
            public void createChatRoom(String phone) {
                showProgressCancelable(true,true);
                ChatHelper.getInstance().createNewOneToOneChat(phone, new CreateOneToOneChatCallback() {
                    @Override
                    public void onSuccessful(String sessionId) {
                        showProgress(false);
                        jumpToOneChatRoom(sessionId,false);
                        getChatSessions();
                    }

                    @Override
                    public void onFailure(int errorCode, @Nullable String sessionId) {
                        showProgress(false);
                        if(errorCode == GcChatError.SESSION_ALREADY_EXIST){
                            jumpToOneChatRoom(sessionId,false);
                        }
                    }
                });
                //创建个人聊天
                chatContactDialog.dismiss();
            }
        });

        chatContactDialog.setOnAddClickListener(new ChatContactDialog.OnAddClickListener() {
            @Override
            public void getSplitList(List<KycContactEntity> peopleList) {
                //创建群聊
                checkAndCreateGroup(peopleList);
            }
        });

        chatContactDialog.setTvInviteClickListener(new ChatContactDialog.TvInviteClickListener() {
            @Override
            public void sendInvite(String phone) {

                try {
                    String msg = mContext.getString(R.string.sprint20_67);
                    String[] s = getUserInfoEntity().userName.split(" ");
                    String body = msg.replace("XXX", s[0]);
                    AndroidUtils.sendSmsWithBody(mContext, BuildConfig.DefaultInternationalAreaCode + phone, body);
                }catch (Throwable e){
                    e.printStackTrace();
                }
//
            }
        });

        chatContactDialog.showWithBottomAnim();
    }

    private void showNewGroupDialog() {
        groupDialog = new NewGroupDialog(mContext);
        groupDialog.setNewGroupListener(new NewGroupDialog.NewGroupListener() {
            @Override
            public void createNewGroupChat(String groupName) {
                chooseContactType = 3;
                mGroupName = groupName;
                checkPermission();
                groupDialog.dismiss();
            }
        });

        groupDialog.showWithBottomAnim();

    }

    private void checkAndCreateGroup(List<KycContactEntity> peopleList) {
        int size = peopleList.size();
        if(size>99){
            showChatConfirmDialog(getString(R.string.sprint20_90), "", getString(R.string.common_7), getString(R.string.sprint20_91),
                    new ChatConfirmDialog.ConfirmListener() {
                        @Override
                        public void clickLeft() {

                        }

                        @Override
                        public void clickRight() {
                            dissmissChatConfirmDialog();
                        }
                    });
            return;
        }
        if(chatContactDialog != null){
            chatContactDialog.dismiss();
        }
        for(int i = 0;i < size;i++){
            KycContactEntity kycContactEntity = peopleList.get(i);
            String userId = kycContactEntity.getUserId();
            String name = kycContactEntity.getContactsName();
            boolean isNumberAlreadyAdded = false;
            for (GcChatParticipant gcChatParticipant : groupParticipantList) {
                if (userId.equals(gcChatParticipant.getUser().getExternalId())) {
                    isNumberAlreadyAdded = true;
                    break;
                }
            }
            if (!isNumberAlreadyAdded) {
                GcChatParticipant newGcChatParticipant = new GcChatParticipant();
                GcChatUser newUser = new GcChatUser();
                newUser.setName(name);
                newUser.setExternalId(userId);
                newUser.setGcUserId(kycContactEntity.getPhone());
                newGcChatParticipant.setUser(newUser);
                groupParticipantList.add(newGcChatParticipant);
            } else {
//                showTipDialog("Member with number " + userId + " already added!!");
            }
        }

        ArrayList<String> memberList = new ArrayList<>();
        for (GcChatParticipant gcChatParticipant: groupParticipantList){
            memberList.add(gcChatParticipant.getUser().getExternalId());
        }

        showProgressCancelable(true,true);
        GcChatSDK.createGroup(memberList, mGroupName, "", new CreateGroupCallback() {
            @Override
            public void onSuccessful(String sessionId) {
                showProgress(false);
                if(chatContactDialog != null){
                    chatContactDialog.dismiss();
                }
                if(groupDialog != null){
                    groupDialog.dismiss();
                }
                jumpToOneChatRoom(sessionId,true);
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                showTipDialog("Error in creating group. Please try again!! - "+ GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }


    /**
     * 根据是否注册来排序
     */
    public class registerComparator implements Comparator<KycContactEntity> {

        @Override
        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getUserStatus().equals(Constants.unregistered) || o2.getUserStatus().equals(Constants.registered)) {
                return 1;
            } else if (o1.getUserStatus().equals(Constants.registered) || o2.getUserStatus().equals(Constants.unregistered)) {
                return -1;
            }
            return 0;
        }
    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
