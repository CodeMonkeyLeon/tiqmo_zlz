package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class CustomMsgDialog extends Dialog {

    public CustomMsgDialog(Context context) {
        super(context);
    }

    public CustomMsgDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String message;
        private String title;
        private String positiveButtonText;
        private View contentView;
        private OnClickListener positiveButtonClickListener;
        private int textSize;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }
 public Builder setTextSize(int size) {
            this.textSize = size;
            return this;
        }

        //gravity
        int gravity;

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }


        public CustomMsgDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomMsgDialog dialog = new CustomMsgDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_msg_layout, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title
            // set the confirm button
            if (positiveButtonText != null) {
                ((Button) layout.findViewById(R.id.positive_dl)).setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    layout.findViewById(R.id.positive_dl).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(R.id.positive_dl).setVisibility(View.GONE);
            }
            // set the content message
            if (message != null) {
                TextView msgTv = (TextView) layout.findViewById(R.id.message_dl);
                if (textSize >10){
                    msgTv.setTextSize(textSize);
                }
                msgTv.setText(message.trim());
                if (0 != gravity) {
                    msgTv.setGravity(gravity);
                }
            }
            if (title != null) {
                TextView titleTv = (TextView) layout.findViewById(R.id.id_title);
                if (textSize >10){
                    titleTv.setTextSize(textSize);
                }
                titleTv.setText(title);
                ((LinearLayout) layout.findViewById(R.id.id_center_view)).setBackgroundResource(R.drawable.dialog_center_bg);
            } else {
                ((LinearLayout) layout.findViewById(R.id.id_title_layout)).setVisibility(View.GONE);
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }
}

