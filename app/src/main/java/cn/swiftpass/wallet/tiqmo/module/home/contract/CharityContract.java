package cn.swiftpass.wallet.tiqmo.module.home.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;

public class CharityContract {

    public interface CharityPresenter extends BasePresenter<CharityView> {
        void preCharityOrder(String channelCode,String payAmount,String domainDonation);

        void checkOut(String orderNo, String orderInfo);
    }

    public interface CharityView extends BaseView<CharityPresenter> {
        void preCharityOrderSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity);
        void preCharityOrderFail(String errorCode,String errorMsg);

        void checkOutSuccess(CheckOutEntity checkOutEntity);

        void checkOutFail(String errorCode, String errorMsg);
    }
}
