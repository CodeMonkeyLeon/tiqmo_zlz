/*
 * Copyright (C) Globalegrow E-Commerce Co. , Ltd. 2007-2015.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Globalegrow E-Commerce Co. , Ltd. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Globalegrow.
 */
package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class SetBranchActivity extends BaseCompatActivity {

    private static final String k8s_dev_url = "https://jed-dev-k8s-wallet-api.tiqmopayment.com/";
    private static final String k8s_test_url = "https://jed-test-k8s-wallet-api.tiqmopayment.com/";
    private static final String oci_test_url = "https://wallet-api-test-oci.tiqmopayment.com/";
    private static final String oci_sit_url = "https://wallet-api-sit-oci.tiqmopayment.com/";
    private static final String dev_url = "http://wallet-saas-dev.wallyt.net/tiqmo/";
    private static final String dev_yanzheng_url = "http://wallet-saas-dev.wallyt.net/tiqmo_yanzheng/";

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.et_partnerNo)
    EditTextWithDel etPartnerNo;
    @BindView(R.id.et_url)
    EditTextWithDel etUrl;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.rb_k8s_dev)
    RadioButton rbK8sDev;
    @BindView(R.id.rb_k8s_test)
    RadioButton rbK8sTest;
    @BindView(R.id.rb_oci_sit)
    RadioButton rbOciSit;
    @BindView(R.id.rb_oci_test)
    RadioButton rbOciTest;
    @BindView(R.id.rb_dev)
    RadioButton rbDev;
    @BindView(R.id.rb_dev_yanzheng)
    RadioButton rbDevYanzheng;
    @BindView(R.id.rg_setting)
    RadioGroup rgSetting;

    private String lastDevelopUrl = "";

    @Override
    protected int getLayoutID() {
        return R.layout.activity_choose_url;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        lastDevelopUrl = SpUtils.getInstance().getLastDevelopUrl();
        etPartnerNo.getEditText().setText("96601");
        if (TextUtils.isEmpty(lastDevelopUrl)) {
            setServerUrl(BuildConfig.ServerUrl);
        } else {
            setServerUrl(lastDevelopUrl);
        }

        rbK8sDev.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etUrl.setContentText(k8s_dev_url);
                }
            }
        });
        rbK8sTest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etUrl.setContentText(k8s_test_url);
                }
            }
        });
        rbOciSit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etUrl.setContentText(oci_sit_url);
                }
            }
        });
        rbOciTest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etUrl.setContentText(oci_test_url);
                }
            }
        });
        rbDev.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etUrl.setContentText(dev_url);
                }
            }
        });
        rbDevYanzheng.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etUrl.setContentText(dev_yanzheng_url);
                }
            }
        });
    }

    private void initWallet() {
        String url = etUrl.getEditText().getText().toString().trim();
        String partnerNo = etPartnerNo.getEditText().getText().toString().trim();
        if (TextUtils.isEmpty(url)) {
            url = BuildConfig.ServerUrl;
        }
        if (TextUtils.isEmpty(partnerNo)) {
            partnerNo = "96601";
        }
        AppClient.init(this,
                url + BuildConfig.RequestPath,
                url + BuildConfig.UploadRequestPath,
                partnerNo,
                BuildConfig.OSType,
                BuildConfig.DefaultLanguage,
                BuildConfig.DefaultInternationalAreaCode,
                AndroidUtils.getCurrentAppVersionName(mContext),
                BuildConfig.isEnableEncryptMode,
                BuildConfig.serverPublicKey);
    }

    @OnClick({R.id.iv_back, R.id.tv_confirm, R.id.tv_restore})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                lastDevelopUrl = etUrl.getEditText().getText().toString().trim();
                SpUtils.getInstance().setLastDevelopUrl(lastDevelopUrl);
//                AppClient.getInstance().getUserManager().clearLastUserInfo();
                initWallet();
                finish();
                break;
            case R.id.tv_restore:
                setServerUrl(BuildConfig.ServerUrl);
                break;
            default:
                break;
        }
    }

    private void setServerUrl(String url){
        etUrl.getEditText().setText(url);
        if(k8s_dev_url.equals(url)){
            rbK8sDev.setChecked(true);
            etUrl.setContentText(k8s_dev_url);
        }
        if(k8s_test_url.equals(url)){
            rbK8sTest.setChecked(true);
            etUrl.setContentText(k8s_test_url);
        }
        if(oci_sit_url.equals(url)){
            rbOciSit.setChecked(true);
            etUrl.setContentText(oci_sit_url);
        }
        if(oci_test_url.equals(url)){
            rbOciTest.setChecked(true);
            etUrl.setContentText(oci_test_url);
        }
        if(dev_url.equals(url)){
            rbDev.setChecked(true);
            etUrl.setContentText(dev_url);
        }
        if(dev_yanzheng_url.equals(url)){
            rbDevYanzheng.setChecked(true);
            etUrl.setContentText(dev_yanzheng_url);
        }
    }
}
