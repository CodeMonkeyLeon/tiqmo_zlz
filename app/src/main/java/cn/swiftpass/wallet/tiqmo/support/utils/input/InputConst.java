package cn.swiftpass.wallet.tiqmo.support.utils.input;

/**
 * 输出相关的常量
 */
public interface InputConst {
    String CHINESE = "\\u4E00-\\u9FFF";
    String ENGLISH = "a-zA-Z";
    String SPECIAL_CHAR = "`~!\\-_@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？";
    String EMPTY = " ";
    String NUMBER = "0-9";
}
