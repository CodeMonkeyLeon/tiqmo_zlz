package cn.swiftpass.wallet.tiqmo.sdk.util;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomProgressDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * App语言包下载
 */
public class LanguageDownloadManager {
    private static LanguageDownloadManager instance;
    private static final Object INSTANCE_LOCK = new Object();
    private static final String TAG = "LanguageDownloadManager";
    private Certificate[] mCertificates;
    private CustomProgressDialog progressDialog;
    private String version;
    /**
     * 下载压缩语言包的连接
     */
    private String DOWNLOADCERTURL = BuildConfig.ServerUrl + "cms/multiLanguage/download?partnerNo=" + BuildConfig.partnerNo + "&version=" + version + "&osType=AOS";

    private LanguageDownloadManager() {
    }

    private static class LanguageDownloadHolder {
        public static LanguageDownloadManager languageDownloadInstance = new LanguageDownloadManager();
    }

    public static LanguageDownloadManager getInstance() {
        return LanguageDownloadHolder.languageDownloadInstance;
    }

    /**
     * 只有当证书下载成功了 才可以直接调用这个方法  主要是为了解决activity传参无法序列化问题 需要内存存放
     *
     * @return
     */
    public Certificate[] getDownloadCerts() {

        return mCertificates;
    }


    /**
     * 证书初始化，提前下载证书配置
     */
    public void initCertificate(Context mcontext,String version,OnCertDownLoadListener onCertDownLoadListener) {
        this.version = version;
        DOWNLOADCERTURL = BuildConfig.ServerUrl + "cms/multiLanguage/download?partnerNo=" + BuildConfig.partnerNo + "&version=" + version + "&osType=AOS";
        loadCertificates(mcontext, false, onCertDownLoadListener);
    }

    /**
     * 异步拉取证书
     *
     * @param mContext
     * @param showDialog             是否显示dialog阻塞
     * @param onCertDownLoadListener
     */
    public void loadCertificates(Context mContext, boolean showDialog, OnCertDownLoadListener onCertDownLoadListener) {

        if (mCertificates != null && mCertificates.length > 0) {
            LogUtils.i(TAG, "Certificates ALREADY EXIT SUCCESS BACK");
            onCertDownLoadListener.onCertsDownLoaFinish(true, mCertificates);
        } else {
            if (!NetworkUtil.isNetworkConnected(mContext)) {
                LogUtils.i(TAG, "Certificates NOT EXIT NETWORK ERROR");
                if (onCertDownLoadListener != null) {
                    onCertDownLoadListener.onCertsDownLoaFinish(false, null);
                }
                return;
            }
            LogUtils.i(TAG, "Certificates NOT EXIT START DOWN");
            new DownLoadCertsTask(onCertDownLoadListener, mContext, showDialog).execute();
        }
    }

    /**
     * 耗时任务读取通讯录
     */
    private class DownLoadCertsTask extends AsyncTask<Void, Void, Certificate[]> {
        private OnCertDownLoadListener onCertDownLoadListener;

        private boolean showDialog;
        private Context mContext;

        public DownLoadCertsTask(OnCertDownLoadListener onCertDownLoadListener, Context mContext, boolean showDialogIn) {
            this.onCertDownLoadListener = onCertDownLoadListener;
            this.showDialog = showDialogIn;
            this.mContext = mContext;
        }

        @Override
        protected Certificate[] doInBackground(Void... voids) {
            LogUtils.i(TAG, " down load doInBackground ->" + Thread.currentThread().getName());
            return downLoadCertificates();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtils.i(TAG, "down load onPreExecute->");
            //看需不需要加载框显示
            if (showDialog && mContext != null) {
//                showDialogNotCancel(mContext);
            }

        }

        @Override
        protected void onPostExecute(Certificate[] contactEntities) {
            super.onPostExecute(contactEntities);
            if (contactEntities != null && contactEntities.length > 0) {
                LogUtils.i(TAG, "down load onPostExecute->");
            }
            if (onCertDownLoadListener != null) {
                //证书下载成功 缓存到内存中
                mCertificates = contactEntities;
                onCertDownLoadListener.onCertsDownLoaFinish(contactEntities != null, contactEntities);
                if (showDialog && mContext != null) {
                    dismissDialog();
                }
            }
        }
    }

    /**
     * 加锁 阻塞 防止并发请求
     *
     * @return
     */
    private synchronized Certificate[] downLoadCertificates() {
        if (mCertificates != null && mCertificates.length > 0) {
            return mCertificates;
        }
        LogUtils.i(TAG, "downLoadCertificates start ->"+DOWNLOADCERTURL);

        //1.通过证书下载地址进行证书下载
        OkHttpClient okHttpClient = getHttpsClient();
        Request request = new Request.Builder()
                .url(DOWNLOADCERTURL)
                .addHeader("Connection", "close")
                .build();
        List<Certificate> certificates = new ArrayList<>();
        ZipInputStream zin = null;
        FileOutputStream out = null;
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful() && response.body() != null) {
                zin = new ZipInputStream(response.body().byteStream());
                ZipEntry entry;
                //如果entry不为空，并不在同一个目录下
                while (zin != null && ((entry = zin.getNextEntry()) != null)) {
                    //解压出的文件路径
                    if (!entry.isDirectory()) {
//                        String fileName = entry.getName();

                        String outFileName =
                                AndroidUtils.validateFilenameInDir(entry.getName(), ".");
                        //byte[] fileContent = entry.getExtra();
                        LogUtils.i(TAG, "fileName: " + outFileName);

                        File file = new File(ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + outFileName);
                        file.createNewFile();
                        LogUtils.i(TAG, "fileName: " + file.getAbsolutePath());
                        out = new FileOutputStream(file);
                        int len;
                        byte[] buffer = new byte[1024];
                        while ((len = zin.read(buffer)) != -1) {
                            out.write(buffer, 0, len);
                            out.flush();
                        }
                        zin.closeEntry();
                        SpUtils.getInstance().setLanguageVersion(version);
                        LogUtils.i(TAG, "downLoadCertificates success->");
                    }
                }
            } else {
//                //网络或者解压缩问题失败
                LogUtils.i(TAG, "downLoadCertificates failed->");
            }
        } catch (Exception e) {
            try {
                if (out != null) {
                    out.close();
                }
                if (zin != null) {
                    zin.close();
                }
            } catch (Exception ex) {
                if (BuildConfig.DEBUG) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (zin != null) {
                    zin.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        mCertificates = certificates.toArray(new Certificate[certificates.size()]);
        return mCertificates;
    }

    private OkHttpClient getHttpsClient() {
        OkHttpClient.Builder okhttpClient = new OkHttpClient().newBuilder();
        okhttpClient.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession sslSession) {
                if(BuildConfig.ServerUrl.contains(hostname)) {
                    return true;
                }
                return false;
            }
        });
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] x509Certificates,
                    String s) throws java.security.cert.CertificateException {
            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] x509Certificates,
                    String s) throws java.security.cert.CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
//为OkHttpClient设置sslSocketFactory
            okhttpClient.sslSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return okhttpClient.build();
    }


    /**
     * 获取条目byte[]字节
     *
     * @param zis
     * @return
     */
    public byte[] getByte(InflaterInputStream zis) {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] temp = new byte[1024];
            byte[] buf = null;
            int length = 0;
            while ((length = zis.read(temp, 0, 1024)) != -1) {
                bout.write(temp, 0, length);
            }
            buf = bout.toByteArray();
            bout.close();
            return buf;
        } catch (IOException e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
            return null;
        }
    }


    public void dismissDialog() {
        if (progressDialog == null) {
            return;
        }
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
    }


    public void showDialogNotCancel(Context context) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.common_34),R.style.CustomProgressDialog);
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        } else {
            if (progressDialog != null) {
                try {
                    Activity activity = (Activity) context;
                    if (!activity.isFinishing()) {
                        progressDialog.setCancel(false);
                        progressDialog.show();
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, e.getMessage());
                }

            }
        }

    }


    public interface OnCertDownLoadListener {
        void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates);
    }

}
