package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PaybillLocalAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListNewEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillTypePresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class PayBillLocalActivity extends BaseCompatActivity<PayBillContract.BillTypePresenter> implements PayBillContract.BillTypeView {

    public static final String TAG = "PayBillLocalActivity";

    public static final String TYPE_BILL_SERVICES = "Bill Services";
    public static final String TYPE_MOI_SERVICES = "MOI Services";

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_local_paybill)
    RecyclerView rvLocalPaybill;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private PayBillTypeListNewEntity payBillCountryEntity;
    private String mChannelCode = Constants.paybill_local;
    private String mCountryCode;
    private PaybillLocalAdapter paybillLocalAdapter;

    private List<PayBillTypeEntity> mPayBillList = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_local;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_6);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        if (getIntent() != null && getIntent().getExtras() != null) {
            payBillCountryEntity = (PayBillTypeListNewEntity) getIntent().getExtras().getSerializable(Constants.payBillCountryEntity);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);
            mCountryCode = getIntent().getStringExtra(Constants.COUNTRY_CODE);
            if (payBillCountryEntity != null) {
                mPayBillList.clear();
                mPayBillList.addAll(payBillCountryEntity.getBillTypeList());

                LinearLayoutManager manager = new LinearLayoutManager(mContext);
                MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
                rvLocalPaybill.addItemDecoration(myItemDecoration);
                manager.setOrientation(RecyclerView.VERTICAL);
                rvLocalPaybill.setLayoutManager(manager);
                paybillLocalAdapter = new PaybillLocalAdapter(mPayBillList);
                paybillLocalAdapter.bindToRecyclerView(rvLocalPaybill);

                paybillLocalAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                        PayBillTypeEntity payBillTypeEntity = paybillLocalAdapter.getDataList().get(position);
                        LogUtils.i(TAG, "本地帐单 : " + payBillTypeEntity);
                        if (TextUtils.equals(TYPE_BILL_SERVICES, payBillTypeEntity.billerType)) {
                            //Bill Service
                            BillServiceActivity.startBillServiceActivity(PayBillLocalActivity.this, payBillTypeEntity, mCountryCode, mChannelCode, payBillTypeEntity.billerType);
                        } else if (TextUtils.equals(TYPE_MOI_SERVICES, payBillTypeEntity.billerType)) {
                            //MOI Service
                            MOIServiceActivity.startMOIServiceActivity(PayBillLocalActivity.this, payBillTypeEntity, mCountryCode, mChannelCode, payBillTypeEntity.billerType);
                        }
                    }
                });
            }
        }
    }

    @OnClick(R.id.iv_back)
    public void onClick() {
        finish();
    }

    @Override
    public void getPayBillTypeSuccess(PayBillTypeListEntity payBillTypeListEntity) {
        if (payBillTypeListEntity != null) {
            List<PayBillTypeEntity> typeList = payBillTypeListEntity.billTypeList;
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillTypePresenter getPresenter() {
        return new PayBillTypePresenter();
    }
}
