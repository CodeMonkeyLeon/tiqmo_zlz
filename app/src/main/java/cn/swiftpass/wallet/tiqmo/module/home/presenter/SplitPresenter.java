package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.contract.SplitBillContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SplitPresenter implements SplitBillContract.SplitPresenter {
    SplitBillContract.SplitView mView;
    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (mView == null) {
                    return;
                }
                mView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView == null) {
                    return;
                }
                mView.showError(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getSplit(String splitType) {
        AppClient.getInstance().getSplit(splitType, new LifecycleMVPResultCallback<SplitListEntity>(mView,true) {
            @Override
            protected void onSuccess(SplitListEntity result) {
                if (mView != null) {

                    mView.getSplitSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showError(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void sendNotify(List<NotifyEntity> receivers) {
        AppClient.getInstance().sendNotify(receivers, new LifecycleMVPResultCallback<PayBillOrderInfoEntity>(mView, true) {
            @Override
            protected void onSuccess(PayBillOrderInfoEntity result) {
                if (mView != null) {
                    mView.sendNotifySuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.sendNotifyFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void ReceiveSplitBillDetail(String orderNo) {
        AppClient.getInstance().splitBillPayDetail(orderNo, new LifecycleMVPResultCallback<RequestCenterEntity>(mView,true) {
            @Override
            protected void onSuccess(RequestCenterEntity result) {
                if (mView != null) {
                    mView.ReceiveSplitBillDetailSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showError(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getSplitDetail(String originalOrderNo, String orderRelevancy, String splitType) {
        AppClient.getInstance().getSplitDetail(originalOrderNo,orderRelevancy,splitType ,new LifecycleMVPResultCallback<SendReceiveSplitDetailEntity>(mView,true) {
            @Override
            protected void onSuccess(SendReceiveSplitDetailEntity result) {
                if (mView != null) {
                    mView.getSplitDetailSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.showError(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void attachView(SplitBillContract.SplitView splitView) {
        mView = splitView;
    }

    @Override
    public void detachView() {
        mView = null;
    }

}
