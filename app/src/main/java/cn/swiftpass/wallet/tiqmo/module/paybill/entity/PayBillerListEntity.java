package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillerListEntity extends BaseEntity {

    public List<PayBillerEntity> billerList = new ArrayList<>();
}
