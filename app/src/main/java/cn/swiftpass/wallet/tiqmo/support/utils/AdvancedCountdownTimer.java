package cn.swiftpass.wallet.tiqmo.support.utils;

import android.os.CountDownTimer;

public class AdvancedCountdownTimer {

    private static class AdvancedCountdownTimerHolder {
        private static AdvancedCountdownTimer instance = new AdvancedCountdownTimer();
    }

    private CountDownTimer countDownTimer;

    public static AdvancedCountdownTimer getInstance() {
        return AdvancedCountdownTimer.AdvancedCountdownTimerHolder.instance;
    }

    private String lastMobilePhone = "";
    private int delayFinishTime;

    /**
     * 主要解决登录时输入一个手机号 发送验证码 这个时候推出 重新发送这个验证码 次倒计时为全局处理
     *
     * @param onCountDownListener
     * @param phoneNumber
     * @param totlaSecond
     */
    public void startCountDown(final String phoneNumber, int totlaSecond, final OnCountDownListener onCountDownListener) {
        // TODO: 2019/8/15 计时器在所有调用的地方，当页面退出时都没有主动停止
        if (phoneNumber.equals(lastMobilePhone)) {
            //倒计时跟之前登录的倒计时一样，接着上次的时间进行
            countDownEvent(phoneNumber, delayFinishTime, onCountDownListener);
        } else {
            countDownEvent(phoneNumber, totlaSecond, onCountDownListener);
        }
    }

    public void startCountDown(int totlaSecond, final OnCountDownListener onCountDownListener) {

        countDownEvent(totlaSecond, onCountDownListener);
    }

    public void cancel(){
        if(countDownTimer != null){
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    /**
     * 正常倒计时
     *
     * @param totlaSecond
     * @param onCountDownListener
     */
    public void countDownEvent(int totlaSecond, final OnCountDownListener onCountDownListener) {
        if (totlaSecond==0){
            if (onCountDownListener!=null){
                onCountDownListener.onFinish();
            }
        }else {
            countDownTimer = new CountDownTimer(totlaSecond * 1000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    int delaySecond = (int) (millisUntilFinished / 1000);
                    delayFinishTime = delaySecond;
                    if (onCountDownListener!=null){
                        onCountDownListener.onTick(delaySecond);
                    }

                }

                @Override
                public void onFinish() {
                    if (onCountDownListener!=null){
                        onCountDownListener.onFinish();
                    }
                }
            }.start();
        }

    }


    /**
     * 正常倒计时
     *
     * @param totlaSecond
     * @param onCountDownListener
     */
    public void countDownEvent(final String phoneNumber, int totlaSecond, final OnCountDownListener onCountDownListener) {
        this.lastMobilePhone = phoneNumber;
        countDownTimer = new CountDownTimer(totlaSecond * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                int delaySecond = (int) (millisUntilFinished / 1000);
                delayFinishTime = delaySecond;
                onCountDownListener.onTick(delaySecond);
            }

            @Override
            public void onFinish() {
                onCountDownListener.onFinish();
            }
        }.start();
    }


    public interface OnCountDownListener {
        void onTick(int millisUntilFinished);

        void onFinish();
    }


}
