package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class UpdateUserEntity extends BaseEntity {
    public String message;
    public String authentication;
    public String payPasswordSet;
    public String loginPasswordSet;
    public String callingCode;
    public String phone;
    public String userName;
    public String idNumber;
    public String nickName;
    public String avatar;
    public String gender;
    public String email;
    public String scannedCodeDisplay;
    public String userId;
    public String sessionId;
    public String accountName;
    public String ibanNum;
    public String balance;
    public String cities;
    public String Neighborhoods;
    public String poBox;
    public String buildingNo;
    public String street;
    public String sourceOfFundCode;
    public String sourceOfFundDesc;
    public String professionCode;
    public String professionDesc;
    public String businessTypeCode;
    public String businessTypeDesc;
    public String companyName;
    public String salaryRangeCode;
    public String salaryRangeDesc;
    public String imrSetUpFlag;
}
