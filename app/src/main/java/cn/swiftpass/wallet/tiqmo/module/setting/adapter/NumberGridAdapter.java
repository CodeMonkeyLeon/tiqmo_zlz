package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.CommonAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.NumberEntity;

public class NumberGridAdapter extends CommonAdapter {

    private Context context;

    private LayoutInflater inflater;

    private OnItemClick onItemClickListener;

    public void setOnItemClickListener(OnItemClick itemClickListener) {
        this.onItemClickListener = itemClickListener;
    }

    public interface OnItemClick {
        void onItemClick(NumberEntity numberEntity, int position);
    }

    public NumberGridAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_grid_number, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final NumberEntity numberEntity = (NumberEntity) getItem(position);
        if (numberEntity != null) {
            int imgId = numberEntity.imgResId;
            holder.ivNumber.setImageResource(imgId);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(numberEntity, position);
                    }
                }
            });
        }
        return convertView;
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.iv_number)
        ImageView ivNumber;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
