package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MarketDetailsEntity extends BaseEntity {

   public String activityNo;
   public String activityName;
   public String activityDesc;
   public String activityConditions;
   //活动商户logo图片
   public ArrayList<String> activityMerchantLogo = new ArrayList<>();
   public ArrayList<String> vicePagePictures;
   public ArrayList<MerchantInfo> merchantInfoList;

   public class MerchantInfo extends BaseEntity {
      public String merchantHeadPicUrl;
      public String merchantName;
   }
}
