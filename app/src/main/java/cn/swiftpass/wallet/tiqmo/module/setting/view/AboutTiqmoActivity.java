package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;

public class AboutTiqmoActivity extends BaseCompatActivity {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_about_tiqmo)
    ImageView ivAboutTiqmo;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_about_tiqmo;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.sprint11_133);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if(LocaleUtils.isRTL(mContext)){
            ivAboutTiqmo.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_about_img_ar));
        }else{
            ivAboutTiqmo.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_about_img));
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
