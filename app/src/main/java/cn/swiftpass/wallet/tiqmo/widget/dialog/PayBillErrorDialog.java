package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;

public class PayBillErrorDialog extends BottomDialog {

    private Context mContext;

    private int errorType = 1;

    private BottomButtonListener bottomButtonListener;

    private String billName;

    public void setBottomButtonListener(BottomButtonListener bottomButtonListener) {
        this.bottomButtonListener = bottomButtonListener;
    }

    public PayBillErrorDialog(Context context, int errorType, String billName) {
        super(context);
        this.mContext = context;
        this.errorType = errorType;
        this.billName = billName;
        initViews(mContext);
    }


    public interface BottomButtonListener {
        void clickButton();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_paybill_error, null);
        ImageView ivDialogImage = view.findViewById(R.id.iv_dialog_image);
        TextView tvDialogTitle = view.findViewById(R.id.tv_dialog_title);
        TextView tvDialogContent = view.findViewById(R.id.tv_dialog_content);
        LinearLayout llWhatTodo = view.findViewById(R.id.ll_what_todo);
        TextView tvContentThree = view.findViewById(R.id.tv_content_three);
        TextView tvDialogButton = view.findViewById(R.id.tv_dialog_button);
        llWhatTodo.setVisibility(View.GONE);
        tvDialogButton.setText(mContext.getString(R.string.BillPay_58));
        if (errorType == 1) {
            llWhatTodo.setVisibility(View.VISIBLE);
            tvDialogTitle.setText(mContext.getString(R.string.BillPay_48));
            tvDialogContent.setText(mContext.getString(R.string.BillPay_49).replace("XXX", billName));
            ivDialogImage.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_nobill_found));
            tvContentThree.setText(Spans.builder().text(mContext.getString(R.string.BillPay_53))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80a3a3a3_803a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"))
                    .size(14)
                    .text(" " + mContext.getString(R.string.BillPay_54))
                    .color(mContext.getColor(R.color.color_fc4f00))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"))
                    .size(14)
                    .click(tvContentThree, new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View widget) {
                            HashMap<String, Object> hashMap = new HashMap<>();
                            hashMap.put("isHideTitle", true);
                            ActivitySkipUtil.startAnotherActivity((Activity) mContext, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }

                        @Override
                        public void updateDrawState(@NonNull TextPaint ds) {
                            ds.setColor(mContext.getColor(R.color.color_fc4f00));
                            ds.setUnderlineText(false);
                        }
                    })
                    .build());
            tvDialogButton.setText(mContext.getString(R.string.BillPay_55));
        } else if (errorType == 2) {
            tvDialogTitle.setText(mContext.getString(R.string.BillPay_56));
            ivDialogImage.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bill_overdue));
            tvDialogContent.setText(mContext.getString(R.string.BillPay_57).replace("XXX", billName));
        } else if (errorType == 3) {
            ivDialogImage.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_amount_error));
            tvDialogTitle.setText(mContext.getString(R.string.BillPay_59));
            tvDialogContent.setText(mContext.getString(R.string.BillPay_61));
        } else {
            ivDialogImage.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_amount_error));
            tvDialogTitle.setText(mContext.getString(R.string.BillPay_59));
            tvDialogContent.setText(mContext.getString(R.string.BillPay_60));
        }

        tvDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomButtonListener != null) {
                    bottomButtonListener.clickButton();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
