package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeCountryEntity extends BaseEntity {
    public int id;
    //是否默认国家 1是 0否
    public String isdefault;
    //国家代码
    public String countryCode;
    //国家冠码
    public String callingCode;
    //国家名称
    public String countryName;
    //国家logo
    public String countryLogo;

    public String countryType;
}
