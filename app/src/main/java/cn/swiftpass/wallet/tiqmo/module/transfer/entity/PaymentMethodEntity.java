package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PaymentMethodEntity extends BaseEntity {

    public String balance;
    public boolean defaultPaymentMethod;
    public String paymentMethodNo;
    public String paymentType;
    public boolean usable;
    public String paymentMethodDesc;
}
