package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.contract.NotificationContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.DownloadFileUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitPeopleListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherDetailEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class NotificationPresenter implements NotificationContract.Presenter {

    NotificationContract.View view;

    @Override
    public void sendNotify(List<NotifyEntity> receivers) {
        AppClient.getInstance().sendNotify(receivers, new LifecycleMVPResultCallback<PayBillOrderInfoEntity>(view, true) {
            @Override
            protected void onSuccess(PayBillOrderInfoEntity result) {
                view.sendNotifySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                view.sendNotifyFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getHistoryDetail(String orderNo, String orderType,String queryType) {
        AppClient.getInstance().getTransferHistoryDetail(orderNo, orderType,queryType, new LifecycleMVPResultCallback<TransferHistoryDetailEntity>(view, true) {
            @Override
            protected void onSuccess(TransferHistoryDetailEntity result) {
                view.getHistoryDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                view.getHistoryDetailFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getSplitDetailList(String newOrderNo) {
        AppClient.getInstance().getSplitDetailList(newOrderNo, new LifecycleMVPResultCallback<SplitPeopleListEntity>(view, true) {
            @Override
            protected void onSuccess(SplitPeopleListEntity result) {
                view.getSplitDetailListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                view.getSplitDetailListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(view, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                view.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                view.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getVoucherDetail(String orderNo) {
        AppClient.getInstance().getVoucherDatail(orderNo, new LifecycleMVPResultCallback<EVoucherDetailEntity>(view, true) {
            @Override
            protected void onSuccess(EVoucherDetailEntity result) {
                view.getVoucherDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                view.getVoucherDetailFail(errorCode, errorMsg);
            }
        });
    }


    @Override
    public void getDownloadFileUrl(String orderNo) {
        AppClient.getInstance().getDownloadFileUrl(orderNo, new LifecycleMVPResultCallback<DownloadFileUrlEntity>(view, true) {
            @Override
            protected void onSuccess(DownloadFileUrlEntity result) {
                if (view != null) {
                    view.getDownloadFileUrlSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (view != null) {
                    view.getDownloadFileUrlFail(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void attachView(NotificationContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
