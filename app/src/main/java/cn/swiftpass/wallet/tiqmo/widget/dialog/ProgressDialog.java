package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.airbnb.lottie.LottieAnimationView;

import cn.swiftpass.wallet.tiqmo.R;

public class ProgressDialog extends BottomDialog{

    public ProgressDialog(Context context) {
        super(context);
        initViews(context);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_progress_new, null);
        LottieAnimationView animationView = view.findViewById(R.id.loadingAnimView);

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        setCanceledOnTouchOutside(false);
    }
}
