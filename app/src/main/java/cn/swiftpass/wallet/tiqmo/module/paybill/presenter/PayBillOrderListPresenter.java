package cn.swiftpass.wallet.tiqmo.module.paybill.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PayBillOrderListPresenter implements PayBillContract.BillOrderListPresenter {

    PayBillContract.BillOrderListView billOrderListView;

    @Override
    public void getPayBillOrderList(List<String> countryCodeList, String domesticFlag) {
        AppClient.getInstance().getTransferManager().getBillOrderHistory(countryCodeList, domesticFlag, new LifecycleMVPResultCallback<PayBillOrderListEntity>(billOrderListView, true) {
            @Override
            protected void onSuccess(PayBillOrderListEntity result) {
                billOrderListView.getPayBillOrderListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billOrderListView.getPayBillOrderListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getPayBillOrderIOList(String billerId, String sku, String aliasName, String channelCode) {
        AppClient.getInstance().getTransferManager().getPayBillOrderIOList(billerId, sku, aliasName, channelCode, new LifecycleMVPResultCallback<PayBillIOListEntity>(billOrderListView, true) {
            @Override
            protected void onSuccess(PayBillIOListEntity result) {
                billOrderListView.getPayBillOrderIOListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billOrderListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getPayBillCountry() {
        AppClient.getInstance().getTransferManager().getPayBillCountry(new LifecycleMVPResultCallback<PayBillCountryListEntity>(billOrderListView, true) {
            @Override
            protected void onSuccess(PayBillCountryListEntity result) {
                billOrderListView.getPayBillCountrySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billOrderListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void deletePayBillOrder(String billerId, String aliasName) {
        AppClient.getInstance().getTransferManager().deletePayBillOrder(billerId, aliasName, new LifecycleMVPResultCallback<Void>(billOrderListView, true) {
            @Override
            protected void onSuccess(Void result) {
                billOrderListView.deletePayBillOrderSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                billOrderListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PayBillContract.BillOrderListView billOrderListView) {
        this.billOrderListView = billOrderListView;
    }

    @Override
    public void detachView() {
        this.billOrderListView = null;
    }
}
