package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class TransferFeePresenter implements TransferContract.TransferFeePresenter {

    private TransferContract.TransferFeeView transferFeeView;

    @Override
    public void getTransferFee(String type, String orderAmount, String orderCurrencyCode) {
        AppClient.getInstance().getTransferFee(type, orderAmount, orderCurrencyCode, "", new LifecycleMVPResultCallback<TransFeeEntity>(transferFeeView, true) {
            @Override
            protected void onSuccess(TransFeeEntity result) {
                transferFeeView.getTransferFeeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferFeeView.getTransferFeeFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void transferPay(String type, String orderAmount, String orderCurrencyCode,
                            String cvv, String protocolNo, String transFees, String vat,String shopperResultUrl) {
        AppClient.getInstance().transferPay(type, orderAmount, orderCurrencyCode, cvv, protocolNo, transFees, vat,shopperResultUrl, new LifecycleMVPResultCallback<TransferPayEntity>(transferFeeView, true) {
            @Override
            protected void onSuccess(TransferPayEntity result) {
                transferFeeView.transferPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                transferFeeView.transferPayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void addMoneyBindCard(String custName, String cardNo, String expire, String cvv, String type, String orderAmount, String orderCurrencyCode, String transFees, String vat,String shopperResultUrl,
            String creditType) {
        AppClient.getInstance().addMoneyCard(custName, cardNo, expire, cvv, type, orderAmount,
                orderCurrencyCode, transFees, vat,shopperResultUrl,creditType, new LifecycleMVPResultCallback<CardBind3DSEntity>(transferFeeView, true) {
                    @Override
                    protected void onSuccess(CardBind3DSEntity result) {
                        transferFeeView.addMoneyBindCardSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        transferFeeView.addMoneyBindCardFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void attachView(TransferContract.TransferFeeView transferFeeView) {
        this.transferFeeView = transferFeeView;
    }

    @Override
    public void detachView() {
        this.transferFeeView = null;
    }
}
