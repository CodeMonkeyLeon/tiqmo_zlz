package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.VerifyPwdCodeView;

public class GetCardPwdOneActivity extends BaseCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_second_title)
    TextView tvSecondTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_face)
    ImageView ivFace;
    @BindView(R.id.tv_one)
    TextView tvOne;
    @BindView(R.id.tv_two)
    TextView tvTwo;
    @BindView(R.id.verift_pwd)
    VerifyPwdCodeView veriftPwd;

    private OpenCardReqEntity openCardReqEntity;

    private String proxyCardNo;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_get_card_pwd_one;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if(getIntent() != null && getIntent().getExtras() != null) {
            proxyCardNo = getIntent().getExtras().getString(Constants.proxyCardNo);
        }
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        //默认输入框为空
        veriftPwd.setEmpty();
        veriftPwd.setOnCodeFinishListener(new VerifyPwdCodeView.OnCodeFinishListener() {
            @Override
            public void onTextChange(View view, String content) {

            }

            @Override
            public void onComplete(View view, String content) {
                openCardReqEntity.passwordOne = content;
                UserInfoManager.getInstance().setOpenCardReqEntity(openCardReqEntity);
                HashMap<String,Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.proxyCardNo,proxyCardNo);
                ActivitySkipUtil.startAnotherActivity(GetCardPwdOneActivity.this, GetCardPwdTwoActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }
}
