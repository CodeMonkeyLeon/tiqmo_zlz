package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.Manifest;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.adapter.RechargeContactAdapter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeContract;
import cn.swiftpass.wallet.tiqmo.module.topup.presenter.RechargeCountryPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestContactEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ContactUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class RechargeContactActivity extends BaseCompatActivity<RechargeContract.RechargeCountryPresenter> implements RechargeContract.RechargeCountryView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_calling_code)
    ImageView ivCallingCode;
    @BindView(R.id.tv_calling_code)
    TextView tvCallingCode;
    @BindView(R.id.ll_line)
    View llLine;
    @BindView(R.id.con_area)
    RelativeLayout conArea;
    @BindView(R.id.et_phone)
    EditTextWithDel etPhone;
    @BindView(R.id.ll_area_number)
    ConstraintLayout llAreaNumber;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_your)
    TextView tvYour;
    @BindView(R.id.iv_no_permission)
    ImageView ivNoPermission;
    @BindView(R.id.tv_no_permission)
    TextView tvNoPermission;
    @BindView(R.id.tv_allow)
    TextView tvAllow;
    @BindView(R.id.ll_permission)
    LinearLayout llPermission;
    @BindView(R.id.rl_contacts)
    RecyclerView rlContacts;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar idSideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.con_bottom)
    ConstraintLayout conBottom;

    boolean isSearchEmpty;

    private RechargeContactAdapter kycContactAdapter;
    private LinearLayoutManager mLayoutManager;

    private String phone,requestPhoneNumber;
    private KycContactEntity currentKycContact;

    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;

    private List<KycContactEntity> filterContactList = new ArrayList<>();

    public List<RequestContactEntity> requestContactList = new ArrayList<>();
    public List<KycContactEntity> phoneList = new ArrayList<>();

    private RechargeCountryEntity rechargeCountryEntity;

    private String callingCode;

    private boolean isContinue;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_enter_phone;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        tvTitle.setText(R.string.Topup_new_2);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle,llAreaNumber,etPhone,tvCallingCode);
//        etPhone.setLineVisible(false);
        etPhone.getTlEdit().setHint(getString(R.string.sprint11_106));
        currentKycContact = new KycContactEntity();
        if (getIntent() != null && getIntent().getExtras() != null) {
            rechargeCountryEntity = (RechargeCountryEntity) getIntent().getExtras().getSerializable(Constants.rechargeCountryEntity);
            if(rechargeCountryEntity != null){
                callingCode = rechargeCountryEntity.callingCode;
                String countryLogo = rechargeCountryEntity.countryLogo;
                tvCallingCode.setText("+" + callingCode);

                Glide.with(mContext)
                        .load(countryLogo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivCallingCode);
            }
        }

        idSideBar.setTextView(idDialog);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 0.5f);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rlContacts.setLayoutManager(mLayoutManager);
        kycContactAdapter = new RechargeContactAdapter(phoneList);
        kycContactAdapter.bindToRecyclerView(rlContacts);

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = kycContactAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (mLayoutManager != null) {
                        mLayoutManager.scrollToPositionWithOffset(position, 0);
                    }
                }
            }
        });

        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                phone = etPhone.getText().toString().trim();
                if (!hasFocus) {
//                    if (TextUtils.isEmpty(phone)) {
////                        etPhone.showErrorViewWithMsg(getString(R.string.MobileValidation_SU_0002_2_D_2));
//                    } else if (phone.equals(userInfoEntity.getPhone())) {
//                        etPhone.showErrorViewWithMsg(getString(R.string.stt_4_1));
//                    } else if (!RegexUtil.isPhone(phone)) {
//                        etPhone.showErrorViewWithMsg(getString(R.string.wtw_17));
//                    }
                }
            }
        });

        etPhone.addTextChangedListener(new SimpleTextWatcher(){
            @Override
            public void afterTextChanged(Editable s) {
                phone = s.toString();
                etPhone.showErrorViewWithMsg("");
                if (TextUtils.isEmpty(phone)) {
                    tvContinue.setEnabled(false);
                } else {
                    if (phone.equals(userInfoEntity.getPhone())) {
                        tvContinue.setEnabled(false);
                    } else if (RegexUtil.isPhone(phone)) {
                        tvContinue.setEnabled(true);
                    } else {
                        tvContinue.setEnabled(true);
                    }
                }
                if (phoneList != null && phoneList.size() > 0) {
                    searchFilterWithAllList(phone);
                } else {
                    isSearchEmpty = true;
                }
            }
        });

        kycContactAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                KycContactEntity kycContactEntity = kycContactAdapter.getDataList().get(position);
                requestPhoneNumber = kycContactEntity.getPhoneNumber().replace(" ","");
                currentKycContact = kycContactEntity;
                mPresenter.getRechargeOperator(requestPhoneNumber);
            }
        });
    }

    private void searchFilterWithAllList(String filterStr) {
        llPermission.setVisibility(View.GONE);
        tvYour.setBackgroundResource(R.drawable.shape_top);
        conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
        try {
            filterContactList.clear();
            //去除空格的匹配规则
            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            if (TextUtils.isEmpty(filterStr)) {
                kycContactAdapter.setIsFilter(false);
//            llEmpty.setVisibility(View.GONE);
                idSideBar.setVisibility(View.VISIBLE);
                filterContactList.clear();
                kycContactAdapter.setDataList(phoneList);
                return;
            } else {
                idSideBar.setVisibility(View.GONE);
                kycContactAdapter.setIsFilter(true);
            }
            filterContactList.clear();
            if (phoneList != null && phoneList.size() > 0) {
                int size = phoneList.size();
                for (int i = 0; i < size; i++) {
                    KycContactEntity kycContactEntity = phoneList.get(i);
                    String name = kycContactEntity.getNameNoMapping() + kycContactEntity.getPhoneNoMapping();
                    String pinyinName = Pinyin.toPinyin(name, "");
                    String phoneNumber = kycContactEntity.getPhoneNumber();
                    if(pinyinName.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterContactList.add(kycContactEntity);
                    }else if (phoneNumber.startsWith("+"+callingCode+" " + filterStr)) {
                        filterContactList.add(kycContactEntity);
                    }
                }

                if (filterContactList.size() == 0) {
                    isSearchEmpty = true;
                    tvNoPermission.setText(R.string.wtw_18_1);
                    ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                    llPermission.setVisibility(View.VISIBLE);
                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                    tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                    tvAllow.setVisibility(View.GONE);
                    kycContactAdapter.setDataList(filterContactList);
                } else {
                    isSearchEmpty = false;
                    llPermission.setVisibility(View.GONE);
                    tvYour.setBackgroundResource(R.drawable.shape_top);
                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                    kycContactAdapter.setDataList(filterContactList);
                }
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkPermission();

        if (etPhone != null) {
            String phone = etPhone.getText().toString();
            if (!TextUtils.isEmpty(phone)) {
                searchFilterWithAllList(phone);
            }
        }
    }

    private void checkPermission() {
        phoneList.clear();
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
            llPermission.setVisibility(View.VISIBLE);
            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
            tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
            tvAllow.setVisibility(View.GONE);
            conRecycler.setVisibility(View.GONE);
            if (!ActivityCompat.shouldShowRequestPermissionRationale(RechargeContactActivity.this, Manifest.permission.READ_CONTACTS)) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
            PermissionInstance.getInstance().getPermission(RechargeContactActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    llPermission.setVisibility(View.GONE);
                    tvYour.setBackgroundResource(R.drawable.shape_top);
                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                    if (phoneList.size() == 0) {
                        ContactUtils.getInstance().getAllContactsAsyn(mContext, true,callingCode, new ContactUtils.OnReadContactsSuccessCallBack() {
                            @Override
                            public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                                phoneList.addAll(contactEntities);
                                int size = phoneList.size();
                                if (size == 0) {
                                    llPermission.setVisibility(View.VISIBLE);
                                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                                    tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                                    tvNoPermission.setText(R.string.wtw_18_1);
                                    ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                                    idSideBar.setVisibility(View.GONE);
                                    tvAllow.setVisibility(View.GONE);
                                } else {
                                    llPermission.setVisibility(View.GONE);
                                    tvYour.setBackgroundResource(R.drawable.shape_top);
                                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                                    idSideBar.setVisibility(View.VISIBLE);
                                    conRecycler.setVisibility(View.VISIBLE);
                                    kycContactAdapter.setDataList(phoneList);
                                }
                            }
                        });
                    }
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(RechargeContactActivity.this, Manifest.permission.READ_CONTACTS)) {
                        if (!firstRejectContacts) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_30));
                        } else {
                            firstRejectContacts = false;
                        }
                    }
                }
            }, Manifest.permission.READ_CONTACTS);
        } else {
            llPermission.setVisibility(View.GONE);
            tvYour.setBackgroundResource(R.drawable.shape_top);
            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
            if (phoneList.size() == 0) {
                ContactUtils.getInstance().getAllContactsAsyn(mContext, true,callingCode, new ContactUtils.OnReadContactsSuccessCallBack() {
                    @Override
                    public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                        phoneList.addAll(contactEntities);
                        int size = phoneList.size();
                        if (size == 0) {
                            llPermission.setVisibility(View.VISIBLE);
                            conRecycler.setVisibility(View.GONE);
                            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                            tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                            tvNoPermission.setText(R.string.wtw_18_1);
                            ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                            idSideBar.setVisibility(View.GONE);
                            tvAllow.setVisibility(View.GONE);
                        } else {
                            llPermission.setVisibility(View.GONE);
                            tvYour.setBackgroundResource(R.drawable.shape_top);
                            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                            idSideBar.setVisibility(View.VISIBLE);
                            conRecycler.setVisibility(View.VISIBLE);
                            kycContactAdapter.setDataList(phoneList);
                        }
                    }
                });
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_continue, R.id.tv_allow,R.id.tv_get_help,R.id.con_area})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.con_area:
                mPresenter.getRechargeCountry();
                break;
            case R.id.tv_get_help:
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_allow:
                AndroidUtils.startAppSetting(mContext);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_continue:
                isContinue = true;
                phone = etPhone.getText().toString().trim();
                requestPhoneNumber = "+"+callingCode+phone;
                mPresenter.getRechargeOperator(requestPhoneNumber.replace(" ",""));
                break;
            default:
                break;
        }
    }

    @Override
    public void getRechargeOperatorSuccess(RechargeOperatorListEntity rechargeOperatorListEntity) {
        if(rechargeOperatorListEntity != null) {
            HashMap<String, Object> hashMap = new HashMap<>();
            currentKycContact.setPhoneNumber(requestPhoneNumber);
            hashMap.put(Constants.rechargeContactEntity, currentKycContact);
            hashMap.put(Constants.rechargeOperatorListEntity, rechargeOperatorListEntity);
            ActivitySkipUtil.startAnotherActivity(this, RechargeConfirmActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getRechargeCountrySuccess(RechargeCountryListEntity rechargeCountryListEntity) {
        if (rechargeCountryListEntity != null) {
            List<RechargeCountryEntity> countryList = rechargeCountryListEntity.countryList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            for (int i = 0; i < countryList.size(); i++) {
                RechargeCountryEntity imrCountryEntity = countryList.get(i);
                if (imrCountryEntity != null) {
                    String isDefault = imrCountryEntity.isdefault;
                    if("1".equals(isDefault)) {
                        countryList.remove(imrCountryEntity);
                        i--;
                    }else{
                        SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                        selectInfoEntity.imgUrl = imrCountryEntity.countryLogo;
                        selectInfoEntity.businessParamValue = imrCountryEntity.countryName + "  " + "+" + imrCountryEntity.callingCode;
                        selectInfoEntityList.add(selectInfoEntity);
                    }
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.BillPay_2), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    RechargeCountryEntity countryEntity = countryList.get(position);
                    if(countryEntity != null){
                        callingCode = countryEntity.callingCode;
                        String countryLogo = countryEntity.countryLogo;
                        tvCallingCode.setText("+" + callingCode);
                        etPhone.setContentText("");

                        Glide.with(mContext)
                                .load(countryLogo)
                                .dontAnimate()
                                .format(DecodeFormat.PREFER_RGB_565)
                                .into(ivCallingCode);

                        checkPermission();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getRechargeCountrySuccess");
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if(isContinue){
            isContinue = false;
            etPhone.showErrorViewWithMsg(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public RechargeContract.RechargeCountryPresenter getPresenter() {
        return new RechargeCountryPresenter();
    }
}
