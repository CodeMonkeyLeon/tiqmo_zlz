package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class ErrorBottomDialog extends BottomDialog{

    private Context mContext;

    private onConfirmListener closeListener;

    private TextView tvErrorTitle,tvErrorContent;
    private ImageView ivError;
    private TextView tvClose;

    public void setCloseListener(String confirmMsg,onConfirmListener closeListener) {
        this.closeListener = closeListener;
        if(tvClose != null) {
            tvClose.setText(confirmMsg);
            tvClose.setVisibility(View.VISIBLE);
        }
    }

    public ErrorBottomDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public void setErrorMsg(String errorTitle,String errorContent,int errorMsgId){
        if(tvErrorTitle != null && !TextUtils.isEmpty(errorTitle)){
            tvErrorTitle.setText(errorTitle);
        }

        if(tvErrorContent != null && !TextUtils.isEmpty(errorContent)){
            tvErrorContent.setText(errorContent);
        }

        if(ivError != null && errorMsgId != -1){
            ivError.setImageResource(errorMsgId);
        }
    }

    public interface onConfirmListener {
        void clickConfirm();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_bottom_error, null);
        tvClose = view.findViewById(R.id.tv_renew_id);
        tvErrorTitle = view.findViewById(R.id.tv_error_title);
        tvErrorContent = view.findViewById(R.id.tv_error_content);
        ivError = view.findViewById(R.id.iv_error);

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (closeListener != null) {
                    closeListener.clickConfirm();
                }
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
