package cn.swiftpass.wallet.tiqmo.support.utils.encry;

import android.util.Log;

import java.security.MessageDigest;
import java.security.Signature;

import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.Base64;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class EncryptUtils {

    public static final String TAG = EncryptUtils.class.getSimpleName();

    /**
     * 入参 加签
     *
     * @param data
     * @return
     */
    public static String getDigestSign(String data) {
        try {
            MessageDigest sha256Digest = DigestUtils.getSha256Digest();
            sha256Digest.update(data.getBytes());
            // 完成哈希计算，得到摘要
            byte[] shaEncoded = sha256Digest.digest();
            //使用摘要，使用SHA256withRSA加签 完成签名，返回byte数组使用base64编码
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(RSAHelper.getPrivateKey(UserInfoManager.getInstance().getAppPriKey()));
            signature.update(shaEncoded);
            String sign = Base64.encodeBytes(signature.sign());
            return sign;
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * 获得加密 加密处理
     *
     * @return
     */
    public static String getEncryptionToken(String token, String pubKey) {
        try {
            return RSAHelper.encrypt(token, pubKey);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }

    public static String getAESKey() {
        return RandomUtils.getNumSmallLetter(16);
    }

}
