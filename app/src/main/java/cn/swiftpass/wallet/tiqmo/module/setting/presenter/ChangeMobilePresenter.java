package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.PersonInfoContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ChangeMobilePresenter implements PersonInfoContract.ChangePhonePresenter {

    private PersonInfoContract.ChangePhoneView changePhoneView;

    @Override
    public void checkPhone(String callingCode, String phone, String checkIdNumber) {
        AppClient.getInstance().checkPhoneUpdate(callingCode, phone, checkIdNumber, new LifecycleMVPResultCallback<Void>(changePhoneView, true) {
            @Override
            protected void onSuccess(Void result) {
                changePhoneView.checkPhoneSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                changePhoneView.checkPhoneFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(PersonInfoContract.ChangePhoneView changePhoneView) {
        this.changePhoneView = changePhoneView;
    }

    @Override
    public void detachView() {
        this.changePhoneView = null;
    }
}
