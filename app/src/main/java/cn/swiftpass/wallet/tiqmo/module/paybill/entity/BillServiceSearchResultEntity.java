package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * @author lizheng.zhao
 * @date 2022/07/14
 */
public class BillServiceSearchResultEntity extends BaseEntity {

    private List<PayBillerEntity> billerList;

    public void setBillerList(List<PayBillerEntity> billerList) {
        this.billerList = billerList;
    }

    public List<PayBillerEntity> getBillerList() {
        return billerList;
    }


}
