package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.AddMoneyFragment;

public class AddMoneyActivity extends BaseCompatActivity {

    @BindView(R.id.fl_add_money)
    FrameLayout flAddMoney;

    private AddMoneyFragment addMoneyFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_money;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null) {
            addMoneyFragment = AddMoneyFragment.getInstance(true);

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_add_money, addMoneyFragment);
            ft.commit();
        } else {
            addMoneyFragment = AddMoneyFragment.getInstance(true);

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_add_money, addMoneyFragment);
            ft.commit();
        }
    }
}
