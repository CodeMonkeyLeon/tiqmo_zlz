package cn.swiftpass.wallet.tiqmo.support;

import android.text.TextUtils;

import org.json.JSONObject;

import java.security.cert.Certificate;
import java.util.Map;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AreaEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.AutoLoginSucEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.SPFuncUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.utils.encry.RSAHelper;

public class UserInfoManager {

    private static UserInfoManager sInstance = new UserInfoManager();

    public AutoLoginSucEntity getUserInfo() {
        return userInfo;
    }

    private String currencyCode;

    private boolean isOpenFace;

    private AreaEntity areaEntity;

    private boolean isSetBudget;

    private AppsFlyerEntity appsFlyerEntity;

    private OpenCardReqEntity openCardReqEntity;

    private CheckPhoneEntity checkPhoneEntity;

    public CheckPhoneEntity getCheckPhoneEntity() {
        return this.checkPhoneEntity;
    }

    public void setCheckPhoneEntity(final CheckPhoneEntity checkPhoneEntity) {
        this.checkPhoneEntity = checkPhoneEntity;
    }

    public boolean isSetBudget() {
        return this.isSetBudget;
    }

    public void setSetBudget(final boolean setBudget) {
        this.isSetBudget = setBudget;
    }

    public AppsFlyerEntity getAppsFlyerEntity() {
        if (appsFlyerEntity == null) {
            appsFlyerEntity = new AppsFlyerEntity();
        }
        return this.appsFlyerEntity;
    }

    public void setAppsFlyerEntity(final AppsFlyerEntity appsFlyerEntity) {
        this.appsFlyerEntity = appsFlyerEntity;
    }

    public OpenCardReqEntity getOpenCardReqEntity() {
        if (openCardReqEntity == null) {
            return new OpenCardReqEntity();
        }
        return this.openCardReqEntity;
    }

    public void setOpenCardReqEntity(final OpenCardReqEntity openCardReqEntity) {
        this.openCardReqEntity = openCardReqEntity;
    }

    public AreaEntity getAreaEntity() {
        return this.areaEntity;
    }

    public String getCallingCode() {
        if (getAreaEntity() != null) {
            return getAreaEntity().callingCode;
        }
        return Constants.CALLINGCODE_KSA;
    }

    public void setAreaEntity(final AreaEntity areaEntity) {
        this.areaEntity = areaEntity;
    }

    public void setUserInfo(AutoLoginSucEntity userInfo) {
        this.userInfo = userInfo;
    }


    public String getConvertUnit() {
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            if (configEntity.currencyCds != null && configEntity.currencyCds.size() > 0) {
                String convert = configEntity.currencyCds.get(0).getConvertUnit();
                if (!TextUtils.isEmpty(convert)) {
                    return convert;
                }
            }
        }
        return "100";
    }


    public String getCurrencyCode() {
//        if (!TextUtils.isEmpty(currencyCode)) {
//            return currencyCode;
//        }

        currencyCode = "SAR";
        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if (configEntity != null) {
            if (configEntity.currencyCds != null && configEntity.currencyCds.size() > 0) {
                currencyCode = configEntity.currencyCds.get(0).getCurrencyCode();
            }
        }
        return currencyCode;
    }

    public boolean isOpenFace() {
        return this.isOpenFace;
    }

    public void setOpenFace(final boolean openFace) {
        this.isOpenFace = openFace;
    }

    /**
     * 用戶登录 设备登录 信息
     */
    private AutoLoginSucEntity userInfo;

    private UserInfoManager() {

    }

    public static UserInfoManager getInstance() {
        return sInstance;
    }


    public final String LOGIN_OTHER_DEVICE_YES = "Y";

    /**
     * 初始化客户端公私钥
     */
    public void initAppKeypair() {
        Map<String, String> map = RSAHelper.generateKeyPair();
        if (null != map) {
            setAppPriKey(map.get(Constants.APP_PRIVATEKEY));
            setAppPubKey(map.get(Constants.APP_PUBLICKEY));
        }
    }

    private int kycType;

    public int getKycType() {
        return kycType;
    }

    public void setKycType(int kycType) {
        this.kycType = kycType;
    }

    /**
     * 客户端私钥
     */
    private String appPriKey = "";

    public String getAppPriKey() {
        return appPriKey;
    }

    public void setAppPriKey(String aPriKey) {
        this.appPriKey = aPriKey;
    }

    /**
     * 客户端公钥
     */
    private String appPubKey = "";

    public void setAppPubKey(String aPubKey) {
        this.appPubKey = aPubKey;
    }

    public String getAppPubKey() {
        return appPubKey;
    }

    /**
     * 服务端公钥
     */
    private static String serverPubKey = "";

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String sPubKey) {
        serverPubKey = sPubKey;
    }

    /**
     * 客户端sessionid
     */
    private String sessionId = "";

    public void saveSessionId(String sessionId) {
        this.sessionId = sessionId;
        if (userInfo != null) {
            userInfo.setsId(sessionId);
        }
    }

    public String getSessionId() {
        if (userInfo != null) {
            return userInfo.getsId();
        }
        return sessionId;
    }

    public boolean isLogin() {
        return (boolean) SPFuncUtils.get(Constants.CACHE_IS_LOGIN, false);
    }

    /**
     * 用户是否登录
     *
     * @return
     */
    public boolean isHasSessionId() {
        return !TextUtils.isEmpty(getSessionId());
    }


    /**
     * 保存用户登录状态
     */
    public void saveLoginStatus() {
        SPFuncUtils.put(Constants.CACHE_IS_LOGIN, true);
    }

    /**
     * 保存用户登出状态
     */
    public void saveLogoutStatus() {
//        saveSessionId("");
        SPFuncUtils.put(Constants.CACHE_IS_LOGIN, false);
    }

    public boolean checkLoginStatus() {
        boolean isLogin = isLogin() && isHasSessionId();
        return isLogin;
    }

    /**
     * 客户端UserID
     */
    private String mUserId = "";


    public String getUserId() {
        if (userInfo != null) {
            return userInfo.getWalletId();
        }
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmGcmDeviceToken() {
        return mGcmDeviceToken;
    }

    public void setmGcmDeviceToken(String mGcmDeviceToken) {
        this.mGcmDeviceToken = mGcmDeviceToken;
    }

    //gcm推送 客户端需要的token
    private String mGcmDeviceToken;
    private String balance;

    public String getBalance() {
        return balance;
    }

//    public void setBalance(String balance) {
//        this.balance = balance;
//    }

    //app错误码
    private JSONObject errorCodeList;

    public JSONObject getErrorCodeList() {
        return errorCodeList;
    }

    public void setErrorCodeObject(JSONObject jsonObject) {
        this.errorCodeList = jsonObject;
    }


    public Certificate[] getCertificates() {
        return certificates;
    }

    private Certificate[] certificates;

    public void setSdkCertifacates(Certificate[] certificatesIn) {
        certificates = certificatesIn;
    }

}
