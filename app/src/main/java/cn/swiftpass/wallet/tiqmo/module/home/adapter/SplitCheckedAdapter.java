package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SplitCheckedAdapter extends BaseRecyclerAdapter<KycContactEntity> {

    public SplitCheckedAdapter(@Nullable List<KycContactEntity> data) {
        super(R.layout.item_split_contact_header, data);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null) {
//            return Math.min(5, mDataList.size());
            return mDataList.size();
        }
        return 0;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, KycContactEntity kycContactEntity, int position) {

        String name = kycContactEntity.getContactsName();
        baseViewHolder.setText(R.id.tv_user_name, name);
//        baseViewHolder.setText(R.id.tv_user_name, AndroidUtils.getContactName(name));
        baseViewHolder.setTextColor(R.id.tv_user_name, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_6b6c73)));
        ImageView ivAvatar = baseViewHolder.getView(R.id.iv_avatar);
        String avatarUrl = kycContactEntity.getHeadIcon();
        String gender = kycContactEntity.getSex();
        if (!TextUtils.isEmpty(avatarUrl)) {
            Glide.with(mContext).clear(ivAvatar);
            Glide.with(mContext)
                    .asBitmap()
                    .load(avatarUrl)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .dontAnimate()
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(new BitmapImageViewTarget(ivAvatar) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                            ivAvatar.setImageBitmap(bitmap);
                        }
                    });
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivAvatar);
        }

        baseViewHolder.addOnClickListener(R.id.iv_delete);
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }
}
