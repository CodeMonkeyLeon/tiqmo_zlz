package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

//Otp验证风控信息
public class RiskControlEntity extends BaseEntity {

    //风控短信类型  只要不为空  就走OTP
    public String type;
    //风控短信格式
    public int codeType;
    //风控短信长度
    public String codeLength;
    //风控操作类型
    public String riskActionType;
    //下一步要求OTP
    public String riskCode;
    public String riskMessage;

    public int expiredTime;
    //是否走支付密码  Y是 N否
    public String paySecretRequired;

    //(1:需要，0:不需要) 等于1，则需要IVR
    public String isNeedIvr;

    public String outActionId;

    //IVR重试时间
    public int ivrRetryTime;

    public String categoryId;

    /**
     * 触发原因 DEVICE_ID_CHANGE 设备Id变更 DEVICE_TOKEN_CHANGE 设备Token变更 USER_STATUS_S_LOGIN 暂停用户状态登录
     */
    public String triggerRiskReason;

    public boolean isOtp() {
        return !TextUtils.isEmpty(type);
    }

    //判断是否走支付密码
    public boolean isPayPwd(String paySecretRequired) {
        return "Y".equals(paySecretRequired);
    }

    public boolean isNeedIvr() {
        return "1".equals(isNeedIvr);
    }
}
