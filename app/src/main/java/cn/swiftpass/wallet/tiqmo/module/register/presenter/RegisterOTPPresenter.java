package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterOTPContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;


public class RegisterOTPPresenter implements RegisterOTPContract.Presenter {

    private RegisterOTPContract.View mRegisterOTPView;

//    @Override
//    public void setKsaCardChannel(String txnType, String currencyCode, String txnTypeEnabled, String proxyCardNo) {
//        AppClient.getInstance().getCardManager().setKsaCardChannel(txnType, currencyCode, txnTypeEnabled, proxyCardNo, new LifecycleMVPResultCallback<Void>(mRegisterOTPView,true) {
//            @Override
//            protected void onSuccess(Void result) {
//                mRegisterOTPView.setKsaCardChannelSuccess(result);
//            }
//
//            @Override
//            protected void onFail(String errorCode, String errorMsg) {
//                mRegisterOTPView.showErrorMsg(errorCode,errorMsg);
//            }
//        });
//    }

    @Override
    public void getKsaCardDetails(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaCardDetails(proxyCardNo, new LifecycleMVPResultCallback<CardDetailEntity>(mRegisterOTPView,true) {
            @Override
            protected void onSuccess(CardDetailEntity result) {
                mRegisterOTPView.getKsaCardDetailsSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire) {
        AppClient.getInstance().getCardManager().activateCard(proxyCardNo, cardNo, cardExpire, new LifecycleMVPResultCallback<Void>(mRegisterOTPView,true) {
            @Override
            protected void onSuccess(Void result) {
                mRegisterOTPView.activateCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardLimit(String txnType,  String dailyLimitValue, String monthlyLimitValue, String proxyCardNo,String up) {
        AppClient.getInstance().getCardManager().setKsaCardLimit(txnType,dailyLimitValue, monthlyLimitValue, proxyCardNo,up, new LifecycleMVPResultCallback<Void>(mRegisterOTPView,true) {
            @Override
            protected void onSuccess(Void result) {
                mRegisterOTPView.setKsaCardLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardStatus(String status, String proxyCardNo, String reason,String reasonStatus) {
        AppClient.getInstance().getCardManager().setKsaCardStatus(status, proxyCardNo, reason,reasonStatus, new LifecycleMVPResultCallback<Void>(mRegisterOTPView,true) {
            @Override
            protected void onSuccess(Void result) {
                mRegisterOTPView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

//    @Override
//    public void openKsaCardNoFee(OpenCardReqEntity openCardReqEntity) {
//        AppClient.getInstance().getCardManager().openKsaCardNoFee(openCardReqEntity.cardProductType, openCardReqEntity.cardType, openCardReqEntity.cardFaceId,
//                openCardReqEntity.province, openCardReqEntity.city, openCardReqEntity.address, openCardReqEntity.pinBean,openCardReqEntity.cardOfName, new LifecycleMVPResultCallback<KsaPayResultEntity>(mRegisterOTPView,true) {
//                    @Override
//                    protected void onSuccess(KsaPayResultEntity result) {
//                        mRegisterOTPView.openKsaCardNoFeeSuccess(result);
//                    }
//
//                    @Override
//                    protected void onFail(String errorCode, String errorMsg) {
//                        mRegisterOTPView.showErrorMsg(errorCode,errorMsg);
//                    }
//                });
//    }

    @Override
    public void getKsaCardPayResult(String vat, String totalAmount, OpenCardReqEntity appNiOpenCardReq) {
        AppClient.getInstance().getCardManager().getKsaCardPayResult(vat, totalAmount, appNiOpenCardReq, new LifecycleMVPResultCallback<KsaPayResultEntity>(mRegisterOTPView,true) {
            @Override
            protected void onSuccess(KsaPayResultEntity result) {
                mRegisterOTPView.getKsaCardPayResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.getKsaCardPayResultFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void sendOTP(String callingCode, String phone,String type,List<String> additionalData) {

        AppClient.getInstance().obtainVerifyCodeOfType(callingCode, phone, type,additionalData, new LifecycleMVPResultCallback<RegisterOtpEntity>(mRegisterOTPView, true) {
            @Override
            protected void onSuccess(RegisterOtpEntity result) {
                mRegisterOTPView.sendOTPSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.sendOTPFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void tryOTP(String callingCode, String phone,String type,List<String> additionalData) {
        if (mRegisterOTPView != null) {
            mRegisterOTPView.showProgress(true);
        }
        AppClient.getInstance().obtainVerifyCodeOfType(callingCode, phone, type, additionalData, new LifecycleMVPResultCallback<RegisterOtpEntity>(mRegisterOTPView, true) {
            @Override
            protected void onSuccess(RegisterOtpEntity result) {
                if (mRegisterOTPView != null) {
                    mRegisterOTPView.showProgress(false);
                    mRegisterOTPView.tryOTPSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mRegisterOTPView != null) {
                    mRegisterOTPView.showProgress(false);
                    mRegisterOTPView.tryOTPFailed(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void verifyOTP(String callingCode, String phone, String verifyCode,String type,String orderNo) {
        if (mRegisterOTPView != null) {
            mRegisterOTPView.showProgress(true);
        }

        AppClient.getInstance().checkVerifyCodeOfType(callingCode, phone, verifyCode,type,orderNo,new LifecycleMVPResultCallback<Void>(mRegisterOTPView, true) {
            @Override
            protected void onSuccess(Void result) {
                if (mRegisterOTPView != null) {
                    mRegisterOTPView.verifyOTPSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mRegisterOTPView != null) {
                    mRegisterOTPView.showProgress(false);

                    mRegisterOTPView.verifyOTPFailed(errorCode, errorMsg);
                }
            }
        });
    }


    @Override
    public void attachView(RegisterOTPContract.View view) {
        mRegisterOTPView = view;
    }

    @Override
    public void detachView() {
        mRegisterOTPView = null;
    }

    @Override
    public void loginAccount(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude) {
        if (mRegisterOTPView == null) {
            return;
        }
        AppClient.getInstance().loginByType(callingCode, phone, password, verifyCode, type, longitude, latitude, new LifecycleMVPResultCallback<UserInfoEntity>(mRegisterOTPView, false) {
            @Override
            protected void onSuccess(UserInfoEntity response) {
                if (mRegisterOTPView != null) {
                    mRegisterOTPView.loginAccountSuccess(response);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.loginAccountError(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void stopAccount() {
        AppClient.getInstance().getUserManager().stopAccount(new LifecycleMVPResultCallback<Void>(mRegisterOTPView, true) {
            @Override
            protected void onSuccess(Void result) {
                mRegisterOTPView.stopAccountSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.stopAccountFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(mRegisterOTPView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mRegisterOTPView.transferSurePaySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mRegisterOTPView.transferSurePayFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark, String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName) {
        AppClient.getInstance().transferToContact(payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, payerName, new LifecycleMVPResultCallback<TransferEntity>(mRegisterOTPView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mRegisterOTPView.transferToContactSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mRegisterOTPView.transferToContactFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType) {
        AppClient.getInstance().confirmPay(orderNo, paymentMethodNo, orderType, new LifecycleMVPResultCallback<TransferEntity>(mRegisterOTPView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mRegisterOTPView.confirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.confirmPayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void splitBill(String oldOrderNo, String receivableAmount, String currencyCode, String orderRemark, List<SplitBillPayerEntity> receiverInfoList) {
        AppClient.getInstance().splitBill(oldOrderNo, receivableAmount, currencyCode, orderRemark, receiverInfoList, new LifecycleMVPResultCallback<TransferEntity>(mRegisterOTPView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mRegisterOTPView.splitBillSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.splitBillFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void trustDevice() {
        AppClient.getInstance().trustDevice(new LifecycleMVPResultCallback<Void>(mRegisterOTPView,false) {
            @Override
            protected void onSuccess(Void result) {
                mRegisterOTPView.trustDeviceSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRegisterOTPView.trustDeviceFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName, String nickName, String relationshipCode, String callingCode,
                                  String phone, String transferDestinationCountryCode, String payeeInfoCountryCode,
                                  String birthPlace, String birthDate, String sex, String cityName, String districtName,
                                  String poBox, String buildingNo, String street, String idNo, String idExpiry, String bankAccountType,
                                  String ibanNo, String bankAccountNo, String saveFlag, String receiptOrgName, String receiptOrgBranchName,
                                  String cityId, String currencyCode, String channelPayeeId, String channelCode,String branchId) {
        AppClient.getInstance().imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag,receiptOrgName,
                receiptOrgBranchName,cityId,currencyCode,channelPayeeId,channelCode,branchId, new LifecycleMVPResultCallback<ImrAddBeneResultEntity>(mRegisterOTPView, true) {
                    @Override
                    protected void onSuccess(ImrAddBeneResultEntity result) {
                        mRegisterOTPView.imrAddBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mRegisterOTPView.imrAddBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo) {
        AppClient.getInstance().saveImrBeneficiaryInfo(beneficiaryInfo.payeeInfoId,
                beneficiaryInfo.transferDestinationCountryCode, beneficiaryInfo.receiptMethod,
                beneficiaryInfo.receiptOrgCode, beneficiaryInfo.receiptOrgBranchCode,
                beneficiaryInfo.payeeFullName, beneficiaryInfo.nickName, beneficiaryInfo.relationshipCode,
                beneficiaryInfo.callingCode, beneficiaryInfo.phone, beneficiaryInfo.payeeInfoCountryCode,
                beneficiaryInfo.birthPlace, beneficiaryInfo.birthDate, beneficiaryInfo.sex,
                beneficiaryInfo.cityName, beneficiaryInfo.districtName, beneficiaryInfo.poBox,
                beneficiaryInfo.buildingNo, beneficiaryInfo.street, beneficiaryInfo.idNo,
                beneficiaryInfo.idExpiry, beneficiaryInfo.bankAccountType, beneficiaryInfo.ibanNo,
                beneficiaryInfo.bankAccountNo,beneficiaryInfo.receiptOrgName,beneficiaryInfo.receiptOrgBranchName,
                beneficiaryInfo.cityId,beneficiaryInfo.currencyCode,beneficiaryInfo.channelPayeeId,beneficiaryInfo.channelCode,beneficiaryInfo.branchId,
                new LifecycleMVPResultCallback<ResponseEntity>(mRegisterOTPView, true) {
                    @Override
                    protected void onSuccess(ResponseEntity result) {
                        mRegisterOTPView.imrEditBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mRegisterOTPView.imrEditBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }
}
