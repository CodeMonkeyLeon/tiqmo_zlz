package cn.swiftpass.wallet.tiqmo.module.guide.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.module.guide.entity.GuideDefaultPoint
import cn.swiftpass.wallet.tiqmo.module.guide.entity.GuideSelectedPoint
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils

class GuidePointView : View {

    private lateinit var mContext: Context


    private var mPointSize: Float = 0f

    private var mR: Float = 0f

    private var mSelectedIndex = 0

    private val mSelectedPaint by lazy {
        Paint().apply {
            val paintColor = mContext.getColor(
                ThemeSourceUtils.getSourceID(
                    mContext,
                    R.attr.color_guide_point_selected
                )
            )
            color = paintColor
            style = Paint.Style.FILL
            isAntiAlias = true
        }
    }


    private val mDefaultPaint by lazy {
        Paint().apply {
            val paintColor = mContext.getColor(
                ThemeSourceUtils.getSourceID(
                    mContext,
                    R.attr.color_guide_point_default
                )
            )
            color = paintColor
            style = Paint.Style.FILL
            isAntiAlias = true
        }
    }


    private val mDefaultPointCoordinate by lazy {
        mutableListOf<GuideDefaultPoint>()
    }


    private val mSelectedPointCoordinate by lazy {
        mutableListOf<GuideSelectedPoint>()
    }

    constructor(context: Context) : super(context) {
        mContext = context
    }


    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        mContext = context
    }


    constructor(context: Context, attributeSet: AttributeSet, def: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = measuredWidth
        mDefaultPointCoordinate.clear()
        mSelectedPointCoordinate.clear()

        mPointSize = width / 11f
        mR = mPointSize / 2

        var stX = 0f
        for (i in 0..5) {
            mDefaultPointCoordinate.add(
                GuideDefaultPoint(
                    stX,
                    0f,
                    mR
                )
            )
            stX += mPointSize * 2
        }


        for (i in 0..3) {
            val p = mDefaultPointCoordinate[i]
            mSelectedPointCoordinate.add(
                GuideSelectedPoint(
                    p.x,
                    p.y,
                    mR,
                    mPointSize
                )
            )
        }
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.run {

            mDefaultPointCoordinate.forEach {
                drawCircle(it.getRx(), it.getRy(), it.r, mDefaultPaint)
            }

            drawSelectedPoint(
                canvas,
                mSelectedPointCoordinate[mSelectedIndex]
            )
        }
    }


    private fun drawSelectedPoint(
        canvas: Canvas,
        point: GuideSelectedPoint
    ) {
        canvas.drawCircle(point.getLeftRx(), point.getLeftRy(), point.r, mSelectedPaint)
        canvas.drawCircle(point.getRightRx(), point.getRightRy(), point.r, mSelectedPaint)
        canvas.drawRect(
            point.getTopRightX(),
            point.getTopRightY(),
            point.getBottomRightX(),
            point.getBottomRightY(),
            mSelectedPaint
        )
    }


    fun setSelect(index: Int) {
        mSelectedIndex = index
        invalidate()
    }

}