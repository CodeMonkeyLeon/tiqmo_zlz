package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ReferralCodeDescEntity extends BaseEntity {

    public String activityStartTime;
    public String activityEndTime;
    public String activityPicUrl;
    public String haveReferralActivityFlag;

    public String[] useReferralCodeDesc = new String[3];
    public String[] amountAndCurrencyInfo = new String[2];
}
