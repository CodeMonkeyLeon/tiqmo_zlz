package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity

import java.io.Serializable

data class CardBannerEntity(
    val imgId: Int
) : Serializable
