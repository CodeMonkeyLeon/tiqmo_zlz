package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrCityEntity extends BaseEntity {

    public String cityId;
    public String cityName;
}
