package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpThreeEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class QuestionListAdapter extends BaseRecyclerAdapter<HelpThreeEntity> {

    private int currentPosition = 0;
    //0点赞 1反对
    private String thumb;
    private String comment;

    private String filterString = "";
    private SubmitListener submitListener;

    public void setSubmitListener(SubmitListener submitListener) {
        this.submitListener = submitListener;
    }

    public interface SubmitListener {
        void clickSubmitComment(int position,String id,String comment);
        void clickSubmitThumb(int position,String id,String comment,String thumb);
    }

    public void setFilterString(final String filterString) {
        this.filterString = filterString;
    }

    public QuestionListAdapter(@Nullable List<HelpThreeEntity> data) {
        super(R.layout.item_help_three,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, HelpThreeEntity helpThreeEntity, int position) {
        LinearLayout llHelpType = baseViewHolder.getView(R.id.ll_help_type);
        LinearLayout llQuestionTitle = baseViewHolder.getView(R.id.ll_question_title);
        ImageView ivHelpArrow = baseViewHolder.getView(R.id.iv_help_arrow);
        TextView tvQuestionContent = baseViewHolder.getView(R.id.tv_question_content);
        TextView tvQuestionTitle = baseViewHolder.getView(R.id.tv_question_title);
        CheckBox cbGood = baseViewHolder.getView(R.id.cb_good);
        CheckBox cbBad = baseViewHolder.getView(R.id.cb_bad);
        EditTextWithDel etImprove = baseViewHolder.getView(R.id.et_improve);
        etImprove.getTlEdit().setHint(mContext.getString(R.string.DigitalCard_86));
        TextView tvSubmit = baseViewHolder.getView(R.id.tv_submit);
        TextView tvSubmitSuccess = baseViewHolder.getView(R.id.tv_submit_success);
        LinearLayout llQuestionContent = baseViewHolder.getView(R.id.ll_question_content);
        LinearLayout llSubmitContent = baseViewHolder.getView(R.id.ll_submit_content);
        LinearLayout llSubmitSuccess = baseViewHolder.getView(R.id.ll_submit_success);
        LocaleUtils.viewRotationY(mContext,ivHelpArrow);
        if(!TextUtils.isEmpty(filterString)) {
            tvQuestionTitle.setText(AndroidUtils.getHighLightText(mContext, helpThreeEntity.title, filterString,""));
            tvQuestionContent.setText(AndroidUtils.getHighLightText(mContext, helpThreeEntity.content, filterString,""));
        }else{
            tvQuestionTitle.setText(helpThreeEntity.title);
            tvQuestionContent.setText(helpThreeEntity.content);
        }
        String id = helpThreeEntity.id;
        boolean hasSubmit = helpThreeEntity.hasSubmit;

        comment = helpThreeEntity.comment;

        tvSubmit.setVisibility(!TextUtils.isEmpty(comment)?View.VISIBLE:View.GONE);
        tvSubmit.setEnabled(!TextUtils.isEmpty(comment));

        if (etImprove.getTag()!=null && etImprove.getTag() instanceof  TextWatcher){
            etImprove.getEditText().removeTextChangedListener((TextWatcher) etImprove.getTag());
            etImprove.getEditText().clearFocus();
        }
        etImprove.setContentText(comment);
        TextWatcher watcher=  new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                comment = s.toString();
                helpThreeEntity.comment = comment;
//                getDataList().get(position).comment = comment;
                tvSubmit.setVisibility(!TextUtils.isEmpty(comment)?View.VISIBLE:View.GONE);
                tvSubmit.setEnabled(!TextUtils.isEmpty(comment));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        etImprove.getEditText().addTextChangedListener(watcher);
        etImprove.setTag(watcher);


        thumb = helpThreeEntity.thumb;
        if("1".equals(thumb)){
            cbGood.setChecked(false);
            cbBad.setChecked(true);
//            cbBad.setEnabled(false);
            cbGood.setEnabled(true);
        }else if("0".equals(thumb)){
            cbGood.setChecked(true);
            cbBad.setChecked(false);
            cbBad.setEnabled(true);
//            cbGood.setEnabled(false);
        }else{
            cbGood.setChecked(false);
            cbBad.setChecked(false);
            cbBad.setEnabled(true);
            cbGood.setEnabled(true);
        }
        if(hasSubmit){
            llSubmitSuccess.setVisibility(View.VISIBLE);
            llSubmitContent.setVisibility(View.GONE);
        }else{
            llSubmitSuccess.setVisibility(View.GONE);
            llSubmitContent.setVisibility(View.VISIBLE);
        }

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(submitListener != null){
                    submitListener.clickSubmitComment(position,id,helpThreeEntity.comment);
                }
            }
        });

        cbBad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cbGood.setEnabled(true);
//                    cbBad.setEnabled(false);
                    thumb = "1";
                    cbGood.setChecked(false);
                    if(submitListener != null){
                        submitListener.clickSubmitThumb(position,id,helpThreeEntity.comment,thumb);
                    }
                }
            }
        });

        cbGood.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
//                    cbGood.setEnabled(false);
                    cbBad.setEnabled(true);
                    thumb = "0";
                    cbBad.setChecked(false);
                    if(submitListener != null){
                        submitListener.clickSubmitThumb(position,id,helpThreeEntity.comment,thumb);
                    }
                }
            }
        });

        if(position == currentPosition){
            llQuestionContent.setVisibility(View.VISIBLE);
            ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_down_arrow));
        }else{
            llQuestionContent.setVisibility(View.GONE);
            ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_right_arrow));
        }
        llQuestionTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(llQuestionContent.getVisibility() == View.GONE){
                    currentPosition = position;
                    llQuestionContent.setVisibility(View.VISIBLE);
                    ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_down_arrow));
                }else{
                    llQuestionContent.setVisibility(View.GONE);
                    ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_right_arrow));
                    currentPosition = -1;
                }
                notifyDataSetChanged();
            }
        });
    }
}
