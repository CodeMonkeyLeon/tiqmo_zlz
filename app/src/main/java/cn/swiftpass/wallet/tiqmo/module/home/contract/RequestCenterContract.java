package cn.swiftpass.wallet.tiqmo.module.home.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MsgListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;

public class RequestCenterContract {
    public interface RequestCenterListView extends BaseView<RequestCenterContract.RequestCenterListPresenter> {
        void getRequestCenterListSuccess(RequestCenterListEntity result);

        void getRequestCenterListFail(String errorCode, String errorMsg);

        void sendNotifySuccess(PayBillOrderInfoEntity result);

        void sendNotifyFail(String errorCode, String errorMsg);

        void getMsgListSuccess(MsgListEntity msgListEntity);

        void getMsgListFail(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void showErrorMsg(String errCode, String errMsg);

        void removeRequestSuccess();

        void removeRequestFail(String errorCode, String errorMsg);

        void transferSurePaySuccess(TransferEntity transferEntity);

        void transferSurePayFail(String errCode, String errMsg);
    }

    public interface RequestCenterListPresenter extends BasePresenter<RequestCenterContract.RequestCenterListView> {
        void getRequestCenterList();

        void sendNotify(List<NotifyEntity> receivers);

        void getMsgList(int pageSize, int pageNum);

        void removeRequest(String infoId);

        void getLimit(String type);

        void updateClickTimeService(String messageId);

        void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                             String exchangeRate, String transCurrencyCode, String transFees, String vat);
    }
}
