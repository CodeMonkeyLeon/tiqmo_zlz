package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ResultEntity extends BaseEntity {

    public String result;

    public boolean renewIdSuccess(ResultEntity resultEntity) {
        return "1".equals(resultEntity.result);
    }
}
