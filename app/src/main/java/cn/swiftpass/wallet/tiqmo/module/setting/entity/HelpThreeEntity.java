package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class HelpThreeEntity extends BaseEntity {
    public String id;
    public String parentId;
    public String title;
    public String content;
    public String nodeLevel;
    public boolean hasSubmit;
    public String thumb = "";
    public String comment = "";
    public List<HelpTwoEntity> children = new ArrayList<>();
}
