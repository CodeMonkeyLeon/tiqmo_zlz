//package cn.swiftpass.wallet.tiqmo.sdk.net.api;
//
//import java.math.BigDecimal;
//
//import cn.swiftpass.wallet.sdk.entity.JsonResponse;
//import cn.swiftpass.wallet.sdk.entity.OrderEntity;
//import cn.swiftpass.wallet.sdk.entity.PayeeInfoListEntity;
//import cn.swiftpass.wallet.sdk.entity.SupportedCurrenciesEntity;
//import cn.swiftpass.wallet.sdk.entity.TransferPayeeEntity;
//import cn.swiftpass.wallet.sdk.net.RequestCall;
//import cn.swiftpass.wallet.sdk.net.annotation.Headers;
//import cn.swiftpass.wallet.sdk.net.annotation.Param;
//
///**
// * Created by 叶智星 on 2018年09月18日.
// * 每一个不曾起舞的日子，都是对生命的辜负。
// */
//public interface TransferApi {
//
//
//    @Headers({"Service-Id:1047"})
//    RequestCall<JsonResponse<TransferPayeeEntity>> checkTransferTelephone(@Param("callingCode") String callingCode,
//                                                                          @Param("payeeNumber") String payeeNumber,
//                                                                          @Param("payeeNumberType") String payeeNumberType);
//
//
//    @Headers({"Service-Id:1052"})
//    RequestCall<JsonResponse<Void>> checkTransferName(@Param("phone") String telephone,
//                                                      @Param("username") String name);
//
//
//    @Headers({"Service-Id:1043"})
//    RequestCall<JsonResponse<SupportedCurrenciesEntity>> getSupportedCurrencies(@Param("payeePhone") String telephone);
//
//
//    @Headers({"Service-Id:1011"})
//    RequestCall<JsonResponse<OrderEntity>> transfer(@Param("payeeUserId") String payeeUserId,
//                                                    @Param("payeeAmount") BigDecimal amount,
//                                                    @Param("payeeCurrencyCode") String payeeCurrencyCode,
//                                                    @Param("remark") String remark,
//                                                    @Param("payeeNumberType") String payeeNumberType);
//
//    @Headers({"Service-Id:1061"})
//    RequestCall<JsonResponse<PayeeInfoListEntity>> queryLatestTransfer(@Param("payerUserId") String payerUserId);
//
//}
