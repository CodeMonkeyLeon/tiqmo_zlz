package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import cn.swiftpass.wallet.tiqmo.module.home.contract.CharityContract;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class CharityOrderPresenter implements CharityContract.CharityPresenter {

    private CharityContract.CharityView mCharityView;

    @Override
    public void preCharityOrder(String channelCode, String payAmount,String domainDonation) {
        AppClient.getInstance().getTransferManager().preCharityOrder(channelCode, payAmount,domainDonation, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mCharityView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mCharityView.preCharityOrderSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCharityView.preCharityOrderFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(mCharityView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                mCharityView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCharityView.checkOutFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(CharityContract.CharityView charityView) {
        this.mCharityView = charityView;
    }

    @Override
    public void detachView() {
        mCharityView = null;
    }
}
