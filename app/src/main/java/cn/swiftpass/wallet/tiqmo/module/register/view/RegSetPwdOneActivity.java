package cn.swiftpass.wallet.tiqmo.module.register.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.NumberGridAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.NumberEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyGridView;

public class RegSetPwdOneActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_reg_name)
    TextView tvUserName;
    @BindView(R.id.grid_number)
    MyGridView gridNumber;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.cb_reg_one)
    CheckBox cbRegOne;
    @BindView(R.id.cb_reg_two)
    CheckBox cbRegTwo;
    @BindView(R.id.cb_reg_three)
    CheckBox cbRegThree;
    @BindView(R.id.cb_reg_four)
    CheckBox cbRegFour;
    @BindView(R.id.cb_reg_five)
    CheckBox cbRegFive;
    @BindView(R.id.cb_reg_six)
    CheckBox cbRegSix;
    @BindView(R.id.ll_check)
    LinearLayout llCheck;
    @BindView(R.id.iv_user_avatar)
    RoundedImageView ivUserAvatar;
    @BindView(R.id.con_avatar)
    ConstraintLayout conAvatar;
    @BindView(R.id.tv_name_first)
    TextView tvNameFirst;
    @BindView(R.id.tv_error)
    TextView tvError;

    private NumberGridAdapter numberGridAdapter;
    private StringBuilder stringBuilder;
    private CheckPhoneEntity checkPhoneEntity;
    private boolean isChangeLoginPd;
    private String birthday;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_set_reg_pwd_one;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if(getIntent() != null && getIntent().getExtras() != null){
            isChangeLoginPd = getIntent().getExtras().getBoolean(Constants.isChangeLoginPd);
        }
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if(isChangeLoginPd){
            tvTitle.setText(R.string.sprint19_33);
        }else {
            if (checkPhoneEntity != null && checkPhoneEntity.operateType == 1) {
                ivBack.setVisibility(View.GONE);
                tvTitle.setText(R.string.sprint19_21);
            } else {
                tvTitle.setText(R.string.sprint19_31);
                ivBack.setVisibility(View.GONE);
            }
        }
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkUser(userInfoEntity);

        stringBuilder = new StringBuilder();
        numberGridAdapter = new NumberGridAdapter(mContext);
        gridNumber.setAdapter(numberGridAdapter);
        numberGridAdapter.setOnItemClickListener(new NumberGridAdapter.OnItemClick() {
            @Override
            public void onItemClick(NumberEntity numberEntity, int position) {
                if(numberEntity != null){
                    int type = numberEntity.type;
                    int length = stringBuilder.length();
                    if(type == 1){
                        if(length == 6)return;
                        String number = numberEntity.number;
                        if(!TextUtils.isEmpty(number)) {
                            stringBuilder.append(number);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length);
                            checkBox.setChecked(true);
                        }
                    }else if(type == 2){
                        if(stringBuilder.length()>0) {
                            tvError.setVisibility(View.GONE);
                            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length-1);
                            checkBox.setChecked(false);
                        }
                    }
                    if(stringBuilder.length() == 6){
                        if(checkPhoneEntity != null) {
                            if(RegexUtil.isMultiChar(stringBuilder.toString())){
                                setErrorMsg(getString(R.string.sprint19_53));
                                return;
                            }else if(RegexUtil.isAddNumber(stringBuilder.toString())){
                                setErrorMsg(getString(R.string.sprint19_54));
                                return;
                            }else if(RegexUtil.isIdBirthdayNumber(stringBuilder.toString(),checkPhoneEntity.checkIdNumber+checkPhoneEntity.phone,birthday)){
                                setErrorMsg(getString(R.string.sprint19_55));
                                return;
                            }
                            checkPhoneEntity.passwordOne = stringBuilder.toString();
                            UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                        }
                        HashMap<String, Object> mHashMap = new HashMap<>(16);
                        mHashMap.put(Constants.isChangeLoginPd, isChangeLoginPd);
                        ActivitySkipUtil.startAnotherActivity(RegSetPwdOneActivity.this, RegSetPwdTwoActivity.class,mHashMap,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });
        numberGridAdapter.changeData(AndroidUtils.getRegNumberList(mContext));
    }

    private void setErrorMsg(String errorMsg){
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(errorMsg);
        for(int i=0;i<6;i++) {
            CheckBox checkBox = (CheckBox) llCheck.getChildAt(i);
            checkBox.setChecked(false);
        }
        stringBuilder = new StringBuilder();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvError.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (checkPhoneEntity != null && checkPhoneEntity.operateType == 1) {
            return;
        }
        super.onBackPressed();
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.getAvatar();
            String gender = userInfoEntity.gender;
            birthday = userInfoEntity.birthday;
            if (!TextUtils.isEmpty(userInfoEntity.userName)) {
                if (userInfoEntity.userName.contains(" ")) {
                    String[] fastName = userInfoEntity.userName.split(" ");
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + fastName[0] + "!");
                } else {
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + userInfoEntity.userName + "!");
                }
                tvNameFirst.setText(userInfoEntity.userName.substring(0,1).toUpperCase(Locale.ENGLISH));
            }
            if (!TextUtils.isEmpty(avatar)) {
                conAvatar.setVisibility(View.GONE);
                ivUserAvatar.setVisibility(View.VISIBLE);
                Glide.with(this).clear(ivUserAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .placeholder(R.drawable.l_default_avatar)
                        .into(ivUserAvatar);
            } else {
                conAvatar.setVisibility(View.VISIBLE);
                ivUserAvatar.setVisibility(View.GONE);
            }
        }else{
            if(checkPhoneEntity != null){
                birthday = checkPhoneEntity.birthday;
                tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + checkPhoneEntity.firstName + "!");
                conAvatar.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(checkPhoneEntity.firstName)) {
                    tvNameFirst.setText(checkPhoneEntity.firstName.substring(0, 1).toUpperCase(Locale.ENGLISH));
                }
            }
        }
    }

    @OnClick(R.id.iv_back)
    public void onClick() {
        finish();
    }
}
