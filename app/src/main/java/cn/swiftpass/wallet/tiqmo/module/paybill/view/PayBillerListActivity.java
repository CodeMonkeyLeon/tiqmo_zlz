package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillerListAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.interfaces.OnBillerListItemClickListener;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillerListPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class PayBillerListActivity extends BaseCompatActivity<PayBillContract.BillerListPresenter> implements PayBillContract.BillerListView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_country_list)
    RecyclerView rvCountryList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
//    @BindView(R.id.sc_content)
//    NestedScrollView scContent;

    private PayBillerListAdapter payBillerListAdapter;

    private List<PayBillerEntity> showBillerList = new ArrayList<>();
    private List<PayBillerEntity> filterBillerList = new ArrayList<>();

    private String searchString;

    private String countryCode, billerType, billerDescription, channelCode;

    private String billerId, billerName, billerLogo;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_select_biller;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_10);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        etSearch.setHint(getString(R.string.BillPay_11));

        if (getIntent() != null && getIntent().getExtras() != null) {
            countryCode = getIntent().getExtras().getString(Constants.paybill_countryCode);
            billerType = getIntent().getExtras().getString(Constants.paybill_billerType);
            billerDescription = getIntent().getExtras().getString(Constants.paybill_billerDescription);
            channelCode = getIntent().getExtras().getString(Constants.CHANNEL_CODE);
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvCountryList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvCountryList.setLayoutManager(manager);
        payBillerListAdapter = new PayBillerListAdapter(showBillerList);
        payBillerListAdapter.bindToRecyclerView(rvCountryList);


        payBillerListAdapter.setOnBillerListItemClickListener(new OnBillerListItemClickListener() {
            @Override
            public void onBillServiceBillerListItemClick(int position, PayBillerEntity payBillerEntity) {
                if (payBillerEntity != null) {
                    billerId = payBillerEntity.billerId;
                    billerName = payBillerEntity.billerName;
                    billerLogo = payBillerEntity.imgUrl;
                    mPresenter.getPayBillSkuList(billerId, channelCode);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etSearch.hideErrorView();
                searchString = s.toString();
                if (showBillerList != null && showBillerList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });

        mPresenter.getPayBillerList(countryCode, billerType, billerDescription, channelCode);
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            filterBillerList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
//                sideBar.setVisibility(View.VISIBLE);
                filterBillerList.clear();
                payBillerListAdapter.setDataList(showBillerList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (showBillerList != null && showBillerList.size() > 0) {
                int size = showBillerList.size();
                for (int i = 0; i < size; i++) {
                    PayBillerEntity payBillerEntity = showBillerList.get(i);
                    String name = payBillerEntity.billerName;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterBillerList.add(payBillerEntity);
                    }
                }
                payBillerListAdapter.setDataList(filterBillerList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }


    @Override
    public void getPayBillerListSuccess(PayBillerListEntity payBillerListEntity) {
        if (payBillerListEntity != null) {
            showBillerList.clear();
            showBillerList.addAll(payBillerListEntity.billerList);
            payBillerListAdapter.setDataList(showBillerList);
        }
    }

    @Override
    public void getPayBillSkuListSuccess(PayBillSkuListEntity payBillSkuListEntity) {
        if (payBillSkuListEntity != null) {
            int sceneType = payBillSkuListEntity.sceneType;
            String sku = payBillSkuListEntity.sku;
            if (sceneType == 1 || sceneType == 3) {
                if (!TextUtils.isEmpty(sku)) {
//                    HashMap<String, Object> mHashMap = new HashMap<>();
//                    mHashMap.put(Constants.paybill_billerId, billerId);
//                    mHashMap.put(Constants.paybill_billerName, billerName);
//                    mHashMap.put(Constants.paybill_billerLogo, billerLogo);
//                    mHashMap.put(Constants.paybill_billerSku, sku);
//                    mHashMap.put(Constants.CHANNEL_CODE, channelCode);
//                    ActivitySkipUtil.startAnotherActivity(PayBillerListActivity.this, PayBillFetchActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

                    PayBillFetchActivity.startPayBillFetchActivity(PayBillerListActivity.this,
                            billerId,
                            billerName,
                            billerLogo,
                            sku,
                            channelCode);
                }
            } else {
//                HashMap<String, Object> mHashMap = new HashMap<>();
//                mHashMap.put(Constants.paybill_billerId, billerId);
//                mHashMap.put(Constants.paybill_billerName, billerName);
//                mHashMap.put(Constants.paybill_billerLogo, billerLogo);
//                mHashMap.put(Constants.paybill_billerSkuListEntity, payBillSkuListEntity);
//                mHashMap.put(Constants.CHANNEL_CODE, channelCode);
//                ActivitySkipUtil.startAnotherActivity(PayBillerListActivity.this, PayBillSkuActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                PayBillSkuActivity.startPayBillSkuActivity(PayBillerListActivity.this,
                        billerId,
                        billerName,
                        billerLogo,
                        channelCode,
                        payBillSkuListEntity);
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillerListPresenter getPresenter() {
        return new PayBillerListPresenter();
    }
}
