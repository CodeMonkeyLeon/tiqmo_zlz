package cn.swiftpass.wallet.tiqmo.module.login.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.MainActivity;
import cn.swiftpass.wallet.tiqmo.module.login.contract.LoginContract;
import cn.swiftpass.wallet.tiqmo.module.login.presenter.LoginPresent;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.adapter.NumberGridAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.NumberEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanPayActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.TiqmoLaunchSDK;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.AuthApi;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.app2app.PaySDKEntity;
import cn.swiftpass.wallet.tiqmo.support.app2app.PaySDKUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BiometricInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.GPSUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyGridView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CommonNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.LogOutDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.TrustDeviceDialog;

public class LoginFastNewActivity extends BaseCompatActivity<LoginContract.Presenter> implements LoginContract.View {

    @BindView(R.id.iv_head_circle)
    ImageView ivCircle;
    @BindView(R.id.iv_splash)
    ImageView ivSplash;
    @BindView(R.id.iv_log_out)
    ImageView ivLogOut;
    @BindView(R.id.iv_user_avatar)
    RoundedImageView ivUserAvatar;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.cb_reg_one)
    CheckBox cbRegOne;
    @BindView(R.id.cb_reg_two)
    CheckBox cbRegTwo;
    @BindView(R.id.cb_reg_three)
    CheckBox cbRegThree;
    @BindView(R.id.cb_reg_four)
    CheckBox cbRegFour;
    @BindView(R.id.cb_reg_five)
    CheckBox cbRegFive;
    @BindView(R.id.cb_reg_six)
    CheckBox cbRegSix;
    @BindView(R.id.ll_check)
    LinearLayout llCheck;
    @BindView(R.id.tv_forget_pwd)
    TextView tvForgetPwd;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.ll_face_login)
    LinearLayout llFaceLogin;
    @BindView(R.id.grid_number)
    MyGridView gridNumber;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    //    @BindView(R.id.sc_content)
//    NestedScrollView scContent;
    @BindView(R.id.con_avatar)
    ConstraintLayout conAvatar;
    @BindView(R.id.tv_name_first)
    TextView tvNameFirst;

    private NumberGridAdapter numberGridAdapter;
    private StringBuilder stringBuilder;

    private String phone, password;
    //上一次用户登录的信息
    private UserInfoEntity mUserInfoEntity;

    private CardNotifyEntity cardNotifyEntity;
    private int msgType;

    private static final String INTENT_EXTRA_LAST_USER_INFO = "LastUserInfo";
    private static final String INTENT_EXTRA_MESSAGE = "MessageEntity";

    private static LoginFastNewActivity LoginFastNewActivity;
    private String loginType;
    private String lockAcc;
    private String callingCode;

    //    private IProviderBinder binder;
    AsyncTask<Integer, Void, Void> asyncTask;
    String resourcePath = "0";
    private BottomDialog bottomDialog;

    private CheckPhoneEntity checkPhoneEntity;

    private boolean isFromSDK = false;

    public static LoginFastNewActivity getLoginFastNewActivity() {
        return LoginFastNewActivity;
    }

    public static void startActivityOfNewTaskType(Context context, UserInfoEntity lastUserInfo, String message) {
        Intent intent = new Intent(context, LoginFastNewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_EXTRA_LAST_USER_INFO, lastUserInfo);
        intent.putExtra(INTENT_EXTRA_MESSAGE, message);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_fast_login_new;
    }

    private void getIntentData() {
        Uri uri = getIntent().getData();
        if (PaySDKUtils.checkUriIsFromPaySDK(uri)) {
            String orderInfo = getIntent().getStringExtra("orderInfo");
            String appID = getIntent().getStringExtra("appID");
            SpUtils.getInstance().setPaymentCheck(new PaySDKEntity(orderInfo, appID));
            isFromSDK = true;
            checkUserInfo();
        } else {
            SpUtils.getInstance().setPaymentCheck(null);
        }
    }

    private void checkUserInfo() {
        if (AppClient.getInstance().isLoginSuccess() && !isFinishing()) {
            startToPayCheckoutActivity();
        }
    }

    private void startToPayCheckoutActivity() {
        if (SpUtils.getInstance().getPaymentCheck() == null) {
            return;
        }
        ActivitySkipUtil.startAnotherActivity(this,
                ScanPayActivity.class, "PaySDKData",
                SpUtils.getInstance().getPaymentCheck(), RIGHT_IN);
        finish();
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        getIntentData();
        LoginFastNewActivity = this;
        if (getIntent() != null && getIntent().getExtras() != null) {
            msgType = getIntent().getIntExtra(Constants.MSGTYPE, -100);
            switch (msgType) {
                case -1:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_CARD_ENTITY);
                    break;
                case -2:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_MONEY_ENTITY);
                    break;
                case -3:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.REQUEST_TRANSFER_ENTITY);
                    break;
                case -4:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.MSG_ENTITY);
                    break;
                default:
                    break;
            }
        }
        ivLogOut.setVisibility(View.VISIBLE);
        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();

        mUserInfoEntity = getUserInfoEntity();
        checkUser(mUserInfoEntity);

        LocaleUtils.viewRotationY(this, ivCircle,ivLogOut);

        callingCode = UserInfoManager.getInstance().getCallingCode();

        stringBuilder = new StringBuilder();
        numberGridAdapter = new NumberGridAdapter(mContext);
        gridNumber.setAdapter(numberGridAdapter);
        numberGridAdapter.setOnItemClickListener(new NumberGridAdapter.OnItemClick() {
            @Override
            public void onItemClick(NumberEntity numberEntity, int position) {
                if(numberEntity != null){
                    int type = numberEntity.type;
                    int length = stringBuilder.length();
                    if(type == 1){
                        if(length == 6)return;
                        String number = numberEntity.number;
                        if(!TextUtils.isEmpty(number)) {
                            stringBuilder.append(number);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length);
                            if(checkBox != null) {
                                checkBox.setChecked(true);
                            }
                        }
                    }else if(type == 2){
                        if(stringBuilder.length()>0) {
                            tvError.setVisibility(View.GONE);
                            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                            CheckBox checkBox = (CheckBox) llCheck.getChildAt(length-1);
                            if(checkBox != null) {
                                checkBox.setChecked(false);
                            }
                        }
                    }
                    if(stringBuilder.length() == 6){
                        password = stringBuilder.toString();
                        loginRequest(AuthApi.LOGIN_TYPE_PD);
                    }
                }
            }
        });
        numberGridAdapter.changeData(AndroidUtils.getRegNumberList(mContext));
        getPermission();
        if (getConfigEntity() != null) {
            TiqmoLaunchSDK.showUpdateIfNeeded(mContext, getConfigEntity());
        }
    }

    private void setErrorMsg(String errorMsg){
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(errorMsg);
        for(int i=0;i<6;i++) {
            CheckBox checkBox = (CheckBox) llCheck.getChildAt(i);
            checkBox.setChecked(false);
        }
        stringBuilder = new StringBuilder();
    }

    private void showLogOutDialog() {
        LogOutDialog logOutDialog = new LogOutDialog(mContext);
        logOutDialog.setLogoutClickListener(new LogOutDialog.LogoutClickListener() {
            @Override
            public void clickLogout() {
                AppClient.getInstance().logout(new ResultCallback<Void>() {
                    @Override
                    public void onResult(Void result) {

                    }

                    @Override
                    public void onFailure(String code, String error) {

                    }
                });
                AppClient.getInstance().clearLastSessionInfo();
                AppClient.getInstance().setLastUserID("");
                AppClient.getInstance().getUserManager().clearLastUserInfo();
                ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, ChooseLanguageActivity.class,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                ProjectApp.removeAllTaskIncludeMainStack();
            }

            @Override
            public void clickKeepSign() {

            }
        });
        logOutDialog.setCancelable(false);
        logOutDialog.setCanceledOnTouchOutside(false);
        logOutDialog.showWithBottomAnim();
    }

    @OnClick({R.id.tv_forget_pwd,R.id.ll_face_login, R.id.iv_log_out})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        switch (view.getId()) {
            case R.id.tv_forget_pwd:
                if(AndroidUtils.isCloseUse(this,Constants.profile_forgotLoginPd))return;
                mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_FORGET_LOGIN_PD);
                ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, RegisterActivity.class,
                        mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_log_out:
                showLogOutDialog();
//                ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, LoginActivity.class,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.ll_face_login:
                if (AndroidUtils.isOpenBiometricLogin()) {
                    loginWithFio();
                }
                break;
            default:
                break;
        }
    }

    private void showVpnInterceptDialog() {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.vpn_intercept_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        LinearLayout llContent = view.findViewById(R.id.ll_content);

        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                /* transaction completed */
                Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);
                /* resource path if needed */
                String resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);

                if (transaction.getTransactionType() == TransactionType.SYNC) {
                    /* check the result of synchronous transaction */
                } else {
                    /* wait for the asynchronous transaction callback in the onNewIntent() */
                }

                break;
            case CheckoutActivity.RESULT_CANCELED:
                /* shopper canceled the checkout process */
                Toast.makeText(getBaseContext(),"canceled",Toast.LENGTH_LONG).show();
                break;
            case CheckoutActivity.RESULT_ERROR:
                /* error occurred */
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);

                Toast.makeText(getBaseContext(),"error",Toast.LENGTH_LONG).show();
                Log.e("errorrr",String.valueOf(error.getErrorInfo()));
                Log.e("errorrr2",String.valueOf(error.getErrorCode()));
                Log.e("errorrr3",String.valueOf(error.getErrorMessage()));
                Log.e("errorrr4",String.valueOf(error.describeContents()));
        }
    }

    private void loginWithFio() {
        BiometricInstance.getInstance().clickWithFio(LoginFastNewActivity.this, true, true,
                new BiometricInstance.BiometricListener() {
                    @Override
                    public void onBiometricFail(String errMsg) {
                        showTipDialog(errMsg);
                    }

                    @Override
                    public void onBiometricSuccess() {
                        loginRequest(AuthApi.LOGIN_TYPE_FIO);
                    }

                    @Override
                    public void onBiometricError() {

                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
        GPSUtils.getInstance(mContext).removeListener();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ACTIVATE_USER == event.getEventType()) {
            dismissNoticeDialog();
            mPresenter.activateUserStatus(callingCode,phone,lockAcc);
        }
    }

    private void loginRequest(String type) {
        if (!getPermission()) {
            return;
        }
        if (AndroidUtils.checkVPN(mContext)) {
            showVpnInterceptDialog();
            return;
        }
        loginType = type;
        mPresenter.loginAccount(callingCode, phone, password, "", loginType, ProjectApp.getLongitude(), ProjectApp.getLatitude());
//        mPresenter.loginAccount("966", "545556776", "147258", "", loginType, ProjectApp.getLongitude(), ProjectApp.getLatitude());
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
        AppClient.getInstance().clearLastSessionInfo();
        if (ProjectApp.isBiometricSupported() && AndroidUtils.isOpenBiometricLogin()) {
            llFaceLogin.setVisibility(View.VISIBLE);
        } else {
            llFaceLogin.setVisibility(View.INVISIBLE);
        }

        getConfig();
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkUser(userInfoEntity);

    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.getAvatar();
            String gender = userInfoEntity.gender;
            if (!TextUtils.isEmpty(userInfoEntity.userName)) {
                if (userInfoEntity.userName.contains(" ")) {
                    String[] fastName = userInfoEntity.userName.split(" ");
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + fastName[0] + "!");
                } else {
                    tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + userInfoEntity.userName + "!");
                }
                tvNameFirst.setText(userInfoEntity.userName.substring(0,1).toUpperCase(Locale.ENGLISH));
            }
            if (!TextUtils.isEmpty(avatar)) {
                conAvatar.setVisibility(View.GONE);
                ivUserAvatar.setVisibility(View.VISIBLE);
                Glide.with(this).clear(ivUserAvatar);
                int round = (int) AndroidUtils.dip2px(mContext, 8);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(R.drawable.l_default_avatar)
                        .into(ivUserAvatar);
            } else {
                conAvatar.setVisibility(View.VISIBLE);
                ivUserAvatar.setVisibility(View.GONE);
            }
            phone = userInfoEntity.phone;
        }else{
            if(checkPhoneEntity != null){
                phone = checkPhoneEntity.phone;
                tvUserName.setText(getString(R.string.Login_SU_0005_4_D_1) + " " + checkPhoneEntity.firstName + "!");
                conAvatar.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(checkPhoneEntity.firstName)) {
                    tvNameFirst.setText(checkPhoneEntity.firstName.substring(0, 1).toUpperCase(Locale.ENGLISH));
                }
            }
        }
    }

    @Override
    public LoginContract.Presenter getPresenter() {
        return new LoginPresent();
    }

    @Override
    public void loginAccountSuccess(UserInfoEntity mUserInfoEntity) {
        if (mUserInfoEntity == null) return;
        this.userInfoEntity = mUserInfoEntity;
        if(checkPhoneEntity != null){
            if(checkPhoneEntity.isNewDevice()){
                showTrustDeviceDialog(mUserInfoEntity);
                return;
            }
        }

        RiskControlEntity riskControlEntity = mUserInfoEntity.riskControlInfo;
        lockAcc = userInfoEntity.lockAcc;
        String lockReActive = userInfoEntity.lockReActive;
        if(!"A".equals(lockAcc)){
            showAccountStatusDialog(mContext, lockAcc, new CommonNoticeDialog.CommonConfirmListener() {
                @Override
                public void commonConfirm(int type) {
                    if (riskControlEntity != null) {
                        if (!TextUtils.isEmpty(riskControlEntity.type)) {
                            if (riskControlEntity.isOtp()) {
                                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                                mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_ACTIVATE_USER);
                                mHashMaps.put(Constants.DATA_FIRST_TITLE, getString(R.string.sprint11_130));
                                mHashMaps.put(Constants.OTP_riskControlEntity, riskControlEntity);
                                ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            }
                        } else {
                            successLogin(mUserInfoEntity);
                        }
                    }else {
                        if ("E".equals(lockAcc)) {
                            mPresenter.activateUserStatus(callingCode, phone, lockAcc);
                        } else if ("C".equals(lockAcc)) {
                            HashMap<String, Object> mHashMap = new HashMap<>(16);
                            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_REG);
                            ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, RegisterActivity.class, mHashMap, RIGHT_IN);
                            dismissNoticeDialog();
                        } else if ("Y".equals(lockReActive)) {
                            //是否需要激活
                            mPresenter.activateUserStatus(callingCode, phone, lockAcc);
                        }
                    }
                }
            });
        }else {
            if (riskControlEntity != null) {
                if (!TextUtils.isEmpty(riskControlEntity.type)) {
                    if (riskControlEntity.isOtp()) {
                        HashMap<String, Object> mHashMaps = new HashMap<>(16);
                        if(!"A".equals(lockAcc)) {
                            mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_ACTIVATE_USER);
                            mHashMaps.put(Constants.DATA_FIRST_TITLE, getString(R.string.sprint11_130));
                        }else {
                            mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_FAST_LOGIN);
                        }
                        mHashMaps.put(Constants.LOGIN_TYPE, loginType);
                        mHashMaps.put(Constants.DATA_PHONE_NUMBER, phone);
                        mHashMaps.put(Constants.DATA_PHONE_PD, password);
                        mHashMaps.put(Constants.OTP_riskControlEntity, riskControlEntity);
                        ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                } else {
                    successLogin(mUserInfoEntity);
                }
            } else {
                successLogin(mUserInfoEntity);
            }
        }
    }

    private void showTrustDeviceDialog(UserInfoEntity userInfoEntity) {
        this.mUserInfoEntity = userInfoEntity;
        TrustDeviceDialog trustDeviceDialog = new TrustDeviceDialog(mContext);
        trustDeviceDialog.setTrustDeviceListener(new TrustDeviceDialog.TrustDeviceListener() {
            @Override
            public void trust() {
                showProgress(true);
                mPresenter.trustDevice();
            }

            @Override
            public void skip() {
                successLogin(mUserInfoEntity);
            }
        });
        trustDeviceDialog.setCancelable(false);
        trustDeviceDialog.setCanceledOnTouchOutside(false);
        trustDeviceDialog.show();
    }

    //登录成功
    public void successLogin(UserInfoEntity userInfoEntity) {
        userInfoEntity.phone = phone;
        AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, true);

        if (isFromSDK) {
            startToPayCheckoutActivity();
        } else {
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(Constants.MSGTYPE, msgType);
            maps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            ActivitySkipUtil.startAnotherActivity(LoginFastNewActivity.this, MainActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
        ProjectApp.removeAllTaskExcludeMainStack();
    }

    @Override
    public void loginAccountError(String errorCode, String errorMsg) {
        if("030208".equals(errorCode) || "030209".equals(errorCode) || "030210".equals(errorCode)){
            ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
            errorBottomDialog.setErrorMsg(mContext.getString(R.string.sprint18_2),errorMsg,ThemeSourceUtils.getSourceID(mContext,R.attr.icon_error_mobile_number));
            errorBottomDialog.showWithBottomAnim();
        }else {
            setErrorMsg(errorMsg);
        }
    }

    @Override
    public void checkPhoneSuccess(Void result) {
    }

    @Override
    public void checkPhoneError(String errorCode, String errorMsg) {
    }

    @Override
    public void activateUserStatusSuccess(Void result) {
        dismissNoticeDialog();
        showActivateSuccessDialog(mContext, lockAcc, new CommonNoticeDialog.CommonConfirmListener() {
            @Override
            public void commonConfirm(int type) {
                switch (lockAcc){
                    case "S":
                    case "E":
                        dismissNoticeDialog();
                        break;
                    case "D":
                    case "U":
                        contactHelp();
                        break;
                    default:break;
                }
            }
        });
    }

    @Override
    public void renewIdSuccess(ResultEntity resultEntity) {
        dismissNoticeDialog();
        if(resultEntity != null){
            String result = resultEntity.result;
            showActivateSuccessDialog(mContext, lockAcc, new CommonNoticeDialog.CommonConfirmListener() {
                @Override
                public void commonConfirm(int type) {
                    dismissNoticeDialog();
                }
            });
        }
    }

    @Override
    public void activateUserStatusFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void renewIdFail(String errorCode, String errorMsg) {
        dismissNoticeDialog();
        showActivateSuccessDialog(mContext, "W", new CommonNoticeDialog.CommonConfirmListener() {
            @Override
            public void commonConfirm(int type) {
                switch (lockAcc){
                    case "S":break;
                    case "D":break;
                    case "U":break;
                    case "E":break;
                    default:break;
                }
            }
        });
    }

    @Override
    public void trustDeviceFail(String errorCode, String errorMsg) {
        showProgress(false);
        showTipDialog(errorMsg);
    }

    @Override
    public void trustDeviceSuccess(Void result) {
        showProgress(false);
        successLogin(mUserInfoEntity);
    }
}
