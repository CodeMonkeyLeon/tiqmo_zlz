package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class AccountPayDetailActivity extends BaseCompatActivity {

    TransferDetailFragment transferDetailFragment;

    private CardNotifyEntity cardNotifyEntity;

    private String transferType;
    //12 退款   18分拆账单 21邀请码   19支付订单返现  15 EVoucher
    private int sceneType;

    private boolean fromHistory = false;
    private boolean isfromSDK = false;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_account_pay_detail;
    }


    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
        if (getIntent() != null && getIntent().getExtras() != null) {
            cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_MONEY_ENTITY);
            transferType = getIntent().getExtras().getString(Constants.sceneType);
            fromHistory = getIntent().getExtras().getBoolean(Constants.fromHistory);
            isfromSDK = getIntent().getExtras().getBoolean(Constants.IS_FROM_SDK);
            if (cardNotifyEntity != null) {
                sceneType = cardNotifyEntity.sceneType;
                cardNotifyEntity.transferType = transferType;
                cardNotifyEntity.fromHistory = fromHistory;
//                WindowManager wm = this.getWindow().getWindowManager();
//                DisplayMetrics metrics = new DisplayMetrics();
//                wm.getDefaultDisplay().getMetrics(metrics);
//                int height = AndroidUtils.dip2px(mContext, 600f);
//                if (metrics.heightPixels < height) {
//                    this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//                } else {
//                    this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
//                }
//                if (sceneType == 11 || sceneType == 15 || sceneType == 5 || sceneType == 12) {
//                    this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
//                }
                int minHeight = AndroidUtils.dip2px(mContext, 530f);
                int maxHeight = AndroidUtils.dip2px(mContext, 580f);
                FrameLayout frameLayout = findViewById(R.id.fl_transfer_detail);
//                frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(
//                        new ViewTreeObserver.OnGlobalLayoutListener() {
//
//                            @Override
//                            public void onGlobalLayout() {
//                                frameLayout.getViewTreeObserver()
//                                        .removeOnGlobalLayoutListener(this);
//                                int height = frameLayout.getHeight();
//                                if (sceneType == 11) {
//                                    getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, maxHeight);
//                                } else if (sceneType == 12) {
//                                    getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, minHeight);
//                                } else {
//                                    if (height < maxHeight) {
//                                        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//                                    } else {
//                                        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, maxHeight);
//                                    }
//                                }
//                            }
//                        });

                transferDetailFragment = TransferDetailFragment.getInstance(cardNotifyEntity, false);

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fl_transfer_detail, transferDetailFragment);
                ft.commit();
            }
        }

        if (isfromSDK) {
            countDown();
        }
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    // app2app支付成功后，等待3秒关闭当前界面，回到源app
    private void countDown() {
        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ProjectApp.removeApp2App();
                finish();
            }
        }.start();
    }
}
