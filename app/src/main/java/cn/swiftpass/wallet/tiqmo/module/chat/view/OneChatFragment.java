package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.Manifest;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gc.gcchat.GcChatSession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.chat.Contract.ChatContract;
import cn.swiftpass.wallet.tiqmo.module.chat.Presenter.ChatPresenter;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.ChatHistoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatContactDialog;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ContactUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class OneChatFragment extends BaseFragment<ChatContract.ChatPresenter> implements ChatContract.ChatView {
    @BindView(R.id.ll_content_empty)
    LinearLayout llContentEmpty;

    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;

    //通讯录联系人List
    public List<KycContactEntity> phoneList = new ArrayList<>();
    @BindView(R.id.rl_chat_history)
    RecyclerView rlChatHistory;
    @BindView(R.id.iv_no_chat_list)
    ImageView ivNoChatList;
    @BindView(R.id.tv_no_chat_list)
    TextView tvNoChatList;
    @BindView(R.id.tv_no_chat_list_two)
    TextView tvNoChatListTwo;
    @BindView(R.id.iv_no_chat_line)
    ImageView ivNoChatLine;
    @BindView(R.id.iv_add_chat_new)
    ImageView ivAddChatNew;
    private List<KycContactEntity> contactInviteDtos;

    private ChatContactDialog dialog;


    private final ArrayList<GcChatSession> all = new ArrayList<>();

    private final ArrayList<GcChatSession> individual = new ArrayList<>();
    private ChatHistoryAdapter adapter;
    private int type = 1;

    public static OneChatFragment newInstance() {
        OneChatFragment fragment = new OneChatFragment();
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_chat_history;
    }

    @Override
    protected void initView(View parentView) {
        adapter = new ChatHistoryAdapter(individual);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }

    @OnClick({R.id.iv_add_chat_new})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_add_chat_new:
                checkPermission();
                break;
        }
    }

    private void checkPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
            PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                        @Override
                        public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                            phoneList.clear();
                            phoneList.addAll(contactEntities);
                            mPresenter.contactInvite(phoneList);
                        }
                    });
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {
                        if (!firstRejectContacts) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_30));
                        } else {
                            firstRejectContacts = false;
                        }
                    }
                }
            }, Manifest.permission.READ_CONTACTS);
        } else {
            ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                @Override
                public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                    phoneList.clear();
                    phoneList.addAll(contactEntities);
                    mPresenter.contactInvite(phoneList);
                }
            });
        }
    }

    @Override
    public ChatContract.ChatPresenter getPresenter() {
        return new ChatPresenter();
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {
        if (contactInviteEntity != null) {
            contactInviteDtos = contactInviteEntity.getContactInviteDtos();

            for (int i = 0; i < contactInviteDtos.size(); i++) {
                KycContactEntity kycContactEntity = contactInviteDtos.get(i);
                if (kycContactEntity.getContactsName() != null) {
                    kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                }
            }

            Collections.sort(contactInviteDtos, new registerComparator());
            showContactDialog(contactInviteDtos);
        }
    }

    private void showContactDialog(List<KycContactEntity> phoneList) {
        dialog = ChatContactDialog.getInstance(mContext, type);
        dialog.setPhoneList(phoneList);


        dialog.setCreateChatRoom(new ChatContactDialog.CreateChatRoom() {
            @Override
            public void createChatRoom(String phone) {
                //创建个人聊天
                dialog.dismiss();
            }
        });


        dialog.setTvInviteClickListener(new ChatContactDialog.TvInviteClickListener() {
            @Override
            public void sendInvite(String phone) {

                String msg = mContext.getString(R.string.sprint20_67);
//                String[] s = userInfoEntity.getUserName().split(" ");
//                String body = msg.replace("XXX", s[0])
//                        .replace("YYY",etBillDesc.getText().toString().trim())
//                        .replace("ZZZ",etBillAmount.getText().toString().trim());
                AndroidUtils.sendSmsWithBody(mContext, BuildConfig.DefaultInternationalAreaCode + phone, msg);
//
            }
        });

        dialog.showWithBottomAnim();
    }

    /**
     * 根据是否注册来排序
     */
    public class registerComparator implements Comparator<KycContactEntity> {

        @Override
        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getUserStatus().equals(Constants.unregistered) || o2.getUserStatus().equals(Constants.registered)) {
                return 1;
            } else if (o1.getUserStatus().equals(Constants.registered) || o2.getUserStatus().equals(Constants.unregistered)) {
                return -1;
            }
            return 0;
        }
    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
