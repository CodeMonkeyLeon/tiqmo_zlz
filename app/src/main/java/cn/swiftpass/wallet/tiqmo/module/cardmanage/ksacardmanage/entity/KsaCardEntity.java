package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class KsaCardEntity extends BaseEntity {

    //	卡种枚举值 (Standard 标准卡 - Platinum 白金卡)
    public String cardProductType;
    //	卡状态枚举(INACTIVATE:未激活 - ACTIVATE:激活 - LOCKED:锁定(冻结)等)  SHIELD被屏蔽
    public String cardStatus;
    //	卡类型枚举值: VIRTUAL虚拟卡 - PHYSICAL物理卡
    public String cardType;
    //	卡标识（卡管理中相当于id级别的参数，将会贯穿整个卡管理，作用等价卡号）
    public String proxyCardNo;
    //	卡片颜色 : 标准卡的01表示浅色 ,02表示深色；白金卡的06浅色,07深色
    public String cardFaceId;

    public String cardNumber;
    // 脱敏后的卡号（后四位）****3212
    public String maskedCardNo;

    public String cardHolderName;

    public String expiryTime;

    public String cvv2;

    public String deliveryTime;
    //卡是否即将过期标识（只需要关注是否为true,当为true的时候优先展示改值，次级展示cardStatus）：true表示即将过期 false表示还没过期
    public boolean expiringSoonFlag;
    //卡即将到期倒计时提示信息（如果该字段存在并且有值则显示，否则不显示）
    public String countdownMessage;

    public boolean isShowCardDetail;

    public CardDetailEntity cardDetailEntity;

    public int getCardDrawable(){
        if(Constants.card_type_Standard.equals(cardProductType)){
//            if("01".equals(cardFaceId)){
//                return R.drawable.home_standard_card_green;
//            }else if("02".equals(cardFaceId)){
//                return R.drawable.home_standard_card_blue;
//            }
            return R.drawable.home_standard_card;
        }else if(Constants.card_type_Platinum.equals(cardProductType)) {
//            if("06".equals(cardFaceId)){
//                return R.drawable.home_card_platinum;
//            }else if("07".equals(cardFaceId)){
//                return R.drawable.home_card_platinum;
//            }
            return R.drawable.home_card_platinum;
        }
        return R.drawable.home_standard_card;
    }

    public boolean isBlocked(){
        return "BLOCKED".equals(cardStatus);
    }

    public boolean isLocked(){
        return "LOCKED".equals(cardStatus);
    }

    public boolean isNotActivate(){
        return "INACTIVE".equals(cardStatus);
    }

    public boolean isExpired(){
        return "EXPIRED".equals(cardStatus);
    }


    public boolean isActivate(){
        return "ACTIVE".equals(cardStatus);
    }
    public boolean isShield(){
        return "SHIELD".equals(cardStatus);
    }

    public boolean isVirtualCard(){
        return "VIRTUAL".equals(cardType);
    }
}


