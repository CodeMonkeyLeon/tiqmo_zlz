package cn.swiftpass.wallet.tiqmo.support.utils;

import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

public class FilePickerUtils {
    public static void startGeneralFilePicker(Activity activity, int requestCode) {
        String[] mimeTypes = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/pdf", "image/*"};
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        activity.startActivityForResult(intent, requestCode);
    }

    public static File getFileFromResult(Activity context, Intent data) {
        ClipData clipData = data.getClipData();
        if (clipData == null) {
            Uri uri = data.getData();
            return copyUriToInternal(context, uri);
        }
        if (clipData.getDescription() != null) {
            int mimeCount = clipData.getDescription().getMimeTypeCount();
            if (mimeCount > 0) {
                LogUtils.d("clipdata first mime - %s", clipData.getDescription().getMimeType(0));
            }
        }
        return null;
    }

    public static File copyUriToInternal(Context context, Uri uriIN) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uriIN, "r", null);
            InputStream inputStream = new FileInputStream(parcelFileDescriptor.getFileDescriptor());
            String fileName = "";
            String mimeType = "";
            try {
                ContentResolver cR = context.getContentResolver();
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                mimeType = mime.getExtensionFromMimeType(cR.getType(uriIN));
                fileName = PathUtil.getPath(context, uriIN);
                LogUtils.d("user picked file %s",fileName);
                if (fileName != null && fileName.contains("/")) {
                    String[] splitArray = fileName.split("/");
                    if (splitArray.length > 0) {
                        fileName = splitArray[splitArray.length - 1];
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (fileName == null || fileName.isEmpty()) {
                // to be tested
                List<String> uriPathSegments = uriIN.getPathSegments();
                String inputFileName = uriPathSegments.get(uriPathSegments.size() - 1);
                if (inputFileName.endsWith(".pdf")) {
                    fileName = System.currentTimeMillis() + "." + (mimeType == null ? "pdf" : mimeType);
                }
                else {
                    fileName = System.currentTimeMillis() + "." + (mimeType == null ? "jpg" : mimeType);
                }
            }

            File file = new File(context.getCacheDir(), fileName);
            FileOutputStream fis = new FileOutputStream(file);

            byte[] b = new byte[1024];
            int len;
            while ((len = inputStream.read(b, 0, 1024)) > 0) {
                fis.write(b, 0, len);
            }

            return file;
        } catch (Exception e) {
        }
        return null;
    }
}
