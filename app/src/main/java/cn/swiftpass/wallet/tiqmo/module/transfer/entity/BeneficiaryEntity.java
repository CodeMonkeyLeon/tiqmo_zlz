package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class BeneficiaryEntity extends BaseEntity {

    public String orgNo;
    public String beneficiaryIbanNo;
    public String beneficiaryName;
    public String bankName;
    public String id;
    public String bankLogo;
    public String userId;
    public String chooseRelationship;
    public String transTimeType;
    public String transTimeDescOne;
    public String transTimeDescTwo;
    public String remark;
    public String orderAmount;
    public String transferPurpose = "";
    public String transferPurposeDesc = "";

    public RiskControlEntity riskControlInfo;
}
