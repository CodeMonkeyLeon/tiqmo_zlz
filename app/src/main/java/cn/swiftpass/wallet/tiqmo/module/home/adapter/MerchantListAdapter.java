package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class MerchantListAdapter extends BaseRecyclerAdapter<String> {

    private int itemWidth, itemHeight;

    public MerchantListAdapter(Context mContext, @Nullable List<String> data) {
        super(R.layout.merchant_item, data);
        itemWidth = AndroidUtils.getScreenWidth(mContext) / 3;
        itemHeight = AndroidUtils.dip2px(mContext, 52);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, String merchantHeadPicUrl, int position) {
        RoundedImageView market = baseViewHolder.getView(R.id.iv_merchant_item);
//        ConstraintLayout llMerchant = baseViewHolder.getView(R.id.ll_merchant);
//        llMerchant.setLayoutParams(new ConstraintLayout.LayoutParams(itemWidth, itemHeight));
//        TextView tv_merchant_name = baseViewHolder.getView(R.id.tv_merchant_name);

        if (!TextUtils.isEmpty(merchantHeadPicUrl)) {
            Glide.with(mContext).clear(market);
            Glide.with(mContext)
                    .load(merchantHeadPicUrl)
                    .dontAnimate()
                    .skipMemoryCache(true)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(market);
        }

//        tv_merchant_name.setText(item.merchantName);
    }

    @Override
    public OnItemClickListener getOnItemClickListener() {
        return super.getOnItemClickListener();
    }
}
