package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 今日支出信息
 */
public class TodaySpendingEntity extends BaseEntity {
    /**
     * \"todaySpendingInfo\":{\"valuePercent\":\"0.00%\",\"todaySpendingMoney\":2000.000000,\"todayDiviLastPercent\":\"100%\",\"directFlag\":\"0\"}
     */

    //箭头标识:1表示上升箭头(当一周前今日支出为0时，也是这个值);0表示无箭头;-1表示下降箭头
    public String directFlag;
    //百分比(差值绝对值/上周今日,分母如果为0，直接为100%)
    public String valuePercent;
    //今日支出金额
    public String todaySpendingMoney;
    //支出差值在上周今日占比(百分比(最大值100%，超过的也是100%,分母为0也是100%))
    public String todayDiviLastPercent;

    public int getDirectFlagRes(String directFlag) {
        if ("1".equals(directFlag)) {
            return R.drawable.d_arrow_top_spending;
        } else if ("-1".equals(directFlag)) {
            return R.drawable.d_arrow_down_spending;
        }
        return 0;
    }
}
