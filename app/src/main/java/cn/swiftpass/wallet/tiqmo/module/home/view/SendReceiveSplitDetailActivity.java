package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SendReceiveSplitDetailAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.SplitBillContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.SendReceiveDetailPresenter;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CircularProgressView1;
import cn.swiftpass.wallet.tiqmo.widget.ColorItem;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.NoBalanceDialog;

public class SendReceiveSplitDetailActivity extends BaseCompatActivity<SplitBillContract.SendReceiveDetailPresenter> implements SplitBillContract.SendReceiveDetailView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.split_title)
    TextView splitTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_circle_view)
    CircularProgressView1 ivCircleView;
    @BindView(R.id.tv_paid_money)
    TextView tvPaidMoney;
    @BindView(R.id.tv_unpaid_money)
    TextView tvUnpaidMoney;
    @BindView(R.id.tv_refuse_money)
    TextView tvRefuseMoney;
    @BindView(R.id.ll_unpaid)
    LinearLayout llUnpaid;
    @BindView(R.id.ll_reject)
    LinearLayout llReject;
    @BindView(R.id.tv_split_count)
    TextView tvSplitCount;
    @BindView(R.id.rl_menber)
    RecyclerView rlMenber;
    @BindView(R.id.tv_remind_unpaid_people)
    TextView tvRemindUnpaidPeople;
    @BindView(R.id.tv_paid)
    TextView tvPaid;
    @BindView(R.id.tv_reject)
    TextView tvReject;
    @BindView(R.id.paid_view)
    View paidView;
    @BindView(R.id.unpaid_view)
    View unPaidView;
    @BindView(R.id.reject_view)
    View rejectView;
    @BindView(R.id.tv_paid_percentage)
    TextView tvpaidPercentage;

    private SendReceiveSplitDetailEntity detailEntity;
    private SendReceiveSplitDetailAdapter adapter;
    private boolean isReceive;
    private String sendType;

    private RiskControlEntity riskControlEntity;
    private BottomDialog payDialog;
    private NoBalanceDialog noBalanceDialog;
    private BottomDialog bottomDialog;

    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_send_receive_split_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        //设置Acitivity不占满屏幕
//        windowColor();
        if (getIntent() != null && getIntent().getExtras() != null) {
            detailEntity = (SendReceiveSplitDetailEntity) getIntent().getExtras().get(Constants.SEND_RECEIVE_SPLIT_DETAIL);
            isReceive = getIntent().getExtras().getBoolean("isReceive",false);

        }
        tvTitle.setText(R.string.request_transfer_10);
        initData();
        initView();
    }

    private void initData(){
        if (detailEntity != null) {
            tvAmount.setText(Spans.builder()
                    .text(AndroidUtils.getTransferMoney(detailEntity.getTotalAmount()))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_title_text_color)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(28)
                    .text(" " + LocaleUtils.getCurrencyCode(detailEntity.getCurrency()))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_60white_603a3b44)))
                    .size(20)
                    .build());

            splitTitle.setText(detailEntity.getSplitTitel());
            tvTime.setText(detailEntity.getSplitTime());


            tvPaidMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferStringMoney(detailEntity.getPaidAmount()))
                    .color(mContext.getColor(R.color.green2))
                    .size(14)
                    .text(" " + LocaleUtils.getCurrencyCode(detailEntity.getCurrency()))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_time_color)))
                    .size(12)
                    .build());

            tvpaidPercentage.setText(Spans.builder()
                    .text(detailEntity.getPaidProportion())
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_title_text_color)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(22)
                    .text("%")
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_title_text_color)))
                    .size(20)
                    .build());
            tvUnpaidMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferStringMoney(detailEntity.getUnpaidAmount()))
                    .color(mContext.getColor(R.color.color_f1a13c))
                    .size(14)
                    .text(" " + LocaleUtils.getCurrencyCode(detailEntity.getCurrency()))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_time_color)))
                    .size(12)
                    .build());

            tvRefuseMoney.setText(Spans.builder()
                    .text(AndroidUtils.getTransferStringMoney(detailEntity.getRejectedAmount()))
                    .color(mContext.getColor(R.color.red2))
                    .size(14)
                    .text(" " + LocaleUtils.getCurrencyCode(detailEntity.getCurrency()))
                    .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_card_time_color)))
                    .size(12)
                    .build());

            int paidProportion = Integer.parseInt(detailEntity.getPaidProportion());
            int unPaidProportion = Integer.parseInt(detailEntity.getUnpaidProportion());
            int rejectProportion = Integer.parseInt(detailEntity.getRejectedProportion());

            if (unPaidProportion == 0) {
                llUnpaid.setVisibility(View.GONE);
            }
            if (rejectProportion == 0) {
                llReject.setVisibility(View.GONE);
            }

            if (isReceive) {
                tvRemindUnpaidPeople.setText(Spans.builder()
                        .text(mContext.getString(R.string.scan_pay_7))
                        .color(mContext.getColor(R.color.white))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(" "+AndroidUtils.getTransferStringMoney(detailEntity.getMyReceiveAmount()))
                        .color(mContext.getColor(R.color.white))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(" "+LocaleUtils.getCurrencyCode(detailEntity.getCurrency()))
                        .color(mContext.getColor(R.color.white))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());
                if ("C".equals(detailEntity.getPaymentStatus())) {
                    //未支付保留按钮
                    tvRemindUnpaidPeople.setEnabled(true);
                    tvReject.setVisibility(View.VISIBLE);
                }else {
                    //已支付或拒绝,隐藏按钮
                    tvReject.setVisibility(View.GONE);
                    tvRemindUnpaidPeople.setVisibility(View.GONE);
                }
            }else {
                if ("able".equals(detailEntity.getRemind())) {
                    tvRemindUnpaidPeople.setEnabled(true);
                }
                //所有人已支付或拒绝
                if (unPaidProportion == 0) {
                    tvRemindUnpaidPeople.setVisibility(View.GONE);
                }
            }

            tvSplitCount.setText(detailEntity.getPayerCount());

        }
    }

    private void initView() {
        int paid = Integer.parseInt(detailEntity.getPaidProportion());
        int unPaid = Integer.parseInt(detailEntity.getUnpaidProportion());
        int rejected = Integer.parseInt(detailEntity.getRejectedProportion());
        setActivityPie(ivCircleView,paid,unPaid,rejected);

        rlMenber.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new SendReceiveSplitDetailAdapter(detailEntity.getSplitPayerDtos());
        adapter.bindToRecyclerView(rlMenber);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getSplitDetail(detailEntity.getOriginalOrderNo(),detailEntity.getOrderRelevancy(),Constants.receive);
        int paid = Integer.parseInt(detailEntity.getPaidProportion());
        int unPaid = Integer.parseInt(detailEntity.getUnpaidProportion());
        int rejected = Integer.parseInt(detailEntity.getRejectedProportion());
        setActivityPie(ivCircleView,paid,unPaid,rejected);
    }


    private void setActivityPie(CircularProgressView1 progress, int paid, int unPaid, int rejected) {
        try {
            progress.setVisibility(View.VISIBLE);
            List<ColorItem> items = new ArrayList<>();
            progress.setBackColor(R.color.transparent);

            if (paid != 0) {
                items.add(new ColorItem(paid, mContext.getColor(R.color.green2), "0"));
            }
            if (unPaid != 0) {
                items.add(new ColorItem(unPaid, mContext.getColor(R.color.color_f1a13c), "0"));
            }
            if (rejected != 0) {
                items.add(new ColorItem(rejected, mContext.getColor(R.color.red2), "0"));
            }

            if (paid == 100 || unPaid == 100 || rejected == 100) {
                progress.setSpaceProgressAngle(0);
            }else {
                progress.setSpaceProgressAngle(360*3/100);
            }
            progress.setItems(items);

        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    @OnClick({R.id.tv_remind_unpaid_people,R.id.tv_reject,R.id.iv_back})
    public void onClickView(View view){
        switch (view.getId()){
            case R.id.tv_remind_unpaid_people:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (isReceive) {
                    mPresenter.ReceiveSplitBillDetail(detailEntity.getOrderNo());
                }else {
                    mPresenter.sendSplitsRemind(detailEntity.getOriginalOrderNo(),detailEntity.getOrderRelevancy());
                }
                break;

            case R.id.tv_reject:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                NotifyEntity notifyEntity = new NotifyEntity();
                notifyEntity.msgSubject = "TRANSFER";
                sendType = "REJECT";
                notifyEntity.msgResult = "REJECT";
                notifyEntity.orderNo = detailEntity.getOrderNo();
                List<NotifyEntity> receivers = new ArrayList<>();
                receivers.add(notifyEntity);
                if (mPresenter != null) {
                    mPresenter.sendNotify(receivers);
                }
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    public SplitBillContract.SendReceiveDetailPresenter getPresenter() {
        return new SendReceiveDetailPresenter();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,Constants.TYPE_TRANSFER_RC,transferEntity,false);
    }

    @Override
    public void ReceiveSplitBillDetailSuccess(RequestCenterEntity requestCenterEntity) {
        if (requestCenterEntity != null) {
            showPayDetailDialog(requestCenterEntity);
        }
    }

    @Override
    public void getSplitDetailSuccess(SendReceiveSplitDetailEntity detailEntity) {
        if (detailEntity != null) {
            this.detailEntity = detailEntity;
            initData();
            initView();
        }
    }

    @Override
    public void sendSplitsRemindSuccess() {
        tvRemindUnpaidPeople.setEnabled(false);
        showTipDialog(mContext.getString(R.string.sprint17_41));
    }

    @Override
    public void showError(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {
        if ("REJECT".equals(sendType)) {
            showTipDialog(errorMsg);
        }else {
            if ("010107".equals(errorCode)) {
                //余额不足
                showNoBalanceDialog();
                if (payDialog != null) {
                    payDialog.dismiss();
                }
            } else if ("030204".equals(errorCode) || "030207".equals(errorCode)) {
                if (payDialog != null) {
                    payDialog.dismiss();
                }
                showExcessBeneficiaryDialog(errorMsg);
            } else {
                showTipDialog(errorMsg);
            }
        }
    }

    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
//                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity payBillOrderInfoEntity) {
        if(payDialog != null){
            payDialog.dismiss();

        }
        if ("REJECT".equals(sendType)) {
            userInfoEntity.requestCounts = userInfoEntity.requestCounts - 1;
//            SpUtils.getInstance().setReqCounts(userInfoEntity.requestCounts);
            AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
            mPresenter.getSplitDetail(detailEntity.getOriginalOrderNo(),detailEntity.getOrderRelevancy(),Constants.receive);

        }else {
        if (payBillOrderInfoEntity == null) return;
        riskControlEntity = payBillOrderInfoEntity.riskControlInfo;
        if (riskControlEntity != null) {
            if (riskControlEntity.isOtp()) {
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                setTransferMap(mHashMaps);
                ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                jumpToPwd(false);
            } else if (riskControlEntity.isNeedIvr()) {
                jumpToPwd(true);
            }else {
                mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT,detailEntity.getOrderNo(),"BAL",detailEntity.getMyReceiveAmount(),
                        "1.000000",UserInfoManager.getInstance().getCurrencyCode(),null,null);
            }

        }
        }
    }

    private void showNoBalanceDialog(){
        noBalanceDialog = new NoBalanceDialog(mContext);
        noBalanceDialog.showWithBottomAnim();
    }

    private void showPayDetailDialog(RequestCenterEntity requestCenterEntity) {
        payDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirm_transaction, null);
        TextView tvConfirm = contentView.findViewById(R.id.tv_confirm);
        TextView tvCancel = contentView.findViewById(R.id.tv_cancel);
        TextView tvMoney = contentView.findViewById(R.id.tv_money);
        TextView tvNote = contentView.findViewById(R.id.tv_note);
        TextView tvName = contentView.findViewById(R.id.tv_name);
        TextView tvPhone = contentView.findViewById(R.id.tv_phone);
        LottieAnimationView ivSend = contentView.findViewById(R.id.iv_send);
        TextView tvError = contentView.findViewById(R.id.tv_error);
        ConstraintLayout con_note = contentView.findViewById(R.id.con_note);
        RoundedImageView ivFromAvatar = contentView.findViewById(R.id.iv_from_avatar);
        RoundedImageView ivToAvatar = contentView.findViewById(R.id.iv_to_avatar);
        LocaleUtils.viewRotationY(mContext, ivSend);

        String requestIcon = requestCenterEntity.requestIcon;
        String receiveIcon = requestCenterEntity.receiveIcon;
        String requestPhone = requestCenterEntity.requestPhone;
        String receivePhone = requestCenterEntity.receivePhone;
        String requestUserName = requestCenterEntity.requestUserName;
        String receiveUserName = requestCenterEntity.receiveUserName;
        String requestSex = requestCenterEntity.requestSex;
        String receiveSex = requestCenterEntity.receiveSex;
        String transferAmount = requestCenterEntity.transferAmount;
        String note = requestCenterEntity.transferRemark;
        String currencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferAmount = AndroidUtils.getTransferMoney(transferAmount);
        if (!TextUtils.isEmpty(transferAmount)) {
            tvMoney.setText(Spans.builder()
                    .text(transferAmount + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode(currencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15))).size(10)
                    .build());
        }

        tvName.setText(AndroidUtils.getContactName(requestUserName));
        tvPhone.setText(AndroidUtils.getPhone(requestPhone));
        if (!TextUtils.isEmpty(note)) {
            con_note.setVisibility(View.VISIBLE);
            tvNote.setText(note);
        } else {
            con_note.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(receiveIcon)) {
            Glide.with(mContext).clear(ivFromAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(receiveIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, receiveSex))
                    .into(ivFromAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, receiveSex)).into(ivFromAvatar);
        }

        if (!TextUtils.isEmpty(requestIcon)) {
            Glide.with(mContext).clear(ivToAvatar);
            int round = (int) AndroidUtils.dip2px(mContext, 8);
            Glide.with(mContext)
                    .load(requestIcon)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getDefAvatar(mContext, requestSex))
                    .into(ivToAvatar);
        } else {
            Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, requestSex)).into(ivToAvatar);
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendType = "TRANSFER";
                NotifyEntity notifyEntity = new NotifyEntity();
                notifyEntity.msgSubject = "TRANSFER";
                notifyEntity.msgResult = "CONFIRM";
                notifyEntity.orderNo = requestCenterEntity.orderNo;
                List<NotifyEntity> receivers = new ArrayList<>();
                receivers.add(notifyEntity);
                mPresenter.sendNotify(receivers);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payDialog != null) {
                    payDialog.dismiss();
                }
            }
        });

        payDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        payDialog.setContentView(contentView);
        Window dialogWindow = payDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        payDialog.showWithBottomAnim();
    }
    private void setTransferMap(HashMap<String, Object> mHashMap){
        TransferEntity transferEntity = new TransferEntity();
        transferEntity.orderNo = detailEntity.getOrderNo();
        transferEntity.orderAmount = detailEntity.getMyReceiveAmount();
        transferEntity.orderCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
        transferEntity.remark = "vh";
        transferEntity.payMethod = "BAL";
        transferEntity.exchangeRate = "1.000000";
        mHashMap.put(Constants.TRANSFER_ENTITY, transferEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_TRANSFER_RC);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_TRANSFER_REQUEST);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.splitbill_13));

    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }
}
