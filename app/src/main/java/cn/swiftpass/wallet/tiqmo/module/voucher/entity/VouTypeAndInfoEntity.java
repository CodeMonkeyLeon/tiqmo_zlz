package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class VouTypeAndInfoEntity extends BaseEntity {

    public ArrayList<VouTypeAndInfo> voucherTypeAndInfo = new ArrayList<>();

    public static class VouTypeAndInfo extends BaseEntity {
        //代金券类型是否可输入金额 N： 不可输入金额 Y： 可输入金额
        public String voucherType;
        //输入金额时下单用这个字段
        public String voucherCode;
        public String voucherTypeName;
        public String minInputAmount;
        public String maxInputAmount;
        public String voucherTypeLightLogoUrl;
        public String voucherTypeDarkLogoUrl;
        public ArrayList<VouInfo> voucherInfos;
    }

    public static class VouInfo extends BaseEntity {
        public String voucherCode;
        public String voucherName;
        public String voucherAmount;
        public String voucherCurrency;
        public String voucherTypeLightIconUrl;
        public String voucherTypeDarkIconLogoUrl;
    }
}
