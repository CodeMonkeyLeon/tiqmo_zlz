package cn.swiftpass.wallet.tiqmo.sdk.listener;

public interface ResultCallback<T> {
    void onResult(T response);

    void onFailure(String errorCode, String errorMsg);
}
