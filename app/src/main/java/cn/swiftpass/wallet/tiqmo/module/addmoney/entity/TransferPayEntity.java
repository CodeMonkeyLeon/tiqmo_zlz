package cn.swiftpass.wallet.tiqmo.module.addmoney.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class TransferPayEntity extends BaseEntity {

    private String partnerNo;
    private String partnerUserId;
    private String openId;
    private String partnerOrderNo;
    private String orderNo;
    private String orderType;
    private String orderStatus;
    private String orderAmount;
    private String orderCurrencyCode;
    private String authPage;
    private String paymentMethodDesc;
    private String startTime;
    private String endTime;
    private String remark;
    //hyper pay 通道 checkoutId
    public String checkoutId;
    //hyper pay 通道 TokenId
    public String paymentId;

    public String couponType;

    public String transFees;
    public String discountTransFees;
    public String vat;
    public String discountVat;
    public String couponAmount = "0";
    public String couponCurrencyCode;
    public String totalAmount;

    public RiskControlEntity riskControlInfo;

    public String getTransFees() {
        return AndroidUtils.getTransferMoney(transFees);
    }

    public String getTotalAmount() {
        return AndroidUtils.getTransferMoney(totalAmount);
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setTransFees(String transFees) {
        this.transFees = transFees;
    }

    public String getDiscountTransFees() {
        return AndroidUtils.getTransferMoney(discountTransFees);
    }

    public void setDiscountTransFees(String discountTransFees) {
        this.discountTransFees = discountTransFees;
    }

    public String getVat() {
        return AndroidUtils.getTransferMoney(vat);
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getDiscountVat() {
        return AndroidUtils.getTransferMoney(discountVat);
    }

    public void setDiscountVat(String discountVat) {
        this.discountVat = discountVat;
    }

    public String getPartnerNo() {
        return partnerNo;
    }

    public void setPartnerNo(String partnerNo) {
        this.partnerNo = partnerNo;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPartnerOrderNo() {
        return partnerOrderNo;
    }

    public void setPartnerOrderNo(String partnerOrderNo) {
        this.partnerOrderNo = partnerOrderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderAmount() {
        return AndroidUtils.getTransferMoney(orderAmount);
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderCurrencyCode() {
        return orderCurrencyCode;
    }

    public void setOrderCurrencyCode(String orderCurrencyCode) {
        this.orderCurrencyCode = orderCurrencyCode;
    }

    public String getAuthPage() {
        return authPage;
    }

    public void setAuthPage(String authPage) {
        this.authPage = authPage;
    }

    public String getPaymentMethodDesc() {
        return paymentMethodDesc;
    }

    public void setPaymentMethodDesc(String paymentMethodDesc) {
        this.paymentMethodDesc = paymentMethodDesc;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
