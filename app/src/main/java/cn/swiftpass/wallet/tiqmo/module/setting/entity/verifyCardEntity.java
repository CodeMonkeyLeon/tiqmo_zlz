package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * Created by aijingya on 2020/7/15.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.entity
 * @Description:
 * @date 2020/7/15.16:53.
 */
public class verifyCardEntity extends BaseEntity {
    private String cardFaceBack;
    private String openId;
    private String cardType;
    private String cardCategory;
    private String bankName;
    public String cardOrganization;
    private String partnerNo;
    private String cardTypeDesc;
    private String cardFacebar;
    private String cardFace;
    private String cardFaceMini;
    private String partnerUserId;
    //脱敏显示的卡号
    public String cardNo;

    public String getCardFaceBack() {
        return cardFaceBack;
    }

    public void setCardFaceBack(String cardFaceBack) {
        this.cardFaceBack = cardFaceBack;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardCategory() {
        return cardCategory;
    }

    public void setCardCategory(String cardCategory) {
        this.cardCategory = cardCategory;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCardOrganization() {
        return cardOrganization;
    }

    public void setCardOrganization(String cardOrganization) {
        this.cardOrganization = cardOrganization;
    }

    public String getPartnerNo() {
        return partnerNo;
    }

    public void setPartnerNo(String partnerNo) {
        this.partnerNo = partnerNo;
    }

    public String getCardTypeDesc() {
        return cardTypeDesc;
    }

    public void setCardTypeDesc(String cardTypeDesc) {
        this.cardTypeDesc = cardTypeDesc;
    }

    public String getCardFacebar() {
        return cardFacebar;
    }

    public void setCardFacebar(String cardFacebar) {
        this.cardFacebar = cardFacebar;
    }

    public String getCardFace() {
        return cardFace;
    }

    public void setCardFace(String cardFace) {
        this.cardFace = cardFace;
    }

    public String getCardFaceMini() {
        return cardFaceMini;
    }

    public void setCardFaceMini(String cardFaceMini) {
        this.cardFaceMini = cardFaceMini;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }
}
