package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class ConfirmPayDialog extends BottomDialog{

    private Context mContext;

    private PayMoneyListener payMoneyListener;

    private ChangePhoneFeeEntity mChangePhoneFeeEntity;
    private TransferLimitEntity mTransferLimitEntity;

    TextView tvAddMoney;
    TextView tvBalance;
    TextView tvCurrency;
    LinearLayout llError;
    TextView tvError;
    UserInfoEntity userInfoEntity;
    String totalAmount;
    public interface PayMoneyListener {
        void payMoney(ChangePhoneFeeEntity mChangePhoneFeeEntity);

        void addMoney();
    }

    public void setPayMoneyListener(final PayMoneyListener payMoneyListener) {
        this.payMoneyListener = payMoneyListener;
    }

    public ConfirmPayDialog(Context context, ChangePhoneFeeEntity changePhoneFeeEntity, TransferLimitEntity transferLimitEntity) {
        super(context);
        this.mContext = context;
        this.mChangePhoneFeeEntity = changePhoneFeeEntity;
        mTransferLimitEntity = transferLimitEntity;
        initViews(context);
    }

    public void updateBalance(){
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity != null) {
            String balance = userInfoEntity.getBalance();

            if(tvBalance != null) {
                tvBalance.setText(balance);
            }
            if(tvCurrency != null) {
                tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
            }
            checkMoney(userInfoEntity,totalAmount);
        }
    }

    private boolean checkMoney(UserInfoEntity userInfoEntity,String totalAmount){
        String balanceNumber = userInfoEntity.getBalanceNumber();
        if (!TextUtils.isEmpty(balanceNumber)) {
            try {
                balanceNumber = balanceNumber.replace(",", "");
                double valiableMoney = Double.parseDouble(balanceNumber);
                double money = AndroidUtils.getTransferMoneyNumber(totalAmount);
                double monthLimitMoney = AndroidUtils.getTransferMoneyNumber(mTransferLimitEntity.monthLimitAmt);
                if (valiableMoney == 0) {
                    tvAddMoney.setEnabled(true);
                }
                if (money > monthLimitMoney) {
                    llError.setVisibility(View.VISIBLE);
                    tvError.setText(mContext.getString(R.string.wtw_25_3));
                    return false;
                }else if (money <= valiableMoney) {
                    tvAddMoney.setEnabled(false);
                    llError.setVisibility(View.GONE);
                    if (money > 0) {
                        return true;
                    }
                } else {
                    tvAddMoney.setEnabled(true);
                    llError.setVisibility(View.VISIBLE);
                    tvError.setText(mContext.getString(R.string.wtw_25));
                    return false;
                }
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        return false;
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirm_pay, null);
        ImageView ivBack = mView.findViewById(R.id.iv_back);
        tvBalance = mView.findViewById(R.id.tv_balance);
        tvCurrency = mView.findViewById(R.id.tv_currency);
        tvAddMoney = mView.findViewById(R.id.tv_add_money);
        llError = mView.findViewById(R.id.ll_error);
        tvError = mView.findViewById(R.id.tv_error);
        TextView tvTransFee = mView.findViewById(R.id.tv_change_fee);
        TextView tvVat = mView.findViewById(R.id.tv_vat);
        TextView tvWrongVatMoney = mView.findViewById(R.id.tv_wrong_vat_money);
        TextView tvWrongFees = mView.findViewById(R.id.tv_wrong_fees);
        TextView tvCancel = mView.findViewById(R.id.tv_cancel);
        TextView tvConfirm = mView.findViewById(R.id.tv_confirm);
        TextView tvTotalAmount = mView.findViewById(R.id.tv_total_amount);
        ConstraintLayout llTransferVat = mView.findViewById(R.id.ll_vat);
        ConstraintLayout llTransferFee = mView.findViewById(R.id.con_change_fee);

        LocaleUtils.viewRotationY(mContext,ivBack);

        tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        if(mChangePhoneFeeEntity != null){
            totalAmount = mChangePhoneFeeEntity.totalAmount;
            String vatAmount = mChangePhoneFeeEntity.discountVat;
            String wrongVat = mChangePhoneFeeEntity.vat;
            String wrongTransferFee = mChangePhoneFeeEntity.transFees;
            String transFees = mChangePhoneFeeEntity.discountTransFees;
            String orderCurrencyCode = LocaleUtils.getCurrencyCode("");
            if(!TextUtils.isEmpty(totalAmount)) {
                tvTotalAmount.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(totalAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                        .build());
            }
            if (!TextUtils.isEmpty(wrongVat)) {
                ////如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                llTransferVat.setVisibility(View.VISIBLE);
                tvWrongVatMoney.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(wrongVat) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());

                if (!TextUtils.isEmpty(vatAmount)) {
                    tvVat.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(vatAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
            }else {
                //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                if (!TextUtils.isEmpty(vatAmount) && BigDecimalFormatUtils.compareBig(vatAmount, "0")) {
                    tvWrongVatMoney.setVisibility(View.GONE);
                    tvVat.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(vatAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                } else {
                    llTransferVat.setVisibility(View.GONE);
                }
            }

            if (!TextUtils.isEmpty(wrongTransferFee)) {
                //如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                llTransferFee.setVisibility(View.VISIBLE);
                tvWrongFees.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(wrongTransferFee) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .build());

                if(!TextUtils.isEmpty(transFees)) {
                    tvTransFee.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(transFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }
            }else{
                //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                if (!TextUtils.isEmpty(transFees) && BigDecimalFormatUtils.compareBig(transFees,"0")) {
                    tvWrongFees.setVisibility(View.GONE);
                    tvTransFee.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(transFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }else{
                    llTransferFee.setVisibility(View.GONE);
                }
            }
            updateBalance();
            tvConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(checkMoney(userInfoEntity,totalAmount)) {
                        if (payMoneyListener != null) {
                            payMoneyListener.payMoney(mChangePhoneFeeEntity);
                        }
                    }
                }
            });
            tvAddMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (payMoneyListener != null) {
                        payMoneyListener.addMoney();
                    }
                }
            });
        }


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
