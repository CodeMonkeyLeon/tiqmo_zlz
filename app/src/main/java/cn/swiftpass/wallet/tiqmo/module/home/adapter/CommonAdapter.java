package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用的List数据列表适配器的基类，带有缓存
 */
public abstract class CommonAdapter extends BaseAdapter {
    /**
     * 当前适配器中的数据列表
     */
    public List list;

    public List<?> getDatas() {
        return list;
    }

    /**
     * 构造函数
     */
    public CommonAdapter() {
    }

    /**
     * 构造函数
     *
     * @param list 初始化适配器中的数据
     */
    public CommonAdapter(List<?> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list == null ? null : list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    /**
     * 往当前ListView中增加数据列表
     *
     * @param datas 增加的数据列表
     */
    public void addData(List<?> datas) {
        if (this.list == null) this.list = new ArrayList();
        this.list.addAll(datas);
        notifyDataSetChanged();
    }

    public void clearData() {
        if (list != null && list.size() > 0) {
            list.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * 往当前ListView中增加数据列表
     *
     * @param datas    增加的数据列表
     * @param position 加入的位置
     */
    public void addData(List<?> datas, int position) {
        if (this.list == null) this.list = new ArrayList();
        if (this.list.size() + 1 <= position) {
            this.list.addAll(this.list.size(), datas);
        } else {
            this.list.addAll(position, datas);
        }

        notifyDataSetChanged();
    }

    /**
     * 更改当前ListView中的数据列表
     *
     * @param datas 新的数据列表
     */
    public void changeData(List<?> datas) {
        this.list = datas;
        notifyDataSetChanged();
    }

    public void removeDatas(List<?> datas, int position) {
        if (this.list != null && this.list.size() > 0) {
            for (Object object : datas) {
                this.list.remove(object);
            }
            notifyDataSetChanged();
        }
    }

    protected String getString(Context context, int resId) {
        return context.getResources().getString(resId);
    }

    protected int getColor(Context context, int resId) {
        return context.getResources().getColor(resId);
    }

    public int getCompatColor(Context context, int color) {
        return ContextCompat.getColor(context, color);
    }
}