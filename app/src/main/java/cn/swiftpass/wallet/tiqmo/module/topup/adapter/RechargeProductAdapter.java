package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.graphics.Typeface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zrq.spanbuilder.Spans;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.ChooseProductEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class RechargeProductAdapter extends BaseRecyclerAdapter<ChooseProductEntity> {

    public RechargeProductAdapter(@Nullable List<ChooseProductEntity> data) {
        super(R.layout.item_recharge_product,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, ChooseProductEntity chooseProductEntity, int position) {
        String description = chooseProductEntity.description;
        String amount = chooseProductEntity.amount;
        String destinationAmount = chooseProductEntity.destinationAmount;
        String destinationCurrencyCode = chooseProductEntity.destinationCurrencyCode;
//        baseViewHolder.setText(R.id.tv_product_name,destinationAmount+" "+destinationCurrencyCode+" "+description);
        baseViewHolder.setText(R.id.tv_product_name,description);
        if(destinationAmount.length()>4) {
            baseViewHolder.setText(R.id.tv_product_money, Spans.builder()
                    .text(destinationAmount + "\n").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(12)
                    .text(destinationCurrencyCode).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(10)
                    .build());
        }else{
            baseViewHolder.setText(R.id.tv_product_money, Spans.builder()
                    .text(destinationAmount + "\n").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(16)
                    .text(destinationCurrencyCode).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                    .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf")).size(12)
                    .build());
        }
    }
}
