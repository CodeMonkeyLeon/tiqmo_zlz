package cn.swiftpass.wallet.tiqmo.module.voucher;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.adapter.VoucherBrandSearchAdapter;
import cn.swiftpass.wallet.tiqmo.module.voucher.adapter.VoucherMarketAdapter;
import cn.swiftpass.wallet.tiqmo.module.voucher.adapter.VoucherRecentAdapter;
import cn.swiftpass.wallet.tiqmo.module.voucher.dialog.EVoucherSelectCountryDialogFrag;
import cn.swiftpass.wallet.tiqmo.module.voucher.dialog.OrderRepeatNoticeDia;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.RecentVoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandListEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.EVoucherContract;
import cn.swiftpass.wallet.tiqmo.module.voucher.presenter.VoucherBrandListPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

import static cn.swiftpass.wallet.tiqmo.module.home.presenter.HomeFragmentPresenter.SCENE_TYPE_VOUCHER;

public class VoucherMarketActivity extends BaseCompatActivity<EVoucherContract.VoucherBrandListPresenter> implements EVoucherContract.VoucherBrandListView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_biller)
    RoundedImageView ivBiller;
    @BindView(R.id.tv_biller_name)
    TextView tvBillerName;
    @BindView(R.id.ll_sku_head)
    LinearLayout llSkuHead;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_select_brand)
    RecyclerView rvSelectBrand;
    @BindView(R.id.rv_recent_brand)
    RecyclerView rvRecentBrand;
    @BindView(R.id.rv_search_brand)
    RecyclerView rvSearchBrand;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_background)
    LinearLayout llBackground;
    @BindView(R.id.ll_search_content)
    LinearLayout llSearchContent;
    @BindView(R.id.ll_show_more)
    LinearLayout llShowMore;
    @BindView(R.id.tv_show_more)
    TextView tvShowMore;
    @BindView(R.id.iv_show_more)
    ImageView ivShowMore;
    @BindView(R.id.ll_recent)
    LinearLayout llRecent;
    @BindView(R.id.ll_select_brand)
    LinearLayout llSelectBrand;
    @BindView(R.id.sw_evoucher_category)
    SwipeRefreshLayout swEvoucherCategory;

    private StatusView typeStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private VoucherMarketAdapter voucherMarketAdapter;

    private VoucherBrandSearchAdapter voucherBrandSearchAdapter;

    private VoucherRecentAdapter voucherRecentAdapter;

    //代金券品牌信息
    public List<VoucherBrandEntity> queryVoucherBrandAndCountResList = new ArrayList<>();

    public List<VoucherBrandEntity> showList = new ArrayList<>();

    private EVoucherOrderInfoEntity mEVoucherOrderInfoEntity;

    private boolean isShowMore;

    //代金券 最近交易列表
    public List<RecentVoucherBrandEntity> voucherRecentDealsInfoList = new ArrayList<>();

    private List<VoucherBrandEntity> filterTypeList = new ArrayList<>();

    private String searchString;

    private String brandName;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_voucher_market;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.newhome_13);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        swEvoucherCategory.setEnabled(false);
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setVisibility(View.GONE);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_31));
        typeStatusView = new StatusView.Builder(mContext, rvSelectBrand).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();

        etSearch.setHint(getString(R.string.newhome_30));
        ivShowMore.setVisibility(View.GONE);

        GridLayoutManager manager = new GridLayoutManager(mContext, 3);
        MyItemDecoration myItemDecoration = MyItemDecoration.createHorizontal(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        MyItemDecoration myItemDecoration1 = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        myItemDecoration1.setGrid(true);
        rvSelectBrand.addItemDecoration(myItemDecoration);
        rvSelectBrand.addItemDecoration(myItemDecoration1);
        rvSelectBrand.setLayoutManager(manager);
        voucherMarketAdapter = new VoucherMarketAdapter(mContext, queryVoucherBrandAndCountResList);
        voucherMarketAdapter.bindToRecyclerView(rvSelectBrand);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        MyItemDecoration myItemDecoration2 = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvSearchBrand.addItemDecoration(myItemDecoration2);
        rvSearchBrand.setLayoutManager(layoutManager);
        voucherBrandSearchAdapter = new VoucherBrandSearchAdapter(filterTypeList);
        voucherBrandSearchAdapter.bindToRecyclerView(rvSearchBrand);

        voucherMarketAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                mPresenter.checkOrderExist(queryVoucherBrandAndCountResList.get(position), SCENE_TYPE_VOUCHER);
            }
        });

        voucherBrandSearchAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                mPresenter.checkOrderExist(filterTypeList.get(position), SCENE_TYPE_VOUCHER);
            }
        });

        llBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        voucherRecentAdapter = new VoucherRecentAdapter(mContext, voucherRecentDealsInfoList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        MyItemDecoration myItemDecoration3 = MyItemDecoration.createHorizontal(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        MyItemDecoration myItemDecoration4 = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        myItemDecoration4.setGrid(true);
        rvRecentBrand.addItemDecoration(myItemDecoration3);
        rvRecentBrand.addItemDecoration(myItemDecoration4);
        rvRecentBrand.setLayoutManager(gridLayoutManager);
        rvRecentBrand.setAdapter(voucherRecentAdapter);
        voucherRecentAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                RecentVoucherBrandEntity recentVoucherBrandEntity = voucherRecentDealsInfoList.get(position);
                brandName = recentVoucherBrandEntity.voucherName;
                mPresenter.eVoucherOrder(recentVoucherBrandEntity.voucherCode, "");
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etSearch.hideErrorView();
                searchString = s.toString();
                if (queryVoucherBrandAndCountResList != null && queryVoucherBrandAndCountResList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });

        mPresenter.getVoucherBrandList();
        mPresenter.getRecentVoucherBrandList();
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            filterTypeList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
                llSearchContent.setVisibility(View.GONE);
//                rvSearchBrand.setVisibility(View.GONE);
//                llRecent.setVisibility(View.VISIBLE);
//                llSelectBrand.setVisibility(View.VISIBLE);
//                rvRecentBrand.setVisibility(View.VISIBLE);
//                rvSelectBrand.setVisibility(View.VISIBLE);
                filterTypeList.clear();
//                if (showList.size() > 0) {
//                    voucherMarketAdapter.setDataList(showList);
//                    llShowMore.setVisibility(View.VISIBLE);
//                } else {
//                    voucherMarketAdapter.setDataList(queryVoucherBrandAndCountResList);
//                    llShowMore.setVisibility(View.GONE);
//                }
                return;
            } else {
//                rvSearchBrand.setVisibility(View.VISIBLE);
//                rvSelectBrand.setVisibility(View.GONE);
//                llShowMore.setVisibility(View.GONE);
//                llSelectBrand.setVisibility(View.GONE);
//                llRecent.setVisibility(View.GONE);
//                rvRecentBrand.setVisibility(View.GONE);
            }
            if (queryVoucherBrandAndCountResList != null && queryVoucherBrandAndCountResList.size() > 0) {
                int size = queryVoucherBrandAndCountResList.size();
                for (int i = 0; i < size; i++) {
                    VoucherBrandEntity voucherBrandEntity = queryVoucherBrandAndCountResList.get(i);
                    String name = voucherBrandEntity.voucherBrandName;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterTypeList.add(voucherBrandEntity);
                    }
                }
                if (filterTypeList.size() > 0) {
                    llSearchContent.setVisibility(View.VISIBLE);
                } else {
                    llSearchContent.setVisibility(View.GONE);
                }
                voucherBrandSearchAdapter.setDataList(filterTypeList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close, R.id.ll_show_more})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_show_more:
                if (isShowMore) {
                    isShowMore = false;
                    voucherMarketAdapter.setDataList(showList);
                    tvShowMore.setText(getString(R.string.newhome_28));
                    ivShowMore.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_more_down));
                } else {
                    isShowMore = true;
                    voucherMarketAdapter.setDataList(queryVoucherBrandAndCountResList);
                    tvShowMore.setText(getString(R.string.newhome_29));
                    ivShowMore.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_more_up));
                }
                break;
            case R.id.iv_search:
                llBackground.setVisibility(View.VISIBLE);
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                llSearchContent.setVisibility(View.GONE);
                llBackground.setVisibility(View.GONE);
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void getVoucherBrandListSuccess(VoucherBrandListEntity voucherBrandListEntity) {
        isShowMore = false;
        if (voucherBrandListEntity != null) {
            queryVoucherBrandAndCountResList.clear();
            queryVoucherBrandAndCountResList.addAll(voucherBrandListEntity.queryVoucherBrandAndCountResList);
            int size = queryVoucherBrandAndCountResList.size();
            if (size > 6) {
                llShowMore.setVisibility(View.VISIBLE);
                for (int i = 0; i < 6; i++) {
                    VoucherBrandEntity voucherBrandEntity = queryVoucherBrandAndCountResList.get(i);
                    showList.add(voucherBrandEntity);
                }
                voucherMarketAdapter.setDataList(showList);
            } else {
                llShowMore.setVisibility(View.GONE);
                voucherMarketAdapter.setDataList(queryVoucherBrandAndCountResList);
            }
        }
    }

    @Override
    public void getRecentVoucherBrandListSuccess(VoucherBrandListEntity voucherBrandListEntity) {
        if (voucherBrandListEntity != null) {
            voucherRecentDealsInfoList.clear();
            voucherRecentDealsInfoList.addAll(voucherBrandListEntity.voucherRecentDealsInfoList);
            voucherRecentAdapter.setDataList(voucherRecentDealsInfoList);
        }
    }

    @Override
    public void getVoucherBrandListFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getRecentVoucherBrandListFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void showOrderRepeatDia(VoucherBrandEntity voucherBrandEntity, String sceneType) {
        OrderRepeatNoticeDia dia = OrderRepeatNoticeDia.newInstance(voucherBrandEntity.voucherBrandName);
        if (getFragmentManager() != null) {
            dia.show(getSupportFragmentManager());
            dia.setListener(new OrderRepeatNoticeDia.ContinueClickListener() {
                @Override
                public void continueClick() {
                    mPresenter.continueToSelectTickets(VoucherMarketActivity.this, voucherBrandEntity, sceneType);
                }
            });
        }
    }

    @Override
    public void checkOrderStatusSuccess(VoucherBrandEntity voucherBrandEntity, String sceneType) {
        mPresenter.continueToSelectTickets(VoucherMarketActivity.this, voucherBrandEntity, sceneType);
    }

    @Override
    public void eVoucherOrderSuccess(EVoucherOrderInfoEntity eVoucherOrderInfoEntity) {
        if (eVoucherOrderInfoEntity != null) {
            this.mEVoucherOrderInfoEntity = eVoucherOrderInfoEntity;
            mPresenter.checkOut("", eVoucherOrderInfoEntity.orderInfo);
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity, CheckOutEntity checkOutEntity) {
        HashMap<String, Object> limitMap = new HashMap<>();
        limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        limitMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        limitMap.put("brandName", brandName);
        limitMap.put(Constants.mEVoucherOrderInfoEntity, mEVoucherOrderInfoEntity);
        limitMap.put(Constants.sceneType, Constants.sceneType_EVoucher);
        ActivitySkipUtil.startAnotherActivity(VoucherMarketActivity.this,
                EVoucherPayReviewActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        mPresenter.getLimit(Constants.LIMIT_TRANSFER, checkOutEntity);
    }

    @Override
    public void showEVouCountryDia(VouSupCountryEntity result, String providerCode, String brandCode, String brandName) {
        result.voucherServiceProviderCode = providerCode;
        result.voucherBrandCode = brandCode;
        result.brandName = brandName;
        EVoucherSelectCountryDialogFrag dialogFrag = EVoucherSelectCountryDialogFrag.newInstance(result);
        if (getFragmentManager() != null) {
            dialogFrag.show(getSupportFragmentManager(), "voucherS");
            dialogFrag.setOnCountryClickListener(new EVoucherSelectCountryDialogFrag.CountryClickListener() {
                @Override
                public void onCountryClick(VouSupCountryEntity countryEntity, int position) {
                    mPresenter.skipToVouTypeAndInfo(VoucherMarketActivity.this, countryEntity.voucherServiceProviderCode, countryEntity.voucherBrandCode,
                            countryEntity.brandName, countryEntity.voucherSupportCountry.get(position).countryCode);
                }
            });
        }
    }

    @Override
    public EVoucherContract.VoucherBrandListPresenter getPresenter() {
        return new VoucherBrandListPresenter();
    }
}
