package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.UnScrollViewPager;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMainPagerAdapter;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.AddBeneficiarySuccessDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrDeclarationDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrEditBeneficiarySuccessDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrMainContract;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrMainPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;

public class ImrMainActivity extends BaseCompatActivity<ImrMainContract.IImrMainPresenter> implements ImrMainContract.ImrMainView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.indicator_imr_main)
    MagicIndicator indicatorImrMain;
    @BindView(R.id.vp_imr_main)
    UnScrollViewPager vpImrMain;

    AddBeneficiarySuccessDialog addBeneficiarySuccessDialog;
    BeneficiaryListFragment beneficiaryListFragment;
    SendMoneyFragment sendMoneyFragment;

    @Override
    public ImrMainContract.IImrMainPresenter getPresenter() {
        return new ImrMainPresenter();
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_imr_main;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_IMR_ADD_BENE_SUCCESS == event.getEventType()) {
            ImrBeneficiaryEntity imrBeneficiaryEntity = event.getImrBeneficiaryEntity();
            RiskControlEntity riskControlEntity = event.getRiskControlEntity();
            addBeneficiarySuccessDialog = AddBeneficiarySuccessDialog.getInstance(mContext, imrBeneficiaryEntity);
            addBeneficiarySuccessDialog.setAddBeneficiaryDoneListener(new AddBeneficiarySuccessDialog.AddBeneficiaryDoneListener() {
                @Override
                public void addBeneficiaryDone() {
                    addBeneficiarySuccessDialog.dismiss();
                    if (beneficiaryListFragment != null) {
                        beneficiaryListFragment.addBeneficiaryIvr(true,riskControlEntity,imrBeneficiaryEntity);
                    }
                }
            });
            addBeneficiarySuccessDialog.showWithBottomAnim();

            if (beneficiaryListFragment != null) {
                beneficiaryListFragment.getBeneficiaryList(false);
            }
            AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(null);
            if (sendMoneyFragment != null) {
                sendMoneyFragment.initUI();
            }
        } else if (EventEntity.EVENT_IMR_SEND_ADD_BENE_SUCCESS == event.getEventType()) {
            if (beneficiaryListFragment != null) {
                vpImrMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
                beneficiaryListFragment.getBeneficiaryList(false);
            }
            AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(null);
            if (sendMoneyFragment != null) {
                sendMoneyFragment.initUI();
            }
        } else if (EventEntity.EVENT_IMR_EDIT_BENE_SUCCESS == event.getEventType()) {
            RiskControlEntity riskControlEntity = event.getRiskControlEntity();
            ImrBeneficiaryDetails.ImrBeneficiaryDetail entity = event.getResponseEntity();
            ImrEditBeneficiarySuccessDialog editSuccessDialog = ImrEditBeneficiarySuccessDialog.getInstance(mContext, entity);
            editSuccessDialog.setOnDialogClickListener(new ImrEditBeneficiarySuccessDialog.OnDialogClickListener() {
                @Override
                public void onRemoveClick() {
//                    if (beneficiaryListFragment != null) {
//                        beneficiaryListFragment.getBeneficiaryList(false);
//                    }
                }

                @Override
                public void onCancelClick() {
                    editSuccessDialog.dismiss();
                    if (beneficiaryListFragment != null) {
                        beneficiaryListFragment.addBeneficiaryIvr(true,riskControlEntity,null);
                    }
                    if (beneficiaryListFragment != null) {
                        beneficiaryListFragment.getBeneficiaryList(false);
                    }
                }
            });
            editSuccessDialog.showWithBottomAnim();
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        initView();
        initData();
    }

    private void initView() {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, headCircle);
        tvTitle.setText(R.string.IMR_20);

        List<String> title = new ArrayList<>(Arrays.asList(this.getString(R.string.IMR_21),
                this.getString(R.string.IMR_22)));


        List<Fragment> fragments = new ArrayList<>();
        beneficiaryListFragment = new BeneficiaryListFragment();
        sendMoneyFragment = new SendMoneyFragment();
        fragments.add(beneficiaryListFragment);
        fragments.add(sendMoneyFragment);
        if (LocaleUtils.isRTL(mContext)) {
            Collections.reverse(title);
            Collections.reverse(fragments);
        }
        ImrMainPagerAdapter adapter = new ImrMainPagerAdapter(getSupportFragmentManager(), fragments);
        vpImrMain.setScrollEnable(false);// 禁止滑动
        vpImrMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if (LocaleUtils.isRTL(mContext) ? position == 0 : position == 1) {
//                    ImrDeclarationDialog dialog = new ImrDeclarationDialog();
//                    dialog.setCancelListener(new ImrDeclarationDialog.OnDialogCancelListener() {
//                        @Override
//                        public void onCancel() {
//                            vpImrMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? title.size() - 1 : 0);
//                        }
//                    });
//                    dialog.show(getSupportFragmentManager(), "ImrDeclaration");
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        vpImrMain.setAdapter(adapter);
        if (LocaleUtils.isRTL(mContext)) {
            vpImrMain.setCurrentItem(title.size() - 1);
        }
        indicatorImrMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return title == null ? 0 : title.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(title.get(index));
                clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                clipPagerTitleView.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                clipPagerTitleView.setClipColor(Color.WHITE);
                clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));

                clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ButtonUtils.isFastDoubleClick()) {
                            return;
                        }
                        setViewPageIndex(index);
                    }
                });
                return clipPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setLineHeight(UIUtil.dip2px(context, 44));
                indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });
        indicatorImrMain.setNavigator(commonNavigator);
        commonNavigator.onPageSelected(LocaleUtils.isRTL(mContext) ? title.size() - 1 : 0);
        ViewPagerHelper.bind(indicatorImrMain, vpImrMain);

        getBalance();
    }

    private void initData() {

    }

    public void setViewPageIndex(int index){
        if (LocaleUtils.isRTL(mContext) ? index == 0 : index == 1) {
            ImrDeclarationDialog dialog = ImrDeclarationDialog.getSingleton();
            dialog.setOnAgreeClickListener(new ImrDeclarationDialog.OnAgreeClickListener() {
                @Override
                public void onAgreeClick() {
                    vpImrMain.setCurrentItem(index);
                    if (sendMoneyFragment != null) {
                        sendMoneyFragment.initUI();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "declaration");
        } else {
            vpImrMain.setCurrentItem(index);
        }
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }
}
