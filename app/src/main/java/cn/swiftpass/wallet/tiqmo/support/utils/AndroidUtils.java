package cn.swiftpass.wallet.tiqmo.support.utils;

import static android.content.Context.CONNECTIVITY_SERVICE;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.scottyab.rootbeer.RootBeer;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.SelectTransferTypeActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.ComingSoonActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.EVoucherHistoryActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.NumberEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CardManageActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.OrginalTransactionActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CommonNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ExpireIdDialog;

public class AndroidUtils {

    private static final String TAG = "AndroidUtils";
    private static final String ATOZSTR = "[A-Z]";
    private static final String SPECIAL_CHAR_ONE = "#";
    private static final String SPECIAL_CHAR_TWO = "@";

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.widthPixels;
    }

    public  static ArrayList<Double> getintformap(HashMap<Double,Double>map){
        ArrayList<Double>dlk=new ArrayList<>();
        @SuppressWarnings("rawtypes")
        Set set=map.entrySet();
        @SuppressWarnings("rawtypes")
        Iterator iterator=set.iterator();
        while (iterator.hasNext()){
            @SuppressWarnings("rawtypes")
            Map.Entry mapentry= (Map.Entry) iterator.next();
            dlk.add((Double) mapentry.getKey());
        }
        Collections.sort(dlk);
        return dlk;
    }

    public static boolean isHKLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_MO) || lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_TW_NORMAL) || lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_MO_NEW_NORMAL) || lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_HK_NORMAL) || lan.equalsIgnoreCase(Constants.LANG_CODE_ZH_MO_NEW_NORMAL));
    }


    public static boolean isEGLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(Constants.LANG_CODE_EN_US) || lan.equalsIgnoreCase(Constants.LANG_CODE_EN_US_NORMAL));
    }

    public static void setDrawableStart(Context context, TextView textView,int resId){
        Drawable drawable = context.getResources().getDrawable(resId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        textView.setCompoundDrawables(drawable, null, null, null);
    }
    public static void setDrawableEnd(Context context, TextView textView,int resId){
        Drawable drawable = context.getResources().getDrawable(resId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        textView.setCompoundDrawables(null, null, drawable, null);
    }

    public static int getActionBarHeight(Context context) {
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 获取当前app的版本名字.
     *
     * @param context
     * @return
     */
    public static String getCurrentAppVersionName(Context context) {
        PackageInfo info = getCurrentAppPackageInfo(context);
        String version = info.versionName;
        return version;
    }

    private static PackageInfo getCurrentAppPackageInfo(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            String packageName = context.getPackageName();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            return info;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean checkFirstTimeTC(Activity context) {
        String preVersionName = SpUtils.getInstance().getAppVersionName();
        String nowVersionName = null;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            nowVersionName = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException var5) {
            var5.printStackTrace();
        }

        return TextUtils.isEmpty(preVersionName) || !preVersionName.equals(nowVersionName);
    }

    /**
     * 得到当前软键盘的高度
     *
     * @return 软键盘的高度
     */
    public static int getCurrentSoftInputHeight(Activity activity) {
        final View decorView = activity.getWindow().getDecorView();
        Rect rect = new Rect();
        decorView.getWindowVisibleDisplayFrame(rect);
        // 获取屏幕的高度(包括状态栏，导航栏)
        int screenHeight = decorView.getRootView().getHeight();
        int keySoftHeight = screenHeight - rect.bottom;
        return keySoftHeight;
    }

    public static boolean keyboardIsShow(Activity activity) {
        if (getCurrentSoftInputHeight(activity) > 200) {//导航高度没有高于200px的，所以如果高于200就就默认为有键盘弹出
            return true;
        }
        return false;
    }

//    public static void showKeyboard(Activity activity, EditText editText) {
//        editText.setFocusable(true);
//        editText.setFocusableInTouchMode(true);
//        editText.requestFocus();
//        InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
//        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
//    }

    public static void showKeyboard(Activity activity, EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public static void showKeyboardDelay(final EditText editText) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(editText, 0);
            }
        }, 300);
    }

    public static void showKeyboardView(EditText editText) {
        InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(editText, 0);
    }

    /**
     * 判断点击的位置是否在view范围内
     *
     * @param view
     * @param ev
     * @return
     */
    public static boolean inRangeOfView(View view, MotionEvent ev) {
        if (view != null) {
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            if (ev.getX() < x || ev.getX() > (x + view.getWidth()) || ev.getY() < y || ev.getY() > (y + view.getHeight())) {
                return false;
            }
            return true;
        }
        return true;
    }

    public static void hideKeyboard(View view) {
        if (view != null && view.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    /**
     * Dialog中隐藏软键盘
     */
    public static void HideSoftKeyBoardDialog(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
        } catch (Exception e) {
            LogUtils.d(TAG, "---" + e + "---");
        }
    }

    public static String addStrToPhone(String phoneNum) {
        if (!TextUtils.isEmpty(phoneNum)) {
            if (phoneNum.contains("+")) {
                return phoneNum;
            } else {
                return "+" + phoneNum;
            }

        }
        return "";
    }

    public static String addStarPhone(String phoneNum) {
        if (!TextUtils.isEmpty(phoneNum)) {
            StringBuilder sb = new StringBuilder();
            int length = phoneNum.length();
            for (int i = 0; i < length; i++) {
                char c = phoneNum.charAt(i);
                if (i > 0 && i < length - 2) {
                    sb.append('*');
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }
        return "";
    }

    /**
     * 获取剪切板文本内容，默认返回""
     *
     * @param context
     * @return
     */
    public static String getClipboardText(Context context) {
        try {
            ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData abc = cm.getPrimaryClip();
            ClipData.Item item = abc.getItemAt(0);
            return item.getText().toString();
        } catch (Throwable e) {
            LogUtils.d(TAG, "---" + e + "---");
        }
        return "";
    }

    /**
     * 复制文本到剪切板
     *
     * @param context
     * @param text
     */
    public static void setClipboardText(Context context, String text, boolean isIbanNumber) {
        try {
            ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            if (isIbanNumber) {
                text = text.replace(" ", "");
            }
            ClipData cd = ClipData.newPlainText("text", text);
            cm.setPrimaryClip(cd);
        } catch (Throwable e) {
            LogUtils.d(TAG, "---" + e + "---");
        }
    }

    /**
     * 是否开启生物识别登录
     */
    public static boolean isOpenBiometricLogin() {
        return AppClient.getInstance().getBiometricManager().isEnableFingerprintLogin();
    }


    /**
     * 是否开启生物识别支付
     */
    public static boolean isOpenBiometricPay() {
        return AppClient.getInstance().getBiometricManager().isEnableFingerprintPay();
    }

//    public static Uri saveImageToSdCard(Context context, Bitmap bmp, String filePath) {
//        // 首先保存图片
//        File appDir = new File(filePath);
//        if (!appDir.exists()) {
//            appDir.mkdir();
//        }
//        String fileName = System.currentTimeMillis() + ".jpg";
//        File file = new File(appDir, fileName);
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(file);
//            //通过io流的方式来压缩保存图片
//            boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
//            fos.flush();
//            //把文件插入到系统图库
//            //MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), fileName, null);
//            //保存图片后发送广播通知更新数据库
////            Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
//            Uri uri = Uri.fromFile(file);
//            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
//            return uri;
//        } catch (IOException e) {
//            LogUtils.e(TAG, Log.getStackTraceString(e));
//        } finally {
//            FileUtils.closeIO(fos);
//        }
//        return null;
//    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int screenHeight = dm.heightPixels;
        return screenHeight;
    }

    /**
     * 加载本地图片
     *
     * @param url
     * @return
     */
    public static Bitmap getLocalBitmap(String url) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(url);
            return BitmapFactory.decodeStream(fis);  ///把流转化为Bitmap图片

        } catch (FileNotFoundException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
            return null;
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
    }

    public static void copyToClipBoard(Context context, String text) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(
                Context.CLIPBOARD_SERVICE);
        cm.setPrimaryClip(ClipData.newPlainText("xdroid_copy", text));
        Toast.makeText(context, "copy success", Toast.LENGTH_SHORT).show();
    }

//    public static void shareImage(Context mcontext, String content, String imgPath) {
////        content = "Hello \uD83D\uDC4F hurry up and arrange you delivery \uD83D\uDE80 to receive your shipment \uD83C\uDF81 33262714881 from iHerb.com faster";
//        Intent intent = new Intent(Intent.ACTION_SEND);
//        //文本内容为空就分享截图
//        if (TextUtils.isEmpty(content)) {
//            File f = new File(imgPath);
//            if (f != null && f.exists() && f.isFile()) {
//                intent.setType("image/*");
//                Uri u = Uri.fromFile(f);
//                intent.putExtra(Intent.EXTRA_STREAM, u);
//            }
//        } else {
//            intent.setType("text/plain");
//            intent.putExtra(Intent.EXTRA_TEXT, content);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        }
//        intent.putExtra(Intent.EXTRA_SUBJECT, "");
////        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mcontext.startActivity(Intent.createChooser(intent, ""));
//    }

    /**
     * 返回uri
     */
    private static Uri getUriForFile(Context context, File file) {
        //应用包名.fileProvider
        String authority = context.getPackageName().concat(".fileProvider");
        Uri fileUri = FileProvider.getUriForFile(context, authority, file);
        Log.e(TAG, "onSuccess: 文件路径：" + authority);
        Log.e(TAG, "onSuccess: 文件路径：" + fileUri.toString());
        Log.e(TAG, "onSuccess: 文件路径：" + file.toString());
        return fileUri;
    }

    public static void shareFile(Context context, File file) {
//        File file = new File(fileName);
        if (null != file && file.exists()) {
            Intent share = new Intent(Intent.ACTION_SEND);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                Uri contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", file);
//                share.putExtra(Intent.EXTRA_STREAM, contentUri);
//                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            } else {
            share.putExtra(Intent.EXTRA_STREAM, getUriForFile(context, file));
//            }

            share.setType("application/pdf");//此处可发送多种文件
//            share.setType("application/vnd.ms-excel");//此处可发送多种文件
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            share.addCategory(Intent.CATEGORY_DEFAULT);
            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            share.setDataAndType(Uri.fromFile(file), "application/pdf");
//            context.startActivity(share);
            context.startActivity(Intent.createChooser(share, context.getString(R.string.wtw_34)));
        } else {
            Toast.makeText(context, "file not found", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * FLAG_GRANT_READ_URI_PERMISSION 设置临时权限 如果不写 吊起的第三方app 拿不到图片
     *
     * @param mcontext
     * @param content
     * @param imgPath
     */
    public static void shareImage(Context mcontext, String content, Uri imgPath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        //文本内容为空就分享截图
        if (TextUtils.isEmpty(content)) {
            intent.putExtra(Intent.EXTRA_STREAM, imgPath);
            //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
            intent.setType("image/*");
        } else {
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, content);
        }
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_PREFIX_URI_PERMISSION);
        }
        mcontext.startActivity(Intent.createChooser(intent, ""));

    }

    public static void shareInviteImage(Context mcontext, String content, Uri imgPath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        //文本内容为空就分享截图
//        if (TextUtils.isEmpty(content)) {
        intent.putExtra(Intent.EXTRA_STREAM, imgPath);
        //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
        intent.setType("image/*");
//        } else {
//            intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, content);
//        }
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_PREFIX_URI_PERMISSION);
        }
        mcontext.startActivity(Intent.createChooser(intent, content));

    }

    public static int getRewardIcon(String badges) {
        if ("SILVER".equals(badges)) {
            return R.drawable.l_silver_reward;
        } else if ("GOLD".equals(badges)) {
            return R.drawable.l_gold_reward;
        } else {
            return R.drawable.l_platinum_reward;
        }
    }

    public static int getRewardTitle(String badges) {
        if ("SILVER".equals(badges)) {
            return R.string.sprint11_42;
        } else if ("GOLD".equals(badges)) {
            return R.string.sprint11_41;
        } else {
            return R.string.sprint11_43;
        }
    }

    public static void layoutView(View v, int width, int height) {
        v.layout(0, 0, width, height);
        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
        v.measure(measuredWidth, measuredHeight);
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
    }

    public static void layoutView(View v, int left, int top, int width, int height) {
        v.layout(left, top, left + width, top + height);
        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
        v.measure(measuredWidth, measuredHeight);
        v.layout(left, top, left + v.getMeasuredWidth(), top + v.getMeasuredHeight());
    }

    public static void startAppSetting(Context mContext) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + mContext.getPackageName()));
        mContext.startActivity(intent);
    }

    public static Calendar long2calendar(long time) {
        Date date = new Date(time);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static String long2str(long time, String format) {
        if (TextUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        return new SimpleDateFormat(format,Locale.ENGLISH).format(long2calendar(time).getTime());
    }

    public static String localImageStr(long time) {
        return long2str(time, "yyyyMMdd_HHmmss");
    }

    /**
     * 根据作用域保存图片 Android 10后需要用这个方案
     * 1.外部存储也有应用的私有目录，严格来说叫做外部存储私有目录 卸载应用后，外部存储私有路径中的文件同样会被清除
     * 2./storage/emulated/0/Android/data/<package> 拥有files文件夹和cache文件夹 getExternalCacheDir/getExternalFilesDir
     * 3.尽管这些文件在技术上可被用户和其他应用访问（因为它们存储在外部存储上），但它们不能为应用之外的用户提供价值。可以使用此目录来存储您不想与其他应用共享的文件。
     * <p>
     * <p>
     * getExternalFilesDir Context.getExternalFilesDir()方法可以获取到 SDCard/Android/data/{package_name}/files/
     * getExternalCacheDir Context.getExternalCacheDir()方法可以获取到 SDCard/Android/data/{package_name}/cache
     * Environment.getExternalStorageDirectory().getAbsolutePath()   storage/emulated/0/
     * <p>
     * 分享的图片不应该在相册中显示 放在app sd卡 私有路径
     *
     * @param context
     * @param bmp
     * @return
     */
    public static Uri tempSaveImageToSdCardByScopedStorageByShareImage(Context context, Bitmap bmp, String fileName) {
        // 首先保存图片 必须带文件后缀
        FileOutputStream fos = null;
        File appDir = null;
        try {
            appDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            ///storage/emulated/0/Android/data/com.bochk.bocpay/files/Pictures
            LogUtils.i(TAG, "appDir:" + appDir.getAbsolutePath());
            File file = new File(appDir, fileName);
            fos = new FileOutputStream(file);
            boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
            fos.flush();
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Uri uriForFile = FileProvider.getUriForFile(
                        context,
                        BuildConfig.APPLICATION_ID+".fileProvider",
                        file
                );
                return uriForFile;
            } else {
                return Uri.fromFile(file);
            }
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
//            FileUtils.closeIO(fos);
        }
        return null;

    }

    public static boolean saveImageToGalleryByScopedStorage(Context context, Bitmap bmp) {
        // 首先保存图片 必须带文件后缀
        String fileName = "transfer_" + localImageStr(System.currentTimeMillis());
        return saveImageToGalleryByScopedStorage(context, bmp, fileName);
    }

    /**
     * 公有目录写入数据，必须要用MediaStore
     *
     * @param context
     * @param bmp
     * @param name
     * @return
     */
    public static boolean saveImageToGalleryByScopedStorage(Context context, Bitmap bmp, String name) {
        // 首先保存图片 必须带文件后缀
        String fileName = name + ".jpg";
        OutputStream outputStream = null;
        FileOutputStream fos = null;
        try {
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //设置保存参数到ContentValues中
                ContentValues contentValues = new ContentValues();
                //设置文件名
                contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
                //设置文件类型
                contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
                //RELATIVE_PATH是相对路径不是绝对路径
                //DCIM是系统文件夹，关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM);
                //因为insert 如果uri存在的话，会返回null 所以需要先delete
                context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Images.Media.DISPLAY_NAME + "=?",
                        new String[]{fileName});
                //执行insert操作，向系统文件夹中添加文件
                //EXTERNAL_CONTENT_URI代表外部存储器，该值不变
                Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                if (uri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    outputStream = context.getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                        outputStream.flush();
                        outputStream.close();

                        return isSuccess;
                    }
                }
            } else {
                String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + context.getString(R.string.app_name);
                File appDir = new File(storePath);
                if (!appDir.exists()) {
                    appDir.mkdir();
                }
                File file = new File(appDir, fileName);
                fos = new FileOutputStream(file);
                //通过io流的方式来压缩保存图片
                boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
                fos.flush();
                //保存图片后发送广播通知更新数据库
                Uri uri = Uri.fromFile(file);
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                return isSuccess;
            }

        } catch (IOException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }

                if (fos != null) {
                    fos.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
//            FileUtils.closeIO(outputStream);
//            FileUtils.closeIO(fos);
        }
        return false;
    }

    public static boolean saveImageToGallery(Context context, Bitmap bmp) {
        // 首先保存图片
//        String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + context.getString(R.string.app_name);
        String storePath = context.getExternalFilesDir(Environment.DIRECTORY_SCREENSHOTS).getAbsolutePath() + File.separator + context.getString(R.string.app_name);
        File appDir = new File(storePath);
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            //通过io流的方式来压缩保存图片
            boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
            fos.flush();
            //把文件插入到系统图库
            //MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), fileName, null);
            //保存图片后发送广播通知更新数据库
            Uri uri = Uri.fromFile(file);
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
            if (isSuccess) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
//            FileUtils.closeIO(fos);
        }
        return false;
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height, int color) {
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        c.drawColor(color);
        /** 如果不设置canvas画布为白色，则生成透明 */
        v.layout(0, 0, width, height);
        v.draw(c);
        return bmp;
    }

    public static Bitmap getViewGroupBitmap(LinearLayout viewGroup) {
        //viewGroup的总高度
        int h = 0;
        Bitmap bitmap;

        int count = viewGroup.getChildCount();
        // 适用于ListView或RecyclerView等求高度
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            h += view.getHeight();
        }

        // 若viewGroup是ScrollView，那么他的直接子元素有id的话，如下所示:
        // h = mLinearLayout.getHeight();
        // 创建对应大小的bitmap(重点)
        bitmap = Bitmap.createBitmap(viewGroup.getWidth(), h, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        viewGroup.draw(canvas);
        return bitmap;
    }

    public static Bitmap loadBitmapFromView(View v, int left, int top, int width, int height, int color) {
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        c.drawColor(color);
        /** 如果不设置canvas画布为白色，则生成透明 */
        v.layout(left, top, left + width, top + height);
        v.draw(c);
        return bmp;
    }

    public static String convertBankCardNoWithBlank(String BankCardNo) {
        String regex = "(.{4})";
        String CardNo_with_black = BankCardNo.replaceAll(regex, "$1  ");
        return CardNo_with_black;
    }

    public static String converExpireDate(String ExpireDate) {
        String first = ExpireDate.substring(0, 2);
        String second = ExpireDate.substring(2, ExpireDate.length());
        return first + "/" + second;
    }

    public static String changeBanNum(String iBanNum) {
        char[] bankNoArray = iBanNum.toCharArray();
        String bankNoString = "";
        for (int i = 0; i < bankNoArray.length; i++) {
            if (i % 2 == 0 && i < 4) {
                bankNoString += " ";
            }
            if (i % 4 == 0 && i > 3) {
                bankNoString += " ";
            }
            bankNoString += bankNoArray[i];
        }
        return bankNoString;
    }

    public static String showCardNum(String cardNum) {
        char[] bankNoArray = cardNum.toCharArray();
        String bankNoString = "";
        for (int i = 0; i < bankNoArray.length; i++) {
            if (i % 4 == 0 && i > 3) {
                bankNoString += " ";
            }
            bankNoString += bankNoArray[i];
        }
        return bankNoString;
    }

    public static String getPhone(String phone) {
        if (!TextUtils.isEmpty(phone) && !phone.startsWith("+966")) {
            return "+966 " + phone;
        }
        return phone;
    }

    /**
     * 获取后台的金额  要先除以100  主要用来展示
     *
     * @param money
     * @return
     */
    public static String getTransferMoney(String money) {
        try {
            money = money.replace(",", "");
            String resultMoney = BigDecimalFormatUtils.div(money, UserInfoManager.getInstance().getConvertUnit(),2);
            return AmountUtil.dataFormat(resultMoney);
        } catch (Exception e) {
            LogUtils.d(TAG, "---" + e + "---");
        }
        return "";
    }

    /**
     * 获取后台的金额  去掉多余的0  用于spending模块
     *
     * @param money
     * @return
     */
    public static String getTransferStringMoney(String money) {
        if(TextUtils.isEmpty(money))money = "0.0";
        try {
            money = money.replace(",", "");
            if(BigDecimalFormatUtils.isZero(money)){
                return "0";
            }
            String resultMoney = BigDecimalFormatUtils.div(money,UserInfoManager.getInstance().getConvertUnit(),2);
            return subZeroAndDot(resultMoney);
        } catch (Exception e) {
            LogUtils.d(TAG, "---" + e + "---");
        }
        return "";
    }

    public static String subZeroAndDot(String s){
        if(s.indexOf(".") > 0){
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }


    public static double getTransferMoneyNumber(String money) {
        try {
            money = money.replace(",", "");
            if(BigDecimalFormatUtils.isZero(money)){
                return 0;
            }
            String resultMoney = BigDecimalFormatUtils.div(money,UserInfoManager.getInstance().getConvertUnit(),2);
            return Double.parseDouble(resultMoney);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public static float getTransferMoneyFloat(String money) {
        try {
            money = money.replace(",", "");
            String resultMoney = BigDecimalFormatUtils.div(money,UserInfoManager.getInstance().getConvertUnit(),2);
            return Float.parseFloat(resultMoney);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public static int getTransferMoneyInt(String money) {
        try {
            money = money.replace(",", "");
            String resultMoney = BigDecimalFormatUtils.div(money,UserInfoManager.getInstance().getConvertUnit(),0);
            return Integer.parseInt(resultMoney);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public static String getTransferIntNumber(String money) {
        try {
            money = money.replace(",", "");
            String resultMoney = BigDecimalFormatUtils.div(money,UserInfoManager.getInstance().getConvertUnit(),0);
            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "0";
    }

    /**
     * 传给后台的金额  要先*100
     *
     * @param money
     * @return
     */
    public static String getReqTransferMoney(String money) {
        try {
            money = money.replace(",", "");
            String resultMoney = BigDecimalFormatUtils.mul(money,UserInfoManager.getInstance().getConvertUnit(),0);
            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "";
    }

    public static float getScreenDensity(Activity activity) {
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();//屏幕分辨率容器
        activity.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        int width = mDisplayMetrics.widthPixels;
        int height = mDisplayMetrics.heightPixels;
        float density = mDisplayMetrics.density;
        int densityDpi = mDisplayMetrics.densityDpi;
        return density;
    }

    /**
     * 判断是否使用VPN
     * @return
     */
    public static boolean checkVPN(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context .getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_VPN);
        boolean isVpnConn = networkInfo == null ? false : networkInfo.isConnected();
        return isVpnConn;
    }


    /**
     * 判断是否使用代理ip
     * @param context
     * @return
     */
    public static boolean isWifiProxy(Context context) {
        final boolean IS_ICS_OR_LATER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
        String proxyAddress;
        int proxyPort;
        if (IS_ICS_OR_LATER) {
            proxyAddress = System.getProperty("http.proxyHost");
            String portStr = System.getProperty("http.proxyPort");
            proxyPort = Integer.parseInt((portStr != null ? portStr : "-1"));
        } else {
            proxyAddress = android.net.Proxy.getHost(context);
            proxyPort = android.net.Proxy.getPort(context);
        }
        return (!TextUtils.isEmpty(proxyAddress)) && (proxyPort != -1);
    }

    public static String getContactName(String name) {
        if (!TextUtils.isEmpty(name)) {
            if (name.contains(" ")) {
                String[] names = name.split(" ");
                if (names.length > 2) {
                    int length = names.length;
                    name = names[0] + " " + names[length - 1];
                }
            }
        }
        return name;
    }

    public static int getBudgetCategoryDrawable(Context context, int value) {
        return ThemeSourceUtils.getSourceID(context, getBudgetCategoryDrawable(value));
    }

//    public static int getBudgetCategoryDrawable(int value) {
//        switch (value) {
//            case 1:
//                return R.attr.icon_budget_general;
//            case 2:
//                return R.attr.icon_budget_bill;
//            case 3:
//                return R.attr.icon_budget_entertainment;
//            default:
//                return -1;
//        }
//    }

    public static int getBudgetCategoryDrawable(int value) {
        switch (value) {
            case 1:
                return R.attr.digital_record_1;
            case 2:
                return R.attr.icon_digital_fee;
            case 3:
                return R.attr.digital_record_5;
            case 4:
                return R.attr.digital_record_4;
            case 6:
                return R.attr.digital_record_6;
            case 7:
                return R.attr.digital_record_7;
            case 8:
                return R.attr.digital_record_8;
            case 9:
                return R.attr.digital_record_9;
            case 10:
                return R.attr.digital_record_10;
            case 11:
                return R.attr.digital_record_11;
            case 12:
                return R.attr.digital_record_12;
            default:
                break;
        }
        return -1;
    }

    public static int getSpendingCategoryDrawable(Context context, int value) {
        return ThemeSourceUtils.getSourceID(context, getSpendingCategoryDrawable(value));
    }

    //    public static int getSpendingCategoryDrawable(int value) {
//        switch (value) {
//            case 1:
//                return R.drawable.d_spending_general;
//            case 2:
//                return R.drawable.d_spending_bills;
//            case 3:
//                return R.drawable.d_spending_eviron;
//            default:
//                return -1;
//        }
//    }

    public static int getSpendingProgressColor(String spendingCategoryId){
        switch (spendingCategoryId){
            case "1":
                //General
                return R.drawable.progressbar_budget_646478;
            case "2":
                //Bills
                return R.drawable.progressbar_budget_eb5f5a;
            case "3":
                //Entertainment
                return R.drawable.progressbar_budget_f0a03c;
            case "4":
                //Health
                return R.drawable.progressbar_budget_50c8f0;
            case "6":
                //Food & drink
                return R.drawable.progressbar_budget_a5d741;
            case "7":
                //Holiday
                return R.drawable.progressbar_budget_8223f5;
            case "8":
                //Groceries
                return R.drawable.progressbar_budget_69dc96;
            case "9":
                //Retail
                return R.drawable.progressbar_budget_2382f5;
            case "10":
                //Clothing
                return R.drawable.progressbar_budget_ff32be;
            case "11":
                //Motoring
                return R.drawable.progressbar_budget_c8c8c8;
            case "12":
                //Transport
                return R.drawable.progressbar_budget_ffdc00;
            default:return R.drawable.progressbar_budget_646478;
        }
    }

    public static int getSpendingCategoryColor(String spendingCategoryId){
        switch (spendingCategoryId){
            case "1":
                //General
                return R.color.color_646478;
            case "2":
                //Bills
                return R.color.color_EB5F5A;
            case "3":
                //Entertainment
                return R.color.color_F0A03C;
            case "4":
                //Health
                return R.color.color_50C8F0;
            case "6":
                //Food & drink
                return R.color.color_A5D741;
            case "7":
                //Holiday
                return R.color.color_8223F5;
            case "8":
                //Groceries
                return R.color.color_69DC96;
            case "9":
                //Retail
                return R.color.color_2382F5;
            case "10":
                //Clothing
                return R.color.color_FF32BE;
            case "11":
                //Motoring
                return R.color.color_C8C8C8;
            case "12":
                //Transport
                return R.color.color_FFDC00;
            default:return R.color.color_646478;
        }
    }

    public static int getSpendingCategoryDrawable(int value) {
        switch (value) {
            case 1:
                return R.attr.digital_record_1;
            case 2:
                return R.attr.icon_digital_fee;
            case 3:
                return R.attr.digital_record_5;
            case 4:
                return R.attr.digital_record_4;
            case 6:
                return R.attr.digital_record_6;
            case 7:
                return R.attr.digital_record_7;
            case 8:
                return R.attr.digital_record_8;
            case 9:
                return R.attr.digital_record_9;
            case 10:
                return R.attr.digital_record_10;
            case 11:
                return R.attr.digital_record_11;
            case 12:
                return R.attr.digital_record_12;
            default:
                break;
        }
        return -1;
    }

    public static int getCardFaceDrawable(String cardFaceId, boolean isHorizontal) {
//        if(TextUtils.isEmpty(cardFaceId)){
//            return isHorizontal ? R.drawable.standard_card_green_h : R.drawable.standard_card_green;
//        }
//        switch (cardFaceId) {
//            case "01":
//                return isHorizontal ? R.drawable.standard_card_green_h : R.drawable.standard_card_green;
//            case "02":
//                return isHorizontal ? R.drawable.standard_card_blue_h : R.drawable.standard_card_blue;
//            case "06":
//                return isHorizontal ? R.drawable.card_platinum_h : R.drawable.card_platinum;
//            case "07":
//                return isHorizontal ? R.drawable.card_platinum_h : R.drawable.card_platinum;
//            default:
//                return -1;
//        }

        if (TextUtils.isEmpty(cardFaceId)) {
            return isHorizontal ? R.drawable.standard_card_h : R.drawable.standard_card;
        }
        switch (cardFaceId) {
            case "01":
            case "02":
                return isHorizontal ? R.drawable.standard_card_h : R.drawable.standard_card;
            case "06":
            case "07":
                return isHorizontal ? R.drawable.card_platinum_h : R.drawable.platinum_card;
            default:
                return -1;
        }

    }

    public static int getNotifyDrawable(Context context, String msgSubject) {
        return ThemeSourceUtils.getSourceID(context, getNotifyDrawable(msgSubject));
    }

    private static int getNotifyDrawable(String msgSubject) {
        //MQR_PAYMENT和H5_PAYMENT 和 APP_PAYMENT 不需要toast
        switch (msgSubject) {
            case Constants.NOTIFY_SUBJECT_CQR_PAYMENT:
            case Constants.NOTIFY_SUBJECT_MQR_PAYMENT:
            case Constants.NOTIFY_SUBJECT_H5_PAYMENT:
            case Constants.NOTIFY_SUBJECT_APP_PAYMENT:
            case Constants.NOTIFY_SUBJECT_PAYMENT_REFUND:
                return R.attr.icon_scan_pay;
//            case Constants.NOTIFY_SUBJECT_BILL_PAYMENT_REFUND:
            case Constants.NOTIFY_SUBJECT_BILL_PAYMENT:
                return R.attr.utilities;

            case Constants.NOTIFY_SUBJECT_TRANSFER:
                return R.attr.transfer_any_contact;

            case Constants.NOTIFY_SUBJECT_TRANSFER_BANK:
                return R.attr.transfer_add_money_bank_account;

            case Constants.NOTIFY_SUBJECT_RECIEVE_REQUEST_TRANSFER:
            case Constants.NOTIFY_SUBJECT_REQUEST_TRANSFER_APPROVED:
            case Constants.NOTIFY_SUBJECT_REQUEST_TRANSFER_REJECTED:
                return R.attr.transfer_request_transfer;

            case Constants.NOTIFY_SUBJECT_ADD_MONEY:
                return R.attr.icon_add_money;

            case Constants.NOTIFY_SUBJECT_SPLIT_BILL_CONFIRM:
            case Constants.NOTIFY_SUBJECT_SPLIT_BILL_REJECT:
                return R.attr.icon_split_the_bill;

            case Constants.NOTIFY_SUBJECT_MOBILE_TOP_UP:
                return R.attr.mobile_topup;

            case Constants.NOTIFY_SUBJECT_PURCHASE_E_VOUCHER:
                return R.attr.evoucher_purchase;

            case Constants.NOTIFY_SUBJECT_BindCard:
                return R.attr.icon_add_money;

            case Constants.NOTIFY_SUBJECT_LOGIN_PD_CHANGE:
            case Constants.NOTIFY_SUBJECT_LOGIN_PD_RESET:
                return R.attr.forgot_login_password;

            case Constants.NOTIFY_SUBJECT_PAYMENT_PD_CHANGE:
            case Constants.NOTIFY_SUBJECT_PAYMENT_PD_RESET:
                return R.attr.forgot_login_password;

            case Constants.NOTIFY_SUBJECT_REGISTERATION:
                return R.attr.icon_registration_success;

            case Constants.NOTIFY_SUBJECT_CHANGE_LOGIN_DEVICE:
                return R.attr.icon_login_in_another_mobile;

            case Constants.NOTIFY_SUBJECT_KYC:
                return R.attr.icon_kyc_under_review;

            case Constants.NOTIFY_SUBJECT_CASHBACK_MARKET_ACTIVITY:
            case Constants.NOTIFY_SUBJECT_REFERRALCODE_CASHBACK:
                return R.attr.icon_cashback;

            case Constants.NOTIFY_SUBJECT_PORTAL_PUSH:
            case Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY:
            case Constants.NOTIFY_SUBJECT_UPDATE_USER_ID:
            case Constants.NOTIFY_SUBJECT_USER_KYC_STATUS_UNPD:
                return R.attr.icon_marketing_campaign;
            case Constants.NOTIFY_SUBJECT_I18N_REMTTIANCE_ACCEPT:
                return R.attr.home_int_remittance;

            default:
                return R.attr.icon_marketing_campaign;
        }
    }

    //消息中心点击通知消息跳转
    public static void jumpToNotifyActivity(Activity mActivity, String msgSubject) {
        //MQR_PAYMENT和H5_PAYMENT 和 APP_PAYMENT 不需要toast
        switch (msgSubject) {
            case Constants.NOTIFY_SUBJECT_CQR_PAYMENT:
            case Constants.NOTIFY_SUBJECT_MQR_PAYMENT:
            case Constants.NOTIFY_SUBJECT_H5_PAYMENT:
            case Constants.NOTIFY_SUBJECT_APP_PAYMENT:
            case Constants.NOTIFY_SUBJECT_PAYMENT_REFUND:
//            case Constants.NOTIFY_SUBJECT_BILL_PAYMENT_REFUND:
            case Constants.NOTIFY_SUBJECT_BILL_PAYMENT:
            case Constants.NOTIFY_SUBJECT_TRANSFER:
            case Constants.NOTIFY_SUBJECT_TRANSFER_BANK:
            case Constants.NOTIFY_SUBJECT_RECIEVE_REQUEST_TRANSFER:
            case Constants.NOTIFY_SUBJECT_REQUEST_TRANSFER_APPROVED:
            case Constants.NOTIFY_SUBJECT_ADD_MONEY:
            case Constants.NOTIFY_SUBJECT_SPLIT_BILL_CONFIRM:
            case Constants.NOTIFY_SUBJECT_MOBILE_TOP_UP:
            case Constants.NOTIFY_SUBJECT_PURCHASE_E_VOUCHER:
                ActivitySkipUtil.startAnotherActivity(mActivity, EVoucherHistoryActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case Constants.NOTIFY_SUBJECT_BindCard:
                ActivitySkipUtil.startAnotherActivity(mActivity, CardManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case Constants.NOTIFY_SUBJECT_LOGIN_PD_CHANGE:
            case Constants.NOTIFY_SUBJECT_LOGIN_PD_RESET:
            case Constants.NOTIFY_SUBJECT_PAYMENT_PD_CHANGE:
            case Constants.NOTIFY_SUBJECT_PAYMENT_PD_RESET:
            case Constants.NOTIFY_SUBJECT_REGISTERATION:
            case Constants.NOTIFY_SUBJECT_CHANGE_LOGIN_DEVICE:
            case Constants.NOTIFY_SUBJECT_KYC:
            case Constants.NOTIFY_SUBJECT_SPLIT_BILL_REJECT:
            case Constants.NOTIFY_SUBJECT_REQUEST_TRANSFER_REJECTED:
                break;
            default:
                break;
        }
    }

    public static String forceL2R(String content) {
        if (content == null) {
            return null;
        }
        return "\u202D" + content + "\u202C";
    }

    public static int getLoopHistoryDrawable(Context context, int subSceneType) {
        if (getLoopHistoryDrawable(subSceneType) == -1) {
            return -1;
        }
        return ThemeSourceUtils.getSourceID(context, getLoopHistoryDrawable(subSceneType));
    }

    private static int getLoopHistoryDrawable(int subSceneType) {
        switch (subSceneType) {
            case 1:
                return R.attr.digital_record_1;
            case 2:
                return R.attr.digital_record_2;
            case 3:
                return R.attr.digital_record_3;
            case 4:
                return R.attr.digital_record_4;
            case 5:
                return R.attr.digital_record_5;
            case 6:
                return R.attr.digital_record_6;
            case 7:
                return R.attr.digital_record_7;
            case 8:
                return R.attr.digital_record_8;
            case 9:
                return R.attr.digital_record_9;
            case 10:
                return R.attr.digital_record_10;
            case 11:
                return R.attr.digital_record_11;
            case 12:
                return R.attr.digital_record_12;
            default:
                break;
        }
        return -1;
    }

    public static int getLoopHistoryDrawableD(int subSceneType) {
        switch (subSceneType) {
            case 1:
                return R.drawable.d_digital_record_1;
            case 2:
                return R.drawable.d_digital_record_2;
            case 3:
                return R.drawable.d_digital_record_3;
            case 4:
                return R.drawable.d_digital_record_4;
            case 5:
                return R.drawable.d_digital_record_5;
            case 6:
                return R.drawable.d_digital_record_6;
            case 7:
                return R.drawable.d_digital_record_7;
            case 8:
                return R.drawable.d_digital_record_8;
            case 9:
                return R.drawable.d_digital_record_9;
            case 10:
                return R.drawable.d_digital_record_10;
            case 11:
                return R.drawable.d_digital_record_11;
            case 12:
                return R.drawable.d_digital_record_12;
            default:
                break;
        }
        return -1;
    }

    public static int getHistoryDrawable(Context context, int sceneType) {
        if (getHistoryDrawable(sceneType) == -1) {
            return -1;
        }
        return ThemeSourceUtils.getSourceID(context, getHistoryDrawable(sceneType));
    }

    private static int getHistoryDrawable(int sceneType) {
        switch (sceneType) {
            case 1:
                return R.attr.home_tuition_fees;
            case 2:
                return R.attr.icon_split_the_bill;
//            case 5:
//                return R.attr.mobile_topup;
            case 6:
                return R.attr.utilities;
            case 7:
            case 8:
                return -1;
            case 9:
                return R.attr.add_money_bank_account;
            case 10:
            case 35:
                return R.attr.add_money_slider;
            case 11:
                return R.attr.home_int_remittance;
            case 12:
                return R.attr.scan_qr_success;
            case 13:
                return R.attr.add_money_slider;
            case 14:
                return -1;
//            case 15:
//                return R.attr.evoucher_purchase;
            case 16:
                return R.attr.home_travel;
            case 17:
                break;
            case 18:
                return R.attr.icon_split_the_bill;
            case 19:
                return R.attr.icon_cashback;
            case 21:
                return R.attr.icon_cashback;
            case 22:
                //退款
                return R.attr.icon_transfer_refund;
            case 23:
                //国际话费充值
                return R.attr.mobile_topup;
            case 25:
                //卡费
                return R.attr.home_card;
            case 28:
                //卡费
                return R.attr.home_charity;
            case 38:
            case 39:
                return R.attr.digital_record_2;
            case 37:
            case 40:
                //争议费
                return R.attr.icon_digital_fee;
            default:
                break;
        }
        return -1;
    }

    public static int getHistoryDrawableD(int sceneType) {
        switch (sceneType) {
            case 1:
                return R.drawable.d_tuition_fees;
            case 2:
                return R.drawable.d_cinema_booking;
//            case 5:
//                return R.drawable.d_mobile_topup;
            case 6:
                return R.drawable.d_utilities;
            case 7:
            case 8:
                return -1;
            case 9:
                return R.drawable.d_add_money_bank_account;
            case 10:
            case 35:
                return R.drawable.d_add_money_slider;
            case 11:
                return R.drawable.d_int_remittance;
            case 12:
                return R.drawable.d_scan_qr_success;
            case 13:
                return R.drawable.d_add_money_slider;
            case 14:
                return -1;
//            case 15:
//                return R.drawable.d_evoucher_purchase;
            case 16:
                return R.drawable.d_travel;
            case 17:
                break;
            case 18:
                return R.drawable.d_split_the_bill;
            case 19:
                return R.drawable.d_cashback;
            case 21:
                return R.drawable.d_cashback;
            case 22:
                return R.drawable.d_transfer_refund;
//            case 23:
//                return R.drawable.d_mobile_topup;
            case 25:
                return R.drawable.d_home_card;
            case 28:
                return R.drawable.d_history_charity;
            case 38:
            case 39:
                return R.drawable.d_digital_record_2;
            case 37:
            case 40:
                return R.drawable.d_digital_fee;
            default:
                break;
        }
        return -1;
    }

    //sdk方法，判断手机是否root
    public static boolean isRooted() {
        boolean flag = false;
        try {
            if (!BuildConfig.SKIP_ROOT) {
                RootBeer rootBeer = new RootBeer(ProjectApp.getContext());
                return (rootBeer.isRootedWithoutBusyBoxCheck() || rootBeer.detectRootCloakingApps());
            }
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return flag;
    }

    /**
     * 根据给定的宽和高进行拉伸
     *
     * @param origin    原图
     * @param newWidth  新图的宽
     * @param newHeight 新图的高
     * @return new Bitmap
     */
    public static Bitmap scaleBitmap(Bitmap origin, int newWidth, int newHeight) {
        if (origin == null) {
            return null;
        }
        int height = origin.getHeight();
        int width = origin.getWidth();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);// 使用后乘
        Bitmap newBM = Bitmap.createBitmap(origin, 0, 0, width, height, matrix, false);
//        if (!origin.isRecycled()) {
//            origin.recycle();
//        }
        return newBM;
    }


    public static boolean compareDate(Date nowDate, Date compareDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String nowStr = df.format(nowDate);
        String compareStr = df.format(compareDate);
        try {
            Date now = df.parse(nowStr);
            Date compare = df.parse(compareStr);
            if (now.after(compare)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
            return false;
        }
    }

    public static Calendar longTocalendar(long time) {
        Date date = new Date(time);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static String getCurrentTime(long time, String format) {
        return new SimpleDateFormat(format,Locale.ENGLISH).format(longTocalendar(time).getTime());
    }

    public static String getContactPhone(String phone) {
        if (!TextUtils.isEmpty(phone)) {
            if (!phone.startsWith("+966")) {
                return "+966 " + phone;
            }
        }
        return phone;
    }

    public static void setCardLogoVisible(String cardNumber, ImageView iv_visa, ImageView iv_mastercard) {
        if (cardNumber.startsWith("4")) {
            iv_visa.setVisibility(View.VISIBLE);
            iv_mastercard.setVisibility(View.GONE);
        } else if (cardNumber.startsWith("5")) {
            iv_mastercard.setVisibility(View.VISIBLE);
            iv_visa.setVisibility(View.GONE);
        } else {
            iv_mastercard.setVisibility(View.GONE);
            iv_visa.setVisibility(View.GONE);
        }
    }

    public static int getCardLogo(String bankName) {
        if (TextUtils.isEmpty(bankName)) return -1;
        if ("VISA".equals(bankName.toUpperCase(Locale.ENGLISH))) {
            return R.attr.icon_visa_card;
        } else if ("MASTER".equals(bankName.toUpperCase(Locale.ENGLISH))) {
            return R.attr.icon_master_card;
        } else if ("MADA".equals(bankName.toUpperCase(Locale.ENGLISH))) {
            return R.attr.icon_mada_card;
        }
        return -1;
    }

    public static ArrayList<SelectInfoEntity> comparatorAZ(ArrayList<SelectInfoEntity> selectInfoList) {
        //a-z排序
        Collections.sort(selectInfoList, new PinyinComparator());

        return selectInfoList;
    }

    public static class PinyinComparator implements Comparator<SelectInfoEntity> {

        @Override
        public int compare(SelectInfoEntity o1, SelectInfoEntity o2) {
            if (o1.getSortLetter().equals(SPECIAL_CHAR_TWO) || o2.getSortLetter().equals(SPECIAL_CHAR_ONE)) {
                return -1;
            } else if (o1.getSortLetter().equals(SPECIAL_CHAR_ONE) || o2.getSortLetter().equals(SPECIAL_CHAR_TWO)) {
                return 1;
            } else {
                return o1.getSortLetter().compareTo(o2.getSortLetter());
            }
        }
    }

    /**
     * 根据生日获取年龄
     *
     * @param birthday
     * @return
     */
    public static int getAge(String birthday) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy");
            LocalDate dateTime = LocalDate.parse(birthday, formatter);
            LocalDate l = LocalDate.of(dateTime.getYear(), dateTime.getMonth().getValue(), dateTime.getDayOfMonth());
            LocalDate now = LocalDate.now();
            Period diff = Period.between(l, now);
            return diff.getYears();
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return 0;
    }

    public static String getShortMonth(String birthday) {
        HashMap<String, String> charMap = new HashMap<>();
        charMap.put("January", "Jan");
        charMap.put("February", "Feb");
        charMap.put("March", "Mar");
        charMap.put("April", "Apr");
        charMap.put("May", "May");
        charMap.put("June", "Jun");
        charMap.put("July", "Jul");
        charMap.put("August", "Aug");
        charMap.put("September", "Sept");
        charMap.put("October", "Oct");
        charMap.put("November", "Nov");
        charMap.put("December", "Dec");

        String monthKey = birthday.split(" ")[0];

        if (charMap.containsKey(monthKey)) {
            return birthday.replace(monthKey, charMap.get(monthKey));
        }

        return birthday;
    }

    public static boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
        }
        return false;
    }

    //跳转到交易详情页面公共方法   多处使用
    public static void jumpToTransferDetail(Activity mActivity, TransferHistoryDetailEntity transferHistoryDetailEntity, String orderType, boolean fromHistory) {
        if (transferHistoryDetailEntity != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
            cardNotifyEntity.payerAmount = transferHistoryDetailEntity.tradeAmount;
            cardNotifyEntity.paymentMethodDesc = transferHistoryDetailEntity.paymentMethod;
            cardNotifyEntity.orderNo = transferHistoryDetailEntity.orderNo;
            cardNotifyEntity.startTime = transferHistoryDetailEntity.createTime;
            cardNotifyEntity.orderDesc = transferHistoryDetailEntity.orderDesc;
            cardNotifyEntity.extendProperties = transferHistoryDetailEntity.extendProperties;
            cardNotifyEntity.orderCurrencyCode = transferHistoryDetailEntity.currencyType;
            cardNotifyEntity.orderStatus = transferHistoryDetailEntity.orderStatus;
            cardNotifyEntity.sceneType = transferHistoryDetailEntity.sceneType;
            cardNotifyEntity.orderType = transferHistoryDetailEntity.orderType;
            cardNotifyEntity.basicExtendProperties = transferHistoryDetailEntity.basicExtendProperties;
            cardNotifyEntity.logo = transferHistoryDetailEntity.logo;
            cardNotifyEntity.respMsg = transferHistoryDetailEntity.respMsg;
            cardNotifyEntity.remark = transferHistoryDetailEntity.remark;
            cardNotifyEntity.outTradeNo = transferHistoryDetailEntity.outTradeNo;
            cardNotifyEntity.originalOrderNo = transferHistoryDetailEntity.originalOrderNo;
            cardNotifyEntity.orderDirection = transferHistoryDetailEntity.orderDirection;
            cardNotifyEntity.sex = transferHistoryDetailEntity.sex;
            cardNotifyEntity.shareDesc = transferHistoryDetailEntity.shareDesc;
            cardNotifyEntity.orderAmount = transferHistoryDetailEntity.orderAmount;
            cardNotifyEntity.discountType = transferHistoryDetailEntity.discountType;
            cardNotifyEntity.popupInfo = transferHistoryDetailEntity.popupInfo;
            cardNotifyEntity.cashBackOrderNo = transferHistoryDetailEntity.cashBackOrderNo;
            cardNotifyEntity.voucherPinCode = transferHistoryDetailEntity.voucherPinCode;
            cardNotifyEntity.subSceneType = transferHistoryDetailEntity.subSceneType;
            cardNotifyEntity.isShowEInvoice = transferHistoryDetailEntity.isShowEInvoice;
            cardNotifyEntity.downloadFile = transferHistoryDetailEntity.downloadFile;
            cardNotifyEntity.invoiceType = transferHistoryDetailEntity.invoiceType;
            mHashMaps.put(Constants.sceneType, Constants.history_detail);
            mHashMaps.put(Constants.fromHistory, fromHistory);
            mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            if ("D".equals(orderType)) {
                ActivitySkipUtil.startAnotherActivity(mActivity, OrginalTransactionActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                ActivitySkipUtil.startAnotherActivity(mActivity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            }
        }
    }

    /**
     * Android 10 以上适配
     *
     * @param context
     * @param uri
     * @return
     */
    public static String getFilePathFromUri(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME};
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(uri, filePathColumn, null,
                null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            try {
                filePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
                return filePath;
            } catch (Exception e) {
                LogUtils.d(TAG, "---" + e + "---");
            } finally {
                cursor.close();
            }
        }
        return "";
    }

    /**
     * Android 10 以上适配  根据Uri获取path
     *
     * @param context
     * @param uri
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static String getPathFromUriApiQ(Context context, Uri uri) {
        File file = null;
        //android10以上转换
        if (uri.getScheme().equals(ContentResolver.SCHEME_FILE)) {
            file = new File(uri.getPath());
        } else if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //把文件复制到沙盒目录
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            if (cursor.moveToFirst()) {
                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                try {
                    InputStream is = contentResolver.openInputStream(uri);
//                    File cache = new File(context.getExternalCacheDir().getAbsolutePath(), Math.round((Math.random() + 1) * 1000) + displayName);
                    File cache = new File(context.getExternalCacheDir().getAbsolutePath(), new SecureRandom().nextInt(1000) + displayName);
                    FileOutputStream fos = new FileOutputStream(cache);
                    android.os.FileUtils.copy(is, fos);
                    file = cache;
                    fos.close();
                    is.close();
                } catch (IOException e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        }else{
            file = new File(uri.getPath());
        }
        if(file == null)return null;
        return file.getAbsolutePath();
    }

    public static File getFileFromUriApiQ(Context context, Uri uri) {
        File file = null;
        //android10以上转换
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && uri.getScheme().equals(ContentResolver.SCHEME_FILE)) {
            file = new File(uri.getPath());
        } else if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //把文件复制到沙盒目录
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            if (cursor.moveToFirst()) {
                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                try {
                    InputStream is = contentResolver.openInputStream(uri);
//                    File cache = new File(context.getExternalCacheDir().getAbsolutePath(), Math.round((Math.random() + 1) * 1000) + displayName);
                    File cache = new File(context.getExternalCacheDir().getAbsolutePath(), new SecureRandom().nextInt(1000) + displayName);
                    FileOutputStream fos = new FileOutputStream(cache);
                    android.os.FileUtils.copy(is, fos);
                    file = cache;
                    fos.close();
                    is.close();
                } catch (IOException e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }
        }else{
            file = new File(uri.getPath());
        }
        if(file == null)return null;
        return file;
    }

    /**
     * 将图片转换成Base64编码的字符串
     */
    public static String imageToBase64(String path) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
            is = new FileInputStream(path);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = android.util.Base64.encodeToString(data, android.util.Base64.DEFAULT);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }

        }
        return result;
    }

    /**
     * 将Base64编码转换为图片
     *
     * @param base64Str
     * @param path
     * @return true
     */
    public static boolean base64ToFile(String base64Str, String path) {
        byte[] data = android.util.Base64.decode(base64Str, android.util.Base64.NO_WRAP);
        for (int i = 0; i < data.length; i++) {
            if (data[i] < 0) {
                //调整异常数据
                data[i] += 256;
            }
        }
        OutputStream os = null;
        try {
            os = new FileOutputStream(path);
            os.write(data);
            os.flush();
            os.close();
            return true;
        } catch (FileNotFoundException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
            return false;
        } catch (IOException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            }catch (Exception e){
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
    }

    /*
     * bitmap转base64
     * */
    public static String bitmapToBase64(Bitmap bitmap) {
        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = android.util.Base64.encodeToString(bitmapBytes, android.util.Base64.DEFAULT);
            }
        } catch (IOException e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        return result;
    }

    public static int getSpendingColorId(int id) {
        switch (id) {
            case 1:
                return R.color.color_5081f7;
            case 2:
                return R.color.color_6AD995;
            case 3:
                return R.color.color_F9DC4A;
        }
        return R.color.color_EB5E58;
    }


    /**
     * @param countryType
     * @param localCountry getString(R.string.BillPay_4)
     * @return
     */
    public static String getChannelCode(String countryType, String localCountry) {
        if (TextUtils.isEmpty(countryType)) {
            return "";
        }

        if (TextUtils.equals(countryType, localCountry)) {
            return Constants.paybill_local;
        }

        return Constants.paybill_inter;
    }


    /**
     * 将 json对象转化为 String
     *
     * @param t
     * @param <T>
     * @return
     */
    public static <T> String jsonToString(T t) {
        Gson gson = new Gson();
        return gson.toJson(t);
    }

    /**
     * 将字符串 转换为 json对象
     *
     * @param jsonStr
     * @param t
     * @param <T>
     * @return
     */
    public static <T> T stringToJson(String jsonStr, T t) {
        try {
            Gson gson = new Gson();
            return (T) gson.fromJson(jsonStr, t.getClass());
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
                LogUtils.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    /**
     * dp转换成px
     *
     * @param dp dp
     * @return px值
     */
    public static int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * 设置指定字体高亮
     *
     * @return CharSequence型字符串
     */
    public static CharSequence getHighLightText(Context context, String text, String keyword,String fontStyle) {
        SpannableStringBuilder style = new SpannableStringBuilder(text);
        if (!TextUtils.isEmpty(keyword)) {
            int base = 0;//基准index，表示每一次进行字符串截取之后，新字符字符串的开始index相对于text原始字符串的位置
            int start;
            do {
                Log.i("getHighLightText", "现在的text：" + text);
                start = text.indexOf(keyword);
                int end;
                if (start >= 0) {
                    end = start + keyword.length();
                    style.setSpan(
                            new ForegroundColorSpan(ContextCompat.getColor(context, R.color.color_fc4f00)),
                            base + start, base + end,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if(!TextUtils.isEmpty(fontStyle)) {
                        style.setSpan(new TypefaceSpan(Typeface.createFromAsset(context.getAssets(), fontStyle)),
                                base + start, base + end,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    text = text.substring(end);
                    base += end;
                }
            } while (start >= 0 && text.length() > 0);
        }
        return style;
    }

    public static String validateFilenameInDir(String filename, String
            intendedDirectory) throws IOException{
        File checkFile = new File(filename);
        String canonicalPathToCheck = checkFile.getCanonicalPath();
        File intendedDir = new File(intendedDirectory);
        String canonicalPathToVerify = intendedDir.getCanonicalPath();
        if (canonicalPathToCheck.startsWith(canonicalPathToVerify)){
            return canonicalPathToCheck;
        } else{
            throw new IllegalStateException("This file is outside the intended extraction directory.");
        }
    }

    public static void jumpToIvrActivity(Activity activity, RiskControlEntity riskControlEntity,HashMap<String, Object> hashMap){
        if(riskControlEntity.isNeedIvr()) {
            ActivitySkipUtil.startAnotherActivity(activity, IvrRequestActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    //跳转到充值页面
    public static void jumpToAddMoneyActivity(Activity mActivity){
        ActivitySkipUtil.startAnotherActivity(mActivity, SelectTransferTypeActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
    }

    /**
     * 是否关闭了该模块功能
     * @param key
     * @return
     */
    public static boolean isCloseUse(Activity mActivity,String key){
        UserUnableUseEntity userUnableUseEntity = SpUtils.getInstance().getUserUnableUseEntity();
        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if(userInfoEntity == null)return false;
        String lockAcc = userInfoEntity.lockAcc;
//        String lockAcc = "A";
        String lockReActive = userInfoEntity.lockReActive;
        if(userUnableUseEntity != null) {
            List<String> userAppFuncCodeList = userUnableUseEntity.userDisableAppFuncCodeList;
            if (userAppFuncCodeList.contains(key)) {
                if(!"A".equals(lockAcc)){
                    showAccountStatusDialog(mActivity, lockAcc, new CommonNoticeDialog.CommonConfirmListener() {
                        @Override
                        public void commonConfirm(int type) {
                            if("E".equals(lockAcc)){
                                AppClient.getInstance().getUserManager().renewId(userInfoEntity.callingCode, userInfoEntity.phone, lockAcc, new ResultCallback<ResultEntity>() {
                                    @Override
                                    public void onResult(ResultEntity response) {
                                        dismissNoticeDialog();
                                        if(response != null){
                                            String result = response.result;
                                            showActivateSuccessDialog(mActivity, lockAcc, new CommonNoticeDialog.CommonConfirmListener() {
                                                @Override
                                                public void commonConfirm(int type) {
                                                    dismissNoticeDialog();
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(String errorCode, String errorMsg) {

                                    }
                                });
                            }else if("Y".equals(lockReActive)){
                                AppClient.getInstance().getUserManager().activateUserStatus(userInfoEntity.callingCode, userInfoEntity.phone, lockAcc, new ResultCallback<Void>() {
                                    @Override
                                    public void onResult(Void response) {
                                        dismissNoticeDialog();
                                        showActivateSuccessDialog(mActivity, lockAcc, new CommonNoticeDialog.CommonConfirmListener() {
                                            @Override
                                            public void commonConfirm(int type) {
                                                switch (lockAcc){
                                                    case "S":
                                                    case "E":
                                                        dismissNoticeDialog();
                                                        break;
                                                    case "D":
                                                    case "U":
                                                        ((BaseCompatActivity)mActivity).contactHelp();
                                                        break;
                                                    default:break;
                                                }
                                            }
                                        });
                                    }

                                    @Override
                                    public void onFailure(String errorCode, String errorMsg) {

                                    }
                                });
                            }
                        }
                    });
                }else if(userInfoEntity.isFreezeAccount() || userInfoEntity.isBlockAccount()){
                    showCommonOneButtonDialog(mActivity,mActivity.getString(userInfoEntity.isFreezeAccount()?R.string.sprint20_103:R.string.sprint20_106), ThemeSourceUtils.getSourceID(mActivity, R.attr.icon_home_account_freeze), mActivity.getString(R.string.sprint20_104) + "\n"
                            + mActivity.getString(R.string.sprint20_105), mActivity.getString(R.string.sprint20_63), new ExpireIdDialog.RenewIdListener() {
                        @Override
                        public void clickRenew() {
                            ((BaseCompatActivity)mActivity).contactHelp();
                        }
                    });
                }else {
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("title", "");
                    ActivitySkipUtil.startAnotherActivity(mActivity, ComingSoonActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                return true;
            }
        }
        return false;
    }
    static ExpireIdDialog oneButtonBottomDialog;
    protected static void showCommonOneButtonDialog(Context mContext,String title,int imgId,String content,String confirmText,ExpireIdDialog.RenewIdListener renewIdListener){
        oneButtonBottomDialog = new ExpireIdDialog(mContext);
        oneButtonBottomDialog.setTitle(title);
        oneButtonBottomDialog.setImage(imgId);
        oneButtonBottomDialog.setContent(content);
        oneButtonBottomDialog.setConfirmText(confirmText);
        oneButtonBottomDialog.setCloseListener(renewIdListener);
        oneButtonBottomDialog.showWithBottomAnim();
    }

    protected static void dismissNoticeDialog(){
        if(commonNoticeDialog != null && commonNoticeDialog.isShowing()){
            commonNoticeDialog.dismiss();
        }
    }
    static CommonNoticeDialog commonNoticeDialog;
    protected static void showActivateSuccessDialog(Context mContext, String lockAcc, CommonNoticeDialog.CommonConfirmListener commonConfirmListener){
        commonNoticeDialog = new CommonNoticeDialog(mContext);
        String title = "",content = "",confirmContent = "";
        int imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);;
        switch (lockAcc){
            //非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
            case "S":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_112);
                confirmContent = mContext.getString(R.string.sprint11_113);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_reactivated);
                break;
            case "D":
            case "U":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_119);
                confirmContent = mContext.getString(R.string.sprint11_120);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_ivr_call);
                break;
            case "E":
                title = mContext.getString(R.string.sprint11_111);
                content = mContext.getString(R.string.sprint11_126);
                confirmContent = mContext.getString(R.string.sprint11_113);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_has_renew);
                break;
            case "W":
                //Id更新失败或者处理中
                title = mContext.getString(R.string.sprint11_127);
                content = mContext.getString(R.string.sprint11_128);
                confirmContent = "";
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_not_renew);
                break;
            default:break;
        }
        commonNoticeDialog.setCommonText(title,content,confirmContent,imgId);
        commonNoticeDialog.setCommonConfirmListener(commonConfirmListener);
        commonNoticeDialog.showWithBottomAnim();
    }

    protected static void showAccountStatusDialog(Context mContext,String lockAcc,CommonNoticeDialog.CommonConfirmListener commonConfirmListener){
        commonNoticeDialog = new CommonNoticeDialog(mContext);
        String title = "",content = "",confirmContent = "";
        int imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);;
        switch (lockAcc){
            //非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
            case "S":
                title = mContext.getString(R.string.sprint11_108);
                content = mContext.getString(R.string.sprint11_109);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);
                break;
            case "D":
                title = mContext.getString(R.string.sprint11_117);
                content = mContext.getString(R.string.sprint11_118);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_dormant);
                break;
            case "C":
                title = mContext.getString(R.string.sprint11_114);
                content = mContext.getString(R.string.sprint11_115);
                confirmContent = mContext.getString(R.string.sprint11_116);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_closed);
                break;
            case "U":
                title = mContext.getString(R.string.sprint11_121);
                content = mContext.getString(R.string.sprint11_122);
                confirmContent = mContext.getString(R.string.sprint11_110);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_inactive);
                break;
            case "E":
                /**
                 * {\"codeType\":0,\"partnerNo\":\"96601\",\"requestCounts\":0,\"lockAcc\":\"E\"}"
                 */
                title = mContext.getString(R.string.sprint11_123);
                content = mContext.getString(R.string.sprint11_124);
                confirmContent = mContext.getString(R.string.sprint11_125);
                imgId = ThemeSourceUtils.getSourceID(mContext,R.attr.icon_account_expired);
                break;
            default:break;
        }
        commonNoticeDialog.setCommonText(title,content,confirmContent,imgId);
        commonNoticeDialog.setCommonConfirmListener(commonConfirmListener);
        commonNoticeDialog.showWithBottomAnim();
    }

    public static InputFilter getEmjoFilter() {
        //表情过滤器
        InputFilter emojiFilter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
                Matcher emojiMatcher = emoji.matcher(source);
                if (emojiMatcher.find()) {
                    return "";
                }
                return null;
            }
        };
        return emojiFilter;
    }

    public static HashMap<String,String> getLanguageMap(LanguageCodeEntity languageCodeEntity){
        HashMap<String,String> languageMap = new HashMap<>();
        String filePath = ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File file = new File(filePath);
        File[] filenameList = file.listFiles();
        InputStream inputs = null;
        for (int i = 0; i < filenameList.length; i++) {
            String filename = filenameList[i].getName();
            if (!TextUtils.isEmpty(filename) && filename.contains(languageCodeEntity.key)) {
                try {
                    FileInputStream fis = new FileInputStream(filenameList[i]);
                    byte[] buffer = new byte[fis.available()];
                    fis.read(buffer);
                    inputs = new ByteArrayInputStream(buffer);
                    fis.close();
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
                try {
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    XmlPullParser xmlPullParser = factory.newPullParser();
                    xmlPullParser.setInput(inputs, "UTF-8");
                    int eventType = xmlPullParser.getEventType();

                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        String nodeName = xmlPullParser.getName();
                        switch (eventType) {

                            case XmlPullParser.START_DOCUMENT:
                                break;

                            case XmlPullParser.START_TAG:
                                if("string".equals(nodeName)){
                                    String b = xmlPullParser.getAttributeValue("","name");
                                    String s =  xmlPullParser.nextText();
                                    b = b.replace("-","_");
                                    s = s.replace("\\n","\n");
//                                    LogUtils.d("errorMsg", "---"+s+"---");
                                    languageMap.put(b,s);
                                }
                                break;
                            case XmlPullParser.END_TAG:
                                break;
                        }
                        eventType = xmlPullParser.next();
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }
        return languageMap;
    }

    /**
     * 调用系统界面，给指定的号码发送短信，并附带短信内容
     *
     * @param context
     * @param number
     * @param body
     */
    public static void sendSmsWithBody(Context context, String number, String body) {
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(Uri.parse("smsto:" + number));
        sendIntent.putExtra("sms_body", body);
        context.startActivity(sendIntent);
    }

    public static List<NumberEntity> getRegNumberList(Context context){
        List<NumberEntity> numberEntityList = new ArrayList<>();
        numberEntityList.add(new NumberEntity("1",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_1)));
        numberEntityList.add(new NumberEntity("2",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_2)));
        numberEntityList.add(new NumberEntity("3",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_3)));
        numberEntityList.add(new NumberEntity("4",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_4)));
        numberEntityList.add(new NumberEntity("5",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_5)));
        numberEntityList.add(new NumberEntity("6",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_6)));
        numberEntityList.add(new NumberEntity("7",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_7)));
        numberEntityList.add(new NumberEntity("8",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_8)));
        numberEntityList.add(new NumberEntity("9",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_9)));
        numberEntityList.add(new NumberEntity("",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_10)));
        numberEntityList.add(new NumberEntity("0",1,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_0)));
        numberEntityList.add(new NumberEntity("",2,ThemeSourceUtils.getSourceID(context,R.attr.icon_number_delete)));
        return numberEntityList;
    }

    /**
     * 打印日志并输出到本地功能
     * @param logMessage
     */
    public static void writeLogToFile(String logMessage) {
        logMessage = logMessage+"\n";
        String fileName = "tiqmo_log.txt";
        String logFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + "/" + fileName;
        try {
            FileOutputStream outputStream = new FileOutputStream(new File(logFilePath), true);
            outputStream.write(logMessage.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getAvatarUrl(String url){
        if(!TextUtils.isEmpty(url) && url.contains("&")){
            url = url.split("&")[0];
        }
        return url;
    }

}
