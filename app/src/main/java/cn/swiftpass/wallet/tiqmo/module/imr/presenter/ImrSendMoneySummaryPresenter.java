package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import cn.swiftpass.wallet.tiqmo.module.imr.contract.ImrSendMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ImrSendMoneySummaryPresenter implements ImrSendMoneyContract.SummaryPresenter {

    ImrSendMoneyContract.SummaryView mView;

    @Override
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType) {
        AppClient.getInstance().confirmPay(orderNo, paymentMethodNo, orderType, new LifecycleMVPResultCallback<TransferEntity>(mView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mView.confirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.confirmPayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrCheck(String orderNo) {
        AppClient.getInstance().imrCheck(orderNo, new LifecycleMVPResultCallback<Void>(mView, true) {
            @Override
            protected void onSuccess(Void result) {
                mView.imrCheckSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.imrCheckFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(ImrSendMoneyContract.SummaryView summaryView) {
        this.mView = summaryView;
    }

    @Override
    public void detachView() {
        this.mView = null;
    }
}
