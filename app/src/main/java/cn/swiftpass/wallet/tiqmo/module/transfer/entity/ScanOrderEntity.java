package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ScanOrderEntity extends BaseEntity {

    /**
     * 扫码获取订单信息
     */
    public OrderInfoEntity orderInfo;

    /**
     * 扫码获取用户信息
     */
    public UserInfoEntity scanUserInfo;

    /**
     * o-订单，m-合法商城html，h-合法html，s-合法字符串，i-非法内容 c-转账收款订单
     */
    public String type;
}
