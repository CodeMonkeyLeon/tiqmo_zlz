package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by YZX on 2018年12月19日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class DecryptedResponse {
    private String code;
    private String msg;
    private String content;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
