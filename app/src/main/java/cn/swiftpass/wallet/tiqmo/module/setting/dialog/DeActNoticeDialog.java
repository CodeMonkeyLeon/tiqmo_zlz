package cn.swiftpass.wallet.tiqmo.module.setting.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;

public class DeActNoticeDialog extends BaseDialogFragmentN {

    private static DeActNoticeDialog dialog;
    private TextView backHome;
    private OnBackToHomeClickListener listener;

    public void setBackToHomeClickListener(OnBackToHomeClickListener listener) {
        this.listener = listener;
    }

    private DeActNoticeDialog() {}

    public static DeActNoticeDialog getInstance() {
        if (dialog == null) {
            dialog = new DeActNoticeDialog();
        }
        return dialog;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_de_act_notice;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        backHome = view.findViewById(R.id.tv_back_to_home);
        backHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onBTHClick();
                }
            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        listener = null;
    }

    public interface OnBackToHomeClickListener {
        void onBTHClick();
    }
}
