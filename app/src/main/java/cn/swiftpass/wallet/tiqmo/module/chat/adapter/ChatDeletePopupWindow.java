package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;

public class ChatDeletePopupWindow extends PopupWindow
{

    private OnChatDeleteClickListener onChatDeleteClickListener;

    private Context context;
    private MessageEntity messageEntity;

    private View view;

    public interface OnChatDeleteClickListener
    {
        void onChatReply(MessageEntity messageEntity);
        void onChatDeleteMe(MessageEntity messageEntity);
        void onChatDeleteOther(MessageEntity messageEntity);
        void onChatReport(MessageEntity messageEntity);
    }

    public void setOnChatDeleteClickListener(final OnChatDeleteClickListener onChatDeleteClickListener) {
        this.onChatDeleteClickListener = onChatDeleteClickListener;
    }

    public ChatDeletePopupWindow(Context context,MessageEntity messageEntity)
    {
        this.context = context;
        this.messageEntity = messageEntity;
        initViews();
    }

    public View getView(){
        return view;
    }

    private void initViews() {
        view = View.inflate(context, R.layout.view_chat_delete, null);
        TextView tvReply = (TextView)view.findViewById(R.id.tv_chat_reply);
        TextView tvDeleteMe = (TextView)view.findViewById(R.id.tv_chat_delete_me);
        TextView tvDeleteOther = (TextView)view.findViewById(R.id.tv_chat_delete_other);
        TextView tvChatReport = (TextView)view.findViewById(R.id.tv_chat_report);
        if(System.currentTimeMillis() - messageEntity.getTimestamp() > 2000*60){
            tvDeleteOther.setVisibility(View.GONE);
        }else{
            tvDeleteOther.setVisibility(View.VISIBLE);
        }
        tvReply.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onChatDeleteClickListener != null){
                    onChatDeleteClickListener.onChatReply(messageEntity);
                }
            }
        });

        tvDeleteMe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onChatDeleteClickListener != null){
                    onChatDeleteClickListener.onChatDeleteMe(messageEntity);
                }
            }
        });
        tvDeleteOther.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onChatDeleteClickListener != null){
                    onChatDeleteClickListener.onChatDeleteOther(messageEntity);
                }
            }
        });
        tvChatReport.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onChatDeleteClickListener != null){
                    onChatDeleteClickListener.onChatReport(messageEntity);
                }
            }
        });
        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setContentView(view);
        setAnimationStyle(android.R.style.Animation_Dialog);
        setBackgroundDrawable(context.getResources().getDrawable(android.R.color.transparent));
        setFocusable(true); // 设置PopupWindow可获得焦点
        setTouchable(true); // 设置PopupWindow可触摸
        setOutsideTouchable(true); // 设置非PopupWindow区域可触摸
    }
}

