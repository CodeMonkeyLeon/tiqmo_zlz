package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrExchangeRateEntity extends BaseEntity {

    public String partnerNo;
    //汇率
    public String exchangeRate = "0";
    //目标币种
    public String destinationCurrencyCode;
    //源币种
    public String originatingCurrencyCode;
    //原金额，默认为1
    public String orginatingAmount = "0";
    //目标金额
    public String destinationAmount = "0";
    //目标金额最小值
    public String minDestinationAmount = "0";
    //目标金额最大值
    public String maxDestinationAmount = "0";
    //目标金额小数位数
    public String destinationAmountDecimalPoints;
    //手续费
    public String charges = "0";
    //增值税
    public String vatCharges = "0";
    //Y-可切换是否包含，N-不可切换
    public String chargesSwitchFlag;
    //Y-包含，N-不包含
    public String chargesDefaultFlag;

    //是否动态计算  Y/N,提示客户端在App输入金额变化后需要重新请求接口动态计算某些金额（手续费/税费/目标金额）
    public String dynamicCalculation;
}
