package cn.swiftpass.wallet.tiqmo.module.imr;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

/**
 * des:水平滑动的视图
 */
public class HorizontalSlidingItemView extends FrameLayout {

    private Context context;
    private FrameLayout mainLayout;// 主View
    private LinearLayout llDelete;// 删除按钮
    private LinearLayout llBeneficiaryOp;// 操作按钮
    private LinearLayout llBeneficiaryInfo;// 受益人信息
    private LinearLayout llBeneficiaryEdit;// 编辑按钮
    private LinearLayout llBeneficiarySend;// 发送按钮
    private TextView tvBeneficiarySend;
    private ImageView ivBeneficiarySend;

    private int defDeleteWidth = 57;
    private int defSliding = 20;

    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public HorizontalSlidingItemView(Context context) {
        this(context, null);
    }

    public HorizontalSlidingItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalSlidingItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initView();
    }

    void initView() {
        mainLayout = (FrameLayout) FrameLayout.inflate(context, R.layout.horizontal_sliding_item_view, null);
        this.addView(mainLayout);

        llDelete = mainLayout.findViewById(R.id.ll_delete);
        llBeneficiaryOp = mainLayout.findViewById(R.id.ll_beneficiary_op);
        llBeneficiaryInfo = mainLayout.findViewById(R.id.ll_beneficiary_info);
        llBeneficiaryEdit = mainLayout.findViewById(R.id.ll_beneficiary_edit);
        llBeneficiarySend = mainLayout.findViewById(R.id.ll_beneficiary_send);
        tvBeneficiarySend = mainLayout.findViewById(R.id.tv_beneficiary_send);
        ivBeneficiarySend = mainLayout.findViewById(R.id.iv_beneficiary_send);
    }

    public void setTvBeneficiarySend(int colorId,String send) {
        tvBeneficiarySend.setTextColor(context.getColor(colorId));
        this.tvBeneficiarySend.setText(send);
    }

    public void setIvBeneficiarySend(final int resId) {
        this.ivBeneficiarySend.setBackgroundResource(resId);
    }

    float oldX = -1;
    float diffX;
    float oldTouch = -1;
    boolean isDeleteShow = false;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                LogUtils.e("HorizontalSlidingItemView",
                        "ACTION_DOWN: ev.getX(): " + ev.getX() + " -ev.getY(): " + ev.getY());
                oldX = ev.getX();
                oldTouch = ev.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                if (LocaleUtils.isRTL(context)) {
                    if (isDeleteShow && diffX > 0) {
                        return false;
                    }
                    diffX = ev.getX() - oldX;
                    if (isDeleteShow) {
                        llBeneficiaryInfo.setTranslationX(Math.max(0, Math.min((AndroidUtils.dip2px(context, defDeleteWidth) + diffX),
                                AndroidUtils.dip2px(context, defDeleteWidth))));
                        llBeneficiaryOp.setTranslationX(Math.max(0, Math.min((AndroidUtils.dip2px(context, defDeleteWidth) + diffX),
                                AndroidUtils.dip2px(context, defDeleteWidth))));
                    } else {
                        llBeneficiaryInfo.setTranslationX(Math.max(0, Math.min(diffX, AndroidUtils.dip2px(context, defDeleteWidth))));
                        llBeneficiaryOp.setTranslationX(Math.max(0, Math.min(diffX, AndroidUtils.dip2px(context, defDeleteWidth))));
                    }
                } else {
                    if (isDeleteShow && diffX < 0) {
                        return false;
                    }
                    diffX = ev.getX() - oldX;
                    if (isDeleteShow) {
                        llBeneficiaryInfo.setTranslationX(Math.min(0, Math.max(-(AndroidUtils.dip2px(context, defDeleteWidth) - diffX),
                                -AndroidUtils.dip2px(context, defDeleteWidth))));
                        llBeneficiaryOp.setTranslationX(Math.min(0, Math.max(-(AndroidUtils.dip2px(context, defDeleteWidth) - diffX),
                                -AndroidUtils.dip2px(context, defDeleteWidth))));
                    } else {
                        llBeneficiaryInfo.setTranslationX(Math.min(0, Math.max(diffX, -AndroidUtils.dip2px(context, defDeleteWidth))));
                        llBeneficiaryOp.setTranslationX(Math.min(0, Math.max(diffX, -AndroidUtils.dip2px(context, defDeleteWidth))));
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (Math.abs(ev.getX() - oldTouch) < 2) { // 点击事件
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) llBeneficiaryInfo.getLayoutParams();
                    float pointX = ev.getX() - params.leftMargin;
                    float pointY = ev.getY() - params.topMargin;

                    if (pointX >= 0 && pointX <= llBeneficiaryInfo.getWidth() && pointY >= 0 &&
                            pointY <= llBeneficiaryInfo.getHeight()) { // item区域
                        if (isDeleteShow) {
                            if (LocaleUtils.isRTL(context) ? pointX < AndroidUtils.dip2px(context, defDeleteWidth) :
                                    pointX > llBeneficiaryInfo.getWidth() - AndroidUtils.dip2px(context, defDeleteWidth)) {
                                if (onClickListener != null) {
                                    onClickListener.onDeleteClick();
                                }
                            } else {
                                llBeneficiaryInfo.animate().translationX(0);
                                llBeneficiaryOp.animate().translationX(0);
                                isDeleteShow = false;
                                if (onClickListener != null) {
                                    onClickListener.onDeleteShow(false);
                                }
                            }
                        } else {
                            if (llBeneficiaryOp.getVisibility() == GONE) {
                                llBeneficiaryOp.setVisibility(VISIBLE);
                                llBeneficiaryInfo.setElevation(getResources().getDimension(R.dimen.dimen_elevation));
                            } else {
                                llBeneficiaryOp.setVisibility(GONE);
                                llBeneficiaryInfo.setElevation(0);
                            }
                            if (onClickListener != null) {
                                onClickListener.onBenOPShow(llBeneficiaryOp.getVisibility() == VISIBLE);
                            }
                        }
                    } else if (llBeneficiaryOp.getVisibility() == VISIBLE) { // 操作按钮区域
                        if (isDeleteShow) {
                            if (LocaleUtils.isRTL(context) ? pointX < AndroidUtils.dip2px(context, defDeleteWidth) :
                                    pointX > llBeneficiaryInfo.getWidth() - AndroidUtils.dip2px(context, defDeleteWidth)) {
                                if (onClickListener != null) {
                                    onClickListener.onDeleteClick();
                                    return true;
                                }
                            }
                        }
                        if (pointX >= 0 && pointX <= (isDeleteShow ? (llBeneficiaryInfo.getWidth() >> 1)
                                + (LocaleUtils.isRTL(context) ? AndroidUtils.dip2px(context, defDeleteWidth)
                                : - AndroidUtils.dip2px(context, defDeleteWidth)):
                                llBeneficiaryInfo.getWidth() >> 1) && pointY > llBeneficiaryInfo.getHeight()
                                && pointY <= llBeneficiaryOp.getBottom()) { // edit按钮
                            if (onClickListener != null) {
                                if (LocaleUtils.isRTL(context)) {
                                    onClickListener.onSendClick();
                                } else {
                                    onClickListener.onEditClick();
                                }
                            }
                        } else if (pointX >= (isDeleteShow ? (llBeneficiaryInfo.getWidth() >> 1)
                                + (LocaleUtils.isRTL(context) ? AndroidUtils.dip2px(context, defDeleteWidth)
                                : - AndroidUtils.dip2px(context, defDeleteWidth) ):
                                llBeneficiaryInfo.getWidth() >> 1) && pointX <= llBeneficiaryInfo.getWidth()
                                && pointY > llBeneficiaryInfo.getHeight() && pointY <= llBeneficiaryOp.getBottom()) { // send按钮区域
                            if (onClickListener != null) {
                                if (LocaleUtils.isRTL(context)) {
                                    onClickListener.onEditClick();
                                } else {
                                    onClickListener.onSendClick();
                                }
                            }
                        }
                    }
                    return super.onTouchEvent(ev);
                }
                resetView();
                return true;
            default:
                break;
        }
        return true;
    }

    private void resetView() {
        if (LocaleUtils.isRTL(context)) {
            if (isDeleteShow) {
                llBeneficiaryInfo.animate().translationX(diffX > -AndroidUtils.dip2px(context, defSliding)
                        ? AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                llBeneficiaryOp.animate().translationX(diffX > -AndroidUtils.dip2px(context, defSliding)
                        ? AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                isDeleteShow = diffX > -AndroidUtils.dip2px(context, defSliding);
            } else {
                llBeneficiaryInfo.animate().translationX(diffX > AndroidUtils.dip2px(context, defSliding)
                        ? AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                llBeneficiaryOp.animate().translationX(diffX > AndroidUtils.dip2px(context, defSliding)
                        ? AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                isDeleteShow = diffX > AndroidUtils.dip2px(context, defSliding);
            }
        } else {
            if (isDeleteShow) {
                llBeneficiaryInfo.animate().translationX(diffX < AndroidUtils.dip2px(context, defSliding)
                        ? -AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                llBeneficiaryOp.animate().translationX(diffX < AndroidUtils.dip2px(context, defSliding)
                        ? -AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                isDeleteShow = diffX < AndroidUtils.dip2px(context, defSliding);
            } else {
                llBeneficiaryInfo.animate().translationX(diffX < -AndroidUtils.dip2px(context, defSliding)
                        ? -AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                llBeneficiaryOp.animate().translationX(diffX < -AndroidUtils.dip2px(context, defSliding)
                        ? -AndroidUtils.dip2px(context, defDeleteWidth) : 0);
                isDeleteShow = diffX < -AndroidUtils.dip2px(context, defSliding);
            }
        }
        if (onClickListener != null) {
            onClickListener.onDeleteShow(isDeleteShow);
        }
        diffX = 0;
    }

    public void resetRemoveIcon() {
        llBeneficiaryInfo.animate().translationX(0);
        llBeneficiaryOp.animate().translationX(0);
        isDeleteShow = false;
    }

    public void showRemoveIcon() {
        llBeneficiaryInfo.setTranslationX(LocaleUtils.isRTL(context) ?
                AndroidUtils.dip2px(context, defDeleteWidth) : -AndroidUtils.dip2px(context, defDeleteWidth));
        llBeneficiaryOp.setTranslationX(LocaleUtils.isRTL(context) ?
                AndroidUtils.dip2px(context, defDeleteWidth) : -AndroidUtils.dip2px(context, defDeleteWidth));
        isDeleteShow = true;
    }

    public void showBeneficiaryOp(boolean isShowOp) {
        llBeneficiaryOp.setVisibility(isShowOp ? VISIBLE : GONE);
        llBeneficiaryInfo.setElevation(isShowOp ? getResources().getDimension(R.dimen.dimen_elevation) : 0);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = (int) ev.getX();
                startY = (int) ev.getY();
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                int dX = (int) (ev.getX() - startX);
                int dY = (int) (ev.getY() - startY);
                if (Math.abs(dX) < 3 && Math.abs(dY) < 3) {
                    return false;
                }
                if (Math.abs(dX) + 50 > Math.abs(dY)) {//左右滑动
                    if (dX < 0 && isDeleteShow) {
                        getParent().requestDisallowInterceptTouchEvent(false);
                    } else {
                        getParent().requestDisallowInterceptTouchEvent(true);
                    }

                } else {//上下滑动
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_CANCEL:
                resetView();
                break;
            default:
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    private int startX;
    private int startY;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = (int) ev.getX();
                startY = (int) ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:

                int dX = (int) (ev.getX() - startX);
                int dY = (int) (ev.getY() - startY);
                if (Math.abs(dX) < 3 && Math.abs(dY) < 3) {
                    return false;
                }
                if (Math.abs(dX) + 50 > Math.abs(dY)) {//左右滑动
                    return false;
                } else {//上下滑动
                    return true;
                }
            case MotionEvent.ACTION_UP:
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public interface OnClickListener {
        void onDeleteClick();

        void onEditClick();

        void onSendClick();

        void onDeleteShow(boolean isShow);

        void onBenOPShow(boolean isShow);
    }
}
