package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity

import java.io.Serializable

data class CardTypePointEntity(
    val textId: Int,
    val priceText: String? = "",
    val isStandardCard: Boolean = true,
    var isSelect: Boolean = false
) : Serializable
