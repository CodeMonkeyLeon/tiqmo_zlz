package cn.swiftpass.wallet.tiqmo.module.chat.Presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.chat.Contract.ChatContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ChatPresenter implements ChatContract.ChatPresenter {

    private ChatContract.ChatView mView;

    @Override
    public void attachView(ChatContract.ChatView chatView) {
        this.mView = chatView;
    }

    @Override
    public void detachView() {
        mView = null;
    }

    @Override
    public void contactInvite(List<KycContactEntity> contactInviteDtos) {
        for (int i = 0; i < contactInviteDtos.size(); i++) {
            contactInviteDtos.get(i).setCallingCode("+966");
            contactInviteDtos.get(i).setPhone(contactInviteDtos.get(i).getPhone().substring(5));
        }
        AppClient.getInstance().contactInvite(contactInviteDtos, new LifecycleMVPResultCallback<ContactInviteEntity>(mView) {
            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView != null) {
                    mView.contactInviteFail(errorCode,errorMsg);
                }
            }

            @Override
            protected void onSuccess(ContactInviteEntity result) {
                if (mView != null) {
                    mView.contactInviteSuccess(result);
                }
            }
        });
    }

}
