package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class KycProgressDialog extends Dialog {
    private Context context = null;
    private static final String TAG = "CustomProgressDialog";

    /**
     * dialog 是否可以点击back按键退出
     *
     * @param cancel
     */
    public void setCancel(boolean cancel) {
        this.isCancel = cancel;
    }

    private boolean isCancel = true;
    private static KycProgressDialog customProgressDialog = null;

    public KycProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    public KycProgressDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }


    public static KycProgressDialog createDialog(Context context, String message) {
        customProgressDialog = new KycProgressDialog(context, R.style.CustomProgressDialog);
        customProgressDialog.setContentView(R.layout.view_kyc_loading);
        customProgressDialog.getWindow().getAttributes().gravity = Gravity.TOP;
        //去掉遮罩层（全透明）
        customProgressDialog.getWindow().setDimAmount(0.7f);
        return customProgressDialog;
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (customProgressDialog == null) {
            return;
        }

//        ImageView imageView = customProgressDialog.findViewById(R.id.loadingImageView);
//        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
//        animationDrawable.start();
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }

        }
    }

    /**
     * 动态判断dialog 点击back按键是否可以取消
     *
     * @param event
     * @return
     */
    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        LogUtils.i(TAG, "isCancel:" + isCancel);
        if (!isCancel) {
            return false;
        } else {
            customProgressDialog.dismiss();
        }
        return true;
    }
}
