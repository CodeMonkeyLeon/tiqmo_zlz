package cn.swiftpass.wallet.tiqmo.base.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitBillPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class TransferSummaryPresenter implements BaseTransferContract.BaseTransferPresenter{

    private BaseTransferContract.BaseTransferView mBaseTransferView;

    @Override
    public void getKsaCardDetails(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaCardDetails(proxyCardNo, new LifecycleMVPResultCallback<CardDetailEntity>(mBaseTransferView,true) {
            @Override
            protected void onSuccess(CardDetailEntity result) {
                mBaseTransferView.getKsaCardDetailsSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire) {
        AppClient.getInstance().getCardManager().activateCard(proxyCardNo, cardNo, cardExpire, new LifecycleMVPResultCallback<Void>(mBaseTransferView,true) {
            @Override
            protected void onSuccess(Void result) {
                mBaseTransferView.activateCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardLimit(String txnType,  String dailyLimitValue, String monthlyLimitValue, String proxyCardNo,String up) {
        AppClient.getInstance().getCardManager().setKsaCardLimit(txnType, dailyLimitValue, monthlyLimitValue, proxyCardNo,up, new LifecycleMVPResultCallback<Void>(mBaseTransferView,true) {
            @Override
            protected void onSuccess(Void result) {
                mBaseTransferView.setKsaCardLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardStatus(String status, String proxyCardNo, String reason,String reasonStatus) {
        AppClient.getInstance().getCardManager().setKsaCardStatus(status, proxyCardNo, reason,reasonStatus, new LifecycleMVPResultCallback<Void>(mBaseTransferView,true) {
            @Override
            protected void onSuccess(Void result) {
                mBaseTransferView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaCardPayResult(String vat, String totalAmount, OpenCardReqEntity appNiOpenCardReq) {
        AppClient.getInstance().getCardManager().getKsaCardPayResult(vat, totalAmount, appNiOpenCardReq, new LifecycleMVPResultCallback<KsaPayResultEntity>(mBaseTransferView,true) {
            @Override
            protected void onSuccess(KsaPayResultEntity result) {
                mBaseTransferView.getKsaCardPayResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.getKsaCardPayResultFail(errorCode,errorMsg);
            }
        });
    }



    @Override
    public void attachView(BaseTransferContract.BaseTransferView view) {
        mBaseTransferView = view;
    }

    @Override
    public void detachView() {
        mBaseTransferView = null;
    }


    @Override
    public void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount, String exchangeRate, String transCurrencyCode, String transFees, String vat) {
        AppClient.getInstance().transferSurePay(sceneType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat, new LifecycleMVPResultCallback<TransferEntity>(mBaseTransferView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mBaseTransferView.transferSurePaySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mBaseTransferView.transferSurePayFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void transferToContact(String payerUserId, String callingCode, String payerNum, String payeeUserId, String payeeNum, String payeeAmount, String payeeCurrencyCode, String remark, String sceneType, String transTimeType, String payeeNumberType, String transferPurpose, String transFees, String vat, String payerName) {
        AppClient.getInstance().transferToContact(payerUserId, callingCode, payerNum, payeeUserId, payeeNum, payeeAmount, payeeCurrencyCode, remark,
                sceneType, transTimeType, payeeNumberType, transferPurpose, transFees, vat, payerName, new LifecycleMVPResultCallback<TransferEntity>(mBaseTransferView, true) {
                    @Override
                    protected void onSuccess(TransferEntity result) {
                        mBaseTransferView.transferToContactSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mBaseTransferView.transferToContactFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void confirmPay(String orderNo, String paymentMethodNo, String orderType) {
        AppClient.getInstance().confirmPay(orderNo, paymentMethodNo, orderType, new LifecycleMVPResultCallback<TransferEntity>(mBaseTransferView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mBaseTransferView.confirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.confirmPayFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void splitBill(String oldOrderNo, String receivableAmount, String currencyCode, String orderRemark, List<SplitBillPayerEntity> receiverInfoList) {
        AppClient.getInstance().splitBill(oldOrderNo, receivableAmount, currencyCode, orderRemark, receiverInfoList, new LifecycleMVPResultCallback<TransferEntity>(mBaseTransferView, true) {
            @Override
            protected void onSuccess(TransferEntity result) {
                mBaseTransferView.splitBillSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseTransferView.splitBillFail(errorCode, errorMsg);
            }
        });
    }


    @Override
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName, String nickName, String relationshipCode, String callingCode, String phone, String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace, String birthDate, String sex, String cityName, String districtName, String poBox, String buildingNo, String street, String idNo, String idExpiry, String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag, String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode, String channelPayeeId, String channelCode,String branchId) {
        AppClient.getInstance().imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag,receiptOrgName,
                receiptOrgBranchName,cityId,currencyCode,channelPayeeId,channelCode,branchId, new LifecycleMVPResultCallback<ImrAddBeneResultEntity>(mBaseTransferView, true) {
                    @Override
                    protected void onSuccess(ImrAddBeneResultEntity result) {
                        mBaseTransferView.imrAddBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mBaseTransferView.imrAddBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo) {
        AppClient.getInstance().saveImrBeneficiaryInfo(beneficiaryInfo.payeeInfoId,
                beneficiaryInfo.transferDestinationCountryCode, beneficiaryInfo.receiptMethod,
                beneficiaryInfo.receiptOrgCode, beneficiaryInfo.receiptOrgBranchCode,
                beneficiaryInfo.payeeFullName, beneficiaryInfo.nickName, beneficiaryInfo.relationshipCode,
                beneficiaryInfo.callingCode, beneficiaryInfo.phone, beneficiaryInfo.payeeInfoCountryCode,
                beneficiaryInfo.birthPlace, beneficiaryInfo.birthDate, beneficiaryInfo.sex,
                beneficiaryInfo.cityName, beneficiaryInfo.districtName, beneficiaryInfo.poBox,
                beneficiaryInfo.buildingNo, beneficiaryInfo.street, beneficiaryInfo.idNo,
                beneficiaryInfo.idExpiry, beneficiaryInfo.bankAccountType, beneficiaryInfo.ibanNo,
                beneficiaryInfo.bankAccountNo,beneficiaryInfo.receiptOrgName,beneficiaryInfo.receiptOrgBranchName,
                beneficiaryInfo.cityId,beneficiaryInfo.currencyCode,beneficiaryInfo.channelPayeeId,beneficiaryInfo.channelCode,beneficiaryInfo.branchId,
                new LifecycleMVPResultCallback<ResponseEntity>(mBaseTransferView, true) {
                    @Override
                    protected void onSuccess(ResponseEntity result) {
                        mBaseTransferView.imrEditBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mBaseTransferView.imrEditBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }
}
