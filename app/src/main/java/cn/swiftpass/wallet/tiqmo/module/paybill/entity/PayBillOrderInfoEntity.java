package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillOrderInfoEntity extends BaseEntity {
    public String orderInfo;

    public RiskControlEntity riskControlInfo;
}
