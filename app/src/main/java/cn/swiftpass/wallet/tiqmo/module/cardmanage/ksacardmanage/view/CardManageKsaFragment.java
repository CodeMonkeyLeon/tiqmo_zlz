package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Handler;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.youth.banner.Banner;
import com.youth.banner.listener.OnPageChangeListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.HomeCardBannerAdapter;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.KsaCardFilterDialog;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardListEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardListKsaPresenter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.TransferHistoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.AsteriskPasswordTransformationMethod;
import cn.swiftpass.wallet.tiqmo.widget.MyNestedScrollView;
import cn.swiftpass.wallet.tiqmo.widget.WrapContentLinearLayoutManager;
import cn.swiftpass.wallet.tiqmo.widget.adapter.ItemTouchHelperListener;
import cn.swiftpass.wallet.tiqmo.widget.adapter.SimpleItemTouchHelperCallback;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CardKycNoticeDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class CardManageKsaFragment extends BaseFragment<CardKsaContract.CardListPresenter> implements CardKsaContract.CardListView, ItemTouchHelperListener {

    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_add_card)
    TextView tvAddCard;
    @BindView(R.id.iv_kyc)
    ImageView ivKyc;
    @BindView(R.id.tv_kyc)
    TextView tvKyc;
    @BindView(R.id.ll_not_kyc)
    LinearLayout llNotKyc;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_get_digital_card)
    TextView tvGetDigitalCard;
    @BindView(R.id.card_manage_bg)
    ConstraintLayout cardManageBg;
    @BindView(R.id.tv_balance_title)
    TextView tvBalanceTitle;
    @BindView(R.id.sw_card_history)
    SwipeRefreshLayout swCardHistory;
    @BindView(R.id.tv_card_balance)
    TextView tvCardBalance;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.iv_add_money)
    ImageView ivAddMoney;
    @BindView(R.id.iv_freeze)
    ImageView ivFreeze;
    @BindView(R.id.iv_limit)
    ImageView ivLimit;
    @BindView(R.id.iv_setting)
    ImageView ivSetting;
    @BindView(R.id.tv_freeze)
    TextView tvFreeze;
    @BindView(R.id.tv_limits)
    TextView tvLimits;
    @BindView(R.id.tv_settings)
    TextView tvSettings;
    @BindView(R.id.ll_card_setting)
    LinearLayout llCardSetting;
    @BindView(R.id.ll_freeze)
    LinearLayout llFreeze;
    @BindView(R.id.ll_limits)
    LinearLayout llLimits;
    @BindView(R.id.ll_settings)
    LinearLayout llSettings;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.iv_filter)
    ImageView ivFilter;
    @BindView(R.id.tv_history_title)
    TextView tvHistoryTitle;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;
    @BindView(R.id.ry_transaction_history)
    RecyclerView ryTransactionHistory;
    @BindView(R.id.tv_card_number)
    TextView tvCardNumber;
    @BindView(R.id.view_bottom)
    View viewBottom;
    @BindView(R.id.iv_add_card)
    ImageView ivAddCard;
    @BindView(R.id.ll_title)
    ConstraintLayout llTitle;
    @BindView(R.id.con_add_money)
    ConstraintLayout conAddMoney;
    @BindView(R.id.sc_have_no_card)
    MyNestedScrollView scHaveNoCard;
    @BindView(R.id.banner_card)
    Banner bannerCard;
    @BindView(R.id.iv_money_see)
    ImageView ivMoneySee;

    private ImageView ivNoTransaction;
    private View noHistoryView;

    boolean isChooseLimit;

    private TransferHistoryAdapter transferHistoryAdapter;
    public List<TransferHistoryEntity> historyEntityList = new ArrayList<>();
    public List<TransferHistoryListEntity> historyListEntityList = new ArrayList<>();

    private int pageSize = 30;
    private int currentPage = 1;
    private FilterEntity filterEntity;
    public List<String> typeList = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();
    private HashMap<Integer, String> selectedArray = new HashMap<>();

    private String startDateTime = "", endDateTime = "";
    private String accountNo = "";
    private StatusView historyStatusView;
    private String direction, orderNo;

    private String startDate = "", endDate = "";

//    private FilterListEntity filterListEntity;

    private int historyPosition;
    private SimpleItemTouchHelperCallback itemTouchHelperCallback;
    ItemTouchHelper historyTouchHelper;

    private WrapContentLinearLayoutManager wrapContentLinearLayoutManager;

    //卡状态	ALLOCATED 可用 LOCKED暂时冻结 BLOCKED 注销卡
//    private String cardStatus;
    //卡类型 实体卡或虚拟卡	PHYSICAL 实体卡 VIRTUAL虚拟卡
//    private String cardType;

    private UserInfoEntity userInfoEntity;

    private HomeCardBannerAdapter cardBannerAdapter;

    private int currentBannerPosition = 0;
    private int lastDetailPosition = -1;

    private List<KsaCardEntity> cardList = new ArrayList<>();

    private CardDetailEntity cardDetailEntity;
    private KsaCardEntity ksaCardEntity;

    private int otpType = 1;

    private String mProxyCardNo = "52494000108045353797";

    private boolean isHideMoney;

    boolean showCardDetail;

    private RiskControlEntity riskControlEntity;

    public static CardManageKsaFragment cardManageKsaFragment;

    public static CardManageKsaFragment getInstance() {
        CardManageKsaFragment cardManageKsaFragment = new CardManageKsaFragment();
        return cardManageKsaFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cardManageKsaFragment = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_card_manage_ksa;
    }

    @Override
    protected void initView(View parentView) {
        cardManageKsaFragment = this;
        LocaleUtils.viewRotationY(mContext, headCircle);
        swCardHistory.setColorSchemeResources(R.color.color_B00931);
        filterEntity = new FilterEntity();
        initRecyclerView();
        userInfoEntity = getUserInfoEntity();
        if (userInfoEntity.isKyc()) {
            initKyc();
        } else {
            initNoKyc();
        }
        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
    }

    private void initNoKyc() {
        tvContentOne.setText(getString(R.string.DigitalCard_22));
        tvGetDigitalCard.setText(getString(R.string.DigitalCard_23));
        conAddMoney.setVisibility(View.GONE);
        swCardHistory.setVisibility(View.GONE);
        scHaveNoCard.setVisibility(View.VISIBLE);
        llNotKyc.setVisibility(View.VISIBLE);
        tvAddCard.setVisibility(View.GONE);
        ivAddCard.setVisibility(View.GONE);
    }

    private void initKyc() {
        try {
            tvContentOne.setText(getString(R.string.DigitalCard_1));
            tvGetDigitalCard.setText(getString(R.string.DigitalCard_10));
            swCardHistory.setVisibility(View.VISIBLE);
            scHaveNoCard.setVisibility(View.GONE);
            conAddMoney.setVisibility(View.VISIBLE);
            llNotKyc.setVisibility(View.GONE);
            tvAddCard.setVisibility(View.VISIBLE);
            ivAddCard.setVisibility(View.VISIBLE);

            isHideMoney = SpUtils.getInstance().getHidePwd();
            if (isHideMoney) {
                ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_hide));
                tvCardBalance.setTransformationMethod(new AsteriskPasswordTransformationMethod());
            } else {
                ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_unhide));
                tvCardBalance.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            tvCardBalance.setText(getUserInfoEntity().getBalance());
            tvCurrency.setText(LocaleUtils.getCurrencyCode(""));
//            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//                @Override
//                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                    if (verticalOffset >= 0) {
//                        swCardHistory.setEnabled(true);
//                    } else {
//                        swCardHistory.setEnabled(false);
//                    }
//                }
//            });

            cardBannerAdapter = new HomeCardBannerAdapter(mContext, cardList);
            cardBannerAdapter.setCardBannerClickListener(new HomeCardBannerAdapter.CardBannerClickListener() {
                @Override
                public void activateCard(KsaCardEntity ksaCardEntity) {
                    if (ksaCardEntity.isVirtualCard()) {
                        mPresenter.activateCard(ksaCardEntity.proxyCardNo, "", "");
                    } else {
                        showCardDetail = true;
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put(Constants.ksaCardEntity, ksaCardEntity);
                        ActivitySkipUtil.startAnotherActivity(mActivity, ActivateCardActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }

                @Override
                public void getCardDetails(KsaCardEntity ksaCardEntity) {
                    if (AndroidUtils.isCloseUse(mActivity, Constants.card_detail)) return;
                    otpType = 2;
                    List<String> additionalData = new ArrayList<>();
                    mPresenter.getOtpType("ActionType", "TCA09", additionalData);
                }

                @Override
                public void getNewCard(KsaCardEntity ksaCardEntity) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, CardIntroductionActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }

                @Override
                public void copyCardNo(KsaCardEntity ksaCardEntity) {
                    String cardNumber = ksaCardEntity.cardNumber;
                    if (!TextUtils.isEmpty(cardNumber)) {
                        AndroidUtils.setClipboardText(mContext, cardNumber, true);
                        showTipDialog(getString(R.string.DigitalCard_87));
                    }
                }

                @Override
                public void contactSupport() {
                    contactHelp();
                }
            });
            bannerCard.addBannerLifecycleObserver(this)//添加生命周期观察者
                    .setAdapter(cardBannerAdapter, false)
                    .setUserInputEnabled(true)//禁止手动滑动Banner
                    .isAutoLoop(false)//是否允许自动轮播
                    .setBannerGalleryEffect(20, 20, 10, 0.9f)
                    .addOnPageChangeListener(new OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            currentBannerPosition = position;
                            KsaCardEntity ksaCardEntity = cardList.get(position);
                            if (ksaCardEntity != null) {
                                setCardStatus(ksaCardEntity);
                            }
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }

        showProgress(true);
        mPresenter.getCardList();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            tvCardBalance.setText(userInfoEntity.getBalance());
        } else if (EventEntity.EVENT_KSA_CARD_BLOCK == event.getEventType()) {
            if (cardList.get(currentBannerPosition) != null) {
                cardList.get(currentBannerPosition).cardStatus = "BLOCKED";
            }
            setCardStatus(ksaCardEntity);
            cardBannerAdapter.notifyItemChanged(currentBannerPosition);
        } else if (EventEntity.EVENT_KSA_CARD_ACTIVATE == event.getEventType()) {
            if (cardList.get(currentBannerPosition) != null) {
                cardList.get(currentBannerPosition).cardStatus = "ACTIVATE";
            }
            setCardStatus(ksaCardEntity);
            cardBannerAdapter.notifyItemChanged(currentBannerPosition);
        } else if (Constants.KYC_TYPE_KSA_ADD_CARD == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, GetNewCardOneActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (EventEntity.EVENT_KSA_SHOW_DETAIL == event.getEventType()) {
            showCardDetail = true;
            CardDetailEntity cardDetailEntity = event.getCardDetailEntity();
            setCardDetail(cardDetailEntity);
        } else if (EventEntity.EVENT_KSA_CARD_FREEZE == event.getEventType()) {
            showCardDetail = true;
            requestTransfer();
        }
    }

    private void setCardDetail(CardDetailEntity cardDetailEntity) {
        try {
            if (cardDetailEntity != null) {
                int time = Integer.parseInt(cardDetailEntity.expireTimes);
                Handler mHandler = new Handler();
                mHandler.postDelayed(timerRunnable, time * 1000);
                if (cardDetailEntity != null) {
                    this.cardDetailEntity = cardDetailEntity;
                    cardList.get(currentBannerPosition).isShowCardDetail = true;
                    if (lastDetailPosition != -1 && lastDetailPosition != currentBannerPosition) {
                        cardList.get(lastDetailPosition).isShowCardDetail = false;
                    }
                    cardList.get(currentBannerPosition).cardDetailEntity = cardDetailEntity;
                    cardBannerAdapter.setShowCardDetail(currentBannerPosition, cardDetailEntity);
                    cardBannerAdapter.notifyItemChanged(currentBannerPosition);
                    if (lastDetailPosition != -1 && lastDetailPosition != currentBannerPosition) {
                        cardBannerAdapter.notifyItemChanged(lastDetailPosition);
                    }
                    lastDetailPosition = currentBannerPosition;
//                    cardBannerAdapter.notifyDataSetChanged();
                }
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void initNoContentView() {
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        TextView tvContent = noHistoryView.findViewById(R.id.tv_result_content);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvContent.setVisibility(View.VISIBLE);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_transaction));
        tvContent.setText(getString(R.string.Cards_filter_12));
        tvNoContent.setText(getString(R.string.Cards_home_9));
        historyStatusView = new StatusView.Builder(mContext, ryTransactionHistory).setNoContentView(noHistoryView).build();
    }

    private void initRecyclerView() {
        initNoContentView();

        wrapContentLinearLayoutManager = new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        ryTransactionHistory.setLayoutManager(wrapContentLinearLayoutManager);
        transferHistoryAdapter = new TransferHistoryAdapter(historyEntityList, mActivity);
        itemTouchHelperCallback = new SimpleItemTouchHelperCallback(this, 1);
        historyTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        historyTouchHelper.attachToRecyclerView(ryTransactionHistory);

        transferHistoryAdapter.bindToRecyclerView(ryTransactionHistory);
        swCardHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                mPresenter.getCardList();
                currentPage = 1;
                orderNo = "";
                direction = "";
                mPresenter.getHistoryList(mProxyCardNo, "", typeList, tradeType, startDate, endDate, pageSize, currentPage, direction, orderNo);
            }
        });

        transferHistoryAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                currentPage += 1;
                direction = "down";
                mPresenter.getHistoryList(mProxyCardNo, "", typeList, tradeType, startDate, endDate, pageSize, currentPage, direction, orderNo);
            }
        }, ryTransactionHistory);

        transferHistoryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (AndroidUtils.isCloseUse(mActivity, Constants.card_history)) return;
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
                if (transferHistoryEntity != null) {
                    mPresenter.getHistoryDetail(transferHistoryEntity.orderNo, transferHistoryEntity.orderType, "");
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        tvFilter.setEnabled(true);
    }

    public void getList() {
//        swCardHistory.setRefreshing(true);
        llFilter.removeAllViews();
        filterEntity = new FilterEntity();
        typeList = new ArrayList<>();
        tradeType = new ArrayList<>();
        startDate = "";
        endDate = "";
        currentPage = 1;
        orderNo = "";
        direction = "";
        mPresenter.getHistoryList(mProxyCardNo, "", new ArrayList<>(), new ArrayList<>(), "", "", 30, 1, "", "");
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (getUserInfoEntity().isKyc()) {
//            if (!hidden) {
//                mPresenter.getCardList();
//            }
//        }
//    }

    @Override
    protected void initData() {
        if (getUserInfoEntity().isKyc()) {
//            swCardHistory.setRefreshing(true);
            noticeThemeChange();
        }
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setTextColorByAttr(tvBalanceTitle, R.attr.color_40white_757575);
        helper.setImageResourceByAttr(headCircle, R.attr.icon_circle);
        helper.setImageResourceByAttr(ivAddMoney, R.attr.icon_add_money_card);
        helper.setTextColorByAttr(tvFilter, R.attr.history_filter_text_color);
        helper.setTextColorByAttr(tvAddMoney, R.attr.color_white_3a3b44);
        helper.setImageResourceByAttr(ivFreeze, R.attr.icon_digital_freeze);
        helper.setImageResourceByAttr(ivLimit, R.attr.icon_digital_limits);
        helper.setImageResourceByAttr(ivSetting, R.attr.icon_digital_settings);
        helper.setImageResourceByAttr(ivFilter, R.attr.icon_filter);
        helper.setTextColorByAttr(tvHistoryTitle, R.attr.history_filter_title_text_color);
        helper.setTextColorByAttr(tvFilter, R.attr.history_filter_text_color);
        helper.setTextColorByAttr(tvCardBalance, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvCurrency, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvFreeze, R.attr.color_80white_803a3b44);
        helper.setTextColorByAttr(tvLimits, R.attr.color_80white_803a3b44);
        helper.setTextColorByAttr(tvSettings, R.attr.color_80white_803a3b44);
        helper.setTextColorByAttr(tvContentOne, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvGetDigitalCard, R.attr.color_1da1f1_fc4f00);
        helper.setTextColorByAttr(tvTitle, R.attr.wallet_setting_text_color);
        helper.setBackgroundResourceByAttr(cardManageBg, R.attr.bg_trust_device);
        helper.setBackgroundResourceByAttr(llNotKyc, R.attr.shape_40black_white);
        helper.setImageResourceByAttr(ivKyc, R.attr.icon_home_info);
        helper.setTextColorByAttr(tvKyc, R.attr.color_white_3a3b44);
        helper.setBackgroundResourceByAttr(tvGetDigitalCard, R.attr.shape_1da1f1_fc4f00_10);

        if (noHistoryView != null) {
            helper.setImageResourceByAttr(noHistoryView.findViewById(R.id.iv_no_content), R.attr.icon_no_transaction);
            helper.setTextColorByAttr(noHistoryView.findViewById(R.id.tv_result_content), R.attr.color_white_3a3b44);
        }
        if (transferHistoryAdapter != null) {
            transferHistoryAdapter.changeTheme();
        }
    }

    @Override
    protected void restart() {
        if (getUserInfoEntity().isKyc()) {
            if (showCardDetail) {
                showCardDetail = false;
            } else {
                initKyc();
            }
        }
    }

    @OnClick({R.id.tv_filter, R.id.ll_freeze, R.id.ll_limits, R.id.ll_settings, R.id.tv_add_money,
            R.id.tv_get_digital_card, R.id.iv_add_card, R.id.ll_not_kyc, R.id.iv_money_see})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> mHashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_money_see:
                if (isHideMoney) {
                    //如果已经隐藏了金额  则显示
                    isHideMoney = false;
                    ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_unhide));
                    tvCardBalance.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    isHideMoney = true;
                    ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_hide));
                    tvCardBalance.setTransformationMethod(new AsteriskPasswordTransformationMethod());
                }
                SpUtils.getInstance().setHidePwd(isHideMoney);
                break;
            case R.id.ll_not_kyc:
                showCardKycDialog();
                break;
            case R.id.iv_add_card:
                if (AndroidUtils.isCloseUse(mActivity, Constants.cards_addNew)) return;
//                ActivitySkipUtil.startAnotherActivity(mActivity, GetNewCardTwoActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                mPresenter.activateCardResult();
                break;
            case R.id.tv_get_digital_card:
                if (getUserInfoEntity().isKyc()) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, CardIntroductionActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    showCardKycDialog();
                }
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(mActivity);
                break;
            case R.id.ll_freeze:
                if (AndroidUtils.isCloseUse(mActivity, Constants.cards_settings)) return;
                otpType = 1;
                List<String> additionalData = new ArrayList<>();
                additionalData.add(ksaCardEntity.maskedCardNo);
                if (ksaCardEntity.isLocked()) {
                    mPresenter.getOtpType("ActionType", "TCA04", additionalData);
                } else {
                    mPresenter.getOtpType("ActionType", "TCA03", additionalData);
                }
                break;
            case R.id.ll_limits:
                if (AndroidUtils.isCloseUse(mActivity, Constants.cards_limit)) return;
                isChooseLimit = true;
                mPresenter.getKsaLimitChannel(ksaCardEntity.proxyCardNo);
                break;
            case R.id.ll_settings:
                if (AndroidUtils.isCloseUse(mActivity, Constants.cards_settings)) return;
                if (ksaCardEntity != null && ksaCardEntity.isLocked()) {
                    mHashMap.put(Constants.proxyCardNo, ksaCardEntity.proxyCardNo);
                    ActivitySkipUtil.startAnotherActivity(mActivity, BlockMyCardActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    isChooseLimit = false;
                    mPresenter.getKsaLimitChannel(ksaCardEntity.proxyCardNo);
                }
                break;
            case R.id.tv_filter:
                showCardFilterDialog(1);
                break;
            default:
                break;
        }
    }

    private void refreshList(FilterEntity filterEntity) {
        if (filterEntity != null) {
            String showStartDate = "", showEndDate = "";
            startDate = filterEntity.startDate;
            showStartDate = filterEntity.showStartDate;
            endDate = filterEntity.endDate;
            showEndDate = filterEntity.showEndDate;
            typeList = filterEntity.paymentType;
            tradeType = filterEntity.tradeType;
            selectedArray = filterEntity.selectedArray;
            categoryList = filterEntity.categoryList;
            List<FilterCodeEntity> filterList = new ArrayList<>();

            if (TextUtils.isEmpty(startDate) && !TextUtils.isEmpty(endDate)) {
                showStartDate = "...";
            }

            if (!TextUtils.isEmpty(startDate) && TextUtils.isEmpty(endDate)) {
                showEndDate = getString(R.string.History_filter_14);
            }

            if (!TextUtils.isEmpty(startDate) || !TextUtils.isEmpty(endDate)) {
                FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                filterCodeEntity.type = 3;
                filterCodeEntity.title = showStartDate + "-" + showEndDate;
                filterList.add(filterCodeEntity);
            }

            int typeSize = typeList.size();
            for (int i = 0; i < typeSize; i++) {
                FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                filterCodeEntity.type = 2;
                String type = typeList.get(i);
                filterCodeEntity.value = type;
                if ("P".equals(type)) {
                    filterCodeEntity.title = getString(R.string.History_filter_5);
                } else if ("I".equals(type)) {
                    filterCodeEntity.title = getString(R.string.History_filter_6);
                }
                filterList.add(filterCodeEntity);
            }

            int categorySize = categoryList.size();
            for (int i = 0; i < categorySize; i++) {
                FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                filterCodeEntity.type = 1;
                filterCodeEntity.title = categoryList.get(i);
                filterCodeEntity.value = tradeType.get(i);
                filterList.add(filterCodeEntity);
            }

            initFilter(filterList);
            currentPage = 1;
            orderNo = "";
            direction = "";
            showProgress(true);
            mPresenter.getHistoryList(mProxyCardNo, "", typeList, tradeType, startDate, endDate, pageSize, currentPage, direction, orderNo);
        }
    }

    private void initFilter(List<FilterCodeEntity> filterList) {
        if (filterList != null && filterList.size() > 0) {
            scFilter.setVisibility(View.GONE);
            llFilter.removeAllViews();
            int size = filterList.size();
            for (int i = 0; i < size; i++) {
                final FilterCodeEntity filterCodeEntity = filterList.get(i);
                if (filterCodeEntity != null) {
                    String title = filterCodeEntity.title;
                    final View view = LayoutInflater.from(mContext).inflate(R.layout.item_top_filter, null);
                    TextView tvTopFilter = view.findViewById(R.id.tv_top_filter);
                    ImageView ivDelete = view.findViewById(R.id.iv_filter_delete);
                    if (!TextUtils.isEmpty(title)) {
                        tvTopFilter.setText(title);
                    }
                    ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int type = filterCodeEntity.type;
                            switch (type) {
                                case 1:
                                    tradeType.remove(filterCodeEntity.value);
                                    categoryList.remove(filterCodeEntity.title);
                                    // 键和值
                                    Integer key = null;
                                    String value = null;
                                    // 获取键值对的迭代器
                                    Iterator it = selectedArray.entrySet().iterator();
                                    while (it.hasNext()) {
                                        HashMap.Entry entry = (HashMap.Entry) it.next();
                                        key = (Integer) entry.getKey();
                                        value = (String) entry.getValue();
                                        if (filterCodeEntity.value.equals(value)) {
                                            it.remove();
                                        }
                                        System.out.println("key:" + key + "---" + "value:" + value);
                                    }
                                    break;
                                case 2:
                                    typeList.remove(filterCodeEntity.value);
                                    break;
                                case 3:
                                    startDate = "";
                                    endDate = "";
                                    filterEntity.showStartDate = "";
                                    filterEntity.showEndDate = "";
                                    filterEntity.startDate = "";
                                    filterEntity.endDate = "";
                                    break;
                                default:
                                    break;
                            }
                            llFilter.removeView(view);

                            currentPage = 1;
                            orderNo = "";
                            direction = "";
                            showProgress(true);
                            mPresenter.getHistoryList(mProxyCardNo, "", typeList, tradeType, startDate, endDate, pageSize, currentPage, direction, orderNo);
                        }
                    });
                    llFilter.addView(view);
                }
            }
        } else {
            scFilter.setVisibility(View.GONE);
        }
    }

    private void showCardFilterDialog(int changeType) {
        KsaCardFilterDialog cardFilterDialog = new KsaCardFilterDialog(mContext);
        cardFilterDialog.setFilterEntity(filterEntity);
        cardFilterDialog.setApplyFilterListener(new KsaCardFilterDialog.ApplyFilterListener() {
            @Override
            public void changeFilter(FilterEntity filterEntity) {
                cardFilterDialog.dismiss();
                refreshList(filterEntity);
            }
        });
        cardFilterDialog.showWithBottomAnim();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDissmiss(int position, int type) {
        historyPosition = position;
        TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
        if (transferHistoryEntity != null) {
            mPresenter.deleteTransferHistory(transferHistoryEntity.orderNo);
        }
    }

    private void setCardStatus(KsaCardEntity ksaCardEntity) {
        this.ksaCardEntity = ksaCardEntity;
        mProxyCardNo = ksaCardEntity.proxyCardNo;
        getList();
        String cardType = ksaCardEntity.cardType;
        tvFreeze.setText(getString(R.string.DigitalCard_47));
        ivSetting.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_digital_settings));
        tvSettings.setText(getString(R.string.DigitalCard_50));
        String cardStatus = ksaCardEntity.cardStatus;
        if (ksaCardEntity.isLocked()) {
            llFreeze.setAlpha(1.0f);
            tvFreeze.setText(getString(R.string.DigitalCard_48));
            llFreeze.setEnabled(true);
            llLimits.setAlpha(0.3f);
            llLimits.setEnabled(false);
            llSettings.setAlpha(1.0f);
            llSettings.setEnabled(true);
            ivSetting.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_home_card_block));
            tvSettings.setText(getString(R.string.sprint20_53));
        } else if (ksaCardEntity.isBlocked() || ksaCardEntity.isNotActivate() || ksaCardEntity.isExpired()) {
            llFreeze.setAlpha(0.3f);
            llFreeze.setEnabled(false);
            llLimits.setAlpha(0.3f);
            llLimits.setEnabled(false);
            llSettings.setAlpha(0.3f);
            llSettings.setEnabled(false);
        } else {
            llFreeze.setAlpha(1.0f);
            llFreeze.setEnabled(true);
            llLimits.setAlpha(1.0f);
            llLimits.setEnabled(true);
            llSettings.setAlpha(1.0f);
            llSettings.setEnabled(true);
        }
    }

    @Override
    public void getCardListSuccess(KsaCardListEntity ksaCardListEntity) {
        currentBannerPosition = 0;
        if (ksaCardListEntity != null) {
            cardList.clear();
            cardList.addAll(ksaCardListEntity.cardList);
            int size = cardList.size();
            cardBannerAdapter.setDatas(cardList);
            bannerCard.setCurrentItem(currentBannerPosition, false);
            if (size > 0) {
                scHaveNoCard.setVisibility(View.GONE);
                swCardHistory.setVisibility(View.VISIBLE);
                conAddMoney.setVisibility(View.VISIBLE);
                llTitle.setVisibility(View.VISIBLE);
                KsaCardEntity ksaCardEntity = cardList.get(currentBannerPosition);
//                ksaCardEntity.proxyCardNo = mProxyCardNo;
                setCardStatus(ksaCardEntity);
            } else {
                scHaveNoCard.setVisibility(View.VISIBLE);
                swCardHistory.setVisibility(View.GONE);
                conAddMoney.setVisibility(View.GONE);
                llTitle.setVisibility(View.GONE);
            }
        }
        showProgress(false);
    }

    @Override
    public void getCardListFail(String errorCode, String errorMsg) {
        showProgress(false);
        showTipDialog(errorMsg);
    }

    @Override
    public void getHistoryListSuccess(TransferHistoryMainEntity transferHistoryMainEntity) {
        showProgress(false);
        List<TransferHistoryEntity> tempHistoryList = new ArrayList<>();
        swCardHistory.setRefreshing(false);
        if (transferHistoryMainEntity != null) {
            List<TransferHistoryListEntity> historyListEntityList = transferHistoryMainEntity.transactionHistorys;
            int size = historyListEntityList.size();
            for (int i = 0; i < size; i++) {
                TransferHistoryListEntity historyListEntity = historyListEntityList.get(i);
                if (historyListEntity != null) {
                    tempHistoryList.addAll(historyListEntity.transactionListInfos);
                }
            }
        }

        if (currentPage == 1) {
            ryTransactionHistory.scrollToPosition(0);
            historyEntityList.clear();
            if (tempHistoryList.size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                transferHistoryAdapter.setDataList(tempHistoryList);
            }
        } else {
            if (tempHistoryList.size() == 0) {
                transferHistoryAdapter.loadMoreEnd(false);
            } else {
                transferHistoryAdapter.addData(tempHistoryList);
                if (tempHistoryList.size() < pageSize) {
                    transferHistoryAdapter.loadMoreEnd(false);
                } else {
                    transferHistoryAdapter.loadMoreComplete();
                }
            }
        }
        historyEntityList.addAll(tempHistoryList);
        if (historyEntityList.size() > 0) {
            TransferHistoryEntity lastEntity = historyEntityList.get(historyEntityList.size() - 1);
            if (lastEntity != null) {
                orderNo = lastEntity.orderNo;
            }
        }
    }

    @Override
    public void getHistoryDetailSuccess(TransferHistoryDetailEntity transferHistoryDetailEntity) {
        AndroidUtils.jumpToTransferDetail(mActivity, transferHistoryDetailEntity, "", false);
        showCardDetail = true;
    }

    @Override
    public void deleteTransferHistorySuccess(Void result) {
        if (transferHistoryAdapter != null) {
            //移除数据
            transferHistoryAdapter.getDataList().remove(historyPosition);
            transferHistoryAdapter.notifyDataSetChanged();
            if (transferHistoryAdapter.getDataList().size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void activateCardSuccess(Void result) {
        if (cardList.get(currentBannerPosition) != null) {
            cardList.get(currentBannerPosition).cardStatus = "ACTIVATE";
        }
        setCardStatus(ksaCardEntity);
        cardBannerAdapter.notifyItemChanged(currentBannerPosition);
    }

    @Override
    public void activateCardResultSuccess(Void result) {
//        ActivitySkipUtil.startAnotherActivity(mActivity, GetNewCardTwoActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        CardIntroductionActivity.Companion.startCardIntroductionActivity(mActivity);
    }

    @Override
    public void getHistoryListFail(String errorCode, String errorMsg) {
        swCardHistory.setRefreshing(false);
        showProgress(false);
        showTipDialog(errorMsg);
        if (currentPage == 1) {
            showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
        }
    }

    private final Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            if (cardList.get(currentBannerPosition) != null) {
                cardList.get(currentBannerPosition).isShowCardDetail = false;
            }
            cardBannerAdapter.setShowCardDetail(currentBannerPosition, cardDetailEntity);
            cardBannerAdapter.notifyItemChanged(currentBannerPosition);
        }
    };

    @Override
    public void getKsaCardDetailsSuccess(CardDetailEntity cardDetailEntity) {
        setCardDetail(cardDetailEntity);
    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {
        if (otpType == 1) {
            if (ksaCardEntity.isLocked()) {
                if (cardList.get(currentBannerPosition) != null) {
                    cardList.get(currentBannerPosition).cardStatus = "ACTIVATE";
                }
            } else {
                if (cardList.get(currentBannerPosition) != null) {
                    cardList.get(currentBannerPosition).cardStatus = "LOCKED";
                }
            }
            setCardStatus(ksaCardEntity);
            cardBannerAdapter.notifyItemChanged(currentBannerPosition);
        }
//        if (getUserInfoEntity().isKyc()) {
//            showProgress(true);
//            mPresenter.getCardList();
//        }
    }

    @Override
    public void getKsaLimitChannelSuccess(KsaLimitChannelEntity ksaLimitChannelEntity) {
        showCardDetail = true;
        if (ksaLimitChannelEntity != null) {
            ksaLimitChannelEntity.proxyCardNo = ksaCardEntity.proxyCardNo;
            HashMap<String, Object> mHashMap = new HashMap<>();
            if (isChooseLimit) {
                mHashMap.put(Constants.ksaLimitChannelEntity, ksaLimitChannelEntity);
                mHashMap.put(Constants.ksaCardEntity, ksaCardEntity);
                ActivitySkipUtil.startAnotherActivity(mActivity, KsaCardLimitActivity.class, mHashMap,
                        ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                mHashMap.put(Constants.ksaCardEntity, ksaCardEntity);
                mHashMap.put(Constants.ksaLimitChannelEntity, ksaLimitChannelEntity);
                ActivitySkipUtil.startAnotherActivity(mActivity, KsaCardSettingActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public CardKsaContract.CardListPresenter getPresenter() {
        return new CardListKsaPresenter();
    }

    private void showCardKycDialog() {
        CardKycNoticeDialog cardKycNoticeDialog = new CardKycNoticeDialog(mContext);
        cardKycNoticeDialog.setContinueListener(new CardKycNoticeDialog.ContinueListener() {
            @Override
            public void clickContinue() {
                cardKycNoticeDialog.dismiss();
                if (isKyc(Constants.KYC_TYPE_KSA_ADD_CARD)) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, GetNewCardOneActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
        cardKycNoticeDialog.showWithBottomAnim();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(mActivity, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                } else if (riskControlEntity.isNeedIvr()) {
                    jumpToPwd(true);
                } else {
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    private void requestTransfer() {
        if (otpType == 1) {
            if (ksaCardEntity.isLocked()) {
                mPresenter.setKsaCardStatus(Constants.TYPE_CARD_UNFREEZE, ksaCardEntity.proxyCardNo, "", "");
            } else {
                mPresenter.setKsaCardStatus(Constants.TYPE_CARD_FREEZE, ksaCardEntity.proxyCardNo, "", "");
            }
        } else if (otpType == 2) {
            mPresenter.getKsaCardDetails(ksaCardEntity.proxyCardNo);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap) {
        if (otpType == 1) {
            if (ksaCardEntity.isLocked()) {
                mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_UNFREEZE);
            } else {
                mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_FREEZE);
            }
        } else if (otpType == 2) {
            mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_DETAIL);
        }
        mHashMap.put(Constants.ksaCardEntity, ksaCardEntity);

        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        if (otpType == 1) {
            if (ksaCardEntity.isLocked()) {
                mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_FREEZE_CARD);
            } else {
                mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_UNFREEZE_CARD);
            }
        } else if (otpType == 2) {
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_CARD_DETAIL);
        }
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if (isNeedIvr) {
            ActivitySkipUtil.startAnotherActivity(mActivity, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            ActivitySkipUtil.startAnotherActivity(mActivity, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
