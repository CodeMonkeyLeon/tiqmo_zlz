package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrgBranchEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPayerDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrQueryPayerEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.AddBeneFivePresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class AddBeneficiaryFiveActivity extends BaseCompatActivity<AddBeneficiaryContract.AddFivePresenter> implements AddBeneficiaryContract.AddFiveView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_agent_name)
    EditTextWithDel etAgentName;
    @BindView(R.id.et_agent_branch)
    EditTextWithDel etAgentBranch;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.et_city)
    EditTextWithDel etCity;
    @BindView(R.id.ll_city)
    ConstraintLayout llCity;
    @BindView(R.id.ll_bene_currency)
    ConstraintLayout ll_bene_currency;
    @BindView(R.id.ll_agent_branch)
    ConstraintLayout llAgentBranch;
    @BindView(R.id.et_bene_currency)
    EditTextWithDel etBeneCurrency;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;

    private ImrBankAgentEntity imrNameEntity, imrBranchEntity;
    private ImrCityEntity imrCityEntity;
    private boolean isEditBeneficiary = false;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;// edit data

    private String receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
            nickName, relationshipCode, callingCode, phone,
            transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
            birthDate, sex, cityName, districtName,
            poBox, buildingNo, street, idNo, idExpiry,
            bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
            cityId,channelPayeeId,channelCode,branchId;

    private String payeeInfoId;
    private ImrExchangeRateEntity imrExchangeRateEntity;
    private boolean showSaveList;

    private String beneCurrency;
    public List<String> agentSupportCurrencyCodes = new ArrayList<>();

    public List<ImrQueryPayerEntity> imrQueryPayerInfoInfoList = new ArrayList<>();
    public List<ImrOrgBranchEntity> receiptOrgBranchInfoList = new ArrayList<>();

    private RiskControlEntity riskControlEntity;

    private static AddBeneficiaryFiveActivity addBeneficiaryFiveActivity;
    private BottomDialog bottomDialog;

    public static AddBeneficiaryFiveActivity getAddBeneficiaryFiveActivity() {
        return addBeneficiaryFiveActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_beneficiary_five;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        addBeneficiaryFiveActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvTitle.setText(getString(R.string.IMR_20));
        etAgentName.getTlEdit().setHint(getString(R.string.IMR_84_1));
        etAgentBranch.getTlEdit().setHint(getString(R.string.IMR_85_1));
        etBeneCurrency.getTlEdit().setHint(getString(R.string.IMR_new_3));
        etCity.getTlEdit().setHint(getString(R.string.IMR_72));
        ivKycStep.setVisibility(View.VISIBLE);
        ivKycStep.setImageResource(R.drawable.kyc_step_3);
//        ivKycStep.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_imr_add_step_5));

        isEditBeneficiary = getIntent().getBooleanExtra(Constants.IS_FROM_EDIT_BENEFICIARY, false);
        if (isEditBeneficiary) {
            beneficiaryInfo = (ImrBeneficiaryDetails.ImrBeneficiaryDetail) getIntent().getSerializableExtra("BeneficiaryInfo");
            receiptOrgCode = beneficiaryInfo.receiptOrgCode;
            receiptOrgBranchCode = beneficiaryInfo.receiptOrgBranchCode;
            transferDestinationCountryCode = beneficiaryInfo.transferDestinationCountryCode;
            agentSupportCurrencyCodes = beneficiaryInfo.imrPaymentEntity.supportCurrencyList;
            beneCurrency = beneficiaryInfo.currencyCode;
            receiptMethod = beneficiaryInfo.receiptMethod;
            cityId = beneficiaryInfo.cityId;
            receiptOrgName = beneficiaryInfo.receiptOrgName;
            receiptOrgBranchName = beneficiaryInfo.receiptOrgBranchName;
            channelCode = beneficiaryInfo.channelCode;
            channelPayeeId = beneficiaryInfo.channelPayeeId;
            etCity.setContentText(beneficiaryInfo.cityName);
            etBeneCurrency.setContentText(beneCurrency);
            etAgentName.setContentText(receiptOrgName);
            etAgentBranch.setContentText(receiptOrgBranchName);
            tvContinue.setEnabled(!TextUtils.isEmpty(etAgentName.getEditText().getText().toString().trim()) && !TextUtils.isEmpty(etAgentBranch.getEditText().getText().toString().trim()));
        } else {
            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            if (imrBeneficiaryEntity != null) {
                receiptMethod = imrBeneficiaryEntity.receiptMethod;
                payeeFullName = imrBeneficiaryEntity.payeeFullName;
                nickName = imrBeneficiaryEntity.nickName;
                relationshipCode = imrBeneficiaryEntity.relationshipCode;
                callingCode = imrBeneficiaryEntity.callingCode;
                phone = imrBeneficiaryEntity.phone;
                transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;
                payeeInfoCountryCode = imrBeneficiaryEntity.payeeInfoCountryCode;
                birthPlace = imrBeneficiaryEntity.birthPlace;
                birthDate = imrBeneficiaryEntity.birthDate;
                sex = imrBeneficiaryEntity.sex;
                cityName = imrBeneficiaryEntity.cityName;
                districtName = imrBeneficiaryEntity.districtName;
                poBox = imrBeneficiaryEntity.poBox;
                buildingNo = imrBeneficiaryEntity.buildingNo;
                street = imrBeneficiaryEntity.street;
                idNo = imrBeneficiaryEntity.idNo;
                idExpiry = imrBeneficiaryEntity.idExpiry;
                bankAccountType = imrBeneficiaryEntity.bankAccountType;
                ibanNo = imrBeneficiaryEntity.ibanNo;
                bankAccountNo = imrBeneficiaryEntity.bankAccountNo;
                saveFlag = imrBeneficiaryEntity.saveFlag;
                showSaveList = imrBeneficiaryEntity.showSaveList;
                agentSupportCurrencyCodes = imrBeneficiaryEntity.agentSupportCurrencyCodes;
                channelCode = imrBeneficiaryEntity.channelCode;
                channelPayeeId = imrBeneficiaryEntity.channelPayeeId;
            }
        }

        if(TextUtils.isEmpty(beneCurrency)) {
            if (agentSupportCurrencyCodes.size() == 1) {
                beneCurrency = agentSupportCurrencyCodes.get(0);
                etBeneCurrency.setContentText(beneCurrency);
            } else if (agentSupportCurrencyCodes.size() > 1) {
                beneCurrency = "";
                etBeneCurrency.setContentText("");
            }
        }

        if(isChannelTF()){
            if(Constants.imr_AT.equals(receiptMethod)){
                llCity.setVisibility(View.VISIBLE);
            }
            if(Constants.imr_CPA.equals(receiptMethod)){
                //CPA隐藏agentBranch
                llAgentBranch.setVisibility(View.GONE);
            }
            ll_bene_currency.setVisibility(View.VISIBLE);
        }

        etAgentName.setFocusableFalse();
        etAgentBranch.setFocusableFalse();
        etCity.setFocusableFalse();
        etBeneCurrency.setFocusableFalse();
        etAgentName.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isChannelTF()){
                    if(Constants.imr_AT.equals(receiptMethod)){
                        if(TextUtils.isEmpty(cityId)) {
                            return;
                        }
                        if(TextUtils.isEmpty(beneCurrency)){
                            return;
                        }
                        ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
                        int size = imrQueryPayerInfoInfoList.size();
                        for (int i = 0; i < size; i++) {
                            ImrQueryPayerEntity imrQueryPayerEntity = imrQueryPayerInfoInfoList.get(i);
                            if (imrQueryPayerEntity != null) {
                                SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                                selectInfoEntity.businessParamValue = imrQueryPayerEntity.receiptOrgName;
                                selectInfoEntityList.add(selectInfoEntity);
                            }
                        }
                        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_84_1), true, true);
                        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                ImrQueryPayerEntity imrQueryPayerEntity = imrQueryPayerInfoInfoList.get(position);
                                if (imrQueryPayerEntity != null) {
                                    receiptOrgBranchInfoList.clear();
                                    receiptOrgBranchInfoList.addAll(imrQueryPayerEntity.receiptOrgBranchInfoList);
                                    receiptOrgCode = imrQueryPayerEntity.receiptOrgCode;
                                    receiptOrgBranchCode = "";
                                    etAgentBranch.getEditText().setText("");
                                    receiptOrgName = imrQueryPayerEntity.receiptOrgName;
                                    etAgentName.getEditText().setText(receiptOrgName);
                                    checkContinueStatus();
                                }
                            }
                        });
                        dialog.showNow(getSupportFragmentManager(), "getTFBankAgentNameSuccess");
                    }else{
                        mPresenter.getBankAgentName(transferDestinationCountryCode, receiptMethod,channelCode);
                    }
                }else {
                    mPresenter.getBankAgentName(transferDestinationCountryCode, receiptMethod,channelCode);
                }
            }
        });
        etAgentBranch.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(receiptOrgCode)) {
                    if(isChannelTF()){
                        if(Constants.imr_AT.equals(receiptMethod)){
                            if(TextUtils.isEmpty(cityId)) {
                                return;
                            }
                            if(TextUtils.isEmpty(beneCurrency)){
                                return;
                            }
                            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
                            int size = receiptOrgBranchInfoList.size();
                            for (int i = 0; i < size; i++) {
                                ImrOrgBranchEntity imrOrgBranchEntity = receiptOrgBranchInfoList.get(i);
                                if (imrOrgBranchEntity != null) {
                                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                                    selectInfoEntity.businessParamValue = imrOrgBranchEntity.receiptOrgBranchName;
                                    selectInfoEntityList.add(selectInfoEntity);
                                }
                            }
                            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_85_1), true, true);
                            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    ImrOrgBranchEntity imrOrgBranchEntity = receiptOrgBranchInfoList.get(position);
                                    if (imrOrgBranchEntity != null) {
                                        receiptOrgBranchCode = imrOrgBranchEntity.receiptOrgBranchCode;
                                        receiptOrgBranchName = imrOrgBranchEntity.receiptOrgBranchName;
                                        etAgentBranch.getEditText().setText(receiptOrgBranchName);
                                        checkContinueStatus();
                                    }
                                }
                            });
                            dialog.showNow(getSupportFragmentManager(), "getTFBankAgentBranchSuccess");
                        }else{
                            mPresenter.getBankAgentBranch(transferDestinationCountryCode, receiptMethod, receiptOrgCode);
                        }
                    }else {
                        mPresenter.getBankAgentBranch(transferDestinationCountryCode, receiptMethod, receiptOrgCode);
                    }
                }
            }
        });

        etBeneCurrency.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
                int size = agentSupportCurrencyCodes.size();
                for (int i = 0; i < size; i++) {
                    String currency = agentSupportCurrencyCodes.get(i);
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = currency;
                    selectInfoEntityList.add(selectInfoEntity);
                }
                ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_new_3), false, false);
                dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        beneCurrency = agentSupportCurrencyCodes.get(position);
                        if (!TextUtils.isEmpty(beneCurrency)) {
                            etBeneCurrency.getEditText().setText(beneCurrency);
                            if(!TextUtils.isEmpty(cityId)){
                                mPresenter.getImrPayerDetail(transferDestinationCountryCode,cityId,LocaleUtils.getRequestCurrencyCode(),beneCurrency,channelCode);
                            }
                            checkContinueStatus();
                        }
                    }
                });
                dialog.showNow(getSupportFragmentManager(), "getBeneCurrencySuccess");
            }
        });
        etCity.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.getImrCityList(transferDestinationCountryCode,channelCode);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        addBeneficiaryFiveActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ADD_SEND_BENEFICIARY == event.getEventType()) {
            requestAddBene();
        }
    }

    private void requestAddBene(){
        if (isEditBeneficiary) {
            mPresenter.saveImrBeneficiaryInfo(beneficiaryInfo);
        }else {
            mPresenter.imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                    nickName, relationshipCode, callingCode, phone,
                    transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                    birthDate, sex, cityName, districtName,
                    poBox, buildingNo, street, idNo, idExpiry,
                    bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
                    cityId, beneCurrency, channelPayeeId, channelCode, branchId);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_continue})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_continue:
                if (showSaveList) {
                    List<String> additionalData = new ArrayList<>();
                    mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                } else {
                    if (isEditBeneficiary) {
                        beneficiaryInfo.receiptOrgCode = receiptOrgCode;
                        beneficiaryInfo.receiptOrgBranchCode = receiptOrgBranchCode;
                        beneficiaryInfo.currencyCode = beneCurrency;
                        beneficiaryInfo.cityId = cityId;
                        beneficiaryInfo.receiptOrgName = receiptOrgName;
                        beneficiaryInfo.receiptOrgBranchName = receiptOrgBranchName;
                    } else {
                        imrBeneficiaryEntity.receiptOrgBranchCode = receiptOrgBranchCode;
                        imrBeneficiaryEntity.receiptOrgCode = receiptOrgCode;
                        imrBeneficiaryEntity.beneCurrency = beneCurrency;
                        imrBeneficiaryEntity.cityId = cityId;
                        imrBeneficiaryEntity.receiptOrgName = receiptOrgName;
                        imrBeneficiaryEntity.receiptOrgBranchName = receiptOrgBranchName;
                        AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(imrBeneficiaryEntity);
                    }

                    List<String> additionalData = new ArrayList<>();
                    mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                }
                break;
            default:
                break;
        }
    }

    private void checkContinueStatus() {
        if (TextUtils.isEmpty(etAgentName.getEditText().getText().toString().trim())) {
            tvContinue.setEnabled(false);
            return;
        }else {
            if(TextUtils.isEmpty(etAgentBranch.getEditText().getText().toString().trim())){
                if(!Constants.imr_CPA.equals(receiptMethod)){
                    tvContinue.setEnabled(false);
                    return;
                }
            }
            if(isChannelTF()){
                if(Constants.imr_AT.equals(receiptMethod)){
                    if(TextUtils.isEmpty(cityId)){
                        tvContinue.setEnabled(false);
                        return;
                    }
                }
                if(TextUtils.isEmpty(beneCurrency)){
                    tvContinue.setEnabled(false);
                    return;
                }
            }
            tvContinue.setEnabled(true);
        }
    }

    @Override
    public void getImrCityListSuccess(ImrCityListEntity imrCityListEntity) {
        if (imrCityListEntity != null) {
            List<ImrCityEntity> imrQueryCityInfoList = imrCityListEntity.imrQueryCityInfoList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = imrQueryCityInfoList.size();
            for (int i = 0; i < size; i++) {
                ImrCityEntity imrCityEntity = imrQueryCityInfoList.get(i);
                if (imrCityEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = imrCityEntity.cityName;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_72), true, false);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    imrCityEntity = imrQueryCityInfoList.get(position);
                    if (imrCityEntity != null) {
                        cityId = imrCityEntity.cityId;
                        cityName = imrCityEntity.cityName;
                        etCity.getEditText().setText(cityName);
                        if(!TextUtils.isEmpty(beneCurrency)){
                            mPresenter.getImrPayerDetail(transferDestinationCountryCode,cityId,LocaleUtils.getRequestCurrencyCode(),beneCurrency,channelCode);
                        }
                        checkContinueStatus();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getCityIdSuccess");
        }
    }

    @Override
    public void getImrCityListFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getBankAgentNameSuccess(ImrBankAgentListEntity imrBankAgentListEntity) {
        if (imrBankAgentListEntity != null) {
            List<ImrBankAgentEntity> bankAgentList = imrBankAgentListEntity.bankAgentList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = bankAgentList.size();
            for (int i = 0; i < size; i++) {
                ImrBankAgentEntity imrBankAgentEntity = bankAgentList.get(i);
                if (imrBankAgentEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = imrBankAgentEntity.bankAgentDesc;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog agentNameDialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_84_1), true, true);
            agentNameDialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    imrNameEntity = bankAgentList.get(position);
                    if (imrNameEntity != null) {
                        receiptOrgCode = imrNameEntity.bankAgentCode;
                        receiptOrgBranchCode = "";
                        etAgentBranch.getEditText().setText("");
                        receiptOrgName = imrNameEntity.bankAgentDesc;
                        etAgentName.getEditText().setText(receiptOrgName);
                        checkContinueStatus();
                    }
                }
            });
            agentNameDialog.showNow(getSupportFragmentManager(), "getBankAgentNameSuccess");
        }
    }

    @Override
    public void getBankAgentNameFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getBankAgentBranchSuccess(ImrBankAgentListEntity imrBankAgentListEntity) {
        if (imrBankAgentListEntity != null) {
            List<ImrBankAgentEntity> bankAgentList = imrBankAgentListEntity.bankAgentList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = bankAgentList.size();
            for (int i = 0; i < size; i++) {
                ImrBankAgentEntity imrBankAgentEntity = bankAgentList.get(i);
                if (imrBankAgentEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = imrBankAgentEntity.bankAgentDesc;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_85_1), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    imrBranchEntity = bankAgentList.get(position);
                    if (imrBranchEntity != null) {
                        receiptOrgBranchCode = imrBranchEntity.bankAgentCode;
                        receiptOrgBranchName = imrBranchEntity.bankAgentDesc;
                        etAgentBranch.getEditText().setText(receiptOrgBranchName);
                        checkContinueStatus();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getBankAgentBranchSuccess");
        }
    }

    @Override
    public void getBankAgentBranchFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity) {
        if (imrAddBeneResultEntity != null) {
            if(showSaveList) {
                payeeInfoId = imrAddBeneResultEntity.payeeInfoId;
                imrBeneficiaryEntity.payeeInfoId = payeeInfoId;
                mPresenter.activateBeneficiary(payeeInfoId);
            }else{
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_ADD_BENE_SUCCESS, imrBeneficiaryEntity,riskControlEntity));
                ProjectApp.removeImrAddBeneTask();
                finish();
            }
        }
    }

    @Override
    public void imrAddBeneficiaryFail(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }
    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity) {
        if (imrExchangeRateEntity != null) {
            this.imrExchangeRateEntity = imrExchangeRateEntity;
            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
        }
    }

    @Override
    public void imrExchangeRateFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        mHashMap.put(Constants.imrExchangeRateEntity, imrExchangeRateEntity);
        mHashMap.put(Constants.ImrBeneficiaryEntity, imrBeneficiaryEntity);
        ActivitySkipUtil.startAnotherActivity(AddBeneficiaryFiveActivity.this, ImrSendMoneyActivity.class, mHashMap,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        if ("0".equals(saveFlag)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_SEND_ADD_BENE_SUCCESS, imrBeneficiaryEntity));
            ProjectApp.removeImrAddBeneTask();
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getImrPayerDetailSuccess(ImrPayerDetailEntity imrPayerDetailEntity) {
        if(imrPayerDetailEntity != null){
            imrQueryPayerInfoInfoList.clear();
            imrQueryPayerInfoInfoList.addAll(imrPayerDetailEntity.imrQueryPayerInfoInfoList);
        }
    }

    @Override
    public void getImrPayerDetailFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void imrEditBeneficiarySuccess(ResponseEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_EDIT_BENE_SUCCESS, beneficiaryInfo,riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrEditBeneficiaryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void activateBeneficiarySuccess(Void result) {
        if(imrBeneficiaryEntity != null) {
            mPresenter.imrExchangeRate(imrBeneficiaryEntity.payeeInfoId, "", "", channelCode);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddBeneficiaryContract.AddFivePresenter getPresenter() {
        return new AddBeneFivePresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr() && showSaveList){
                    jumpToPwd(true);
                }else{
                    requestAddBene();
                }
            } else {
                requestAddBene();
            }
        } else {
            requestAddBene();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        if(showSaveList) {
            mHashMap.put(Constants.sceneType, Constants.TYPE_ADD_SEND_BENE);
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_ADD_SEND_BENE);
//            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtw_1));
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_new_15));
        }else{
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_ADD_IMR);
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_new_15));
            mHashMap.put(Constants.sceneType, Constants.TYPE_IMR_ADD_BENEFICIARY);
            if(isEditBeneficiary){
                mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_96));
                mHashMap.put("BeneficiaryInfo", beneficiaryInfo);
                mHashMap.put(Constants.IS_FROM_EDIT_BENEFICIARY, true);
            }
        }
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }


    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
