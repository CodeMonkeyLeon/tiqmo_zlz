package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SplitListEntity extends BaseEntity {

    private List<SplitDetail> splitBillSentReceivedRecordDtos;

    public List<SplitDetail> getSplitBillSentReceivedRecordDtos() {
        return this.splitBillSentReceivedRecordDtos;
    }

    public void setSplitBillSentReceivedRecordDtos(final List<SplitDetail> splitBillSentReceivedRecordDtos) {
        this.splitBillSentReceivedRecordDtos = splitBillSentReceivedRecordDtos;
    }

    public static class SplitDetail extends BaseEntity {
        private String totalAmount;         //总金额
        private String paidProportion;      //已支付占比
        private String unpaidProportion;    //未支付占比
        private String rejectedProportion;  //拒绝占比
        private String splitTitel;          //标题
        private String splitTime ;          //时间
        private String originalOrderNo ;    //原订单号
        private String currency ;           //币种
        private String orderRelevancy ;     //群组

        public String getTotalAmount() {
            return this.totalAmount;
        }

        public void setTotalAmount(final String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getPaidProportion() {
            return this.paidProportion;
        }

        public void setPaidProportion(final String paidProportion) {
            this.paidProportion = paidProportion;
        }

        public String getUnpaidProportion() {
            return this.unpaidProportion;
        }

        public void setUnpaidProportion(final String unpaidProportion) {
            this.unpaidProportion = unpaidProportion;
        }

        public String getRejectedProportion() {
            return this.rejectedProportion;
        }

        public void setRejectedProportion(final String rejectedProportion) {
            this.rejectedProportion = rejectedProportion;
        }

        public String getSplitTitel() {
            return this.splitTitel;
        }

        public void setSplitTitel(final String splitTitel) {
            this.splitTitel = splitTitel;
        }

        public String getSplitTime() {
            return this.splitTime;
        }

        public void setSplitTime(final String splitTime) {
            this.splitTime = splitTime;
        }

        public String getOriginalOrderNo() {
            return this.originalOrderNo;
        }

        public void setOriginalOrderNo(final String originalOrderNo) {
            this.originalOrderNo = originalOrderNo;
        }

        public String getCurrency() {
            return this.currency;
        }

        public void setCurrency(final String currency) {
            this.currency = currency;
        }

        public String getOrderRelevancy() {
            return this.orderRelevancy;
        }

        public void setOrderRelevancy(final String orderRelevancy) {
            this.orderRelevancy = orderRelevancy;
        }
    }

}
