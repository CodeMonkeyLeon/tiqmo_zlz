package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by YZX on 2018年11月23日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class AvatarUrlEntity {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
