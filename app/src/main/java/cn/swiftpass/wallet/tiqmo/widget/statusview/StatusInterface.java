package cn.swiftpass.wallet.tiqmo.widget.statusview;

public interface StatusInterface {

    void showLoading();

    void showNetError();

    void showEmpty();

    void dismissLoading();

    interface OnClickListener {
        void onClick();
    }
}
