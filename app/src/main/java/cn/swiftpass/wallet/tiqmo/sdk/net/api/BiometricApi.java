package cn.swiftpass.wallet.tiqmo.sdk.net.api;

import cn.swiftpass.wallet.tiqmo.sdk.entity.ASKeyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.FIOEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
import cn.swiftpass.wallet.tiqmo.sdk.entity.PreVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.net.RequestCall;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Headers;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Param;


public interface BiometricApi {

    String BIOMETRIC_TYPE_FACE = "BIO_FACE";
    String BIOMETRIC_TYPE_FINGERPRINT = "BIO_FINGER";
    String FIO_TYPE_UNLOCK = "S";
    String FIO_TYPE_PAYMENT = "P";


    @Headers({"Service-Id:1064"})
    RequestCall<JsonResponse<ASKeyEntity>> getASKey(@Param("bioscanType") String bioscanType,
                                                    @Param("brand") String brand,
                                                    @Param("model") String model,
                                                    @Param("os") String os,
                                                    @Param("platform") String platform);


    @Headers({"Service-Id:1065"})
    RequestCall<JsonResponse<FIOEntity>> getFIO(@Param("askey") String askey);


    @Headers({"Service-Id:1066"})
    RequestCall<JsonResponse<Void>> registerBiometric(@Param("askey") String askey,
                                                      @Param("fioId") String fioId);


    @Headers({"Service-Id:1067"})
    RequestCall<JsonResponse<PreVerifyEntity>> preVerify(@Param("fioId") String fioId,
                                                         @Param("operateType") String operateType);


    @Headers({"Service-Id:1068"})
    RequestCall<JsonResponse<Void>> verify(@Param("fioId") String fioId,
                                           @Param("preAuthId") String preAuthId);


    @Headers({"Service-Id:1069"})
    RequestCall<JsonResponse<Void>> logoutBiometric(@Param("bioscanType") String bioscanType);
}
