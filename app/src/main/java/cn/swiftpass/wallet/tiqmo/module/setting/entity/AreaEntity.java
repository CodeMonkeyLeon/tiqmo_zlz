package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 根据区号切换机构号 公钥等配置信息
 */
public class AreaEntity extends BaseEntity {

    public String callingCode;
    public String publicKey;
    public String partnerNo;
    public String countryName;
    public int countryImgId;

    public String getShowCallingCode() {
        return "+" + callingCode;
    }

    public AreaEntity(final String callingCode, final String publicKey, final String partnerNo, String countryName, int countryImgId) {
        this.callingCode = callingCode;
        this.publicKey = publicKey;
        this.partnerNo = partnerNo;
        this.countryName = countryName;
        this.countryImgId = countryImgId;
    }
}
