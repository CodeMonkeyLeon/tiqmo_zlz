package cn.swiftpass.wallet.tiqmo.module.imr.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SelectInfoAdapter extends BaseRecyclerAdapter<SelectInfoEntity> {

    private boolean isVisible;

    public void setSearchVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public SelectInfoAdapter(@Nullable List<SelectInfoEntity> data) {
        super(R.layout.item_select_info, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SelectInfoEntity item, int voucherCategory) {
        if (item == null) {
            return;
        }

        if (mDataList.size() > (isVisible ? 5 : 9) && voucherCategory == mDataList.size() - 1) {
            baseViewHolder.setGone(R.id.view, false);
        } else {
            baseViewHolder.setGone(R.id.view, true);
        }

        ImageView ivIcon = baseViewHolder.getView(R.id.iv_icon);
        ImageView ivArrow = baseViewHolder.getView(R.id.iv_arrow);
        TextView tvContent = baseViewHolder.getView(R.id.tv_label_content);

        LocaleUtils.viewRotationY(mContext, ivArrow);

        if (!TextUtils.isEmpty(item.imgUrl)) {
            Glide.with(mContext).clear(ivIcon);
            Glide.with(mContext)
                    .load(item.imgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivIcon);
        } else {
            ivIcon.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(item.businessParamValue)) {
            tvContent.setText(item.businessParamValue);
        }
        baseViewHolder.addOnClickListener(R.id.ll_imr_info);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mDataList.get(i).getSortLetter();
            if (TextUtils.isEmpty(sortStr)) {
                return -1;
            }
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }
}
