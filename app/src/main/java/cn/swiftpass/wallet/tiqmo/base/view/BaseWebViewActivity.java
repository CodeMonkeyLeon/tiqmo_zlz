package cn.swiftpass.wallet.tiqmo.base.view;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CustomDialog;

public abstract class BaseWebViewActivity<P extends BasePresenter> extends BaseCompatActivity<P> {

    private String[] perms = {Manifest.permission.CALL_PHONE};
    private final int PERMS_REQUEST_CODE = 201;
    private ValueCallback<Uri[]> mValueCallback;
    protected static final String TAG = "WebViewActivity";
    private CustomDialog mDealDialog;
    private String tel;
    protected String mOriginalUrl;
    protected String lan;
    protected String currencyShow;
    protected String theme;
    private WebView webView;

    @Override
    protected void init(Bundle savedInstanceState) {
        lan = SpUtils.getInstance().getAppLanguage();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleUtils.updateResources(newBase));
    }

    protected void initWebView(WebView webView, String url, boolean isCardDetail) {
        LogUtils.i(TAG, "original url :" + url);
        this.webView = webView;
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                mValueCallback = filePathCallback;
                startActivityForResult(fileChooserParams.createIntent(), 1);
                return true;
            }

        });

        currencyShow = LocaleUtils.getCurrencyShow();
        theme = ThemeUtils.getCurrentTheme(mContext);

        WebSettings settings = webView.getSettings();
        String userAgent = settings.getUserAgentString();
        settings.setUserAgentString(userAgent + " ;" + BuildConfig.UserAgent);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setDomStorageEnabled(true);
        // 设置WebView可触摸放大缩小
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        //隐藏掉缩放工具
        settings.setDisplayZoomControls(false);

        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setAllowUniversalAccessFromFileURLs(false);

        if (!isCardDetail) {
            if (url.contains("?")) {
                url = url + "&theme=" + theme + "&currencyShow=" + currencyShow;
            } else {
                url = url + "?theme=" + theme + "&currencyShow=" + currencyShow;
            }
        }
        webView.loadUrl(url);
        webView.setLongClickable(true);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }


    protected void setWebViewClient(final WebView webView) {
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                LogUtils.i(TAG, "shouldOverrideUrlLoading:" + url);
                if (url.endsWith(".pdf")) {
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else if (url.startsWith("tel:")) {
                    showCallPhoneDialog(url);
                } else if (url.contains("token%3Dabc")) {
                    finish();
                }else if(url.contains(Constants.shopperResultURL)){
                    request3ds(url,2);
                } else {
                    view.loadUrl(url);
                    if (url.contains("3ds") && url.contains("sid_")) {
                        request3ds(url,1);
                    }
                }
                return true;
            }


            @Override
            public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                super.onReceivedClientCertRequest(view, request);
                LogUtils.i(TAG, "onReceivedClientCertRequest:" + view.getUrl());
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                LogUtils.i(TAG, "onReceivedError:" + error.toString());
          /*      if (isOpenErrorHint) {
                    // 断网或者网络连接超时
                    int errorCode = error.getErrorCode();
                    if (errorCode == ERROR_PROXY_AUTHENTICATION || errorCode == ERROR_HOST_LOOKUP || errorCode == ERROR_CONNECT || errorCode == ERROR_TIMEOUT) {
                        // 避免出现默认的错误界面
                        if (!"about:blank".equals(view.getUrl())) {
                            view.loadUrl("about:blank");
//                            showErrorMsgDialog(view.getContext(), getString(ErrorCode.CONTENT_TIME_OUT.msgId));
                        }else {
                            if (webView.canGoBack()) {
                                webView.goBack();
                            } else {
                                onBackPressed();
                            }
                        }
                    }
                }*/
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.i(TAG, "onReceivedHttpError:" + errorResponse.toString());
            }

        });
    }

    protected void request3ds(String url,int operateType) {
    }


//    private static void addCardSuccess(CardNotifyEntity cardNotifyEntity, Context context) {
//        //绑卡成功
//        HashMap<String, Object> mHashMaps = new HashMap<>(16);
//        mHashMaps.put(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
//        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
//            if ("000000".equals(cardNotifyEntity.respCode)) {
//                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AddCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
//            } else {
//                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AddCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
//            }
////            ProjectApp.addCardSuccess = false;
//        } else {
//            Intent it = new Intent(context, LoginFastActivity.class);
//            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            it.putExtra(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
//            ProjectApp.addCardSuccess = true;
//            context.startActivity(it);
//        }
//    }

//    private static void addMoneySuccess(CardNotifyEntity cardNotifyEntity, Context context) {
//        //充值成功
//        HashMap<String, Object> mHashMaps = new HashMap<>(16);
//        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
//        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
//            ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
//            ProjectApp.addMoneySuccessOnMain = false;
//        } else {
//            Intent it = new Intent(context, LoginFastActivity.class);
//            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            it.putExtra(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
//            context.startActivity(it);
//        }
//    }

    @Override
    protected void onRequestPermissionsResult(int requestCode, boolean isSuccess, String[] deniedPermissions) {
        super.onRequestPermissionsResult(requestCode, isSuccess, deniedPermissions);
        if (requestCode == PERMS_REQUEST_CODE) {
            if (isSuccess) {
                callPhoneNumber(tel);
            }
        }
    }


    public void callPhone(String phoneNum) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            //Android 6.0以上版本需要获取权限
            requestPermissionsInCompatMode(perms, PERMS_REQUEST_CODE);//请求权限
        } else {
            callPhoneNumber(phoneNum);
        }
    }

    private void callPhoneNumber(String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        startActivity(intent);
    }

    protected void showCallPhoneDialog(String url) {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        tel = url.replace("tel:", "");

        if (TextUtils.isEmpty(tel)) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(tel);
        builder.setNegativeButton(getString(R.string.common_6), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.wtw_11), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callPhone(tel);
            }
        }).setLeftColor(R.color.color_007AFF).setRightColor(R.color.color_007AFF);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
