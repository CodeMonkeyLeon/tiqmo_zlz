package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillTypeIconEntity extends BaseEntity {

    public String imgUrl;
    public String billerDescription;
    public String billerDescriptionShow;

}
