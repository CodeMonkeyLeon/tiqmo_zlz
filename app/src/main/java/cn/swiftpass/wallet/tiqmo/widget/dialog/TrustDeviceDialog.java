package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class TrustDeviceDialog extends BottomDialog{
    private TrustDeviceListener trustDeviceListener;

    public void setTrustDeviceListener(TrustDeviceListener trustDeviceListener) {
        this.trustDeviceListener = trustDeviceListener;
    }

    public interface TrustDeviceListener {
        void trust();
        void skip();
    }

    public TrustDeviceDialog(Context context) {
        super(context);
        initViews(context);
    }

    private void initViews(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_trust_device, null);
        TextView tvTrust = view.findViewById(R.id.tv_trust);
        TextView tvSkip = view.findViewById(R.id.tv_skip);
        ImageView ivCircle = view.findViewById(R.id.iv_circle);
        LocaleUtils.viewRotationY(context,ivCircle);

        tvTrust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (trustDeviceListener != null) {
                    trustDeviceListener.trust();
                }
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (trustDeviceListener != null) {
                    trustDeviceListener.skip();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.width = AndroidUtils.getScreenWidth(context);
            layoutParams.height = AndroidUtils.getScreenHeight(context);

            //设置dialog沉浸式效果
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                dialogWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                dialogWindow.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                dialogWindow.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            }
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
