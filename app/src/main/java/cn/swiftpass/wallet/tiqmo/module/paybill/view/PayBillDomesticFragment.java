package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillPaymentsAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillOrderListPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class PayBillDomesticFragment extends BaseFragment<PayBillContract.BillOrderListPresenter> implements PayBillContract.BillOrderListView {

    @BindView(R.id.tv_payments_title)
    TextView tvPaymentsTitle;
    @BindView(R.id.iv_filter)
    ImageView ivFilter;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.ll_top)
    LinearLayout llTop;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;
    @BindView(R.id.ry_payments_history)
    RecyclerView ryPaymentsHistory;
    @BindView(R.id.sw_payments)
    SwipeRefreshLayout swPayments;
    @BindView(R.id.view_bottom)
    View viewBottom;

    private String domesticFlag = "Y";
    private FilterEntity filterEntity;

    private PayBillPaymentsAdapter payBillPaymentsAdapter;

    private List<String> countryCodeList = new ArrayList<>();

    private List<PayBillOrderEntity> domesticList = new ArrayList<>();

    private int historyPosition;

    private PayBillOrderListEntity payBillOrderListEntity;

    private StatusView historyStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private boolean isClick;
    private List<PayBillCountryEntity> countryList = new ArrayList<>();

    private static final String mChannelCode = Constants.paybill_local;

    private String billerId;
    private String aliasName;
    private PayBillOrderEntity payBillOrderEntity;

    public static PayBillDomesticFragment getInstance(PayBillOrderListEntity payBillOrderListEntity) {
        PayBillDomesticFragment payBillDomesticFragment = new PayBillDomesticFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.paybill_billerOrderListEntity, payBillOrderListEntity);
        payBillDomesticFragment.setArguments(bundle);
        return payBillDomesticFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_paybill_payment;
    }

    @Override
    protected void initView(View parentView) {
        swPayments.setColorSchemeResources(R.color.color_B00931);
        filterEntity = new FilterEntity();

        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }

        ivFilter.setVisibility(View.GONE);
        tvFilter.setVisibility(View.GONE);

        swPayments.setVisibility(View.VISIBLE);
        initRecyclerView();
    }

    public void getPayBillOrderList(String domesticFlag) {
        mPresenter.getPayBillOrderList(countryCodeList, domesticFlag);
    }

    private void initRecyclerView() {
        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.History_filter_13));
        historyStatusView = new StatusView.Builder(mContext, ryPaymentsHistory).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();

        swPayments.setEnabled(false);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryPaymentsHistory.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryPaymentsHistory.setLayoutManager(manager);
        payBillPaymentsAdapter = new PayBillPaymentsAdapter(domesticList);
        payBillPaymentsAdapter.bindToRecyclerView(ryPaymentsHistory);

        if (getArguments() != null) {
            payBillOrderListEntity = (PayBillOrderListEntity) getArguments().getSerializable(Constants.paybill_billerOrderListEntity);
            setOrderList(payBillOrderListEntity);
        }

        payBillPaymentsAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                PayBillOrderEntity orderEntity = domesticList.get(position);
                historyPosition = position;
                switch (view.getId()) {
                    case R.id.tv_pay:
                        if (orderEntity != null) {
                            payBillOrderEntity = orderEntity;
                            billerId = payBillOrderEntity.billerId;
                            aliasName = payBillOrderEntity.alaisName;
                            mPresenter.getPayBillOrderIOList(payBillOrderEntity.billerId, payBillOrderEntity.sku, payBillOrderEntity.alaisName, mChannelCode);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void setOrderList(PayBillOrderListEntity payBillOrderListEntity) {
        domesticList.clear();
        if (payBillOrderListEntity != null) {
            List<PayBillOrderEntity> list = payBillOrderListEntity.domesticList;
            int size = list.size();
            tvPaymentsTitle.setText(getString(R.string.BillPay_30) + " (" + size + ")");
            if (size == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                domesticList.addAll(list);
                payBillPaymentsAdapter.setDataList(domesticList);
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }

    @OnClick({R.id.tv_filter, R.id.iv_filter})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_filter:
            case R.id.iv_filter:
                isClick = true;
                if (countryList.size() > 0) {
//                    showFilterDialog(countryList);
                } else {
                    mPresenter.getPayBillCountry();
                }
                break;
            default:
                break;
        }
    }

//    private void showFilterDialog(List<PayBillCountryEntity> countryList) {
//        BillPaymentFilterDialog paymentFilterDialog = new BillPaymentFilterDialog(mContext, countryList);
//        paymentFilterDialog.setApplyFilterListener(new BillPaymentFilterDialog.ApplyFilterListener() {
//            @Override
//            public void changeFilter(List<String> selectCountryCodeList, List<PayBillCountryEntity> selectCountryList) {
//                countryList.clear();
//                countryList.addAll(selectCountryList);
//                countryCodeList.clear();
//                countryCodeList.addAll(selectCountryCodeList);
//                mPresenter.getPayBillOrderList(countryCodeList, domesticFlag);
//            }
//        });
//        paymentFilterDialog.showWithBottomAnim();
//    }

    @Override
    public void getPayBillOrderIOListSuccess(PayBillIOListEntity payBillIOListEntity) {
        if (payBillIOListEntity != null) {
            String isValidBiller = payBillIOListEntity.isValidBiller;
            if ("N".equals(isValidBiller)) {
                showCustomDialog(mContext, R.string.BillPay_34, R.string.wtw_11, R.string.common_6, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPresenter.deletePayBillOrder(billerId, aliasName);
                        dialog.dismiss();
                    }
                });
            } else {
//                HashMap<String, Object> mHashMap = new HashMap<>();
//                mHashMap.put(Constants.paybill_billerIOListEntity, payBillIOListEntity);
//                mHashMap.put(Constants.paybill_billerOrderEntity, payBillOrderEntity);
//                mHashMap.put(Constants.CHANNEL_CODE, mChannelCode);
//                ActivitySkipUtil.startAnotherActivity(mActivity, PayBillFetchActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                if(payBillOrderEntity != null && payBillOrderEntity.isMoiBill()){
                    PayBillMoiFetchActivity.startBillMoiServiceFetchActivity(mActivity,
                            billerId,
                            payBillIOListEntity,
                            payBillOrderEntity,
                            mChannelCode);
                }else {
                    PayBillFetchActivity.startPayBillFetchActivity(mActivity,
                            billerId,
                            payBillIOListEntity,
                            payBillOrderEntity,
                            mChannelCode);
                }
            }
        }
    }

    @Override
    public void getPayBillOrderListSuccess(PayBillOrderListEntity payBillOrderListEntity) {
        setOrderList(payBillOrderListEntity);
    }

    @Override
    public void deletePayBillOrderSuccess(Void result) {
        mPresenter.getPayBillOrderList(countryCodeList, domesticFlag);
//        if (payBillPaymentsAdapter != null) {
//            //移除数据
//            payBillPaymentsAdapter.getDataList().remove(historyPosition);
//            payBillPaymentsAdapter.notifyDataSetChanged();
//            if (payBillPaymentsAdapter.getDataList().size() == 0) {
////                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
//            }
//        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getPayBillOrderListFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
        showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
    }

    @Override
    public void getPayBillCountrySuccess(PayBillCountryListEntity payBillCountryListEntity) {
        countryList.clear();
        if (payBillCountryListEntity != null) {
            countryList.addAll(payBillCountryListEntity.countryList);
//            showFilterDialog(countryList);
        }
    }

    @Override
    public PayBillContract.BillOrderListPresenter getPresenter() {
        return new PayBillOrderListPresenter();
    }
}
