package cn.swiftpass.wallet.tiqmo.sdk.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;

//SQLiteOpenHelper的一个简单实现
public class AppSQLiteOpenHelper extends SQLiteOpenHelper {


    AppSQLiteOpenHelper(Context context, String name, int version) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            //每多一个表，都要db.execSQL(),
            db.execSQL(InternationalCodeDao.CREATE_TABLE_SQL);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            LogUtil.d(e.toString(),e);
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }
}