package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class HomeBottomView extends LinearLayout {

    @BindView(R.id.iv_background)
    ImageView ivBackground;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.iv_money_see)
    ImageView ivMoneySee;
    @BindView(R.id.iv_go_kyc)
    ImageView iv_go_kyc;
    @BindView(R.id.con_is_kyc)
    ConstraintLayout conIsKyc;
    @BindView(R.id.tv_no_top)
    TextView tvNoTop;
    @BindView(R.id.tv_go_kyc)
    TextView tvGoKyc;
    @BindView(R.id.con_no_kyc)
    ConstraintLayout conNoKyc;
    @BindView(R.id.ll_scan)
    LinearLayout llScan;

    private Context context;
    private boolean isHideMoney = false;
    private BottomListener bottomListener;

    public void setBottomListener(BottomListener bottomListener) {
        this.bottomListener = bottomListener;
    }

    public HomeBottomView(Context context) {
        super(context);
        initView(context);
    }

    public HomeBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        this.context = context;
        inflate(context, R.layout.view_home_bottom, this);
        ButterKnife.bind(this);
        setFocusable(true);
        LocaleUtils.viewRotationY(context, ivBackground, iv_go_kyc);
    }

    public void setAmount(String balance) {
        tvAmount.setText(AndroidUtils.getTransferMoney(balance));
        invalidate();
    }

    public void setMoneyHide() {
        isHideMoney = SpUtils.getInstance().getHidePwd();
        if (isHideMoney) {
            ivMoneySee.setImageResource(R.drawable.d_money_hide);
            tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        } else {
            ivMoneySee.setImageResource(R.drawable.d_money_show);
            tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        invalidate();
    }

    public void setUserInfo(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String balance = userInfoEntity.getBalance();
            tvCurrency.setText(LocaleUtils.getCurrencyCode(""));
            if (userInfoEntity.isKyc()) {
                if (!TextUtils.isEmpty(balance)) {
                    tvAmount.setText(balance);
                }
                conIsKyc.setVisibility(View.VISIBLE);
                conNoKyc.setVisibility(View.GONE);
            } else {
                conIsKyc.setVisibility(View.GONE);
                conNoKyc.setVisibility(View.VISIBLE);
            }
        }

        ivBackground.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                return;
            }
        });

        ivMoneySee.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (isHideMoney) {
                    //如果已经隐藏了金额  则显示
                    isHideMoney = false;
                    ivMoneySee.setImageResource(R.drawable.d_money_show);
                    tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    isHideMoney = true;
                    ivMoneySee.setImageResource(R.drawable.d_money_hide);
                    tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
                }
                SpUtils.getInstance().setHidePwd(isHideMoney);
            }
        });

        conNoKyc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (bottomListener != null) {
                    bottomListener.kycClick();
                }
            }
        });

        llScan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (bottomListener != null) {
                    bottomListener.scanClick();
                }
            }
        });
    }

    public interface BottomListener {
        void scanClick();

        void kycClick();
    }
}
