package cn.swiftpass.wallet.tiqmo.sdk.net.api;


import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardListEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonResponse;
import cn.swiftpass.wallet.tiqmo.sdk.net.RequestCall;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Headers;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.Param;

/**
 * Created by 叶智星 on 2018年09月18日.
 * 每一个不曾起舞的日子，都是对生命的辜负。
 */
public interface CardApi {

    String CARD_LIST_TYPE_ALL = "A";
    String CARD_LIST_TYPE_CREDIT = "C";
    String CARD_LIST_TYPE_DEBIT = "D";
    String CARD_LIST_TYPE_SEMI = "S";
    String CARD_LIST_TYPE_PREPAID = "P";

   /* @Headers({"Service-Id:1023"})
    RequestCall<JsonResponse<AllCardEntity>> getAllCardByType(@Param("type") String operatingType);


    @Headers({"Service-Id:2011"})
    RequestCall<JsonResponse<Void>> getCardDetail(@Param("cardId") String cardID);


    @Headers({"Service-Id:1028"})
    RequestCall<JsonResponse<Void>> unbindCard(@Param("cardId") String cardID);


    @Headers({"Service-Id:1024"})
    RequestCall<JsonResponse<BindCardOPTEntity>> obtainOPTToBindBankCard(@Param("firstName") String firstName,
                                                                         @Param("lastName") String lastName,
                                                                         @Param("gender") String gender,
                                                                         @Param("birthday") String birthday,
                                                                         @Param("idType") String idType,
                                                                         @Param("idNo") String idNo,
                                                                         @Param("phoneNumber") String phoneNumber,
                                                                         @Param("cardNo") String cardNo,
                                                                         @Param("cardType") String cardType,
                                                                         @Param("expire") String expire,
                                                                         @Param("cvv") String cvv);


    @Headers({"Service-Id:1026"})
    RequestCall<JsonResponse<CardEntity>> bindBankCard(@Param("cardNo") String cardNo,
                                                       @Param("otpCode") String otpCode);

    @Headers({"Service-Id:1029"})
    RequestCall<JsonResponse<CardTypeEntity>> getCardType(@Param("cardNo") String cardNo,
                                                          @Param("cardId") String cardID);

    @Headers({"Service-Id:1049"})
    RequestCall<JsonResponse<CheckOTPEntity>> obtainBankCardOPTToCheck(@Param("cardNo") String cardNo,
                                                                       @Param("protocolNo") String cardID);

    @Headers({"Service-Id:1050"})
    RequestCall<JsonResponse<CheckOTPEntity>> checkBankCardOTP(@Param("protocolNo") String cardID,
                                                               @Param("verificationCode") String otbCode);*/

    @Headers({"Service-Id:3009"})
    RequestCall<JsonResponse<AllCardEntity>> getBankCardList(@Param("cardType") String cardType);
    @Headers({"Service-Id:3009"})
    RequestCall<JsonResponse<AllCardEntity>> getBankCardList();

    @Headers({"Service-Id:3011"})
    RequestCall<JsonResponse<CardBind3DSEntity>> bindCard(@Param("custName") String custName,
                                                          @Param("cardNo") String cardNo,
                                                          @Param("expire") String expire,
                                                          @Param("cvv") String cvv,
                                                          @Param("type") String type);

    @Headers({"Service-Id:3012"})
    RequestCall<JsonResponse<unBindCardEntity>> unBindCard(@Param("protocolNo") String protocolNo);

    @Headers({"Service-Id:3047"})
    RequestCall<JsonResponse<Void>> setDefaultCard(@Param("protocolNo") String protocolNo);

    @Headers({"Service-Id:3051"})
    RequestCall<JsonResponse<verifyCardEntity>> verifyCard(@Param("cardNo") String cardNo);

    @Headers({"Service-Id:3125"})
    RequestCall<JsonResponse<Void>> openKsaCardAccount(@Param("productType") String productType);

    @Headers({"Service-Id:3126"})
    RequestCall<JsonResponse<Void>> setKsaCardPin(@Param("pin") String pin,
                                                  @Param("proxyCardNo") String proxyCardNo);

    @Headers({"Service-Id:3127"})
    RequestCall<JsonResponse<KsaCardListEntity>> getKsaCardList();

    @Headers({"Service-Id:3128"})
    RequestCall<JsonResponse<KsaCardSummaryEntity>> getKsaCardSummary(@Param("cardType") String cardType,
                                                                      @Param("cardProductType") String cardProductType);

    @Headers({"Service-Id:3129"})
    RequestCall<JsonResponse<KsaPayResultEntity>> getKsaCardPayResult(@Param("vat") String vat,
                                                                      @Param("totalAmount") String totalAmount,
                                                                      @Param("appNiOpenCardReq") OpenCardReqEntity appNiOpenCardReq);

//    @Headers({"Service-Id:3130"})
//    RequestCall<JsonResponse<KsaPayResultEntity>> openKsaCardNoFee(@Param("cardProductType") String cardProductType,
//                                                                   @Param("cardType") String cardType,
//                                                                   @Param("cardFaceId") String cardFaceId,
//                                                                   @Param("province") String province,
//                                                                   @Param("city") String city,
//                                                                   @Param("address") String address,
//                                                                   @Param("pinBean") CardPinEntity pinBean,
//                                                                   @Param("cardOfName") String cardOfName);

    @Headers({"Service-Id:3246"})
    RequestCall<JsonResponse<Void>> activateCard(@Param("proxyCardNo") String proxyCardNo,
                                                 @Param("cardNo") String cardNo,
                                                 @Param("cardExpire") String cardExpire);

    @Headers({"Service-Id:3247"})
    RequestCall<JsonResponse<TransferHistoryMainEntity>> getKsaCardHistory(@Param("protocolNo") String protocolNo,
                                                                           @Param("accountNo") String accountNo,
                                                                           @Param("paymentType") List<String> paymentType,
                                                                           @Param("tradeType") List<String> tradeType,
                                                                           @Param("startDate") String startDate,
                                                                           @Param("endDate") String endDate,
                                                                           @Param("pageSize") int pageSize,
                                                                           @Param("pageNum") int pageNum,
                                                                           @Param("direction") String direction,
                                                                           @Param("orderNo") String orderNo);

    @Headers({"Service-Id:3108"})
    RequestCall<JsonResponse<CardDetailEntity>> getKsaCardDetails(@Param("proxyCardNo") String proxyCardNo);

    @Headers({"Service-Id:3114"})
    RequestCall<JsonResponse<Void>> setKsaCardStatus(@Param("status") String status,
                                                     @Param("proxyCardNo") String proxyCardNo,
                                                     @Param("reason") String reason,
                                                     @Param("reasonStatus") String reasonStatus);

    @Headers({"Service-Id:3111"})
    RequestCall<JsonResponse<KsaLimitChannelEntity>> getKsaLimitChannel(@Param("currencyCode") String currencyCode,@Param("proxyCardNo") String proxyCardNo);

    @Headers({"Service-Id:3112"})
    RequestCall<JsonResponse<Void>> setKsaDayCardLimit(@Param("txnType") String txnType,
                                                       @Param("currencyCode") String currencyCode,
                                                       @Param("dailyLimitValue") String dailyLimitValue,
                                                       @Param("proxyCardNo") String proxyCardNo,
                                                       @Param("up") String up);

    @Headers({"Service-Id:3112"})
    RequestCall<JsonResponse<Void>> setKsaMonthCardLimit(@Param("txnType") String txnType,
                                                         @Param("currencyCode") String currencyCode,
                                                         @Param("monthlyLimitValue") String monthlyLimitValue,
                                                         @Param("proxyCardNo") String proxyCardNo,
                                                         @Param("up") String up);

    @Headers({"Service-Id:3112"})
    RequestCall<JsonResponse<Void>> setKsaCardChannel(@Param("txnType") String txnType,
                                                      @Param("currencyCode") String currencyCode,
                                                      @Param("txnTypeEnabled") String txnTypeEnabled,
                                                      @Param("proxyCardNo") String proxyCardNo);

    @Headers({"Service-Id:3254"})
    RequestCall<JsonResponse<Void>> activateCardResult();
}
