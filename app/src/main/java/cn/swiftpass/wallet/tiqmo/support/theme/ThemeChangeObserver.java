package cn.swiftpass.wallet.tiqmo.support.theme;

public interface ThemeChangeObserver {

    void notifyByThemeChanged();
}
