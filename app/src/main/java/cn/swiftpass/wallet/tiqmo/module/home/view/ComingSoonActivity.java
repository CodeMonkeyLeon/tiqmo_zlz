package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;

public class ComingSoonActivity extends BaseCompatActivity {

    private ComingSoonFragment comingSoonFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_coming_soon;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        String title = getIntent().getExtras().getString("title");
        comingSoonFragment = ComingSoonFragment.getInstance(title);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fl_coming_soon, comingSoonFragment);
        ft.commit();
    }
}
