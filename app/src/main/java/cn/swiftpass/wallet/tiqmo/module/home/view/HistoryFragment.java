package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.home.contract.HistoryContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HistoryListParamEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.home.interfaces.HistoryFragmentImp;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.HistoryPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.UnScrollViewPager;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMainPagerAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.KotlinUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class HistoryFragment extends BaseFragment<HistoryContract.Presenter> implements HistoryContract.View {

    //    @BindView(R.id.tv_background)
//    TextView tvBackground;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.id_img_no_title_head_circle)
    ImageView mImgNoTitleHeadCircle;

    @BindView(R.id.id_img_head_circle)
    ImageView mImgHeadCircle;

    //    @BindView(R.id.iv_kyc_step)
//    ImageView ivKycStep;
//    @BindView(R.id.common_head)
//    ConstraintLayout commonHead;

    @BindView(R.id.vp_history_main)
    UnScrollViewPager vpAnalyticsMain;
    @BindView(R.id.bg_main)
    ConstraintLayout bgMain;


    @BindView(R.id.tv_history_title)
    TextView tvHistoryTitle;

    @BindView(R.id.iv_filter)
    ImageView ivFilter;

    @BindView(R.id.tv_filter)
    TextView tvFilter;


    @BindView(R.id.id_ll_back_head)
    LinearLayout mLlBackHead;

    @BindView(R.id.id_ll_no_title_head)
    LinearLayout mLlNoTitleHead;

    @BindView(R.id.indicator_history_main)
    MagicIndicator indicatorAnalyticsMain;

    private CommonNavigator commonNavigator;

    private int currentPosition;

    private List<String> mTitle;


    private String isFrom;


    private boolean isClick;

    private List<Fragment> mFragments;

    private ImrMainPagerAdapter mPagerAdapter;

    private TransactionHistoryFragment transactionHistoryFragment;
    private PreAuthorizationsHistoryFragment preAuthorizationHistoryFragment;

    private HistoryFragmentImp mTransactionImp;

    private HistoryFragmentImp mPreAuthorizationImp;

    private FilterListEntity filterListEntity;


    @Override
    protected int getLayoutID() {
        return R.layout.fragment_ksa_history;
    }


    public static HistoryFragment getInstance(FilterEntity filterEntity, String isFrom) {
        HistoryFragment historyFragment = new HistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("FilterEntity", filterEntity);
        bundle.putString(Constants.isFrom, isFrom);
        historyFragment.setArguments(bundle);
        return historyFragment;
    }


    /**
     * 预收款页面是否展示
     *
     * @return
     */
    private boolean isPreAuthShow() {
        if (indicatorAnalyticsMain.getVisibility() == View.GONE) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundResourceByAttr(bgMain, R.attr.bg_040f33_white);

        if (isPreAuthShow()) {
            //交易历史和预授权历史
            KotlinUtils.INSTANCE.changePngToWhite(getContext(), ivFilter, R.drawable.d_filter);
            tvHistoryTitle.setTextColor(getContext().getColor(R.color.white));
            tvFilter.setTextColor(getContext().getColor(R.color.white));
        } else {
            //只有交易历史
            if (ThemeUtils.isCurrentDark(mContext)) {
                //暗色模式
                KotlinUtils.INSTANCE.changePngToWhite(getContext(), ivFilter, R.drawable.d_filter);
                tvHistoryTitle.setTextColor(getContext().getColor(R.color.white));
                tvFilter.setTextColor(getContext().getColor(R.color.white));
            } else {
                //亮色模式
                ivFilter.setImageResource(R.drawable.l_filter);
                tvHistoryTitle.setTextColor(getContext().getColor(R.color.color_090a15_light));
                tvFilter.setTextColor(getContext().getColor(R.color.color_061f6f));
            }
        }

        if (indicatorAnalyticsMain != null) {
            indicatorAnalyticsMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
            if (commonNavigator != null) {
                commonNavigator.setAdapter(new CommonNavigatorAdapter() {
                    @Override
                    public int getCount() {
                        return mTitle == null ? 0 : mTitle.size();
                    }

                    @Override
                    public IPagerTitleView getTitleView(Context context, final int index) {
                        ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                        clipPagerTitleView.setText(mTitle != null ? mTitle.get(index) : "");
                        clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                        clipPagerTitleView.setTextColor(mContext.getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                        clipPagerTitleView.setClipColor(Color.WHITE);
                        clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf"));
                        clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf"));

                        clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (ButtonUtils.isFastDoubleClick()) {
                                    return;
                                }
                                vpAnalyticsMain.setCurrentItem(index);
                            }
                        });
                        return clipPagerTitleView;
                    }

                    @Override
                    public IPagerIndicator getIndicator(Context context) {
                        LinePagerIndicator indicator = new LinePagerIndicator(context);
                        indicator.setLineHeight(UIUtil.dip2px(context, 44));
                        indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                        indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                        return indicator;
                    }

                    @Override
                    public float getTitleWeight(Context context, int index) {
                        return 1.0f;
                    }
                });
                indicatorAnalyticsMain.setNavigator(commonNavigator);
            }


            if (transactionHistoryFragment != null) {
                transactionHistoryFragment.noticeThemeChange();
            }

            if (preAuthorizationHistoryFragment != null) {
                preAuthorizationHistoryFragment.noticeThemeChange();
            }
        }
    }


    @Override
    protected void initView(View parentView) {
        if (getArguments() != null) {
            isFrom = getArguments().getString(Constants.isFrom);
        }
        LocaleUtils.viewRotationY(mContext, mImgHeadCircle, mImgNoTitleHeadCircle);
        initLayout();
    }


    private void initLayout() {
        try {
            tvTitle.setVisibility(View.GONE);
            if (getArguments() != null) {
                currentPosition = getArguments().getInt("position");
                ivBack.setVisibility(View.GONE);
            }
            mTitle = new ArrayList<>(Arrays.asList(this.getString(R.string.trx_detail_12),
                    this.getString(R.string.sprint21_20)));
            mFragments = new ArrayList<>();
            transactionHistoryFragment = TransactionHistoryFragment.getInstance(isFrom);
            mTransactionImp = (HistoryFragmentImp) transactionHistoryFragment;
            preAuthorizationHistoryFragment = PreAuthorizationsHistoryFragment.getInstance(isFrom);
            mPreAuthorizationImp = (HistoryFragmentImp) preAuthorizationHistoryFragment;
            mFragments.add(transactionHistoryFragment);
            mFragments.add(preAuthorizationHistoryFragment);
            if (LocaleUtils.isRTL(mContext)) {
                //阿拉伯语
                Collections.reverse(mTitle);
                Collections.reverse(mFragments);
            }
            mPagerAdapter = new ImrMainPagerAdapter(getChildFragmentManager(), mFragments);
            vpAnalyticsMain.setScrollEnable(false);// 禁止滑动
            vpAnalyticsMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    currentPosition = position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            vpAnalyticsMain.setAdapter(mPagerAdapter);
            if (LocaleUtils.isRTL(mContext)) {
                vpAnalyticsMain.setCurrentItem(mTitle.size() - 1);
            }
            commonNavigator = new CommonNavigator(mContext);
            commonNavigator.setAdjustMode(true);
            indicatorAnalyticsMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
            commonNavigator.setAdapter(new CommonNavigatorAdapter() {
                @Override
                public int getCount() {
                    return mTitle == null ? 0 : mTitle.size();
                }

                @Override
                public IPagerTitleView getTitleView(Context context, final int index) {
                    ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                    clipPagerTitleView.setText(mTitle.get(index));
                    clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                    clipPagerTitleView.setTextColor(mContext.getResources().getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                    clipPagerTitleView.setClipColor(Color.WHITE);
                    clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf"));
                    clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Semi-Bold.ttf" : "fonts/SFProDisplay-Semibold.ttf"));

                    clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ButtonUtils.isFastDoubleClick()) {
                                return;
                            }
                            vpAnalyticsMain.setCurrentItem(index);
                        }
                    });
                    return clipPagerTitleView;
                }

                @Override
                public IPagerIndicator getIndicator(Context context) {
                    LinePagerIndicator indicator = new LinePagerIndicator(context);
                    indicator.setLineHeight(UIUtil.dip2px(context, 44));
                    indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                    indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                    return indicator;
                }

                @Override
                public float getTitleWeight(Context context, int index) {
                    return 1.0f;
                }
            });
            indicatorAnalyticsMain.setNavigator(commonNavigator);
            commonNavigator.onPageSelected(LocaleUtils.isRTL(mContext) ? mTitle.size() - 1 : 0);
            ViewPagerHelper.bind(indicatorAnalyticsMain, vpAnalyticsMain);

            if (currentPosition == 0) {
                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
            } else {
                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 0 : 1);
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
        showPreAuthLayout(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvFilter.setEnabled(true);
        ivFilter.setEnabled(true);
    }


    private boolean isTransactionPosition() {
        if (isPreAuthShow()) {
            //交易历史和预授权历史都展示
            if (LocaleUtils.isRTL(mContext)) {
                //阿拉伯语
                if (currentPosition == 1) {
                    return true;
                }
            } else {
                //英语
                if (currentPosition == 0) {
                    return true;
                }
            }
        } else {
            //只展示交易历史
            return true;
        }
        return false;
    }


    @OnClick({R.id.tv_filter, R.id.iv_filter})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_filter:
            case R.id.iv_filter:
                isClick = true;
                if (isTransactionPosition()) {
                    mTransactionImp.isClick(isClick);
                } else {
                    mPreAuthorizationImp.isClick(isClick);
                }

                if (filterListEntity != null) {
                    if (isTransactionPosition()) {
                        mTransactionImp.showFilterHistoryDialog(filterListEntity);
                    } else {
                        mPreAuthorizationImp.showFilterHistoryDialog(filterListEntity);
                    }
                } else {
                    if (isTransactionPosition()) {
                        getFilterList(false);
                    } else {
                        getFilterList(true);
                    }
                }
                break;
            default:
                break;
        }
    }


    @Override
    protected void initData() {
        noticeThemeChange();
    }


    private void getHistoryListData() {

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (Constants.from_evoucher.equals(isFrom)) {
                            getFilterList(false);
                        } else if (Constants.from_notification_center.equals(isFrom)) {
                            transactionHistoryFragment.setRefreshing(true);
                            getList(false);
                        } else {
                            transactionHistoryFragment.setRefreshing(true);
                            getList(false);
                        }
                    }
                }
                , 200);
    }

    private void getPreAuthListData() {
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        getList(true);
                    }
                }, 300
        );
    }


    private void getList(boolean isPreAuth) {
        if (isPreAuth) {
            //预授权历史
            HistoryListParamEntity param = mPreAuthorizationImp.updateGetListStatus();
            mPresenter.getHistoryList(
                    param.getProtocolNo(),
                    param.getAccountNo(),
                    param.getTypeList(),
                    param.getTradeType(),
                    param.getStartDate(),
                    param.getEndDate(),
                    param.getPageSize(),
                    param.getCurrentPage(),
                    param.getDirection(),
                    param.getOrderNo(),
                    true
            );
        } else {
            //交易历史
            HistoryListParamEntity param = mTransactionImp.updateGetListStatus();
            mPresenter.getHistoryList(
                    param.getProtocolNo(),
                    param.getAccountNo(),
                    param.getTypeList(),
                    param.getTradeType(),
                    param.getStartDate(),
                    param.getEndDate(),
                    param.getPageSize(),
                    param.getCurrentPage(),
                    param.getDirection(),
                    param.getOrderNo(),
                    false
            );
        }
    }


    private void getFilterList(boolean isPreAuth) {
        if (isPreAuth) {
            //预授权历史
            mPresenter.getFilterList(
                    preAuthorizationHistoryFragment.getClickStatus(),
                    true
            );
        } else {
            //交易历史
            mPresenter.getFilterList(
                    transactionHistoryFragment.getClickStatus(),
                    false
            );
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {

        switch (event.getEventType()) {
            case EventEntity.EVENT_HISTORY_INIT_DATA:
                //初始化数据
                vpAnalyticsMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
                getHistoryListData();
                getPreAuthListData();
                break;

            case EventEntity.EVENT_HISTORY_REFRESH:
                //交易历史列表
                HistoryListParamEntity param = event.getHistoryListParamEntity();
                mPresenter.getHistoryList(
                        param.getProtocolNo(),
                        param.getAccountNo(),
                        param.getTypeList(),
                        param.getTradeType(),
                        param.getStartDate(),
                        param.getEndDate(),
                        param.getPageSize(),
                        param.getCurrentPage(),
                        param.getDirection(),
                        param.getOrderNo(),
                        false
                );
                break;
            case EventEntity.EVENT_PRE_AUTH_HISTORY_REFRESH:
                //预授权历史列表
                HistoryListParamEntity paramPre = event.getHistoryListParamEntity();
                mPresenter.getHistoryList(
                        paramPre.getProtocolNo(),
                        null,
                        null,
                        null,
                        paramPre.getStartDate(),
                        paramPre.getEndDate(),
                        paramPre.getPageSize(),
                        paramPre.getCurrentPage(),
                        paramPre.getDirection(),
                        paramPre.getOrderNo(),
                        true
                );
                break;
            case EventEntity.EVENT_HISTORY_DETAIL:
                //交易历史详情
                HistoryDetailEntity detail = event.getHistoryDetailEntity();
                mPresenter.getHistoryDetail(
                        detail.getOrderNo(),
                        detail.getOrderType(),
                        detail.getQueryType(),
                        false
                );
                break;

            case EventEntity.EVENT_PRE_AUTH_HISTORY_DETAIL:
                //预授权历史详情
                HistoryDetailEntity detailPre = event.getHistoryDetailEntity();
                mPresenter.getHistoryDetail(
                        detailPre.getOrderNo(),
                        detailPre.getOrderType(),
                        null,
                        true
                );
                break;

            case EventEntity.EVENT_HISTORY_DELETE:
                //交易历史删除
                String orderNo = event.getEventMsg();
                mPresenter.deleteTransferHistory(
                        orderNo,
                        false);
                break;

            case EventEntity.EVENT_PRE_AUTH_HISTORY_DELETE:
                //预授权历史删除
                String orderNoPre = event.getEventMsg();
                mPresenter.deleteTransferHistory(
                        orderNoPre,
                        true);
                break;
            default:
                break;
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void restart() {

    }


    @Override
    public HistoryContract.Presenter getPresenter() {
        return new HistoryPresenter();
    }


    private void showPreAuthLayout(boolean isShow) {
        if (isShow) {
            //展示预授权布局
            mLlBackHead.setVisibility(View.VISIBLE);
            mLlNoTitleHead.setVisibility(View.GONE);
            indicatorAnalyticsMain.setVisibility(View.VISIBLE);
            KotlinUtils.INSTANCE.changePngToWhite(getContext(), ivFilter, R.drawable.d_filter);
            tvHistoryTitle.setTextColor(getContext().getColor(R.color.white));
            tvFilter.setTextColor(getContext().getColor(R.color.white));

            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) vpAnalyticsMain.getLayoutParams();
            layoutParams.topMargin = AndroidUtils.dip2px(getContext(), 160);
            vpAnalyticsMain.setLayoutParams(layoutParams);

            mTitle = new ArrayList<>(Arrays.asList(this.getString(R.string.trx_detail_12),
                    this.getString(R.string.sprint21_20)));
            if (LocaleUtils.isRTL(mContext)) {
                //阿拉伯语
                Collections.reverse(mTitle);
            }

            mPagerAdapter.notifyDataSetChanged();
            commonNavigator.getAdapter().notifyDataSetChanged();
        } else {
            //隐藏预授权布局
            mPreAuthorizationImp.hideProcess();
            mLlBackHead.setVisibility(View.GONE);
            mLlNoTitleHead.setVisibility(View.VISIBLE);
            indicatorAnalyticsMain.setVisibility(View.GONE);

            if (ThemeUtils.isCurrentDark(mContext)) {
                //暗色模式
                KotlinUtils.INSTANCE.changePngToWhite(getContext(), ivFilter, R.drawable.d_filter);
                tvHistoryTitle.setTextColor(getContext().getColor(R.color.white));
                tvFilter.setTextColor(getContext().getColor(R.color.white));
            } else {
                //亮色模式
                ivFilter.setImageResource(R.drawable.l_filter);
                tvHistoryTitle.setTextColor(getContext().getColor(R.color.color_090a15_light));
                tvFilter.setTextColor(getContext().getColor(R.color.color_061f6f));
            }

            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) vpAnalyticsMain.getLayoutParams();
            layoutParams.topMargin = AndroidUtils.dip2px(getContext(), 95);
            vpAnalyticsMain.setLayoutParams(layoutParams);


            mTitle = new ArrayList<>(Arrays.asList(this.getString(R.string.trx_detail_12)));

            if (LocaleUtils.isRTL(mContext)) {
                //阿拉伯语
                vpAnalyticsMain.setCurrentItem(1);
                currentPosition = 1;
            } else {
                //英语
                vpAnalyticsMain.setCurrentItem(0);
                currentPosition = 0;
            }
        }
    }

//    private boolean mIsInit = true;

    @Override
    public void getHistoryListSuccess(
            @Nullable TransferHistoryMainEntity result,
            int currentPage,
            boolean isPreAuth
    ) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
//            if (mIsInit) {
//                mIsInit = false;
//                result.transactionHistorys.clear();
//            }

            if (currentPage > 1) {
                //加载更多数据
            } else {
                //加载第一页数据
                if (result != null && result.transactionHistorys.size() > 0) {
                    //预授权列表有数据，展示预授权布局
                    showPreAuthLayout(true);
                } else {
                    //预授权列表无数据，隐藏预授权布局
                    showPreAuthLayout(false);
                }
            }


            if (isPreAuthShow()) {
                //展示交易历史和预授权历史
                if (LocaleUtils.isRTL(mContext)) {
                    //阿拉伯语
                    if (currentPosition == 0) {
                        //正处于预授权历史
                        mPreAuthorizationImp.getHistoryListSuccess(result);
                    } else {
                        //正处于交易历史
                        preAuthorizationHistoryFragment.updatePreAuthListData(result);
                    }
                } else {
                    //英语
                    if (currentPosition == 1) {
                        //正处于预授权历史
                        mPreAuthorizationImp.getHistoryListSuccess(result);
                    } else {
                        //正处于交易历史
                        preAuthorizationHistoryFragment.updatePreAuthListData(result);
                    }
                }
            }
        } else {
            //交易记录
            mTransactionImp.getHistoryListSuccess(result);
        }
    }

    @Override
    public void getHistoryListFail(@Nullable String errorCode, @Nullable String errorMsg, int pageNum, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
            if (pageNum > 1) {
                //加载更多数据报错，不处理
            } else {
                //第一页报错，隐藏预授权页面
                showPreAuthLayout(false);
                mPreAuthorizationImp.getHistoryListFail(errorCode, errorMsg);
            }
        } else {
            //交易记录
            mTransactionImp.getHistoryListFail(errorCode, errorMsg);
        }
    }

    @Override
    public void getHistoryDetailSuccess(@Nullable TransferHistoryDetailEntity result, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
            mPreAuthorizationImp.getHistoryDetailSuccess(result);
        } else {
            //交易记录
            mTransactionImp.getHistoryDetailSuccess(result);
        }
    }

    @Override
    public void getHistoryDetailFail(@Nullable String errorCode, @Nullable String errorMsg, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
            mPreAuthorizationImp.getHistoryDetailFail(errorCode, errorMsg);
        } else {
            //交易记录
            mTransactionImp.getHistoryDetailFail(errorCode, errorMsg);
        }
    }

    @Override
    public void deleteTransferHistorySuccess(@Nullable Void result, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
            mPreAuthorizationImp.deleteTransferHistorySuccess(result);
        } else {
            //交易记录
            mTransactionImp.deleteTransferHistorySuccess(result);
        }
    }

    @Override
    public void deleteTransferHistoryFail(@Nullable String errorCode, @Nullable String errorMsg, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
            mPreAuthorizationImp.deleteTransferHistoryFail(errorCode, errorMsg);
        } else {
            //交易记录
            mTransactionImp.deleteTransferHistoryFail(errorCode, errorMsg);
        }
    }


    @Override
    public void getFilterListSuccess(@Nullable FilterListEntity result, boolean isClick, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
            mPreAuthorizationImp.getFilterListSuccess(result);
        } else {
            //交易记录
            mTransactionImp.getFilterListSuccess(result);
        }
    }

    @Override
    public void getFilterListFailed(@Nullable String errorCode, @Nullable String errorMsg, boolean isClick, boolean isPreAuth) {
        showProgress(false);
        if (isPreAuth) {
            //预授权历史
        } else {
            //交易记录
        }
    }
}
