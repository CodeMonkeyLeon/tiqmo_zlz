package cn.swiftpass.wallet.tiqmo.module.home.view;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.SendSplitAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.SplitBillContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.SplitPresenter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class SendSplitFragment extends BaseFragment<SplitBillContract.SplitPresenter> implements SplitBillContract.SplitView {
    @BindView(R.id.ll_no_send)
    LinearLayout llNoSend;
    @BindView(R.id.tv_split_1)
    TextView tvSplit1;
    @BindView(R.id.rl_send_split)
    RecyclerView rlSendSplit;

    private SendSplitAdapter adapter;
    private SplitListEntity entity;

    private List<SplitListEntity.SplitDetail> details = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_send_split;
    }

    public static SendSplitFragment getInstance(){
        SendSplitFragment fragment = new SendSplitFragment();
        return fragment;
    }

    @Override
    protected void initView(View parentView) {
        llNoSend.setVisibility(View.VISIBLE);
        mPresenter.getSplit(Constants.sent);

    }



    @Override
    public SplitBillContract.SplitPresenter getPresenter() {
        return new SplitPresenter();
    }

    @Override
    public void ReceiveSplitBillDetailSuccess(RequestCenterEntity requestCenterEntity) {

    }

    @Override
    public void getSplitSuccess(SplitListEntity entity) {
        if (entity.getSplitBillSentReceivedRecordDtos() != null) {
            details = entity.getSplitBillSentReceivedRecordDtos();
            llNoSend.setVisibility(View.GONE);
            rlSendSplit.setVisibility(View.VISIBLE);

            initRecycleView();
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        if (transferLimitEntity != null) {
            HashMap<String, Object> limitMap = new HashMap<>();
            limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
            ActivitySkipUtil.startAnotherActivity(mActivity, CreateSplitBillActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }

    }

    @Override
    public void getSplitDetailSuccess(SendReceiveSplitDetailEntity detailEntity) {
        if (detailEntity != null) {
            HashMap<String,Object> map = new HashMap<>();
            map.put(Constants.SEND_RECEIVE_SPLIT_DETAIL,detailEntity);
            ActivitySkipUtil.startAnotherActivity(mActivity,SendReceiveSplitDetailActivity.class,map, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        }

    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {

    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity result) {

    }


    private void initRecycleView() {
        rlSendSplit.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        adapter = new SendSplitAdapter(details);
        adapter.bindToRecyclerView(rlSendSplit);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                String originalOrderNo = details.get(position).getOriginalOrderNo();
                String orderRelevancy = details.get(position).getOrderRelevancy();
                mPresenter.getSplitDetail(originalOrderNo,orderRelevancy,Constants.sent);
            }
        });
    }

    @Override
    public void showError(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @OnClick({R.id.tv_split_1})
    public void onClickView(View view){
        switch (view.getId()){
            case R.id.tv_split_1:
                mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                break;
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {
        mPresenter.getSplit(Constants.sent);
    }
}
