package cn.swiftpass.wallet.tiqmo.support.app2app;

import java.io.Serializable;

public class PaySDKEntity implements Serializable {
    private String orderInfo;
    private String appID;

    public PaySDKEntity(String orderInfo, String appID) {
        this.orderInfo = orderInfo;
        this.appID = appID;
    }

    public String getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(String orderInfo) {
        this.orderInfo = orderInfo;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

}
