package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.youth.banner.Banner;
import com.youth.banner.listener.OnPageChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.CardBannerAdapter;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardServiceEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.CardSummaryPresenter;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class GetNewCardThreeActivity extends BaseCompatActivity<CardKsaContract.GetCardSummaryPresenter> implements CardKsaContract.GetCardSummaryView {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.banner_card)
    Banner bannerCard;
    @BindView(R.id.iv_blue_color)
    ImageView ivBlueColor;
    @BindView(R.id.con_blue_color)
    ConstraintLayout conBlueColor;
    @BindView(R.id.iv_green_color)
    ImageView ivGreenColor;
    @BindView(R.id.con_green_color)
    ConstraintLayout conGreenColor;
    @BindView(R.id.tv_choose_card)
    TextView tvChooseCard;
    @BindView(R.id.ll_card_color)
    LinearLayout llCardColor;

    private CardBannerAdapter cardBannerAdapter;

    //卡片颜色 : 标准卡的01表示浅色 ,02表示深色；白金卡的06浅色,07深色
    private String cardFaceId = "02";

    private List<CardServiceEntity> bannerList = new ArrayList<>();

    private OpenCardReqEntity openCardReqEntity;

    private String cardProductType;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_new_card_three;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.DigitalCard_0);
        setDarkBar();
        fixedSoftKeyboardSliding();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
        cardProductType = openCardReqEntity.cardProductType;
        try {
            if(Constants.card_type_Standard.equals(cardProductType)){
                bannerList.add(new CardServiceEntity(R.drawable.standard_card_h, R.string.DigitalCard_11));
                bannerList.add(new CardServiceEntity(R.drawable.standard_card_h, R.string.DigitalCard_11));
//                bannerList.add(new CardServiceEntity(R.drawable.standard_card_green_h, R.string.DigitalCard_12));
            }else {
                bannerList.add(new CardServiceEntity(R.drawable.card_platinum_h, R.string.DigitalCard_11));
                llCardColor.setVisibility(View.GONE);
                cardFaceId = "07";
//                bannerList.add(new CardServiceEntity(R.drawable.card_platinum_h, R.string.DigitalCard_12));
            }
            cardBannerAdapter = new CardBannerAdapter(bannerList);
            bannerCard.addBannerLifecycleObserver(this)//添加生命周期观察者
                    .setAdapter(cardBannerAdapter,false)
                    .setUserInputEnabled(true)//禁止手动滑动Banner
                    .isAutoLoop(false)//是否允许自动轮播
                    .setBannerGalleryEffect(bannerList.size()>1?80:0,bannerList.size()>1?60:0, 0, 1f)
                    .addOnPageChangeListener(new OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            chooseCard(position);
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void chooseCard(int position){
        if(position == 0){
            if(Constants.card_type_Standard.equals(cardProductType)) {
                cardFaceId = "02";
            }else{
                cardFaceId = "07";
            }
            ivBlueColor.setImageResource(R.drawable.digital_stand_check);
            ivGreenColor.setImageResource(R.drawable.digital_green_normal);
            tvChooseCard.setEnabled(true);
            bannerCard.setCurrentItem(0);
        }else{
            if(Constants.card_type_Standard.equals(cardProductType)) {
                cardFaceId = "01";
            }else{
                cardFaceId = "06";
            }
            ivBlueColor.setImageResource(R.drawable.digital_stand_normal);
            ivGreenColor.setImageResource(R.drawable.digital_green_check);
            tvChooseCard.setEnabled(true);
            bannerCard.setCurrentItem(1);
        }
    }

    @OnClick({R.id.iv_back, R.id.con_blue_color, R.id.con_green_color, R.id.tv_choose_card})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.con_blue_color:
                chooseCard(0);
                break;
            case R.id.con_green_color:
                chooseCard(1);
                break;
            case R.id.tv_choose_card:
                openCardReqEntity.cardFaceId = cardFaceId;
                UserInfoManager.getInstance().setOpenCardReqEntity(openCardReqEntity);
                mPresenter.getKsaCardSummary("",openCardReqEntity.cardProductType);
                break;
            default:
                break;
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getKsaCardSummarySuccess(KsaCardSummaryEntity ksaCardSummaryEntity) {
        if(ksaCardSummaryEntity != null){
            HashMap<String,Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksaCardSummaryEntity,ksaCardSummaryEntity);
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardFourActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

//    @Override
//    public void openKsaCardNoFeeSuccess(KsaPayResultEntity result) {
//
//    }

    @Override
    public void getKsaCardPayResultSuccess(KsaPayResultEntity result) {

    }

    @Override
    public void setKsaCardPinSuccess(Void result) {

    }

    @Override
    public CardKsaContract.GetCardSummaryPresenter getPresenter() {
        return new CardSummaryPresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity riskControlEntity) {

    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {

    }
}
