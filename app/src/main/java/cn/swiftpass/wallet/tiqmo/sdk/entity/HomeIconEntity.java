package cn.swiftpass.wallet.tiqmo.sdk.entity;

public class HomeIconEntity {
    public int imgId;
    public int textId;

    public HomeIconEntity(int imgId, int textId) {
        this.imgId = imgId;
        this.textId = textId;
    }
}
