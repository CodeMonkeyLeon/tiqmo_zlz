package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;


public class BottomDialog extends Dialog {

    private Context context;

    protected CustomProgressDialog mProgressDialog;

    private CustomDialog mDealDialog;

    public BottomDialog(Context context, Bitmap bitmap) {
        this(context, R.style.DialogBottomIn);
        this.context = context;
        initStyle(R.style.DialogBottomIn, bitmap);
    }

    public BottomDialog(Context context) {
        this(context, R.style.DialogBottomIn);
        this.context = context;
        initStyle(R.style.DialogBottomIn,null);
    }

    public BottomDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;

    }

    /**
     * 如果用户提出需要点击dialog 后面的按钮点击事件不被满屏的dialog消费
     * 则初始化的构造函数里面调用下面的代码。但是需要自己维护关闭的事件
     **/
//    private void closeOutSideTouch(){
//        Window window = getWindow();
//        if (window != null) {
//            int flag = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
//            window.setFlags(flag, flag);
//        }
//    }


    private void initStyle(int themeResId, Bitmap blurBackgroundDrawer) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().getAttributes().windowAnimations = themeResId;
        }
        setCanceledOnTouchOutside(true);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        getWindow().setBackgroundDrawableResource(R.color.colorPrimaryDark);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
//                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
     /*   getWindow().setWindowAnimations(R.style.popup_style);
        showMatchParentWidthAndGravityBottom();
*/
        //blurBackgroundDrawer为模糊后的背景图片
        if (blurBackgroundDrawer != null) {
            Window window = getWindow();
            window.setBackgroundDrawable(new BitmapDrawable(context.getResources(), blurBackgroundDrawer));
        }

        setCancelable(true);
    }

    /**
     * 宽度全屏显示在底部
     */
    public void showMatchParentWidthAndGravityBottom() {
        if (context == null || getWindow() == null) {
            return;
        }
        getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = AndroidUtils.getScreenWidth(getContext());
        getWindow().setAttributes(lp);
        show();
    }

    /**
     * 宽度全屏显示在底部，从底部动画冒出
     */
    public void showWithBottomAnim() {
        getWindow().setWindowAnimations(R.style.popup_style);
        showMatchParentWidthAndGravityBottom();
    }


    public void showProgress(boolean isShow) {
        if (mProgressDialog == null) {
            mProgressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.common_34),R.style.CustomProgressDialog);
            mProgressDialog.setCancel(false);
            if (mProgressDialog.getWindow() != null) {
                mProgressDialog.getWindow().setDimAmount(0.7f);
            }
        }
        if (isShow) {
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    public void showToast(String content) {
        showToast(content, Toast.LENGTH_SHORT);
    }

    public void showToast(String content, int duration) {
        Toast toast = Toast.makeText(context, content, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public CustomDialog showCustomDialog(Context mContext, int titleId, int msgId, int posStr, int navId, DialogInterface.OnClickListener posClick, DialogInterface.OnClickListener navClick) {
        CustomDialog mDealDialog = null;
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(titleId);
        builder.setMessage(msgId);
        builder.setNegativeButton(mContext.getString(navId), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        builder.setPositiveButton(mContext.getString(posStr), posClick);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
        return mDealDialog;
    }

    public void showErrorDialog(Context mContext, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.common_5), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    /**
     * 检查是否有permission权限  返回true则表示有
     */
    public boolean isGranted_(String permission) {
        if (context == null) {
            return false;
        }
        int checkSelfPermission = ActivityCompat.checkSelfPermission(context, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    protected void showLackOfPermissionDialog(String title, String message) {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.common_9), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(context);
            }
        });
        builder.setNegativeButton(context.getString(R.string.common_10), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_1da1f1).setRightColor(R.color.color_1da1f1);
        Activity mActivity = (Activity) context;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    /**
     * 居中显示
     */
    public void showMatchParentWidthAndGravityCenter(int width) {
        if (context == null || getWindow() == null) {
            return;
        }
        getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = width;
        getWindow().setAttributes(lp);
        show();
    }
}
