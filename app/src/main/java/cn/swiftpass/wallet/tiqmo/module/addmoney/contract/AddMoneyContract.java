package cn.swiftpass.wallet.tiqmo.module.addmoney.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanBaseActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TokenDataEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;

public class AddMoneyContract {

    public interface View extends BaseView<AddMoneyContract.Presenter> {
        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void googlePaySuccess(CardNotifyEntity cardNotifyEntity);

        void googlePayFail(String errCode, String errMsg);

        void getTransferFeeSuccess(TransFeeEntity transFeeEntity);

        void getTransferFeeFail(String errCode, String errMsg);

        void getCardListSuccess(AllCardEntity allCardEntity);

        void getIbanActivityDetailSuccess(IbanBaseActivityEntity result);

        void showErrorMsg(String errCode,String errMsg);
    }

    public interface Presenter extends BasePresenter<AddMoneyContract.View> {
        void getCardList(String cardType);

        void getLimit(String type);

        void getTransferFee(String type, String orderAmount, String orderCurrencyCode);

        void googlePay(String type, String orderAmount, String orderCurrencyCode, TokenDataEntity tokenData, String transFees, String vat);

        void getIbanActivityDetail();

    }

    public interface CardListView extends BaseView<AddMoneyContract.CardListPresenter> {
        void getCardListSuccess(AllCardEntity allCardEntity);
        void unbindCardSuccess(unBindCardEntity result);
        void bindNewCardSuccess(CardBind3DSEntity result);
        void cardAddMoneyPreOrderSuccess(TransferPayEntity transferPayEntity);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface CardListPresenter extends BasePresenter<AddMoneyContract.CardListView> {
        void getCardList(String cardType);
        void bindNewCard(String cardHolderName,String cardNum, String expireDate,String CVV,String type);
        void unbindCard(String protocolNo);
        void cardAddMoneyPreOrder(String type, String orderAmount, String orderCurrencyCode,String protocolNo);
    }

    public interface AddCardView extends BaseView<AddMoneyContract.AddCardPresenter> {
        void bindCardPreOrderSuccess(TransferPayEntity result);
        void verifyCardSuccess(verifyCardEntity verifyCardEntity);
        void verifyCardFail(String errorCode,String errorMsg);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface AddCardPresenter extends BasePresenter<AddMoneyContract.AddCardView> {
        void bindCardPreOrder(String custName, String cardNo, String expire, String cvv, String type, String orderAmount,
                              String orderCurrencyCode,String creditType);

        void verifyCard(String cardNo);
    }

    public interface ConfirmTransferView extends BaseView<AddMoneyContract.ConfirmTransferPresenter> {
        void cardAddMoneyConfirmPaySuccess(CardBind3DSEntity cardBind3DSEntity);
        void bindCardConfirmPayPaySuccess(CardBind3DSEntity cardBind3DSEntity);
        void request3dsSuccess(CardNotifyEntity cardNotifyEntity);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface ConfirmTransferPresenter extends BasePresenter<AddMoneyContract.ConfirmTransferView> {
        void bindCardConfirmPay(String orderNo, String cvv, String type,
                           String shopperResultUrl,
                           String creditType);
        void cardAddMoneyConfirmPay(String orderNo, String cvv, String type,
                           String shopperResultUrl,
                           String creditType);
        void request3ds(String sessionId,String orderNo);
    }
}
