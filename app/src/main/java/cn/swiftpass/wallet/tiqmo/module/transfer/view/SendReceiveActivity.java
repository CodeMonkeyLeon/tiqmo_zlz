package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.makeramen.roundedimageview.RoundedImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.ComingSoonActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.SendReceiveAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.SendReceiveContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveTransferDetail;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SplitOderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransactionListInfos;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.SendReceivePresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.WrapContentLinearLayoutManager;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class SendReceiveActivity extends BaseCompatActivity<SendReceiveContract.SendReceivePresenter> implements SendReceiveContract.SendReceiveView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.ll_transfer)
    LinearLayout llTransfer;
    @BindView(R.id.ll_request)
    LinearLayout llRequest;
    @BindView(R.id.ll_split_bill)
    LinearLayout llSplitBill;
    @BindView(R.id.ll_chat)
    LinearLayout llChat;
    @BindView(R.id.sw_history)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ry_transaction_history)
    RecyclerView ryTransactionHistory;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.iv_filter)
    ImageView ivFilter;
    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;

    @BindView(R.id.group_avatar)
    LinearLayout groupAvatar;
    @BindView(R.id.ll_avatar_line_1)
    LinearLayout llAvatarLine1;
    @BindView(R.id.ll_avatar_line_2)
    LinearLayout llAvatarLine2;
    @BindView(R.id.avatar_line1_1)
    RoundedImageView imageView11;
    @BindView(R.id.avatar_line1_2)
    RoundedImageView imageView12;
    @BindView(R.id.avatar_line2_1)
    RoundedImageView imageView21;
    @BindView(R.id.avatar_line2_2)
    RoundedImageView imageView22;
    @BindView(R.id.tv_avatar_more)
    TextView tvAvatarMore;
    @BindView(R.id.cl_avatar_more)
    ConstraintLayout clAvatarMore;

    public static final int filter_request = 1001;

    public List<String> typeList = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();
    private FilterEntity filterEntity;
    private HashMap<Integer, String> selectedArray = new HashMap<>();
    private String startDate = "", endDate = "";
    private int pageSize = 10;
    private int currentPage = 1;
    private String direction, orderNo;

    private String sceneType;
    private SendReceiveEntity entity;
    private boolean isMulti;//是否拆分账单
    private View noDetailView;
    private WrapContentLinearLayoutManager wrapContentLinearLayoutManager;
    private SendReceiveAdapter sendReceiveAdapter;
    public List<TransactionListInfos> transactionInfoList = new ArrayList<>();
    public List<SplitOderDetailEntity> splitEntities = new ArrayList<>();
    private List<KycContactEntity> kycContactEntityList = new ArrayList<>();
    private static List<Bitmap> groupHeadIcons = new ArrayList<>();
    @Override
    protected int getLayoutID() {
        return R.layout.activity_send_receive;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        tvTitle.setText(R.string.newhome_14);
        tvTitle.setTextColor(getColor(R.color.white));
        swipeRefreshLayout.setColorSchemeResources(R.color.color_B00931);
        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle, scFilter, llFilter);
        if (getIntent() != null && getIntent().getExtras() != null) {
            entity = (SendReceiveEntity) getIntent().getExtras().get(Constants.SEND_RECEIVE_ENTITY);
            isMulti = getIntent().getBooleanExtra("isMulti", false);
            if (entity != null) {
                if (entity.getHeadIcon().size() == 1) {
                    ivAvatar.setVisibility(View.VISIBLE);
                    String avatarUrl = entity.getHeadIcon().get(0);
                    if (!TextUtils.isEmpty(avatarUrl)) {
                        Glide.with(mContext).clear(ivAvatar);
                        Glide.with(mContext)
                                .asBitmap()
                                .load(avatarUrl)
                                .diskCacheStrategy(DiskCacheStrategy.DATA)
                                .dontAnimate()
                                .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                                .format(DecodeFormat.PREFER_RGB_565)
                                .into(new BitmapImageViewTarget(ivAvatar) {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                                        ivAvatar.setImageBitmap(bitmap);
                                    }
                                });
                    } else {
                        Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivAvatar);
                    }
                }else {
                    //群组头像
                    ivAvatar.setVisibility(View.GONE);
                    groupAvatar.setVisibility(View.VISIBLE);
                    loadGroupAvatar();

                }
                    tvName.setText(entity.getFirstName());

            }
            if (isMulti) {
                llTransfer.setVisibility(View.GONE);
                llRequest.setVisibility(View.GONE);
                llSplitBill.setVisibility(View.VISIBLE);
            }
            wrapContentLinearLayoutManager = new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            ryTransactionHistory.setLayoutManager(wrapContentLinearLayoutManager);
            sendReceiveAdapter = new SendReceiveAdapter(transactionInfoList, this);
            sendReceiveAdapter.bindToRecyclerView(ryTransactionHistory);
            sendReceiveAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                    if (isMulti) {
                        RecyclerView recyclerView = view.findViewById(R.id.send_receive_info);
                        if (recyclerView.getVisibility() == View.VISIBLE) {
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            sendReceiveAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
                @Override
                public void onLoadMoreRequested() {
                    currentPage += 1;
                    mPresenter.SendReceiveDetail(entity.getGroupId(), "", entity.getIsGroup(),currentPage, pageSize);
                }
            }, ryTransactionHistory);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    currentPage = 1;
                    mPresenter.SendReceiveDetail(entity.getGroupId(), "", entity.getIsGroup(),currentPage, pageSize);
                }
            });
        }

        mPresenter.SendReceiveDetail(entity.getGroupId(), "", entity.getIsGroup(),currentPage, pageSize);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mPresenter.SendReceiveDetail(entity.getGroupId(), "", entity.getIsGroup(),currentPage, pageSize);
    }

    private void loadGroupAvatar() {
        List<String> headIcon = entity.getHeadIcon();
        int size = headIcon.size();
        if (size == 2) {
            llAvatarLine2.setVisibility(View.GONE);
        }else if (size == 3){
            imageView22.setVisibility(View.GONE);
            clAvatarMore.setVisibility(View.GONE);
        }else if (size == 4){
            clAvatarMore.setVisibility(View.GONE);
        }else{
            tvAvatarMore.setText("+"+(size-3));
            imageView22.setVisibility(View.GONE);
        }

        for (int i = 0; i < headIcon.size(); i++) {
            String avatar = headIcon.get(i);
            if (i==0) {
                if (!TextUtils.isEmpty(avatar)) {
                    Glide.with(this).clear(imageView11);
                    int round = (int) AndroidUtils.dip2px(mContext, 8);
                    Glide.with(this)
                            .load(avatar)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                            .into(imageView11);
                } else {
                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView11);
                }
            }else if (i == 1){
                if (!TextUtils.isEmpty(avatar)) {
                    Glide.with(this).clear(imageView12);
                    int round = (int) AndroidUtils.dip2px(mContext, 8);
                    Glide.with(this)
                            .load(avatar)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                            .into(imageView12);
                } else {
                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView12);
                }
            }else if (i == 2){
                if (!TextUtils.isEmpty(avatar)) {
                    Glide.with(this).clear(imageView21);
                    int round = (int) AndroidUtils.dip2px(mContext, 8);
                    Glide.with(this)
                            .load(avatar)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                            .into(imageView21);
                } else {
                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView21);
                }
            } else if (i == 3){
                if (!TextUtils.isEmpty(avatar)) {
                    Glide.with(this).clear(imageView22);
                    int round = (int) AndroidUtils.dip2px(mContext, 8);
                    Glide.with(this)
                            .load(avatar)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, ""))
                            .into(imageView22);
                } else {
                    Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(imageView22);
                }
            }
        }

    }


    @OnClick({R.id.iv_back,R.id.ll_transfer,R.id.ll_request,R.id.ll_chat,R.id.iv_filter,R.id.tv_filter,R.id.ll_split_bill})
    public void onClick(View v){
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_transfer:
                sceneType = Constants.TYPE_TRANSFER_WW;
                mPresenter.getTransferPeopleDetail(entity.getGroupId(),entity.getIsGroup());
                break;
            case R.id.ll_request:
                sceneType = Constants.TYPE_TRANSFER_RT;
                mPresenter.getTransferPeopleDetail(entity.getGroupId(),entity.getIsGroup());
                break;
            case R.id.ll_split_bill:
                mPresenter.getTransferPeopleDetail(entity.getGroupId(),entity.getIsGroup());
                break;
            case R.id.ll_chat:
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("title", getString(R.string.newhome_12));
                ActivitySkipUtil.startAnotherActivity(this, ComingSoonActivity.class,hashMap,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_filter:
            case R.id.tv_filter:

                break;

        }

    }


    @Override
    public SendReceiveContract.SendReceivePresenter getPresenter() {
        return new SendReceivePresenter();
    }


    @Override
    public void getTransferPeopleDetailSuccess(SendReceiveTransferDetail result) {
        if (result != null) {
            //个体转账
            if (result.getTransUserInfoList() != null) {
                HashMap<String, Object> map = new HashMap<>(16);
                List<SendReceiveTransferDetail.TransUserInfoList> infoList = result.getTransUserInfoList();
                if (infoList.size() == 1 && !isMulti) {
                    KycContactEntity kycContactEntity = new KycContactEntity();
                    kycContactEntity.setContactsName(infoList.get(0).getTransName());
                    kycContactEntity.setPhone(infoList.get(0).getTransUserPhone());
                    kycContactEntity.setHeadIcon(infoList.get(0).getTransUserHeadIcon());
                    kycContactEntity.setUserId(infoList.get(0).getTransUserId());
                    map.put(Constants.sceneType,sceneType);
                    map.put(Constants.CONTACT_ENTITY, kycContactEntity);
                    if (sceneType == Constants.TYPE_TRANSFER_WW) {
                        ActivitySkipUtil.startAnotherActivity(this, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }else if (sceneType == Constants.TYPE_TRANSFER_RT){
                        ActivitySkipUtil.startAnotherActivity(this, RequestTransferActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }else {
                    kycContactEntityList.clear();
                    for (int i = 0; i < infoList.size(); i++) {
                        KycContactEntity kycContactEntity = new KycContactEntity();
                        kycContactEntity.setContactsName(infoList.get(i).getTransName());
                        kycContactEntity.setPhone(infoList.get(i).getTransUserPhone());
                        kycContactEntity.setHeadIcon(infoList.get(i).getTransUserHeadIcon());
                        kycContactEntity.setUserId(infoList.get(i).getTransUserId());
                        kycContactEntityList.add(kycContactEntity);
                    }
                    mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);

                }
            }
        }
    }

    @Override
    public void SendReceiveDetailSuccess(SendReceiveDetailListEntity listEntity) {
        swipeRefreshLayout.setRefreshing(false);
        List<TransactionListInfos> tempList = new ArrayList<>();
        if (listEntity.getDetailList() != null) {
            List<SendReceiveDetailEntity> detailList = listEntity.getDetailList();
            for (int i = 0; i < detailList.size(); i++) {
                SendReceiveDetailEntity sendReceiveDetailEntity = detailList.get(i);
                if (sendReceiveDetailEntity != null) {
                    tempList.addAll(sendReceiveDetailEntity.getTransactionListInfos());
                }
            }
        }
        sendReceiveAdapter.loadMoreEnd(false);
        if (currentPage == 1) {
            ryTransactionHistory.scrollToPosition(0);
            transactionInfoList.clear();
            if (tempList.size() == 0) {
                sendReceiveAdapter.loadMoreEnd(false);
            } else {
                sendReceiveAdapter.setDataList(tempList);
            }
        } else {
            if (transactionInfoList.size() == 0) {
                sendReceiveAdapter.loadMoreEnd(false);
            } else {
                sendReceiveAdapter.addData(transactionInfoList);
                if (transactionInfoList.size() < pageSize) {
                    sendReceiveAdapter.loadMoreEnd(false);
                } else {
                    sendReceiveAdapter.loadMoreComplete();
                }
            }
        }


    }

    @Override
    public void SendReceiveDetailFail(String errorCode, String errorMsg) {
        sendReceiveAdapter.loadMoreEnd(false);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        if (transferLimitEntity != null) {
            HashMap<String, Object> limitMap = new HashMap<>();
            limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
            limitMap.put(Constants.CONTACT_ENTITY, kycContactEntityList);
            limitMap.put("cannotAdd", true);
            ActivitySkipUtil.startAnotherActivity(this, CreateSplitBillActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }

    }


    @Override
    public void showError(String errorCode, String errorMsg) {
        swipeRefreshLayout.setRefreshing(false);
        showProgress(false);
        showTipDialog(errorMsg);
    }
}
