package cn.swiftpass.wallet.tiqmo.module.home.view;

import static cn.swiftpass.wallet.tiqmo.module.home.presenter.HomeFragmentPresenter.SCENE_TYPE_TOP_UP;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gc.gcchat.GcChatConnectionStatus;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatUser;
import com.gc.gcchat.callback.ConnectionCallback;
import com.gc.gcchat.callback.GcChatRestoreBackupCallback;
import com.gc.gcchat.callback.InitCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.makeramen.roundedimageview.RoundedImageView;
import com.youth.banner.Banner;
import com.youth.banner.indicator.RectangleIndicator;
import com.youth.banner.listener.OnBannerListener;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.HomeBannerAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.HomeContactAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.HomeIcAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.contract.HomeFragmentContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AnalysisGraphicColorEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AnalysisGraphicEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HomeAnalysisEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.LineChartDataEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.LinePointEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingActivityDataEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingCategoryBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.HomeFragmentPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrMainActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.view.PayBillCountryActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.view.PayBillPaymentActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeDomeOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInterOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeCountryActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargePaymentActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.ScanQrActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.SendReceiveActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferContactActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.VoucherMarketActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.dialog.EVoucherSelectCountryDialogFrag;
import cn.swiftpass.wallet.tiqmo.module.voucher.dialog.OrderRepeatNoticeDia;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.HomeIconEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.AsteriskPasswordTransformationMethod;
import cn.swiftpass.wallet.tiqmo.widget.BrokenView;
import cn.swiftpass.wallet.tiqmo.widget.MyGridView;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ExpireIdDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.PendingBalanceDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class HomePageFragment extends BaseFragment<HomeFragmentContract.Presenter> implements HomeFragmentContract.View {


    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;

    @BindView(R.id.con_avatar)
    ConstraintLayout conAvatar;
    @BindView(R.id.tv_name_first)
    TextView tvNameFirst;
    @BindView(R.id.iv_qr_pay)
    ImageView ivQrPay;
    @BindView(R.id.iv_chat)
    ImageView ivChat;
    @BindView(R.id.iv_request_center)
    ImageView ivRequestCenter;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.iv_money_see)
    ImageView ivMoneySee;
    @BindView(R.id.ll_head)
    ConstraintLayout llHead;
    @BindView(R.id.rl_send_contacts)
    RecyclerView rlSendContacts;
    @BindView(R.id.tv_all_analytics)
    TextView tvAllAnalytics;
    @BindView(R.id.tv_left_money)
    TextView tvLeftMoney;
    @BindView(R.id.home_bg)
    ConstraintLayout homeBg;
    @BindView(R.id.tv_home_customise)
    TextView tvHomeCustomise;
    @BindView(R.id.banner_home)
    Banner bannerHome;
    @BindView(R.id.tv_home_add_money)
    TextView tvHomeAddMoney;
    @BindView(R.id.tv_send_activity)
    TextView tvSendActivity;
    @BindView(R.id.tv_analytics)
    TextView tvAnalytics;
    @BindView(R.id.ll_not_kyc)
    ConstraintLayout llNotKyc;
    @BindView(R.id.tv_kyc_title)
    TextView tvKycTitle;
    @BindView(R.id.tv_kyc)
    TextView tvKyc;
    @BindView(R.id.iv_kyc)
    ImageView ivKyc;
    @BindView(R.id.sw_home)
    SwipeRefreshLayout swHome;
    @BindView(R.id.tv_set_up)
    TextView tvSetUp;
    @BindView(R.id.iv_pending_balance)
    ImageView ivPendingBalance;
    @BindView(R.id.tv_home_account)
    TextView tvHomeAccount;
    @BindView(R.id.ll_front_account)
    ConstraintLayout llFrontAccount;
    @BindView(R.id.tv_wallet_account)
    TextView tvWalletAccount;
    @BindView(R.id.tv_wallet_bank)
    TextView tvWalletBank;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_bank_number)
    TextView tvBankNumber;
    @BindView(R.id.iv_home_copy)
    ImageView ivHomeCopy;
    @BindView(R.id.tv_home_share)
    TextView tvHomeShare;
    @BindView(R.id.tv_home_back)
    TextView tvHomeBack;
    @BindView(R.id.iv_home_back)
    ImageView ivHomeBack;
    @BindView(R.id.ll_back_account)
    ConstraintLayout llBackAccount;
    @BindView(R.id.ll_top_content)
    LinearLayout llTopContent;
    @BindView(R.id.tv_home_line)
    View tvHomeLine;
    @BindView(R.id.tv_notify_number)
    TextView tvNotifyNumber;

    @BindView(R.id.gv_service)
    MyGridView gvService;
    @BindView(R.id.iv_reward)
    ImageView ivReward;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_complete_kyc)
    MyTextView tvCompleteKyc;
    @BindView(R.id.ll_kyc_content)
    LinearLayout llKycContent;
    @BindView(R.id.tv_freeze_account)
    MyTextView tvFreezeAccount;

    @BindView(R.id.brokenView)
    BrokenView brokenView;
    @BindView(R.id.iv_budget)
    ImageView ivBudget;
    @BindView(R.id.tv_budget)
    MyTextView tvBudget;
    @BindView(R.id.ll_money_left)
    LinearLayout llMoneyLeft;
    @BindView(R.id.tv_spending_notice)
    MyTextView tvSpendingNotice;
    @BindView(R.id.ll_spending_notice)
    LinearLayout llSpendingNotice;
    @BindView(R.id.ll_budget)
    ConstraintLayout llBudget;
    @BindView(R.id.tv_not_kyc_spending)
    MyTextView tvNotKycSpending;
    @BindView(R.id.tv_not_kyc_budget)
    MyTextView tvNotKycBudget;
    @BindView(R.id.ll_not_kyc_spending)
    LinearLayout llNotKycSpending;
    @BindView(R.id.ll_kyc_spending)
    LinearLayout llKycSpending;
    @BindView(R.id.iv_no_contacts)
    ImageView ivNoContacts;
    @BindView(R.id.tv_no_contacts1)
    TextView tvNoContacts1;
    @BindView(R.id.tv_no_contacts2)
    TextView tvNoContacts2;
    @BindView(R.id.con_no_contacts)
    ConstraintLayout conNoContacts;

    private UserInfoEntity userInfoEntity;

    private HomeIcAdapter homeIcAdapter;

    private HomeContactAdapter homeContactAdapter;

    private HomeBannerAdapter homeBannerAdapter;


    private boolean isHideMoney;
    private List<HomeIconEntity> iconList = new ArrayList<>();

    private List<AllMarketInfoEntity.MarketInfo> bannerList = new ArrayList<>();

    private BalanceEntity pendingBanlance;
    private String totalHoldAmount = "0.0";
    private PendingBalanceDialog pendingBalanceDialog;
    private boolean isPendingBalance, isShowCharity;

    private String badges;

    private String shareIbanNum;

    //点对应的数据
    public List<List<String>> data = new ArrayList<>();
    //X轴刻度值
    public List<String> x = new ArrayList<>();
    //Y轴刻度
    public List<String> y = new ArrayList<>();

    List<String> xStrList = new ArrayList<>();
    List<String> yStrList = new ArrayList<>();
    HashMap<Double, Double> map = new HashMap<>();
    private List<LinePointEntity> pointList = new ArrayList<>();

    //周期类型 W 周维度 M 月维度  Y 年维度
    private String periodType = "M";
    //当前日期 格式为 YYYY-MM-DD  传给后台用
    private String spendingAnalysisQueryDate;

    public static HomePageFragment getInstance() {
        HomePageFragment homeFragment = new HomePageFragment();
        return homeFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_home_page;
    }

    private void initUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            tvUserName.setText(getString(R.string.sprint20_108) + " " + userInfoEntity.getFirstName() + " " + "👋");
            tvNameFirst.setText(userInfoEntity.userName.substring(0,1).toUpperCase(Locale.ENGLISH));
            tvBankName.setText(userInfoEntity.accountName);
            String ibanNum = userInfoEntity.ibanNum;
            if (!TextUtils.isEmpty(ibanNum)) {
                shareIbanNum = AndroidUtils.changeBanNum(ibanNum);
                tvBankNumber.setText(shareIbanNum);
            }
            if(userInfoEntity.isFreezeAccount() || userInfoEntity.isBlockAccount()){
                tvHomeAddMoney.setVisibility(View.GONE);
                tvFreezeAccount.setText(userInfoEntity.isBlockAccount()?getString(R.string.sprint20_106):getString(R.string.sprint20_103));
                tvFreezeAccount.setVisibility(View.VISIBLE);
            }else{
                tvHomeAddMoney.setVisibility(View.VISIBLE);
                tvFreezeAccount.setVisibility(View.GONE);
//                tvHomeAddMoney.setVisibility(View.GONE);
//                tvFreezeAccount.setVisibility(View.VISIBLE);
            }
            setKycStatus(userInfoEntity);
            String avatar = userInfoEntity.avatar;
            String avatarUrl = AndroidUtils.getAvatarUrl(avatar);
            String lastAvatarUrl = (String) ivAvatar.getTag();
            if (!TextUtils.isEmpty(avatar)) {
                conAvatar.setVisibility(View.GONE);
                ivAvatar.setVisibility(View.VISIBLE);
                if(!avatarUrl.equals(lastAvatarUrl)) {
                    Glide.with(this).clear(ivAvatar);
                    int round = (int) AndroidUtils.dip2px(mContext, 8);
                    Glide.with(this)
                            .load(avatar)
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(ivAvatar);
                    ivAvatar.setTag(AndroidUtils.getAvatarUrl(avatar));
                }
            } else {
                conAvatar.setVisibility(View.VISIBLE);
                ivAvatar.setVisibility(View.INVISIBLE);
            }

            setUserInfo(userInfoEntity);
            initChatSdk(userInfoEntity.userId);
        }
    }

    private void initChatSdk(String chatUserId) {
        try {
            if (GcChatSDK.isInitializedSuccessfully()) {
                loginAndInitialize(chatUserId);
            } else {
                GcChatSDK.init(mContext, new InitCallback() {
                    @Override
                    public void onSuccessful() {
                        loginAndInitialize(chatUserId);
                    }

                    @Override
                    public void onFailure(int errorCode) {
                        showProgress(false);
                    }
                });
            }
        } catch (Throwable e) {
            showProgress(false);
            e.printStackTrace();
        }
    }

    private void loginAndInitialize(String phone) {
        GcChatSDK.checkIfRestoreRequired(ChatHelper.getInstance().getJwtTokenRT(phone), new GcChatRestoreBackupCallback() {
            @Override
            public void onSuccessful(boolean isBackupAvailable) {
                LogUtils.d("isBackupAvailable %s", isBackupAvailable);
                if (isBackupAvailable) {
                    showProgress(false);
                } else {
                    connectGcSdk(phone);
                }
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
            }
        });
    }

    private void connectGcSdk(String phone){
        GcChatSDK.connect(ChatHelper.getInstance().getJwtTokenRT(phone), new ConnectionCallback() {
            @Override
            public void onConnectionUpdate(int i) {
                showProgress(false);
                if (i == GcChatConnectionStatus.CONNECTED) {
                } else if (i == GcChatConnectionStatus.RECONNECTING) {
                    isChatConnected = false;
                    connectGcSdk(phone);
                } else if (i == GcChatConnectionStatus.DISCONNECTED) {
                    isChatConnected = false;
                    connectGcSdk(phone);
                }
            }

            @Override
            public void onConnected(@NonNull GcChatUser gcChatUser) {
                isChatConnected = true;
            }

            @Override
            public void onDisconnected(int i) {
                isChatConnected = false;
            }
        });
    }

    private void setPendingBalance(BalanceListEntity balanceListEntity) {
        if (balanceListEntity != null) {
            totalHoldAmount = balanceListEntity.totalHoldAmount;

            List<BalanceEntity> balanceInfos = balanceListEntity.balanceInfos;
            int size = balanceInfos.size();
            for (int i = 0; i < size; i++) {
                BalanceEntity balanceEntity = balanceInfos.get(i);
                String accountType = balanceEntity.getAccountType();
                if ("301".equals(accountType)) {
                    pendingBanlance = balanceEntity;
                }
            }

            if ((pendingBanlance != null && pendingBanlance.balanceNotNull()) || BigDecimalFormatUtils.compareBig(totalHoldAmount,"0")) {
                ivPendingBalance.setVisibility(View.VISIBLE);
            } else {
                ivPendingBalance.setVisibility(View.GONE);
            }
        }
    }

    public void setUserInfo(UserInfoEntity userInfoEntity) {
        badges = userInfoEntity.badges;
        if (!TextUtils.isEmpty(badges)) {
            ivReward.setImageResource(AndroidUtils.getRewardIcon(badges));
        }
        isHideMoney = SpUtils.getInstance().getHidePwd();
        if (isHideMoney) {
            ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_hide));
            tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        } else {
            ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_unhide));
            tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }

        String balance = userInfoEntity.getBalance();
        tvCurrency.setText(LocaleUtils.getCurrencyCode(""));
        if (userInfoEntity.isKyc()) {
            if (!TextUtils.isEmpty(balance)) {
                tvAmount.setText(balance);
            }
        } else {

        }
    }

    private void addMoney() {
        if (AndroidUtils.isCloseUse(mActivity, Constants.home_addMoney)) return;
        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
            return;
        if (isKyc(Constants.KYC_TYPE_HOME_ADD_MONEY)) {
            AndroidUtils.jumpToAddMoneyActivity(mActivity);
            setAlpha(mContext, 0.5f);
        }
    }

    @Override
    public void showServicesList(List<HomeIconEntity> list) {
//        iconList.clear();
//        homeServiceAdapter.setDataList(iconList);
        iconList.addAll(list);
        homeIcAdapter = new HomeIcAdapter(mContext);
        gvService.setAdapter(homeIcAdapter);
        homeIcAdapter.changeData(iconList);
        homeIcAdapter.setOnItemClickListener(new HomeIcAdapter.OnItemClick() {
            @Override
            public void onItemClick(HomeIconEntity homeIconEntity, int position) {
                HashMap<String, Object> hashMap = new HashMap<>();
                switch (position) { // case
                    case 0:
                        addMoney();
                        break;
                    case 1:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_send)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_HOME_SEND_MONEY)) {
                            ActivitySkipUtil.startAnotherActivity(mActivity, TransferActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                        break;
                    case 2:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_request)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_TRANSFER)) {
                            HashMap<String, Object> map = new HashMap<>(16);
                            map.put(Constants.sceneType, Constants.TYPE_TRANSFER_RT);
                            ActivitySkipUtil.startAnotherActivity(mActivity, TransferContactActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                        break;
                    case 3:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_intTransfer)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (!isKyc(Constants.KYC_TYPE_IMR)) {
                            return;
                        }
                        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
                        if (userInfoEntity != null) {
                            boolean isIMRSetup = true;
                            ActivitySkipUtil.startAnotherActivity(mContext, ImrMainActivity.class);
                        }
                        break;
                    case 4:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_splitBill)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_SPLIT_BILL)) {
                            ActivitySkipUtil.startAnotherActivity(mActivity, SendReceiveSplitActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                        break;
                    case 5:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_billPayments)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_PAY_BILL)) {
                            mPresenter.getPayBillOrderList();
                        }
                        break;
                    case 6:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_mobileTopup)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_TOP_UP)) {
                            mPresenter.getTopUpOrderList();
                        }
                        break;
                    case 7:
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_NULL)) {
                            isShowCharity = true;
                            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
                        }
                        break;
                    case 8:
                        if (AndroidUtils.isCloseUse(mActivity, Constants.home_marketplace)) return;
                        if (mActivity instanceof BaseCompatActivity && !((BaseCompatActivity) mActivity).getPermission())
                            return;
                        if (isKyc(Constants.KYC_TYPE_NULL)) {
                            ActivitySkipUtil.startAnotherActivity(mActivity, VoucherMarketActivity.class,
                                    hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void getPayBillOrderListSuccess(PayBillOrderListEntity payBillOrderListEntity) {
        if (payBillOrderListEntity != null) {
            List<PayBillOrderEntity> internationalList = payBillOrderListEntity.internationalList;
            List<PayBillOrderEntity> domesticList = payBillOrderListEntity.domesticList;
//            internationalList.addAll(payBillOrderListEntity.getTestList());
            if (internationalList.size() == 0 && domesticList.size() == 0) {
                ActivitySkipUtil.startAnotherActivity(mContext, PayBillCountryActivity.class);
            } else {
                HashMap<String, Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.paybill_billerOrderListEntity, payBillOrderListEntity);
                ActivitySkipUtil.startAnotherActivity(mActivity, PayBillPaymentActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public void getHomeAnalysisSuccess(HomeAnalysisEntity homeAnalysisEntity) {

    }

    @Override
    public void getTopUpOrderListSuccess(RechargeOrderListEntity rechargeOrderListEntity) {
        if (rechargeOrderListEntity != null) {
            List<RechargeInterOrderEntity> internationalList = rechargeOrderListEntity.internationalList;
            List<RechargeDomeOrderEntity> domesticList = rechargeOrderListEntity.domesticList;
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.rechargeOrderListEntity, rechargeOrderListEntity);
            if (internationalList.size() == 0) {
                ActivitySkipUtil.startAnotherActivity(mActivity, RechargeCountryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                ActivitySkipUtil.startAnotherActivity(mActivity, RechargePaymentActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public void getInviteDetail(InviteEntity inviteEntity) {
        if (inviteEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.INVITE_ENTITY, inviteEntity);
            mHashMap.put(Constants.badges, badges);
            ActivitySkipUtil.startAnotherActivity(mActivity, InviteFriendsActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getSendReceiveSuccess(List<SendReceiveEntity> sendReceiveList) {
        if (sendReceiveList != null && sendReceiveList.size() > 0) {
            conNoContacts.setVisibility(View.GONE);
            rlSendContacts.setVisibility(View.VISIBLE);
            List<SendReceiveEntity> entities = sendReceiveList;
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            MyItemDecoration myItemDecoration1 = MyItemDecoration.createHorizontal(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 6));
            if (rlSendContacts.getItemDecorationCount() == 0) {
                rlSendContacts.addItemDecoration(myItemDecoration1);
            }
            rlSendContacts.setLayoutManager(layoutManager);
            homeContactAdapter = new HomeContactAdapter(entities);
            homeContactAdapter.bindToRecyclerView(rlSendContacts);

            homeContactAdapter.setDataList(entities);

            homeContactAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                    if (ButtonUtils.isFastDoubleClick()) {
                        return;
                    }
                    SendReceiveEntity Entity = homeContactAdapter.getDataList().get(position);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Constants.SEND_RECEIVE_ENTITY, Entity);
                    if ("Y".equals(Entity.getIsGroup())) {
                        map.put("isMulti", true);
                    }
                    ActivitySkipUtil.startAnotherActivity(mActivity, SendReceiveActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

                }
            });
        } else {
            rlSendContacts.setVisibility(View.GONE);
            conNoContacts.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void initView(View parentView) {
        LocaleUtils.viewRotationY(mContext, headCircle,ivNoContacts,ivHomeBack);
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        initUser(userInfoEntity);
        llBackAccount.setRotationY(-90f);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        UserInfoManager.getInstance().setmGcmDeviceToken(token);
                        if (!TextUtils.isEmpty(token)) {
                            SpUtils.getInstance().setDeviceToken(token);
                        }
                        LogUtils.i("HomeFragment", "onMessageReceived: " + token);
                    }
                });

        swHome.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                restart();
            }
        });

        try {
            homeBannerAdapter = new HomeBannerAdapter(mContext,bannerList);
            bannerHome.addBannerLifecycleObserver(this)//添加生命周期观察者
                    .setAdapter(homeBannerAdapter)
                    .setIndicator(new RectangleIndicator(mContext))
                    .setOnBannerListener(new OnBannerListener() {
                        @Override
                        public void OnBannerClick(Object data, int position) {
                            if (ButtonUtils.isFastDoubleClick()) return;
                            if (bannerList.get(position).referralCodeActivityFlag.equals("Y")) {
                                mPresenter.getRefCodeDescDetails();
                            } else {
                                mPresenter.getMarketDetails(bannerList.get(position).activityNo);
                            }
                        }
                    });
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void flipAnimation() {
        final ConstraintLayout visibletext;
        final ConstraintLayout invisibletext;
        //逻辑判断
        if (llFrontAccount.getVisibility() == View.GONE) {
            visibletext = llBackAccount;
            invisibletext = llFrontAccount;
        } else {
            invisibletext = llBackAccount;
            visibletext = llFrontAccount;
        }
        //LinearInterpolator()     其变化速率恒定
        ObjectAnimator visToInvis = ObjectAnimator.ofFloat(visibletext, "rotationY", 0f, 90f);
        visToInvis.setDuration(500);
        //AccelerateInterpolator()    其变化开始速率较慢，后面加速
        visToInvis.setInterpolator(new AccelerateInterpolator());
        final ObjectAnimator invisToVis = ObjectAnimator.ofFloat(invisibletext, "rotationY",
                -90f, 0f);
        invisToVis.setDuration(500);
        //DecelerateInterpolator()   其变化开始速率较快，后面减速
        invisToVis.setInterpolator(new DecelerateInterpolator());
        visToInvis.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator anim) {
                visibletext.setVisibility(View.GONE);
                invisToVis.start();
                invisibletext.setVisibility(View.VISIBLE);
            }
        });
        visToInvis.start();
    }

    @Override
    protected void initData() {
        spendingAnalysisQueryDate = TimeUtils.getRequestDay(TimeUtils.getCurrentMonth());
        mPresenter.getServicesList();
        mPresenter.getConfig();
        mPresenter.getUserInfo(true);
        getBalance();
        mPresenter.getAllMarket();
        mPresenter.requestVoucherBrand();
        mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
//        mPresenter.getHomeAnalysis();
        if (userInfoEntity.isKyc()) {
            mPresenter.getSendReceive();
        }
        showHomeResume();
    }

    @Override
    protected void restart() {
//        showUserInfo();
        setAlpha(mContext, 1f);
        mPresenter.getUserInfo(false);
        mPresenter.getConfig();
        getBalance();
        mPresenter.getAllMarket();
        mPresenter.getAnalysisDetail(periodType, spendingAnalysisQueryDate);
        getUserDisableAppFuncCodeList();
//        mPresenter.getHomeAnalysis();
        if (userInfoEntity.isKyc()) {
            mPresenter.getSendReceive();
        }
        showHomeResume();
    }

    private void setKycStatus(UserInfoEntity userInfoEntity) {
        if (userInfoEntity.isKyc()) {
            tvAllAnalytics.setVisibility(View.VISIBLE);
            llNotKycSpending.setVisibility(View.GONE);
            llKycSpending.setVisibility(View.VISIBLE);
            llNotKyc.setVisibility(View.GONE);
            llTopContent.setVisibility(View.VISIBLE);
        } else if (userInfoEntity.isKycChecking() && SpUtils.getInstance().isKycChecking()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        } else {
            tvAllAnalytics.setVisibility(View.GONE);
            llNotKycSpending.setVisibility(View.VISIBLE);
            llKycSpending.setVisibility(View.GONE);
            tvKyc.setText(getString(R.string.sprint20_98)+"\n"+getString(R.string.sprint20_98_1));
            llNotKyc.setVisibility(View.VISIBLE);
            llTopContent.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
        if (ProjectApp.closePendingBalance) {
            if (pendingBalanceDialog != null && pendingBalanceDialog.isShowing()) {
                pendingBalanceDialog.dismiss();
                ProjectApp.closePendingBalance = false;
            }
        }
        setKycStatus(getUserInfoEntity());
        SpUtils.getInstance().setReferCode("");
    }

    @Override
    public void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    private void showHomeResume() {
        swHome.setRefreshing(false);
        setUserInfo(userInfoEntity);
        showRequestCenterIcon();
    }


    private void showRequestCenterIcon() {
        int requestCounts = SpUtils.getInstance().getReqCounts();
        if (requestCounts > 0) {
            tvNotifyNumber.setVisibility(View.VISIBLE);
            tvNotifyNumber.setText(requestCounts+"");
        } else {
            tvNotifyNumber.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_HOME_SCAN == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, ScanQrActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.KYC_TYPE_E_VOUCHER == event.getEventType()) {

        } else if (Constants.KYC_TYPE_TOP_UP == event.getEventType()) {
//            mPresenter.getTopUpList();
            mPresenter.getTopUpOrderList();
        } else if (Constants.KYC_TYPE_REFERRAL_CODE == event.getEventType()) {
            mPresenter.getInviteDetail();
//            ActivitySkipUtil.startAnotherActivity(getActivity(), ReferralCodeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.KYC_TYPE_SPLIT_BILL == event.getEventType()) {
            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
        } else if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            BalanceListEntity balanceListEntity = event.getBalanceListEntity();
            setPendingBalance(balanceListEntity);
            setUserInfo(userInfoEntity);
        } else if (Constants.KYC_TYPE_IMR == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mContext, ImrMainActivity.class);
        } else if (Constants.KYC_TYPE_PAY_BILL == event.getEventType()) {
            mPresenter.getPayBillOrderList();
        } else if (Constants.KYC_TYPE_HOME_ADD_MONEY == event.getEventType()) {
            AndroidUtils.jumpToAddMoneyActivity(mActivity);
        } else if (Constants.KYC_TYPE_HOME_SEND_MONEY == event.getEventType()) {
            ActivitySkipUtil.startAnotherActivity(mActivity, TransferActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (EventEntity.EVENT_CHANGE_PHONE_SUCCESS == event.getEventType()) {
            showNafathErrorDialog("4");
        } else if (EventEntity.EVENT_GET_NEW_NOTIFICATION == event.getEventType()) {
            showRequestCenterIcon();
        }
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundResourceByAttr(llHead, R.attr.bg_home_top);
        helper.setImageBgResourceByAttr(headCircle, R.attr.icon_circle);
        helper.setBackgroundResourceByAttr(llFrontAccount, R.attr.shape_20white_e4e4e4_stroke_16);
        helper.setBackgroundResourceByAttr(llBackAccount, R.attr.shape_20white_e4e4e4_stroke_16);
        helper.setBackgroundResourceByAttr(tvHomeLine, R.attr.color_70white_4d979797);
        helper.setImageResourceByAttr(ivQrPay, R.attr.home_qr_scan);
        helper.setImageResourceByAttr(ivChat, R.attr.home_chat);
        helper.setImageResourceByAttr(ivRequestCenter, R.attr.home_bell);
        helper.setImageResourceByAttr(ivBudget, R.attr.icon_spending_budget);
        helper.setTextColorByAttr(tvCurrency, R.attr.color_60white_603a3b44);
        helper.setTextColorByAttr(tvUserName, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvAmount, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvHomeAccount, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvHomeBack, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvBankName, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvBankNumber, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvWalletBank, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvWalletAccount, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvHomeShare, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvSendActivity, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvAnalytics, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvBudget, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvSetUp, R.attr.color_1da1f1_fc4f00);
        helper.setTextColorByAttr(tvLeftMoney, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvHomeAddMoney, R.attr.color_white_3a3b44);
        helper.setTextColorByAttr(tvKyc, R.attr.color_70white_6b6c73);
        helper.setTextColorByAttr(tvKycTitle, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvNotKycBudget, R.attr.color_1da1f1_fc4f00);
        helper.setTextColorByAttr(tvNotKycSpending, R.attr.color_white_090a15);
        helper.setBackgroundResourceByAttr(tvCompleteKyc, R.attr.bg_btn_next_page_card);
        helper.setBackgroundResourceByAttr(llNotKyc, R.attr.shape_20white_e4e4e4_stroke_16);
        helper.setBackgroundResourceByAttr(llNotKycSpending, R.attr.shape_20white_e4e4e4_stroke_16);
        helper.setBackgroundResourceByAttr(llBudget, R.attr.shape_061f6f_spending_d9d9d9);
        helper.setImageResourceByAttr(ivKyc, R.attr.icon_home_info);
        helper.setImageResourceByAttr(ivNoContacts, R.attr.icon_home_no_contacts);
        helper.setTextColorByAttr(tvNoContacts1, R.attr.color_white_090a15);
        helper.setTextColorByAttr(tvNoContacts2, R.attr.color_1da1f1_fc4f00);


        isHideMoney = SpUtils.getInstance().getHidePwd();
        if (isHideMoney) {
            ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_hide));
            tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        } else {
            ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_unhide));
            tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }

        if (homeIcAdapter != null) {
            homeIcAdapter.changeTheme();
        }

        if (homeContactAdapter != null) {
            homeContactAdapter.changeTheme();
        }

        AndroidUtils.setDrawableStart(mContext, tvHomeAddMoney, ThemeSourceUtils.getSourceID(mContext, R.attr.home_add_money));
        AndroidUtils.setDrawableStart(mContext, tvHomeAccount, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_home_account));
        AndroidUtils.setDrawableEnd(mContext, tvHomeShare, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_share));
        AndroidUtils.setDrawableEnd(mContext, tvSetUp, ThemeSourceUtils.getSourceID(mContext, R.attr.icon_spending_set_up));

//        helper.setImageBgResourceByAttr(ivMarketPlace, R.attr.icon_vouchers_history);
        helper.setBackgroundColorByAttr(homeBg, R.attr.color_030b28_f0f7fe);
        showRequestCenterIcon();
//        if (userInfoEntity != null) {
//            if (TextUtils.isEmpty(userInfoEntity.avatar)) {
//                Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, userInfoEntity.gender)).into(ivAvatar);
//            }
//        }
    }

    @Override
    public HomeFragmentContract.Presenter getPresenter() {
        return new HomeFragmentPresenter();
    }

    @Override
    public void getUserInfoSuccess(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            AppClient.getInstance().getUserManager().bindUserInfo(userInfoEntity, false);
            initUser(userInfoEntity);
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        try {
            if (transferLimitEntity != null) {
                if (isPendingBalance) {
                    isPendingBalance = false;
                    if (pendingBanlance != null) {
                        if (!TextUtils.isEmpty(transferLimitEntity.totalMonthLimitAmt)) {
                            pendingBanlance.totalMonthLimitAmt = AndroidUtils.getTransferMoney(transferLimitEntity.totalMonthLimitAmt);
                        }
                        showPendingBalanceDialog(pendingBanlance,totalHoldAmount);
                    }
                } else if (isShowCharity) {
                    isShowCharity = false;
                    HashMap<String, Object> limitMap = new HashMap<>();
                    limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
                    ActivitySkipUtil.startAnotherActivity(mActivity, CharityActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    HashMap<String, Object> limitMap = new HashMap<>();
                    limitMap.put(Constants.TransferLimitEntity, transferLimitEntity);
                    ActivitySkipUtil.startAnotherActivity(mActivity, CreateSplitBillActivity.class, limitMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    // 显示营销信息列表
    @Override
    public void showMarketList(AllMarketInfoEntity result) {
        bannerList.clear();
        bannerList.addAll(result.marketActivityInfos);
        homeBannerAdapter.setDatas(bannerList);
//        homeBannerAdapter.setDatas(bannerList);
//        MarketListAdapter marketListAdapter = new MarketListAdapter(result.marketActivityInfos);
//        marketListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
//                if (ButtonUtils.isFastDoubleClick()) return;
//                if (result.marketActivityInfos.get(position).referralCodeActivityFlag.equals("Y")) {
//                    mPresenter.getRefCodeDescDetails();
//                } else {
//                    mPresenter.getMarketDetails(result.marketActivityInfos.get(position).activityNo);
//                }
//            }
//        });
    }

    // 显示营销详情dialog
    @Override
    public void showMarketDetailsDia(MarketDetailsEntity result) {
        MarketDetailsDialogFragment fragment = MarketDetailsDialogFragment.newInstance(result);
        if (getFragmentManager() != null) {
            fragment.show(getFragmentManager(), "MarketDetails");
        }
    }

    @Override
    public void showMarketDetailsForRefCodeDia(ReferralCodeDescEntity result) {
        MarketDetailsForRefCodeDia dialog = MarketDetailsForRefCodeDia.newInstance(result);
        dialog.setConfirmClickListener(new MarketDetailsForRefCodeDia.OnConfirmClickListener() {
            @Override
            public void onConfirmClick() {
                if (isKyc(Constants.KYC_TYPE_REFERRAL_CODE)) {
                    mPresenter.getInviteDetail();
//                    ActivitySkipUtil.startAnotherActivity(getActivity(), ReferralCodeActivity.class,
//                            ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
        if (getFragmentManager() != null) {
            dialog.show(getFragmentManager(), "ReferralCodeMarket");
        }
    }

    // 显示电子代金券的列表。
    @Override
    public void showVoucherBrandList(EVoucherEntity result) {
    }

    // 隐藏电子代金券列表
    @Override
    public void hideVoucherBrandList(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

//    private void showVoucherBrand(ArrayList<EVoucherEntity.StoreInfo> data, MarqueeView targetView) {
//        if (data == null || data.size() <= 0) {
//            targetView.setVisibility(View.GONE);
//            return;
//        }
//        targetView.setVisibility(View.VISIBLE);
//        ArrayList<View> source = new ArrayList<>();
//        int multiple = 2; // 最小倍数
//        while (multiple * data.size() < 16) {
//            multiple = multiple * 2;
//        }
//        for (int i = 0; i < data.size() * multiple; i++) {
//            View view = getMarketItem(ThemeUtils.isCurrentDark(mContext) ?
//                    data.get(i % data.size()).voucherBrandDarkLogo :
//                    data.get(i % data.size()).voucherBrandLightLogo);
//            source.add(view);
//        }
//        targetView.addViewInQueue(source);
//        targetView.setScrollSpeed(2);
//        targetView.setViewMargin(15);//间距
//        targetView.setMultiple(multiple);// 设置源数据的倍数显示数
//        targetView.setOnItemClickListener(new MarqueeView.OnItemClickListener() {
//            @Override
//            public void onItemClick(int position) {
//                if (data.size() <= position || data.get(position) == null || ButtonUtils.isFastDoubleClick()) {
//                    return;
//                }
//                if (isKyc(Constants.KYC_TYPE_E_VOUCHER)) {
//                    mPresenter.checkOrderStatus(data, position, SCENE_TYPE_VOUCHER);
//                }
//            }
//        });
//        targetView.startScroll();
//    }

    // 显示电子代金券支持的国家列表dialog
    @Override
    public void showEVouCountryDia(VouSupCountryEntity result, String providerCode, String brandCode, String brandName) {
        result.voucherServiceProviderCode = providerCode;
        result.voucherBrandCode = brandCode;
        result.brandName = brandName;
        EVoucherSelectCountryDialogFrag dialogFrag = EVoucherSelectCountryDialogFrag.newInstance(result);
        if (getFragmentManager() != null) {
            dialogFrag.show(getFragmentManager(), "voucherS");
            dialogFrag.setOnCountryClickListener(new EVoucherSelectCountryDialogFrag.CountryClickListener() {
                @Override
                public void onCountryClick(VouSupCountryEntity countryEntity, int position) {
                    mPresenter.skipToVouTypeAndInfo(mActivity, countryEntity.voucherServiceProviderCode, countryEntity.voucherBrandCode,
                            countryEntity.brandName, countryEntity.voucherSupportCountry.get(position).countryCode);
                }
            });
        }
    }

    // 显示话费充值商店列表dialog
    @Override
    public void showTopUpSelectDia(ArrayList<EVoucherEntity.StoreInfo> data) {
        EVoucherSelectCountryDialogFrag dia = EVoucherSelectCountryDialogFrag.newInstance(data);
        if (getFragmentManager() != null) {
            dia.show(getFragmentManager(), "voucherS");
            dia.setListener(new EVoucherSelectCountryDialogFrag.OnOperatorClickListener() {
                @Override
                public void onOperatorClick(int position) {
                    mPresenter.checkOrderStatus(data, position, SCENE_TYPE_TOP_UP);
                }
            });
        }
    }

    // 用户已经在当前商户下单并未完成时的提示dialog
    @Override
    public void showOrderRepeatDia(ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType) {
        OrderRepeatNoticeDia dia = OrderRepeatNoticeDia.newInstance(data.get(position).voucherBrandName);
        if (getFragmentManager() != null) {
            dia.show(getFragmentManager());
            dia.setListener(new OrderRepeatNoticeDia.ContinueClickListener() {
                @Override
                public void continueClick() {
                    mPresenter.continueToSelectTickets(mActivity, data, position, sceneType);
                }
            });
        }
    }

    // 用户在当前商户没有未完成订单时，继续购券流程，包含电子代金券和话费充值
    @Override
    public void checkOrderStatusSuccess(ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType) {
        mPresenter.continueToSelectTickets(mActivity, data, position, sceneType);
    }

    // 显示接口返回的错误信息
    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    // 初始化电子代金券的子view
    private LinearLayout getMarketItem(String url) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.evoucher_store_item, null);
        ImageView imageView = view.findViewById(R.id.iv_store_logo);
        Glide.with(mContext)
                .load(url)
                .dontAnimate()
                .format(DecodeFormat.PREFER_RGB_565)
                .into(imageView);
        return view;
    }

//    private void showKycCheckingDialog(){
//        KycCheckingDialog kycCheckingDialog = new KycCheckingDialog(mContext);
//        kycCheckingDialog.setContinueListener(new KycCheckingDialog.ContinueListener() {
//            @Override
//            public void clickContinue() {
//                kycCheckingDialog.dismiss();
//            }
//        });
//        kycCheckingDialog.showWithBottomAnim();
//    }

    private void showPendingBalanceDialog(BalanceEntity balanceEntity,String totalHoldAmount) {
        pendingBalanceDialog = new PendingBalanceDialog(mContext, balanceEntity, totalHoldAmount);
        pendingBalanceDialog.setConfirmListener(new PendingBalanceDialog.ConfirmListener() {
            @Override
            public void clickConfrim() {
                contactHelp();
//                HashMap<String, Object> mHashMap = new HashMap<>(16);
//                mHashMap.put(Constants.balanceEntity, balanceEntity);
//                ActivitySkipUtil.startAnotherActivity(mActivity, PendingBalanceActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        pendingBalanceDialog.showWithBottomAnim();
    }

    @OnClick({R.id.iv_avatar,R.id.con_avatar, R.id.tv_complete_kyc, R.id.iv_qr_pay, R.id.iv_chat, R.id.iv_request_center, R.id.iv_money_see,
            R.id.tv_all_analytics, R.id.iv_pending_balance,
            R.id.tv_home_account, R.id.tv_home_back, R.id.iv_home_copy, R.id.tv_home_share, R.id.tv_home_add_money,
            R.id.tv_set_up,R.id.tv_not_kyc_budget,R.id.tv_no_contacts2,R.id.tv_freeze_account})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.tv_freeze_account:
                showCommonOneButtonDialog(userInfoEntity.isBlockAccount()?getString(R.string.sprint20_106):getString(R.string.sprint20_103), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_home_account_freeze), getString(R.string.sprint20_104) + "\n"
                        + getString(R.string.sprint20_105), getString(R.string.sprint20_63), new ExpireIdDialog.RenewIdListener() {
                    @Override
                    public void clickRenew() {
                        contactHelp();
                    }
                });
                break;
            case R.id.tv_home_add_money:
                addMoney();
                break;
            case R.id.iv_home_copy:
                String bankNumber = tvBankNumber.getText().toString().trim();
                AndroidUtils.setClipboardText(mContext, bankNumber, true);
                showTipDialog(getString(R.string.iban_copy));
                break;
            case R.id.tv_home_share:
                downShareImage(llTopContent, shareIbanNum);
                break;
            case R.id.tv_home_account:
            case R.id.tv_home_back:
                flipAnimation();
                break;
            case R.id.iv_pending_balance:
//                totalHoldAmount = "1000000";
//                pendingBanlance = new BalanceEntity();
//                pendingBanlance.balance = "5000000";
//                showPendingBalanceDialog(pendingBanlance, totalHoldAmount);
                if(pendingBanlance != null && pendingBanlance.balanceNotNull()){
                    isPendingBalance = true;
                    mPresenter.getLimit(Constants.LIMIT_ADD_MONEY);
                }else {
                    showPendingBalanceDialog(pendingBanlance, totalHoldAmount);
                }
                break;
            case R.id.tv_complete_kyc:
                isKyc(Constants.KYC_TYPE_NULL);
                break;
            case R.id.con_avatar:
            case R.id.iv_avatar:
//                ReferralCodeDialog.getInstance(mContext, null).showWithBottomAnim();
                ActivitySkipUtil.startAnotherActivity(mActivity, ProfileActivity.class,
                        ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_qr_pay:
                if(AndroidUtils.isCloseUse(mActivity, Constants.send_scan))return;
                if (isKyc(Constants.KYC_TYPE_HOME_SCAN)) {
                    HashMap<String, Object> mHashMap = new HashMap<>(16);
                    mHashMap.put(Constants.isFrom, "home");
                    ActivitySkipUtil.startAnotherActivity(mActivity, ScanQrActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.iv_chat:
                if (AndroidUtils.isCloseUse(mActivity, Constants.home_charity)) return;
                if (isKyc(Constants.KYC_TYPE_NULL)) {
                    hashMap.put("title", getString(R.string.newhome_23));
                    ActivitySkipUtil.startAnotherActivity(mActivity, ComingSoonActivity.class,
                            hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
//                ActivitySkipUtil.startAnotherActivity(mActivity, GetHelpActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_request_center:
                ActivitySkipUtil.startAnotherActivity(mActivity, RequestCenterActivity.class,
                        ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_money_see:
                if (isHideMoney) {
                    //如果已经隐藏了金额  则显示
                    isHideMoney = false;
                    ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_unhide));
                    tvAmount.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    isHideMoney = true;
                    ivMoneySee.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.home_hide));
                    tvAmount.setTransformationMethod(new AsteriskPasswordTransformationMethod());
                }
                SpUtils.getInstance().setHidePwd(isHideMoney);
                break;
            case R.id.tv_all_analytics:
                if (AndroidUtils.isCloseUse(mActivity, Constants.tab_budget)) return;
                if (isKyc(Constants.KYC_TYPE_NULL)) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, AnalyticsActivity.class,
                            hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.tv_set_up:
            case R.id.tv_not_kyc_budget:
                if (AndroidUtils.isCloseUse(mActivity, Constants.tab_budget)) return;
                if (isKyc(Constants.KYC_TYPE_NULL)) {
                    hashMap.put(Constants.openAnalyticsPage, 1);
                    ActivitySkipUtil.startAnotherActivity(mActivity, AnalyticsActivity.class,
                            hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            case R.id.tv_no_contacts2:
                if (isKyc(Constants.KYC_TYPE_HOME_SEND_MONEY)) {
                    ActivitySkipUtil.startAnotherActivity(mActivity, TransferActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void getAnalysisDetailSuccess(SpendingDetailEntity spendingDetailEntity) {
        try {
            if (spendingDetailEntity != null) {
                SpendingActivityDataEntity spendingActivityDataEntity = spendingDetailEntity.tiqmoInnerActivityData;
                String receivedAmount = spendingActivityDataEntity.receivedAmount;
                String receivedTxnCurrency = spendingActivityDataEntity.receivedTxnCurrency;
                String receivedAmountRatio = spendingActivityDataEntity.receivedAmountRatio;
                String sendingAmount = spendingActivityDataEntity.sendingAmount;
                String sendingTxnCurrency = spendingActivityDataEntity.sendingTxnCurrency;
                String sendingAmountRatio = spendingActivityDataEntity.sendingAmountRatio;

                AnalysisGraphicEntity analysisGraphicEntity = spendingDetailEntity.graphicInfo;
                if (analysisGraphicEntity != null) {
                    generateData(analysisGraphicEntity);
                }

                SpendingCategoryBudgetEntity spendingCategoryBudgetInfo = spendingDetailEntity.spendingCategoryBudgetInfo;
                if (spendingCategoryBudgetInfo != null) {
                    //是否设置预算的标识
                    boolean setBudgetFlag = spendingCategoryBudgetInfo.setBudgetFlag;
                    //剩余预算金额
                    String leftSpendableAmount = spendingCategoryBudgetInfo.leftSpendableAmount;
                    //剩余预算币种
                    String leftSpendableTxnCurrency = spendingCategoryBudgetInfo.leftSpendableTxnCurrency;
                    //预算耗尽提醒
                    String budgetSpendingReminder = spendingCategoryBudgetInfo.budgetSpendingReminder;
                    if (!TextUtils.isEmpty(budgetSpendingReminder)) {
                        llSpendingNotice.setVisibility(View.VISIBLE);
                        tvSpendingNotice.setText(budgetSpendingReminder);
                    } else {
                        llSpendingNotice.setVisibility(View.GONE);
                    }

                    if (setBudgetFlag) {
                        tvLeftMoney.setText(Spans.builder()
                                .text(AndroidUtils.getTransferStringMoney(leftSpendableAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(16)
                                .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .build());
                        tvSetUp.setVisibility(View.GONE);
                        llMoneyLeft.setVisibility(View.VISIBLE);
                    } else {
                        tvSetUp.setVisibility(View.VISIBLE);
                        llMoneyLeft.setVisibility(View.GONE);
                    }
                }

            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void generateData(AnalysisGraphicEntity analysisGraphicEntity) {
        try {
            List<AnalysisGraphicColorEntity> itemInfo = analysisGraphicEntity.itemInfo;
            String totalSpendingCategoryCurrency = analysisGraphicEntity.totalSpendingCategoryCurrency;
            String avgSpendingAmount = analysisGraphicEntity.avgSpendingAmount;
            String avgSpendingCategoryCurrency = analysisGraphicEntity.avgSpendingCategoryCurrency;
            LineChartDataEntity lineChartDataEntity = analysisGraphicEntity.lineChartData;
            if (lineChartDataEntity != null) {
                data.clear();
                x.clear();
                y.clear();
                //点对应的数据
                data = lineChartDataEntity.data;
                //X轴刻度值
                x = lineChartDataEntity.x;
                //Y轴刻度
                y = lineChartDataEntity.y;

                pointList.clear();
                yStrList.clear();
                for (int i = 0; i < data.get(0).size(); i++) {
                    LinePointEntity linePointEntity = new LinePointEntity();
                    linePointEntity.isSelect = 0;
                    if (i == data.get(0).size() - 1) {
                        linePointEntity.isSelect = 1;
                    }
                    pointList.add(linePointEntity);
                }
                for (int i = 0; i < y.size(); i++) {
                    yStrList.add(AndroidUtils.getTransferIntNumber(y.get(i)));
                }
                int maxY = AndroidUtils.getTransferMoneyInt(y.get(y.size() - 1));
                float avgValue = AndroidUtils.getTransferMoneyFloat(avgSpendingAmount);
                brokenView.setView(lineChartDataEntity.getDataMap(data.get(0)), x, yStrList, maxY, avgValue, pointList, 50);

            }

        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

}