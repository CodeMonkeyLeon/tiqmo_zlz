package cn.swiftpass.wallet.tiqmo.support.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import cn.swiftpass.wallet.tiqmo.module.register.entity.UserUnableUseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AreaEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.LanguageListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.app2app.PaySDKEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class SpUtils extends SpBaseUtil {
    private static Context applicationContext;

    @SuppressLint("StaticFieldLeak")
    private static final class InstanceHolder {
        private static final SpUtils INSTANCE = new SpUtils();
    }

    private SpUtils() {
        super(applicationContext);
        if (applicationContext == null) {
            throw new IllegalArgumentException("请先在application中调用init方法初始化！");
        }
    }

    /**
     * 先在application中初始化
     */
    public static void init(Context context) {
        applicationContext = context.getApplicationContext();
    }

    public static SpUtils getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected String getSpName() {
        return "sp_config";
    }

    /**
     * @return
     * @throws
     * @Title: UserMapBase64
     * @Description: 存储用户的登录次数
     */
    public void setUserLoginCountInfo(String UserMapBase64) {
        putString("User_login_count", UserMapBase64);
    }

    /**
     * @return
     * @throws
     * @Title:
     * @Description: 获取用户的登录次数
     */
    public String getUserLoginCountInfo() {

        return getString("User_login_count", null);
    }

    public String getAppLanguageForServer() {
        String language = getString("App_setting_language", "");
        if (AndroidUtils.isHKLanguage(language)) {
            language = Constants.LANG_CODE_ZH_HK;
        } else if (AndroidUtils.isEGLanguage(language)) {
            language = Constants.LANG_CODE_EN_US;
        } else {
            language = Constants.LANG_CODE_ZH_CN;
        }
        return language;
    }

    public String getAppLanguage() {
        String language = getString("App_setting_language", "");
        return language;
    }

    public void setAppLanguage(String language) {
        putString("App_setting_language", language);
    }

    public void setQrCode(String qrCode) {
        putString(AppClient.getInstance().getUserID() + "qrCode", qrCode);
    }

    public String getQrCode() {
        String qrCode = getString(AppClient.getInstance().getUserID() + "qrCode", "");
        return qrCode;
    }

    public void setPayMethodCode(String payMethodCode) {
        putString(AppClient.getInstance().getUserID() + "payMethodCode", payMethodCode);
    }

    public String getPayMethodCode() {
        String payMethodCode = getString(AppClient.getInstance().getUserID() + "payMethodCode", "");
        return payMethodCode;
    }

    public void setFirstTopUp(String isFirstTopUp) {
        putString(AppClient.getInstance().getUserID() + "isFirstTopUp", isFirstTopUp);
    }

    public String isFirstTopUp() {
        return getString(AppClient.getInstance().getUserID() + "isFirstTopUp","");
    }

    public void setAvailableBalance(String availableBalance) {
        putString(AppClient.getInstance().getUserID() + "availableBalance", availableBalance);
    }

    public String getAvailableBalance() {
        String availableBalance = getString(AppClient.getInstance().getUserID() + "availableBalance", "");
        return availableBalance;
    }

    public void setReqCounts(int reqCounts) {
        putInt(AppClient.getInstance().getUserID() + "reqCounts", reqCounts);
    }

    public int getReqCounts() {
        int reqCounts = getInt(AppClient.getInstance().getUserID() + "reqCounts", 0);
        return reqCounts;
    }

    public void setChannelCode(String  channelCode) {
        putString(AppClient.getInstance().getUserID() + "channelCode", channelCode);
    }

    public String getChannelCode() {
        String channelCode = getString(AppClient.getInstance().getUserID() + "channelCode", Constants.imr_TransFast);
        return channelCode;
    }

    public void setLongitude(String longitude) {
        putString("longitude", longitude);
    }
    public void setLatitude(String latitude) {
        putString("latitude", latitude);
    }

    public String getLongitude() {
        String longitude = getString("longitude", "113.9475016");
        return longitude;
    }
    public String getLatitude() {
        String latitude = getString("latitude", "22.5407226");
        return latitude;
    }

    public void setDeviceToken(String deviceToken) {
        putString("deviceToken", deviceToken);
    }

    public String getDeviceToken() {
        String deviceToken = getString("deviceToken", "");
        return deviceToken;
    }
    public void setProfilePicFileId(String profilePicFileId) {
        putString(AppClient.getInstance().getUserID()+"profilePicFileId", profilePicFileId);
    }

    public String getProfilePicFileId() {
        String profilePicFileId = getString(AppClient.getInstance().getUserID()+"profilePicFileId", "");
        return profilePicFileId;
    }

    public void setLastDevelopUrl(String url) {
        putString("lastDevelopUrl", url);
    }

    public String getLastDevelopUrl() {
        String developUrl = getString("lastDevelopUrl", "");
        return developUrl;
    }

    public void setHasNotify(boolean hasNotify) {
        putBoolean(AppClient.getInstance().getUserID() + "hasNotify", hasNotify);
    }

    public boolean getHasNotify() {
        boolean hasNotify = getBoolean(AppClient.getInstance().getUserID() + "hasNotify", false);
        return hasNotify;
    }

    public void setLoginUser(UserInfoEntity userInfoEntity) {
        putSerializable("appUseList", userInfoEntity);
    }

    public UserInfoEntity getLoginUser() {
        return getSerializable("appUseList", UserInfoEntity.class,"");
    }

    public void setUserUnableUseEntity(UserUnableUseEntity userUnableUseEntity) {
        putSerializable("appUseList", userUnableUseEntity);
    }

    public UserUnableUseEntity getUserUnableUseEntity() {
        return getSerializable("appUseList", UserUnableUseEntity.class,"");
    }

    public void setLockTime(int lockTime) {
        putInt(AppClient.getInstance().getUserID() + "lockTime", lockTime);
    }

    public int getLockTime() {
        int lockTime = getInt(AppClient.getInstance().getUserID() + "lockTime", 0);
        return lockTime;
    }

    public void setLockTimeMsg(String lockTimeMsg) {
        putString("lockTimeMsg", lockTimeMsg);
    }

    public String getLockTimeMsg() {
        String lockTimeMsg = getString("lockTimeMsg", "");
        return lockTimeMsg;
    }

    public void setCertZipName(String certZipName) {
        putString("certZipName", certZipName);
    }

    public void setReferCode(String referCode) {
        putString("referCode", referCode);
    }

    public String getReferCode() {
        return getString("referCode", "");
    }

    public void setReferralCode(String referCode) {
        putString("referralCode", referCode);
    }

    public String getReferralCode() {
        return getString("referralCode", "");
    }

    public void setShareLink(String shareLink) {
        putString("shareLink", shareLink);
    }

    public String getShareLink() {
        return getString("shareLink", "");
    }

    public void setKycChecking(boolean kycChecking) {
        putBoolean("kycChecking", kycChecking);
    }

    public boolean isKycChecking() {
        return getBoolean("kycChecking", false);
    }

    public String getCertZipName() {
        String certZipName = getString("certZipName", "");
        return certZipName;
    }

    public void setLanguageVersion(String languageVersion) {
        putString("languageVersion", languageVersion);
    }

    public String getLanguageVersion() {
        String languageVersion = getString("languageVersion", "");
        return languageVersion;
    }
    public void setExternalId(String externalId) {
        putString("externalId", externalId);
    }

    public String getExternalId() {
        String externalId = getString("externalId", "");
        return externalId;
    }

    public void setHidePwd(boolean hidePwd) {
        putBoolean("hidePwd", hidePwd);
    }

    public boolean getHidePwd() {
        boolean qrCode = getBoolean("hidePwd", false);
        return qrCode;
    }

//    /**
//     * 是否是首次打开qrCode
//     */
//    public void setFirstQrCode(boolean firstQrCode) {
//        putBoolean("firstQrCode", firstQrCode);
//    }

    public boolean getFirstQrCode() {
        boolean firstQrCode = getBoolean("firstQrCode", true);
        return firstQrCode;
    }

//    public void setAppLanguage(String language) {
//        if (TextUtils.isEmpty(language)) {
//            return;
//        }
//        LogUtils.i("LANGUAGE", "language:" + language);//en_HK--- zh_CN--- zh_HK zh_MO
//        if (language.startsWith("en")) {
//            language = Constants.LANG_CODE_EN_US;
//        } else if (language.startsWith("zh")) {
//            if (language.endsWith("CN") || language.endsWith("cn")) {
//                language = Constants.LANG_CODE_ZH_CN;
//            } else if (language.endsWith("HK") || language.endsWith("hk")) {
//                language = Constants.LANG_CODE_ZH_HK;
//            } else {
//                language = Constants.LANG_CODE_ZH_TW;
//            }
//        }
//
//        putString("App_setting_language", language);
//    }


    /**
     * @param isFirstLaunch
     * @throws
     * @Title: setIsFirstLaunch
     * @Description:
     */
    public void setIsFirstLaunch(boolean isFirstLaunch) {
        String version = AndroidUtils.getCurrentAppVersionName(mContext);
        putBoolean("is_first_launch_" + version, isFirstLaunch);
    }

    /**
     * @return
     * @throws
     * @Title: getIsFirstLaunch
     * @Description: 是否是当前版本的第一次打开
     */
    public boolean getIsFirstLaunch() {
        String version = AndroidUtils.getCurrentAppVersionName(mContext);
        return getBoolean("is_first_launch_" + version, true);
    }

    public void setAreaEntity(AreaEntity areaEntity) {
        putSerializable("area_entity", areaEntity);
    }

    public AreaEntity getAreaEntity() {
        return getSerializable("area_entity", AreaEntity.class, "");
    }

    public void setLanguageListEntity(LanguageListEntity languageListEntity) {
        putSerializable("languageListEntity", languageListEntity);
    }

    public LanguageListEntity getLanguageListEntity() {
        return getSerializable("languageListEntity", LanguageListEntity.class, "");
    }

    public void setLanguageCodeEntity(LanguageCodeEntity languageCodeEntity) {
        putSerializable("languageCodeEntity", languageCodeEntity);
    }

    public LanguageCodeEntity getLanguageCodeEntity() {
        return getSerializable("languageCodeEntity", LanguageCodeEntity.class, "");
    }

    public String getCallingCode() {
        if (getAreaEntity() != null) {
            return getAreaEntity().callingCode;
        }
        return Constants.CALLINGCODE_KSA;
    }

    /**
     * @param paySDKEntity
     * @throws
     * @Title: setIsFromPaySDK
     * @Description:
     */
    public void setPaymentCheck(PaySDKEntity paySDKEntity) {
        putSerializable("payment_entity", paySDKEntity);
    }

    /**
     * @return
     * @throws
     * @Title: getPaymentCheck
     * @Description: 如果是三方调起则使用
     */
    public PaySDKEntity getPaymentCheck() {
        return getSerializable("payment_entity", PaySDKEntity.class,"");
    }


    /**
     * 设置升级弹出框每周显示一次
     */
    public boolean isNeedShowUpdateDialog() {
        long time = getLong("update_dialog_time", -1);
        long leftTime = System.currentTimeMillis() - time;
        if (leftTime > (7 * 24 * 3600) || leftTime < 0) {
            putLong("update_dialog_time", System.currentTimeMillis());
            return true;
        }
        return false;
    }


    public String getAppVersionName() {
        String versionName = getString("App_Version_Name", "");
        return versionName;
    }

    public void setAppVersionName(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException var3) {
            LogUtils.e("setAppVersionName", Log.getStackTraceString(var3));
        }
        String versionName = pInfo.versionName;
        putString("App_Version_Name", versionName);
    }

}
