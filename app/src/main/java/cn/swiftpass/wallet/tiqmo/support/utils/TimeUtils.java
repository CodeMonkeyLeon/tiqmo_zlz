package cn.swiftpass.wallet.tiqmo.support.utils;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;
import org.joda.time.format.DateTimeFormat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TimeUtils {

    //是否是第一天
    public static boolean isFirstDay(String firstDay,boolean isMonth) {
        if(isMonth){
            return "2022-01-01".equals(firstDay);
        }else {
            return "2021-12-26".equals(firstDay);
        }
    }

    //根据日期拿到当前年份的第一天
    public static Date getFirstDayByYear(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 0);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    //根据日期拿到当前年份的第一天
    public static Date getCurrentYear() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.YEAR, 0);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    //获取上一年第一天
    public static Date getLastYear(Date date) {
//        Calendar calendar = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    //获取上个月第一天
    public static Date getLastMonth(Date date) {
//        Calendar calendar = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    //获取当前月第一天
    public static Date getCurrentMonth() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    //获取第一个月第一天的日期
    public static Date getFirstMonth() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    //获取下个月第一天
    public static Date getNextMonth(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
//        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static String getChatTime(long time) {
        Date date = new Date(time);
        DateFormat df = new SimpleDateFormat("h:mm a",Locale.ENGLISH);
        String format = df.format(date);
        format = format.replace("AM","am");
        format = format.replace("PM","pm");
        return format;
    }

    public static String getRequestDay(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        String format = df.format(date);
        return format;
    }

    public static String getNumberDay(String birthday) {
        try {
            Date date = DateFormat.getDateInstance().parse(birthday);
            DateFormat df = new SimpleDateFormat("yyyyMMdd",Locale.ENGLISH);
            String format = df.format(date);
            return format;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getGroupCreateDay(String createAt) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            Date date = df.parse(createAt);
            String format = monthToEn(date)+" "+yearToEn(date);
            return format;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getChatUserJoinDay(long joinDay){
        try {
            Date joinDate = new Date(joinDay);
            DateFormat df = new SimpleDateFormat("MMMM yyyy",Locale.ENGLISH);
            String format = df.format(joinDate);
            return format;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String monthToEn(Date date) {
        return new SimpleDateFormat("MMMM", Locale.US).format(date);
    }

    public static String yearToEn(Date date) {
        return new SimpleDateFormat("yyyy", Locale.ENGLISH).format(date);
    }

    /**
     * 获取当前周的第一天
     *
     * @return String
     **/
    public static Date getWeekStart(Date date) {
//        Calendar cal = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        return calendar.getTime();
    }

    public static Date getCurrentWeekStart() {
//        Calendar cal = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        return calendar.getTime();
    }

    /**
     * 获取当前周最后一天
     *
     * @return String
     **/
    public static Date getWeekEnd(Date date) {
//        Calendar cal = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() + 6);
        return calendar.getTime();
    }



    //获取上一周
    public static Date getLastWeek1(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        return calendar.getTime();
    }

    public static Date getLastWeek(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
//        Calendar calendar = Calendar.getInstance();
// 设置指定日期, 获取指定日期的年 周
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int week = calendar.get(Calendar.WEEK_OF_YEAR) - 1;
// 获取当前周的上一周date对象
        week = week - 1;
        Calendar calendarNew = Calendar.getInstance();
//        calendarNew.set(Calendar.YEAR, year);
        calendarNew.set(Calendar.MONTH, Calendar.JANUARY);
        calendarNew.set(Calendar.DATE, 1);
        Calendar cal = (Calendar) calendarNew.clone();
        cal.add(Calendar.DATE, week * 7);
        return cal.getTime();
    }

    //获取下一周
    public static Date getNextWeek(Date date) {
//        Calendar calendar = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
// 设置指定日期, 获取指定日期的年 周
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int week = calendar.get(Calendar.WEEK_OF_YEAR) - 1;
// 获取当前周的下一周date对象
        week = week + 1;
        Calendar calendarNew = Calendar.getInstance();
//        calendarNew.set(Calendar.YEAR, year);
        calendarNew.set(Calendar.MONTH, Calendar.JANUARY);
        calendarNew.set(Calendar.DATE, 1);
        Calendar cal = (Calendar) calendarNew.clone();
        cal.add(Calendar.DATE, week * 7);
        return cal.getTime();
    }

    public static String dateToEn(Date date) {
        try {
            String dayNumberSuffix = getDayNumberSuffix(Integer.parseInt(String.format("%td", date)));
            return new SimpleDateFormat("d MMM", Locale.US).format(date).replace(" ", dayNumberSuffix + " ");
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return "";
    }

    private static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    /**
     * 伊斯兰日历转公历
     * @return
     */
    public static String getGregorianToUmmalqura(UmmalquraCalendar ummalquraCalendar){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("", Locale.ENGLISH);
            dateFormat.setCalendar(ummalquraCalendar);
            dateFormat.applyPattern("yyyy-MM-dd");
            String birNum = dateFormat.format(ummalquraCalendar.getTime());
            org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime dateTime = DateTime.parse(birNum, dateTimeFormatter);

            Chronology iso = ISOChronology.getInstanceUTC();
            Chronology hijri = IslamicChronology.getInstanceUTC();

            int year1 = dateTime.getYear();
            int month1 = dateTime.getMonthOfYear();
            int day1 = dateTime.getDayOfMonth();
            int customHr = 0;
            int customMin = 0;
            int customSec = 0;
            DateTime todayHijri = new DateTime(year1, month1, day1, customHr, customMin, customSec, 0, hijri);
            org.joda.time.LocalDateTime todayIso = new org.joda.time.LocalDateTime(todayHijri, iso);

            String birthday = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(todayIso.toDate());
            return birthday;
        }catch (Throwable e){
            e.printStackTrace();
        }
        return "";
    }

    public static String getEnUmmalqura(UmmalquraCalendar ummalquraCalendar){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("", Locale.ENGLISH);
            dateFormat.setCalendar(ummalquraCalendar);
            dateFormat.applyPattern("MMMM d, y");
            String bir = dateFormat.format(ummalquraCalendar.getTime());
            return bir;
        }catch (Throwable e){
            e.printStackTrace();
        }
        return "";
    }

}
