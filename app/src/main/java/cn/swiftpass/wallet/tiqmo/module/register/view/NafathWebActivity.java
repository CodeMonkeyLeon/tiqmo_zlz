package cn.swiftpass.wallet.tiqmo.module.register.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseWebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathResultEntity;
import cn.swiftpass.wallet.tiqmo.module.register.entity.NafathUrlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class NafathWebActivity extends BaseWebViewActivity {
    /**
     * ERRORCODE_080122("080122", "Please enter a valid ID"),
     * ERRORCODE_080123("080123", "Please enter a valid mobile number"),
     * ERRORCODE_080121("080121", "Mobile number is not linked with ID/iqama in ELM. Please contact ELM to update details"),
     */

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.webView)
    WebView webView;

    private String hashedState,recipientId,authUrl,redirectUrl,noStr;
    private String phoneNo;
    private NafathUrlEntity nafathUrlEntity;
    private CheckPhoneEntity checkPhoneEntity;
    private int operateType;
    private String random;

    /**
     * 强制停止Nafath结果查询
     */
    private boolean isForceStopQuery;
    /**
     * Nafath结果查询 倒计时查询结果
     */
    private Timer queryNafathResultTimer;
    private int retainQueryTime = 60;
    private int retainQueryTimeDelay = 15;

    private Handler queryHandler;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_nafath_webview;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

        LocaleUtils.viewRotationY(this, ivBack, headCircle);
        ivBack.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.sprint11_94));

        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if(checkPhoneEntity != null){
            phoneNo = checkPhoneEntity.phone;
            noStr = checkPhoneEntity.noStr;
            recipientId = checkPhoneEntity.checkIdNumber;
            authUrl = checkPhoneEntity.authUrl;
            hashedState = checkPhoneEntity.hashedState;
            redirectUrl = checkPhoneEntity.redirectUrl;
            operateType = checkPhoneEntity.operateType;
            random = checkPhoneEntity.random;
            userInfoEntity = getUserInfoEntity();
            if(!TextUtils.isEmpty(phoneNo)){
                userInfoEntity.phone = phoneNo;
            }

            if(!TextUtils.isEmpty(authUrl)){
                webView.setWebViewClient(new WebViewClient() {

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        LogUtils.i(TAG, "shouldOverrideUrlLoading:" + url);
//                        url = redirectUrl;
                        if (url.equals(redirectUrl)) {
                            isForceStopQuery = false;
                            //开始倒计时查询结果
                            queryNafathResultTimer = new Timer();
                            TimerTask queryVideoResultTimerTask = new TimerTask() {
                                @Override
                                public void run() {
                                    queryHandler.post(queryVideoResultRunnable);
                                }
                            };
                            queryNafathResultTimer.schedule(queryVideoResultTimerTask, 0, 3000);
                        }else{
                            view.loadUrl(url);
                        }
                        return true;
                    }


                    @Override
                    public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                        super.onReceivedClientCertRequest(view, request);
                        LogUtils.i(TAG, "onReceivedClientCertRequest:" + view.getUrl());
                    }

                    @Override
                    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
                        LogUtils.i(TAG, "onReceivedError:" + error.toString());
                    }

                    @Override
                    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                        super.onReceivedHttpError(view, request, errorResponse);
                        LogUtils.i(TAG, "onReceivedHttpError:" + errorResponse.toString());
                    }

                });
                initWebView(webView, authUrl, true);
            }
        }
        queryHandler = new Handler();
    }

    @OnClick({R.id.iv_back,R.id.tv_title})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_title:
                requestReg();
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeNafathCall();
    }

    private void removeNafathCall(){
        if (queryNafathResultTimer != null) {
            queryNafathResultTimer.cancel();
        }
        if (queryHandler != null && queryVideoResultRunnable != null) {
            queryHandler.removeCallbacks(queryVideoResultRunnable);
        }
        isForceStopQuery = true;
    }

    /**
     * 轮训查询Ivr结果
     */
    private Runnable queryVideoResultRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isForceStopQuery) {
                requestReg();
            }
        }
    };

    private void requestReg(){
        showProgress(true);
        AppClient.getInstance().getUserManager().getNafathResult(UserInfoManager.getInstance().getCallingCode(),phoneNo,hashedState, recipientId,random,operateType == 2? Constants.SCENE_TYPE_FORGET_PD:Constants.SCENE_TYPE_REG_PHONE, new ResultCallback<NafathResultEntity>() {
            @Override
            public void onResult(NafathResultEntity nafathResultEntity) {
                if(nafathResultEntity != null){
//                            nafathResultEntity.authStatus = "S";
                    String authStatus = nafathResultEntity.authStatus;
                    if(nafathResultEntity.isAuthSuccess()){
                        removeNafathCall();
                        if(operateType == 3){
                            //修改手机密码 更新用户信息
                            if (checkPhoneEntity != null) {
                                String changePhoneOrderNo = checkPhoneEntity.changePhoneOrderNo;
                                if(!TextUtils.isEmpty(changePhoneOrderNo)){
                                    userInfoEntity.orderNo = changePhoneOrderNo;
                                }
                                AppClient.getInstance().getUserManager().updateUserInfo(userInfoEntity, new ResultCallback<UserInfoEntity>() {
                                    @Override
                                    public void onResult(UserInfoEntity userInfoEntity) {
                                        showProgress(false);
                                        ProjectApp.changePhoneSuccess = true;
//                                        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_CHANGE_PHONE_SUCCESS));
                                        ProjectApp.removeAllTaskExcludeMainStack();
                                        finish();
                                    }

                                    @Override
                                    public void onFailure(String errorCode, String errorMsg) {
                                        showProgress(false);
                                        showTipDialog(errorMsg);
                                    }
                                });
                            }

                        }else {
                            showProgress(false);
                            String firstName = nafathResultEntity.firstName;
                            String birthday = nafathResultEntity.birthday;
                            if (checkPhoneEntity != null) {
                                checkPhoneEntity.firstName = firstName;
                                if (!TextUtils.isEmpty(birthday)) {
                                    checkPhoneEntity.birthday = birthday;
                                }
                                UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                            }
                            ActivitySkipUtil.startAnotherActivity(NafathWebActivity.this, RegSetPwdOneActivity.class, RIGHT_IN);
                            ProjectApp.removeNafathTask();
                            finish();
                        }
                    }else if(nafathResultEntity.isAuthFail()) {
                        showProgress(false);
                        removeNafathCall();
                        String failType = nafathResultEntity.failType;
//                                failType = "1";
                        if (!TextUtils.isEmpty(failType)) {
                            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_NAFATH_FAIL, failType));
                        }
                        ProjectApp.removeNafathTask();
                        finish();
                    }
                }else{
                    showProgress(false);
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                showProgress(false);
                showTipDialog(errorMsg);
                removeNafathCall();
                finish();
            }
        });
    }
}
