package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankIdAddressEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AddBeneThreePresenter implements AddBeneficiaryContract.AddThreePresenter {

    AddBeneficiaryContract.AddThreeView mAddThreeView;

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mAddThreeView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mAddThreeView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddThreeView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateBeneficiary(String payeeInfoId) {
        AppClient.getInstance().activateIvrBeneficary(payeeInfoId, new LifecycleMVPResultCallback<Void>(mAddThreeView,true) {
            @Override
            protected void onSuccess(Void result) {
                mAddThreeView.activateBeneficiarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddThreeView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getNationality(String countryType,String countryCode) {
        AppClient.getInstance().imrCountryList(countryType,countryCode, new LifecycleMVPResultCallback<ImrCountryListEntity>(mAddThreeView, true) {
            @Override
            protected void onSuccess(ImrCountryListEntity result) {
                mAddThreeView.getNationalitySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddThreeView.getNationalityFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrGetIdOrAddress(String receiptMethod, String countryCode) {
        AppClient.getInstance().imrGetIdOrAddress(receiptMethod, countryCode, new LifecycleMVPResultCallback<ImrBankIdAddressEntity>(mAddThreeView, true) {
            @Override
            protected void onSuccess(ImrBankIdAddressEntity result) {
                mAddThreeView.imrGetIdOrAddressSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddThreeView.imrGetIdOrAddressFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName, String nickName, String relationshipCode, String callingCode, String phone, String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace, String birthDate, String sex, String cityName, String districtName, String poBox, String buildingNo, String street, String idNo, String idExpiry, String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag, String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode, String channelPayeeId, String channelCode,String branchId) {
        AppClient.getInstance().imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag,receiptOrgName,
                receiptOrgBranchName,cityId,currencyCode,channelPayeeId,channelCode,branchId, new LifecycleMVPResultCallback<ImrAddBeneResultEntity>(mAddThreeView, true) {
                    @Override
                    protected void onSuccess(ImrAddBeneResultEntity result) {
                        mAddThreeView.imrAddBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mAddThreeView.imrAddBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void imrExchangeRate(String payeeInfoId,String sourceAmount,
                                String destinationAmount,String channelCode) {
        AppClient.getInstance().imrExchangeRate(payeeInfoId, sourceAmount,
                destinationAmount, channelCode, new LifecycleMVPResultCallback<ImrExchangeRateEntity>(mAddThreeView, true) {
                    @Override
                    protected void onSuccess(ImrExchangeRateEntity result) {
                        mAddThreeView.imrExchangeRateSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mAddThreeView.imrExchangeRateFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mAddThreeView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                mAddThreeView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mAddThreeView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddBeneficiaryContract.AddThreeView view) {
        this.mAddThreeView = view;
    }

    @Override
    public void detachView() {
        this.mAddThreeView = null;
    }
}
