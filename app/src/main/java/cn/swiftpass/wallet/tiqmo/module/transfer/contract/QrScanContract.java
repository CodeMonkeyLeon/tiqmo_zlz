package cn.swiftpass.wallet.tiqmo.module.transfer.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public class QrScanContract {

    public interface View extends BaseView<Presenter> {
        void onCheckStartCodeFailed(String errorCode, String errorMsg);

        void onCheckStartCodeSuccess(Void response);

    }


    public interface Presenter extends BasePresenter<View> {
        void checkStartCode(String activeCode);

    }
}
