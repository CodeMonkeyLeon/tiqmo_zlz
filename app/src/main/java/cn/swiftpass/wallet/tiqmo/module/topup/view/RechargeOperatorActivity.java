package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class RechargeOperatorActivity extends BaseCompatActivity {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.fl_recharge_operator)
    FrameLayout flRechargeOperator;
    private RechargeDomesticFragment rechargeDomesticFragment;
    private RechargeOrderListEntity rechargeOrderListEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_operator;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.topup_1);
        LocaleUtils.viewRotationY(mContext,ivBack,ivHeadCircle);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if(getIntent() != null && getIntent().getExtras() != null) {
            rechargeOrderListEntity = (RechargeOrderListEntity) getIntent().getExtras().getSerializable(Constants.rechargeOrderListEntity);
            rechargeDomesticFragment = RechargeDomesticFragment.getInstance(rechargeOrderListEntity,true);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_recharge_operator, rechargeDomesticFragment);
            ft.commit();
        }
    }
}
