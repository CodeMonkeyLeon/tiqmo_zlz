package cn.swiftpass.wallet.tiqmo.module.register.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.animation.ViewAnim;
import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterPhoneCheckContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.RegisterPhoneCheckPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.SetBranchActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.GPSUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.ErrorBottomDialog;

public class RegisterActivity extends BaseCompatActivity<RegisterPhoneCheckContract.Presenter> implements RegisterPhoneCheckContract.View {

    @BindView(R.id.et_phone)
    EditTextWithDel etPhone;
    @BindView(R.id.tv_content_title)
    TextView tvContentTitle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_calling_code)
    TextView tvCallingCode;
    @BindView(R.id.iv_calling_code)
    ImageView ivCallingCode;
    @BindView(R.id.con_area)
    RelativeLayout conArea;
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_reg_back)
    ImageView ivRegBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.back_head)
    ConstraintLayout backHead;
    @BindView(R.id.reg_head)
    ConstraintLayout regHead;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.ll_area_number)
    ConstraintLayout llAreaNumber;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_notice)
    TextView tvNotice;

    public static final int LOCATION_CODE = 301;

    private String phone;
    //OTP_REG注册登录  OTP_FORGET_LOGIN_PD忘记密码  OTP_CHANGE_PHONE修改手机号
    private String otpType = Constants.OTP_REG;

    private String callingCode;

    private String checkPhoneType;

    private BottomDialog bottomDialog;
    private String mChangePhoneOrderNo;

    private String needCheckIdNumber = "Y";

    @Override
    protected int getLayoutID() {
        return R.layout.activity_register_phone;
    }

    public static void startActivityOfNewTaskType(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        callingCode = UserInfoManager.getInstance().getCallingCode();
        ivCallingCode.setImageResource(R.drawable.icon_ksa);
        tvCallingCode.setText("+" + callingCode);
        etPhone.getTlEdit().setHint(getString(R.string.wtw_12));
        userInfoEntity = getUserInfoEntity();
        UxcamHelper.getInstance().startUxcam();
        if (getIntent() != null && getIntent().getExtras() != null) {
            otpType = getIntent().getExtras().getString(Constants.DATA_OTP_TYPE);
            mChangePhoneOrderNo = getIntent().getExtras().getString(Constants.mChangePhoneOrderNo);
        }
        if (Constants.OTP_REG.equals(otpType)) {
            tvContentTitle.setText(R.string.sprint19_13);
            backHead.setVisibility(View.GONE);
            regHead.setVisibility(View.VISIBLE);
        } else if (Constants.OTP_FORGET_LOGIN_PD.equals(otpType)) {
            tvContentTitle.setText(R.string.sprint19_31);
            tvTitle.setText(R.string.sprint19_31);
            backHead.setVisibility(View.VISIBLE);
            regHead.setVisibility(View.GONE);
        }else if(Constants.OTP_CHANGE_PHONE.equals(otpType)){
            //修改手机号码
            tvTitle.setText(R.string.ChangeMobile_1);
            tvContentTitle.setText(R.string.ChangeMobile_1);
            tvContent.setText(R.string.sprint20_14);
            etPhone.getTlEdit().setHint(getString(R.string.ChangeMobile_2));
            backHead.setVisibility(View.VISIBLE);
            regHead.setVisibility(View.GONE);
        }

        getConfig();

        tvNotice.setText(Spans.builder()
                .text(mContext.getString(R.string.MobileValidation_SU_0002_2_D_6)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.login_botton_notice_text_color))).size(12)
                .text(" " + mContext.getString(R.string.IMR_sendMoney_5)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.login_forgot_pwd_color))).size(12).click(tvNotice, new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        HashMap<String,Object> hashMap = new HashMap<>();
                        hashMap.put(Constants.terms_condition_type,"1");
                        ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, WebViewActivity.class,hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setColor(mContext.getColor(R.color.color_fc4f00));
                        ds.setUnderlineText(false);
                    }
                }).build());

        etPhone.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                phone = etPhone.getText().toString().trim();
                if (!hasFocus) {
                    if (TextUtils.isEmpty(phone)) {
                        etPhone.showErrorViewWithMsg(getString(R.string.MobileValidation_SU_0002_2_D_2));
                    } else if (!RegexUtil.isPhone(phone)) {
                        etPhone.showErrorViewWithMsg(getString(R.string.MobileValidation_SU_0002_2_D_4));
                    }
                }
            }
        });

        etPhone.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                phone = s.toString();
                checkButtonStatus();
            }
        });

        checkPhoneType = "01";

        if (Constants.OTP_REG.equals(otpType)) {
//            getLocation();
           getPermission();
        }
        LocaleUtils.viewRotationY(this, ivRegBack, llAreaNumber, tvCallingCode, etPhone, headCircle,ivBack);

        getNotifyPermission();
    }

    private void refreshItemView(String value, TextView content, TextView title, boolean isInit) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        if (content.getVisibility() == View.GONE) {
            ViewAnim.startViewUp(mContext, title, new ViewAnim.AnimStateListener() {
                @Override
                public void onAnimEnd() {
                    content.setText(value);
                    content.setVisibility(View.VISIBLE);
                    if (!isInit) {
                        checkButtonStatus();
                    }
                }
            });
        } else {
            content.setText(value);
            checkButtonStatus();

        }
    }

    private void checkButtonStatus() {
        tvContinue.setEnabled(false);
        String phone = etPhone.getText().toString().trim();
        if (!TextUtils.isEmpty(phone) && RegexUtil.isPhone(phone)) {
                tvContinue.setEnabled(true);
        }
    }

    @OnClick({R.id.tv_continue,R.id.iv_reg_back,R.id.iv_back_head_logo,R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back_head_logo:
                ActivitySkipUtil.startAnotherActivity(this, SetBranchActivity.class, RIGHT_IN);
//                ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, LoginActivity.class,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_back:
            case R.id.iv_reg_back:
                finish();
                break;
            case R.id.tv_continue:
                if (AndroidUtils.checkVPN(mContext)) {
                    showVpnInterceptDialog();
                    return;
                }
                if (!getPermission()) {
                    return;
                }
                checkPhone();
                break;
            default:
                break;
        }
    }

    private void showVpnInterceptDialog() {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.vpn_intercept_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        LinearLayout llContent = view.findViewById(R.id.ll_content);

        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    private void checkPhone() {
        phone = etPhone.getText().toString().trim();
        if (Constants.OTP_CHANGE_PHONE.equals(otpType)) {
            needCheckIdNumber = "N";
            if(userInfoEntity != null) {
                mPresenter.checkChangePhone(callingCode, phone, ProjectApp.getLongitude(), ProjectApp.getLatitude(), "01", userInfoEntity.idNumber, userInfoEntity.birthday, needCheckIdNumber);
            }
        }else if(Constants.OTP_FORGET_LOGIN_PD.equals(otpType)){
            mPresenter.checkForgetPd(callingCode, phone,  "F");
        }else{
            mPresenter.checkPhoneReg(callingCode, phone, ProjectApp.getLongitude(), ProjectApp.getLatitude());
        }
    }

    @Override
    public RegisterPhoneCheckContract.Presenter getPresenter() {
        return new RegisterPhoneCheckPresenter();
    }

    @Override
    public void checkPhoneRegSuccess(CheckPhoneEntity checkPhoneEntity) {
        if(checkPhoneEntity != null){
            AppClient.getInstance().setLastUserID(checkPhoneEntity.userId);
            checkPhoneEntity.phone = phone;
            checkPhoneEntity.checkPhoneType = checkPhoneType;
            checkPhoneEntity.changePhoneOrderNo = mChangePhoneOrderNo;
            if(getUserInfoEntity() != null){
                checkPhoneEntity.birthday = getUserInfoEntity().birthday;
            }
            if(Constants.OTP_FORGET_LOGIN_PD.equals(otpType)){
                checkPhoneEntity.operateType = 2;
                UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                //忘记密码流程
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);
                ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, RegisterIdActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else if(Constants.OTP_CHANGE_PHONE.equals(otpType)){
                checkPhoneEntity.operateType = 3;
                if(userInfoEntity != null) {
                    checkPhoneEntity.checkIdNumber = userInfoEntity.idNumber;
                }
                UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                //忘记密码流程
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);
                mHashMaps.put(Constants.DATA_FIRST_TITLE, getString(R.string.ChangeMobile_1));
                ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else{
                checkPhoneEntity.operateType = 1;
                UserInfoManager.getInstance().setCheckPhoneEntity(checkPhoneEntity);
                if(checkPhoneEntity.isUserExist()){
                    //如果该手机号之前注册过
                    if(checkPhoneEntity.isNewDevice()){
                        //新设备登录  需要校验详细信息
                        HashMap<String, Object> mHashMaps = new HashMap<>(16);
                        mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_LOGIN_NEW_DEVICE);
                        ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }else{
                        //旧设备
                        HashMap<String, Object> mHashMaps = new HashMap<>(16);
                        mHashMaps.put(Constants.DATA_OTP_TYPE, Constants.OTP_LOGIN_OLD_DEVICE);
                        ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                } else{
                    //未注册继续走注册流程
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);
                    ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, RegisterIdActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        }
    }

    @Override
    public void checkPhoneError(String errorCode, String errorMsg) {
        if("080121".equals(errorCode)){
            showNafathErrorDialog(errorCode);
        }else if("080122".equals(errorCode)){
            //id无效
            etPhone.showErrorViewWithMsg(errorMsg);
        }else if("080123".equals(errorCode)){
            //电话无效
            etPhone.showErrorViewWithMsg(errorMsg);
        }else if("030208".equals(errorCode) || "030209".equals(errorCode) || "030210".equals(errorCode)){
            ErrorBottomDialog errorBottomDialog = new ErrorBottomDialog(mContext);
            errorBottomDialog.setErrorMsg(mContext.getString(R.string.sprint18_2),errorMsg,R.drawable.l_error_mobile_number);
            errorBottomDialog.showWithBottomAnim();
        }else {
            if (Constants.OTP_REG.equals(otpType) || "009995".equals(errorCode) || Constants.OTP_CHANGE_PHONE.equals(otpType)) {
                etPhone.showErrorViewWithMsg(errorMsg);
            } else {
                if ("009995".equals(errorCode)) {
                    etPhone.showErrorViewWithMsg(errorMsg);
                } else {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    mHashMaps.put(Constants.DATA_PHONE_NUMBER, phone);
                    mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);
                    ActivitySkipUtil.startAnotherActivity(RegisterActivity.this, RegisterIdActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
        GPSUtils.getInstance(mContext).removeListener();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_NAFATH_FAIL == event.getEventType()) {
            String failType = event.getEventMsg();
            showNafathErrorDialog(failType);
        }
    }
}
