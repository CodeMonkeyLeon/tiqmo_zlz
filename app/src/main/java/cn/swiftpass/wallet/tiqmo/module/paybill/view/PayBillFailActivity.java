package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;

public class PayBillFailActivity extends BaseCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_second_title)
    TextView tvSecondTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_result_image)
    ImageView ivResultImage;
    @BindView(R.id.tv_result_content)
    TextView tvResultContent;
    @BindView(R.id.tv_result_desc)
    TextView tvResultDesc;
    @BindView(R.id.tv_result_desc_two)
    TextView tvResultDescTwo;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_do_later)
    TextView tvDoLater;
    @BindView(R.id.tv_need_help)
    TextView tvNeedHelp;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_fail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_8);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
    }

    @OnClick({R.id.iv_back, R.id.tv_confirm, R.id.tv_need_help})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                break;
            case R.id.tv_need_help:
                break;
            default:
                break;
        }
    }
}
