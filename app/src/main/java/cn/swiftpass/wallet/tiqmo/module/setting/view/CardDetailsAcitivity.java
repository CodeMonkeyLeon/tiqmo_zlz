package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.TransferHistoryAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.FilterHistoryDialogFragment;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.CardDetailsContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.CardDetailsPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.eventbus.CardEvent;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CircleImageView;
import cn.swiftpass.wallet.tiqmo.widget.adapter.ItemTouchHelperListener;
import cn.swiftpass.wallet.tiqmo.widget.adapter.SimpleItemTouchHelperCallback;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

/**
 * Created by aijingya on 2020/7/13.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.view
 * @Description: 卡详情页面
 * @date 2020/7/13.14:21.
 */
public class CardDetailsAcitivity extends BaseCompatActivity<CardDetailsContract.Presenter> implements CardDetailsContract.View, ItemTouchHelperListener {

    public static final int filter_request = 1001;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_card)
    ImageView ivCard;
    @BindView(R.id.tv_card_num)
    TextView tvCardNum;
    @BindView(R.id.tv_card_holder_name)
    TextView tvCardHolderName;
    @BindView(R.id.tv_valid_thru)
    TextView tvValidThru;
    @BindView(R.id.tv_filter_transaction)
    TextView tvFilterTransaction;
    @BindView(R.id.ry_card_transaction_history)
    RecyclerView ryTransactionHistory;
    //    @BindView(R.id.sw_card_detail)
//    SwipeRefreshLayout swCardDetail;
    @BindView(R.id.fl_card_face_front)
    FrameLayout flCardFaceFront;
    @BindView(R.id.iv_card_setting)
    ImageView ivCardSetting;
    @BindView(R.id.ll_set_default_card)
    LinearLayout llSetDefaultCard;
    @BindView(R.id.ll_remove_card)
    LinearLayout llRemoveCard;
    @BindView(R.id.ll_anim)
    LinearLayout llAnim;
    @BindView(R.id.ll_card)
    LinearLayout llCard;
    @BindView(R.id.ll_view)
    LinearLayout llView;
    @BindView(R.id.ll_round)
    LinearLayout llRound;
    @BindView(R.id.rl_card)
    RelativeLayout rlCard;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.fl_card_face_back)
    FrameLayout flCardFaceBack;
    @BindView(R.id.iv_front_card_image)
    ImageView ivFrontCardImage;
    @BindView(R.id.iv_back_card_image)
    ImageView ivBackCardImage;
    @BindView(R.id.tv_bind_time)
    TextView tvBindTime;
    @BindView(R.id.iv_default_card_image)
    ImageView ivDefaultCardImage;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_set_default_card_text)
    TextView tvSetDefaultCardText;
    @BindView(R.id.civ_bg)
    CircleImageView civBg;
    @BindView(R.id.cv_card_view)
    CardView cvCardView;
    @BindView(R.id.sw_history)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.sc_filter)
    HorizontalScrollView scFilter;
    @BindView(R.id.iv_visa)
    ImageView iv_visa;
    @BindView(R.id.iv_mastercard)
    ImageView iv_mastercard;

    public CardBind3DSEntity cardBind3DSEntity;
    ;
    private long mAnimationStartDuration = 500L;
    private long mAnimationEndDuration = 500L;

    private CardEntity cardEntity;
    private boolean isOnlyCard;

    private BottomDialog bottomDialog;

    private Date mSelectedDate = new Date();
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedDate = Calendar.getInstance();
    Calendar startDate = Calendar.getInstance();
    Calendar endDate = Calendar.getInstance();
//    private SimpleDateFormat mShowDateFormat;

    private FilterListEntity filterListEntity;

//    private String expire_data;

    //本地写死的数据
    HashMap<String, Object> mHashMaps = new HashMap<>();
    HashMap<String, Object> mHashMaps_back = new HashMap<>();
    HashMap<String, Object> mHashMaps_mini = new HashMap<>();
    HashMap<String, Object> mHashMaps_card_bar = new HashMap<>();

    boolean isShowBack = false;
    boolean isShowFront = true;

    private String protocolCode;

    private TransferHistoryAdapter transferHistoryAdapter;
    public List<TransferHistoryEntity> historyEntityList = new ArrayList<>();
    public List<TransferHistoryListEntity> historyListEntityList = new ArrayList<>();

    private int pageSize = 30;
    private int currentPage = 1;
    private FilterEntity filterEntity;
    public List<String> typeList = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();

    private String direction, orderNo;
    private HashMap<Integer, String> selectedArray = new HashMap<>();
    private String startDateTime = "", endDateTime = "";
    private StatusView historyStatusView;

    private int historyPosition;
    private SimpleItemTouchHelperCallback itemTouchHelperCallback;
    ItemTouchHelper historyTouchHelper;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_card_details;
    }

    private void initFilter(List<FilterCodeEntity> filterList) {
        if (filterList != null && filterList.size() > 0) {
            scFilter.setVisibility(View.VISIBLE);
            llFilter.removeAllViews();
            int size = filterList.size();
            for (int i = 0; i < size; i++) {
                final FilterCodeEntity filterCodeEntity = filterList.get(i);
                if (filterCodeEntity != null) {
                    String title = filterCodeEntity.title;
                    final View view = LayoutInflater.from(mContext).inflate(R.layout.item_top_filter, null);
                    TextView tvTopFilter = view.findViewById(R.id.tv_top_filter);
                    ImageView ivDelete = view.findViewById(R.id.iv_filter_delete);
                    if (!TextUtils.isEmpty(title)) {
                        tvTopFilter.setText(title);
                    }
                    ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int type = filterCodeEntity.type;
                            switch (type) {
                                case 1:
                                    tradeType.remove(filterCodeEntity.value);
                                    categoryList.remove(filterCodeEntity.title);
                                    // 键和值
                                    Integer key = null;
                                    String value = null;
                                    // 获取键值对的迭代器
                                    Iterator it = selectedArray.entrySet().iterator();
                                    while (it.hasNext()) {
                                        HashMap.Entry entry = (HashMap.Entry) it.next();
                                        key = (Integer) entry.getKey();
                                        value = (String) entry.getValue();
                                        if (filterCodeEntity.value.equals(value)) {
                                            it.remove();
                                        }
                                        System.out.println("key:" + key + "---" + "value:" + value);
                                    }
                                    break;
                                case 2:
                                    typeList.remove(filterCodeEntity.value);
                                    break;
                                case 3:
                                    startDateTime = "";
                                    endDateTime = "";
                                    filterEntity.showStartDate = "";
                                    filterEntity.showEndDate = "";
                                    filterEntity.startDate = "";
                                    filterEntity.endDate = "";
                                    break;
                                default:
                                    break;
                            }
                            llFilter.removeView(view);

                            currentPage = 1;
                            orderNo = "";
                            direction = "";
                            showProgress(true);
                            mPresenter.getHistoryList(protocolCode, "", typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
                        }
                    });
                    llFilter.addView(view);
                }
            }
        } else {
            scFilter.setVisibility(View.GONE);
        }
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (getIntent() != null && getIntent().getExtras() != null) {
            cardEntity = (CardEntity) getIntent().getExtras().getSerializable(Constants.CARDDETAILS);
            isOnlyCard = getIntent().getExtras().getBoolean(Constants.ONLYCARD);
        }

        filterEntity = new FilterEntity();

        mHashMaps.put("424242", R.drawable.card_front_new);
        mHashMaps.put("454347", R.drawable.card_front_new);
        mHashMaps.put("543603", R.drawable.card_front_new);
        mHashMaps.put("222300", R.drawable.card_front_new);
        mHashMaps.put("519999", R.drawable.card_front_new);
        mHashMaps.put("500018", R.drawable.card_front_new);
        mHashMaps.put("465858", R.drawable.card_front_new);

        mHashMaps_back.put("424242", R.drawable.card_back_new);
        mHashMaps_back.put("454347", R.drawable.card_back_new);
        mHashMaps_back.put("543603", R.drawable.card_back_new);
        mHashMaps_back.put("222300", R.drawable.card_back_new);
        mHashMaps_back.put("519999", R.drawable.card_back_new);
        mHashMaps_back.put("500018", R.drawable.card_back_new);
        mHashMaps_back.put("465858", R.drawable.card_back_new);

        mHashMaps_mini.put("424242", R.drawable.card_middle_new);
        mHashMaps_mini.put("454347", R.drawable.card_middle_new);
        mHashMaps_mini.put("543603", R.drawable.card_middle_new);
        mHashMaps_mini.put("222300", R.drawable.card_middle_new);
        mHashMaps_mini.put("519999", R.drawable.card_middle_new);
        mHashMaps_mini.put("500018", R.drawable.card_middle_new);
        mHashMaps_mini.put("465858", R.drawable.card_middle_new);

        mHashMaps_card_bar.put("424242", R.drawable.card_small_new);
        mHashMaps_card_bar.put("454347", R.drawable.card_small_new);
        mHashMaps_card_bar.put("543603", R.drawable.card_small_new);
        mHashMaps_card_bar.put("222300", R.drawable.card_small_new);
        mHashMaps_card_bar.put("519999", R.drawable.card_small_new);
        mHashMaps_card_bar.put("500018", R.drawable.card_small_new);
        mHashMaps_card_bar.put("465858", R.drawable.card_small_new);

        flCardFaceFront.setVisibility(View.VISIBLE);
        flCardFaceBack.setVisibility(View.VISIBLE);
        llRemoveCard.setEnabled(false);

        if (LocaleUtils.isRTL(mContext)) {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            scFilter.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
        LocaleUtils.viewRotationY(mContext, headCircle, scFilter, llFilter, ivBack);

        if (cardEntity != null) {
            tvCardNum.setText(AndroidUtils.convertBankCardNoWithBlank(cardEntity.getCardNo()));
            AndroidUtils.setCardLogoVisible(cardEntity.getCardNo(), iv_visa, iv_mastercard);
            protocolCode = cardEntity.getProtocolNo();
            tvCardHolderName.setText(cardEntity.getCustName());

            //如果是默认卡，则按钮不可点击，图片字体颜色加上透明
            if (cardEntity.getDefaultCardInd().equalsIgnoreCase("1")) {
                ivDefaultCardImage.getBackground().setAlpha(77);
                tvSetDefaultCardText.setTextColor(getColor(R.color.white_transparent));

                llSetDefaultCard.setEnabled(false);

            } else if (cardEntity.getDefaultCardInd().equalsIgnoreCase("2")) {//不是默认卡
                ivDefaultCardImage.getBackground().setAlpha(255);
                tvSetDefaultCardText.setTextColor(getColor(R.color.white));

                llSetDefaultCard.setEnabled(true);
            }

            tvBindTime.setText(mContext.getString(R.string.card_back_3) + " " + cardEntity.getBindTime());

            //本地有图先取本地的图
            int resId = (int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6));
            if (resId != 0 && resId != -1) {
                ivFrontCardImage.setImageResource((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)));
            } else {
                if (cardEntity.getCardFace() != null && !TextUtils.isEmpty(cardEntity.getCardFace())) {
                    String card_url = (String) mHashMaps.get(cardEntity.getCardFace());
                    Glide.with(this)
                            .load(card_url)
                            .placeholder((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)))
                            .error((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)))
                            .into(ivFrontCardImage);
                } else {
                    ivFrontCardImage.setImageResource((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)));
                }
            }

            //本地有图先取本地的图
            int resIdBar = (int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6));
            if (resIdBar != 0 && resIdBar != -1) {
                ivCard.setImageResource((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)));
            } else {
                if (cardEntity.getCardFacebar() != null && !TextUtils.isEmpty(cardEntity.getCardFacebar())) {
                    String card_url = (String) mHashMaps_card_bar.get(cardEntity.getCardFacebar());
                    Glide.with(CardDetailsAcitivity.this)
                            .load(card_url)
                            .placeholder((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)))
                            .error((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)))
                            .into(ivCard);
                } else {
                    ivCard.setImageResource((int) mHashMaps_card_bar.get(cardEntity.getCardNo().substring(0, 6)));
                }
            }

            if (cardEntity.getCardFaceBackColor() != null && !TextUtils.isEmpty(cardEntity.getCardFaceBackColor())) {
                GradientDrawable drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
                drawable.setCornerRadius(40);
                drawable.setColor(Color.parseColor(cardEntity.getCardFaceBackColor()));

                ivBackCardImage.setBackground(drawable);

                //因为颜色一致，手动加个透明度
                String color = cardEntity.getCardFaceBackColor();
                String alpha_color = color.replace("#","#4c");
                cvCardView.setCardBackgroundColor(Color.parseColor(alpha_color));

                civBg.setColorFilter(Color.parseColor(cardEntity.getCardFaceBackColor()));
            } else {

                GradientDrawable drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
                drawable.setCornerRadius(40);
                drawable.setColor(Color.parseColor("#0B31B9"));

                ivBackCardImage.setBackground(drawable);

                civBg.setFillColor(Color.parseColor("#0B31B9"));
            }

            tvValidThru.setText(AndroidUtils.converExpireDate(cardEntity.getExpire()));
        }

        swipeRefreshLayout.setColorSchemeResources(R.color.color_B00931);

        View noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        historyStatusView = new StatusView.Builder(mContext, ryTransactionHistory).setNoContentMsg(getString(R.string.History_filter_13))
                .setNoContentView(noHistoryView).build();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        ryTransactionHistory.setLayoutManager(mLayoutManager);
        transferHistoryAdapter = new TransferHistoryAdapter(historyEntityList, CardDetailsAcitivity.this);
        itemTouchHelperCallback = new SimpleItemTouchHelperCallback(this, 1);
        historyTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        historyTouchHelper.attachToRecyclerView(ryTransactionHistory);
        transferHistoryAdapter.bindToRecyclerView(ryTransactionHistory);

        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                orderNo = "";
                direction = "";
                mPresenter.getHistoryList(protocolCode, "", typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
            }
        });

        transferHistoryAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                currentPage += 1;
                direction = "down";
                mPresenter.getHistoryList(protocolCode, "", typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
            }
        }, ryTransactionHistory);

        transferHistoryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
                if (transferHistoryEntity != null) {
                    mPresenter.getHistoryDetail(transferHistoryEntity.orderNo, transferHistoryEntity.orderType,"");
                }
            }
        });

        mPresenter.getHistoryList(protocolCode, "", typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);

        //监听appBarLayout的偏移
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange() - 15) {
                    ivCard.setVisibility(View.VISIBLE);
                    ivFrontCardImage.setVisibility(View.GONE);

                } else {
                    ivCard.setVisibility(View.GONE);
                    ivFrontCardImage.setVisibility(View.VISIBLE);
                }
            }
        });
    }

//    private void startEditAnimation() {
//        ValueAnimator valueAnimator = createDropAnimator(llCard, AndroidUtils.dip2px(mContext, 400), AndroidUtils.dip2px(mContext, 200));
//        valueAnimator.setDuration(500);
//        valueAnimator.start();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        tvFilterTransaction.setEnabled(true);
    }

//    private ValueAnimator createDropAnimator(final View v, int start, int end) {
//        ValueAnimator animator = ValueAnimator.ofInt(start, end);
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//
//            @Override
//            public void onAnimationUpdate(ValueAnimator arg0) {
//                int value = (int) arg0.getAnimatedValue();
//                ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
//                layoutParams.height = value;
//                v.setLayoutParams(layoutParams);
//            }
//        });
//        return animator;
//    }

    @Override
    public void getBindCardTransactionListSuccess(List<CardEntity> result) {

    }

    @Override
    public void getBindCardTransactioListFailed(String errorCode, String errorMsg) {

    }

    @Override
    public void unbindCardSuccess(unBindCardEntity result) {
        eventBus.post(new CardEvent(CardEvent.remove_card_success));
        //解绑卡片成功直接跳转到卡列表页面
        ProjectApp.removeAllTaskExcludeMainStack();
        ActivitySkipUtil.startAnotherActivity(CardDetailsAcitivity.this, CardManageActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);

    }

    @Override
    public void unbindCardFailed(String errorCode, String errorMsg) {
        //解绑失败直接提示
        showTipDialog(errorMsg);
    }

    @Override
    public void setDefaultCardSuccess(Void result) {
        //设置默认卡片成功直接跳转到卡列表页面
        ProjectApp.removeAllTaskExcludeMainStack();
        ActivitySkipUtil.startAnotherActivity(CardDetailsAcitivity.this, CardManageActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
    }

    @Override
    public void setDefaultCardFailed(String errorCode, String errorMsg) {
        //设置默认卡失败直接提示
        showTipDialog(errorMsg);
    }

    @Override
    public void getHistoryListSuccess(TransferHistoryMainEntity transferHistoryMainEntity) {
        showProgress(false);
        List<TransferHistoryEntity> tempHistoryList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(false);
        if (transferHistoryMainEntity != null) {
            List<TransferHistoryListEntity> historyListEntityList = transferHistoryMainEntity.transactionHistorys;
            int size = historyListEntityList.size();
            for (int i = 0; i < size; i++) {
                TransferHistoryListEntity historyListEntity = historyListEntityList.get(i);
                if (historyListEntity != null) {
                    tempHistoryList.addAll(historyListEntity.transactionListInfos);
                }
            }
        }

        if (currentPage == 1) {
            ryTransactionHistory.scrollToPosition(0);
            historyEntityList.clear();
            if (tempHistoryList.size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(historyStatusView, StatusView.CONTENT_VIEW);
                transferHistoryAdapter.setDataList(tempHistoryList);
            }
        } else {
            if (tempHistoryList.size() == 0) {
                transferHistoryAdapter.loadMoreEnd(false);
            } else {
                transferHistoryAdapter.addData(tempHistoryList);
                if (tempHistoryList.size() < pageSize) {
                    transferHistoryAdapter.loadMoreEnd(false);
                } else {
                    transferHistoryAdapter.loadMoreComplete();
                }
            }
        }
        historyEntityList.addAll(tempHistoryList);
        if (historyEntityList.size() > 0) {
            TransferHistoryEntity lastEntity = historyEntityList.get(historyEntityList.size() - 1);
            if (lastEntity != null) {
                orderNo = lastEntity.orderNo;
            }
        }
    }

    @Override
    public void getHistoryListFail(String errorCode, String errorMsg) {
        showProgress(false);
        swipeRefreshLayout.setRefreshing(false);
        showTipDialog(errorMsg);
        if (currentPage == 1) {
            showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
        }
    }

    @Override
    public void getHistoryDetailSuccess(TransferHistoryDetailEntity transferHistoryDetailEntity) {
        AndroidUtils.jumpToTransferDetail(CardDetailsAcitivity.this, transferHistoryDetailEntity, "", true);
    }

    @Override
    public void getHistoryDetailFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public CardDetailsContract.Presenter getPresenter() {
        return new CardDetailsPresenter();
    }

    private void showFilterDialog(FilterListEntity filterListEntity) {
        FilterHistoryDialogFragment fragment = FilterHistoryDialogFragment.newInstance(filterEntity, filterListEntity);
        fragment.show(getSupportFragmentManager(), "dialog");
    }

    @OnClick({R.id.iv_back, R.id.iv_card_setting, R.id.ll_set_default_card, R.id.ll_remove_card, R.id.tv_filter_transaction})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_filter_transaction:
                if (filterListEntity != null) {
                    showFilterDialog(filterListEntity);
                } else {
                    AppClient.getInstance().getFilterList(new ResultCallback<FilterListEntity>() {
                        @Override
                        public void onResult(FilterListEntity mFilterListEntity) {
                            filterListEntity = mFilterListEntity;
                            showFilterDialog(filterListEntity);
                        }

                        @Override
                        public void onFailure(String errorCode, String errorMsg) {

                        }
                    });
                }
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_card_setting:
                if (isShowFront && !isShowBack) {
                    startAnimation();
                    llRemoveCard.setEnabled(true);
                } else if (!isShowFront && isShowBack) {
                    endAnimation();
                    llRemoveCard.setEnabled(false);
                }

                break;
            case R.id.ll_set_default_card:
                setDefaultCardDialog(mContext.getString(R.string.card_back_1),
                        ThemeSourceUtils.getSourceID(this, R.attr.icon_set_as_default_card_popup),
                        AndroidUtils.convertBankCardNoWithBlank(cardEntity.getCardNo()),
                        mContext.getString(R.string.confirm_default_1),
                        mContext.getString(R.string.confirm_default_2),
                        true);
                break;
            case R.id.ll_remove_card:
                setDefaultCardDialog(mContext.getString(R.string.card_back_2),
                        ThemeSourceUtils.getSourceID(mContext, R.attr.icon_remove_card_popup),
                        AndroidUtils.convertBankCardNoWithBlank(cardEntity.getCardNo()),
                        isOnlyCard ? mContext.getString(R.string.remove_card_2) : mContext.getString(R.string.remove_card_1),
                        mContext.getString(R.string.remove_card_3),
                        false);
                break;
            default:
                break;
        }
    }

    public void setDefaultCardDialog(String title, int resId, String cardNo, String dialogMsg, String buttonText, boolean isSetDefaultCard) {
        bottomDialog = new BottomDialog(mContext);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_card_dialog, null);
        TextView tv_dialog_title = contentView.findViewById(R.id.tv_dialog_title);
        ImageView iv_dialog_image = contentView.findViewById(R.id.iv_dialog_image);
        TextView tv_card_no = contentView.findViewById(R.id.tv_card_no);
        TextView tv_dialog_message = contentView.findViewById(R.id.tv_dialog_message);
        TextView tv_dialog_button = contentView.findViewById(R.id.tv_dialog_button);
        FrameLayout fl_card_small_face = contentView.findViewById(R.id.fl_card_small_face);
        ImageView iv_mini_card = contentView.findViewById(R.id.iv_mini_card);
        ImageView iv_mini_visa = contentView.findViewById(R.id.iv_mini_visa);
        ImageView iv_mini_mastercard = contentView.findViewById(R.id.iv_mini_mastercard);

        String CardNo = cardEntity.getCardNo();
        String regex = "(.{4})";
        String CardNo_with_black = CardNo.replaceAll(regex, "$1  ");
        tv_card_no.setText(CardNo_with_black);

        tv_dialog_title.setText(title);
        iv_dialog_image.setImageResource(resId);
        tv_card_no.setText(cardNo);
        tv_dialog_message.setText(dialogMsg);
        tv_dialog_button.setText(buttonText);

        int resId_mini = (int) mHashMaps_mini.get(cardEntity.getCardNo().substring(0, 6));
        AndroidUtils.setCardLogoVisible(cardEntity.getCardNo(), iv_mini_visa, iv_mini_mastercard);
        if (resId_mini != 0 && resId_mini != -1) {
            iv_mini_card.setImageResource((int) mHashMaps_mini.get(cardEntity.getCardNo().substring(0, 6)));
        } else {
            if (cardEntity.getCardFaceMini() != null && !TextUtils.isEmpty(cardEntity.getCardFaceMini())) {
                String card_url = (String) mHashMaps_mini.get(cardEntity.getCardFaceMini());
                Glide.with(this)
                        .load(card_url)
                        .placeholder((int) mHashMaps_mini.get(cardEntity.getCardNo().substring(0, 6)))
                        .error((int) mHashMaps_mini.get(cardEntity.getCardNo().substring(0, 6)))
                        .into(iv_mini_card);
            } else {
                iv_mini_card.setImageResource((int) mHashMaps_mini.get(cardEntity.getCardNo().substring(0, 6)));
            }
        }

        tv_dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSetDefaultCard) {
                    if (bottomDialog != null && bottomDialog.isShowing()) {
                        bottomDialog.dismiss();
                    }
                    //设置默认卡
                    mPresenter.setDefaultCard(cardEntity.getProtocolNo());
                } else {
                    if (bottomDialog != null && bottomDialog.isShowing()) {
                        bottomDialog.dismiss();
                    }
                    //删除卡片
                    mPresenter.unBindCard(cardEntity.getProtocolNo());
                }

            }
        });


        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                bottomDialog.dismiss();
                bottomDialog = null;
            }
        });

        bottomDialog.setContentView(contentView);

        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    private void startAnimation() {
        flCardFaceBack.post(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int x = (int) (flCardFaceBack.getX() + flCardFaceBack.getWidth() - ivCardSetting.getWidth() / 2);
                    int y = (int) (flCardFaceBack.getY() + flCardFaceBack.getHeight() - ivCardSetting.getHeight() / 2);
                    int start_radius = ivCardSetting.getWidth() / 2;
                    int end_radius = flCardFaceBack.getWidth();
                    Animator animator = ViewAnimationUtils.createCircularReveal(flCardFaceBack, x, y, start_radius, end_radius);
                    animator.setInterpolator(new AccelerateInterpolator());//out到in
                    animator.setDuration(mAnimationStartDuration);

                    animator.addListener(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);

                            isShowFront = false;
                            isShowBack = true;

                            flCardFaceFront.setVisibility(View.GONE);
                            flCardFaceBack.setVisibility(View.VISIBLE);

                            civBg.setVisibility(View.INVISIBLE);
                        }
                    });

                    flCardFaceFront.setVisibility(View.GONE);
                    flCardFaceBack.setVisibility(View.VISIBLE);

                    animator.start();

                }
            }
        });
    }

    private void endAnimation() {
        flCardFaceFront.post(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int x = (int) flCardFaceFront.getX()-10;
                    int y = (int) flCardFaceFront.getY()-10;
                    int start_radius = ivCardSetting.getWidth() / 2;
                    int end_radius = flCardFaceFront.getWidth() + ivCardSetting.getHeight();
                    Animator animator = ViewAnimationUtils.createCircularReveal(
                            flCardFaceFront,// 操作的视图
                            x,
                            y,
                            start_radius,
                            end_radius);
                    animator.setInterpolator(new LinearOutSlowInInterpolator());
                    animator.setDuration(mAnimationEndDuration);
                    animator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);

                            isShowFront = true;
                            isShowBack = false;

                            flCardFaceFront.setVisibility(View.VISIBLE);
                            flCardFaceBack.setVisibility(View.GONE);

                            civBg.setVisibility(View.VISIBLE);
                        }
                    });

                    flCardFaceFront.setVisibility(View.VISIBLE);
                    flCardFaceBack.setVisibility(View.GONE);
                    animator.start();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (tvFilterTransaction != null) {
            tvFilterTransaction.setEnabled(true);
        }
        if (requestCode == filter_request) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    filterEntity = (FilterEntity) data.getSerializableExtra("filterEntity");
                    if (filterEntity != null) {
                        String showStartDate = "", showEndDate = "";
                        startDateTime = filterEntity.startDate;
                        showStartDate = filterEntity.showStartDate;
                        endDateTime = filterEntity.endDate;
                        showEndDate = filterEntity.showEndDate;
                        typeList = filterEntity.paymentType;
                        tradeType = filterEntity.tradeType;
                        categoryList = filterEntity.categoryList;
                        selectedArray = filterEntity.selectedArray;
                        List<FilterCodeEntity> filterList = new ArrayList<>();

                        if (TextUtils.isEmpty(startDateTime) && !TextUtils.isEmpty(endDateTime)) {
                            showStartDate = "...";
                        }

                        if (!TextUtils.isEmpty(startDateTime) && TextUtils.isEmpty(endDateTime)) {
                            showEndDate = getString(R.string.History_filter_14);
                        }

                        if (!TextUtils.isEmpty(startDateTime) || !TextUtils.isEmpty(endDateTime)) {
                            FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                            filterCodeEntity.type = 3;
                            filterCodeEntity.title = showStartDate + "-" + showEndDate;
                            filterList.add(filterCodeEntity);
                        }

                        int typeSize = typeList.size();
                        for (int i = 0; i < typeSize; i++) {
                            FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                            filterCodeEntity.type = 2;
                            String type = typeList.get(i);
                            filterCodeEntity.value = type;
                            if ("P".equals(type)) {
                                filterCodeEntity.title = getString(R.string.History_filter_5);
                            } else if ("I".equals(type)) {
                                filterCodeEntity.title = getString(R.string.History_filter_6);
                            }
                            filterList.add(filterCodeEntity);
                        }

                        int categorySize = categoryList.size();
                        for (int i = 0; i < categorySize; i++) {
                            FilterCodeEntity filterCodeEntity = new FilterCodeEntity();
                            filterCodeEntity.type = 1;
                            filterCodeEntity.title = categoryList.get(i);
                            filterCodeEntity.value = tradeType.get(i);
                            filterList.add(filterCodeEntity);
                        }

                        initFilter(filterList);
                        currentPage = 1;
                        orderNo = "";
                        direction = "";
                        showProgress(true);
                        mPresenter.getHistoryList(protocolCode, "", typeList, tradeType, startDateTime, endDateTime, pageSize, currentPage, direction, orderNo);
                    }
                }
            }
        }
    }

    @Override
    public void deleteTransferHistorySuccess(Void result) {
        if (transferHistoryAdapter != null) {
            //移除数据
            transferHistoryAdapter.getDataList().remove(historyPosition);
            transferHistoryAdapter.notifyDataSetChanged();
            if (transferHistoryAdapter.getDataList().size() == 0) {
                showStatusView(historyStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void deleteTransferHistoryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDissmiss(int position, int type) {
        historyPosition = position;
        TransferHistoryEntity transferHistoryEntity = transferHistoryAdapter.getDataList().get(position);
        if (transferHistoryEntity != null) {
            mPresenter.deleteTransferHistory(transferHistoryEntity.orderNo);
        }
    }
}