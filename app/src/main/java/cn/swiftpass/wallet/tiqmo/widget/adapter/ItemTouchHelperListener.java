package cn.swiftpass.wallet.tiqmo.widget.adapter;

public interface ItemTouchHelperListener {
    //数据交换
    void onItemMove(int fromPosition, int toPosition);

    //数据删除
    void onItemDissmiss(int position, int type);
}
