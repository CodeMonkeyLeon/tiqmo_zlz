package cn.swiftpass.wallet.tiqmo.module.home.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;

public class AnalysisContract {

    public interface SpendingView extends BaseView<AnalysisContract.SpendingPresenter> {
        void getAnalysisDetailSuccess(SpendingDetailEntity spendingDetailEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface SpendingPresenter extends BasePresenter<AnalysisContract.SpendingView> {
        void getAnalysisDetail(String periodType, String spendingAnalysisQueryDate);

    }

    public interface BudgetView extends BaseView<AnalysisContract.BudgetPresenter> {
        void saveUserSpendingSuccess(Void result);

        void getSpendingBudgetListSuccess(SpendingBudgetListEntity spendingBudgetListEntity);

        void showErrorMsg(String errorCode, String errorMsg);
    }

    public interface BudgetPresenter extends BasePresenter<AnalysisContract.BudgetView> {
        void getSpendingBudgetList();

        void saveUserSpending(List<SpendingBudgetEntity> userSpendingBudgetSaveOrUpdateInfoList, List<String> deleteBudgetIdList);
    }
}
