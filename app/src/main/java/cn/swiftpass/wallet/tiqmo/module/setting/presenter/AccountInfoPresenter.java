package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.AccountInfoContract;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AccountInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AccountInfoPresenter implements AccountInfoContract.Presenter {

    private AccountInfoContract.View baseView;

    @Override
    public void getAccountInfo(String type) {
        AppClient.getInstance().getUserManager().getAccountInfo(type, new LifecycleMVPResultCallback<AccountInfoEntity>(baseView, false) {
            @Override
            protected void onSuccess(AccountInfoEntity result) {
                baseView.getAccountInfoSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.getAccountInfoFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getHistoryList(String protocolNo, String accountNo, List<String> paymentType, List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum,
                               String direction, String orderNo) {
        AppClient.getInstance().getTransferMainHistory(protocolNo, accountNo, paymentType,
                tradeType, startDate, endDate, pageSize, pageNum, direction, orderNo, new LifecycleMVPResultCallback<TransferHistoryMainEntity>(baseView, true) {
                    @Override
                    protected void onSuccess(TransferHistoryMainEntity result) {
                        baseView.getHistoryListSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        baseView.getHistoryListFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void deleteTransferHistory(String orderNo) {
        AppClient.getInstance().deleteTransferHistory(orderNo, new LifecycleMVPResultCallback<Void>(baseView, true) {
            @Override
            protected void onSuccess(Void result) {
                baseView.deleteTransferHistorySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.deleteTransferHistoryFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getHistoryDetail(String orderNo, String orderType,String queryType) {
        AppClient.getInstance().getTransferHistoryDetail(orderNo, orderType,queryType, new LifecycleMVPResultCallback<TransferHistoryDetailEntity>(baseView, true) {
            @Override
            protected void onSuccess(TransferHistoryDetailEntity result) {
                baseView.getHistoryDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.getHistoryDetailFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(AccountInfoContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
