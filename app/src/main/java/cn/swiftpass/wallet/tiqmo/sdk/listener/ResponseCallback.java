package cn.swiftpass.wallet.tiqmo.sdk.listener;



/**
 * Created by YZX on 2018年12月18日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public interface ResponseCallback<T> {
    void onResponse(T response) ;

    void onFailure(Throwable t);
}
