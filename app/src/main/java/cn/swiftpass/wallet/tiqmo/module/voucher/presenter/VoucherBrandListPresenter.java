package cn.swiftpass.wallet.tiqmo.module.voucher.presenter;

import android.app.Activity;

import java.util.HashMap;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherSelectActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.OrderExistEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;

public class VoucherBrandListPresenter implements EVoucherContract.VoucherBrandListPresenter {

    public static final String Y = "Y"; // have limit country or order exits
    public static final String N = "N"; // no limit country or order un exits
    public static final String SCENE_TYPE_VOUCHER = "15";

    EVoucherContract.VoucherBrandListView mVoucherBrandListView;

    @Override
    public void getVoucherBrandList() {
        AppClient.getInstance().getTransferManager().getVoucherBrandList(new LifecycleMVPResultCallback<VoucherBrandListEntity>(mVoucherBrandListView, true) {
            @Override
            protected void onSuccess(VoucherBrandListEntity result) {
                mVoucherBrandListView.getVoucherBrandListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mVoucherBrandListView.getVoucherBrandListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type, CheckOutEntity checkOutEntity) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mVoucherBrandListView, false) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                mVoucherBrandListView.getLimitSuccess(result, checkOutEntity);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mVoucherBrandListView != null) {
                    mVoucherBrandListView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(mVoucherBrandListView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                mVoucherBrandListView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mVoucherBrandListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getRecentVoucherBrandList() {
        AppClient.getInstance().getTransferManager().getRecentVoucherBrandList(new LifecycleMVPResultCallback<VoucherBrandListEntity>(mVoucherBrandListView, true) {
            @Override
            protected void onSuccess(VoucherBrandListEntity result) {
                mVoucherBrandListView.getRecentVoucherBrandListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mVoucherBrandListView.getRecentVoucherBrandListFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void eVoucherOrder(String voucherCode, String voucherAmount) {
        AppClient.getInstance().eVoucherOrder(voucherCode, voucherAmount, new LifecycleMVPResultCallback<EVoucherOrderInfoEntity>(mVoucherBrandListView, true) {

            @Override
            protected void onSuccess(EVoucherOrderInfoEntity result) {
                mVoucherBrandListView.eVoucherOrderSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mVoucherBrandListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void checkOrderExist(VoucherBrandEntity voucherBrandEntity, String sceneType) {
        AppClient.getInstance().checkOrderExist(voucherBrandEntity.voucherServiceProviderCode, voucherBrandEntity.voucherBrandCode, sceneType,
                new LifecycleMVPResultCallback<OrderExistEntity>(mVoucherBrandListView, false) {
                    @Override
                    protected void onSuccess(OrderExistEntity result) {
                        if (result == null || mVoucherBrandListView == null) {
                            return;
                        }
                        if (Y.equals(result.voucherOrderIsProcessingFlag)) {
                            mVoucherBrandListView.showProgress(false);
                            mVoucherBrandListView.showOrderRepeatDia(voucherBrandEntity, sceneType);
                        } else if (N.equals(result.voucherOrderIsProcessingFlag)) {
                            mVoucherBrandListView.checkOrderStatusSuccess(voucherBrandEntity, sceneType);
                        }
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (mVoucherBrandListView == null) {
                            return;
                        }
                        mVoucherBrandListView.showProgress(false);
                        mVoucherBrandListView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void skipToVouTypeAndInfo(Activity activity, String providerCode, String brandCode, String brandName, String countryCode) {
        HashMap<String, String> map = new HashMap<>();
        map.put("providerCode", providerCode);
        map.put("brandCode", brandCode);
        map.put("brandName", brandName);
        map.put("countryCode", countryCode);
        ActivitySkipUtil.startAnotherActivity(activity, EVoucherSelectActivity.class, map,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void requestSupCountry(String providerCode, String brandCode, String brandName) {
        AppClient.getInstance().requestVouSupCountry(providerCode, brandCode,
                new LifecycleMVPResultCallback<VouSupCountryEntity>(mVoucherBrandListView, true) {
                    @Override
                    protected void onSuccess(VouSupCountryEntity result) {
                        if (mVoucherBrandListView == null) {
                            return;
                        }
                        mVoucherBrandListView.showEVouCountryDia(result, providerCode, brandCode, brandName);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (mVoucherBrandListView == null) {
                            return;
                        }
                        mVoucherBrandListView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void continueToSelectTickets(Activity activity, VoucherBrandEntity voucherBrandEntity, String sceneType) {
        if (SCENE_TYPE_VOUCHER.equals(sceneType)) {// EVoucher
            if (voucherBrandEntity == null) {
                return;
            }
            if (voucherBrandEntity.limitCountry.equals(Y)) {
                requestSupCountry(voucherBrandEntity.voucherServiceProviderCode,
                        voucherBrandEntity.voucherBrandCode, voucherBrandEntity.voucherBrandName);
            } else if (voucherBrandEntity.limitCountry.equals(N)) {
                mVoucherBrandListView.showProgress(false);
                skipToVouTypeAndInfo(activity, voucherBrandEntity.voucherServiceProviderCode,
                        voucherBrandEntity.voucherBrandCode, voucherBrandEntity.voucherBrandName, "");
            }
        }
    }

    @Override
    public void attachView(EVoucherContract.VoucherBrandListView voucherBrandListView) {
        this.mVoucherBrandListView = voucherBrandListView;
    }

    @Override
    public void detachView() {
        this.mVoucherBrandListView = null;
    }
}
