package cn.swiftpass.wallet.tiqmo.module.setting.animation.transformer;

import android.view.View;

import cn.swiftpass.wallet.tiqmo.module.setting.animation.AnimationTransformer;


public class DefaultTransformerAdd implements AnimationTransformer {

    @Override
    public void transformAnimation(View view, float fraction, int cardWidth, int cardHeight, int fromPosition, int toPosition) {
        fromPosition = toPosition + 1;
        int positionCount = fromPosition - toPosition;
        float scale = (0.8f - 0.1f * fromPosition) + (0.1f * fraction * positionCount);
        view.setScaleX(scale + 0.2f);
        view.setScaleY(scale + 0.2f);
        view.setTranslationY(-cardHeight * (0.8f - scale) * 0.5f - cardWidth * (0.06f *
                fromPosition - 0.06f * fraction * positionCount));
        view.setAlpha(fraction);
    }

    @Override
    public void transformInterpolatedAnimation(View view, float fraction, int cardWidth, int cardHeight, int fromPosition, int toPosition) {

    }
}
