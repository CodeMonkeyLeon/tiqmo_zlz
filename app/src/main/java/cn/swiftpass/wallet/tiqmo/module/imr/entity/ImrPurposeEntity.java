package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrPurposeEntity extends BaseEntity {

    //汇款目的代码
    public String code;
    //汇款目的词条
    public String desc;
}
