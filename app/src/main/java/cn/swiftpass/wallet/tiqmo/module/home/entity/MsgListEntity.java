package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MsgListEntity extends BaseEntity {

    public List<MsgEntity> msgList = new ArrayList<>();
    public int msgCounts;
}
