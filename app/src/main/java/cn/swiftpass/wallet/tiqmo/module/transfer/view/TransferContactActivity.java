package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.Manifest;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.SimpleTextWatcher;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.KycContactAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferContactListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferContactPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ContactUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.PermissionInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class TransferContactActivity extends BaseCompatActivity<TransferContract.TransferContactPresenter> implements TransferContract.TransferContactView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.con_bottom)
    ConstraintLayout conBottom;
    @BindView(R.id.et_phone)
    CustomizeEditText etPhone;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_your)
    TextView tvYour;
    @BindView(R.id.tv_allow)
    TextView tvAllow;
    @BindView(R.id.rl_contacts)
    RecyclerView rlContacts;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar idSideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_permission)
    LinearLayout llPermission;
    @BindView(R.id.iv_no_permission)
    ImageView ivNoPermission;
    @BindView(R.id.tv_no_permission)
    TextView tvNoPermission;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;

    boolean isSearchEmpty;

    private KycContactAdapter kycContactAdapter;
    private LinearLayoutManager mLayoutManager;

    private String phone;
    private KycContactEntity currentKycContact;

    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;
    private boolean isEditTransfer = false;

    private List<KycContactEntity> filterContactList = new ArrayList<>();

    public List<RequestContactEntity> requestContactList = new ArrayList<>();
    public List<KycContactEntity> phoneList = new ArrayList<>();

    private String sceneType;

    //联系人是否注册列表
    private List<KycContactEntity> contactInviteDtos = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_contact;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
//        etPhone.setLeftText("+" + SpUtils.getInstance().getCallingCode());
        UxcamHelper.getInstance().hideScreen(true);
        if (getIntent() != null && getIntent().getExtras() != null) {
            sceneType = getIntent().getExtras().getString(Constants.sceneType);
            if (Constants.TYPE_TRANSFER_WW.equals(sceneType)) {
                tvTitle.setText(R.string.wtw_1);
            } else if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
                tvTitle.setText(R.string.request_transfer_1);
            }
        }

        idSideBar.setTextView(idDialog);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 0.5f);
//        rlContacts.addItemDecoration(MyItemDecoration.createVertical(getColor(R.color.line_common),ryLineSpace));
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rlContacts.setLayoutManager(mLayoutManager);
        kycContactAdapter = new KycContactAdapter(phoneList);
        kycContactAdapter.bindToRecyclerView(rlContacts);

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = kycContactAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (mLayoutManager != null) {
                        mLayoutManager.scrollToPositionWithOffset(position, 0);
                    }
                }
            }
        });

        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                phone = etPhone.getText().toString().trim();
                if (!hasFocus) {
                    if (TextUtils.isEmpty(phone)) {
//                        etPhone.showErrorViewWithMsg(getString(R.string.MobileValidation_SU_0002_2_D_2));
                    } else if (phone.equals(userInfoEntity.getPhone())) {
                        etPhone.setError(getString(R.string.stt_4_0));
                    } else if (!RegexUtil.isPhone(phone)) {
                        etPhone.setError(getString(R.string.wtw_17));
                    }
                }
            }
        });

        etPhone.addTextChangedListener(new SimpleTextWatcher(){
            @Override
            public void afterTextChanged(Editable s) {
                phone = s.toString();
                etPhone.setError("");
                if (TextUtils.isEmpty(phone)) {
                    tvContinue.setEnabled(false);
                } else {
                    if (phone.equals(userInfoEntity.getPhone())) {
                        tvContinue.setEnabled(true);
                    } else if (RegexUtil.isPhone(phone)) {
                        tvContinue.setEnabled(true);
                    } else {
                        tvContinue.setEnabled(false);
                    }
                }
                if (phoneList != null && phoneList.size() > 0) {
                    searchFilterWithAllList(phone);
                } else {
                    isSearchEmpty = true;
                }
            }
        });

        kycContactAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                KycContactEntity kycContactEntity = kycContactAdapter.getDataList().get(position);
                currentKycContact = kycContactEntity;
                isEditTransfer = false;
                mPresenter.checkContact(SpUtils.getInstance().getCallingCode(), kycContactEntity.getRequestPhoneNumber(), kycContactEntity.getContactsName(), sceneType);
//                HashMap<String, Object> map = new HashMap<>(16);
//                map.put(Constants.CONTACT_ENTITY, kycContactEntity);
//                map.put(Constants.sceneType, sceneType);
//                if (Constants.TYPE_TRANSFER_WW.equals(sceneType)) {
//                    ActivitySkipUtil.startAnotherActivity(TransferContactActivity.this, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                } else if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
//                    ActivitySkipUtil.startAnotherActivity(TransferContactActivity.this, RequestTransferActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                }
            }
        });

        kycContactAdapter.setTvInviteClickListener(new KycContactAdapter.TvInviteClickListener() {
            @Override
            public void sendInvite(String phone) {
                String msg = getString(R.string.sprint21_9);
                if (Constants.TYPE_TRANSFER_RT.equals(sceneType)){
                    msg = getString(R.string.sprint21_10);
                }
                String[] s = userInfoEntity.getUserName().split(" ");
                String body = msg.replace("XXX", s[0]);
                AndroidUtils.sendSmsWithBody(mContext, BuildConfig.DefaultInternationalAreaCode+phone,body);
            }
        });
    }

    private void searchFilterWithAllList(String filterStr) {
        llPermission.setVisibility(View.GONE);
        tvYour.setBackgroundResource(R.drawable.shape_top);
        conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
        try {
            filterContactList.clear();
            //去除空格的匹配规则
            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            if (TextUtils.isEmpty(filterStr)) {
                kycContactAdapter.setIsFilter(false);
//            llEmpty.setVisibility(View.GONE);
                idSideBar.setVisibility(View.VISIBLE);
                filterContactList.clear();
                kycContactAdapter.setDataList(phoneList);
                return;
            } else {
                idSideBar.setVisibility(View.GONE);
                kycContactAdapter.setIsFilter(true);
            }
            filterContactList.clear();
            if (phoneList != null && phoneList.size() > 0) {
                int size = phoneList.size();
                for (int i = 0; i < size; i++) {
                    KycContactEntity kycContactEntity = phoneList.get(i);
                    String name = kycContactEntity.getNameNoMapping() + kycContactEntity.getPhoneNoMapping();
                    String pinyinName = Pinyin.toPinyin(name, "");
                    String phoneNumber = kycContactEntity.getPhone();
                    if(pinyinName.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterContactList.add(kycContactEntity);
                    }else if (phoneNumber.startsWith("+966 " + filterStr)) {
                        filterContactList.add(kycContactEntity);
                    }
                }

                if (filterContactList.size() == 0) {
                    isSearchEmpty = true;
                    tvNoPermission.setText(R.string.wtw_18_1);
                    ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                    llPermission.setVisibility(View.VISIBLE);
                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                    tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                    tvAllow.setVisibility(View.GONE);
                    kycContactAdapter.setDataList(filterContactList);
                } else {
                    isSearchEmpty = false;
                    llPermission.setVisibility(View.GONE);
                    tvYour.setBackgroundResource(R.drawable.shape_top);
                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                    kycContactAdapter.setDataList(filterContactList);
                }
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        checkPermission();

        if (etPhone != null) {
            String phone = etPhone.getText().toString();
            if (!TextUtils.isEmpty(phone)) {
                searchFilterWithAllList(phone);
            }
        }
    }

    private void checkPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.READ_CONTACTS)) {
            llPermission.setVisibility(View.VISIBLE);
            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
            tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
            tvAllow.setVisibility(View.VISIBLE);
            conRecycler.setVisibility(View.GONE);
            if (!ActivityCompat.shouldShowRequestPermissionRationale(TransferContactActivity.this, Manifest.permission.READ_CONTACTS)) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
            PermissionInstance.getInstance().getPermission(TransferContactActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    llPermission.setVisibility(View.GONE);
                    tvYour.setBackgroundResource(R.drawable.shape_top);
                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                    if (phoneList.size() == 0) {
                        ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                            @Override
                            public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                                phoneList.addAll(contactEntities);
                                int size = phoneList.size();
                                if (size == 0) {
                                    llPermission.setVisibility(View.VISIBLE);
                                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                                    tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                                    tvNoPermission.setText(R.string.wtw_18_1);
                                    ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                                    idSideBar.setVisibility(View.GONE);
                                    tvAllow.setVisibility(View.GONE);
                                } else {
                                    llPermission.setVisibility(View.GONE);
                                    tvYour.setBackgroundResource(R.drawable.shape_top);
                                    conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                                    idSideBar.setVisibility(View.VISIBLE);
                                    mPresenter.contactInvite(phoneList);
//                                    if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
//                                        conRecycler.setVisibility(View.VISIBLE);
//                                        kycContactAdapter.setDataList(phoneList);
//                                    } else if (Constants.TYPE_TRANSFER_WW.equals(sceneType)) {
//                                        for (int i = 0; i < size; i++) {
//                                            KycContactEntity kycContactEntity = phoneList.get(i);
//                                            RequestContactEntity requestContactEntity = new RequestContactEntity();
//                                            requestContactEntity.callingCode = SpUtils.getInstance().getCallingCode();
//                                            requestContactEntity.contactsName = kycContactEntity.getContactsName();
//                                            if (kycContactEntity.getPhone() != null && kycContactEntity.getPhone().length() > 5) {
//                                                requestContactEntity.phone = kycContactEntity.getPhone().substring(5);
//                                            }
//                                            requestContactList.add(requestContactEntity);
//                                        }
//                                        if (requestContactList.size() == size) {
//                                            mPresenter.getContactList(requestContactList);
//                                        }
//                                    }
                                }
                            }
                        });
                    }
                }

                @Override
                public void rejectPermission() {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(TransferContactActivity.this, Manifest.permission.READ_CONTACTS)) {
                        if (!firstRejectContacts) {
                            showLackOfPermissionDialog(" ", getString(R.string.common_30));
                        } else {
                            firstRejectContacts = false;
                        }
                    }
                }
            }, Manifest.permission.READ_CONTACTS);
        } else {
            llPermission.setVisibility(View.GONE);
            tvYour.setBackgroundResource(R.drawable.shape_top);
            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
            if (phoneList.size() == 0) {
                ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                    @Override
                    public void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities) {
                        phoneList.addAll(contactEntities);
                        int size = phoneList.size();
                        if (size == 0) {
                            llPermission.setVisibility(View.VISIBLE);
                            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                            tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                            tvNoPermission.setText(R.string.wtw_18_1);
                            ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                            idSideBar.setVisibility(View.GONE);
                            tvAllow.setVisibility(View.GONE);
                        } else {
                            llPermission.setVisibility(View.GONE);
                            tvYour.setBackgroundResource(R.drawable.shape_top);
                            conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                            idSideBar.setVisibility(View.VISIBLE);
                            mPresenter.contactInvite(phoneList);
//                            if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
//                                conRecycler.setVisibility(View.VISIBLE);
//                                kycContactAdapter.setDataList(phoneList);
//                            } else if (Constants.TYPE_TRANSFER_WW.equals(sceneType)) {
//                                for (int i = 0; i < size; i++) {
//                                    KycContactEntity kycContactEntity = phoneList.get(i);
//                                    RequestContactEntity requestContactEntity = new RequestContactEntity();
//                                    requestContactEntity.callingCode = SpUtils.getInstance().getCallingCode();
//                                    requestContactEntity.contactsName = kycContactEntity.getContactsName();
//                                    if (kycContactEntity.getPhone() != null && kycContactEntity.getPhone().length() > 5) {
//                                        requestContactEntity.phone = kycContactEntity.getPhone().substring(5);
//                                    }
//                                    requestContactList.add(requestContactEntity);
//                                }
//                                if (requestContactList.size() == size) {
//                                    mPresenter.getContactList(requestContactList);
//                                }
//                            }
                        }
                    }
                });
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_continue, R.id.tv_allow})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_allow:
                AndroidUtils.startAppSetting(mContext);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_continue:
                currentKycContact = null;
                String phone = etPhone.getText().toString().trim();
                isEditTransfer = true;
                mPresenter.checkContact(SpUtils.getInstance().getCallingCode(), phone, "", sceneType);
                break;
            default:
                break;
        }
    }

    @Override
    public void getContactListSuccess(TransferContactListEntity transferContactListEntity) {
        if (transferContactListEntity != null) {
            phoneList.clear();
            phoneList = transferContactListEntity.phoneList;
            int size = phoneList.size();
            if (size == 0) {
                conRecycler.setVisibility(View.GONE);
                idSideBar.setVisibility(View.GONE);
                tvNoPermission.setText(R.string.wtw_18_1);
                ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                llPermission.setVisibility(View.VISIBLE);
                conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                tvAllow.setVisibility(View.GONE);
            } else {
                conRecycler.setVisibility(View.VISIBLE);
                idSideBar.setVisibility(View.VISIBLE);
                llPermission.setVisibility(View.GONE);
                tvYour.setBackgroundResource(R.drawable.shape_top);
                conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                for (int i = 0; i < size; i++) {
                    KycContactEntity kycContactEntity = phoneList.get(i);
                    if (kycContactEntity.getContactsName() != null) {
                        kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                    }
                }
                Collections.sort(phoneList, new PinyinComparator());
                kycContactAdapter.setDataList(phoneList);
            }
        }
    }

    private static final String ATOZSTR = "[A-Z]";
    private static final String SPECIAL_CHAR_ONE = "#";
    private static final String SPECIAL_CHAR_TWO = "@";

    public class PinyinComparator implements Comparator<KycContactEntity> {

        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getSortLetter().equals(SPECIAL_CHAR_TWO) || o2.getSortLetter().equals(SPECIAL_CHAR_ONE)) {
                return -1;
            } else if (o1.getSortLetter().equals(SPECIAL_CHAR_ONE) || o2.getSortLetter().equals(SPECIAL_CHAR_TWO)) {
                return 1;
            } else {
                return o1.getSortLetter().compareTo(o2.getSortLetter());
            }
        }

    }

    @Override
    public void getContactListFail(String errCode, String errMsg) {
        llPermission.setVisibility(View.VISIBLE);
        conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
        tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
        tvNoPermission.setText(R.string.wtw_18_1);
        ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
        tvAllow.setVisibility(View.GONE);
        showTipDialog(errMsg);
    }

    @Override
    public void checkContactSuccess(KycContactEntity kycContactEntity) {
//        String flag = kycContactEntity.flag;
//        if ("2".equals(flag)) {
//            etPhone.showErrorViewWithMsg(getString(R.string.wtw_16));
//        } else if ("4".equals(flag) && !Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
//            etPhone.showErrorViewWithMsg(getString(R.string.wtw_16_1));
//        } else {
        HashMap<String, Object> map = new HashMap<>(16);
        map.put(Constants.CONTACT_ENTITY, kycContactEntity);
        map.put(Constants.sceneType, sceneType);
        if (Constants.TYPE_TRANSFER_WW.equals(sceneType)) {
            map.put(Constants.CONTACT_ENTITY, kycContactEntity);
            ActivitySkipUtil.startAnotherActivity(TransferContactActivity.this, TransferMoneyActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
            if (currentKycContact != null) {
                kycContactEntity.setContactsName(currentKycContact.getContactsName());
            }

            if (isSearchEmpty) {
            } else {
                if (filterContactList.size() > 0) {
                    kycContactEntity.setContactsName(filterContactList.get(0).getContactsName());
                }
            }
            if (isEditTransfer) {
                kycContactEntity.setContactsName("");
            }
            map.put(Constants.CONTACT_ENTITY, kycContactEntity);
            ActivitySkipUtil.startAnotherActivity(TransferContactActivity.this, RequestTransferActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
//        }
    }

    @Override
    public void checkContactFail(String errCode, String errMsg) {
        if (isEditTransfer) {
            etPhone.setError(errMsg);
        } else {
            showTipDialog(errMsg);
        }
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {
        if (contactInviteEntity != null) {
            contactInviteDtos.clear();
            contactInviteDtos.addAll(contactInviteEntity.getContactInviteDtos());

            for (int i = 0; i < contactInviteDtos.size(); i++) {
                KycContactEntity kycContactEntity = contactInviteDtos.get(i);
                if (kycContactEntity.getContactsName() != null) {
                    kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                }
            }

            Collections.sort(contactInviteDtos, new RegisterComparator());
            phoneList.clear();
            phoneList.addAll(contactInviteDtos);

            int size = phoneList.size();
            if (size == 0) {
                conRecycler.setVisibility(View.GONE);
                idSideBar.setVisibility(View.GONE);
                tvNoPermission.setText(R.string.wtw_18_1);
                ivNoPermission.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_no_contacts));
                llPermission.setVisibility(View.VISIBLE);
                conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                tvYour.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_040f33_fcfcfc)));
                tvAllow.setVisibility(View.GONE);
            } else {
                conRecycler.setVisibility(View.VISIBLE);
                idSideBar.setVisibility(View.VISIBLE);
                llPermission.setVisibility(View.GONE);
                tvYour.setBackgroundResource(R.drawable.shape_top);
                conBottom.setBackgroundColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_051446_eaeef3)));
                for (int i = 0; i < size; i++) {
                    KycContactEntity kycContactEntity = phoneList.get(i);
                    if (kycContactEntity.getContactsName() != null) {
                        kycContactEntity.setSortLetterName(kycContactEntity.getContactsName());
                    }
                }
//                Collections.sort(phoneList, new PinyinComparator());
                kycContactAdapter.setDataList(phoneList);
            }
        }
    }

    /**
     * 根据是否注册来排序
     */
    public class RegisterComparator implements Comparator<KycContactEntity> {

        @Override
        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getUserStatus().equals(Constants.unregistered) || o2.getUserStatus().equals(Constants.registered)) {
                return 1;
            }else if (o1.getUserStatus().equals(Constants.registered) || o2.getUserStatus().equals(Constants.unregistered)){
                return -1;
            }
            return 0;
        }
    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public TransferContract.TransferContactPresenter getPresenter() {
        return new TransferContactPresenter();
    }

}
