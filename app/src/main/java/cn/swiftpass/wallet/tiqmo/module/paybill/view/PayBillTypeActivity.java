package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillTypeAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeIconEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillTypePresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class PayBillTypeActivity extends BaseCompatActivity<PayBillContract.BillTypePresenter> implements PayBillContract.BillTypeView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_type_list)
    RecyclerView rvTypeList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_country)
    RoundedImageView ivCountry;
    @BindView(R.id.tv_country_name)
    TextView tvCountryName;
//    @BindView(R.id.sc_content)
//    NestedScrollView scContent;

    private PayBillTypeAdapter payBillTypeAdapter;

    private List<PayBillTypeEntity> typeEntityList = new ArrayList<>();
    private List<PayBillTypeEntity> filterTypeList = new ArrayList<>();

    private String searchString;

    private String countryCode;
    private String countryName;
    private String countryLogo;

    private StatusView typeStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;

    private String channelCode = Constants.paybill_inter;

    private PayBillCountryEntity payBillCountryEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_select_type;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_8);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setVisibility(View.GONE);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_31));
        typeStatusView = new StatusView.Builder(mContext, rvTypeList).setNoContentMsg(getString(R.string.BillPay_31))
                .setNoContentView(noHistoryView).build();

        etSearch.setHint(getString(R.string.BillPay_9));

        if (getIntent() != null && getIntent().getExtras() != null) {
//            countryCode = getIntent().getExtras().getString(Constants.paybill_countryCode);
//            countryName = getIntent().getExtras().getString(Constants.paybill_countryName);
//            countryLogo = getIntent().getExtras().getString(Constants.paybill_countryLogo);
            payBillCountryEntity = (PayBillCountryEntity) getIntent().getExtras().getSerializable(Constants.payBillCountryEntity);
            if (payBillCountryEntity != null) {
                countryCode = payBillCountryEntity.countryCode;
                countryName = payBillCountryEntity.countryName;
                countryLogo = payBillCountryEntity.countryLogo;
                if (getString(R.string.BillPay_4).equals(payBillCountryEntity.countryType)) {
                    channelCode = Constants.paybill_local;
                }
                tvCountryName.setText(countryName);
                Glide.with(mContext)
                        .load(countryLogo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivCountry);
            }
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvTypeList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvTypeList.setLayoutManager(manager);
        payBillTypeAdapter = new PayBillTypeAdapter(typeEntityList);
        payBillTypeAdapter.bindToRecyclerView(rvTypeList);

        payBillTypeAdapter.setOnIconItemClickListener(new PayBillTypeAdapter.OnIconItemClick() {
            @Override
            public void onItemClick(PayBillTypeEntity payBillTypeEntity, PayBillTypeIconEntity payBillTypeIconEntity, int position) {
                if (payBillTypeIconEntity != null) {
                    HashMap<String, Object> mHashMap = new HashMap<>();
                    mHashMap.put(Constants.paybill_countryCode, countryCode);
                    mHashMap.put(Constants.paybill_billerType, payBillTypeEntity.billerType);
                    mHashMap.put(Constants.paybill_billerDescription, payBillTypeIconEntity.billerDescription);
                    mHashMap.put(Constants.CHANNEL_CODE, channelCode);
                    ActivitySkipUtil.startAnotherActivity(PayBillTypeActivity.this, PayBillerListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etSearch.hideErrorView();
                searchString = s.toString();
                if (typeEntityList != null && typeEntityList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });

        mPresenter.getPayBillTypeList(countryCode, channelCode);
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            filterTypeList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
//                sideBar.setVisibility(View.VISIBLE);
                filterTypeList.clear();
                payBillTypeAdapter.setDataList(typeEntityList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (typeEntityList != null && typeEntityList.size() > 0) {
                int size = typeEntityList.size();
                for (int i = 0; i < size; i++) {
                    PayBillTypeEntity payBillTypeEntity = typeEntityList.get(i);
                    String name = payBillTypeEntity.billerType;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterTypeList.add(payBillTypeEntity);
                    }
                }
                payBillTypeAdapter.setDataList(filterTypeList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void getPayBillTypeSuccess(PayBillTypeListEntity payBillTypeListEntity) {
        typeEntityList.clear();
        if (payBillTypeListEntity != null) {
            List<PayBillTypeEntity> typeList = payBillTypeListEntity.billTypeList;
            if (typeList.size() == 0) {
                showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
            } else {
                showStatusView(typeStatusView, StatusView.CONTENT_VIEW);
                typeEntityList.addAll(typeList);
                payBillTypeAdapter.setDataList(typeEntityList);
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
//        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillTypePresenter getPresenter() {
        return new PayBillTypePresenter();
    }
}
