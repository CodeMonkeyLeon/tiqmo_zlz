package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class PayBillFetchActivity extends BaseCompatActivity{

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_biller)
    RoundedImageView ivBiller;
    @BindView(R.id.tv_biller_name)
    TextView tvBillerName;
    @BindView(R.id.ll_sku_head)
    LinearLayout llSkuHead;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.fl_fetch_bill)
    FrameLayout flFetchBill;

    private PayBillFetchFragment payBillFetchFragment;

    private PayBillIOListEntity payBillIOListEntity;
    private PayBillOrderEntity payBillOrderEntity;

    private String billerId, sku, billerName, billerLogo;

    private String mChannelCode;
    private int sceneType = -1;

    public static void startPayBillFetchActivity(Activity fromActivity,
                                                 String billerId,
                                                 String billerName,
                                                 String billerLogo,
                                                 String sku,
                                                 String channelCode) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.paybill_billerId, billerId);
        mHashMap.put(Constants.paybill_billerName, billerName);
        mHashMap.put(Constants.paybill_billerLogo, billerLogo);
        mHashMap.put(Constants.paybill_billerSku, sku);
        mHashMap.put(Constants.CHANNEL_CODE, channelCode);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillFetchActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    public static void startPayBillFetchActivity(Activity fromActivity,
                                                 String billerId,
                                                 PayBillIOListEntity payBillIOListEntity,
                                                 PayBillOrderEntity payBillOrderEntity,
                                                 String channelCode) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.paybill_billerId, billerId);
        mHashMap.put(Constants.paybill_billerIOListEntity, payBillIOListEntity);
        mHashMap.put(Constants.paybill_billerOrderEntity, payBillOrderEntity);
        mHashMap.put(Constants.CHANNEL_CODE, channelCode);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillFetchActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startPayBillFetchActivity(Activity fromActivity,
                                                        PayBillIOListEntity data,
                                                        String billerId,
                                                        String billerName,
                                                        String billerLogo,
                                                        String channelCode) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(Constants.paybill_billerIOListEntity, data);
        hashMap.put(Constants.paybill_billerId, billerId);
        hashMap.put(Constants.paybill_billerName, billerName);
        hashMap.put(Constants.paybill_billerLogo, billerLogo);
        hashMap.put(Constants.CHANNEL_CODE, channelCode);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillFetchActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_fetch_paybill;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null) {

            billerId = getIntent().getExtras().getString(Constants.paybill_billerId);
            billerName = getIntent().getExtras().getString(Constants.paybill_billerName);
            billerLogo = getIntent().getExtras().getString(Constants.paybill_billerLogo);
            sku = getIntent().getExtras().getString(Constants.paybill_billerSku);
            payBillIOListEntity = (PayBillIOListEntity) getIntent().getSerializableExtra(Constants.paybill_billerIOListEntity);
            payBillOrderEntity = (PayBillOrderEntity) getIntent().getSerializableExtra(Constants.paybill_billerOrderEntity);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);

            if (payBillOrderEntity != null) {
                billerId = payBillOrderEntity.billerId;
                billerName = payBillOrderEntity.billerName;
                billerLogo = payBillOrderEntity.billerImgUrl;
                sku = payBillOrderEntity.sku;
            }
            tvBillerName.setText(billerName);
            Glide.with(mContext)
                    .load(billerLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.utilities))
                    .into(ivBiller);

            if(payBillOrderEntity != null) {
                payBillFetchFragment = PayBillFetchFragment.getInstance(billerId,payBillIOListEntity,payBillOrderEntity,mChannelCode);
            }else if(payBillIOListEntity != null){
                payBillFetchFragment = PayBillFetchFragment.getInstance(payBillIOListEntity,billerId,billerName,mChannelCode,false,false);
            }else{
                payBillFetchFragment = PayBillFetchFragment.getInstance(billerId,billerName,sku,mChannelCode);
            }

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fl_fetch_bill, payBillFetchFragment);
            ft.commit();
        }
    }

    public void saveEditData(int position, String content) {
        if(payBillFetchFragment != null){
            payBillFetchFragment.saveEditData(position,content);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_get_help})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_help:
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }
}
