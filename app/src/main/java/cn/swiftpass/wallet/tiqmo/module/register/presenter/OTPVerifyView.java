package cn.swiftpass.wallet.tiqmo.module.register.presenter;


import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public interface OTPVerifyView<V extends BasePresenter> extends BaseView<V> {
    void verifyOTPFailed(String errorCode, String errorMsg);

    void verifyOTPSuccess(Void response);
}
