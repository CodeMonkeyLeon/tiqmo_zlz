package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

// serviceBody for 3074
public class ReferralCodeEntity extends BaseEntity {

    public String referralCode;
    public String[] referralCodeDesc = new String[3];
    public String[] amountAndCurrencyInfo = new String[2];

    //分享描述
    public String shareDesc;
}
