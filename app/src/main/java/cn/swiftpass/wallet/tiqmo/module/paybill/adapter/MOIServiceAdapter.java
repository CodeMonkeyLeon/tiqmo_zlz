package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.interfaces.OnMoiServiceItemClickListener;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class MOIServiceAdapter extends BaseRecyclerAdapter<PayBillerEntity> {

    public MOIServiceAdapter(@Nullable List<PayBillerEntity> data) {
        super(R.layout.item_paybill_country, data);
    }


    private OnMoiServiceItemClickListener mOnItemClickListener;


    public void setOnMoiItemClickListener(OnMoiServiceItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillerEntity payBillerEntity, int position) {
        if (payBillerEntity != null) {
            baseViewHolder.setText(R.id.tv_country_name, payBillerEntity.billerName);

            baseViewHolder.setGone(R.id.tv_area_code, true);

            RoundedImageView ivCountry = baseViewHolder.getView(R.id.iv_country);
            String imgUrl = payBillerEntity.imgUrl;
            Glide.with(mContext)
                    .load(imgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivCountry);
            baseViewHolder.setGone(R.id.tv_country_type, true);

            View itemView = baseViewHolder.getView(R.id.id_cl_moi_item);
            if (itemView != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onMoiServiceItemClick(position, payBillerEntity);
                        }
                    }
                });
            }
        }
    }
}
