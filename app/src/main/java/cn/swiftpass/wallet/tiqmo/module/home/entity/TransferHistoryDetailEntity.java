package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferHistoryDetailEntity extends BaseEntity {

    public String orderNo;
    public String logo;
    public String orderDesc;
    public String tradeAmount;
    public String currencyType;
    public String createTime;
    public String orderStatus;
    public String paymentMethod;
    public String orderType;
    public String orderDirection;
    public String outTradeNo;
    public String originalOrderNo;
    public String respMsg;
    public int sceneType;
    public String sex;
    public List<ExtendEntity> extendProperties = new ArrayList<>();
    public List<ExtendEntity> basicExtendProperties = new ArrayList<>();

    //分享描述
    public String shareDesc;
    //优惠类型 1：返现 2：立减
    public String discountType;
    //返现订单号
    public String cashBackOrderNo;
    //原金额
    public String orderAmount;
    // 代金券的 pin 码
    public String voucherPinCode;
    public String remark;
    public String popupInfo;
    public int subSceneType;
    //是否展示 发票 按钮 1-展示；0-不展示
    public int isShowEInvoice;

    //download按钮是否需要下载pdf       1-下载；0-不下载
    public int downloadFile;

    /**
     * 发票类型
     */
    public String invoiceType;


    public boolean isNeedDownloadFile() {
        return downloadFile == 1;
    }

}
