package cn.swiftpass.wallet.tiqmo.module.home.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;

public class HomeAnalysisEntity extends BaseEntity {

    //金额单位(如果没有返回，APP可以获取默认币种)
    private String moneyCurrency;

    //今日支出信息(注意：如果这个对象没有返回或者为null,APP就应该按照需求展示词条”No transactions yet”)
    public TodaySpendingEntity todaySpendingInfo;

    //自然月预算信息(注意：如果这个对象没有返回或者为null,APP就应该按照需求展示”Set up budget +”)
    public BudgetHomeEntity budgetInfo;

    public String getMoneyCurrency() {
        if (TextUtils.isEmpty(moneyCurrency)) {
            return LocaleUtils.getCurrencyCode("");
        }
        return this.moneyCurrency;
    }
}
