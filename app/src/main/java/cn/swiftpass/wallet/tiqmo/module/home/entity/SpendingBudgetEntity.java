package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SpendingBudgetEntity extends BaseEntity {
    //记录id
    public String id = "";
    //分类代码
    public int categoryCode;
    public String categoryName;
    //预算金额 单位：最小币种单位
    public String budgetLimitAmount;
    //分类位置 1,2,3,4,5传给后台
    public int categoryOrder;
    //已花预算金额 单位：最小币种单位
    public String alreadySpendingAmount;
    //已花预算百分比(结果已乘100，无百分号)
    public String spendingCategoryRatio;
}
