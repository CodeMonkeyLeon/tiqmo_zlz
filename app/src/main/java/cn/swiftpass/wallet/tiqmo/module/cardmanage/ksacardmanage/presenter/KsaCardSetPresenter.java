package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class KsaCardSetPresenter implements CardKsaContract.SetCardLimitPresener {

    CardKsaContract.SetCardLimitView setCardLimitView;

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(setCardLimitView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                setCardLimitView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                setCardLimitView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaLimitChannel(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaLimitChannel(proxyCardNo, new LifecycleMVPResultCallback<KsaLimitChannelEntity>(setCardLimitView,true) {
            @Override
            protected void onSuccess(KsaLimitChannelEntity result) {
                setCardLimitView.getKsaLimitChannelSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                setCardLimitView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardLimit(String txnType,  String dailyLimitValue, String monthlyLimitValue, String proxyCardNo,String up) {
        AppClient.getInstance().getCardManager().setKsaCardLimit(txnType, dailyLimitValue, monthlyLimitValue, proxyCardNo,up, new LifecycleMVPResultCallback<Void>(setCardLimitView,true) {
            @Override
            protected void onSuccess(Void result) {
                setCardLimitView.setKsaCardLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                setCardLimitView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardKsaContract.SetCardLimitView setCardLimitView) {
        this.setCardLimitView = setCardLimitView;
    }

    @Override
    public void detachView() {
        this.setCardLimitView = null;
    }
}
