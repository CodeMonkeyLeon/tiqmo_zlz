package cn.swiftpass.wallet.tiqmo.module.home.view;

import static android.app.Activity.RESULT_OK;

import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.google.android.flexbox.FlexboxLayout;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterCodeEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CardDetailsAcitivity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ViewAnimUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.TimePickerDialogUtils;
import cn.swiftpass.wallet.tiqmo.widget.pickerview.listener.OnTimeSelectListener;

public class FilterHistoryDialogFragment extends BaseDialogFragmentN implements View.OnClickListener {

    TextView tvClear;
    LinearLayout llTop;
    TextView tvLine;
    LinearLayout llTransferCategory;
    FlexboxLayout flCategory;
    ScrollView scCategory;
    LinearLayout llMid;
    LinearLayout llPaymentType;
    CheckBox cbDebit;
    CheckBox cbCredit;
    LinearLayout llType;
    LinearLayout llMidTwo;
    LinearLayout llDateTitle;
    EditTextWithDel etDateFrom;
    ImageView ivDateFrom;
    ImageView ivDateFromDelete;
    ImageView ivDateToDelete;
    ImageView ivCategoryArrow;
    ImageView ivTypeArrow;
    ImageView ivDateArrow;
    EditTextWithDel etDateTo;
    ImageView ivDateTo;
    LinearLayout llDate;
    TextView tvCancel;
    TextView tvConfirm;
    TextView tvError;
    LinearLayout llBottom;
    ConstraintLayout llContent;
    ConstraintLayout conContent;
    LinearLayout con_date_to;
    LinearLayout con_date_from;

    private Date mSelectedFromDate;
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedFromDate = Calendar.getInstance();
    Calendar startFromDate = Calendar.getInstance();
    Calendar endFromDate = Calendar.getInstance();

    private Date mSelectedToDate;
    // 设置日历的显示的地区（根据自己的需要写）
    Calendar selectedToDate = Calendar.getInstance();
    Calendar startToDate = Calendar.getInstance();
    Calendar endToDate = Calendar.getInstance();

    private HashMap<Integer, String> selectedArray = new HashMap<>();
    private List<FilterCodeEntity> filterList = new ArrayList<>();
    private FilterListEntity filterListEntity;

    public List<String> typeList = new ArrayList<>();
    public List<String> categoryList = new ArrayList<>();
    public List<String> tradeType = new ArrayList<>();

    private String startDate = "", endDate = "";
    private boolean isShowDateError;

    public HashMap<String, String> charMap = new HashMap<>();
    private FilterEntity filterEntity;

    private boolean mIsPreAuth; //是否是预授权

    public static FilterHistoryDialogFragment newInstance(FilterEntity filterEntity, FilterListEntity filterListEntity) {
        FilterHistoryDialogFragment fragment = new FilterHistoryDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("filter", filterEntity);
        bundle.putSerializable("filterListEntity", filterListEntity);
        fragment.setArguments(bundle);
        return fragment;
    }


    public static FilterHistoryDialogFragment newHistoryInstance(
            FilterEntity filterEntity,
            FilterListEntity filterListEntity,
            boolean isPreAuth
    ) {
        FilterHistoryDialogFragment fragment = new FilterHistoryDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("filter", filterEntity);
        bundle.putSerializable("filterListEntity", filterListEntity);
        bundle.putBoolean("isPreAuth", isPreAuth);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayoutID() {
        return R.layout.dialog_filter_history;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);

        scCategory.setAlpha(1f);
        llType.setAlpha(1f);
//        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
//        if (configEntity != null) {
//            if (configEntity.filterOrderConditions != null && configEntity.filterOrderConditions.size() > 0) {
//                filterList = configEntity.filterOrderConditions;
//            }
//        }

        // January February March April May June July August September October November December
        // Jan、Feb、Mar、Apr、May、Jun、Jul、Aug、Sept、Oct、Nov、Dec
        charMap.put("January", "Jan");
        charMap.put("February", "Feb");
        charMap.put("March", "Mar");
        charMap.put("April", "Apr");
        charMap.put("May", "May");
        charMap.put("June", "Jun");
        charMap.put("July", "Jul");
        charMap.put("August", "Aug");
        charMap.put("September", "Sept");
        charMap.put("October", "Oct");
        charMap.put("November", "Nov");
        charMap.put("December", "Dec");

        etDateFrom.getEditText().setCursorVisible(false);
        etDateFrom.getEditText().setInputType(InputType.TYPE_NULL);
        etDateFrom.getEditText().setFocusable(false);
        etDateFrom.getEditText().setFocusableInTouchMode(false);
        etDateTo.getEditText().setCursorVisible(false);
        etDateTo.getEditText().setInputType(InputType.TYPE_NULL);
        etDateTo.getEditText().setFocusable(false);
        etDateTo.getEditText().setFocusableInTouchMode(false);

        startFromDate.set(Calendar.YEAR, selectedFromDate.get(Calendar.YEAR) - 90);
        endFromDate.set(Calendar.YEAR, selectedFromDate.get(Calendar.YEAR));

        startToDate.set(Calendar.YEAR, selectedToDate.get(Calendar.YEAR) - 90);
        endToDate.set(Calendar.YEAR, selectedToDate.get(Calendar.YEAR));

        cbCredit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cbCredit.setTextColor(isChecked ? mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)) :
                        mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                if (isChecked) {
                    if (!typeList.contains("I")) {
                        typeList.add("I");
                    }
                } else {
                    if (typeList.contains("I")) {
                        typeList.remove("I");
                    }
                }
            }
        });

        cbDebit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cbDebit.setTextColor(isChecked ? mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)) :
                        mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                if (isChecked) {
                    if (!typeList.contains("P")) {
                        typeList.add("P");
                    }
                } else {
                    if (typeList.contains("P")) {
                        typeList.remove("P");
                    }
                }
            }
        });

        if (getArguments() != null) {
            filterEntity = (FilterEntity) getArguments().getSerializable("filter");
            filterListEntity = (FilterListEntity) getArguments().getSerializable("filterListEntity");
            mIsPreAuth = getArguments().getBoolean("isPreAuth", false);
            if (filterListEntity != null) {
                filterList = filterListEntity.filterOrderConditions;
            }
            setFilter(filterEntity);
        }

        etDateFrom.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startContentAnimation(false);
                startCategoryAnim(false);
                startTypeAnim(false);
                ivTypeArrow.setRotationX(180f);
                selectFromDatetime();
            }
        });

        etDateTo.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startContentAnimation(false);
                startCategoryAnim(false);
                startTypeAnim(false);
                ivTypeArrow.setRotationX(180f);
                selectToDatetime();
            }
        });


        if (mIsPreAuth) {
            llTop.setVisibility(View.GONE);
            tvLine.setVisibility(View.GONE);
            llMid.setVisibility(View.GONE);
            llMidTwo.setVisibility(View.GONE);
        }


    }

    private void initView(View parentView) {
        tvClear = parentView.findViewById(R.id.tv_clear);
        llTop = parentView.findViewById(R.id.ll_top);
        tvLine = parentView.findViewById(R.id.tv_line);
        llTransferCategory = parentView.findViewById(R.id.ll_transfer_category);
        flCategory = parentView.findViewById(R.id.fl_category);
        scCategory = parentView.findViewById(R.id.sc_category);
        llMid = parentView.findViewById(R.id.ll_mid);
        llPaymentType = parentView.findViewById(R.id.ll_payment_type);
        cbDebit = parentView.findViewById(R.id.cb_debit);
        cbCredit = parentView.findViewById(R.id.cb_credit);
        llType = parentView.findViewById(R.id.ll_type);
        llMidTwo = parentView.findViewById(R.id.ll_mid_two);
        llDateTitle = parentView.findViewById(R.id.ll_date_title);
        etDateFrom = parentView.findViewById(R.id.et_date_from);
        ivDateFrom = parentView.findViewById(R.id.iv_date_from);
        ivDateFromDelete = parentView.findViewById(R.id.iv_date_from_delete);
        ivDateToDelete = parentView.findViewById(R.id.iv_date_to_delete);
        ivCategoryArrow = parentView.findViewById(R.id.iv_category_arrow);
        ivTypeArrow = parentView.findViewById(R.id.iv_type_arrow);
        ivDateArrow = parentView.findViewById(R.id.iv_date_arrow);
        etDateTo = parentView.findViewById(R.id.et_date_to);
        ivDateTo = parentView.findViewById(R.id.iv_date_to);
        llDate = parentView.findViewById(R.id.ll_date);
        tvCancel = parentView.findViewById(R.id.tv_cancel);
        tvConfirm = parentView.findViewById(R.id.tv_confirm);
        tvError = parentView.findViewById(R.id.tv_error_date);
        llBottom = parentView.findViewById(R.id.ll_bottom);
        llContent = parentView.findViewById(R.id.ll_content);
        conContent = parentView.findViewById(R.id.con_content);
        con_date_to = parentView.findViewById(R.id.con_date_to);
        con_date_from = parentView.findViewById(R.id.con_date_from);

        etDateFrom.getTlEdit().setHint(mContext.getString(R.string.History_filter_8));
        etDateTo.getTlEdit().setHint(mContext.getString(R.string.History_filter_9));

        llTransferCategory.setOnClickListener(this);
        llPaymentType.setOnClickListener(this);
        tvClear.setOnClickListener(this);
        llDateTitle.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        ivDateFromDelete.setOnClickListener(this);
        ivDateToDelete.setOnClickListener(this);
    }

    private void setFilter(FilterEntity filterEntity) {
        if (filterEntity != null) {
            selectedArray = filterEntity.selectedArray;
            categoryList = filterEntity.categoryList;
            tradeType = filterEntity.tradeType;
            typeList = filterEntity.paymentType;
            if (typeList.contains("P")) {
                cbDebit.setChecked(true);
                cbDebit.setTextColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)));
            } else {
                cbDebit.setChecked(false);
            }

            mSelectedFromDate = filterEntity.fromDate;
            mSelectedToDate = filterEntity.toDate;

            startDate = filterEntity.startDate;
            String showStartDate = filterEntity.showStartDate;
            String showEndDate = filterEntity.showEndDate;
            if (!TextUtils.isEmpty(showStartDate)) {
                etDateFrom.getEditText().setText(showStartDate);
                ivDateFrom.setVisibility(View.GONE);
                ivDateFromDelete.setVisibility(View.VISIBLE);
            } else {
                etDateFrom.getEditText().setText("");
                mSelectedFromDate = null;
                startDate = "";
            }

            endDate = filterEntity.endDate;
            if (!TextUtils.isEmpty(showEndDate)) {
                etDateTo.getEditText().setText(showEndDate);
                ivDateTo.setVisibility(View.GONE);
                ivDateToDelete.setVisibility(View.VISIBLE);
            } else {
                etDateTo.getEditText().setText("");
                mSelectedToDate = null;
                endDate = "";
            }

            if (typeList.contains("I")) {
                cbCredit.setChecked(true);
            } else {
                cbCredit.setChecked(false);
            }
            int size = filterList.size();
            for (int i = 0; i < size; i++) {
                if (!TextUtils.isEmpty(selectedArray.get(i))) {
                    filterList.get(i).selected = "1";
                } else {
                    filterList.get(i).selected = "0";
                }
            }
        } else {
            filterEntity = new FilterEntity();
        }
        initFilterCode(flCategory, filterList);
    }

    private void initFilterCode(FlexboxLayout flexboxLayout, final List<FilterCodeEntity> filterList) {
        if (filterList != null && filterList.size() > 0) {
            flexboxLayout.removeAllViews();
            flexboxLayout.setVisibility(View.VISIBLE);
            flexboxLayout.setFlexDirection(FlexboxLayout.FLEX_DIRECTION_ROW);
            flexboxLayout.setFlexWrap(FlexboxLayout.FLEX_WRAP_WRAP);
            flexboxLayout.setDividerDrawable(ContextCompat.getDrawable(mContext, R.drawable.item_divider_filter));
            flexboxLayout.setShowDivider(FlexboxLayout.SHOW_DIVIDER_MIDDLE);
            for (int i = 0; i < filterList.size(); i++) {
                final FilterCodeEntity marks = filterList.get(i);
                if (marks != null) {
                    TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_filter_text, null);
                    textView.setActivated(true);
                    textView.setText(marks.title);
                    textView.setTag(marks.value);
                    textView.setSelected(!"0".equals(marks.selected));
                    textView.setTextColor(!"0".equals(marks.selected) ? mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)) :
                            mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                    if (!"0".equals(marks.selected)) {
                        selectedArray.put(i, marks.value);
                    }
                    final int finalI = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if ("0".equals(marks.selected)) {
                                marks.selected = "1";
                                selectedArray.put(finalI, marks.value);
                                tradeType.add(marks.value);
                                categoryList.add(marks.title);
                            } else {
                                marks.selected = "0";
                                selectedArray.remove(finalI);
                                tradeType.remove(marks.value);
                                categoryList.remove(marks.title);
                            }
                            view.setSelected(!"0".equals(marks.selected));
                            textView.setTextColor(!"0".equals(marks.selected) ? mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_fc4f00_061f6f)) :
                                    mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                        }
                    });
                    flexboxLayout.addView(textView);
                }
            }
        } else {
            flexboxLayout.setVisibility(View.GONE);
        }

    }

    private void startContentAnimation(boolean isExpand) {
        final int bottom = llContent.getPaddingBottom();
        if (isExpand) {
            ValueAnimator valueAnimator = ValueAnimator.ofInt(bottom, bottom - AndroidUtils.dip2px(mContext, 160));
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int value = (int) animation.getAnimatedValue();
                    llContent.setPadding(llContent.getPaddingLeft(),
                            llContent.getPaddingTop(),
                            llContent.getPaddingRight(),
                            value);
                }
            });
            valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            valueAnimator.setDuration(200);
            valueAnimator.start();
        } else {
            ValueAnimator valueAnimator = ValueAnimator.ofInt(bottom, bottom + AndroidUtils.dip2px(mContext, 160));
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int value = (int) animation.getAnimatedValue();
                    llContent.setPadding(llContent.getPaddingLeft(),
                            llContent.getPaddingTop(),
                            llContent.getPaddingRight(),
                            value);
                }
            });
            valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            valueAnimator.setDuration(400);
            valueAnimator.start();
        }
    }

    private void startCategoryAnim(boolean isExpand) {
        if (isExpand) {
            ivCategoryArrow.setRotationX(0f);
            ViewAnimUtils.showViewWithAnim(scCategory, 300, AndroidUtils.dip2px(mContext, 170));
        } else {
            ViewAnimUtils.hideViewWithAnim(scCategory, 300, AndroidUtils.dip2px(mContext, 170),
                    new ViewAnimUtils.AnimationEnd() {
                        @Override
                        public void onAnimationEnd() {
                            ivCategoryArrow.setRotationX(180f);
                        }
                    });
        }
    }

    private void startTypeAnim(boolean isExpand) {
        if (isExpand) {
            ivTypeArrow.setRotationX(0f);
            ViewAnimUtils.showViewWithAnim(llType, 200, AndroidUtils.dip2px(mContext, 36));
        } else {
            ViewAnimUtils.hideViewWithAnim(llType, 200, AndroidUtils.dip2px(mContext, 36),
                    new ViewAnimUtils.AnimationEnd() {
                        @Override
                        public void onAnimationEnd() {
                            ivTypeArrow.setRotationX(180f);
                        }
                    });
        }
    }

    private void startDateAnim(boolean isExpand) {
        if (isExpand) {
            ivDateArrow.setRotationX(0f);
            ViewAnimUtils.showViewWithAnim(llDate, 400, AndroidUtils.dip2px(mContext, 60));
            if (isShowDateError) {
                tvError.setVisibility(View.VISIBLE);
            }
        } else {
            ViewAnimUtils.hideViewWithAnim(llDate, 400, AndroidUtils.dip2px(mContext, 60),
                    new ViewAnimUtils.AnimationEnd() {
                        @Override
                        public void onAnimationEnd() {
                            ivDateArrow.setRotationX(180f);
                            if (isShowDateError) {
                                tvError.setVisibility(View.GONE);
                            }
                        }
                    });
        }
    }

    private void selectFromDatetime() {
        if (mSelectedFromDate == null) {
            selectedFromDate.setTime(endFromDate.getTime());
        } else {
            selectedFromDate.setTime(mSelectedFromDate);
        }
        TimePickerDialogUtils.showTimePicker(getContext(), selectedFromDate, startFromDate, endFromDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedFromDate = date;
                ivDateFromDelete.setVisibility(View.VISIBLE);
                ivDateFrom.setVisibility(View.GONE);
                startDate = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(date);
                if (!TextUtils.isEmpty(endDate)) {
                    boolean big = AndroidUtils.compareDate(mSelectedFromDate, mSelectedToDate);
                    if (!big) {
                        tvError.setVisibility(View.VISIBLE);
                        isShowDateError = true;
                        tvConfirm.setEnabled(false);
                    } else {
                        tvConfirm.setEnabled(true);
                        tvError.setVisibility(View.GONE);
                        isShowDateError = false;
                    }
                }
                try {
                    String monthKey = startDate.split(" ")[0];
                    String month = startDate.replace(monthKey, charMap.get(monthKey));
                    etDateFrom.getEditText().setText(month);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
            }
        }, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                startContentAnimation(true);
                if (scCategory.getVisibility() == View.GONE) {
                    startCategoryAnim(true);
                }
                if (llType.getVisibility() == View.GONE) {
                    startTypeAnim(true);
                }
            }
        });
    }

    private void selectToDatetime() {
        if (mSelectedToDate == null) {
            selectedToDate.setTime(endToDate.getTime());
        } else {
            selectedToDate.setTime(mSelectedToDate);
        }
        TimePickerDialogUtils.showTimePicker(getContext(), selectedToDate, startToDate, endToDate, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                mSelectedToDate = date;
                ivDateToDelete.setVisibility(View.VISIBLE);
                ivDateTo.setVisibility(View.GONE);
                endDate = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH).format(date);
                if (!TextUtils.isEmpty(startDate)) {
                    boolean big = AndroidUtils.compareDate(mSelectedFromDate, mSelectedToDate);
                    if (!big) {
                        tvError.setVisibility(View.VISIBLE);
                        isShowDateError = true;
                        tvConfirm.setEnabled(false);
                    } else {
                        tvError.setVisibility(View.GONE);
                        isShowDateError = false;
                        tvConfirm.setEnabled(true);
                    }
                }
                try {
                    String monthKey = endDate.split(" ")[0];
                    String month = endDate.replace(monthKey, charMap.get(monthKey));
                    etDateTo.getEditText().setText(month);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
            }
        }, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (scCategory.getVisibility() == View.GONE) {
                    startCategoryAnim(true);
                }
                if (llType.getVisibility() == View.GONE) {
                    startTypeAnim(true);
                }
                startContentAnimation(true);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        Intent resultIntent = new Intent();
        switch (view.getId()) {
            case R.id.ll_transfer_category:
                startCategoryAnim(scCategory.getVisibility() != View.VISIBLE);
                break;
            case R.id.ll_payment_type:
                startTypeAnim(llType.getVisibility() != View.VISIBLE);
                break;
            case R.id.tv_clear:
                filterEntity = new FilterEntity();
                setFilter(filterEntity);
                tvError.setVisibility(View.GONE);
                isShowDateError = false;
                ivDateFromDelete.setVisibility(View.GONE);
                ivDateFrom.setVisibility(View.VISIBLE);
                ivDateToDelete.setVisibility(View.GONE);
                ivDateTo.setVisibility(View.VISIBLE);
                tvConfirm.setEnabled(true);
                break;
            case R.id.ll_date_title:
                startDateAnim(llDate.getVisibility() != View.VISIBLE);
                break;
            case R.id.iv_date_from_delete:
                etDateFrom.getEditText().setText(null);
                mSelectedFromDate = null;
                startDate = "";
                tvConfirm.setEnabled(true);
                tvError.setVisibility(View.GONE);
                isShowDateError = false;
                ivDateFromDelete.setVisibility(View.GONE);
                ivDateFrom.setVisibility(View.VISIBLE);
                break;
            case R.id.iv_date_to_delete:
                etDateTo.getEditText().setText(null);
                tvConfirm.setEnabled(true);
                mSelectedToDate = null;
                endDate = "";
                tvError.setVisibility(View.GONE);
                isShowDateError = false;
                ivDateToDelete.setVisibility(View.GONE);
                ivDateTo.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_confirm:
                if (filterEntity != null) {
                    filterEntity.tradeType = tradeType;
                    filterEntity.categoryList = categoryList;
                    filterEntity.paymentType = typeList;
                    filterEntity.startDate = startDate;
                    filterEntity.fromDate = mSelectedFromDate;
                    filterEntity.toDate = mSelectedToDate;
                    filterEntity.showStartDate = etDateFrom.getEditText().getText().toString().trim();
                    filterEntity.endDate = endDate;
                    filterEntity.showEndDate = etDateTo.getEditText().getText().toString().trim();
                }
                resultIntent.putExtra("filterEntity", filterEntity);
                if (getTargetFragment() != null) {
                    getTargetFragment().onActivityResult(BaseHistoryFragment.FILTER_REQUEST, RESULT_OK, resultIntent);
                } else {
                    if (getActivity() != null) {
                        if (getActivity() instanceof AccountDetailActivity) {
                            ((AccountDetailActivity) getActivity()).onActivityResult(AccountDetailActivity.filter_request, RESULT_OK, resultIntent);
                        } else if (getActivity() instanceof CardDetailsAcitivity) {
                            ((CardDetailsAcitivity) getActivity()).onActivityResult(CardDetailsAcitivity.filter_request, RESULT_OK, resultIntent);
                        }
                    }
                }
                dismiss();
                break;
            default:
                break;
        }
    }

}
