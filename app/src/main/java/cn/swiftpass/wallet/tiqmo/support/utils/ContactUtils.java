package cn.swiftpass.wallet.tiqmo.support.utils;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.SparseArray;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.RegexUtil;

public class ContactUtils {


    private static final String ATOZSTR = "[A-Z]";
    private static final String SPECIAL_CHAR_ONE = "#";
    private static final String SPECIAL_CHAR_TWO = "@";
    private String phone;
    /**
     * 要实现读取出来的列表顺序是a-z排序
     */
    private CharacterParser characterParser;

    public ContactUtils() {
        characterParser = new CharacterParser();
    }


    public static ContactUtils getInstance() {
        return ContactUtils.ContactUtilsolder.instance;
    }

    private static class ContactUtilsolder {
        private static ContactUtils instance = new ContactUtils();
    }

    public ArrayList<KycContactEntity> getContacts() {
        return contacts;
    }

    /**
     * 作为缓存来处理
     */
    private ArrayList<KycContactEntity> contacts;
    private static final String TAG = "ContactUtils";


    /**
     * @param context
     * @param isRefresh true 需要从本地重新读取
     * @return
     */
    public ArrayList<KycContactEntity> getAllContacts(Context context, boolean isRefresh) {
        if (isRefresh) {
            return getAllContacts(context);
        } else {
            return contacts;
        }
    }


    /**
     * 外部不需要维护线程 内部已经处理
     *
     * @param context
     * @param isRefresh
     * @param onReadContactsSuccessCallBack
     */
    public void getAllContactsAsyn(Context context, boolean isRefresh, OnReadContactsSuccessCallBack onReadContactsSuccessCallBack) {
        phone = ProjectApp.getPhone();
//        if (isRefresh) {
            new ReadContactsTask(context,SpUtils.getInstance().getCallingCode(), onReadContactsSuccessCallBack).execute();
//        } else {
//            if (onReadContactsSuccessCallBack != null) {
//                onReadContactsSuccessCallBack.OnReadContactsSuccess(contacts);
//            }
//        }
    }

    public void getAllContactsAsyn(Context context, boolean isRefresh,String callingCode, OnReadContactsSuccessCallBack onReadContactsSuccessCallBack) {
        phone = ProjectApp.getPhone();
//        if (isRefresh) {
            new ReadContactsTask(context,callingCode, onReadContactsSuccessCallBack).execute();
//        } else {
//            if (onReadContactsSuccessCallBack != null) {
//                onReadContactsSuccessCallBack.OnReadContactsSuccess(contacts);
//            }
//        }
    }

    public ArrayList<KycContactEntity> getInviteContact(Context context, boolean isRefresh) {
        if (isRefresh) {
            return getAllContact(context, false,SpUtils.getInstance().getCallingCode());
        } else {
            return contacts;
        }
    }

//    private synchronized ArrayList<KycContactEntity> getAllContacts(Context context,String callingCode) {
//        return getAllContact(context, true,callingCode);
//    }

    private synchronized ArrayList<KycContactEntity> getAllContacts(Context context) {
        return getAllContact(context, true,SpUtils.getInstance().getCallingCode());
    }

    private synchronized ArrayList<KycContactEntity> getAllContact(Context context, boolean needEmail,String callingCode) {
        String plusCallingCode = "+"+callingCode+" ";
        String zeroCallingCode = "00"+callingCode;
        Set<String> filterPhoneUserNames = new HashSet<String>();
        ArrayList<KycContactEntity> result = null;
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = null;
        try {
            Uri contactUri = ContactsContract.RawContactsEntity.CONTENT_URI;

            String[] fields = new String[]{ContactsContract.RawContactsEntity.CONTACT_ID,
                    ContactsContract.RawContactsEntity.MIMETYPE,
                    ContactsContract.RawContactsEntity.DATA1};

            String selection = ContactsContract.RawContactsEntity.DELETED + "=? AND " + ContactsContract.RawContactsEntity.MIMETYPE + " IN (?,?,?)";


            //只要名字，邮箱，手机号，其他数据不要
            String[] selectionArgs = null;
            //判断是否需要邮箱  默认需要
            if (needEmail) {
                selectionArgs = new String[]{"0",
                        //0表示数据没有被删除
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,};
            } else {
                selectionArgs = new String[]{"0",
                        //0表示数据没有被删除
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE
                };
            }
            /**
             * 1.在Android中怎么区分各个Provider？有提供联系人的，有提供图片的等等。所以就需要有一个唯一的标识来标识这个Provider，Uri就是这个标识
             * 2.查询要返回的列（Column）
             * 3.查询where字句
             * 4.查询条件属性值
             * 5.结果排序规则
             */
            cursor = cr.query(contactUri, fields, selection, selectionArgs, null);

            int itemCount = 0;
            if (cursor != null) {
                SparseArray<List<KycContactEntity>> mapEntity = new SparseArray<>(cursor.getCount());
                SparseArray<String> mapName = new SparseArray<>(cursor.getCount() / 2);
                SparseArray<String> mapCompany = new SparseArray<>(cursor.getCount() / 2);
                int contactIDIndex = cursor.getColumnIndex(fields[0]);
                int typeIndex = cursor.getColumnIndex(fields[1]);
                int dataIndex = cursor.getColumnIndex(fields[2]);
                int companyIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY);
                while (cursor.moveToNext()) {
                    int contactID = cursor.getInt(contactIDIndex);
                    String type = cursor.getString(typeIndex);
                    //三星部分手机手机号中间格式化有空格 要去掉
                    String data = cursor.getString(dataIndex);
                    String company = "";
                    if (ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE.equals(type)) {
                        company = cursor.getString(companyIndex);
                        mapCompany.append(contactID, company);
                    }
                    List<KycContactEntity> entityList = mapEntity.get(contactID);
                    if (entityList == null) {
                        entityList = new LinkedList<>();
                        //contactID是递增的，所以用append增加性能
                        mapEntity.append(contactID, entityList);
                    }
                    KycContactEntity entity = null;
                    switch (type) {
                        //data是手机号
                        case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                            data = data.replace(" ", "");
                            //先过滤掉除数字外的字符 然后判断00966,966,0开头，然后匹配5*开头去掉51,52，长度9位
                            String regEx="[^0-9]";
                            Pattern p = Pattern.compile(regEx);
                            Matcher m = p.matcher(data);
                            data = m.replaceAll("").trim();
                            if (data.startsWith(zeroCallingCode) || data.startsWith(callingCode) || data.startsWith("0")) {
                                if (data.startsWith(zeroCallingCode) && data.length() > zeroCallingCode.length()) {
                                    data = data.replace(data.substring(0, zeroCallingCode.length()), plusCallingCode);
                                } else if (data.startsWith("0") && "966".equals(callingCode) && data.length() > 1) {
                                    data = plusCallingCode + data.substring(1);
                                } else if (data.startsWith(callingCode) && data.length() > callingCode.length()) {
                                    data = data.replace(data.substring(0, callingCode.length()), plusCallingCode);
                                }else{
                                    break;
                                }
                                if(data.length()>5 && "966".equals(callingCode) && !RegexUtil.isPhone(data.substring(5))){
                                    break;
                                }
                                if (data.length() > 14) {
                                    break;
                                }
                                if (!data.contains(phone)) {
                                    entity = new KycContactEntity();
                                    entity.setPhone(data);
                                    entity.setPhoneNumber(data);
                                }
                            }
                            break;
                        //data是邮箱
                        case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE:
//                            entity = new KycContactEntity();
//                            entity.setPhone(data);
                            break;
                        //data是名字
                        case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                            mapName.append(contactID, data);
                            break;
                        default:
                            break;
                    }
//                    if (entity != null && !ProjectApp.getPhone().equals(entity.getRequestPhoneNumber())) {
                    if (entity != null) {
                        entityList.add(entity);
                        itemCount++;
                    }
//                    }
                }
                cursor.close();//提前close释放内存，不用等到finally
                cursor = null;
                result = new ArrayList<>(itemCount);
                for (int i = 0, size = mapEntity.size(); i < size; i++) {
                    List<KycContactEntity> contactList = mapEntity.valueAt(i);
                    for (KycContactEntity entity : contactList) {
                        addSortLetter(result, mapEntity, mapName, mapCompany, i, entity);
                    }
                }
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        ArrayList<KycContactEntity> filterContacts = removalDuplicateAndSort(filterPhoneUserNames, result);
        return filterContacts;
    }

    private void addSortLetter(ArrayList<KycContactEntity> result, SparseArray<List<KycContactEntity>> mapEntity, SparseArray<String> mapName, SparseArray<String> mapCompany, int i, KycContactEntity entity) {
        String userName = mapName.get(mapEntity.keyAt(i));
        String company = mapCompany.get(mapEntity.keyAt(i));
        if (!TextUtils.isEmpty(company)) {
            userName = userName + " " + company;
        }
        // 汉字转换成拼音
        if (!TextUtils.isEmpty(userName)) {
//            String pinyin = converterToFirstSpell(userName);
            String pinyin = Pinyin.toPinyin(userName, "");
//            LogUtils.i(TAG, "userName：--->" + userName + " pinyin:" + pinyin);
            if (!TextUtils.isEmpty(pinyin)) {
                String sortString = pinyin.substring(0, 1).toUpperCase();
                // 正则表达式，判断首字母是否是英文字母
                if (sortString.matches(ATOZSTR)) {
                    entity.setSortLetter(sortString.toUpperCase());
                } else {
                    entity.setSortLetter(SPECIAL_CHAR_ONE);
                }
            } else {
                entity.setSortLetter(SPECIAL_CHAR_ONE);
            }
        } else {
            entity.setSortLetter(SPECIAL_CHAR_ONE);
        }
        entity.setContactsName(userName);
        result.add(entity);
    }

    private ArrayList<KycContactEntity> removalDuplicateAndSort(Set<String> filterPhoneUserNames, ArrayList<KycContactEntity> result) {
        ArrayList<KycContactEntity> filterContacts = new ArrayList<>();
        //重复通讯录去重
        if (result != null) {
            for (Iterator iter = result.iterator(); iter.hasNext(); ) {
                KycContactEntity contractItem = (KycContactEntity) iter.next();
                //用户名手机号拼接 来判断去重
                String filterItem = "";
                if (!TextUtils.isEmpty(contractItem.getContactsName())) {
                    filterItem += contractItem.getContactsName();
                }
                if (!TextUtils.isEmpty(contractItem.getPhone())) {
                    filterItem += contractItem.getPhone();
                }
//                LogUtils.i(TAG, "filterItem：--->" + filterItem);
                if (filterPhoneUserNames.add(filterItem))
                    filterContacts.add(contractItem);
            }
        }
        //a-z排序
        Collections.sort(filterContacts, new PinyinComparator());
        this.contacts = filterContacts;
        return filterContacts;
    }


    public static class PinyinComparator implements Comparator<KycContactEntity> {

        public int compare(KycContactEntity o1, KycContactEntity o2) {
            if (o1.getSortLetter().equals(SPECIAL_CHAR_TWO) || o2.getSortLetter().equals(SPECIAL_CHAR_ONE)) {
                return -1;
            } else if (o1.getSortLetter().equals(SPECIAL_CHAR_ONE) || o2.getSortLetter().equals(SPECIAL_CHAR_TWO)) {
                return 1;
            } else {
                return o1.getSortLetter().compareTo(o2.getSortLetter());
            }
        }

    }

    /**
     * 耗时任务读取通讯录
     */
    private class ReadContactsTask extends AsyncTask<Void, Void, ArrayList<KycContactEntity>> {
        private Context context;
        private String callingCode;
        private OnReadContactsSuccessCallBack onReadContactsSuccessCallBack;

        public ReadContactsTask(Context contextIn,String callingCode, OnReadContactsSuccessCallBack onReadContactsSuccessCallBackIn) {
            this.context = contextIn;
            this.callingCode = callingCode;
            this.onReadContactsSuccessCallBack = onReadContactsSuccessCallBackIn;
        }

        @Override
        protected ArrayList<KycContactEntity> doInBackground(Void... voids) {
            LogUtils.i(TAG, "doInBackground->");
            return getAllContact(context, false,callingCode);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtils.i(TAG, "onPreExecute->");
        }

        @Override
        protected void onPostExecute(ArrayList<KycContactEntity> contactEntities) {
            super.onPostExecute(contactEntities);
            LogUtils.i(TAG, "onPostExecute->");
            if (onReadContactsSuccessCallBack != null) {
                onReadContactsSuccessCallBack.OnReadContactsSuccess(contactEntities);
            }
        }
    }

    public interface OnReadContactsSuccessCallBack {
        void OnReadContactsSuccess(ArrayList<KycContactEntity> contactEntities);
    }
}
