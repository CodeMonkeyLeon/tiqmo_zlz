package cn.swiftpass.wallet.tiqmo.support.utils;

import android.Manifest;
import android.app.Activity;
import android.os.Build;

import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * 权限获取单例类
 */
public class PermissionInstance {

    private PermissionInstance() {
    }

    private static class PermissonHolder {
        public static PermissionInstance permissionInstance = new PermissionInstance();
    }

    public static PermissionInstance getInstance() {
        return PermissonHolder.permissionInstance;
    }

    public static String[] getPermissons(int type){
        if (Build.VERSION.SDK_INT >=33) {
            //安卓13
            switch (type){
                //相册
                case 1:
                    return new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_MEDIA_IMAGES};
                case 2:
                    return new String[]{Manifest.permission.READ_MEDIA_IMAGES};
                case 3:
                    return new String[]{Manifest.permission.POST_NOTIFICATIONS};
            }
        }else{
            switch (type){
                //相册
                case 1:
                    return new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
                case 2:
                    return new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
            }
        }
        return new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    }

    /**
     * 获取系统权限方法
     *
     * @param mActivity
     * @param permissions    多个权限string 例如 读取联系人权限 Manifest.permission.READ_CONTACTS
     * @param permissionCallback
     */
    public void getPermission(final Activity mActivity, final PermissionCallback permissionCallback, String... permissions) {
        new RxPermissions(mActivity).request(permissions).subscribe(new io.reactivex.functions.Consumer<Boolean>() {
            @Override
            public void accept(Boolean isAccept) throws Exception {
                //是否获取系统权限
                if (isAccept.booleanValue()) {
                    permissionCallback.acceptPermission();
                } else {
                    //拒绝获取系统权限  会弹出app本地权限框
                    permissionCallback.rejectPermission();
                }
            }
        });
    }

    public interface PermissionCallback {
        /**
         * 允许获取系统权限
         */
        void acceptPermission();

        /**
         * 拒绝获取系统权限
         */
        void rejectPermission();
    }
}
