package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class GetTimePresenter implements TransferContract.GetTimePresenter {

    TransferContract.GetTimeView getTimeView;

    @Override
    public void getTime(String ibanNo) {
        AppClient.getInstance().getTransferTime(ibanNo, new LifecycleMVPResultCallback<TimeEntity>(getTimeView, true) {
            @Override
            protected void onSuccess(TimeEntity result) {
                getTimeView.getTimeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                getTimeView.getTimeFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(getTimeView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                getTimeView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                getTimeView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(TransferContract.GetTimeView getTimeView) {
        this.getTimeView = getTimeView;
    }

    @Override
    public void detachView() {
        this.getTimeView = null;
    }
}
