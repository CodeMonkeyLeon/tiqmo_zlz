package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ViewAnimUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class VoucherCategoryAdapter extends BaseRecyclerAdapter<VouTypeAndInfoEntity.VouTypeAndInfo> {

    private Context context;
    private OnVoucherClickListener mVoucherClickListener;
    private OnVoucherMoneyClickListener onVoucherMoneyClickListener;

    public void setVoucherClickListener(OnVoucherClickListener mVoucherClickListener) {
        this.mVoucherClickListener = mVoucherClickListener;
    }

    public void setOnVoucherMoneyClickListener(OnVoucherMoneyClickListener onVoucherMoneyClickListener) {
        this.onVoucherMoneyClickListener = onVoucherMoneyClickListener;
    }

    public VoucherCategoryAdapter(Context context, @Nullable List<VouTypeAndInfoEntity.VouTypeAndInfo> data) {
        super(R.layout.item_voucher_category, data);
        this.context = context;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VouTypeAndInfoEntity.VouTypeAndInfo vouTypeAndInfo, int voucherCategory) {
        if (vouTypeAndInfo == null) {
            return;
        }
        LinearLayout llVoucherCategory = baseViewHolder.getView(R.id.ll_voucher_category);
        ImageView ivStoreLogo = baseViewHolder.getView(R.id.iv_store_logo);
        ImageView ivArrow = baseViewHolder.getView(R.id.iv_arrow);
        TextView tvCategory = baseViewHolder.getView(R.id.tv_voucher_category);
        RecyclerView ryVoucherAmount = baseViewHolder.getView(R.id.rv_voucher_amount);
        ryVoucherAmount.setVisibility(View.GONE);
        ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
        LocaleUtils.viewRotationY(mContext, ivArrow);
        if (!"Y".equals(vouTypeAndInfo.voucherType) && voucherCategory == 0) {
            ryVoucherAmount.setVisibility(View.VISIBLE);
            ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
        }
        String logo = ThemeUtils.isCurrentDark(mContext) ? vouTypeAndInfo.voucherTypeDarkLogoUrl : vouTypeAndInfo.voucherTypeLightLogoUrl;

        if (!TextUtils.isEmpty(logo)) {
            Glide.with(mContext).clear(ivStoreLogo);
            Glide.with(mContext)
                    .load(logo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivStoreLogo);
        }
        if (!TextUtils.isEmpty(vouTypeAndInfo.voucherTypeName)) {
            tvCategory.setText(vouTypeAndInfo.voucherTypeName);
        }
        VoucherAmountAdapter amountAdapter = new VoucherAmountAdapter(voucherCategory, vouTypeAndInfo.voucherInfos);
        GridLayoutManager manager = new GridLayoutManager(mContext, 2);
        ryVoucherAmount.setLayoutManager(manager);
        ryVoucherAmount.setAdapter(amountAdapter);
        amountAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int voucherPosition) {
                if (mVoucherClickListener != null) {
                    mVoucherClickListener.onVoucherClick(voucherCategory, voucherPosition);
                }
            }
        });
        int count = Math.min((vouTypeAndInfo.voucherInfos.size() + 1) >> 1 , 4);
        llVoucherCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Y".equals(vouTypeAndInfo.voucherType)) {
                    if (onVoucherMoneyClickListener != null) {
                        onVoucherMoneyClickListener.onVoucherMoneyClick(vouTypeAndInfo);
                    }
                } else {
                    ryVoucherAmount.measure(0, 0);
                    if (ryVoucherAmount.getVisibility() == View.VISIBLE) {
                        ViewAnimUtils.hideViewWithAnim(ryVoucherAmount, 100 * count,
                                ryVoucherAmount.getMeasuredHeight(), new ViewAnimUtils.AnimationEnd() {
                                    @Override
                                    public void onAnimationEnd() {
                                        ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                                    }
                                });
                    } else {
                        ViewAnimUtils.showViewWithAnim(ryVoucherAmount, 100 * count,
                                ryVoucherAmount.getMeasuredHeight());
                        ivArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                    }
                }
            }
        });
    }

    public interface OnVoucherClickListener {
        void onVoucherClick(int voucherCategory, int voucherPosition);
    }

    public interface OnVoucherMoneyClickListener {
        void onVoucherMoneyClick(VouTypeAndInfoEntity.VouTypeAndInfo vouTypeAndInfo);
    }
}
