package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ResponseEntity extends BaseEntity {
    public String payeeInfoId;
    public String orgNo;
    public String payerUserId;
    public String receiptMethod;
    public String receiptOrgCode;
    public String receiptOrgBranchCode;
    public String payeeFullName;
    public String nickName;
    public String relationshipCode;
    public String callingCode;
    public String phone;
    public String payeeInfoCountryCode;
    public String birthPlace;
    public String birthDate;
    public String sex;
    public String cityName;
    public String districtName;
    public String poBox;
    public String buildingNo;
    public String street;
    public String idNo;
    public String idExpiry;
    public String bankAccountType;
    public String ibanNo;
    public String bankAccountNo;
    public String remark;
    public String createTime;
}
