package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SelectOptionListEntity extends BaseEntity {

    private String key;
    private String value;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
