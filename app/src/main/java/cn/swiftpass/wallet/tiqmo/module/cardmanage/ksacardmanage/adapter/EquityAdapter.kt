package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter

import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardServiceEntity
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder

class EquityAdapter(mList: List<CardServiceEntity>) : BaseRecyclerAdapter<CardServiceEntity>(
    R.layout.item_equity,
    mList
) {


    override fun bindData(baseViewHolder: BaseViewHolder, item: CardServiceEntity?, position: Int) {
        item?.run {
            baseViewHolder.setImageResource(
                R.id.id_img,
                ResourceHelper.getInstance(mContext).getIdentifierByAttrId(imgId)
            )
            baseViewHolder.setText(R.id.id_tv, textId)
        }
    }
}