package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.HelpContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class HelpPresenter implements HelpContract.HelpListPresenter {

    HelpContract.HelpListView mHelpListView;

    @Override
    public void getHelpContentList(String content) {
        AppClient.getInstance().getHelpContentList(content, new LifecycleMVPResultCallback<HelpListEntity>(mHelpListView,true) {
            @Override
            protected void onSuccess(HelpListEntity result) {
                mHelpListView.getHelpContentListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mHelpListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void submitHelpQuestion(String id, String comment, String thumb) {
        AppClient.getInstance().submitHelpQuestion(id, comment, thumb, new LifecycleMVPResultCallback<Void>(mHelpListView,true) {
            @Override
            protected void onSuccess(Void result) {
                mHelpListView.submitHelpQuestionSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mHelpListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(HelpContract.HelpListView helpListView) {
        this.mHelpListView = helpListView;
    }

    @Override
    public void detachView() {
        this.mHelpListView = null;
    }
}
