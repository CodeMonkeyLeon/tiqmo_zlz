package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferHistoryListEntity extends BaseEntity {

    public List<TransferHistoryEntity> transactionListInfos = new ArrayList<>();
    public String dateDesc;
}
