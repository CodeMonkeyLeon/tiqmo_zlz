package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class  GetNewCardSuccessActivity extends BaseCompatActivity {
    
    public static final int type_get_virtual_card = 1;
    public static final int type_get_physical_card = 2;
    public static final int type_activate_card = 3;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_second_title)
    TextView tvSecondTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_result_image)
    ImageView ivResultImage;
    @BindView(R.id.tv_result_content)
    TextView tvResultContent;
    @BindView(R.id.tv_result_desc)
    TextView tvResultDesc;
    @BindView(R.id.tv_result_desc_two)
    TextView tvResultDescTwo;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_result_bottom)
    TextView tvResultBottom;

    private String deliveryTime;
    private int cardSuccessType;
    private String cardType;
    private KsaCardEntity ksaCardEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_get_card_success;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        ivBack.setVisibility(View.GONE);
        tvResultDesc.setVisibility(View.GONE);
//        tvResultDescTwo.setVisibility(View.GONE);
        ivBack.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_card_close));
        if(getIntent() != null && getIntent().getExtras() != null){
            cardType = getIntent().getExtras().getString(Constants.ksa_cardType);
            cardSuccessType = getIntent().getExtras().getInt(Constants.cardSuccessType);
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            deliveryTime = getIntent().getExtras().getString(Constants.ksa_deliveryTime);
            if(Constants.ksa_card_PHYSICAL.equals(cardType)){
                tvResultDesc.setVisibility(View.VISIBLE);
                tvResultDescTwo.setVisibility(View.GONE);
            }else{
                tvResultDescTwo.setVisibility(View.VISIBLE);
            }
            switch (cardSuccessType){
                case 1:break;
                case type_activate_card:
                    ivResultImage.setImageResource(AndroidUtils.getCardFaceDrawable(ksaCardEntity.cardFaceId,true));
                    tvResultContent.setText(getString(R.string.DigitalCard_85));
                    break;
                default:break;
            }
        }
        tvResultBottom.setText(Spans.builder().text(getString(R.string.sprint21_54)).color(mContext.getColor(R.color.color_a3a3a3)).size(12)
                .text(" " + getString(R.string.DigitalCard_39)).color(mContext.getColor(R.color.color_fc4f00)).size(12).click(tvResultBottom, new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("isHideTitle", true);
                        ActivitySkipUtil.startAnotherActivity((Activity) mContext, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setColor(mContext.getColor(R.color.color_fc4f00));
                        ds.setUnderlineText(false);
                    }
                })
                .build());
    }

    @Override
    public void onBackPressed() {
        return;
    }

    @OnClick(R.id.tv_confirm)
    public void onClick() {
        if(cardSuccessType == type_activate_card){
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_ACTIVATE));
        }
        ProjectApp.removeAllTaskExcludeMainStack();
        finish();
    }
}
