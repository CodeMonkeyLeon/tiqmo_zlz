package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class ExpireIdDialog extends BottomDialog{

    private Context mContext;

    private RenewIdListener closeListener;

    private TextView tvClose;
    private TextView tvTitle;
    private TextView tvContentOne;
    private ImageView ivRefCode;

    public void setCloseListener(RenewIdListener closeListener) {
        this.closeListener = closeListener;
    }

    public void setTitle(String message){
        if(tvTitle != null){
            if(!TextUtils.isEmpty(message)) {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(message);
            }else {
                tvTitle.setVisibility(View.GONE);
            }
        }
    }

    public void setContent(String left){
        if(tvContentOne != null){
            if(!TextUtils.isEmpty(left)) {
                tvContentOne.setVisibility(View.VISIBLE);
                tvContentOne.setText(left);
            }else{
                tvContentOne.setVisibility(View.GONE);
            }
        }
    }

    public void setConfirmText(String right){
        if(tvClose != null){
            if(!TextUtils.isEmpty(right)) {
                tvClose.setVisibility(View.VISIBLE);
                tvClose.setText(right);
            }else{
                tvClose.setVisibility(View.GONE);
            }
        }
    }
    public void setImage(int imageId){
        if(ivRefCode != null){
            ivRefCode.setImageResource(imageId);
        }
    }

    public ExpireIdDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface RenewIdListener {
        void clickRenew();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_expire_id, null);
        tvClose = view.findViewById(R.id.tv_renew_id);
        tvTitle = view.findViewById(R.id.tv_title);
        tvContentOne = view.findViewById(R.id.tv_content_one);
        ivRefCode = view.findViewById(R.id.iv_ref_code);

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (closeListener != null) {
                    closeListener.clickRenew();
                }
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
