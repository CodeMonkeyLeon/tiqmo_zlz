package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MsgBizParamEntity extends BaseEntity {

    /**
     * 卡过期消息专用字段
     */
    public String orderNo;
    //4位卡号尾数
    public String maskedCardNo;
    public String cardHolderName;
    //如果这里不为空  则打开webview
    public String targetURL;
    //如果为splitBill  则跳转splitBill页面
    public String targetPage;
}
