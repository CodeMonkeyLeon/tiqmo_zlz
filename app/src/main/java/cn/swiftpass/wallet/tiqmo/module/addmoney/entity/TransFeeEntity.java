package cn.swiftpass.wallet.tiqmo.module.addmoney.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class TransFeeEntity extends BaseEntity {

    /**
     * 交易手续费
     */
    private String transFees;
    /**
     * 折后交易手续费
     */
    private String discountTransFees = "0";
    /**
     * 增值税
     */
    private String vat;
    /**
     * 折后增值税
     */
    private String discountVat  = "0";
    private String totalAmount;
    private String orderAmount;
    public String orderCurrencyCode;

    public String getTransFees() {
        return AndroidUtils.getTransferMoney(transFees);
    }

    public String getTotalAmount() {
        return AndroidUtils.getTransferMoney(totalAmount);
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public void setTransFees(String transFees) {
        this.transFees = transFees;
    }

    public String getDiscountTransFees() {
        return AndroidUtils.getTransferMoney(discountTransFees);
    }

    public void setDiscountTransFees(String discountTransFees) {
        this.discountTransFees = discountTransFees;
    }

    public String getVat() {
        return AndroidUtils.getTransferMoney(vat);
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getDiscountVat() {
        return AndroidUtils.getTransferMoney(discountVat);
    }

    public void setDiscountVat(String discountVat) {
        this.discountVat = discountVat;
    }
}
