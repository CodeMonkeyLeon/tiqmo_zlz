package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillCountryAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListNewEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillCountryPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;
import cn.swiftpass.wallet.tiqmo.widget.statusview.StatusView;

public class PayBillCountryActivity extends BaseCompatActivity<PayBillContract.BillCountryPresenter> implements PayBillContract.BillCountryView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_country_list)
    RecyclerView rvCountryList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_no_result_found)
    LinearLayout llNoResultFound;
//    @BindView(R.id.sc_content)
//    NestedScrollView scContent;

    private PayBillCountryAdapter payBillCountryAdapter;

    private List<PayBillCountryEntity> showCountryList = new ArrayList<>();
    private List<PayBillCountryEntity> filterCountryList = new ArrayList<>();

    private String searchString;

    private StatusView typeStatusView;
    private ImageView ivNoTransaction;
    private View noHistoryView;


    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_select_country;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_2);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        noHistoryView = LayoutInflater.from(mContext).inflate(R.layout.view_no_content_no_button, null);
        ivNoTransaction = noHistoryView.findViewById(R.id.iv_no_content);
        ivNoTransaction.setVisibility(View.GONE);
        TextView tvNoContent = noHistoryView.findViewById(R.id.tv_no_content);
        tvNoContent.setText(getString(R.string.BillPay_37));
        typeStatusView = new StatusView.Builder(mContext, rvCountryList).setNoContentMsg(getString(R.string.BillPay_37))
                .setNoContentView(noHistoryView).build();

        etSearch.setHint(getString(R.string.BillPay_3));
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvCountryList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvCountryList.setLayoutManager(manager);
        payBillCountryAdapter = new PayBillCountryAdapter(showCountryList);
        payBillCountryAdapter.bindToRecyclerView(rvCountryList);
        payBillCountryAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                PayBillCountryEntity payBillCountryEntity = payBillCountryAdapter.getDataList().get(position);
                if (payBillCountryEntity != null) {
                    String countryCode = payBillCountryEntity.countryCode;
                    String countryType = payBillCountryEntity.countryType;
                    HashMap<String, Object> mHashMap = new HashMap<>();
                    mHashMap.put(Constants.payBillCountryEntity, payBillCountryEntity);
                    if (getString(R.string.BillPay_4).equals(countryType)) {
                        if (mPresenter != null) {
                            String channelCode = AndroidUtils.getChannelCode(countryType, getString(R.string.BillPay_4));
                            mPresenter.getPayBillCategory(countryCode, channelCode);
                        }
                    } else {
                        ActivitySkipUtil.startAnotherActivity(PayBillCountryActivity.this, PayBillTypeActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
//                    ActivitySkipUtil.startAnotherActivity(PayBillCountryActivity.this, PayBillTypeActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchString = s.toString();
                if (showCountryList != null && showCountryList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });

        mPresenter.getPayBillCountry();
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            filterCountryList.clear();
            llNoResultFound.setVisibility(View.GONE);
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
//                sideBar.setVisibility(View.VISIBLE);
                filterCountryList.clear();
                payBillCountryAdapter.setDataList(showCountryList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (showCountryList != null && showCountryList.size() > 0) {
                int size = showCountryList.size();
                for (int i = 0; i < size; i++) {
                    PayBillCountryEntity payBillCountryEntity = showCountryList.get(i);
                    String name = payBillCountryEntity.countryName;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterCountryList.add(payBillCountryEntity);
                    }
                }
                payBillCountryAdapter.setDataList(filterCountryList);
                if (filterCountryList.size() == 0) {
                    llNoResultFound.setVisibility(View.VISIBLE);
                }else {
                    llNoResultFound.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void getPayBillCountrySuccess(PayBillCountryListEntity payBillCountryListEntity) {
        if (payBillCountryListEntity != null) {
            List<PayBillCountryEntity> countryList = payBillCountryListEntity.countryList;
            if (countryList != null && countryList.size() > 0) {
                showStatusView(typeStatusView, StatusView.CONTENT_VIEW);
                showCountryList.clear();
                int size = countryList.size();
                for (int i = 0; i < size; i++) {
                    PayBillCountryEntity payBillCountryEntity = countryList.get(i);
                    if (i == 0) {
                        //后台返回默认第一个是本国数据
                        payBillCountryEntity.countryType = getString(R.string.BillPay_4);
                    } else {
                        payBillCountryEntity.countryType = getString(R.string.BillPay_5);
                    }
                    showCountryList.add(payBillCountryEntity);
                }

                payBillCountryAdapter.setDataList(showCountryList);
                if (showCountryList.size() == 0) {
                    llNoResultFound.setVisibility(View.VISIBLE);
                }
            } else {
                showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
            }
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showStatusView(typeStatusView, StatusView.NO_CONTENT_VIEW);
        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillCountryPresenter getPresenter() {
        return new PayBillCountryPresenter();
    }

    @Override
    public void getPayBillCategorySuccess(PayBillTypeListNewEntity payBillTypeListNewEntity, String countryCode, String channelCode) {

        if (payBillTypeListNewEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.payBillCountryEntity, payBillTypeListNewEntity);
            mHashMap.put(Constants.COUNTRY_CODE, countryCode);
            mHashMap.put(Constants.CHANNEL_CODE, channelCode);
            ActivitySkipUtil.startAnotherActivity(PayBillCountryActivity.this, PayBillLocalActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }
}
