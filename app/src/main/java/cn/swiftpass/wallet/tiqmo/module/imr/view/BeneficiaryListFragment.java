package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.BeneficiaryListAdapter;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.BeneficiaryListContract;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.BeneficiaryListPresenter;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrDeclarationDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrRemoveBeneficiaryDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class BeneficiaryListFragment extends BaseFragment<BeneficiaryListContract.Presenter> implements BeneficiaryListContract.View {

    RecyclerView ryBeneficiaryList;

    private ImrExchangeRateEntity imrExchangeRateEntity;
    private ImageView ivNoBeneficiary;
    private TextView tvNoBeneficiary;
    private SwipeRefreshLayout swBeneficiaryList;
    private ImrBeneficiaryListEntity.ImrBeneficiaryBean imrBeneficiaryBean;
    private RiskControlEntity riskControlEntity;
    private int currentPosition,relationPosition;
    private BeneficiaryListAdapter beneficiaryListAdapter;

    private ImrBeneficiaryEntity mImrBeneficiaryEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_beneficiary_list;
    }

    @Override
    protected void initView(View parentView) {
        ryBeneficiaryList = parentView.findViewById(R.id.rv_beneficiary_list);
        ivNoBeneficiary = parentView.findViewById(R.id.iv_no_beneficiary);
        tvNoBeneficiary = parentView.findViewById(R.id.tv_no_beneficiary);
        swBeneficiaryList = parentView.findViewById(R.id.sw_beneficiary_list);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        ryBeneficiaryList.setLayoutManager(manager);

        swBeneficiaryList.setColorSchemeResources(R.color.color_B00931);
        swBeneficiaryList.setRefreshing(false);
        swBeneficiaryList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBeneficiaryList(false);
            }
        });
    }

    @Override
    protected void initData() {
        getBeneficiaryList(true);
    }

    @Override
    protected void restart() {
    }

    public void getBeneficiaryList(boolean isInit) {
        mPresenter.getBeneficiaryList(isInit);
    }

    @Override
    public BeneficiaryListContract.Presenter getPresenter() {
        return new BeneficiaryListPresenter();
    }

    @OnClick({R.id.tv_add_beneficiary})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
//            case R.id.iv_icon_add:
            case R.id.tv_add_beneficiary:
                ImrDeclarationDialog dialog = ImrDeclarationDialog.getSingleton();
                dialog.setOnAgreeClickListener(new ImrDeclarationDialog.OnAgreeClickListener() {
                    @Override
                    public void onAgreeClick() {
                        ActivitySkipUtil.startAnotherActivity(mActivity, AddBeneficiaryOneActivity.class,
                                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                });
                if (getFragmentManager() != null) {
                    dialog.showNow(getFragmentManager(), "declaration");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void showBeneficiaryList(List<ImrBeneficiaryListEntity.ImrBeneficiaryInfo> imrPayeeInfos) {
        ryBeneficiaryList.setVisibility(View.VISIBLE);
        ivNoBeneficiary.setVisibility(View.GONE);
        tvNoBeneficiary.setVisibility(View.GONE);
        swBeneficiaryList.setRefreshing(false);
        beneficiaryListAdapter = new BeneficiaryListAdapter(imrPayeeInfos);
        beneficiaryListAdapter.setOnViewClickListener(new BeneficiaryListAdapter.OnViewClickListener() {
            @Override
            public void onItemDeleteClick(int relationship, int position) {
                ImrRemoveBeneficiaryDialog dialog = ImrRemoveBeneficiaryDialog.getInstance(
                        imrPayeeInfos.get(relationship).imrPayeeDetailInfos.get(position));
                dialog.setOnDialogClickListener(new ImrRemoveBeneficiaryDialog.OnDialogClickListener() {
                    @Override
                    public void onRemoveClick() {
                        mPresenter.removeBeneficiary(imrPayeeInfos.get(relationship)
                                .imrPayeeDetailInfos.get(position).payeeInfoId);
                    }
                });
                if (getFragmentManager() != null) {
                    dialog.show(getFragmentManager(), "removeBeneficiary");
                }
            }

            @Override
            public void onItemEditClick(int relationship, int position) {
                mPresenter.getImrBeneficiaryDetail(imrPayeeInfos.get(relationship)
                        .imrPayeeDetailInfos.get(position));
            }

            @Override
            public void onItemSendClick(int relationship, int position) {
                imrBeneficiaryBean = imrPayeeInfos.get(relationship)
                        .imrPayeeDetailInfos.get(position);
                if (imrBeneficiaryBean != null) {
                    if(imrBeneficiaryBean.isIvrActivite()) {
                        mPresenter.imrExchangeRate(imrBeneficiaryBean.payeeInfoId, "", "", imrBeneficiaryBean.channelCode);
                    }else{
                        currentPosition = position;
                        relationPosition = relationship;
                        List<String> additionalData = new ArrayList<>();
                        mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                    }
                }

            }

            @Override
            public void onItemDeleteShow(int relationship, int position, boolean isShow) {
                for (int i = 0; i < imrPayeeInfos.size(); i++) {
                    for (int j = 0; j < imrPayeeInfos.get(i).imrPayeeDetailInfos.size(); j++) {
                        imrPayeeInfos.get(i).imrPayeeDetailInfos.get(j).setShowRemoveIcon(relationship == i && position == j && isShow);
                    }
                }
                if (ryBeneficiaryList.isComputingLayout()) {
                    beneficiaryListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onItemBenOPShow(int relationship, int position, boolean isShow) {
                imrPayeeInfos.get(relationship).imrPayeeDetailInfos.get(position).isShowBeneficiaryOP = isShow;
                beneficiaryListAdapter.notifyDataSetChanged();
            }
        });
        ryBeneficiaryList.setAdapter(beneficiaryListAdapter);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        swBeneficiaryList.setRefreshing(false);
        showTipDialog(errorMsg);
    }

    @Override
    public void showRemoveBeneficiarySuccess() {

    }

    @Override
    public void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity) {
        if (imrExchangeRateEntity != null) {
            this.imrExchangeRateEntity = imrExchangeRateEntity;
            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
        }
    }

    @Override
    public void imrExchangeRateFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        mHashMap.put(Constants.imrExchangeRateEntity, imrExchangeRateEntity);
        mHashMap.put(Constants.ImrBeneficiaryBean, imrBeneficiaryBean);
        ActivitySkipUtil.startAnotherActivity(mActivity, ImrSendMoneyActivity.class, mHashMap,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void showSendMoneyFragment() {
        if (getActivity() != null && getActivity() instanceof ImrMainActivity) {
            ((ImrMainActivity) getActivity()).setViewPageIndex(LocaleUtils.isRTL(mContext) ? 0 : 1);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_SEND_BENEFICIARY == event.getEventType()) {
            mPresenter.activateBeneficiary(imrBeneficiaryBean.payeeInfoId);
        }else if(EventEntity.EVENT_ADD_BENEFICIARY == event.getEventType()){
            if(mImrBeneficiaryEntity != null) {
                mPresenter.activateBeneficiary(mImrBeneficiaryEntity.payeeInfoId);
            }else{
                mPresenter.getBeneficiaryList(false);
            }
        }
    }

    @Override
    public void showNoBeneficiary() {
        ryBeneficiaryList.setVisibility(View.GONE);
        ivNoBeneficiary.setVisibility(View.VISIBLE);
        tvNoBeneficiary.setVisibility(View.VISIBLE);
        swBeneficiaryList.setRefreshing(false);
    }

    @Override
    public void activateBeneficiarySuccess(Void result) {
//        try {
//            beneficiaryListAdapter.getDataList().get(relationPosition)
//                    .imrPayeeDetailInfos.get(currentPosition).activeState = "1";
//            beneficiaryListAdapter.notifyDataSetChanged();
            mPresenter.getBeneficiaryList(false);
//        }catch (Throwable e){
//            e.getStackTrace();
//        }
    }

    @Override
    public void startEditView(ImrBeneficiaryDetails result) {
//        ActivitySkipUtil.startAnotherActivity(mContext, AddBeneficiaryTwoActivity.class, "", result);
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), AddBeneficiaryOneActivity.class);
            intent.putExtra(Constants.IS_FROM_EDIT_BENEFICIARY, true);
            intent.putExtra("BeneficiaryInfo", result.imrPayeeDetailInfo);
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if(rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            addBeneficiaryIvr(false,riskControlEntity,null);
        }
    }

    public void addBeneficiaryIvr(boolean isAddNew, RiskControlEntity mRiskControlEntity, ImrBeneficiaryEntity imrBeneficiaryEntity){
        if(imrBeneficiaryEntity != null){
            this.mImrBeneficiaryEntity = imrBeneficiaryEntity;
        }
        if(mRiskControlEntity != null) {
            this.riskControlEntity = mRiskControlEntity;
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            setTransferMap(mHashMap,isAddNew);
            ActivitySkipUtil.startAnotherActivity(mActivity, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap,boolean isAddNew){
        if(isAddNew) {
            mHashMap.put(Constants.sceneType, Constants.TYPE_IMR_ADD_BENEFICIARY);
        }else{
            mHashMap.put(Constants.sceneType, Constants.TYPE_SEND_BENEFICIARY);
        }
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtw_1));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);

    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
