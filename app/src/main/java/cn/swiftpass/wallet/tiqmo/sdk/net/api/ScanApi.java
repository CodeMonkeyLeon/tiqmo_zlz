//package cn.swiftpass.wallet.tiqmo.sdk.net.api;
//
//import cn.swiftpass.wallet.sdk.entity.CheckScanInfoEntity;
//import cn.swiftpass.wallet.sdk.entity.JsonResponse;
//import cn.swiftpass.wallet.sdk.net.RequestCall;
//import cn.swiftpass.wallet.sdk.net.annotation.Headers;
//import cn.swiftpass.wallet.sdk.net.annotation.Param;
//
//
///**
// * Created by 叶智星 on 2018年10月10日.
// * 每一个不曾起舞的日子，都是对生命的辜负。
// */
//public interface ScanApi {
//    @Headers({"Service-Id:1044"})
//    RequestCall<JsonResponse<CheckScanInfoEntity>> checkScanInfo(@Param("scanInfo") String scanInfo);
//}
