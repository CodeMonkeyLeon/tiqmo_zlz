package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;

import com.bottomnavigation.MeowBottomNavigation;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.CardManageKsaFragment;
import cn.swiftpass.wallet.tiqmo.module.chat.view.ChatFragment;
import cn.swiftpass.wallet.tiqmo.module.guide.act.GuideActivity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.kyc.dialog.ReferralCodeDialogUtils;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AddCardFailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AddCardSuccessActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.CardManageActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycSuccessActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.TiqmoLaunchSDK;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BiometricInstance;
import cn.swiftpass.wallet.tiqmo.support.utils.KotlinUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.dialog.TouchIdDilaog;

public class MainActivity extends BaseCompatActivity {

    private String TAG = "MainActivity";

    private boolean mIsFirstShowHistory = true;
    public final static int ID_WITH_DRAW = 1;
    public final static int ID_BUDGET = 2;
    public final static int ID_HOME = 3;
    public final static int ID_HISTORY = 4;
    public final static int ID_CARD_HOME = 5;

    public static final String key_with_draw = "key_with_draw";
    public static final String key_budget = "key_budget";
    public static final String key_home = "key_home";
    public static final String key_history = "key_history";
    public static final String key_card_ksa = "key_card_ksa";
    public static final String key_coming_soon = "key_coming_soon";

    public static final String navigation_bar_with_draw = "with_draw";
    public static final String navigation_bar_coming_soon = "coming_soon";
    public static final String navigation_bar_budget = "budget";
    public static final String navigation_bar_home = "home";
    public static final String navigation_bar_history = "history";
    public static final String navigation_bar_card_home = "card_home";

    //    public WithDrawFragment withDrawFragment;
    //    public BudgetFragment budgetFragment;
    public AnalyticsFragment analyticsFragment;
    //    public HomeFragment homeFragment;
    public HomePageFragment homeFragment;
    public HistoryFragment historyFragment;
    //    public CardHomeFragment cardHomeFragment;
    public CardManageKsaFragment cardManageKsaFragment;

    public ChatFragment chatFragment;

    @BindView(R.id.maintabcontent)
    FrameLayout maintabcontent;
    @BindView(R.id.bottom_nav)
    MeowBottomNavigation bottomNav;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;

    private FragmentManager fragmentManager;
    private String currentNavigationTag;

    private boolean isFromReg = false;
    //退出时的时间
    private long mExitTime;
    private int typeId = ID_HOME;

    private int msgType;

//    private BottomDialog bottomDialog;

    private int currentId = ID_HOME;

    private ArrayList<MenuItem> tabImgageViews = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        TiqmoLaunchSDK.saveAppVersion(mContext);
        if (getConfigEntity() != null) {
            TiqmoLaunchSDK.showUpdateIfNeeded(mContext, getConfigEntity());
        }
        startAD();
        SpUtils.getInstance().setKycChecking(false);
        if (getIntent() != null && getIntent().getExtras() != null) {
            isFromReg = getIntent().getExtras().getBoolean(Constants.LOGIN_FROM);
            msgType = getIntent().getIntExtra(Constants.MSGTYPE, -100);
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            CardNotifyEntity cardNotifyEntity;
            switch (msgType) {
                case -1:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_CARD_ENTITY);
                    mHashMaps.put(Constants.MSGTYPE, msgType);
                    mHashMaps.put(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
                    if ("000000".equals(ProjectApp.getCardNotifyEntity().respCode)) {
                        ActivitySkipUtil.startAnotherActivity(MainActivity.this, AddCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                    } else {
                        ActivitySkipUtil.startAnotherActivity(MainActivity.this, AddCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                    }
                    break;
                case -2:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.ADD_MONEY_ENTITY);
                    mHashMaps.put(Constants.MSGTYPE, msgType);
                    mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
                    ActivitySkipUtil.startAnotherActivity(MainActivity.this, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                    break;
                case -3:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.REQUEST_TRANSFER_ENTITY);
                    showRequestDialog(cardNotifyEntity);
                    break;
                case -4:
                    cardNotifyEntity = (CardNotifyEntity) getIntent().getExtras().getSerializable(Constants.MSG_ENTITY);
                    if (Constants.NOTIFY_SUBJECT_MARKETING_ACTIVITY.equals(cardNotifyEntity.subject)) {
                        String activityNo = cardNotifyEntity.activityNo;
                        String activityType = cardNotifyEntity.activityType;
                        showMarketActivityDetail(activityNo, activityType);
                    } else {
                        showNotifyDialog(cardNotifyEntity);
                    }
                    break;
                default:
                    break;
            }

            if (TextUtils.isEmpty(SpUtils.getInstance().isFirstTopUp()) || "true".equals(SpUtils.getInstance().isFirstTopUp())) {
                List<String> tradeList = new ArrayList<>();
                tradeList.add("10");
                AppClient.getInstance().getTransferMainHistory("", "", new ArrayList<>(),
                        tradeList, "", "", 30, 1, "", "", new ResultCallback<TransferHistoryMainEntity>() {
                            @Override
                            public void onResult(TransferHistoryMainEntity transferHistoryMainEntity) {
                                if (transferHistoryMainEntity != null) {
                                    if (transferHistoryMainEntity.transactionHistorys.isEmpty()) {
                                        SpUtils.getInstance().setFirstTopUp("true");
                                    } else {
                                        SpUtils.getInstance().setFirstTopUp("false");
                                    }
                                }
                            }

                            @Override
                            public void onFailure(String errorCode, String errorMsg) {

                            }
                        });
            }
        }

        //判断用户ID是否过期
        if (getUserInfoEntity() != null) {
            if ("Y".equals(getUserInfoEntity().userIdCardExpiredFlag)) {
//                showTipDialog(getUserInfoEntity().userIdCardExpiredMessage, 5000);
                showExpireIdDialog();
            } else if ("I".equals(getUserInfoEntity().userIdCardExpiredFlag) || "YG".equals(getUserInfoEntity().userIdCardExpiredFlag)) {
                showTipDialog(getUserInfoEntity().userIdCardExpiredMessage, 5000);
            }
        }

        //成功登录
        AppsFlyerHelper.getInstance().sendLoginEvent(getUserInfoEntity());

        UxcamHelper.getInstance().startUxcam();
        UxcamHelper.getInstance().sendUxcamUserInfo(getUserInfoEntity());
        if (isFromReg) {
            showTouchIdDialog();
        }
        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState != null) {
//            withDrawFragment = (WithDrawFragment) fragmentManager.getFragment(savedInstanceState, key_with_draw);
//            budgetFragment = (BudgetFragment) fragmentManager.getFragment(savedInstanceState, key_budget);
            analyticsFragment = (AnalyticsFragment) fragmentManager.getFragment(savedInstanceState, key_budget);
            homeFragment = (HomePageFragment) fragmentManager.getFragment(savedInstanceState, key_home);
            historyFragment = (HistoryFragment) fragmentManager.getFragment(savedInstanceState, key_history);
            cardManageKsaFragment = (CardManageKsaFragment) fragmentManager.getFragment(savedInstanceState, key_card_ksa);

            chatFragment = (ChatFragment) fragmentManager.getFragment(savedInstanceState, key_coming_soon);
        } else {
            setupTabIndicator();
        }

//        bottomNav.add(new MeowBottomNavigation.Model(ID_ADD_MONEY, R.drawable.ic_d_add_money,
//                getString(R.string.Tiqmo_Home_V1_Copy_16_12)));
//        bottomNav.add(new MeowBottomNavigation.Model(ID_TRANSFER, R.drawable.ic_d_transfer,
//                getString(R.string.Tiqmo_Home_V1_Copy_16_13)));
//        bottomNav.add(new MeowBottomNavigation.Model(ID_HOME, R.drawable.ic_d_home,
//                getString(R.string.Tiqmo_Home_V1_Copy_16_14)));
//        bottomNav.add(new MeowBottomNavigation.Model(ID_HISTORY, R.drawable.ic_d_history,
//                getString(R.string.Tiqmo_Home_V1_Copy_16_15)));

//        bottomNav.add(new MeowBottomNavigation.Model(ID_WITH_DRAW, ThemeSourceUtils.getSourceID(mContext, R.attr.home_withdraw),
//                getString(R.string.newhome_25)));

        bottomNav.add(new MeowBottomNavigation.Model(ID_WITH_DRAW, ThemeSourceUtils.getSourceID(mContext, R.attr.home_chat),
                getString(R.string.newhome_23)));
        bottomNav.add(new MeowBottomNavigation.Model(ID_BUDGET, ThemeSourceUtils.getSourceID(mContext, R.attr.home_budget),
                getString(R.string.newhome_24)));
//        bottomNav.add(new MeowBottomNavigation.Model(ID_HOME, R.drawable.l_home_icon,
//                getString(R.string.Tiqmo_Home_V1_Copy_16_14)));
        bottomNav.add(new MeowBottomNavigation.Model(ID_HOME, ThemeSourceUtils.getSourceID(mContext, R.attr.home_home),
                getString(R.string.Tiqmo_Home_V1_Copy_16_14)));
        bottomNav.add(new MeowBottomNavigation.Model(ID_HISTORY, ThemeSourceUtils.getSourceID(mContext, R.attr.home_history),
                getString(R.string.Tiqmo_Home_V1_Copy_16_15)));

        bottomNav.add(new MeowBottomNavigation.Model(ID_CARD_HOME, ThemeSourceUtils.getSourceID(mContext, R.attr.home_card),
                getString(R.string.virtualCard_0)));
        bottomNav.show(ID_HOME, false);

        bottomNav.setOnClickMenuListener(model -> {
            int id = model.getId();
            int value = Math.abs(id - currentId);
            LogUtils.d(TAG, "model.getId(): " + model.getId());
            long time = 0;
            switch (value) {
                case 1:
                    time = 300;
                    break;
                case 2:
                    time = 400;
                    break;
                case 3:
                    time = 500;
                    break;
                case 4:
                    time = 600;
                    break;
                default:
                    time = 700;
                    break;
            }
            ProjectApp.getInstance().getMainHandler().post(new Runnable() {
                @Override
                public void run() {
                    changeBottom(id);
                }
            });
            return null;
        });

        parseIntent(getIntent());

        bottomNav.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        if (SpUtils.getInstance().getIsFirstLaunch() && !KotlinUtils.INSTANCE.isSupportGooglePlay()) {
            //首次安装启动，并且不支持google play的手机首次安装，弹框邀请码
            SpUtils.getInstance().setIsFirstLaunch(false);
            ReferralCodeDialogUtils.showReferralCodeNotSupportGooglePlayDialog(this, MainActivity.this);
        }

    }

    private void setupTabIndicator() {
//        withDrawFragment = WithDrawFragment.getInstance();
        chatFragment = ChatFragment.newInstance();
        analyticsFragment = AnalyticsFragment.getInstance(0, false);
//        budgetFragment = BudgetFragment.getInstance();
        homeFragment = HomePageFragment.getInstance();
        historyFragment = HistoryFragment.getInstance(null, "");
        cardManageKsaFragment = CardManageKsaFragment.getInstance();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        addFragment(transaction, withDrawFragment, navigation_bar_with_draw);
//        addFragment(transaction, budgetFragment, navigation_bar_budget);
        addFragment(transaction, chatFragment, navigation_bar_coming_soon);
        addFragment(transaction, analyticsFragment, navigation_bar_budget);
        addFragment(transaction, historyFragment, navigation_bar_history);
        addFragment(transaction, homeFragment, navigation_bar_home);
        addFragment(transaction, cardManageKsaFragment, navigation_bar_card_home);
        transaction.commitNow();

        currentNavigationTag = navigation_bar_home;
    }

    @Override
    protected void onResume() {
        super.onResume();
        UxcamHelper.getInstance().hideScreen(false);
        userInfoEntity = getUserInfoEntity();
        if (userInfoEntity.isKycHigh()) {
            showLogoutDialog();
            return;
        }
        if (ProjectApp.kycSuccess) {
//            AppClient.getInstance().getUserManager().getUserInfo().imrSetUpFlag = "N";
            ActivitySkipUtil.startAnotherActivity(this, KycSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.kycSuccess = false;
            GuideActivity.startGuideActivity(MainActivity.this);
        }
        if (ProjectApp.changePhoneSuccess) {
            showNafathErrorDialog("4");
            ProjectApp.changePhoneSuccess = false;
        }

        if (ProjectApp.addMoneySuccessOnMain) {
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            mHashMaps.put(Constants.MSGTYPE, msgType);
            mHashMaps.put(Constants.ADD_MONEY_ENTITY, ProjectApp.getCardNotifyEntity());
            ActivitySkipUtil.startAnotherActivity(MainActivity.this, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.addMoneySuccessOnMain = false;
        }

        if (ProjectApp.addCardSuccess) {
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            mHashMaps.put(Constants.MSGTYPE, msgType);
            mHashMaps.put(Constants.ADD_CARD_ENTITY, ProjectApp.getCardNotifyEntity());
            if ("000000".equals(ProjectApp.getCardNotifyEntity().respCode)) {
                ActivitySkipUtil.startAnotherActivity(MainActivity.this, AddCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                ActivitySkipUtil.startAnotherActivity(MainActivity.this, AddCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            }
            ProjectApp.addCardSuccess = false;
        }

        if (ProjectApp.requestTransferSuccess) {
            showRequestDialog(ProjectApp.getCardNotifyEntity());
            ProjectApp.requestTransferSuccess = false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //进入主页后清除注册页面的信息
        UserInfoManager.getInstance().setCheckPhoneEntity(null);
        if (ProjectApp.changeTheme) {
            ThemeUtils.loadingCurrentTheme(mContext, true);
            ProjectApp.changeTheme = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        parseIntent(intent);
    }

    private void parseIntent(Intent intent) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        if (null != intent) {
            //绑卡成功
            int msgType = intent.getIntExtra(Constants.MSGTYPE, -100);
            CardNotifyEntity cardNotifyEntity = null;
            if (msgType == -1) {
                if (intent.getSerializableExtra(Constants.ADD_CARD_ENTITY) != null) {
                    cardNotifyEntity = (CardNotifyEntity) intent.getSerializableExtra(Constants.ADD_CARD_ENTITY);
                }
                mHashMaps.put(Constants.MSGTYPE, msgType);
                mHashMaps.put(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
                ActivitySkipUtil.startAnotherActivity(MainActivity.this, CardManageActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == AddMoneyFragment.LOAD_PAYMENT_DATA_REQUEST_CODE) {
//            if (withDrawFragment != null) {
//                withDrawFragment.onActivityResult(requestCode, resultCode, data);
//            }
        if (requestCode == BaseHistoryFragment.FILTER_REQUEST) {
            if (historyFragment != null) {
                historyFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void showTouchIdDialog() {
        if (!ProjectApp.isPhoneNotSupportedFace()) {
            TouchIdDilaog touchIdDilaog = new TouchIdDilaog(mContext);
            touchIdDilaog.setAcceptListener(new TouchIdDilaog.AcceptListener() {
                @Override
                public void clickAccept() {
                    if (touchIdDilaog != null) {
                        touchIdDilaog.dismiss();
                    }
                    if (ProjectApp.isBiometricSupported()) {
                        BiometricInstance.getInstance().registerBiometric(MainActivity.this, true, true,
                                new BiometricInstance.BiometricListener() {
                                    @Override
                                    public void onBiometricFail(String errMsg) {
                                        showTipDialog(errMsg);
                                    }

                                    @Override
                                    public void onBiometricSuccess() {

                                    }

                                    @Override
                                    public void onBiometricError() {

                                    }
                                });
                    } else {
                        showErrorDialog(getString(R.string.common_22));
                    }
                }

                @Override
                public void clickCancel() {
                    if (touchIdDilaog != null) {
                        touchIdDilaog.dismiss();
                    }
                    showProgress(true);
                    AppClient.getInstance().notUseFinger(Constants.NO_USE_FINGER_LOGIN, new ResultCallback<Void>() {
                        @Override
                        public void onResult(Void response) {
                            showProgress(false);
                        }

                        @Override
                        public void onFailure(String errorCode, String errorMsg) {
                            showProgress(false);
                            showTipDialog(errorMsg);
                        }
                    });
                }
            });
//            touchIdDilaog.show();
            touchIdDilaog.showWithBottomAnim();
        }
    }

    public void showBottom(int id) {
        if (bottomNav != null) {
            MeowBottomNavigation.Model model = bottomNav.getModelById(id);
            if (model != null) {
                changeBottom(model.getId());
                bottomNav.show(id, true);
            }
        }
    }

    //对返回键进行监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            mExitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }

    private void changeBottom(int id) {
        userInfoEntity = getUserInfoEntity();
        currentId = id;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        switch (id) {
            case ID_WITH_DRAW:
                if (AndroidUtils.isCloseUse(this, Constants.tab_chat)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                    return;
                }
                if (userInfoEntity.isKycChecking()) {
                    ActivitySkipUtil.startAnotherActivity(MainActivity.this, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else if (userInfoEntity.isKycLow()) {
                    ReferralCodeDialogUtils.showReferralCodeDialog(this, MainActivity.this);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else if (userInfoEntity.isUserIdCardExpried()) {
                    showExpireIdDialog();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else {
                    typeId = id;
                    showFragment(transaction, chatFragment);
                }
                break;
            case ID_BUDGET:
                if (AndroidUtils.isCloseUse(this, Constants.tab_budget)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                    return;
                }
                if (userInfoEntity.isKycChecking()) {
                    ActivitySkipUtil.startAnotherActivity(MainActivity.this, KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else if (userInfoEntity.isKycLow()) {
                    ReferralCodeDialogUtils.showReferralCodeDialog(this, MainActivity.this);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else if (userInfoEntity.isUserIdCardExpried()) {
                    showExpireIdDialog();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else {
                    typeId = id;
                    showFragment(transaction, analyticsFragment);
                }
                break;

            case ID_HOME:
                typeId = id;
                showFragment(transaction, homeFragment);
                break;
            case ID_HISTORY:
                if (AndroidUtils.isCloseUse(this, Constants.tab_history)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                    return;
                }
                typeId = id;
                //首次进入交易历史无需刷新数据，因为交易历史界面初始化时会自动刷新
                //非首次进入交易历史需要把所有筛选数据清空，刷新数据
                if (!mIsFirstShowHistory) {
                    EventBus.getDefault().postSticky(
                            new EventEntity(
                                    EventEntity.EVENT_HISTORY_INIT_DATA,
                                    "transaction initData"
                            )
                    );
                }
                mIsFirstShowHistory = false;
                showFragment(transaction, historyFragment);
                break;
            case ID_CARD_HOME:
                if (AndroidUtils.isCloseUse(this, Constants.tab_cards)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                    return;
                }
                if (userInfoEntity.isUserIdCardExpried()) {
                    showExpireIdDialog();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bottomNav.show(typeId, false);
                        }
                    }, 300);
                } else {
                    typeId = id;
                    showFragment(transaction, cardManageKsaFragment);
                }
                break;
            default:
                break;
        }
        transaction.commitAllowingStateLoss();
    }

    private void showFragment(FragmentTransaction transaction, Fragment fragment) {
        transaction.show(fragment);
        transaction.setMaxLifecycle(fragment, Lifecycle.State.RESUMED);
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment1 : fragments) {
            if (fragment != fragment1) {
                transaction.hide(fragment1);
                transaction.setMaxLifecycle(fragment1, Lifecycle.State.STARTED);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (chatFragment != null) {
            fragmentManager.putFragment(outState, key_coming_soon, chatFragment);
        }
        if (analyticsFragment != null) {
            fragmentManager.putFragment(outState, key_budget, analyticsFragment);
        }
        if (historyFragment != null) {
            fragmentManager.putFragment(outState, key_history, historyFragment);
        }
        if (homeFragment != null) {
            fragmentManager.putFragment(outState, key_home, homeFragment);
        }
        if (cardManageKsaFragment != null) {
            fragmentManager.putFragment(outState, key_card_ksa, cardManageKsaFragment);
        }
        super.onSaveInstanceState(outState);
    }

    private void addFragment(FragmentTransaction transaction, Fragment fragment, String tag) {
        Fragment old = fragmentManager.findFragmentByTag(tag);
        if (old == null) {
            old = fragment;
            transaction.add(R.id.maintabcontent, fragment, tag);
        }
        if (!navigation_bar_home.equals(tag)) {
            transaction.hide(old);
            transaction.setMaxLifecycle(fragment, Lifecycle.State.STARTED);
        } else {
            transaction.setMaxLifecycle(fragment, Lifecycle.State.RESUMED);
        }
    }

    @Override
    public void notifyByThemeChanged() {
        super.notifyByThemeChanged();
        ThemeUtils.setTranslucentStatus(this);
        if (homeFragment != null) {
            homeFragment.noticeThemeChange();
        }
        if (cardManageKsaFragment != null) {
            cardManageKsaFragment.noticeThemeChange();
        }

        if (historyFragment != null) {
            historyFragment.noticeThemeChange();
        }

        if (chatFragment != null) {
            chatFragment.noticeThemeChange();
        }
        if (analyticsFragment != null) {
            analyticsFragment.noticeThemeChange();
        }

        if (bottomNav != null) {
            bottomNav.setCircleColor(getColor(ThemeSourceUtils.getSourceID(MainActivity.this, R.attr.home_bottom_icon_circle_color)));
            bottomNav.setBackgroundBottomColor(getColor(ThemeSourceUtils.getSourceID(MainActivity.this, R.attr.home_bottom_bg_color)));
            bottomNav.setCountTextColor(getColor(ThemeSourceUtils.getSourceID(MainActivity.this, R.attr.home_bottom_count_text_color)));
            bottomNav.getCellById(ID_WITH_DRAW).setIcon(ThemeSourceUtils.getSourceID(mContext, R.attr.home_chat));
            bottomNav.getCellById(ID_BUDGET).setIcon(ThemeSourceUtils.getSourceID(mContext, R.attr.home_budget));
            bottomNav.getCellById(ID_HOME).setIcon(R.drawable.l_home_icon);
            bottomNav.getCellById(ID_HISTORY).setIcon(ThemeSourceUtils.getSourceID(mContext, R.attr.home_history));
            bottomNav.getCellById(ID_CARD_HOME).setIcon(ThemeSourceUtils.getSourceID(mContext, R.attr.home_card));
            if (bottomNav.getCellById(ID_CARD_HOME) != null) {
                bottomNav.getCellById(ID_CARD_HOME).setCircleColor(getColor(ThemeSourceUtils.getSourceID(MainActivity.this, R.attr.home_bottom_icon_circle_color)));
            }
        }
    }
}
