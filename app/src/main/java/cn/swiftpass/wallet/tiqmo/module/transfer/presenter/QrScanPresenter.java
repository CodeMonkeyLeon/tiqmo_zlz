package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import cn.swiftpass.wallet.tiqmo.module.transfer.contract.QrScanContract;

public class QrScanPresenter implements QrScanContract.Presenter {
    private QrScanContract.View mBaseView;

    @Override
    public void attachView(QrScanContract.View view) {
        this.mBaseView = view;
    }

    @Override
    public void detachView() {
        this.mBaseView = null;
    }


    @Override
    public void checkStartCode(String activeCode) {

    }
}
