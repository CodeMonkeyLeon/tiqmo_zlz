package cn.swiftpass.wallet.tiqmo.module.register.contract;


import cn.swiftpass.wallet.tiqmo.base.contract.BaseTransferPresenter;
import cn.swiftpass.wallet.tiqmo.base.contract.BaseTransferView;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.OTPSendPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.OTPSendView;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.OTPTryPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.OTPTryView;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.OTPVerifyPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.OTPVerifyView;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class RegisterOTPContract {

    public interface View extends OTPSendView<Presenter>, OTPTryView<Presenter>, OTPVerifyView<Presenter>, BaseTransferView<Presenter> {
        void stopAccountSuccess(Void result);

        void stopAccountFail(String errorCode, String errorMsg);

        void loginAccountSuccess(UserInfoEntity response);

        void loginAccountError(String errorCode, String errorMsg);

        void trustDeviceFail(String errorCode, String errorMsg);

        void trustDeviceSuccess(Void result);


    }


    public interface Presenter extends OTPSendPresenter<View>, OTPTryPresenter<View>, OTPVerifyPresenter<View>, BaseTransferPresenter<View> {

        void loginAccount(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude);

        void stopAccount();

        void trustDevice();

    }
}
