package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.PersonInfoContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.ChangePhoneFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RegionEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AvatarUrlEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class PersonInfoPresenter implements PersonInfoContract.Presenter {

    PersonInfoContract.View baseView = null;
    private BusinessParamEntity data;

    interface OnResponseListener {
        void onResponseSuccess();
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(baseView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (baseView != null) {
                    baseView.getLimitSuccess(result);
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getEmploymentSector(String businessType, String parentBussinessCode) {
        AppClient.getInstance().getBusinessParam(businessType, parentBussinessCode, new LifecycleMVPResultCallback<BusinessParamEntity>(baseView,true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                baseView.getEmploymentSectorSuccess(result.businessParamList);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    public void getData(OnResponseListener listener) {
        AppClient.getInstance().getBusinessParamList(new LifecycleMVPResultCallback<BusinessParamEntity>(baseView, true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                data = result;
                if (listener != null) {
                    listener.onResponseSuccess();
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView != null) {
                    baseView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getUserInfo() {
        AppClient.getInstance().getUserManager().getUserInfo(new LifecycleMVPResultCallback<UserInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(UserInfoEntity result) {
                baseView.getUserInfoSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.getUserInfoFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void updateUserInfo(UserInfoEntity userInfoEntity) {
        AppClient.getInstance().getUserManager().updateUserInfo(userInfoEntity,
                new LifecycleMVPResultCallback<UserInfoEntity>(baseView, true) {
                    @Override
                    protected void onSuccess(UserInfoEntity result) {
                        baseView.updateUserInfoSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        baseView.showErrorMsg(errorCode, errorMsg);
                        baseView.updateUserInfoFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void uploadAvatar(String localPath) {
        AppClient.getInstance().getUserManager().uploadAvatar(localPath, new LifecycleMVPResultCallback<AvatarUrlEntity>(baseView) {
            @Override
            protected void onSuccess(AvatarUrlEntity result) {
                baseView.showUploadSuccessfulHint();
                baseView.showNewAvatar(result.getUrl());
            }

            @Override
            protected void onFail(String code, String error) {
                baseView.showUploadFailedHint(error);
            }
        });
    }

    @Override
    public void getStateList() {
        AppClient.getInstance().getUserManager().getStateList(new LifecycleMVPResultCallback<StateListEntity>(baseView, true) {
            @Override
            protected void onSuccess(StateListEntity result) {
                if (baseView == null) {
                    return;
                }
                baseView.getStateListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView == null) {
                    return;
                }
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getCityList(String statesId) {
        AppClient.getInstance().requestCityList(statesId, new LifecycleMVPResultCallback<CityEntity>(baseView, true) {
            @Override
            protected void onSuccess(CityEntity result) {
                if (baseView == null || result == null || result.citiesInfoList == null
                        || result.citiesInfoList.size() <= 0) {
                    return;
                }
                baseView.showCityListDia(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView == null) {
                    return;
                }
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getDistrictList(String citiesId) {
        AppClient.getInstance().requestRegionList(citiesId, new LifecycleMVPResultCallback<RegionEntity>(baseView, true) {
            @Override
            protected void onSuccess(RegionEntity result) {
                if (baseView == null || result == null || result.neighborhoodsInfoList == null
                        || result.neighborhoodsInfoList.size() <= 0) {
                    return;
                }
                baseView.showRegionListDia(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView == null) {
                    return;
                }
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getSourceOfFund() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    baseView.showSourceOfFund(data.sourceOfFund);
                }
            });
        } else {
            baseView.showSourceOfFund(data.sourceOfFund);
        }
    }

    @Override
    public void getProfession(String businessType, String parentBussinessCode) {
        AppClient.getInstance().getBusinessParam(businessType, parentBussinessCode, new LifecycleMVPResultCallback<BusinessParamEntity>(baseView,true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                baseView.showProfession(result.businessParamList);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getCompanyType() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    baseView.showCompanyType(data.companyType);
                }
            });
        } else {
            baseView.showCompanyType(data.companyType);
        }
    }

    @Override
    public void getSalaryRange() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    baseView.showSalaryRange(data.annualIncome);
                }
            });
        } else {
            baseView.showSalaryRange(data.annualIncome);
        }
    }

    @Override
    public void getBalance() {
        AppClient.getInstance().getBalance(new LifecycleMVPResultCallback<BalanceListEntity>(baseView, true) {
            @Override
            protected void onSuccess(BalanceListEntity balanceListEntity) {
                if (baseView == null) {
                    return;
                }
                if (balanceListEntity != null) {
                    if(balanceListEntity.balanceInfos.size()>0) {
                        BalanceEntity balanceEntity = balanceListEntity.balanceInfos.get(0);
                        if (balanceEntity != null) {
                            String balance = balanceEntity.balance;
                            if (!TextUtils.isEmpty(balance)) {
                                if (balance.equals("0")) {
                                    baseView.showDeactivationDia();
                                } else {
                                    baseView.showCannotDeactivationDia();
                                }
                            }
                        }
                    }
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (baseView == null) {
                    return;
                }
                baseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void renewId() {
        AppClient.getInstance().getUserManager().renewId(new LifecycleMVPResultCallback<ResultEntity>(baseView, true) {
            @Override
            protected void onSuccess(ResultEntity result) {
                baseView.renewIdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.renewIdFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void verifyEmail(String email) {
        AppClient.getInstance().getUserManager().verifyEmail(email, new LifecycleMVPResultCallback<Void>(baseView, true) {
            @Override
            protected void onSuccess(Void result) {
                baseView.verifyEmailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.verifyEmailFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getChangePhoneFee() {
        AppClient.getInstance().getUserManager().getChangePhoneFee(new LifecycleMVPResultCallback<ChangePhoneFeeEntity>(baseView,true) {
            @Override
            protected void onSuccess(ChangePhoneFeeEntity result) {
                baseView.getChangePhoneFeeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(PersonInfoContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
