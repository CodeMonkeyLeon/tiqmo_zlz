package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class HomeCardBannerAdapter extends BannerAdapter<KsaCardEntity, HomeCardBannerAdapter.BannerViewHolder> {

    private CardBannerClickListener cardBannerClickListener;
    private int currentPosition = -1;
    private CardDetailEntity cardDetailEntity;

    private Context mContext;

    public void setShowCardDetail(int position,CardDetailEntity cardDetailEntity) {
        this.currentPosition = position;
        this.cardDetailEntity = cardDetailEntity;
    }

    public void setCardBannerClickListener(final CardBannerClickListener cardBannerClickListener) {
        this.cardBannerClickListener = cardBannerClickListener;
    }

    public HomeCardBannerAdapter(Context mContext,List<KsaCardEntity> mDatas) {
        //设置数据，也可以调用banner提供的方法,或者自己在adapter中实现
        super(mDatas);
        this.mContext = mContext;
    }

    //创建ViewHolder，可以用viewType这个字段来区分不同的ViewHolder
    @Override
    public BannerViewHolder onCreateHolder(ViewGroup parent, int viewType) {
//        ImageView imageView = new ImageView(parent.getContext());
//        //注意，必须设置为match_parent，这个是viewpager2强制要求的
//        imageView.setLayoutParams(new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT));
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        return new BannerViewHolder(imageView);

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_home_card_banner, parent, false);
        return new BannerViewHolder(itemView);
    }

    @Override
    public void onBindView(BannerViewHolder holder, KsaCardEntity ksaCardEntity, int position, int size) {
        try {
            //	卡种枚举值 (Standard 标准卡 - Platinum 白金卡)
            String cardProductType = ksaCardEntity.cardProductType;
            //	卡状态枚举(INACTIVATE:未激活 - ACTIVATE:激活 - LOCKED:锁定(冻结)等)
            String cardStatus = ksaCardEntity.cardStatus;
            //	卡类型枚举值: VIRTUAL虚拟卡 - PHYSICAL物理卡
            String cardType = ksaCardEntity.cardType;
            //	卡标识（卡管理中相当于id级别的参数，将会贯穿整个卡管理，作用等价卡号）
            String proxyCardNo = ksaCardEntity.proxyCardNo;
            //	卡片颜色 : 标准卡的01表示浅色 ,02表示深色；白金卡的06浅色,07深色
            String cardFaceId = ksaCardEntity.cardFaceId;
            String maskedCardNo = ksaCardEntity.maskedCardNo;
            String cardHolderName = ksaCardEntity.cardHolderName;

            boolean expiringSoonFlag = ksaCardEntity.expiringSoonFlag;
            boolean isShowCardDetail = ksaCardEntity.isShowCardDetail;
            String countdownMessage = ksaCardEntity.countdownMessage;
            if(!TextUtils.isEmpty(countdownMessage)){
                holder.tvExpireDate.setText(countdownMessage);
            }

            if(!TextUtils.isEmpty(maskedCardNo)){
                holder.tvCardHideNumber.setText("**** "+maskedCardNo);
            }

            if(!TextUtils.isEmpty(cardHolderName)){
                holder.tvCardHolderName.setText(cardHolderName);
            }

            CardDetailEntity cardDetailEntity = ksaCardEntity.cardDetailEntity;
//            expiringSoonFlag = true;
            holder.ivBanner.setImageResource(ksaCardEntity.getCardDrawable());
            holder.llActiveCard.setVisibility(View.GONE);
            holder.llGetNewCard.setVisibility(View.GONE);
            holder.llCardFreeze.setVisibility(View.GONE);
            holder.llCardExpire.setVisibility(View.GONE);
            holder.llCardHasExpired.setVisibility(View.GONE);
            holder.llContactSupport.setVisibility(View.GONE);
            holder.llViewDetails.setVisibility(View.VISIBLE);
            holder.llViewDetails.setBackgroundResource(AndroidUtils.getCardFaceDrawable(ksaCardEntity.cardFaceId,false));
//            if(position == currentPosition){
            if(isShowCardDetail){
                if(cardDetailEntity != null) {
                    holder.tvCardNumber.setText(AndroidUtils.showCardNum(cardDetailEntity.cardNo));
                    ksaCardEntity.cardNumber = cardDetailEntity.cardNo;
                    holder.tvCvv.setText(cardDetailEntity.cvv2);
                    holder.tvExpireTime.setText(cardDetailEntity.expireDate);
                    holder.llViewDetails.setVisibility(View.GONE);
                }
            }else{
                holder.llViewDetails.setVisibility(View.VISIBLE);
            }
//            }

            if(ksaCardEntity.isVirtualCard()){
                holder.tvVirtualCard.setText(R.string.DigitalCard_103);
            }else{
                holder.tvVirtualCard.setText(R.string.sprint20_46);
            }

            if (ksaCardEntity.isNotActivate()) {
                holder.llActiveCard.setVisibility(View.VISIBLE);
                holder.llViewDetails.setVisibility(View.GONE);
            }else if (ksaCardEntity.isExpired()) {
                holder.llCardHasExpired.setVisibility(View.VISIBLE);
                holder.llViewDetails.setVisibility(View.GONE);
            } else if (ksaCardEntity.isLocked()) {
                holder.llCardFreeze.setVisibility(View.VISIBLE);
                holder.llViewDetails.setVisibility(View.GONE);
            } else if (ksaCardEntity.isBlocked()) {
                holder.llGetNewCard.setVisibility(View.VISIBLE);
                holder.llViewDetails.setVisibility(View.GONE);
            } else if (ksaCardEntity.isShield()) {
                holder.llContactSupport.setVisibility(View.VISIBLE);
                holder.llViewDetails.setVisibility(View.GONE);
            }else if(expiringSoonFlag || !TextUtils.isEmpty(countdownMessage)){
                holder.llCardExpire.setVisibility(View.VISIBLE);
                holder.llViewDetails.setVisibility(View.GONE);
            }

//            BlurBuilder.blur(holder.ivBanner);

            holder.tvRenewCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.getNewCard(ksaCardEntity);
                    }
                }
            });
            holder.ivExpireClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.llCardExpire.setVisibility(View.GONE);
                    holder.llViewDetails.setVisibility(View.VISIBLE);
                }
            });
            holder.tvKeepVirtual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.getNewCard(ksaCardEntity);
                    }
                }
            });
            holder.tvViewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.getCardDetails(ksaCardEntity);
                    }
                }
            });
            holder.llGetNewCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.getNewCard(ksaCardEntity);
                    }
                }
            });
            holder.tvActiveCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.activateCard(ksaCardEntity);
                    }
                }
            });

            holder.tvContactSupport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.contactSupport();
                    }
                }
            });

            holder.ivCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardBannerClickListener != null){
                        cardBannerClickListener.copyCardNo(ksaCardEntity);
                    }
                }
            });
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    class BannerViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView ivBanner;
        ImageView ivCopy;
        ImageView ivExpireClose;
        ConstraintLayout conCard;
        TextView tvCardHolderName;
        TextView tvCardHideNumber;
        TextView tvCardNumber;
        TextView tvExpireTime;
        TextView tvExpireDate;
        TextView tvCvv;
        TextView tvViewDetails;
        TextView tvActiveCard;
        TextView tvVirtualCard;
        TextView tvGetNewCard;
        TextView tvContactSupport;
        TextView tvKeepVirtual;
        TextView tvRenewCard;
        ConstraintLayout llViewDetails;
        ConstraintLayout llActiveCard;
        ConstraintLayout llCardFreeze;
        ConstraintLayout llCardExpire;
        ConstraintLayout llCardHasExpired;
        LinearLayout llGetNewCard;
        LinearLayout llContactSupport;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            this.ivBanner = itemView.findViewById(R.id.iv_banner);
            this.ivCopy = itemView.findViewById(R.id.iv_copy);
//            this.conCard = itemView.findViewById(R.id.con_card);
            this.tvCardHolderName = itemView.findViewById(R.id.tv_card_holder_name);
            this.tvCardHideNumber = itemView.findViewById(R.id.tv_card_hide_number);
            this.tvCardNumber = itemView.findViewById(R.id.tv_card_number);
            this.tvExpireTime = itemView.findViewById(R.id.tv_expire_time);
            this.tvExpireDate = itemView.findViewById(R.id.tv_expire_date);
            this.tvCvv = itemView.findViewById(R.id.tv_cvv);
            this.tvViewDetails = itemView.findViewById(R.id.tv_view_details);
            this.tvContactSupport = itemView.findViewById(R.id.tv_contact_support);
            this.llViewDetails = itemView.findViewById(R.id.ll_view_details);
            this.llCardFreeze = itemView.findViewById(R.id.ll_card_freeze);
            this.llActiveCard = itemView.findViewById(R.id.ll_active_card);
            this.llGetNewCard = itemView.findViewById(R.id.ll_get_new_card);
            this.llCardExpire = itemView.findViewById(R.id.ll_card_expire);
            this.llCardHasExpired = itemView.findViewById(R.id.ll_card_has_expired);
            this.llContactSupport = itemView.findViewById(R.id.ll_contact_support);
            this.tvActiveCard = itemView.findViewById(R.id.tv_active_card);
            this.tvGetNewCard = itemView.findViewById(R.id.tv_get_new_card);
            this.tvKeepVirtual = itemView.findViewById(R.id.tv_keep_virtual);
            this.tvRenewCard = itemView.findViewById(R.id.tv_renew_card);
            this.ivExpireClose = itemView.findViewById(R.id.iv_expire_close);
            this.tvVirtualCard = itemView.findViewById(R.id.tv_virtual_card);
        }
    }

    public interface CardBannerClickListener{
        void activateCard(KsaCardEntity ksaCardEntity);
        void getCardDetails(KsaCardEntity ksaCardEntity);
        void getNewCard(KsaCardEntity ksaCardEntity);
        void copyCardNo(KsaCardEntity ksaCardEntity);
        void contactSupport();
    }
}
