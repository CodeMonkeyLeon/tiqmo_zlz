package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * Imr beneficiary list item entity
 */
public class ImrBeneficiaryListEntity extends BaseEntity {

    public List<ImrBeneficiaryInfo> imrPayeeInfos;

    public String channelCode = "TransFast";

    public static class ImrBeneficiaryInfo extends BaseEntity {
        public String relationshipCode;
        public int relationshipSrc;
        public boolean isShowSubView;
        public List<ImrBeneficiaryBean> imrPayeeDetailInfos;
    }

    public static class ImrBeneficiaryBean extends BaseEntity {
        public String payeeInfoId;
        public String payeeFullName;
        public String nickName;
        public String countryLogoUrl;
        public String transferDestinationCountryName;
        public String countrySupportedCurrencyName;
        public String sex;
        public String relationshipCode;
        public String transferDestinationCountryCode;
        public String receiptMethod;
        //银行或代理代码
        public String bankAgentCode;
        private boolean isShowRemoveIcon;
        public boolean isShowBeneficiaryOP;

        public String channelCode;

        //IVR 激活状态 0:未激活，1：激活
        public String activeState;

        public boolean isIvrActivite(){
            return "1".equals(activeState);
        }

        public boolean isShowRemoveIcon() {
            return isShowRemoveIcon;
        }

        public void setShowRemoveIcon(boolean showRemoveIcon) {
            isShowRemoveIcon = showRemoveIcon;
        }
    }
}
