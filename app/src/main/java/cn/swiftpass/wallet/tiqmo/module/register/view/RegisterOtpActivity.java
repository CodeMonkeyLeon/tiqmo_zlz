package cn.swiftpass.wallet.tiqmo.module.register.view;

import static cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil.ANIM_TYPE.RIGHT_IN;

import android.Manifest;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.view.ConfirmAddMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.ActivateCardActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.BlockCardConfirmActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.CardManageKsaFragment;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetCardPwdOneActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetCardPwdTwoActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetNewCardSuccessActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetNewCardSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardLimitActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardResultActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardSettingActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.SetPinConfirmActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CharitySummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.CreateSplitBillActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.ReceiveSplitFragment;
import cn.swiftpass.wallet.tiqmo.module.home.view.RequestCenterActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryFiveActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiarySixActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.AddBeneficiaryTwoActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.view.ImrSendMoneySummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.view.PayBillSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterOTPContract;
import cn.swiftpass.wallet.tiqmo.module.register.entity.CheckPhoneEntity;
import cn.swiftpass.wallet.tiqmo.module.register.presenter.RegisterOTPPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AreaEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.view.RechargeSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.AddBeneficiaryActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.RequestTransferActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferSummaryActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.EVoucherPayReviewActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.SystemInitManager;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SMSReceiver;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.VerifyPwdCodeView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.TrustDeviceDialog;

public class RegisterOtpActivity extends BaseCompatActivity<RegisterOTPContract.Presenter> implements RegisterOTPContract.View,SMSReceiver.OnReceivedMessageListener{

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.tv_error_otp)
    TextView tvErrorOtp;
    @BindView(R.id.tv_send_msg)
    TextView tvSendMsg;
    @BindView(R.id.tv_otp_send)
    TextView tvOtpSend;
    @BindView(R.id.tv_otp_time)
    TextView tvOtpTime;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_otp_phone)
    LinearLayout llOtpPhone;
    @BindView(R.id.verift_otp)
    VerifyPwdCodeView veriftOtp;
    @BindView(R.id.verift_otp_kyc)
    VerifyPwdCodeView veriftOtpKyc;
    @BindView(R.id.ll_otp_time)
    LinearLayout llOtpTime;
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_show_time_expire)
    TextView tvShowTimeExpire;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;

    private String phoneNo;
    private String otpType;
    private String type;
    private CheckPhoneEntity checkPhoneEntity;

    private UserInfoEntity userInfoEntity;
    private String codeLength;
    private int expiredTimeKyc;

    private String callingCode;
    //证件号
    private String recipientId;
    private String password;
    private AreaEntity areaEntity;

    private String nafathAuthFlag;

    //第一次流程OTP验证title
    private String firstOtpTitle;

    private RiskControlEntity riskControlEntity;
    //判断是否主动发送otp
    private String riskCode;

    //交易相关
    private TransferEntity transferEntity;
    private String sceneType, pd_sceneType, orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode, transFees, vat;
    private KycContactEntity kycContactEntity;
    private String payerName;
    private CheckOutEntity checkOutEntity;
    private String paymentMethodNo;

    private boolean isFromSDK;
    private boolean isOpen = true;

    //是否走支付密码  Y是 N否
    public String paySecretRequired;

    public String noStr = "";
    private int operateType;

    public String loginType;

    private UserInfoEntity mUserInfoEntity;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    boolean isFromEditBeneficiary = false;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;

    private String receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
            nickName, relationshipCode, imrCallingCode, phone,
            transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
            birthDate, sex, cityName, districtName,
            poBox, buildingNo, street, idNo, idExpiry,
            bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
            cityId, beneCurrency, channelPayeeId, channelCode,branchId;

    /**
     * 付费开卡
     */
    private KsaCardSummaryEntity ksaCardSummaryEntity;
    /**
     * 免费开卡
     */
    private OpenCardReqEntity openCardReqEntity;
    private KsaCardEntity ksaCardEntity;
    private KsaLimitChannelEntity.CardLimitConfigEntity mCardLimitConfigEntity;

    private String proxyCardNo, blockCardReason,blockCardReasonStatus;

    private SMSReceiver receiver = new SMSReceiver();
    private BottomDialog bottomDialog;

    private List<String> additionalData = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_reg_otp;
    }



    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        otpType = Constants.OTP_REG;
        callingCode = UserInfoManager.getInstance().getCallingCode();
        //动态注册广播接收者且设置为最大优先级
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED" );
        filter.setPriority(Integer.MAX_VALUE);//设置动态优先级为最大，1000应该不是最大
        registerReceiver(receiver,filter);
        receiver.setOnReceivedMessageListener(this);

        checkPhoneEntity = UserInfoManager.getInstance().getCheckPhoneEntity();
        if (getIntent() != null && getIntent().getExtras() != null) {
            phoneNo = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);

            mCardLimitConfigEntity = (KsaLimitChannelEntity.CardLimitConfigEntity) getIntent().getExtras().getSerializable(Constants.ksaCardLimitConfigEntity);
            ksaCardSummaryEntity = (KsaCardSummaryEntity) getIntent().getExtras().getSerializable(Constants.ksaCardSummaryEntity);
            openCardReqEntity = (OpenCardReqEntity) getIntent().getExtras().getSerializable(Constants.openCardReqEntity);
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            if(ksaCardEntity != null){
                additionalData.add(ksaCardEntity.maskedCardNo);
            }
            proxyCardNo = getIntent().getExtras().getString(Constants.proxyCardNo);
            blockCardReason = getIntent().getExtras().getString(Constants.blockCardReason);
            blockCardReasonStatus = getIntent().getExtras().getString(Constants.blockCardReasonStatus);
            password = getIntent().getExtras().getString(Constants.DATA_PHONE_PD);
            areaEntity = (AreaEntity) getIntent().getExtras().getSerializable(Constants.DATA_AREA_ENTITY);
            riskControlEntity = (RiskControlEntity) getIntent().getExtras().getSerializable(Constants.OTP_riskControlEntity);
            otpType = getIntent().getExtras().getString(Constants.DATA_OTP_TYPE);
            firstOtpTitle = getIntent().getExtras().getString(Constants.DATA_FIRST_TITLE);
            codeLength = getIntent().getExtras().getString(Constants.DATA_OTP_LENGTH);
            loginType = getIntent().getExtras().getString(Constants.LOGIN_TYPE);
            expiredTimeKyc = getIntent().getExtras().getInt(Constants.DATA_OTP_TIME);
            userInfoEntity = (UserInfoEntity) getIntent().getExtras().getSerializable(Constants.DATA_USER);
            if (userInfoEntity == null) {
                userInfoEntity = getUserInfoEntity();
            }
            isFromEditBeneficiary = getIntent().getExtras().getBoolean(Constants.IS_FROM_EDIT_BENEFICIARY, false);
            if (Constants.OTP_REG.equals(otpType)) {
                tvTitle.setText(R.string.OtpVerification_SU_0003_1_D_1);
                ivEdit.setVisibility(View.VISIBLE);
                type = Constants.REG_OTP_TYPE;
            }else if (Constants.OTP_FORGET_LOGIN_PD.equals(otpType)) {
                tvTitle.setText(R.string.sprint19_31);
                type = Constants.FORGET_OTP_TYPE;
            } else if (Constants.KYC_REG.equals(otpType)) {
                tvTitle.setText(R.string.kyc_6);
                type = Constants.OTP_KYC_REG;
                ivKycStep.setVisibility(View.VISIBLE);
                ivKycStep.setImageResource(R.drawable.step_progress_2_2);
            } else if (Constants.KYC_FORGET_PAY_PD.equals(otpType)) {
                tvTitle.setText(R.string.forget_ppw_1);
                type = Constants.OTP_KYC_PAY_PD;
                ivKycStep.setVisibility(View.GONE);
            } else if (Constants.OTP_FIRST_LOGIN.equals(otpType) || Constants.OTP_FIRST_FAST_LOGIN.equals(otpType)) {
                tvTitle.setText(R.string.loginTitle);
                type = Constants.OTP_TYPE_FIRST_LOGIN;
                ivKycStep.setVisibility(View.GONE);
            }else if(Constants.OTP_CHANGE_PHONE.equals(otpType) || Constants.OTP_LOGIN_NEW_DEVICE.equals(otpType) || Constants.OTP_LOGIN_OLD_DEVICE.equals(otpType)){
                ivEdit.setVisibility(View.VISIBLE);
                tvTitle.setText(firstOtpTitle);
            } else {
                tvTitle.setText(firstOtpTitle);
            }

            if(checkPhoneEntity != null){
                noStr = checkPhoneEntity.noStr;
                riskControlEntity = checkPhoneEntity.riskControlInfo;
                recipientId = checkPhoneEntity.checkIdNumber;
                nafathAuthFlag = checkPhoneEntity.nafathAuthFlag;
                phoneNo = checkPhoneEntity.phone;
                operateType = checkPhoneEntity.operateType;
            }

            if (TextUtils.isEmpty(phoneNo)) {
                phoneNo = getUserInfoEntity().phone;
            }

            if (riskControlEntity != null) {
                type = riskControlEntity.type;
                codeLength = riskControlEntity.codeLength;
                riskCode = riskControlEntity.riskCode;
                expiredTimeKyc = riskControlEntity.expiredTime;
                paySecretRequired = riskControlEntity.paySecretRequired;
            }

            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            if (imrBeneficiaryEntity != null) {
                receiptMethod = imrBeneficiaryEntity.receiptMethod;
                payeeFullName = imrBeneficiaryEntity.payeeFullName;
                nickName = imrBeneficiaryEntity.nickName;
                relationshipCode = imrBeneficiaryEntity.relationshipCode;
                imrCallingCode = imrBeneficiaryEntity.callingCode;
                phone = imrBeneficiaryEntity.phone;
                transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;
                payeeInfoCountryCode = imrBeneficiaryEntity.payeeInfoCountryCode;
                birthPlace = imrBeneficiaryEntity.birthPlace;
                birthDate = imrBeneficiaryEntity.birthDate;
                sex = imrBeneficiaryEntity.sex;
                cityName = imrBeneficiaryEntity.cityName;
                districtName = imrBeneficiaryEntity.districtName;
                poBox = imrBeneficiaryEntity.poBox;
                buildingNo = imrBeneficiaryEntity.buildingNo;
                street = imrBeneficiaryEntity.street;
                idNo = imrBeneficiaryEntity.idNo;
                idExpiry = imrBeneficiaryEntity.idExpiry;
                bankAccountType = imrBeneficiaryEntity.bankAccountType;
                ibanNo = imrBeneficiaryEntity.ibanNo;
                bankAccountNo = imrBeneficiaryEntity.bankAccountNo;
                receiptOrgCode = imrBeneficiaryEntity.receiptOrgCode;
                receiptOrgBranchCode = imrBeneficiaryEntity.receiptOrgBranchCode;
                saveFlag = imrBeneficiaryEntity.saveFlag;
                beneCurrency = imrBeneficiaryEntity.beneCurrency;
                cityId = imrBeneficiaryEntity.cityId;
                receiptOrgName = imrBeneficiaryEntity.receiptOrgName;
                receiptOrgBranchName = imrBeneficiaryEntity.receiptOrgBranchName;
                channelPayeeId = imrBeneficiaryEntity.channelPayeeId;
                channelCode = imrBeneficiaryEntity.channelCode;
                branchId = imrBeneficiaryEntity.branchId;
            }
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            transferEntity = (TransferEntity) getIntent().getExtras().getSerializable(Constants.TRANSFER_ENTITY);
            kycContactEntity = (KycContactEntity) getIntent().getExtras().getSerializable(Constants.CONTACT_ENTITY);
            checkOutEntity = (CheckOutEntity) getIntent().getExtras().getSerializable(Constants.CHECK_OUT_ENTITY);
            sceneType = getIntent().getExtras().getString(Constants.sceneType);
            pd_sceneType = getIntent().getExtras().getString(Constants.pd_sceneType);
            isFromSDK = getIntent().getExtras().getBoolean(Constants.IS_FROM_SDK);
            isOpen = getIntent().getExtras().getBoolean(Constants.isOpen, true);
            if (kycContactEntity != null) {
                payerName = kycContactEntity.getContactsName();
            }
            if (transferEntity != null) {
                orderNo = transferEntity.orderNo;
                payerName = transferEntity.payerName;
                payMethod = transferEntity.payMethod;
                transAmount = transferEntity.orderAmount;
                exchangeRate = transferEntity.exchangeRate;
                transCurrencyCode = transferEntity.orderCurrencyCode;
                transFees = transferEntity.transFees;
                vat = transferEntity.vat;
            } else if (checkOutEntity != null) {
                orderNo = checkOutEntity.orderNo;
                if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
                    paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
                }
            }
        }

        if ("6".equals(codeLength)) {
            veriftOtp.setVisibility(View.GONE);
            veriftOtpKyc.setVisibility(View.VISIBLE);
        }


        tvOtpSend.setText(AndroidUtils.addStrToPhone(callingCode + " " + AndroidUtils.addStarPhone(phoneNo)));

        if ("6".equals(codeLength)) {
            //默认输入框为空
            veriftOtpKyc.setEmpty();
            veriftOtpKyc.setOnCodeFinishListener(new VerifyPwdCodeView.OnCodeFinishListener() {
                @Override
                public void onTextChange(View view, String content) {

                }

                @Override
                public void onComplete(View view, String content) {
                    if (Constants.OTP_FIRST_LOGIN.equals(otpType)) {
                        showProgress(true);
                        mPresenter.loginAccount(callingCode, phoneNo, password, content, type, ProjectApp.getLongitude(), ProjectApp.getLatitude());
                    } else if (Constants.OTP_FIRST_FAST_LOGIN.equals(otpType)) {
                        showProgress(true);
                        mPresenter.loginAccount(callingCode, phoneNo, password, content, type, ProjectApp.getLongitude(), ProjectApp.getLatitude());
                    } else {
                        mPresenter.verifyOTP(callingCode, phoneNo, content, type,orderNo);
                    }
                }
            });
        } else {
            //默认输入框为空
            veriftOtp.setEmpty();
            veriftOtp.setOnCodeFinishListener(new VerifyPwdCodeView.OnCodeFinishListener() {
                @Override
                public void onTextChange(View view, String content) {

                }

                @Override
                public void onComplete(View view, String content) {
                    if (Constants.OTP_FIRST_LOGIN.equals(otpType)) {
                        showProgress(true);
                        mPresenter.loginAccount(callingCode, phoneNo, password, content, type, ProjectApp.getLongitude(), ProjectApp.getLatitude());
                    } else if (Constants.OTP_FIRST_FAST_LOGIN.equals(otpType)) {
                        showProgress(true);
                        mPresenter.loginAccount(callingCode, phoneNo, password, content, type, ProjectApp.getLongitude(), ProjectApp.getLatitude());
                    } else {
                        mPresenter.verifyOTP(callingCode, phoneNo, content, type,orderNo);
                    }
                }
            });
        }

        if ("6".equals(codeLength) || expiredTimeKyc != 0) {
            setResetTime(true, phoneNo, expiredTimeKyc);
        } else if (Constants.OTP_SEND_CODE_000002.equals(riskCode) || expiredTimeKyc == 0) {
            mPresenter.sendOTP(callingCode, phoneNo, type,additionalData);
        }
        LocaleUtils.viewRotationY(this, ivBack, headCircle);

        getReadPermissions();
    }

    /**
     * 权限的验证及处理，相关方法
     */
    private void getReadPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                    | ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS) | ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {//是否请求过该权限
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.RECEIVE_SMS,
                                    Manifest.permission.READ_SMS,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE}, 10001);
                } else {//没有则请求获取权限，示例权限是：存储权限和短信权限，需要其他权限请更改或者替换
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.RECEIVE_SMS,
                                    Manifest.permission.READ_SMS,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10001);
                }
            }
        }

    }

    /**
     * 一个或多个权限请求结果回调
     * 当点击了不在询问，但是想要实现某个功能，必须要用到权限，可以提示用户，引导用户去设置
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 10001:
                for (int i = 0; i < grantResults.length; i++) {
//                   如果拒绝获取权限
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        //判断是否勾选禁止后不再询问
                        boolean flag = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]);
                        if (flag) {
                            getReadPermissions();
                            return;//用户权限是一个一个的请求的，只要有拒绝，剩下的请求就可以停止，再次请求打开权限了
                        } else { // 勾选不再询问，并拒绝
//                            Toast.makeText(this, "请到设置中打开权限", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                }
//                Toast.makeText(RegisterOtpActivity.this, "权限开启完成",Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }



    @OnClick({R.id.iv_back, R.id.iv_edit, R.id.tv_send_msg})
    public void onViewClicked(View view) {
        tvErrorOtp.setVisibility(View.GONE);
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_edit:
                finish();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_send_msg:
                mPresenter.tryOTP(callingCode, phoneNo, type,additionalData);
                break;
            default:
                break;
        }
    }

    @Override
    public void sendOTPFailed(String errorCode, String errorMsg) {
        onOtpActionFailed(errorCode, errorMsg, true);
    }

    private void onOtpActionFailed(final String errorCode, String errorMsg, final boolean isSend) {
        if (!TextUtils.isEmpty(errorMsg)) {
            tvErrorOtp.setText(errorMsg);
        }
        tvErrorOtp.setVisibility(View.VISIBLE);

        if (isSend) {
            tvSendMsg.setClickable(true);
            tvSendMsg.setEnabled(true);
        }
    }

    @Override
    public void sendOTPSuccess(RegisterOtpEntity response) {
        try {
            long expiredTime = response.expiredTime;
            Boolean isExpired = response.verifyCodeExpired;
            if (isExpired != null && !isExpired) {
                tvSendMsg.setEnabled(false);
            } else {
                tvSendMsg.setEnabled(true);
            }
            setResetTime(true, phoneNo, (int) expiredTime);
            clearOtpInput();
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    private void clearOtpInput() {
        if ("6".equals(codeLength)) {
            veriftOtpKyc.setEmpty();
        } else {
            veriftOtp.setEmpty();
        }
    }

    public void setResetTime(boolean isReset, String phoneNo, int time) {
        if (isReset) {
            tvSendMsg.setClickable(false);
            tvSendMsg.setEnabled(false);
            AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, time, new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (isFinishing() || tvSendMsg == null) {
                        return;
                    }

                    if (millisUntilFinished == 0) {
                        tvShowTimeExpire.setText(getString(R.string.common_21));
                        tvOtpTime.setVisibility(View.GONE);
                    } else if (millisUntilFinished < 60 && millisUntilFinished > 0) {
                        tvShowTimeExpire.setText(getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        if (LocaleUtils.isRTL(mContext)) {
                            tvOtpTime.setText("s " + millisUntilFinished);
                        } else {
                            tvOtpTime.setText(millisUntilFinished + " s");
                        }
                    } else {
                        tvShowTimeExpire.setText(getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        int second = millisUntilFinished % 60;
                        if (second < 10) {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":0" + second);
                        } else {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":" + second);
                        }
                    }

                    tvSendMsg.setClickable(false);
                    tvSendMsg.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    if (isFinishing() || tvSendMsg == null) {
                        return;
                    }
                    tvShowTimeExpire.setText(getString(R.string.common_21));
                    tvOtpTime.setVisibility(View.GONE);
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                }
            });
        } else {
            AdvancedCountdownTimer.getInstance().startCountDown(phoneNo, SystemInitManager.getInstance().getSystemInitEntity().getOtpIntervalTime(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (isFinishing() || tvSendMsg == null) {
                        return;
                    }
                    if (millisUntilFinished == 0) {
                        tvShowTimeExpire.setText(getString(R.string.common_21));
                        tvOtpTime.setVisibility(View.GONE);
                    } else if (millisUntilFinished < 60 && millisUntilFinished > 0) {
                        tvShowTimeExpire.setText(getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        tvOtpTime.setText(millisUntilFinished + "s");
                    } else {
                        tvShowTimeExpire.setText(getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        int second = millisUntilFinished % 60;
                        if (second < 10) {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":0" + second);
                        } else {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":" + second);
                        }
                    }

                }

                @Override
                public void onFinish() {
                    if (isFinishing() || tvSendMsg == null) {
                        return;
                    }
                    tvShowTimeExpire.setText(getString(R.string.common_21));
                    tvOtpTime.setVisibility(View.GONE);
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                }
            });
        }
    }

    @Override
    public void tryOTPFailed(String errorCode, String errorMsg) {
        onOtpActionFailed(errorCode, errorMsg, false);
    }

    @Override
    public void tryOTPSuccess(RegisterOtpEntity response) {
        try {
            long expiredTime = response.expiredTime;
            Boolean isExpired = response.verifyCodeExpired;
            if (isExpired != null && !isExpired) {
                tvSendMsg.setEnabled(false);
            } else {
                tvSendMsg.setEnabled(true);
            }
            setResetTime(true, phoneNo, (int) expiredTime);
            clearOtpInput();
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void verifyOTPFailed(String errorCode, String errorMsg) {
        tvErrorOtp.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(errorMsg)) {
            tvErrorOtp.setText(errorMsg);
        }
        //出错则显示红色下划线
//        veriftOtp.showEditTextError();
        clearOtpInput();
    }

    @Override
    public void verifyOTPSuccess(Void response) {
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
        mHashMaps.put(Constants.DATA_OTP_TYPE, otpType);
        switch (otpType) {
            case Constants.OTP_CHANGE_PHONE:
                ActivitySkipUtil.startAnotherActivity(this, NafathAuthActivity.class, mHashMaps, RIGHT_IN);
                finish();
                break;
            case Constants.OTP_LOGIN_OLD_DEVICE:
                //旧设备登录
                ActivitySkipUtil.startAnotherActivity(this, LoginFastNewActivity.class, mHashMaps, RIGHT_IN);
                finish();
                break;
            case Constants.OTP_LOGIN_NEW_DEVICE:
                //新设备登录
                ActivitySkipUtil.startAnotherActivity(this, RegMoreAboutActivity.class, mHashMaps, RIGHT_IN);
                finish();
                break;
            case Constants.OTP_ACTIVATE_USER:
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ACTIVATE_USER));
                finish();
                break;
            case Constants.OTP_REG:
            case Constants.OTP_FORGET_LOGIN_PD:
                //判断是否需要nafath认证
                if("0".equals(nafathAuthFlag)){
                    ActivitySkipUtil.startAnotherActivity(this, NafathAuthActivity.class, mHashMaps, RIGHT_IN);
                }else{
                    ActivitySkipUtil.startAnotherActivity(this, RegSetPwdOneActivity.class, mHashMaps, RIGHT_IN);
                }
                finish();
                break;
            case Constants.OTP_FIRST_CHARITY:
                if ("Y".equals(paySecretRequired)) {
                    CharitySummaryActivity.getCharitySummaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    CharitySummaryActivity.getCharitySummaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_WITHDRAW_ADD_IMR:
                if ("Y".equals(paySecretRequired)) {
                    AddBeneficiaryActivity.getAddBeneficiaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    AddBeneficiaryActivity.getAddBeneficiaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_TRANSFER_CONTACT:
                if ("Y".equals(paySecretRequired)) {
                    TransferMoneyActivity.getTransferMoneyActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    TransferMoneyActivity.getTransferMoneyActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_TRANSFER_IBANK:
                if ("Y".equals(paySecretRequired)) {
                    TransferSummaryActivity.getTransferSummaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    TransferSummaryActivity.getTransferSummaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_TRANSFER_REQUEST:
                if ("Y".equals(paySecretRequired)) {
                    RequestCenterActivity.getRequestCenterActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    RequestCenterActivity.getRequestCenterActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_TOP_UP:
                if ("Y".equals(paySecretRequired)) {
                    EVoucherPayReviewActivity.geteVoucherPayReviewActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    EVoucherPayReviewActivity.geteVoucherPayReviewActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_INTER_TOP_UP:
                if ("Y".equals(paySecretRequired)) {
                    RechargeSummaryActivity.getRechargeSummaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    RechargeSummaryActivity.getRechargeSummaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_PAY_BILL:
                if ("Y".equals(paySecretRequired)) {
                    PayBillSummaryActivity.getPayBillSummaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    PayBillSummaryActivity.getPayBillSummaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_KSA_CARD_BLOCK:
                if ("Y".equals(paySecretRequired)) {
                    BlockCardConfirmActivity.getBlockCardConfirmActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    BlockCardConfirmActivity.getBlockCardConfirmActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_KSA_CARD_SET_PIN:
                if ("Y".equals(paySecretRequired)) {
                    SetPinConfirmActivity.getSetPinConfirmActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    SetPinConfirmActivity.getSetPinConfirmActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_IMR:
                if ("Y".equals(paySecretRequired)) {
                    ImrSendMoneySummaryActivity.getImrSendMoneySummaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    ImrSendMoneySummaryActivity.getImrSendMoneySummaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_PAY_CARD:
                if ("Y".equals(paySecretRequired)) {
                    GetNewCardSummaryActivity.getGetNewCardSummaryActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    GetNewCardSummaryActivity.getGetNewCardSummaryActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_FREEZE_CARD:
            case Constants.OTP_FIRST_CARD_DETAIL:
            case Constants.OTP_FIRST_UNFREEZE_CARD:
                if ("Y".equals(paySecretRequired)) {
                    CardManageKsaFragment.cardManageKsaFragment.jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    CardManageKsaFragment.cardManageKsaFragment.jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_PAY_CARD_NO_FEE:
                if ("Y".equals(paySecretRequired)) {
                    GetCardPwdTwoActivity.getGetCardPwdTwoActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    GetCardPwdTwoActivity.getGetCardPwdTwoActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_SET_KSA_CARD_LIMIT:
                if ("Y".equals(paySecretRequired)) {
                    KsaCardLimitActivity.getKsaCardLimitActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    KsaCardLimitActivity.getKsaCardLimitActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_RECEIVE_PAY:
                if ("Y".equals(paySecretRequired)) {
                    ReceiveSplitFragment.getInstance().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    ReceiveSplitFragment.getInstance().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_KSA_CARD_SET_CHANNEL:
                if ("Y".equals(paySecretRequired)) {
                    KsaCardSettingActivity.getKsaCardSettingActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    KsaCardSettingActivity.getKsaCardSettingActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_PAY_CARD_ACTIVATE:
                if ("Y".equals(paySecretRequired)) {
                    ActivateCardActivity.getActivateCardActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    ActivateCardActivity.getActivateCardActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_ADD_SEND_BENE:
                if ("Y".equals(paySecretRequired)) {
                    if(AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity() != null) {
                        AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity().jumpToPwd(false);
                    }else if(AddBeneficiarySixActivity.getAddBeneficiarySixActivity() != null) {
                        AddBeneficiarySixActivity.getAddBeneficiarySixActivity().jumpToPwd(false);
                    }else if(AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity() != null) {
                        AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity().jumpToPwd(false);
                    }
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    if(AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity() != null) {
                        AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity().jumpToPwd(true);
                    }else if(AddBeneficiarySixActivity.getAddBeneficiarySixActivity() != null) {
                        AddBeneficiarySixActivity.getAddBeneficiarySixActivity().jumpToPwd(true);
                    }else if(AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity() != null) {
                        AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity().jumpToPwd(true);
                    }
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_ADD_IMR:
                //添加受益人IVR放在接口成功后处理
                if ("Y".equals(paySecretRequired)) {
                    if(AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity() != null) {
                        AddBeneficiaryFiveActivity.getAddBeneficiaryFiveActivity().jumpToPwd(false);
                    }else if(AddBeneficiarySixActivity.getAddBeneficiarySixActivity() != null) {
                        AddBeneficiarySixActivity.getAddBeneficiarySixActivity().jumpToPwd(false);
                    }else if(AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity() != null) {
                        AddBeneficiaryTwoActivity.getAddBeneficiaryTwoActivity().jumpToPwd(false);
                    }
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_REQUEST_TRANSFER:
                if ("Y".equals(paySecretRequired)) {
                    RequestTransferActivity.getRequestTransferActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    RequestTransferActivity.getRequestTransferActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_SPLIT_BILL:
                if ("Y".equals(paySecretRequired)) {
                    CreateSplitBillActivity.getCreateSplitBillActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    CreateSplitBillActivity.getCreateSplitBillActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_CHANGE_LOGIN_PD:
                if ("Y".equals(paySecretRequired)) {
                    RegSetPwdTwoActivity.getRegSetPwdTwoActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    RegSetPwdTwoActivity.getRegSetPwdTwoActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            case Constants.OTP_FIRST_HyperPay_ADD_MONEY:
                if ("Y".equals(paySecretRequired)) {
                    ConfirmAddMoneyActivity.getConfirmAddMoneyActivity().jumpToPwd(false);
                    finish();
                }else if(riskControlEntity.isNeedIvr()){
                    ConfirmAddMoneyActivity.getConfirmAddMoneyActivity().jumpToPwd(true);
                    finish();
                } else {
                    checkTransfer();
                }
                break;
            default:
                break;
        }
    }

    private void showTrustDeviceDialog(UserInfoEntity userInfoEntity) {
        this.mUserInfoEntity = userInfoEntity;
        TrustDeviceDialog trustDeviceDialog = new TrustDeviceDialog(mContext);
        trustDeviceDialog.setTrustDeviceListener(new TrustDeviceDialog.TrustDeviceListener() {
            @Override
            public void trust() {
                showProgress(true);
                mPresenter.trustDevice();
            }

            @Override
            public void skip() {
                successLogin();
            }
        });
        trustDeviceDialog.setCancelable(false);
        trustDeviceDialog.setCanceledOnTouchOutside(false);
        trustDeviceDialog.show();
    }

    private void successLogin() {
        if (Constants.OTP_FIRST_LOGIN.equals(otpType)) {
            LoginFastNewActivity.getLoginFastNewActivity().successLogin(mUserInfoEntity);
        } else {
            LoginFastNewActivity.getLoginFastNewActivity().successLogin(mUserInfoEntity);
        }
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public RegisterOTPContract.Presenter getPresenter() {
        return new RegisterOTPPresenter();
    }

    @Override
    public void stopAccountSuccess(Void result) {

    }

    @Override
    public void stopAccountFail(String errorCode, String errorMsg) {

    }

    @Override
    public void loginAccountSuccess(UserInfoEntity userInfoEntity) {
        mUserInfoEntity = userInfoEntity;
//        successLogin();
        if (riskControlEntity != null && Constants.USER_STATUS_S_LOGIN.equals(riskControlEntity.triggerRiskReason)) {
            successLogin();
        } else {
            showTrustDeviceDialog(userInfoEntity);
        }
    }

    @Override
    public void loginAccountError(String errorCode, String errorMsg) {
        showProgress(false);
        tvErrorOtp.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(errorMsg)) {
            tvErrorOtp.setText(errorMsg);
        }
        clearOtpInput();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,sceneType,transferEntity,true);
    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        transferToContactSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void transferToContactFail(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        }else {
            showTipDialog(errMsg);
        }
    }

    @Override
    public void confirmPaySuccess(TransferEntity transferEntity) {
        confirmPaySuccess(this,sceneType,checkOutEntity,isFromSDK,transferEntity);
    }

    @Override
    public void confirmPayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {
        splitBillSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void trustDeviceFail(String errorCode, String errorMsg) {
        showProgress(false);
        showTipDialog(errorMsg);
    }

    @Override
    public void trustDeviceSuccess(Void result) {
        showProgress(false);
        successLogin();
    }

    @Override
    public void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity) {
        if(imrAddBeneResultEntity != null){
            imrBeneficiaryEntity.payeeInfoId = imrAddBeneResultEntity.payeeInfoId;
        }
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_ADD_BENE_SUCCESS, imrBeneficiaryEntity,riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrAddBeneficiaryFail(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void imrEditBeneficiarySuccess(ResponseEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_EDIT_BENE_SUCCESS, beneficiaryInfo,riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrEditBeneficiaryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getKsaCardPayResultSuccess(KsaPayResultEntity ksaPayResultEntity) {
        if (ksaPayResultEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksa_cardType, ksaPayResultEntity.cardType);
            if (ksaCardSummaryEntity != null) {
                mHashMap.put(Constants.ksa_deliveryTime, ksaCardSummaryEntity.deliveryTime);
            }
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void getKsaCardPayResultFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

//    @Override
//    public void openKsaCardNoFeeSuccess(KsaPayResultEntity result) {
//        HashMap<String,Object> mHashMap = new HashMap<>();
//        mHashMap.put(Constants.ksa_cardType,Constants.ksa_card_VIRTUAL);
//        ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//        finish();
//    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {
        if (Constants.TYPE_KSA_CARD_BLOCK.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_BLOCK));
        }
        ProjectApp.removeAllTaskExcludeMainStack();
        finish();
    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_SET_LIMIT_SUCCESS));
        finish();
    }

    @Override
    public void getKsaCardDetailsSuccess(CardDetailEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_SHOW_DETAIL, result));
        finish();
    }

    @Override
    public void activateCardSuccess(Void result) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.ksaCardEntity, ksaCardEntity);
        mHashMap.put(Constants.cardSuccessType, 3);
        ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if ("030194".equals(errorCode)) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.cardResultType, KsaCardResultActivity.cardResultType_Open_Card_fail);
            mHashMap.put(Constants.cardResultErrorMsg, errorMsg);
            ActivitySkipUtil.startAnotherActivity(this, KsaCardResultActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            showTipDialog(errorMsg);
        }
    }

    /**
     * 交易验证
     */
    private void checkTransfer() {
        if (Constants.TYPE_PAY_CARD.equals(sceneType)) {
            mPresenter.getKsaCardPayResult(ksaCardSummaryEntity.vat, ksaCardSummaryEntity.totalAmount, ksaCardSummaryEntity.openCardReqEntity);
        }else if (Constants.TYPE_CHARITY.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        }else if (Constants.TYPE_HyperPay_ADD_MONEY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_HyperPay_ADD_MONEY));
            finish();
        }else if (Constants.TYPE_CHANGE_LOGIN_PD.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_CHANGE_LOGIN_PD));
            finish();
        }else if (Constants.TYPE_WITHDRAW_ADD_BENEFICIARY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_WITHDRAW_ADD_BENEFICIARY));
            finish();
        }else if (Constants.TYPE_ADD_SEND_BENE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ADD_SEND_BENEFICIARY));
            finish();
        } else if (Constants.TYPE_KSA_CARD_SET_LIMIT.equals(sceneType)) {
            String txnType = mCardLimitConfigEntity.txnType;
            String limitMoney = mCardLimitConfigEntity.isDaily ? mCardLimitConfigEntity.dailyLimitValue : mCardLimitConfigEntity.monthlyLimitValue;
            mPresenter.setKsaCardLimit(txnType, mCardLimitConfigEntity.isDaily ? AndroidUtils.getReqTransferMoney(limitMoney) : "",
                    mCardLimitConfigEntity.isDaily ? "" : AndroidUtils.getReqTransferMoney(limitMoney), mCardLimitConfigEntity.proxyCardNo, mCardLimitConfigEntity.up);
        } else if (Constants.TYPE_KSA_CARD_FREEZE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_FREEZE));
            finish();
//            mPresenter.setKsaCardStatus(Constants.TYPE_CARD_FREEZE,ksaCardEntity.proxyCardNo,"");
        } else if (Constants.TYPE_KSA_CARD_ACTIVATE.equals(sceneType)) {
            mPresenter.activateCard(ksaCardEntity.proxyCardNo, ksaCardEntity.cardNumber, ksaCardEntity.expiryTime);
        } else if (Constants.TYPE_KSA_CARD_DETAIL.equals(sceneType)) {
            mPresenter.getKsaCardDetails(ksaCardEntity.proxyCardNo);
        } else if (Constants.TYPE_KSA_CARD_SET_CHANNEL.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_SET_CHANNEL));
            finish();
        } else if (Constants.TYPE_KSA_CARD_UNFREEZE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_FREEZE));
            finish();
//            mPresenter.setKsaCardStatus(Constants.TYPE_CARD_UNFREEZE,ksaCardEntity.proxyCardNo,"");
        } else if (Constants.TYPE_KSA_CARD_SET_PIN.equals(sceneType)) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            mHashMap.put(Constants.proxyCardNo, proxyCardNo);
            ActivitySkipUtil.startAnotherActivity(RegisterOtpActivity.this, GetCardPwdOneActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.TYPE_KSA_CARD_BLOCK.equals(sceneType)) {
            mPresenter.setKsaCardStatus(Constants.TYPE_CARD_BLOCK, proxyCardNo, blockCardReason,blockCardReasonStatus);
        } else if (Constants.TYPE_PAY_CARD_NO_FEE.equals(sceneType)) {
//            mPresenter.openKsaCardNoFee(openCardReqEntity);
            mPresenter.getKsaCardPayResult("", "", openCardReqEntity);
        } else if (Constants.TYPE_IMR_PAY.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_INTER_TOP_UP.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_PAY_BILL.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_SPLIT.equals(sceneType)) {
            if (transferEntity != null) {
                mPresenter.splitBill(transferEntity.orderNo, AndroidUtils.getReqTransferMoney(transferEntity.orderAmount),
                        transferEntity.orderCurrencyCode, transferEntity.remark,
                        transferEntity.receiverInfoList);
            }
        }else if (Constants.TYPE_IMR_ADD_BENEFICIARY.equals(sceneType)) {
            if (isFromEditBeneficiary) {
                beneficiaryInfo = (ImrBeneficiaryDetails.ImrBeneficiaryDetail) getIntent().getSerializableExtra("BeneficiaryInfo");
                mPresenter.saveImrBeneficiaryInfo(beneficiaryInfo);
            } else {
                mPresenter.imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                        nickName, relationshipCode, imrCallingCode, phone,
                        transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                        birthDate, sex, cityName, districtName,
                        poBox, buildingNo, street, idNo, idExpiry,
                        bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
                        cityId, beneCurrency, channelPayeeId, channelCode,branchId);
            }
        } else if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
            if (kycContactEntity != null) {
                mPresenter.transferToContact(kycContactEntity.getUserId(), SpUtils.getInstance().getCallingCode(), kycContactEntity.getRequestPhoneNumber(), userInfoEntity.userId,
                        userInfoEntity.getPhone(), AndroidUtils.getReqTransferMoney(kycContactEntity.payMoney), UserInfoManager.getInstance().getCurrencyCode(),
                        kycContactEntity.remark, sceneType, "", "1", "", "", "", payerName);
            }
        } else {
            if (Constants.TYPE_TRANSFER_RC.equals(sceneType)) {
                mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            } else if (Constants.TYPE_SCAN_MERCHANT.equals(sceneType)) {
                mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
            } else {
                mPresenter.transferSurePay(sceneType, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            }
        }
    }




    @Override
    public void onReceived(String message) {
        if ("6".equals(codeLength)) {
            veriftOtpKyc.setCode(message);
        }else {
            veriftOtp.setCode(message);
        }

    }
}
