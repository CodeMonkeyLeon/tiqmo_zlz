package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillSkuListAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class PayBillSkuActivity extends BaseCompatActivity {

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_select_sku;
    }

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.rv_sku_list)
    RecyclerView rvSkuList;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    @BindView(R.id.id_sideBar)
    SideBar sideBar;
    @BindView(R.id.con_recycler)
    ConstraintLayout conRecycler;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_sku_head)
    LinearLayout llSkuHead;
    @BindView(R.id.iv_biller)
    RoundedImageView ivBiller;
    @BindView(R.id.tv_biller_name)
    TextView tvBillerName;
    @BindView(R.id.tv_sku_title)
    TextView tvSkuTitle;
//    @BindView(R.id.sc_content)
//    NestedScrollView scContent;

    private PayBillSkuListAdapter payBillSkuListAdapter;

    private List<PayBillSkuEntity> showBillSkuList = new ArrayList<>();
    private List<PayBillSkuEntity> filterBillSkuList = new ArrayList<>();

    private String searchString;

    private String billerId, billerName, billerLogo;
    private PayBillSkuListEntity payBillSkuListEntity;

    private String mChannelCode;

    private int sceneType;



        public static void startPayBillSkuActivity(Activity fromActivity,
                                                     String billerId,
                                                     String billerName,
                                                     String billerLogo,
                                                     String channelCode,
                                                     PayBillSkuListEntity payBillSkuListEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.paybill_billerId, billerId);
        mHashMap.put(Constants.paybill_billerName, billerName);
        mHashMap.put(Constants.paybill_billerLogo, billerLogo);
        mHashMap.put(Constants.paybill_billerSkuListEntity, payBillSkuListEntity);
        mHashMap.put(Constants.CHANNEL_CODE, channelCode);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillSkuActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }



    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.Card_limits_2);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        tvTitle.setVisibility(View.INVISIBLE);
        llSkuHead.setVisibility(View.VISIBLE);
        if (getIntent() != null && getIntent().getExtras() != null) {
            billerId = getIntent().getExtras().getString(Constants.paybill_billerId);
            billerName = getIntent().getExtras().getString(Constants.paybill_billerName);
            billerLogo = getIntent().getExtras().getString(Constants.paybill_billerLogo);
            payBillSkuListEntity = (PayBillSkuListEntity) getIntent().getExtras().getSerializable(Constants.paybill_billerSkuListEntity);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);
            if (payBillSkuListEntity != null) {
                showBillSkuList.addAll(payBillSkuListEntity.skuList);
                sceneType = payBillSkuListEntity.sceneType;
            }

            tvBillerName.setText(billerName);
            Glide.with(mContext)
                    .load(billerLogo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.utilities))
                    .into(ivBiller);

        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        rvSkuList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvSkuList.setLayoutManager(manager);
        payBillSkuListAdapter = new PayBillSkuListAdapter(showBillSkuList);
        if (sceneType == 2) {
            //branch
            tvSkuTitle.setText(getString(R.string.BillPay_13));
            etSearch.setHint(getString(R.string.BillPay_14));
            payBillSkuListAdapter.setType(2);
        } else if (sceneType == 4) {
            //plan
            tvSkuTitle.setText(getString(R.string.BillPay_15));
            etSearch.setHint(getString(R.string.BillPay_16));
            payBillSkuListAdapter.setType(1);
        }
        payBillSkuListAdapter.bindToRecyclerView(rvSkuList);
        payBillSkuListAdapter.setDataList(showBillSkuList);

        payBillSkuListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(2000)) {
                    return;
                }
                PayBillSkuEntity payBillSkuEntity = payBillSkuListAdapter.getDataList().get(position);
                if (payBillSkuEntity != null) {
//                    HashMap<String, Object> mHashMap = new HashMap<>();
//                    mHashMap.put(Constants.paybill_billerId, billerId);
//                    mHashMap.put(Constants.paybill_billerName, billerName);
//                    mHashMap.put(Constants.paybill_billerLogo, billerLogo);
//                    mHashMap.put(Constants.paybill_billerSku, payBillSkuEntity.sku);
//                    mHashMap.put(Constants.CHANNEL_CODE, mChannelCode);
//                    ActivitySkipUtil.startAnotherActivity(PayBillSkuActivity.this, PayBillFetchActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

                    PayBillFetchActivity.startPayBillFetchActivity(PayBillSkuActivity.this,
                            billerId,
                            billerName,
                            billerLogo,
                            payBillSkuEntity.sku,
                            mChannelCode);
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etSearch.hideErrorView();
                searchString = s.toString();
                if (showBillSkuList != null && showBillSkuList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            filterBillSkuList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
//                sideBar.setVisibility(View.VISIBLE);
                filterBillSkuList.clear();
                payBillSkuListAdapter.setDataList(showBillSkuList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (showBillSkuList != null && showBillSkuList.size() > 0) {
                int size = showBillSkuList.size();
                for (int i = 0; i < size; i++) {
                    PayBillSkuEntity payBillSkuEntity = showBillSkuList.get(i);
                    String name = payBillSkuEntity.description;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterBillSkuList.add(payBillSkuEntity);
                    }
                }
                payBillSkuListAdapter.setDataList(filterBillSkuList);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @OnClick({R.id.iv_back, R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search:
                conEtSearch.setVisibility(View.VISIBLE);
                conTvSearch.setVisibility(View.GONE);
                etSearch.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
                conEtSearch.setVisibility(View.GONE);
                conTvSearch.setVisibility(View.VISIBLE);
                etSearch.setContentText("");
                break;
            default:
                break;
        }
    }
}
