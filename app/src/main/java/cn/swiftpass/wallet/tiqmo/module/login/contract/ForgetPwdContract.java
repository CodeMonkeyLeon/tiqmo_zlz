package cn.swiftpass.wallet.tiqmo.module.login.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public class ForgetPwdContract {
    public interface View extends BaseView<ForgetPwdContract.Presenter> {
        void resetPwdSuccess(Void response);
        void resetPwdFail(String errorCode,String errorMsg);
    }


    public interface Presenter extends BasePresenter<ForgetPwdContract.View> {
        void resetPwd(String callingCode, String phone, String password);
    }
}
