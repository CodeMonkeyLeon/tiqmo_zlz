package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.UUID;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BitmapUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.DirectoryHelper;
import cn.swiftpass.wallet.tiqmo.widget.CropImageView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;


//专门裁剪activity
public class CropImageActivity extends BaseCompatActivity {
    public static final int RESULT_CODE = CropImageActivity.class.hashCode();
    public static final String INTENT_EXTRA_IMAGE_PATH = "ImagePath";

    public static void startActivityForResult(Activity activity, Uri imageUri) {
        if (imageUri == null || TextUtils.isEmpty(imageUri.toString())) {
            return;
        }
        Intent intent = new Intent(activity, CropImageActivity.class);
        intent.putExtra(INTENT_EXTRA_IMAGE_PATH, imageUri);
        activity.startActivityForResult(intent, 0);
    }

    public static void startActivityForResult(Fragment fragment, Uri imageUri) {
        if (imageUri == null || TextUtils.isEmpty(imageUri.toString())) {
            return;
        }

        Intent intent = new Intent(fragment.getContext(), CropImageActivity.class);
        intent.putExtra(INTENT_EXTRA_IMAGE_PATH, imageUri);
        fragment.startActivityForResult(intent, 0);
    }

    private CropImageView mCropImageView;//裁剪的工作本质上都是由CropImageView去做
    private TextView mTvCancel;
    private TextView mTvDone;


    @Override
    protected int getLayoutID() {
        return R.layout.activity_crop_image;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        mCropImageView = findViewById(R.id.CropActivity_mCropImageView);
        mCropImageView.setCircle(true);
        mTvCancel =  findViewById(R.id.mTvCancel);
        mTvDone =  findViewById(R.id.mTvDone);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
        } else {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setNavigationBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
//        setSystemUiMode(SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        Uri imageUri = getIntent().getParcelableExtra(INTENT_EXTRA_IMAGE_PATH);
        if (imageUri == null || TextUtils.isEmpty(imageUri.toString())) {
            finish();
            return;
        }
        //加载图片
        Glide.with(this).load(imageUri).into(mCropImageView);

        mTvCancel.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });

        mTvDone.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                crop();
            }
        });
    }

    private void crop() {
        Bitmap bitmap = mCropImageView.crop();//CropImageView可以直接获取到裁剪的内容
        String fileName = UUID.randomUUID().toString()+".jpg";
        Uri pathUri = BitmapUtil.saveBitmapCompat(this, bitmap, DirectoryHelper.getExternalImagePath(), fileName, false);
        bitmap.recycle();
        //保存并将结果返回
        if (pathUri != null) {
            Intent intent = new Intent();
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                intent.putExtra(INTENT_EXTRA_IMAGE_PATH, AndroidUtils.getPathFromUriApiQ(mContext, pathUri));
            } else {
                intent.putExtra(INTENT_EXTRA_IMAGE_PATH, DirectoryHelper.getExternalImagePath() + fileName);
            }
            setResult(RESULT_CODE, intent);
        } else {
            LogUtil.e("save image fail");
        }
        finish();
    }

}





























