package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.gc.gcchat.GcChatConnectionStatus;
import com.gc.gcchat.GcChatError;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatUpdateUserProfileRequest;
import com.gc.gcchat.GcChatUser;
import com.gc.gcchat.callback.ConnectionCallback;
import com.gc.gcchat.callback.GcChatRestoreBackupCallback;
import com.gc.gcchat.callback.GcChatSimpleCallback;
import com.gc.gcchat.callback.GetUnReadMessageCountCallback;
import com.gc.gcchat.callback.InitCallback;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.chat.adapter.ViewPagerAdapter;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatHistoryEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.GcChatErrorUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class ChatFragment extends BaseFragment {
    @BindView(R.id.bg_main)
    LinearLayout bgMain;
    @BindView(R.id.ll_chat_top)
    LinearLayout llChatTop;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_search)
    EditTextWithDel etSearch;
    @BindView(R.id.con_et_search)
    ConstraintLayout conEtSearch;

    @BindView(R.id.table_layout_title)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager2 viewPager;


    private List<Fragment> fragments = new ArrayList<>();

    private String[] mTabTitles = new String[3];

    private ChatHistoryEntity historyEntities;

    private AllChatFragment allChatFragment;
    private AllChatFragment oneChatFragment;
    private AllChatFragment groupChatFragment;

    private String profilePicFileId = "";
    private String lastUpdatedProfilePicFileId = "";
    private String lastUpdatedName = "";
    private String lastPhoneNumber = "";
    private String updatedName = "";

    private String phoneNumber;

    private UserInfoEntity userInfoEntity;
    private String chatUserId;

    public static ChatFragment newInstance() {
        ChatFragment fragment = new ChatFragment();
        return fragment;
    }

    @Override
    public void noticeThemeChange() {
        super.noticeThemeChange();
        ResourceHelper helper = ResourceHelper.getInstance(mContext);
        helper.setBackgroundResourceByAttr(bgMain, R.attr.bg_040f33_white);
        helper.setBackgroundResourceByAttr(llChatTop, R.attr.color_06206f_white);
        helper.setTextColorByAttr(tvTitle, R.attr.color_white_090a15);
        helper.setImageResourceByAttr(ivSearch, R.attr.icon_search_contact);
        tabLayout.setTabTextColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_50white_503a3b44)),
                mContext.getColor(ThemeSourceUtils.getSourceID(mContext,R.attr.color_white_090a15)));
        if (allChatFragment != null) {
            allChatFragment.noticeThemeChange();
        }
        if (oneChatFragment != null) {
            oneChatFragment.noticeThemeChange();
        }
        if (groupChatFragment != null) {
            groupChatFragment.noticeThemeChange();
        }
    }


    @Override
    protected int getLayoutID() {
        return R.layout.fragment_chat;
    }

    @Override
    protected void initView(View parentView) {
        tvTitle.setText(R.string.sprint20_1);
        ivBack.setVisibility(View.GONE);
        mTabTitles[0] = mContext.getString(R.string.sprint20_2);
        mTabTitles[1] = mContext.getString(R.string.sprint20_3);
        mTabTitles[2] = mContext.getString(R.string.sprint20_4);
        showChatFragment();
        userInfoEntity = getUserInfoEntity();
        if(userInfoEntity != null){
            updatedName = userInfoEntity.getFirstName();
            chatUserId = userInfoEntity.userId;
            phoneNumber = userInfoEntity.phone;
//            chatUserId = "533456789";
//            chatUserId = "533456785";
        }
        initChatSdk();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {
        updateProfileAndStartChatList();
        if(allChatFragment != null && allChatFragment.isAdded()){
            allChatFragment.restart();
        }
        if(oneChatFragment != null && oneChatFragment.isAdded()){
            oneChatFragment.restart();
        }
        if(groupChatFragment != null && groupChatFragment.isAdded()){
            groupChatFragment.restart();
        }
    }

    private void initChatSdk(){
        try {
            if (GcChatSDK.isInitializedSuccessfully()) {
                if (!isChatConnected) {
                    showProgressCancelable(true,true);
                    loginAndInitialize(chatUserId);
                }else{
                    updateProfileAndStartChatList();
                }
            } else {
                showProgressCancelable(true,true);
                GcChatSDK.init(mContext, new InitCallback() {
                    @Override
                    public void onSuccessful() {
                        loginAndInitialize(chatUserId);
                    }

                    @Override
                    public void onFailure(int errorCode) {
                        showProgress(false);
                        showTipDialog("Error in Initializing. - " + GcChatErrorUtils.getErrorMessage(errorCode));
                    }
                });
            }
        }catch (Throwable e){
            showProgress(false);
            e.printStackTrace();
        }
    }

    private void showChatFragment(){
        allChatFragment = AllChatFragment.newInstance(1);
        oneChatFragment = AllChatFragment.newInstance(2);
        groupChatFragment = AllChatFragment.newInstance(3);

        fragments.add(allChatFragment);
        fragments.add(oneChatFragment);
        fragments.add(groupChatFragment);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity(), fragments);
        viewPager.setAdapter(adapter);
        new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                try {
                    tab.setText(mTabTitles[position]);
                }catch (Exception e){

                }
            }
        }).attach();
    }

    @OnClick({ R.id.iv_search, R.id.iv_close})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_search:
//                conEtSearch.setVisibility(View.VISIBLE);
//                conTvSearch.setVisibility(View.GONE);
//                etSearch.getEditText().requestFocus();
//                AndroidUtils.showKeyboardView(etSearch.getEditText());
                break;
            case R.id.iv_close:
//                conEtSearch.setVisibility(View.GONE);
//                conTvSearch.setVisibility(View.VISIBLE);
//                etSearch.setContentText("");
                break;
        }
    }

    private void updateProfileAndStartChatList() {
        userInfoEntity = getUserInfoEntity();
        updatedName = userInfoEntity.getFirstName();
        phoneNumber = userInfoEntity.phone;
        profilePicFileId = userInfoEntity.avatar;
        if ((!TextUtils.isEmpty(profilePicFileId) && !lastUpdatedProfilePicFileId.equals(profilePicFileId))
                || (!TextUtils.isEmpty(updatedName) && !lastUpdatedName.equals(updatedName))
                ||(!TextUtils.isEmpty(phoneNumber) && !lastPhoneNumber.equals(phoneNumber))) {
            GcChatSDK.updateUserProfile(
                    new GcChatUpdateUserProfileRequest()
                            .setPhoneNumber(phoneNumber)
                            .setName(updatedName)
                            .setProfilePicture(profilePicFileId)
                    , new GcChatSimpleCallback() {
                        @Override
                        public void onSuccessful() {
                            lastUpdatedProfilePicFileId = profilePicFileId;
                        }

                        @Override
                        public void onFailure(int errorCode) {
                            showTipDialog("Error in updating profile!! - " + GcChatErrorUtils.getErrorMessage(errorCode));
                        }
                    }
            );
        }
    }

    private void connectChatSDK(String phone){
        GcChatSDK.connect(ChatHelper.getInstance().getJwtTokenRT(phone), new ConnectionCallback() {
            @Override
            public void onConnected(@NonNull GcChatUser gcChatUser) {
                SpUtils.getInstance().setExternalId(gcChatUser.getExternalId());
                isChatConnected = true;
                showProgress(false);
//                showChatFragment();
                GcChatSDK.getUnreadSessionCount(new GetUnReadMessageCountCallback() {
                    @Override
                    public void onSuccessful(int i) {
                        LogUtils.d("getUnreadSessionCount Successful %s", i);
                        if (gcChatUser.getFileId() != null && !gcChatUser.getFileId().isEmpty()) {
                            profilePicFileId = gcChatUser.getFileId();
                            lastUpdatedProfilePicFileId = profilePicFileId;
                        }
                        if (!TextUtils.isEmpty(gcChatUser.getName())) {
                            lastUpdatedName = gcChatUser.getName();
                        }
                        if (!TextUtils.isEmpty(gcChatUser.getPhoneNumber())) {
                            lastPhoneNumber = gcChatUser.getPhoneNumber();
                        }
                        updateProfileAndStartChatList();
                    }

                    @Override
                    public void onFailure(int errorCode) {
                        showProgress(false);
                        LogUtil.v("getUnreadSessionCount onFailure - " + GcChatErrorUtils.getErrorMessage(errorCode));
                    }
                });
            }

            @Override
            public void onConnectionUpdate(int i) {
                showProgress(false);
                LogUtils.d("onConnectionUpdate %s", i);
                if (i == GcChatConnectionStatus.CONNECTED) {
                    LogUtil.d("gc chat status updated to CONNECTED");
                } else if (i == GcChatConnectionStatus.RECONNECTING) {
                    isChatConnected = false;
                    LogUtil.d("gc chat status updated to RECONNECTING");
//                    showTipDialog("Reconnecting!!");
                    connectChatSDK(chatUserId);
                } else if (i == GcChatConnectionStatus.DISCONNECTED) {
                    isChatConnected = false;
                    LogUtil.d("gc chat status updated to DISCONNECTED");
                    connectChatSDK(chatUserId);
                }
            }


            @Override
            public void onDisconnected(int i) {
                isChatConnected = false;
                showProgress(false);
                if (i == GcChatError.INTERNET_ISSUE
                        || i == GcChatError.AUTHENTICATION) {
                } else if (i == GcChatError.BACKUP_AVAILABLE) {
                    showProgress(false);
                } else {
                    showTipDialog("Error onDisconnected - " + GcChatErrorUtils.getErrorMessage(i));
                }
                connectChatSDK(chatUserId);
            }
        });
    }

    private void loginAndInitialize(String phone){
        GcChatSDK.checkIfRestoreRequired(ChatHelper.getInstance().getJwtTokenRT(phone), new GcChatRestoreBackupCallback() {
            @Override
            public void onSuccessful(boolean isBackupAvailable) {
                showProgress(false);
                LogUtils.d("isBackupAvailable %s", isBackupAvailable);
                if (isBackupAvailable) {
                    showProgress(false);
                } else {
                    connectChatSDK(phone);
                }
            }

            @Override
            public void onFailure(int errorCode) {
                showProgress(false);
                showTipDialog("Error in backup check. - " + GcChatErrorUtils.getErrorMessage(errorCode));
            }
        });
    }

}
