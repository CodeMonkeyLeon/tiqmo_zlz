package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SelectTopUpAdapter extends BaseRecyclerAdapter<EVoucherEntity.StoreInfo> {

    public SelectTopUpAdapter(@Nullable ArrayList<EVoucherEntity.StoreInfo> data) {
        super(R.layout.image_view_item, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, EVoucherEntity.StoreInfo item, int position) {
        if (item == null) {
            return;
        }
        ImageView logo = baseViewHolder.getView(R.id.iv_logo_item);
        String logoUrl = ThemeUtils.isCurrentDark(mContext) ? item.voucherBrandDarkLogo : item.voucherBrandLightLogo;
        if (!TextUtils.isEmpty(logoUrl)) {
            Glide.with(mContext).clear(logo);
            Glide.with(mContext)
                    .load(logoUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(logo);
        }
    }

    @Override
    public OnItemClickListener getOnItemClickListener() {
        return super.getOnItemClickListener();
    }
}
