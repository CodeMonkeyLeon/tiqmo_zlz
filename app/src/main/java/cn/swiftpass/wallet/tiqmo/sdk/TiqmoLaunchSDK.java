package cn.swiftpass.wallet.tiqmo.sdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.module.setting.dialog.UpdateDialog;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AppManageEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class TiqmoLaunchSDK {

    private static final String KEY_LAST_UPDATE_HINT_TIME = "LastUpdateHintTime";

    public static void showUpdateIfNeeded(Context context, ConfigEntity configEntity) {
        if (context == null || configEntity == null) {
            return;
        }
        AppManageEntity appManageEntity = configEntity.appManageInfo;
        if (appManageEntity != null) {
            startUpdate(context, appManageEntity);
//            String lastUpdateTime = AppClient.getInstance().getStorageHelper().getFromConfigurationPreferences(KEY_LAST_UPDATE_HINT_TIME);
//            Calendar now = Calendar.getInstance();
//            Calendar temp = Calendar.getInstance();
//            now.setTimeInMillis(0);
//            now.set(temp.get(Calendar.YEAR), temp.get(Calendar.MONTH), temp.get(Calendar.DAY_OF_MONTH));//只要年月日，其他时间归0,等于说要一个整点的时间
//
//            if (TextUtils.isEmpty(lastUpdateTime) || Integer.parseInt(appManageEntity.flag) == appManageEntity.FORCE_UPDATE) {
//                startUpdate(context, appManageEntity, now.getTimeInMillis());
//            } else {
//                Calendar last = Calendar.getInstance();
//                last.setTimeInMillis(Long.parseLong(lastUpdateTime));
//                if ((now.getTimeInMillis() - last.getTimeInMillis() >= AppConfig.PROMPT_USER_UPDATE_INTERVAL * 24 * 60 * 60 * 1000)) {
//                    startUpdate(context, appManageEntity, now.getTimeInMillis());
//                }
//            }
        }
    }

    /**
     * 更新版本号
     *
     * @param context
     */
    public static void saveAppVersion(Context context) {
        SpUtils.getInstance().setAppVersionName(context);

    }

    public static void startUpdate(Context mContext, AppManageEntity appManageEntity) {
        String flag = appManageEntity.flag;
        if ("3".equals(flag)) {
            UpdateDialog updateDialog = UpdateDialog.getInstance(mContext, appManageEntity);
            updateDialog.setCancelable(false);
            updateDialog.setCanceledOnTouchOutside(false);
            updateDialog.setUpdateListener(new UpdateDialog.UpdateListener() {
                @Override
                public void update() {
                    if (appManageEntity != null) {
                        startWebDownload(mContext, appManageEntity.installUrl);
                    }
                }

                @Override
                public void logout() {
                    if ("3".equals(flag)) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                    } else {
                        updateDialog.dismiss();
                    }
                }
            });
            updateDialog.show();
        }
    }

    public static void startUpdate(Context mContext, AppManageEntity appManageEntity, final long nowTime) {
        if (appManageEntity != null) {
            String version = appManageEntity.version;
            String flag = appManageEntity.flag;
            if (!TextUtils.isEmpty(flag)) {
                if (checkVersionNotSame(mContext, version)) {
                    UpdateDialog updateDialog = UpdateDialog.getInstance(mContext, appManageEntity);
                    updateDialog.setCanceledOnTouchOutside(false);
                    updateDialog.setUpdateListener(new UpdateDialog.UpdateListener() {
                        @Override
                        public void update() {
                            if (appManageEntity != null) {
                                startWebDownload(mContext, appManageEntity.installUrl);
                            }
                        }

                        @Override
                        public void logout() {
                            if (nowTime >= 0) {
                                AppClient.getInstance().getStorageHelper().putToConfigurationPreferences(KEY_LAST_UPDATE_HINT_TIME, String.valueOf(nowTime));
                            }
                            if ("3".equals(flag)) {
                                android.os.Process.killProcess(android.os.Process.myPid());
                            } else {
                                updateDialog.dismiss();
                            }
                        }
                    });
                    //强制升级
//                    if("2".equals(flag) || "3".equals(flag)) {
//                    HashMap<String,Object> mHashMap = new HashMap<>();
//                    mHashMap.put(Constants.appManageEntity,appManageEntity);
//                    ActivitySkipUtil.startAnotherActivity((Activity) mContext, UpdateActivity.class,mHashMap,ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                        }
                    updateDialog.showWithBottomAnim();
//                    }
                }
            }
        }
    }

    public static void startWebDownload(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri content_url = Uri.parse(url);
        intent.setData(content_url);
        context.startActivity(intent);
    }

    /**
     * 比较两个版本号
     *
     * @return
     */
    public static boolean checkVersionNotSame(Context context, String versionName) {
        String preVersionName = SpUtils.getInstance().getAppVersionName();
        return !versionName.equals(preVersionName);
//        PackageInfo pInfo = null;
//        try {
//            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
//        } catch (PackageManager.NameNotFoundException var5) {
//            var5.printStackTrace();
//        }
//        boolean flag = false;
//        String versionNum = pInfo.versionName;
//        if (preVersionName == null) {
//            flag = true;
//        } else if (!preVersionName.equals(versionNum)) {
//            flag = true;
//        } else {
//            flag = false;
//        }
    }
}
