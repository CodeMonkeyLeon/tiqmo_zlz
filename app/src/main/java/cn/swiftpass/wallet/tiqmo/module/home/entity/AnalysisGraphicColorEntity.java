package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 环色占比
 */
public class AnalysisGraphicColorEntity extends BaseEntity {
    //	类别Id
    public String spendingCategoryId;
    //类别名称
    public String spendingCategoryName;
    //类别支出比例(比例已乘以100)
    public String spendingCategoryRatio;
//    //支出类别总笔数
//    public String spendingCategoryTxnCount;
//    //支出类别金额
//    public String spendingCategoryAmount;
//    //支出类别币种
//    public String spendingCategoryCurrency;

    /**
     * 测试数据
     * @return
     */
    public List<AnalysisGraphicColorEntity> getList(){
        List<AnalysisGraphicColorEntity> list = new ArrayList<>();
        for(int i=0;i<12;i++){
            AnalysisGraphicColorEntity analysisGraphicColorEntity = new AnalysisGraphicColorEntity();
            if(i != 4) {
                analysisGraphicColorEntity.spendingCategoryId = i + 1 + "";
                analysisGraphicColorEntity.spendingCategoryRatio = "8";
                list.add(analysisGraphicColorEntity);
            }
        }
        return list;
    }
}
