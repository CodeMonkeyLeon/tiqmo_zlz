package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PaybillLocalAdapter extends BaseRecyclerAdapter<PayBillTypeEntity> {
    public PaybillLocalAdapter(@Nullable List<PayBillTypeEntity> data) {
        super(R.layout.item_paybill_local,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillTypeEntity payBillTypeEntity, int position) {
        //当渠道是=Sadad的时候返回。父分类名称子描述
        String billerTypeSub = payBillTypeEntity.billerTypeShow;
        //当渠道是=Sadad的时候返回。父分类名称子描述-展示用
        String billerTypeSubDescriptionShow = payBillTypeEntity.billerTypeSubDescriptionShow;

        baseViewHolder.setText(R.id.tv_bill_name,billerTypeSub);
        baseViewHolder.setText(R.id.tv_bill_content,billerTypeSubDescriptionShow);
        RoundedImageView ivBillService = baseViewHolder.getView(R.id.iv_bill_service);
        String typeLogo = payBillTypeEntity.imgUrl;
        Glide.with(mContext)
                .load(typeLogo)
                .dontAnimate()
                .format(DecodeFormat.PREFER_RGB_565)
                .placeholder(ThemeSourceUtils.getSourceID(mContext,R.attr.utilities))
                .into(ivBillService);
    }
}
