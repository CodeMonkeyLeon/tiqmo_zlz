package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardServiceEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class CardServiceAdapter extends BaseRecyclerAdapter<CardServiceEntity> {

    public CardServiceAdapter(@Nullable List<CardServiceEntity> data) {
        super(R.layout.item_card_service,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, CardServiceEntity cardServiceEntity, int position) {
        baseViewHolder.setImageResource(R.id.iv_icon, ResourceHelper.getInstance(mContext).getIdentifierByAttrId(cardServiceEntity.imgId));
        baseViewHolder.setBackgroundRes(R.id.rl_service, ThemeSourceUtils.getSourceID(mContext, R.attr.shape_091b57_white));
        baseViewHolder.setText(R.id.tv_icon, cardServiceEntity.textId);
        baseViewHolder.setTextColor(R.id.tv_icon, ResourceHelper.getInstance(mContext).getColorByAttr(R.attr.color_white_3a3b44));
    }
}
