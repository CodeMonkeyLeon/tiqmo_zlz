package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;
import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetHelpActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.ExtendPropertiesEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillCheckoutPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CheckBalanceDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.PayBillErrorDialog;

public class PayBillDetailActivity extends BaseCompatActivity<PayBillContract.BillOrderCheckoutPresenter> implements PayBillContract.BillOrderCheckoutView {

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_detail;
    }


    private static final String ERROR_AMOUNT_ONE = "080039";
    private static final String ERROR_AMOUNT_TWO = "080040";

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_get_help)
    TextView tvGetHelp;
    @BindView(R.id.con_tv_search)
    ConstraintLayout conTvSearch;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_biller)
    RoundedImageView ivBiller;
    @BindView(R.id.tv_biller_name)
    TextView tvBillerName;
    @BindView(R.id.tv_first_money)
    TextView tvFirstMoney;
    @BindView(R.id.tv_second_money)
    TextView tvSecondMoney;
    @BindView(R.id.tv_total_title)
    TextView tvTotalTitle;
    @BindView(R.id.tv_total_amount_pay)
    TextView tvTotalAmountPay;
    @BindView(R.id.tv_error_edit)
    TextView tvErrorEdit;
    @BindView(R.id.tv_show_more)
    TextView tvShowMore;
    @BindView(R.id.iv_show_more)
    ImageView ivShowMore;
    @BindView(R.id.ll_show_more)
    LinearLayout llShowMore;
    @BindView(R.id.tv_bill_amount)
    TextView tvBillAmount;
    @BindView(R.id.tv_amount_title)
    TextView tvAmountTitle;
    @BindView(R.id.con_bill_amount)
    ConstraintLayout conBillAmount;
    @BindView(R.id.tv_biller_fees)
    TextView tvBillerFees;
    @BindView(R.id.tv_discount_money)
    TextView tvDiscountMoney;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.ll_money_detail)
    LinearLayout llMoneyDetail;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_check_balance)
    TextView tvCheckBalance;
    @BindView(R.id.tv_pay)
    TextView tvPay;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.tv_edit_money)
    TextView tvEditMoney;
    @BindView(R.id.tv_bill_detail_amount)
    TextView tvBillDetailAmount;
    @BindView(R.id.ll_add_money)
    LinearLayout llAddMoney;
    @BindView(R.id.tv_reference_value)
    TextView tvReferenceValue;
    @BindView(R.id.tv_reference_name)
    TextView tvReferenceName;
    @BindView(R.id.con_reference)
    ConstraintLayout conReference;
    @BindView(R.id.con_dueto_date)
    ConstraintLayout conDuetoDate;
    @BindView(R.id.tv_dueto_date)
    TextView tvDuetoDate;
    @BindView(R.id.tv_max_money)
    TextView tvMaxMoney;
    @BindView(R.id.tv_min_money)
    TextView tvMinMoney;

    @BindView(R.id.et_change_first_amount)
    EditTextWithDel etChangeFirstAmount;
    @BindView(R.id.tv_change_first_currency)
    TextView tvChangeFirstCurrency;
    @BindView(R.id.con_edit_first_money)
    ConstraintLayout conEditFirstMoney;
    @BindView(R.id.et_change_second_amount)
    EditTextWithDel etChangeSecondAmount;
    @BindView(R.id.tv_change_second_currency)
    TextView tvChangeSecondCurrency;
    @BindView(R.id.con_edit_second_money)
    ConstraintLayout conEditSecondMoney;
    @BindView(R.id.ll_detail_amount)
    ConstraintLayout llDetailAmount;
    @BindView(R.id.ll_bill_fees)
    ConstraintLayout llBillFees;
    @BindView(R.id.ll_vat)
    ConstraintLayout llVat;
    @BindView(R.id.tv_line)
    TextView tvLine;

    @BindView(R.id.id_ll_extend_properties)
    LinearLayout mLlExtendProperties;

    @BindView(R.id.con_exchange_money)
    ConstraintLayout conExchangeMoney;

    @BindView(R.id.con_local_money)
    ConstraintLayout conLocalMoney;
    @BindView(R.id.et_local_amount)
    EditTextWithDel etLocalAmount;
    @BindView(R.id.tv_local_currency)
    TextView tvLocalCurrency;

    private PayBillOrderInfoEntity mPayBillOrderInfoEntity;

    private String totalMoney;
    private double valiableMoney;
    private double monthLimitMoney;

    //账单Biller名称
    public String billerName;
    //账单参考标识名称
    public String billReferenceName;
    //账单参考标识值
    public String billReferenceValue;
    //账单结算日
    public String billDueDate;
    //付款金额
    public String payAmount;
    //付款币种
    public String payCurrencyCode;
    //付款金额是否可编辑
    public String payAmountEditable;
    public String billAmountEditable;
    //非定额账单时不返回
    public String billAmount;
    //	账单币种
    public String billCurrencyCode;
    //汇率
    public String exchangeRate;
    //是否支持超额支付，定额账单时不返回
    public String partialPayAmountEnabled;
    //是否支持超期支付，定额账单时不返回
    public String pastDuePaymentAllowed;
    public String billAmountCustomLabel;
    //账单处理日期
    public String daysToPost;
    //账单处理日期是否按工作日
    public String isPostByBusinessDays;
    public String billerImgUrl;

    //最小可支付金额，非定额账单时可用
    public String minPayableAmount;
    //最大可支付金额，非定额账单时可用
    public String maxPayableAmount;
    //是否支持超额支付，定额账单时不返回
    public String excessPayAmountEnabled;
    public String billProcessingFeePayAmount;
    public String billProcessingFeeAmount;

    private PayBillDetailEntity payBillDetailEntity;
    private TransferLimitEntity transferLimitEntity;
    private String mChannelCode;

    private String orderMoney, feeMoney = "0.0", vatMoney = "0.0";
    private String changeFirstAmount = "0.0";
    private String changeSecondAmount = "0.0";
    private boolean isMoi = false;

    public static void startPayBillDetailActivity(Activity fromActivity, PayBillDetailEntity detailEntity, TransferLimitEntity limitEntity, String channelCode, boolean isMoi) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.paybill_billerDetail, detailEntity);
        mHashMap.put(Constants.TransferLimitEntity, limitEntity);
        mHashMap.put(Constants.CHANNEL_CODE, channelCode);
        mHashMap.put("isMoi", isMoi);
        ActivitySkipUtil.startAnotherActivity(fromActivity, PayBillDetailActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    private void checkBanlance(UserInfoEntity userInfoEntity) {
        String balance = userInfoEntity.getBalance();
        String balanceNumber = userInfoEntity.getBalanceNumber();
        if (!TextUtils.isEmpty(balanceNumber)) {
            try {
                balanceNumber = balanceNumber.replace(",", "");
                valiableMoney = Double.parseDouble(balanceNumber);
//                checkMoney(totalMoney);
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---" + e + "---");
            }
        }
    }

    private String exchangeFirstMoney(String money, String exchangeRate) {
        try {
            if (money.equals(payAmount)) {
                return billAmount;
            }
            orderMoney = money;
            showTotalAmount();
            String resultMoney = BigDecimalFormatUtils.mul(money, exchangeRate, 2, BigDecimal.ROUND_DOWN);
            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
        return "";
    }

    private String exchangeSecondMoney(String money, String exchangeRate) {
        try {
//            if (money.equals(billAmount)) {
//                return payAmount;
//            }
            changeFirstAmount = money;
            String resultMoney = BigDecimalFormatUtils.div(money, exchangeRate, 2);
            return resultMoney;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
        return "";
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.BillPay_20);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if (getIntent() != null && getIntent().getExtras() != null) {
            payBillDetailEntity = (PayBillDetailEntity) getIntent().getExtras().getSerializable(Constants.paybill_billerDetail);
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            mChannelCode = getIntent().getStringExtra(Constants.CHANNEL_CODE);
            isMoi = getIntent().getBooleanExtra("isMoi", false);
            if (transferLimitEntity != null) {
                String monthLimitAmt = transferLimitEntity.monthLimitAmt;
                try {
                    monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
            }
            if (payBillDetailEntity != null) {
                billerName = payBillDetailEntity.billerName;
                billReferenceName = payBillDetailEntity.billReferenceName;
                billReferenceValue = payBillDetailEntity.billReferenceValue;
                billDueDate = payBillDetailEntity.billDueDate;
                billProcessingFeePayAmount = payBillDetailEntity.billProcessingFeePayAmount;
                billProcessingFeeAmount = payBillDetailEntity.billProcessingFeeAmount;


                billAmountCustomLabel = payBillDetailEntity.billAmountCustomLabel;
                if (!TextUtils.isEmpty(billAmountCustomLabel)) {
                    tvAmountTitle.setText(billAmountCustomLabel);
                }
                payAmount = AndroidUtils.getTransferMoney(payBillDetailEntity.payAmount);
                payCurrencyCode = payBillDetailEntity.payCurrencyCode;
                payAmountEditable = payBillDetailEntity.payAmountEditable;
                billAmountEditable = payBillDetailEntity.billAmountEditable;
                billAmount = AndroidUtils.getTransferMoney(payBillDetailEntity.billAmount);
                billCurrencyCode = payBillDetailEntity.billCurrencyCode;
                try {
                    if (!TextUtils.isEmpty(payBillDetailEntity.exchangeRate)) {
                        exchangeRate = payBillDetailEntity.exchangeRate;
                    }
                    changeFirstAmount = billAmount;
                    changeSecondAmount = payAmount;
                    if (!TextUtils.isEmpty(payAmount)) {
                        orderMoney = payAmount;
                    }

                    if (!TextUtils.isEmpty(billProcessingFeePayAmount)) {
                        tvBillerFees.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(billProcessingFeePayAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode("")).size(10)
                                .build());
                        feeMoney = AndroidUtils.getTransferMoney(billProcessingFeePayAmount);
                    }

                    showTotalAmount();
//                    exchangeRate = exchangeRate / 100d;
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
                partialPayAmountEnabled = payBillDetailEntity.partialPayAmountEnabled;
                pastDuePaymentAllowed = payBillDetailEntity.pastDuePaymentAllowed;
                daysToPost = payBillDetailEntity.daysToPost;
                isPostByBusinessDays = payBillDetailEntity.isPostByBusinessDays;
                billerImgUrl = payBillDetailEntity.billerImgUrl;
                minPayableAmount = AndroidUtils.getTransferMoney(payBillDetailEntity.minPayableAmount);
                maxPayableAmount = AndroidUtils.getTransferMoney(payBillDetailEntity.maxPayableAmount);
                excessPayAmountEnabled = payBillDetailEntity.excessPayAmountEnabled;

                if (!TextUtils.isEmpty(maxPayableAmount)) {
                    tvMaxMoney.setText(getString(R.string.BillPay_26) + maxPayableAmount + " " + billCurrencyCode);
                }

                if (!TextUtils.isEmpty(minPayableAmount)) {
                    tvMinMoney.setText(getString(R.string.BillPay_25) + minPayableAmount + " " + billCurrencyCode);
                }

                tvChangeFirstCurrency.setText(billCurrencyCode);
                tvChangeSecondCurrency.setText(payCurrencyCode);
//                tvPay.setText(getString(R.string.IMR_sendMoney_6) + " " + AmountUtil.dataFormat(payAmount) + " " + payCurrencyCode);

                etChangeSecondAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
                etChangeSecondAmount.getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String amount = s.toString();
                        amount = amount.replace(",", "");
                        if (TextUtils.isEmpty(amount)) {
                            tvPay.setEnabled(false);
                            orderMoney = "0";
                            tvFirstMoney.setText(" " + billCurrencyCode);
                            showTotalAmount();
                        } else {
                            try {
                                changeFirstAmount = exchangeFirstMoney(amount, exchangeRate);
                                tvFirstMoney.setText(changeFirstAmount + " " + billCurrencyCode);
                                if (Double.parseDouble(changeFirstAmount) == 0) {
                                    tvPay.setEnabled(false);
                                } else {
                                    tvPay.setEnabled(true);
                                }
                            } catch (Exception e) {
                                tvPay.setEnabled(false);
                            }
                        }
                    }
                });

                etChangeFirstAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
                etChangeFirstAmount.getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String amount = s.toString();
                        amount = amount.replace(",", "");
                        if (TextUtils.isEmpty(amount)) {
                            tvPay.setEnabled(false);
                            orderMoney = "0";
                            tvSecondMoney.setText(" " + payCurrencyCode);
                            showTotalAmount();
                        } else {
                            try {
                                changeSecondAmount = exchangeSecondMoney(amount, exchangeRate);
                                tvSecondMoney.setText(changeSecondAmount + " " + payCurrencyCode);

                                orderMoney = changeSecondAmount;
                                showTotalAmount();

                                if (Double.parseDouble(changeSecondAmount) == 0) {
                                    tvPay.setEnabled(false);
                                } else {
                                    tvPay.setEnabled(true);
                                }
                                checkMoney(amount);
                            } catch (Exception e) {
                                tvPay.setEnabled(false);
                            }
                        }
                    }
                });

                if (!TextUtils.isEmpty(payAmountEditable) && payBillDetailEntity.isPayAmountEditable(payAmountEditable)) {
                    if (TextUtils.isEmpty(payAmount)) {
                        conEditSecondMoney.setVisibility(View.VISIBLE);
                        tvSecondMoney.setVisibility(View.GONE);
                        tvPay.setEnabled(false);
                    }
                    tvEditMoney.setVisibility(View.VISIBLE);
                }
                if (!TextUtils.isEmpty(billAmountEditable) && payBillDetailEntity.isBillAmountEditable(billAmountEditable)) {
                    if (TextUtils.isEmpty(billAmount)) {
                        conEditFirstMoney.setVisibility(View.VISIBLE);
                        tvFirstMoney.setVisibility(View.GONE);
                        tvPay.setEnabled(false);
                    }
                    tvEditMoney.setVisibility(View.VISIBLE);
                }


                tvBillerName.setText(billerName);
                Glide.with(mContext)
                        .load(billerImgUrl)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.utilities))
                        .into(ivBiller);

                tvReferenceValue.setText(billReferenceValue);
                tvReferenceName.setText(billReferenceName);
                if (TextUtils.isEmpty(billDueDate)) {
                    conDuetoDate.setVisibility(View.GONE);
                }
                tvDuetoDate.setText(billDueDate);
                if (TextUtils.isEmpty(billAmount)) {
                    conBillAmount.setVisibility(View.GONE);
                }
                tvBillAmount.setText(billAmount + " " + billCurrencyCode);
                tvFirstMoney.setText(billAmount + " " + billCurrencyCode);
                tvSecondMoney.setText(payAmount + " " + payCurrencyCode);

//                showExtendPropertiesLayout();

            }

            //本地账单 隐藏底部详细金额
            if (Constants.paybill_local.equals(mChannelCode)) {
                conBillAmount.setVisibility(View.GONE);
                tvEditMoney.setVisibility(View.GONE);
                conLocalMoney.setVisibility(View.VISIBLE);
                conExchangeMoney.setVisibility(View.GONE);
                llShowMore.setVisibility(View.GONE);
                tvLine.setVisibility(View.GONE);

                tvLocalCurrency.setText(payCurrencyCode);
                etLocalAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
                etLocalAmount.setContentText(payAmount);
                showTotalAmount();

                if (!TextUtils.isEmpty(payAmountEditable) && !payBillDetailEntity.isPayAmountEditable(payAmountEditable)) {
                    etLocalAmount.setFocusableFalse();
                }
                if (!TextUtils.isEmpty(billAmountEditable) && !payBillDetailEntity.isBillAmountEditable(billAmountEditable)) {
                    etLocalAmount.setFocusableFalse();
                }

                etLocalAmount.getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String amount = s.toString();
                        amount = amount.replace(",", "");
                        if (TextUtils.isEmpty(amount)) {
                            tvPay.setEnabled(false);
                            orderMoney = "0";
                            showTotalAmount();
                        } else {
                            try {
                                orderMoney = amount;
                                showTotalAmount();

                                if (Double.parseDouble(amount) == 0) {
                                    tvPay.setEnabled(false);
                                } else {
                                    tvPay.setEnabled(true);
                                }
                            } catch (Exception e) {
                                tvPay.setEnabled(false);
                            }
                        }
                    }
                });
            } else {
                conLocalMoney.setVisibility(View.GONE);
                conExchangeMoney.setVisibility(View.VISIBLE);
            }
        }
        checkBanlance(getUserInfoEntity());
    }


    private void showExtendPropertiesLayout() {
        if (payBillDetailEntity != null && payBillDetailEntity.getExtendProperties() != null && payBillDetailEntity.getExtendProperties().size() > 0) {
//            llBillFees.setVisibility(View.GONE);
//            llDetailAmount.setVisibility(View.GONE);
            //移除最后一个
            payBillDetailEntity.getExtendProperties().remove(payBillDetailEntity.getExtendProperties().size() - 1);
            for (ExtendPropertiesEntity e : payBillDetailEntity.getExtendProperties()) {
                ItemPayBillExtendPropertiesView itemView = new ItemPayBillExtendPropertiesView(this);

                String key = e.getExtendKey();
                if (TextUtils.equals(e.getExtendKey(), ExtendPropertiesEntity.TYPE_EXTEND_KEY_FESS)) {
//                    key = getString(R.string.BillPay_45);
                    feeMoney = AndroidUtils.getTransferMoney(e.getExtendValue());
                } else if (TextUtils.equals(e.getExtendValue(), ExtendPropertiesEntity.TYPE_EXTEND_KEY_VAT)) {
                    vatMoney = AndroidUtils.getTransferMoney(e.getExtendValue());
//                    key = getString(R.string.add_money_17);
                }

                String value = Spans.builder()
                        .text(AndroidUtils.getTransferMoney(e.getExtendValue()) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(LocaleUtils.getCurrencyCode("")).size(10)
                        .build().toString();

                itemView.setKeyValue(key, value);
                mLlExtendProperties.addView(itemView);
            }
        }
    }


    private void showTotalAmount() {
        try {
            totalMoney = BigDecimalFormatUtils.add(orderMoney, feeMoney, 2);
            totalMoney = BigDecimalFormatUtils.add(totalMoney, vatMoney, 2);
            checkMoney(totalMoney);
            tvTotalAmount.setText(Spans.builder()
                    .text(totalMoney + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode("")).size(10)
                    .build());
            tvTotalAmountPay.setText(Spans.builder()
                    .text(totalMoney + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(20)
                    .text(LocaleUtils.getCurrencyCode("")).size(12)
                    .build());
            tvBillDetailAmount.setText(Spans.builder()
                    .text(orderMoney + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_803a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf")).size(14)
                    .text(LocaleUtils.getCurrencyCode("")).size(10)
                    .build());
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    private void checkMoney(String amount) {
        try {
            tvErrorEdit.setVisibility(View.GONE);
            llAddMoney.setVisibility(View.GONE);
//            tvCheckBalance.setVisibility(View.VISIBLE);
            tvPay.setEnabled(false);
            if (TextUtils.isEmpty(amount)) {
                return;
            }

//            if (BigDecimalFormatUtils.compareInt(orderMoney,"0")==0) {
//                tvPay.setEnabled(false);
//            }

            double money = Double.parseDouble(amount);
//            if (money > 0) {
            tvPay.setEnabled(true);
//            }

            if (money > monthLimitMoney) {
                tvErrorEdit.setVisibility(View.VISIBLE);
                tvErrorEdit.setText(getString(R.string.wtw_25_3));
                tvPay.setEnabled(false);
                return;
            }
            double maxDouble, minDouble;
            if (!TextUtils.isEmpty(maxPayableAmount)) {
                maxDouble = Double.parseDouble(maxPayableAmount.replace(",", ""));
                if (money > maxDouble) {
                    tvErrorEdit.setVisibility(View.VISIBLE);
                    tvPay.setEnabled(false);
                    tvErrorEdit.setText(getString(R.string.sprint19_57));
                    return;
                }
            }

            if (!TextUtils.isEmpty(minPayableAmount)) {
                minDouble = Double.parseDouble(minPayableAmount.replace(",", ""));
                if (money < minDouble) {
                    tvErrorEdit.setVisibility(View.VISIBLE);
                    tvPay.setEnabled(false);
                    tvErrorEdit.setText(getString(R.string.sprint19_57));
                    return;
                }
            }

        } catch (Exception e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
//        if (money > monthLimitMoney) {
//        } else if (money <= valiableMoney) {
//            if (money > 0) {
//                tvPay.setEnabled(true);
//            }
//        } else {
//            llAddMoney.setVisibility(View.VISIBLE);
//            tvCheckBalance.setVisibility(View.GONE);
//        }
//
//        if (valiableMoney == 0) {
//            llAddMoney.setVisibility(View.VISIBLE);
//            tvCheckBalance.setVisibility(View.GONE);
//        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                checkBanlance(userInfoEntity);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_get_help, R.id.ll_show_more, R.id.tv_check_balance, R.id.tv_pay, R.id.tv_add_money,
            R.id.tv_edit_money})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_help:
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("isHideTitle", true);
                ActivitySkipUtil.startAnotherActivity(this, GetHelpActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.ll_show_more:
                if (llMoneyDetail.getVisibility() == View.VISIBLE) {
                    llMoneyDetail.setVisibility(View.GONE);
                    ivShowMore.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_more_down));
                } else {
                    llMoneyDetail.setVisibility(View.VISIBLE);
                    ivShowMore.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_more_up));
                }
                break;
            case R.id.tv_check_balance:
                String balance = getUserInfoEntity().getBalance();
                showBalanceDialog(balance);
                break;
            case R.id.tv_pay:
                payAmount = AndroidUtils.getReqTransferMoney(String.valueOf(orderMoney));
                String requestBillAmount;
                if (Constants.paybill_local.equals(mChannelCode)) {
                    requestBillAmount = payAmount;
                } else {
                    requestBillAmount = AndroidUtils.getReqTransferMoney(changeFirstAmount);
                }
                mPresenter.fetchBill(payAmount, requestBillAmount, billProcessingFeePayAmount, billProcessingFeeAmount, mChannelCode);
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            case R.id.tv_edit_money:
                if ("Y".equals(billAmountEditable)) {
                    if (!TextUtils.isEmpty(billAmount)) {
                        etChangeFirstAmount.setContentText(billAmount);
                    }
                    conEditFirstMoney.setVisibility(View.VISIBLE);
                    tvFirstMoney.setVisibility(View.GONE);
                } else {
                    if (!TextUtils.isEmpty(payAmount)) {
                        etChangeSecondAmount.setContentText(payAmount);
                    }
                    conEditSecondMoney.setVisibility(View.VISIBLE);
                    tvSecondMoney.setVisibility(View.GONE);
                }
                break;
            default:
                break;
        }
    }

    private void showBalanceDialog(String balance) {
        CheckBalanceDialog checkBalanceDialog = new CheckBalanceDialog(mContext, balance);
        checkBalanceDialog.showWithBottomAnim();
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        checkOutEntity.voucherDarkIconUrl = billerImgUrl;
        checkOutEntity.voucherLightIconUrl = billerImgUrl;
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.mPayBillOrderInfoEntity, mPayBillOrderInfoEntity);
        mHashMap.put("billerName", billerName);
        mHashMap.put("isMoi", isMoi);
        ActivitySkipUtil.startAnotherActivity(this,
                PayBillSummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void fetchBillSuccess(PayBillOrderInfoEntity payBillOrderInfoEntity) {
        this.mPayBillOrderInfoEntity = payBillOrderInfoEntity;
        if (payBillOrderInfoEntity != null) {
            String orderInfo = payBillOrderInfoEntity.orderInfo;
            if (!TextUtils.isEmpty(orderInfo)) {
                mPresenter.checkOut("", orderInfo);
            }
        }
    }

    @Override
    public void checkOutFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    private void showErrorDialog(int errorType) {
        PayBillErrorDialog payBillErrorDialog = new PayBillErrorDialog(mContext, errorType, billerName);
        payBillErrorDialog.setBottomButtonListener(new PayBillErrorDialog.BottomButtonListener() {
            @Override
            public void clickButton() {
                payBillErrorDialog.dismiss();
            }
        });
        payBillErrorDialog.showWithBottomAnim();
    }

    @Override
    public void fetchBillFail(String errorCode, String errorMsg) {
        if (ERROR_AMOUNT_ONE.equals(errorCode)) {
            //amount error
            showErrorDialog(3);
        } else if (ERROR_AMOUNT_TWO.equals(errorCode)) {
            showErrorDialog(4);
        } else if ("030213".equals(errorCode)) {
            showCommonErrorDialog(errorMsg);
        } else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public PayBillContract.BillOrderCheckoutPresenter getPresenter() {
        return new PayBillCheckoutPresenter();
    }
}
