package cn.swiftpass.wallet.tiqmo.base.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public interface GetOtpTypePresenter <V extends BaseView> extends BasePresenter<V> {
    void getOtpType(String category, String categoryId, List<String> additionalData);
}
