package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class BudgetCategorychooseAdapter extends BaseRecyclerAdapter<SpendingBudgetEntity> {
    public BudgetCategorychooseAdapter(@Nullable List<SpendingBudgetEntity> data) {
        super(R.layout.item_choose_category, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, SpendingBudgetEntity spendingBudgetEntity, int position) {
        try {
            baseViewHolder.setText(R.id.tv_choose_category, spendingBudgetEntity.categoryName);
            baseViewHolder.setImageResource(R.id.iv_choose_category, AndroidUtils.getBudgetCategoryDrawable(mContext, spendingBudgetEntity.categoryCode));
//            RoundedImageView ivCategory = baseViewHolder.getView(R.id.iv_choose_category);
//            int categoryResId = AndroidUtils.getBudgetCategoryDrawable(mContext, spendingBudgetEntity.categoryCode);
//            ivCategory.setImageResource(categoryResId);
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
