package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;

public class ImrSetupConfirmDialog extends BaseDialogFragmentN {

    TextView tvConfirmContent;
    TextView confirmButton;
    OnDialogClickListener listener;

    public void setOnConfirmClickListener(OnDialogClickListener listener) {
        this.listener = listener;
    }

    public ImrSetupConfirmDialog() {

    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        if (listener != null) {
            listener.onCancel();
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_imr_comfirm;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
        initData();
    }

    private void initData() {
    }

    private void initView(View parentView) {
        tvConfirmContent = parentView.findViewById(R.id.tv_imr_setup_confirm_content);
        confirmButton = parentView.findViewById(R.id.tv_imr_setup_confirm);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onConfirmClick();
                }
                ImrSetupConfirmDialog.this.dismiss();
            }
        });
    }

    public interface OnDialogClickListener {
        void onConfirmClick();
        void onCancel();
    }
}
