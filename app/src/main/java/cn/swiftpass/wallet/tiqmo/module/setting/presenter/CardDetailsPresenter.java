package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.CardDetailsContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

/**
 * Created by aijingya on 2020/7/13.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.presenter
 * @Description:
 * @date 2020/7/13.14:33.
 */
public class CardDetailsPresenter implements CardDetailsContract.Presenter {
    private CardDetailsContract.View mCardDetailsView;

    @Override
    public void getBindCardTransactionList() {
            //获取卡交易列表信息，展示没有此功能
    }

    @Override
    public void unBindCard(String protocolNo) {
        AppClient.getInstance().getCardManager().unBindCard(protocolNo, new LifecycleMVPResultCallback<unBindCardEntity>(mCardDetailsView,true) {
            @Override
            protected void onSuccess(unBindCardEntity result) {
                mCardDetailsView.unbindCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardDetailsView.unbindCardFailed(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void setDefaultCard(String protocolNo) {
        AppClient.getInstance().getCardManager().setDefaultCard(protocolNo, new LifecycleMVPResultCallback<Void>(mCardDetailsView,true) {
            @Override
            protected void onSuccess(Void result) {
                mCardDetailsView.setDefaultCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardDetailsView.setDefaultCardFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getHistoryList(String protocolNo, String accountNo, List<String> paymentType, List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum,
                               String direction, String orderNo) {
        AppClient.getInstance().getTransferMainHistory(protocolNo, accountNo, paymentType,
                tradeType, startDate, endDate, pageSize, pageNum, direction, orderNo, new LifecycleMVPResultCallback<TransferHistoryMainEntity>(mCardDetailsView, false) {
                    @Override
                    protected void onSuccess(TransferHistoryMainEntity result) {
                        mCardDetailsView.getHistoryListSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mCardDetailsView.getHistoryListFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void deleteTransferHistory(String orderNo) {
        AppClient.getInstance().deleteTransferHistory(orderNo, new LifecycleMVPResultCallback<Void>(mCardDetailsView, true) {
            @Override
            protected void onSuccess(Void result) {
                mCardDetailsView.deleteTransferHistorySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardDetailsView.deleteTransferHistoryFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getHistoryDetail(String orderNo, String orderType,String queryType) {
        AppClient.getInstance().getTransferHistoryDetail(orderNo, orderType,queryType, new LifecycleMVPResultCallback<TransferHistoryDetailEntity>(mCardDetailsView, true) {
            @Override
            protected void onSuccess(TransferHistoryDetailEntity result) {
                mCardDetailsView.getHistoryDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardDetailsView.getHistoryDetailFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardDetailsContract.View view) {
        this.mCardDetailsView = view;
    }

    @Override
    public void detachView() {
        this.mCardDetailsView = null;
    }
}
