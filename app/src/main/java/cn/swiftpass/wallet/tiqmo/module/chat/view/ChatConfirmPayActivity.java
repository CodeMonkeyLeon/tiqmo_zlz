package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.Contract.ChatContract;
import cn.swiftpass.wallet.tiqmo.module.chat.Presenter.ChatTransferPresenter;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.ChatTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.MessageUtils;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferMoneyActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ChatConfirmPayActivity extends BaseCompatActivity<ChatContract.ChatTransferPresenter> implements ChatContract.ChatTransferView {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    MyTextView tvTitle;
    @BindView(R.id.iv_from_avatar)
    RoundedImageView ivFromAvatar;
    @BindView(R.id.iv_send)
    LottieAnimationView ivSend;
    @BindView(R.id.iv_to_avatar)
    RoundedImageView ivToAvatar;
    @BindView(R.id.tv_name)
    MyTextView tvName;
    @BindView(R.id.tv_phone)
    MyTextView tvPhone;
    @BindView(R.id.tv_error)
    MyTextView tvError;
    @BindView(R.id.tv_transfer_purpose)
    MyTextView tvTransferPurpose;
    @BindView(R.id.ll_transfer_purpose)
    LinearLayout llTransferPurpose;
    @BindView(R.id.tv_note)
    MyTextView tvNote;
    @BindView(R.id.ll_note)
    LinearLayout llNote;
    @BindView(R.id.tv_money)
    MyTextView tvMoney;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.et_purpose)
    EditTextWithDel etPurpose;
    @BindView(R.id.con_purpose)
    ConstraintLayout conPurpose;
    @BindView(R.id.tv_cancel)
    MyTextView tvCancel;
    @BindView(R.id.tv_confirm)
    MyTextView tvConfirm;

    private ChatTransferEntity chatTransferEntity;
    private String note,chatId,payeeName,phoneNumber,payerUserId,payeeUserId;
    public String orderAmount = "0.00";
    public String transferPurpose = "";
    public String transferPurposeDesc = "";
    public String transferType;
    public String chatFileId;
    public int sceneType;

    private TransferEntity mTransferEntity;

    private String orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode, transFees, vat;
    private RiskControlEntity riskControlEntity;

    private BottomDialog bottomDialog;

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_chat_confirm_pay;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();
        if (getIntent() != null && getIntent().getExtras() != null) {
            chatTransferEntity = (ChatTransferEntity) getIntent().getExtras().getSerializable(Constants.chatTransferEntity);
            userInfoEntity = getUserInfoEntity();
            checkUser(userInfoEntity);
            if(chatTransferEntity != null){
//                note = chatTransferEntity.note;
//                chatId = chatTransferEntity.chatId;
//                payeeName = chatTransferEntity.payeeName;
//                phoneNumber = chatTransferEntity.phoneNumber;
//                orderAmount = chatTransferEntity.orderAmount;
//                transferPurpose = chatTransferEntity.transferPurpose;
//                transferPurposeDesc = chatTransferEntity.transferPurposeDesc;
//                transferType = chatTransferEntity.transferType;
//                sceneType = chatTransferEntity.sceneType;
//                payeeUserId = chatTransferEntity.payeeUserId;
                if(TextUtils.isEmpty(payeeName)){
                    payeeName = phoneNumber;
                }
                tvNote.setText(note);
                tvPhone.setText(AndroidUtils.getPhone(phoneNumber));

                if(!TextUtils.isEmpty(orderAmount)){
                    tvMoney.setText(orderAmount+" "+LocaleUtils.getCurrencyCode(""));
                }

                if(!TextUtils.isEmpty(transferPurposeDesc)){
                    tvTransferPurpose.setText(transferPurposeDesc);
                }

                if (!TextUtils.isEmpty(payeeName)) {
                    tvName.setVisibility(View.VISIBLE);
                } else {
                    tvName.setVisibility(View.GONE);
                }
                tvName.setText(payeeName);

                String gender = "1";
                String avatarUrl = ChatHelper.getInstance().getChatUserAvatar(chatFileId);
                if (!TextUtils.isEmpty(avatarUrl)) {
                    Glide.with(mContext).clear(ivToAvatar);
                    Glide.with(mContext)
                            .asBitmap()
                            .load(avatarUrl)
                            .diskCacheStrategy(DiskCacheStrategy.DATA)
                            .dontAnimate()
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(new BitmapImageViewTarget(ivToAvatar) {
                                @Override
                                public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                                    ivToAvatar.setImageBitmap(bitmap);
                                }
                            });
                } else {
                    Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivToAvatar);
                }

                if (ThemeUtils.isCurrentDark(mContext)) {
                    ivSend.setAnimation("lottie/arrow_right_transfer.json");
                } else {
                    ivSend.setAnimation("lottie/l_request_arrow.json");
                }
            }
        }
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.avatar;
            String gender = userInfoEntity.gender;
            if (!TextUtils.isEmpty(avatar)) {
                Glide.with(this).clear(ivFromAvatar);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                        .into(ivFromAvatar);
            } else {
                Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivFromAvatar);
            }
        }
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {

    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        if (transferEntity == null) return;
        this.mTransferEntity = transferEntity;
        riskControlEntity = transferEntity.riskControlInfo;
        if (riskControlEntity != null) {
            if (riskControlEntity.isOtp()) {
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                setTransferMap(mHashMaps);
                ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                jumpToPwd(false);
            }else if(riskControlEntity.isNeedIvr()){
                jumpToPwd(true);
            }else{
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    private void requestTransfer(){
        if (mTransferEntity != null) {
            orderNo = mTransferEntity.orderNo;
            payeeName = mTransferEntity.payerName;
            payMethod = mTransferEntity.payMethod;
            transAmount = mTransferEntity.orderAmount;
            exchangeRate = mTransferEntity.exchangeRate;
            transCurrencyCode = mTransferEntity.orderCurrencyCode;
            transFees = mTransferEntity.transFees;
            vat = mTransferEntity.vat;
        }
        mPresenter.transferSurePay(transferType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat);
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.TRANSFER_ENTITY, mTransferEntity);
        mHashMap.put(Constants.sceneType, sceneType);
        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_TRANSFER_CONTACT);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtw_1));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,transferType,transferEntity,true);
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        }else {
            showTipDialog(errMsg);
        }
    }

    @Override
    public ChatContract.ChatTransferPresenter getPresenter() {
        return new ChatTransferPresenter();
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }
    @OnClick({R.id.iv_back, R.id.tv_cancel, R.id.tv_confirm})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_cancel:
                finish();
                break;
            case R.id.tv_confirm:
                mPresenter.chatTransferToContact(chatId,userInfoEntity.userId, SpUtils.getInstance().getCallingCode(), userInfoEntity.getPhone(), payeeUserId,
                        phoneNumber, AndroidUtils.getReqTransferMoney(orderAmount), UserInfoManager.getInstance().getCurrencyCode(),
                        note, transferType, "", "1", transferPurpose, "", "", payeeName);
                break;
            default:break;
        }
    }
}
