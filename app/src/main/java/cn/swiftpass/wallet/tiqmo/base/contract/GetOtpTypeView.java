package cn.swiftpass.wallet.tiqmo.base.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;

public interface GetOtpTypeView <V extends BasePresenter> extends BaseView<V> {
    void getOtpTypeSuccess(RechargeOrderInfoEntity riskControlEntity);

    void getOtpTypeFail(String errCode, String errMsg);
}
