package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class CommonNoticeDialog extends BottomDialog{

    private Context mContext;

    private TextView tvCommonTitle;
    private ImageView ivCommon;
    private TextView tvCommonContent;
    private TextView tvCommonConfirm;

    private CommonConfirmListener mCommonConfirmListener;

    /**
     * 用户当前状态 激活状态 Active A(正常状态)/非活跃 Inactive S/休眠状态 Dormant D/注销状态 Closed C/无人认领 Unclaimed U/过期状态 ID Expiry E
     */
    private int type = 1;

    public void setCommonConfirmListener(CommonConfirmListener commonConfirmListener) {
        this.mCommonConfirmListener = commonConfirmListener;
    }

    public CommonNoticeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public void setCommonText(String title,String content,String confirmText,int imgId){
        if(tvCommonTitle != null){
            tvCommonTitle.setText(title);
        }
        if(tvCommonContent != null){
            tvCommonContent.setText(content);
        }
        if(tvCommonConfirm != null){
            if(!TextUtils.isEmpty(confirmText)){
                tvCommonConfirm.setText(confirmText);
            }else{
                tvCommonConfirm.setVisibility(View.GONE);
            }
        }
        if(ivCommon != null){
            ivCommon.setImageResource(imgId);
        }
    }

    public interface CommonConfirmListener {
        void commonConfirm(int type);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_common_notice, null);
        tvCommonTitle = view.findViewById(R.id.tv_common_title);
        tvCommonContent = view.findViewById(R.id.tv_common_content);
        tvCommonConfirm = view.findViewById(R.id.tv_common_confirm);
        ivCommon = view.findViewById(R.id.iv_common);

        tvCommonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 1;
                if(mCommonConfirmListener != null){
                    mCommonConfirmListener.commonConfirm(type);
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
