package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.text.Html;
import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class CurrencyEntity extends BaseEntity {

    /**
     * 后台换算比例  默认值为100
     */
    private String convertUnit = "100";
    private String currencySymbol;
    private String currencyCode;
    private String currencyNumberCode;
    //币种国际化,如果为空，就都展示currencyCode，key是语言表示，值是国际化值
    public LanguageEntity currencyLang;

    public String getConvertUnit() {
        return convertUnit;
    }

    public void setConvertUnit(String convertUnit) {
        this.convertUnit = convertUnit;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        if (!TextUtils.isEmpty(currencySymbol)) {
            if (currencySymbol.equals("¥") || currencySymbol.equals("￥")) {
                currencySymbol = Html.fromHtml("&yen").toString();
            }
        }
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCurrencyNumberCode() {
        return currencyNumberCode;
    }

    public void setCurrencyNumberCode(String currencyNumberCode) {
        this.currencyNumberCode = currencyNumberCode;
    }

    public CurrencyEntity() {
    }

}
