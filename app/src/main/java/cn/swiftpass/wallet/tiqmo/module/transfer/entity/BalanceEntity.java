package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class BalanceEntity extends BaseEntity {

    public String balance;
    private String frozenBalance;
    private String currencyCode;
    /**
     * 账户类型 100:基本户, 101:待结算户, 102:手续费账户, 201:积分账户, 301:过渡户, 302:长款户, 303:短款户, 304:收入户, 999:影子账户
     */
    private String accountType;

    /**
     * 账户状态 1:激活 2:锁定 3:冻结
     */
    private String accountStatus;

    public String totalMonthLimitAmt;

    public boolean balanceNotNull() {
        try {
            if (!TextUtils.isEmpty(balance)) {
                return BigDecimalFormatUtils.compareInt(balance,"0")>0;
//                balanceSum = balanceSum.replace(",", "");
//                return Double.parseDouble(balanceSum);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return false;
    }

    public String getBalance() {
        try {
            if (!TextUtils.isEmpty(balance)) {
                balance = balance.replace(",", "");
                String resultMoney = BigDecimalFormatUtils.div(balance,UserInfoManager.getInstance().getConvertUnit(),2);
                return AmountUtil.dataFormat(resultMoney);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return null;
    }



    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFrozenBalance() {
        return AndroidUtils.getTransferMoney(frozenBalance);
    }

    public void setFrozenBalance(String frozenBalance) {
        this.frozenBalance = frozenBalance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }
}
