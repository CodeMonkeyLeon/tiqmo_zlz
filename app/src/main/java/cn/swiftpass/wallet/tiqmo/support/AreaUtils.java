package cn.swiftpass.wallet.tiqmo.support;

/**
 * Created by congwei.li on 2021/8/18.
 *
 * @Description:
 */
public class AreaUtils {

    public final static int AREA_SAUDI = 1001; // 沙特地区

    public static int getCurrentArea() {
        return AREA_SAUDI;
    }
}
