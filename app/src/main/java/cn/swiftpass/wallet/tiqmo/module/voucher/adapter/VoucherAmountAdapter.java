package cn.swiftpass.wallet.tiqmo.module.voucher.adapter;

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class VoucherAmountAdapter extends BaseRecyclerAdapter<VouTypeAndInfoEntity.VouInfo> {

    private int categoryPosition;

    public VoucherAmountAdapter(int categoryPosition, @Nullable List<VouTypeAndInfoEntity.VouInfo> data) {
        super(R.layout.item_voucher_amount, data);
        this.categoryPosition = categoryPosition;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VouTypeAndInfoEntity.VouInfo item, int position) {
        if (item == null) {
            return;
        }
        ImageView ivVoucherAmount = baseViewHolder.getView(R.id.iv_voucher_amount);
        String logo = ThemeUtils.isCurrentDark(mContext) ? item.voucherTypeDarkIconLogoUrl : item.voucherTypeLightIconUrl;
        if (!TextUtils.isEmpty(logo)) {
            Glide.with(mContext).clear(ivVoucherAmount);
            Glide.with(mContext)
                    .load(logo)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivVoucherAmount);
        }
    }

    @Override
    public OnItemClickListener getOnItemClickListener() {
        return super.getOnItemClickListener();
    }
}
