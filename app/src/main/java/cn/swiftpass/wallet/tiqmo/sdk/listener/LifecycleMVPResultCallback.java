package cn.swiftpass.wallet.tiqmo.sdk.listener;

import java.lang.ref.WeakReference;

import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public abstract class LifecycleMVPResultCallback<T> implements ResultCallback<T> {

    private WeakReference<BaseView> mLifecycleView;
    private boolean isEnableLoading;//自动启动和关闭加载对话框
    private boolean isAutoDismissLoadingInSuccessful;//是否再返回成功的时候关闭加载对话框，用于一些多个接口连续掉用而只启动一个加载对话框的场景

    public LifecycleMVPResultCallback(BaseView lifecycleView) {
        this(lifecycleView, true);
    }

    public LifecycleMVPResultCallback(BaseView lifecycleView, boolean isEnableLoading) {
        this(lifecycleView, isEnableLoading, true, true);
    }

    public LifecycleMVPResultCallback(BaseView lifecycleView, boolean isEnableLoading, boolean isDim) {
        this(lifecycleView, isEnableLoading, true, isDim);
    }

    public LifecycleMVPResultCallback(BaseView lifecycleView, boolean isEnableLoading, boolean isAutoDismissLoadingInSuccessful, boolean isDim) {
        mLifecycleView = new WeakReference<>(lifecycleView);
        this.isEnableLoading = isEnableLoading;
        this.isAutoDismissLoadingInSuccessful = isAutoDismissLoadingInSuccessful;
        if (isEnableLoading && lifecycleView != null) {
            lifecycleView.showProgress(true, isDim);
        }
    }


    public final void onResult(T result) {
        try {
            BaseView view = mLifecycleView.get();
            if (view != null && view.isAttachedToPresenter()) {
                if (isEnableLoading && isAutoDismissLoadingInSuccessful) {
                    view.showProgress(false);
                }
                onSuccess(result);
                mLifecycleView.clear();
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public final void onFailure(String errCode, String errorMsg) {
        try {
            BaseView view = mLifecycleView.get();
            if (view != null && view.isAttachedToPresenter()) {
                if (isEnableLoading) {
                    view.showProgress(false);
                }
                onFail(errCode, errorMsg);
                mLifecycleView.clear();
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    protected abstract void onSuccess(T result);

    protected abstract void onFail(String errorCode, String errorMsg);


}
