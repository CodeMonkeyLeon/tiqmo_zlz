package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardListEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class CardListKsaPresenter implements CardKsaContract.CardListPresenter {

    CardKsaContract.CardListView mCardListView;

    @Override
    public void getCardList() {
        AppClient.getInstance().getCardManager().getKsaCardList(new LifecycleMVPResultCallback<KsaCardListEntity>(mCardListView,true) {
            @Override
            protected void onSuccess(KsaCardListEntity ksaCardListEntity) {
                mCardListView.getCardListSuccess(ksaCardListEntity);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.getCardListFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getHistoryList(String protocolNo, String accountNo, List<String> paymentType, List<String> tradeType, String startDate, String endDate, int pageSize, int pageNum, String direction, String orderNo) {
        AppClient.getInstance().getCardManager().getKsaCardHistory(protocolNo, accountNo, paymentType,
                tradeType, startDate, endDate, pageSize, pageNum, direction, orderNo, new LifecycleMVPResultCallback<TransferHistoryMainEntity>(mCardListView, false) {
                    @Override
                    protected void onSuccess(TransferHistoryMainEntity result) {
                        if (mCardListView == null) return;
                        mCardListView.getHistoryListSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        if (mCardListView == null) return;
                        mCardListView.getHistoryListFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void getHistoryDetail(String orderNo, String orderType,String queryType) {
        AppClient.getInstance().getTransferHistoryDetail(orderNo, orderType,queryType, new LifecycleMVPResultCallback<TransferHistoryDetailEntity>(mCardListView, true) {
            @Override
            protected void onSuccess(TransferHistoryDetailEntity result) {
                if (mCardListView == null) return;
                mCardListView.getHistoryDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mCardListView == null) return;
                mCardListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void deleteTransferHistory(String orderNo) {
        AppClient.getInstance().deleteTransferHistory(orderNo, new LifecycleMVPResultCallback<Void>(mCardListView, true) {
            @Override
            protected void onSuccess(Void result) {
                if (mCardListView == null) return;
                mCardListView.deleteTransferHistorySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mCardListView == null) return;
                mCardListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getKsaCardDetails(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaCardDetails(proxyCardNo, new LifecycleMVPResultCallback<CardDetailEntity>(mCardListView,true) {
            @Override
            protected void onSuccess(CardDetailEntity result) {
                mCardListView.getKsaCardDetailsSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void setKsaCardStatus(String status, String proxyCardNo, String reason,String reasonStatus) {
        AppClient.getInstance().getCardManager().setKsaCardStatus(status, proxyCardNo, reason, reasonStatus,new LifecycleMVPResultCallback<Void>(mCardListView,true) {
            @Override
            protected void onSuccess(Void result) {
                mCardListView.setKsaCardStatusSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getKsaLimitChannel(String proxyCardNo) {
        AppClient.getInstance().getCardManager().getKsaLimitChannel(proxyCardNo, new LifecycleMVPResultCallback<KsaLimitChannelEntity>(mCardListView,true) {
            @Override
            protected void onSuccess(KsaLimitChannelEntity result) {
                mCardListView.getKsaLimitChannelSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateCard(String proxyCardNo, String cardNo, String cardExpire) {
        AppClient.getInstance().getCardManager().activateCard(proxyCardNo, cardNo, cardExpire, new LifecycleMVPResultCallback<Void>(mCardListView,true) {
            @Override
            protected void onSuccess(Void result) {
                mCardListView.activateCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateCardResult() {
        AppClient.getInstance().getCardManager().activateCardResult(new LifecycleMVPResultCallback<Void>(mCardListView,true) {
            @Override
            protected void onSuccess(Void result) {
                mCardListView.activateCardResultSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardKsaContract.CardListView cardListView) {
        this.mCardListView = cardListView;
    }

    @Override
    public void detachView() {
        this.mCardListView = null;
    }

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mCardListView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mCardListView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mCardListView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }
}
