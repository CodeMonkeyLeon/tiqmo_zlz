package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import cn.swiftpass.wallet.tiqmo.ProjectApp
import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardBannerEntity
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils
import com.youth.banner.adapter.BannerAdapter

class CardTypeChooseAdapter :
    BannerAdapter<CardBannerEntity, CardTypeChooseAdapter.BannerViewHolder> {


    constructor(list: List<CardBannerEntity>) : super(list)


    override fun onCreateHolder(
        parent: ViewGroup?,
        viewType: Int
    ): BannerViewHolder {

        val itemView: View = LayoutInflater.from(ProjectApp.getContext())
            .inflate(R.layout.item_card_type_h, parent, false)
        return BannerViewHolder(itemView)
    }

    override fun onBindView(
        holder: BannerViewHolder?,
        data: CardBannerEntity?,
        position: Int,
        size: Int
    ) {

        try {
            holder?.run {
                data?.run {
                    imageView.setImageResource(imgId)
                }
            }
        } catch (e: Exception) {
            LogUtils.d("errorMsg", "---$e---")
        }

    }


    class BannerViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView

        init {
            imageView = itemView.findViewById(R.id.iv_banner)
        }
    }
}