package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public interface OTPTryPresenter<V extends BaseView> extends BasePresenter<V> {
    void tryOTP(String callingCode, String phone, String type, List<String> additionalData);
}
