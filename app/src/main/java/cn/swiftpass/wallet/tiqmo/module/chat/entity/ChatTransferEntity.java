package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChatTransferEntity extends BaseEntity {
    /**
     * {
     *    "text":"send chat text",
     *    "transferMoney":{
     *        "sceneType":"8",// 7 send 8 Request Transfer 18 Split Bill
     *        "remindStatus":"1", // 1 表示已经发送过提醒了 0 或者空 表示 没有提醒过
     *        "payStatus":"1",// 1 表示已经付过钱了 0 还没有付钱
     *        "declineStatus":"1",// 1 表示已经拒绝了 0 还没有拒绝
     *        "orderNo":"model.orderNo", // 订单号，detail 使用
     *        "orderType":"model.orderType",  // 订单类型，detail 使用
     *        "orderAmount":"model.orderAmount",  // chat 显示使用
     *        "orderCurrencyCode":"model.orderCurrencyCode",  // chat 显示使用
     *        "payerUserId":"payerUserId",  // 付款人 UserId
     *        "payerName":"payerUserName", // 付款人 firstName
     *        "payeeUserId":"model.payeeNum" // 收款人 UserId
     *    }
     * }
     */

    public String text;
    public ChatSendMoneyEntity transferMoney;

//    public String orderAmount = "0.00";
//    public String transferPurpose = "";
//    public String transferPurposeDesc = "";
//    public String note = "";
//    public KycContactEntity kycContactEntity;
//    public String transferType;
//    public int sceneType;
//    public String payeeName;
//    public String chatId;
//    public String chatFileId;
//    public String phoneNumber;
//    //收款人的UserId
//    public String payeeUserId;
}
