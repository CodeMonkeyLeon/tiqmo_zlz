package cn.swiftpass.wallet.tiqmo.support.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;

public class ImrFullNameFilter implements InputFilter {

    public static final int MONEY = 9166;
    private final String STR_CHARSEQUENCE = "A-Za-z";

    private int digits = 2;

    private final String STR_SPACE = " ";
    private final String STR_HENGGANG = "-";
    private final String STR_NUMBER = "0-9";
    private final String STR_MONEY_SPECIAL = ".,";

    private Pattern mPattern;

    public ImrFullNameFilter() {
        mPattern = Pattern.compile("[" + STR_CHARSEQUENCE + STR_SPACE + "]*");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String result = dest.toString();
        result = result.substring(0, dstart) + source.subSequence(start, end) + result.substring(dend);

        int len = end - start;

        if (len == 0) {
            return source;
        }

        if (" ".equals(source.toString()) && dstart == 0) {
            return "";
        }

        if (" ".equals(source.toString()) && dstart != 0 && " ".equals(result.substring(dstart - 1))) {
            return "";
        }

        if (result.contains(" ")) {
            String[] temp = result.split(" ");
            int length = temp.length;
            if (length == 4) {
                if (" ".equals(source.toString())) {
                    return "";
                }
            }

            for (int i = 0; i < length; i++) {
                String s = temp[i];
                if (s.length() > 15) {
                    return "";
                }
            }
        } else {
            if (result.length() > 16) {
                return "";
            }
        }

        // if the dot is after the inserted part,
        // nothing can break
        return null;
    }
}
