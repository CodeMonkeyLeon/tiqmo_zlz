package cn.swiftpass.wallet.tiqmo.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.entity.LinePointEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;

/**
 * 折线图
 */
public class BrokenView extends View {
    private Context mContext;
    private int yWidth,bWidth,bHeight,pointHeight; // 左边距 除去下边距的距离
    private int totalValue = 50, pJValue = 10; // Y的刻度 Y的总值
    private String xStr="x",yStr="y";
    private HashMap<Double, Double>map=new HashMap<>();
    private int marginB = 50;
    private List<Double> dlk=new ArrayList<>();
    private List<Integer>xList=new ArrayList<>();
    private Point []mPoints;
    /**
     * 阴影画笔
     */
    private Paint mShadowPaint;
    private int screenWidth;
    /**
     * 阴影走过的路径
     */
    private Path shadowPath;
    //X轴刻度
    private List<String> xStrList = new ArrayList<>();
    //Y轴刻度
    private List<String> yStrList = new ArrayList<>();
    private List<LinePointEntity> pointList = new ArrayList<>();
    //平均值
    private float avgValue;

    private float downX, downY;

    private PointItemClickListener pointItemClickListener;

    public void setPointItemClickListener(final PointItemClickListener pointItemClickListener) {
        this.pointItemClickListener = pointItemClickListener;
    }

    public BrokenView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext=context;
        screenWidth = AndroidUtils.getScreenWidth(mContext);
        init();
    }
    public void  setView(HashMap<Double,Double> map,List<String> xStrList,List<String> yStrList,int totalValue,float avgValue,
                         List<LinePointEntity> pointList,int marginB){
        this.map=map;
        this.totalValue=totalValue;
        this.xStrList = xStrList;
        this.yStrList = yStrList;
        this.avgValue = avgValue;
        this.pointList = pointList;
        this.marginB=marginB;
        this.xList.clear();
        invalidate();
    }

    private void init(){
//        initShadowPaint();
    }

    /**
     * 初始化阴影
     */
    private void initShadowPaint() {
        mShadowPaint = new Paint();
        mShadowPaint.setStyle(Paint.Style.FILL);
        mShadowPaint.setAntiAlias(true);
        Shader shader = new LinearGradient(getWidth() / 2f, getHeight(), getWidth() / 2f, 0,
                Color.parseColor("#FFFF8953"),Color.WHITE, Shader.TileMode.REPEAT);
        mShadowPaint.setShader(shader);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            // 把x 坐标从小到大进行排列  保存在一个集合里面
            dlk = AndroidUtils.getintformap(map);
            xList.clear();
            int height = getHeight();
            int width = getWidth();
            bWidth = 80*screenWidth/720;
            yWidth = 40*screenWidth/720;
            bHeight = height - 40*screenWidth/720;
            int xSize = xStrList.size();
            int jSize = yStrList.size();

            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

//        paint.setStrokeWidth(1);
//        paint.setStyle(Paint.Style.STROKE);
            // 画横轴
            paint.setTextSize(AndroidUtils.dip2px(mContext, 9));
            paint.setColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_8c8ea5)));
            paint.setTextAlign(Paint.Align.CENTER);

            TextPaint tp = new TextPaint();
            tp.setColor(mContext.getColor(R.color.color_8c8ea5));
            tp.setTextSize(AndroidUtils.dip2px(mContext, 9));
            for (int i = 0; i < jSize; i++) {
//            String[] strings = yStrList.get(i).split("\n");
//            Paint.FontMetrics fm = paint.getFontMetrics();
//            float offsetY = fm.descent - fm.ascent;
//            int currentX = bWidth;
//            int currentY = bHeight-(bHeight/(jSize-1))*i;
//            for(String s:strings){
//                canvas.drawText(s,currentX,currentY,paint);
//                currentY +=offsetY;
//                offsetY = 0;
//            }
                //绘制Y轴的刻度  如果总高度300  有3个刻度  则从300-100之间作为显示范围
                canvas.drawText(yStrList.get(i), yWidth, bHeight - (bHeight / (jSize)) * i, paint);
            }
            if (jSize > 0) {
                pointHeight = bHeight - bHeight / jSize;
            }
            //画X轴刻度值
            paint.setTextSize(AndroidUtils.dip2px(mContext, 9));
            paint.setColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_8c8ea5)));
            paint.setTextAlign(Paint.Align.CENTER);

            for (int i = 0; i < xSize; i++) {
                // 计算圆点的X轴坐标  跟X刻度位置对应上
                xList.add(yWidth + (width - bWidth) / xSize * i + 30*screenWidth/720);
                canvas.drawText(xStrList.get(i), yWidth + (width - bWidth) / xSize * i + 30*screenWidth/720, height, paint);
            }

            //画虚线平均值
            drawDashLine(width, avgValue, totalValue, canvas, avgValue + LocaleUtils.getCurrencyCode("")+"\n"+mContext.getResources().getString(R.string.analytics_25));

            // 把所有的点相对于View的集合找到
            mPoints = getPotins(dlk, map, totalValue, xList);
            paint.setColor(mContext.getColor(R.color.color_fc4f00));
            paint.setStrokeWidth(5);
            drawPoints(mPoints, canvas, paint);
            paint.setColor(mContext.getColor(R.color.color_fc4f00));
            for (int i = 0; i < mPoints.length; i++) {
                LinePointEntity linePointEntity = pointList.get(i);
                linePointEntity.setRegion(mPoints[i],screenWidth);
                if (linePointEntity.isSelect == 1) {
                    paintRectBig(mPoints[i],canvas,paint);
                    drawShadowText(BigDecimalFormatUtils.forMatWithDigs(map.get(dlk.get(i))+"",2), LocaleUtils.getCurrencyCode(""), canvas, mPoints[i]);
                } else {
                    canvas.drawOval(paintRect(mPoints[i]), paint);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private RectF paintRect(Point point){
        if(screenWidth>720){
            return new RectF(point.x - 10*screenWidth/720, point.y - 10*screenWidth/720, point.x + 10*screenWidth/720, point.y + 10*screenWidth/720);
        }else {
            return new RectF(point.x - 10, point.y - 10, point.x + 10, point.y + 10);
        }

    }

    private void paintRectBig(Point point,Canvas canvas,Paint paint){
        RectF rectF = new RectF(point.x - 15*screenWidth/720, point.y - 15*screenWidth/720, point.x + 15*screenWidth/720, point.y + 15*screenWidth/720);
//        canvas.drawOval(rectF, paint);
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.l_big_point);
        canvas.drawBitmap(bitmap,point.x - 15*screenWidth/720,point.y - 15*screenWidth/720,paint);
    }

    private void drawDashLine(int width,float avgValue,int totalValue2,Canvas canvas,String avgText){
        if(xList.size() == 0)return;
        if(avgValue == 0) return;
        Paint paint=new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(mContext.getColor(R.color.color_1da1f1));
        paint.setStyle (Paint.Style.STROKE);
        paint.setStrokeWidth(2);

        Path path = new Path();
        int avgHeight = bHeight-(int) (pointHeight*avgValue/totalValue2);
        path.moveTo(xList.get(0), avgHeight);
        path.lineTo(width-160*screenWidth/720, avgHeight);
        //实线12  虚线6  偏移量6
        PathEffect effects = new DashPathEffect(new float[]{8,6},6);
        paint.setPathEffect(effects);
        canvas.drawPath(path, paint);

        //平均值文本换行显示
        paint.setColor(mContext.getColor(R.color.color_1da1f1));
        paint.setTextSize(AndroidUtils.dip2px(mContext,8));
        paint.setStyle (Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.CENTER);

        String[] strings = avgText.split("\n");
        Paint.FontMetrics fm = paint.getFontMetrics();
        float offsetY = fm.descent - fm.ascent;
        int currentX = width-120*screenWidth/720;
        int currentY = avgHeight;
        for(String s:strings) {
            canvas.drawText(s, currentX, currentY, paint);
            currentY += offsetY;
            offsetY = 0;
        }
//        canvas.drawText(avgText,width-120*screenWidth/720 ,avgHeight,paint);
    }

    private void drawPoints(Point[] mPoints2, Canvas canvas, Paint paint) {
        int maxHeightPoint = 0;//记录最高的点
        float maxHeight = 0;
        //根据点的坐标画线
        Point point=new Point();
        Point point2=new Point();
        for (int i = 0; i < mPoints2.length-1; i++) {
            point=mPoints2[i];
            point2=mPoints2[i+1];

            if (maxHeight < mPoints2[i].y) {
                maxHeight = mPoints2[i].y;
                maxHeightPoint = i;
            }

            canvas.drawLine(point.x,point.y,point2.x,point2.y,paint);
        }

        //画阴影
        Path path = new Path();
        path.moveTo(mPoints2[0].x, mPoints2[0].y);
        for (int i = 1; i < mPoints2.length; i++) {
            path.lineTo(mPoints2[i].x, mPoints2[i].y);
        }
        //链接最后两个点
        int index = mPoints2.length - 1;
        path.lineTo(mPoints2[index].x, mPoints2[maxHeightPoint].y);
        path.lineTo(mPoints2[0].x, mPoints2[maxHeightPoint].y);
        path.close();
        mShadowPaint = new Paint();
        mShadowPaint.setStyle(Paint.Style.FILL);
        mShadowPaint.setAntiAlias(true);
        Shader shader = new LinearGradient(0, mPoints2[maxHeightPoint].y, mPoints2[maxHeightPoint].x, 0,
                Color.parseColor("#BFFDFDFD"), Color.parseColor("#BFFF8953"), Shader.TileMode.REPEAT);
        mShadowPaint.setShader(shader);
        canvas.drawPath(path, mShadowPaint);
    }

    // 获得相对于View 所有的点的集合

    private Point []getPotins(List<Double> dlk2, HashMap<Double, Double> map2, int totalValue2,
                              List<Integer> xList2) {
        //获取每一个点的位置
        Point [] points=new Point[dlk2.size()];
        for (int i = 0; i <points.length ; i++) {
            int ph=bHeight-(int) (pointHeight*(map2.get(dlk.get(i))/totalValue2));
            points[i]=new Point(xList2.get(i),ph-2);
        }
        return  points;
    }

    private void drawShadowText(String money,String currencyCode,Canvas canvas,Point point){
//        text = "237648238SAR";
        String text = money+" "+currencyCode;
//        if(LocaleUtils.isRTL(mContext)){
//            text = currencyCode+" "+money;
//        }
        Rect mBonds = new Rect();
        Paint paint=new Paint();
        //根据文本长度来设置textview宽度
        paint.getTextBounds(text, 0, text.length(), mBonds);
        RectF  mBackground = new RectF(point.x-mBonds.width()*screenWidth/720, point.y-80*screenWidth/720, point.x+mBonds.width()*screenWidth/720,point.y-30*screenWidth/720);
//        LogUtils.d("mBonds.width()",point.x+":::"+mBonds.width());
//        RectF mBackground = new RectF(point.x, point.y-80, point.x+mBonds.width()*2,point.y-30);
        if(point.y<80){
            mBackground = new RectF(point.x+20*screenWidth/720, point.y-30*screenWidth/720, point.x+120*screenWidth/720,point.y+20*screenWidth/720);
        }
        paint.setStyle (Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setShadowLayer(10f, 0, 0, mContext.getColor(R.color.gray1));
        canvas.drawRoundRect(mBackground, 10, 10, paint);

        Paint textPaint=new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(mContext.getColor(R.color.color_090a15_light));
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(AndroidUtils.dip2px(mContext,12));
//        paint.getTextBounds(text, 0, text.length(), mBonds);
        drawSpanString(money,currencyCode,textPaint,canvas,mBackground.left, mBackground.bottom-15*screenWidth/720);
//        canvas.drawText(text, mBackground.centerX(), mBackground.bottom-15*screenWidth/720, textPaint);
    }

    private void drawText(String str,int i , int j, Canvas canvas){
        Paint paint=new Paint();
//        paint.setAlpha(0X0000FF);
        paint.setTextSize(9);
        paint.setColor(Color.BLACK);
//        Typeface typeface=Typeface.create("宋体",Typeface.ITALIC);
//        paint.setTypeface(typeface);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(str,i ,j,paint);
    }

    public interface PointItemClickListener{
        void clickPoint(LinePointEntity linePointEntity);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
                downY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                for (LinePointEntity linePointEntity : pointList) {
//                    colorItem.setColor(getContext().getColor(AndroidUtils.getSpendingColorId(colorItem.colorId)));
                    if (linePointEntity.isInRegion(downX, downY)) {
                        if(linePointEntity.isSelect == 0){
                            linePointEntity.isSelect = 1;
                        }else{
                            linePointEntity.isSelect = 0;
                        }
                        if(pointItemClickListener != null){
                            pointItemClickListener.clickPoint(linePointEntity);
                        }
                    }else{
                        linePointEntity.isSelect = 0;
//                        colorItem.isSelect = 0;
//                        colorItem.setColor(getContext().getColor(R.color.color_50dcdcdc));
                    }
                }
                invalidate();
                break;
        }
        return true;
    }

    private void drawSpanString(String money,String currencyCode,Paint mTextPaint,Canvas canvas,float xStart,float yStart){
        String text = money+" "+currencyCode;
        if(LocaleUtils.isRTL(mContext)){
            text = currencyCode+" "+money;
        }
        SpannableString spannableString = new SpannableString(text);

        ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(mContext.getColor(R.color.color_a3a3a3));

        spannableString.setSpan(foregroundSpan, LocaleUtils.isRTL(mContext)?0:money.length(), LocaleUtils.isRTL(mContext)?currencyCode.length():text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        int next;

        float xEnd;

        for (int i = 0; i < spannableString.length(); i = next) {

            next = spannableString.nextSpanTransition(i, spannableString.length(), CharacterStyle.class);

            xEnd = xStart + mTextPaint.measureText(spannableString, i, next);

            ForegroundColorSpan[] fgSpans = spannableString.getSpans(i, next, ForegroundColorSpan.class);

            if (fgSpans.length > 0) {
                int saveColor = mTextPaint.getColor();
                mTextPaint.setColor(fgSpans[0].getForegroundColor());
                canvas.drawText(spannableString, i, next, xStart, yStart, mTextPaint);
                mTextPaint.setColor(saveColor);
            } else {
                canvas.drawText(spannableString, i, next, xStart, yStart, mTextPaint);

            }
            xStart = xEnd;
        }
    }

}
