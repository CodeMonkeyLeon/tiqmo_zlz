package cn.swiftpass.wallet.tiqmo.module.chat.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class ChooseChatTypeDialog extends BottomDialog {

    private Context mContext;

    private ConfirmListener confirmListener;

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public interface ConfirmListener {
        void clickSendMoney();
        void clickRequestMoney();
        void clickChoosePhoto();
    }

    public ChooseChatTypeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_chat_choose_type, null);
        TextView tvSendMoney = view.findViewById(R.id.tv_chat_send_money);
        TextView tvRequestMoney = view.findViewById(R.id.tv_chat_request_money);
        TextView tvChoosePhoto = view.findViewById(R.id.tv_chat_choose_photo);

        tvSendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickSendMoney();
                }
            }
        });

        tvRequestMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickRequestMoney();
                }
            }
        });

        tvChoosePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ButtonUtils.isFastDoubleClick()){
                    return;
                }
                if(confirmListener != null){
                    confirmListener.clickChoosePhoto();
                }
            }
        });

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}

