package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class EVoucherOrderInfoEntity extends BaseEntity {
    public String orderInfo;
    /**
     * 代金券是否在处理中 Y:是 N:无
     */
    public String voucherOrderIsProcessingFlag;

    public RiskControlEntity riskControlInfo;
}
