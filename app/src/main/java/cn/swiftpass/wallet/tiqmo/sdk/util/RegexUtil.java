package cn.swiftpass.wallet.tiqmo.sdk.util;

import android.text.TextUtils;

import java.util.regex.Pattern;

import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;


public class RegexUtil {

    /**
     * 验证手机号码（支持国际格式，+86135xxxx...（中国内地），+00852137xxxx...（中国香港））
     *
     * @param mobile 移动、联通、电信运营商的号码段
     *               <p>移动的号段：134(0-8)、135、136、137、138、139、147（预计用于TD上网卡）
     *               、150、151、152、157（TD专用）、158、159、187（未启用）、188（TD专用）</p>
     *               <p>联通的号段：130、131、132、155、156（世界风专用）、185（未启用）、186（3g）</p>
     *               <p>电信的号段：133、153、180（未启用）、189</p>
     * @return 验证成功返回true，验证失败返回false
     */

    public static final String REG_MOBILE = "^[0-9]{8,15}$";
    public static final String REG_PHONE = "^[0-9]{9}$";
    public static final String REG_FULL_NAME = "^[ A-Za-z]*$";
    public static final String REG_PHONE_LENGTH = "^.{6,8}$";
    public static final String REG_PHONE_CONTAINS_NUMBER = "(.*)?\\d+(.*)?";
    public static final String REG_PHONE_CONTAINS_CHAR = "(.*)?[A-Z]+(.*)?";
    public static final String PAY_PD_LENGTH = "^.{6}$";
    public static final String PAY_OLD_PD_LENGTH = "^.{6,8}$";
    public static final String PAY_PD_ONLY_NUMBER = "^[0-9]*$";
    public static final String REG_PD = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,8}$";
    public static final String PAY_PD = "(?:(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){5,}|(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){5,})\\d";

    private static final String STR_SPECIAL = "!@#$%^&*?";
    private static final String STR_CHARSEQUENCE = "A-Za-z";
    private static final String STR_NUMBER = "0-9";
    private static final String ADD_NUMBER = "^(?!.*(123|234|345|456|567|678|789))$";

    public static boolean isMultiChar(String source){
        int[] charInt = new int[256];
        for(int i=0;i<source.length();i++){
            if(charInt[source.charAt(i)] ++ >= 2 )
                return true;
        }
        return false;
    }

    //递增数字
    public static boolean isAddNumber(String number) {
        String oriNumber = "0123456789";
        if (!TextUtils.isEmpty(number)) {
            int length = number.length();
            for(int i = 0;i<length;i++){
                if(i+3<=length){
                    String s = number.substring(i,i+3);
                    if(oriNumber.contains(s)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //生日和Id包含其中3位
    public static boolean isIdBirthdayNumber(String number,String id,String birthday) {
        if(!TextUtils.isEmpty(birthday)){
            birthday = TimeUtils.getNumberDay(birthday);
        }
        String oriNumber = id+"_"+birthday;
        if (!TextUtils.isEmpty(number)) {
            int length = number.length();
            for(int i = 0;i<length;i++){
                if(i+2<length){
                    String s = number.substring(i,i+3);
                    if(oriNumber.contains(s)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isLoginPassword(String password) {
        return isLoginPasswordLength(password) && isLoginOneNumber(password) && isLoginOneChar(password) && isLoginPasswordFormat(password);
    }

    public static boolean isRegFullName(String name) {
        if (!TextUtils.isEmpty(name)) {
            return Pattern.matches(REG_FULL_NAME, name);
        }
        return false;
    }

    public static boolean isLoginOneNumber(String password) {
        return Pattern.matches(REG_PHONE_CONTAINS_NUMBER, password);
    }

    public static boolean isLoginOneChar(String password) {
        return Pattern.matches(REG_PHONE_CONTAINS_CHAR, password);
    }

    public static boolean isLoginPasswordLength(String password) {
        return Pattern.matches(REG_PHONE_LENGTH, password);
    }

    public static boolean isLoginPasswordFormat(String password) {
        return Pattern.matches("[" + STR_CHARSEQUENCE + STR_NUMBER + STR_SPECIAL + "]*", password);
    }

    public static boolean isPayPasswordFormat(String password) {
        return !Pattern.matches(PAY_PD, password);
    }

    public static boolean isPayOldPasswordLength(String password) {
        return Pattern.matches(PAY_OLD_PD_LENGTH, password);
    }

    public static boolean isPayPasswordNumber(String password) {
        return Pattern.matches(PAY_PD_ONLY_NUMBER, password) && Pattern.matches(PAY_PD_LENGTH, password);
    }

    public static boolean isPayPassword(String password){
        return isPayPasswordNumber(password) && isPayPasswordFormat(password);
    }

    public static boolean isPhone(String phone) {
        if (phone.startsWith("50") || phone.startsWith("53") || phone.startsWith("54") || phone.startsWith("56")
                || phone.startsWith("55") || phone.startsWith("57") || phone.startsWith("58") || phone.startsWith("59")) {
            return Pattern.matches(REG_PHONE, phone);
        }
        return false;
    }

    public static boolean isMobile(String mobile) {
        String regex = "(\\+\\d+)?1[3458]\\d{9}$";
        return Pattern.matches(regex, mobile);
    }

//    public static boolean isEmail(String email) {
//        String regex = "\\w+@\\w+\\.[a-z]+(\\.[a-z]+)?";
//        return Pattern.matches(regex, email);
//    }
//    public static boolean isEmail(String email) {
//        String regex = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
//        return Pattern.matches(regex, email);
//    }
public static boolean isEmail(String email) {
    String regex = "^[0-9a-zA-Z][a-zA-Z0-9\\._-]{1,}@[a-zA-Z0-9-]{1,}[a-zA-Z0-9](\\.[a-zA-Z]{1,})+$";
    return Pattern.matches(regex, email);
}

    public static boolean isLegalPassword(String nickname) {
        String regex = "^[a-zA-Z0-9]{8,16}$";
        return Pattern.matches(regex, nickname);
    }

    public static boolean isCharacter(String nickname) {
        String regex = "[a-zA-Z]";
        return Pattern.matches(regex, nickname);
    }

    public static boolean isRepeatNumber(String number) {
        String regex = "([0-9])\\1{5}";
        return Pattern.matches(regex, number);
    }

    public static boolean isChinese(String nickname) {
        String regex = "[\u4e00-\u9fa5]";
        return Pattern.matches(regex, nickname);
    }

    public static boolean isContainsNumbersOrLettersOrSymbol(String str) {
        int i = 0;
        if (Pattern.matches(".*[0-9].*", str)) {
            i++;
        }
        if (Pattern.matches(".*[a-zA-Z]+.*", str)) {
            i++;
        }
        if (Pattern.matches(".*[!\"#$%&'()*+,-./:;<=>?@^_`{|}~\\[\\]\\\\]+.*", str)) {
            i++;
        }
        return i >= 2;
    }

}
