package cn.swiftpass.wallet.tiqmo.module.home.contract

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter
import cn.swiftpass.wallet.tiqmo.base.view.BaseView
import cn.swiftpass.wallet.tiqmo.module.home.entity.FilterListEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryMainEntity

interface HistoryContract {


    interface View : BaseView<HistoryContract.Presenter> {

        fun getHistoryListSuccess(
            result: TransferHistoryMainEntity?,
            currentPage: Int,
            isPreAuth: Boolean = false
        )

        fun getHistoryListFail(
            errorCode: String?,
            errorMsg: String?,
            currentPage: Int,
            isPreAuth: Boolean = false
        )

        fun getHistoryDetailSuccess(
            result: TransferHistoryDetailEntity?,
            isPreAuth: Boolean = false
        )

        fun getHistoryDetailFail(
            errorCode: String?,
            errorMsg: String?,
            isPreAuth: Boolean = false
        )

        fun deleteTransferHistorySuccess(
            result: Void?,
            isPreAuth: Boolean = false
        )

        fun deleteTransferHistoryFail(
            errorCode: String?,
            errorMsg: String?,
            isPreAuth: Boolean = false
        )


        fun getFilterListSuccess(
            result: FilterListEntity?,
            isClick: Boolean,
            isPreAuth: Boolean = false
        )

        fun getFilterListFailed(
            errorCode: String?,
            errorMsg: String?,
            isClick: Boolean,
            isPreAuth: Boolean = false
        )
    }


    interface Presenter : BasePresenter<HistoryContract.View> {

        fun getFilterList(
            isClick: Boolean,
            isPreAuth: Boolean = false
        )

        fun getHistoryList(
            protocolNo: String?,
            accountNo: String?,
            paymentType: List<String?>?,
            tradeType: List<String?>?,
            startDate: String?,
            endDate: String?,
            pageSize: Int,
            pageNum: Int,
            direction: String?,
            orderNo: String?,
            isPreAuth: Boolean = false
        )

        fun getHistoryDetail(
            orderNo: String?,
            orderType: String?,
            queryType: String?,
            isPreAuth: Boolean = false
        )

        fun deleteTransferHistory(
            orderNo: String?,
            isPreAuth: Boolean = false
        )

    }
}