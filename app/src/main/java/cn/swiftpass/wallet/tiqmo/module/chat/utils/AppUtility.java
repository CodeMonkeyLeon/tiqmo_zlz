package cn.swiftpass.wallet.tiqmo.module.chat.utils;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AppUtility {
public static boolean isSameDay(long day1, long day2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(day1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(day2);
        return (cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR));
    }

    public static long getTimestampFromString(String time) {
        try {
            return Long.parseLong(time);
        } catch (Exception e) {
            return System.currentTimeMillis();
        }
    }

    public static String getCustomDateFromMs(String time) {
        try {
            long timeMs = Long.parseLong(time);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(timeMs);
            if (isSameDay(timeMs, System.currentTimeMillis())) return "Today";
            return new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
        } catch (Exception e) {
            return "Unknown";
        }
    }

    public static class WrapContentLinearLayoutManager extends LinearLayoutManager {
        public WrapContentLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

}
