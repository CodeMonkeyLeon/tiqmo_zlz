package cn.swiftpass.wallet.tiqmo.module.imr.contract;

import cn.swiftpass.wallet.tiqmo.base.contract.GetOtpTypePresenter;
import cn.swiftpass.wallet.tiqmo.base.contract.GetOtpTypeView;
import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankIdAddressEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPayerDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;

public class AddBeneficiaryContract {

    public interface AddThreeView extends GetOtpTypeView<AddThreePresenter> {

        void getNationalitySuccess(ImrCountryListEntity data);

        void getNationalityFail(String errorCode, String errorMsg);

        void imrGetIdOrAddressSuccess(ImrBankIdAddressEntity data);

        void imrGetIdOrAddressFail(String errorCode, String errorMsg);

        void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity);

        void imrAddBeneficiaryFail(String errorCode, String errorMsg);

        void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity);

        void imrExchangeRateFail(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void activateBeneficiarySuccess(Void result);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface AddThreePresenter extends GetOtpTypePresenter<AddThreeView> {
        void getNationality(String countryType,String countryCode);

        void imrGetIdOrAddress(String receiptMethod, String countryCode);

        void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName,
                               String nickName, String relationshipCode, String callingCode, String phone,
                               String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace,
                               String birthDate, String sex, String cityName, String districtName,
                               String poBox, String buildingNo, String street, String idNo, String idExpiry,
                               String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                               String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,
                               String channelPayeeId,String channelCode,String branchId);

        void imrExchangeRate(String payeeInfoId,String sourceAmount,
                             String destinationAmount,String channelCode);

        void getLimit(String type);

        void activateBeneficiary(String payeeInfoId);
    }

    public interface AddOneView extends BaseView<AddBeneficiaryContract.AddOnePresenter> {
        void getDestinationSuccess(ImrCountryListEntity list);

        void getDestinationFail(String errorCode, String errorMsg);
    }

    public interface AddOnePresenter extends BasePresenter<AddBeneficiaryContract.AddOneView> {
        void getDestination(String countryType,String countryCode);
    }

    public interface AddFiveView extends GetOtpTypeView<AddFivePresenter> {

        void getImrCityListSuccess(ImrCityListEntity data);
        void getImrCityListFail(String errorCode, String errorMsg);

        void getBankAgentNameSuccess(ImrBankAgentListEntity data);

        void getBankAgentNameFail(String errorCode, String errorMsg);

        void getBankAgentBranchSuccess(ImrBankAgentListEntity data);

        void getBankAgentBranchFail(String errorCode, String errorMsg);

        void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity);

        void imrAddBeneficiaryFail(String errorCode, String errorMsg);

        void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity);

        void imrExchangeRateFail(String errorCode, String errorMsg);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);

        void getImrPayerDetailSuccess(ImrPayerDetailEntity imrPayerDetailEntity);

        void getImrPayerDetailFail(String errCode, String errMsg);

        void imrEditBeneficiarySuccess(ResponseEntity result);

        void imrEditBeneficiaryFail(String errorCode, String errorMsg);

        void activateBeneficiarySuccess(Void result);
        void showErrorMsg(String errorCode,String errorMsg);
    }

    public interface AddFivePresenter extends GetOtpTypePresenter<AddFiveView> {
        void getBankAgentName(String countryCode, String receiptMethod,String channelCode);
        void getImrCityList(String countryCode,String channelCode);

        void getBankAgentBranch(String countryCode, String receiptMethod, String parentNodeCode);

        void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName,
                               String nickName, String relationshipCode, String callingCode, String phone,
                               String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace,
                               String birthDate, String sex, String cityName, String districtName,
                               String poBox, String buildingNo, String street, String idNo, String idExpiry,
                               String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                               String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,
                               String channelPayeeId,String channelCode,String branchId);

        void imrExchangeRate(String payeeInfoId,String sourceAmount,
                             String destinationAmount,String channelCode);

        void getLimit(String type);

        void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo);

        void getImrPayerDetail(String countryCode,String cityId,
                               String payerCurrencyCode,
                               String payeeCurrencyCode,String channelCode);

        void activateBeneficiary(String payeeInfoId);
    }
}
