package cn.swiftpass.wallet.tiqmo.support.entity;

public class NormalLoginSucEntity extends AutoLoginSucEntity {

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String serverPubKey) {
        this.serverPubKey = serverPubKey;
    }

    private String serverPubKey;

}

