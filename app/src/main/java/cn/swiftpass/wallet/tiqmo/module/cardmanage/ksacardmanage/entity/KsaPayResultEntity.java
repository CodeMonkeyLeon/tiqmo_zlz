package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class KsaPayResultEntity extends BaseEntity {
    /**
     * {\"partnerNo\":\"96601\",\"cardType\":\"PHYSICAL\",\"setPinMsg\":\"PIN not matched，please re-enter\"}}
     */
    //开卡成功但是set pin失败的错误提示(后端做国际化，APP只展示)
    public String setPinMsg;
    //卡类型枚举: VIRTUAL:虚拟卡 PHYSICAL:物理卡
    public String cardType;

    public String proxyCardNo;
    public String cardNo;
    public String maskedCardNo;
    public String cardExpireDate;
}
