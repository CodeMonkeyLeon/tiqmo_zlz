package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.swiftpass.wallet.tiqmo.R;

public class RecordInfoItemView extends FrameLayout {

    private TextView tvDesc;
    private TextView tvAmount;
    private TextView tvCurrency;

    public RecordInfoItemView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public RecordInfoItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RecordInfoItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.item_transfer_record, this, true);
        tvDesc = findViewById(R.id.tv_desc);
        tvAmount = findViewById(R.id.tv_amount);
        tvCurrency = findViewById(R.id.tv_currency);
    }

    public void setDesc(String key) {
        tvDesc.setText(key);
    }

    public void setAmount(String value) {
        tvAmount.setText(value);
    }

    public void setCurrency(String value) {
        tvCurrency.setText(value);
    }
}

