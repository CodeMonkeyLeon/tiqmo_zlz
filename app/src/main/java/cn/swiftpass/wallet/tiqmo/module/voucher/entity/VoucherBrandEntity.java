package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class VoucherBrandEntity extends BaseEntity {

    //代金券服务提供商代码
    public String voucherServiceProviderCode;
    //代金券品牌名称
    public String voucherBrandName;
    //代金券品牌代码
    public String voucherBrandCode;
    public String voucherBrandLogo;
    //是否有国家限制 Y: 有限制 N: 无限制
    public String limitCountry;
    //运营商品牌亮色版小图标 url
    public String operatorSmallLightIcon;
    //运营商品牌暗色版小图标 url
    public String operatorSmallDarkIcon;
    //该品牌下的代金券总数
    public int voucherCount;
}
