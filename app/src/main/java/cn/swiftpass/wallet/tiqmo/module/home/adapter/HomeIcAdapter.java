package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.HomeIconEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;


public class HomeIcAdapter extends CommonAdapter {

    private Context context;

    private LayoutInflater inflater;

    private OnItemClick onItemClickListener;

    public void setOnItemClickListener(OnItemClick itemClickListener){
        this.onItemClickListener = itemClickListener;
    }

    public interface OnItemClick{
        void onItemClick(HomeIconEntity homeIconEntity,int position);
    }

    public HomeIcAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_home_icon_view, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final HomeIconEntity homeIconEntity = (HomeIconEntity) getItem(position);
        if (homeIconEntity != null) {
            holder.rlContent.setBackgroundResource(ThemeSourceUtils.getSourceID(context,R.attr.shape_061f6f_white_8));
            holder.iv_icon.setImageResource(ThemeSourceUtils.getSourceID(context,homeIconEntity.imgId));
            holder.tv_icon.setText(homeIconEntity.textId);
            holder.tv_icon.setTextColor(context.getColor(ThemeSourceUtils.getSourceID(context,R.attr.color_white_3a3b44)));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ButtonUtils.isFastDoubleClick()) {
                        return;
                    }
                    if(onItemClickListener != null) {
                        onItemClickListener.onItemClick(homeIconEntity,position);
                    }
                }
            });
        }
        return convertView;
    }

    public void changeTheme(){
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView iv_icon;
        @BindView(R.id.tv_icon)
        TextView tv_icon;
        @BindView(R.id.rl_content)
        RelativeLayout rlContent;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
