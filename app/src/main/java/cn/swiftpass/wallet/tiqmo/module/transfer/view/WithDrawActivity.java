package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.home.view.WithDrawFragment;

public class WithDrawActivity extends BaseCompatActivity {
    private WithDrawFragment withDrawFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_with_draw;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        withDrawFragment = WithDrawFragment.getInstance();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fl_withdraw, withDrawFragment);
        ft.commit();
    }

    @Override
    public void notifyByThemeChanged() {
        super.notifyByThemeChanged();
        if (withDrawFragment != null) {
            withDrawFragment.noticeThemeChange();
        }
    }
}
