package cn.swiftpass.wallet.tiqmo.module.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;

public class MarketPicAdapter extends PagerAdapter {

    private List<String> data;
    private Context context;
    LayoutInflater lf;

    public MarketPicAdapter(List<String> mListViews, LayoutInflater lf, Context context) {
        this.data = mListViews;
        this.lf = lf;
        this.context = context;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeView(position);//删除页卡
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //这个方法用来实例化页卡
        LinearLayout view = (LinearLayout) lf.inflate(R.layout.market_pictrues_item, null);
        RoundedImageView imageView = view.findViewById(R.id.iv_view_page);
        loadImageView(imageView, data.get(position));
        container.addView(view);//添加页卡
        return view;
    }

    private void loadImageView(ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(context).clear(imageView);
            Glide.with(context)
                    .load(url)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(imageView);
        }
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;//返回页卡的数量
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }
}
