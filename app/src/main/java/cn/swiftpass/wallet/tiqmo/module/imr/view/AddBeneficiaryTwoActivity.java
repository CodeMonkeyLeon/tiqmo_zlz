package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankIdAddressEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPaymentEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.AddBeneThreePresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.adapter.TransferRelationAdapter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NameFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class AddBeneficiaryTwoActivity extends BaseCompatActivity<AddBeneficiaryContract.AddThreePresenter> implements AddBeneficiaryContract.AddThreeView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_beneficiary_fullname)
    EditTextWithDel etBeneficiaryFullname;
    @BindView(R.id.et_beneficiary_nickname)
    EditTextWithDel etBeneficiaryNickname;
    @BindView(R.id.rl_relation)
    RecyclerView rlRelation;
    @BindView(R.id.tv_next_step)
    TextView tvNextStep;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_beneficiary_desc)
    TextView tvBeneficiaryDesc;

    @BindView(R.id.iv_calling_code)
    ImageView ivCallingCode;
    @BindView(R.id.tv_calling_code)
    TextView tvCallingCode;
    @BindView(R.id.ll_line)
    View llLine;
    @BindView(R.id.con_area)
    RelativeLayout conArea;
    @BindView(R.id.et_phone_number)
    EditTextWithDel etPhoneNumber;
    @BindView(R.id.ll_area_number)
    LinearLayout llAreaNumber;
    @BindView(R.id.ll_save_list)
    LinearLayout llSaveList;
    @BindView(R.id.cb_save_to_list)
    CheckBox cbSaveToList;
    @BindView(R.id.et_nationality)
    EditTextWithDel etNationality;
    @BindView(R.id.ll_nationality)
    LinearLayout llNationality;

    private TransferRelationAdapter transferRelationAdapter;
    private LinearLayoutManager mLayoutManager;
    public List<KycContactEntity> contactList = new ArrayList<>();

    private String payeeFullName, nickName, relationshipCode, payeeInfoCountryCode, nationality;

    private boolean isEditBeneficiary = false;

    private ImrCountryEntity imrCountryEntity;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;// edit data

    private String callingCode, phone, countryLogo, receiptMethod, saveFlag = "0";

    private boolean showSaveList;

    private ImrExchangeRateEntity imrExchangeRateEntity;

    private String receiptOrgCode, receiptOrgBranchCode,
            transferDestinationCountryCode, birthPlace,
            birthDate, sex, cityName, districtName,
            poBox, buildingNo, street, idNo, idExpiry,
            bankAccountType, ibanNo, bankAccountNo, receiptOrgName, receiptOrgBranchName,
            cityId,channelPayeeId,channelCode,beneCurrency,branchId="";

    private ImrPaymentEntity imrPaymentEntity;

    private RiskControlEntity riskControlEntity;

    private static AddBeneficiaryTwoActivity addBeneficiaryTwoActivity;
    private BottomDialog bottomDialog;

    public static AddBeneficiaryTwoActivity getAddBeneficiaryTwoActivity() {
        return addBeneficiaryTwoActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_beneficiary_two;
    }

    //处理失焦
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (AndroidUtils.isShouldHideInput(v, ev)) {//点击editText控件外部
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    assert v != null;
                    AndroidUtils.hideKeyboard(v);//软键盘工具类
                    if (etPhoneNumber.getEditText() != null) {
                        etPhoneNumber.getEditText().clearFocus();
                    }
                    if (etBeneficiaryFullname.getEditText() != null) {
                        etBeneficiaryFullname.getEditText().clearFocus();
                    }
                    if (etBeneficiaryNickname.getEditText() != null) {
                        etBeneficiaryNickname.getEditText().clearFocus();
                    }
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        addBeneficiaryTwoActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle, llAreaNumber, tvCallingCode, etPhoneNumber);
        tvTitle.setText(getString(R.string.IMR_20));
        etBeneficiaryFullname.getTlEdit().setHint(getString(R.string.IMR_54));
        etPhoneNumber.getTlEdit().setHint(getString(R.string.IMR_39));
        etNationality.getTlEdit().setHint(getString(R.string.IMR_40));
        etBeneficiaryNickname.getTlEdit().setHint(getString(R.string.IMR_55_1));
        ivKycStep.setVisibility(View.VISIBLE);
        ivKycStep.setImageResource(R.drawable.kyc_step_2);
//        ivKycStep.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_imr_add_step_2));
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rlRelation.setLayoutManager(mLayoutManager);
        contactList.add(new KycContactEntity(getString(R.string.IMR_58), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_my_account)));
        contactList.add(new KycContactEntity(getString(R.string.IMR_59), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_family)));
        contactList.add(new KycContactEntity(getString(R.string.IMR_60), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_friends)));
        contactList.add(new KycContactEntity(getString(R.string.IMR_61), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_service)));
        contactList.add(new KycContactEntity(getString(R.string.IMR_62), ThemeSourceUtils.getSourceID(mContext, R.attr.icon_bank_other_relation)));
        transferRelationAdapter = new TransferRelationAdapter(contactList);
        transferRelationAdapter.bindToRecyclerView(rlRelation);
        etBeneficiaryFullname.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(70),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_SPACE), new NameFilter()});
        etBeneficiaryNickname.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(64),
                new NormalInputFilter(NormalInputFilter.AR_ENGLISH_SPACE)});
        etPhoneNumber.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(15),
                new NormalInputFilter(NormalInputFilter.NUMBER)});
        etNationality.setFocusableFalse();
        transferRelationAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.con_choose:
                        transferRelationAdapter.setPosition(position);
                        relationshipCode = "RELATIONSHIP_" + (position + 1);
                        break;
                    default:
                        break;
                }
                checkNextStatus();
            }
        });

        isEditBeneficiary = getIntent().getBooleanExtra(Constants.IS_FROM_EDIT_BENEFICIARY, false);
        if (isEditBeneficiary) {
            llSaveList.setVisibility(View.GONE);
            tvBeneficiaryDesc.setText(R.string.IMR_96);
            beneficiaryInfo = (ImrBeneficiaryDetails.ImrBeneficiaryDetail) getIntent().getSerializableExtra("BeneficiaryInfo");
            etBeneficiaryFullname.getEditText().setText(beneficiaryInfo.payeeFullName);
            payeeFullName = beneficiaryInfo.payeeFullName;
            countryLogo = beneficiaryInfo.countryLogo;
            callingCode = beneficiaryInfo.callingCode;
            payeeInfoCountryCode = beneficiaryInfo.payeeInfoCountryCode;
            transferDestinationCountryCode = beneficiaryInfo.transferDestinationCountryCode;
            receiptMethod = beneficiaryInfo.receiptMethod;
            imrPaymentEntity = beneficiaryInfo.imrPaymentEntity;
            if(imrPaymentEntity != null) {
                if (imrPaymentEntity.supportCurrencyList.size() > 0){
                    beneCurrency = imrPaymentEntity.supportCurrencyList.get(0);
                }
                channelCode = imrPaymentEntity.channelCode;
                channelPayeeId = imrPaymentEntity.defaultPaymentOrgCode;
            }
            etBeneficiaryFullname.setViewDisable();
            if (!TextUtils.isEmpty(callingCode)) {
                tvCallingCode.setText("+" + callingCode);
            }
            if (!TextUtils.isEmpty(countryLogo)) {
                Glide.with(mContext).clear(ivCallingCode);
                Glide.with(mContext)
                        .load(countryLogo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivCallingCode);
            }
            etNationality.setViewDisable();
            if (!TextUtils.isEmpty(beneficiaryInfo.phone)) {
                etPhoneNumber.getEditText().setText(beneficiaryInfo.phone);
            }
            if (!TextUtils.isEmpty(beneficiaryInfo.payeeInfoCountryName)) {
                etNationality.getEditText().setText(beneficiaryInfo.payeeInfoCountryName);
            }
            if (getPosition(beneficiaryInfo.relationshipCode) != -1) {
                relationshipCode = beneficiaryInfo.relationshipCode;
                transferRelationAdapter.setPosition(getPosition(beneficiaryInfo.relationshipCode));
            }
            if (!TextUtils.isEmpty(beneficiaryInfo.nickName)) {
                nickName = beneficiaryInfo.nickName;
                etBeneficiaryNickname.setContentText(beneficiaryInfo.nickName);
            }
            tvNextStep.setEnabled(true);
        } else {
            tvBeneficiaryDesc.setText(R.string.IMR_52);
            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            countryLogo = imrBeneficiaryEntity.countryLogo;
            callingCode = imrBeneficiaryEntity.callingCode;
            receiptMethod = imrBeneficiaryEntity.receiptMethod;
            transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;
            imrPaymentEntity = imrBeneficiaryEntity.imrPaymentEntity;
            if(imrPaymentEntity != null){
                if(imrPaymentEntity.supportCurrencyList.size()>0) {
                    beneCurrency = imrPaymentEntity.supportCurrencyList.get(0);
                }
                channelCode = imrPaymentEntity.channelCode;
                channelPayeeId = imrPaymentEntity.defaultPaymentOrgCode;
            }
            showSaveList = imrBeneficiaryEntity.showSaveList;
            if (showSaveList) {
                saveFlag = "1";
                llSaveList.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(callingCode)) {
                tvCallingCode.setText("+" + callingCode);
            }
            if (!TextUtils.isEmpty(countryLogo)) {
                Glide.with(mContext).clear(ivCallingCode);
                Glide.with(mContext)
                        .load(countryLogo)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .into(ivCallingCode);
            }

            etBeneficiaryFullname.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
                @Override
                public void onFocusChange(boolean hasFocus) {
                    if (!hasFocus) {
                        if (!TextUtils.isEmpty(payeeFullName) && !payeeFullName.contains(" ")) {
                            etBeneficiaryFullname.showErrorViewWithMsg(getString(R.string.IMR_notice_4));
                        }
                    }
                }
            });

            etBeneficiaryFullname.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    payeeFullName = s.toString().trim();
                    checkNextStatus();
                }
            });

            cbSaveToList.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        saveFlag = "0";
                    } else {
                        saveFlag = "1";
                    }
                }
            });

            etNationality.getEditText().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.getNationality(Constants.imr_nationality,transferDestinationCountryCode);
                }
            });

        }

        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                phone = etPhoneNumber.getEditText().getText().toString().trim();
                checkNextStatus();
            }
        });

        etPhoneNumber.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(phone) && phone.length() < 4) {
                        etPhoneNumber.showErrorViewWithMsg(getString(R.string.IMR_notice_6));
                    }
                }
            }
        });

        etBeneficiaryNickname.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                nickName = s.toString().trim();
                checkNextStatus();
            }
        });
    }

    private int getPosition(String relationshipCode) {
        int relationship = -1;
        if ("RELATIONSHIP_1".equals(relationshipCode)) {
            relationship = 0;
        } else if ("RELATIONSHIP_2".equals(relationshipCode)) {
            relationship = 1;
        } else if ("RELATIONSHIP_3".equals(relationshipCode)) {
            relationship = 2;
        } else if ("RELATIONSHIP_4".equals(relationshipCode)) {
            relationship = 3;
        } else if ("RELATIONSHIP_5".equals(relationshipCode)) {
            relationship = 4;
        }
        return relationship;
    }

    private void checkNextStatus() {
        String phone = etPhoneNumber.getEditText().getText().toString().trim();
        String nationality = etNationality.getEditText().getText().toString().trim();
        tvNextStep.setEnabled(!TextUtils.isEmpty(payeeFullName) && payeeFullName.contains(" ") && !TextUtils.isEmpty(phone) && phone.length() >= 4
                && !TextUtils.isEmpty(relationshipCode) && !TextUtils.isEmpty(nationality));
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        addBeneficiaryTwoActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ADD_SEND_BENEFICIARY == event.getEventType()) {
            requestAddBene();
        }
    }

    private void requestAddBene(){
        if(isChannelThunes(channelCode)) {
            mPresenter.imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                    nickName, relationshipCode, callingCode, phone,
                    transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                    birthDate, sex, cityName, districtName,
                    poBox, buildingNo, street, idNo, idExpiry,
                    bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
                    cityId, beneCurrency, channelPayeeId, channelCode, branchId);
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_next_step})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_next_step:
                phone = etPhoneNumber.getEditText().getText().toString().trim();
                if (isEditBeneficiary) {
                    beneficiaryInfo.nickName = nickName;
                    beneficiaryInfo.payeeInfoCountryCode = payeeInfoCountryCode;
                    beneficiaryInfo.relationshipCode = relationshipCode;
                    beneficiaryInfo.phone = phone;
                    if(isChannelThunes(channelCode)) {
                        List<String> additionalData = new ArrayList<>();
                        mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                    }else{
                        Intent intent;
                        if (Constants.imr_BT.equals(receiptMethod)) {
                            intent = new Intent(this, AddBeneficiarySixActivity.class);
                        } else {
                            intent = new Intent(this, AddBeneficiaryFiveActivity.class);
                        }
                        intent.putExtra(Constants.IS_FROM_EDIT_BENEFICIARY, true);
                        intent.putExtra("BeneficiaryInfo", beneficiaryInfo);
                        startActivity(intent);
                    }
                } else {
                    HashMap<String, Object> hashMap = new HashMap<>(16);
                    imrBeneficiaryEntity.payeeFullName = payeeFullName;
                    imrBeneficiaryEntity.nickName = nickName;
                    imrBeneficiaryEntity.payeeInfoCountryCode = payeeInfoCountryCode;
                    imrBeneficiaryEntity.relationshipCode = relationshipCode;
                    imrBeneficiaryEntity.phone = phone;
                    imrBeneficiaryEntity.saveFlag = saveFlag;
                    imrBeneficiaryEntity.beneCurrency = beneCurrency;
                    AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(imrBeneficiaryEntity);
                    if(isChannelThunes(channelCode)) {
                        List<String> additionalData = new ArrayList<>();
                        mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                    }else {
                        if (Constants.imr_BT.equals(receiptMethod)) {
                            ActivitySkipUtil.startAnotherActivityForResult(AddBeneficiaryTwoActivity.this, AddBeneficiarySixActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, 100);
                        } else {
                            ActivitySkipUtil.startAnotherActivityForResult(AddBeneficiaryTwoActivity.this, AddBeneficiaryFiveActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, 100);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (!isEditBeneficiary) {
            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            if (imrBeneficiaryEntity != null) {
                etBeneficiaryFullname.getEditText().setText(imrBeneficiaryEntity.payeeFullName);
                etBeneficiaryNickname.getEditText().setText(imrBeneficiaryEntity.nickName);
                callingCode = imrBeneficiaryEntity.callingCode;
                nationality = imrBeneficiaryEntity.nationality;
                phone = imrBeneficiaryEntity.phone;
                if (!TextUtils.isEmpty(callingCode)) {
                    tvCallingCode.setText("+" + callingCode);
                }
                if (!TextUtils.isEmpty(phone)) {
                    etPhoneNumber.getEditText().setText(phone);
                }
            }
        }
    }

    @Override
    public void getNationalitySuccess(ImrCountryListEntity imrCountryListEntity) {
        if (imrCountryListEntity != null) {
            List<ImrCountryEntity> imrCountrysList = imrCountryListEntity.imrCountrysList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = imrCountrysList.size();
            for (int i = 0; i < size; i++) {
                ImrCountryEntity imrCountryEntity = imrCountrysList.get(i);
                if (imrCountryEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.imgUrl = imrCountryEntity.countryLogo;
                    selectInfoEntity.businessParamValue = imrCountryEntity.countryName + "  " + "+" + imrCountryEntity.callingCode;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_40), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    imrCountryEntity = imrCountrysList.get(position);
                    if (imrCountryEntity != null) {
                        payeeInfoCountryCode = imrCountryEntity.countryCode;
                        nationality = imrCountryEntity.countryName;
                        etNationality.getEditText().setText(nationality);
                        checkNextStatus();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getNationalitySuccess");
        }
    }

    @Override
    public void getNationalityFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void imrGetIdOrAddressSuccess(ImrBankIdAddressEntity data) {

    }

    @Override
    public void imrGetIdOrAddressFail(String errorCode, String errorMsg) {

    }

    @Override
    public void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity) {
        if (imrAddBeneResultEntity != null) {
            if(showSaveList){
                String payeeInfoId = imrAddBeneResultEntity.payeeInfoId;
                imrBeneficiaryEntity.payeeInfoId = payeeInfoId;
                mPresenter.activateBeneficiary(payeeInfoId);
            }else{
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_ADD_BENE_SUCCESS, imrBeneficiaryEntity,riskControlEntity));
                ProjectApp.removeImrAddBeneTask();
                finish();
            }
        }
    }

    @Override
    public void imrAddBeneficiaryFail(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity) {
        if (imrExchangeRateEntity != null) {
            this.imrExchangeRateEntity = imrExchangeRateEntity;
            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
        }
    }

    @Override
    public void imrExchangeRateFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        mHashMap.put(Constants.imrExchangeRateEntity, imrExchangeRateEntity);
        mHashMap.put(Constants.ImrBeneficiaryEntity, imrBeneficiaryEntity);
        ActivitySkipUtil.startAnotherActivity(AddBeneficiaryTwoActivity.this, ImrSendMoneyActivity.class, mHashMap,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        if ("0".equals(saveFlag)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_SEND_ADD_BENE_SUCCESS, imrBeneficiaryEntity));
            ProjectApp.removeImrAddBeneTask();
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void activateBeneficiarySuccess(Void result) {
        if(imrBeneficiaryEntity != null) {
            mPresenter.imrExchangeRate(imrBeneficiaryEntity.payeeInfoId, "", "", channelCode);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddBeneficiaryContract.AddThreePresenter getPresenter() {
        return new AddBeneThreePresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr() && showSaveList){
                    jumpToPwd(true);
                }else{
                    requestAddBene();
                }
            } else {
                requestAddBene();
            }
        } else {
            requestAddBene();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        if(showSaveList) {
            mHashMap.put(Constants.sceneType, Constants.TYPE_ADD_SEND_BENE);
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_ADD_SEND_BENE);
            //bug 编号1054464
//            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtw_1));
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_new_15));
        }else{
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_ADD_IMR);
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_new_15));
            mHashMap.put(Constants.sceneType, Constants.TYPE_IMR_ADD_BENEFICIARY);
            if(isEditBeneficiary){
                mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_96));
                mHashMap.put("BeneficiaryInfo", beneficiaryInfo);
                mHashMap.put(Constants.IS_FROM_EDIT_BENEFICIARY, true);
            }
        }
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }


    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
