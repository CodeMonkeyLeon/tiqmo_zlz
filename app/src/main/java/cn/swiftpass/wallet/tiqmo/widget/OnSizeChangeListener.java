package cn.swiftpass.wallet.tiqmo.widget;

public interface OnSizeChangeListener {

    void onSizeChanged(int w, int h, int oldw, int oldh);
}
