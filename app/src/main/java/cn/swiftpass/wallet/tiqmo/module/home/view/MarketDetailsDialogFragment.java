package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.MarketPicAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.MerchantListAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.SpaceItemDecoration;

public class MarketDetailsDialogFragment extends BaseDialogFragmentN implements View.OnClickListener {

    private TextView tv_active_name;
    private TextView tv_active_des;
    private TextView tv_conditions_name;
    private TextView tv_conditions_des;
    private TextView tv_merchants;
    private ViewPager vp_active_pictures_list;
    private RecyclerView rv_merchants_list;
    private View v_market_line;

    private MarketDetailsEntity marketDetails;
    private static String TAG = "MarketDetails";

    public static MarketDetailsDialogFragment newInstance(MarketDetailsEntity marketDetails) {
        MarketDetailsDialogFragment fragment = new MarketDetailsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TAG, marketDetails);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_market_details;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);

        if (getArguments() != null) {
            marketDetails = (MarketDetailsEntity) getArguments().getSerializable(TAG);
            showDetails();
        }
    }

    private void showDetails() {
        tv_active_name.setText(marketDetails.activityName);
        tv_active_des.setText(marketDetails.activityDesc);
        tv_conditions_name.setText(getString(R.string.TS_1));
        tv_conditions_des.setText(marketDetails.activityConditions);
        tv_merchants.setText(getString(R.string.merchant_1));

        MarketPicAdapter marketPicAdapter = new MarketPicAdapter(marketDetails.vicePagePictures,
                getLayoutInflater(), getContext());
        vp_active_pictures_list.setAdapter(marketPicAdapter);
        vp_active_pictures_list.setCurrentItem(0);
        vp_active_pictures_list.setLayoutParams(new LinearLayout.LayoutParams(AndroidUtils.getScreenWidth(mContext),
                AndroidUtils.getScreenWidth(mContext) * 218 / 360));

        if (marketDetails.activityMerchantLogo != null && marketDetails.activityMerchantLogo.size() > 0) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
            layoutManager.setOrientation(RecyclerView.HORIZONTAL);
            rv_merchants_list.addItemDecoration(new SpaceItemDecoration(AndroidUtils.dip2px(mContext, 15)));
            rv_merchants_list.setLayoutManager(layoutManager);
            MerchantListAdapter marketListAdapter = new MerchantListAdapter(mContext, marketDetails.activityMerchantLogo);
            rv_merchants_list.setAdapter(marketListAdapter);
            tv_merchants.setVisibility(View.VISIBLE);
            rv_merchants_list.setVisibility(View.VISIBLE);
            v_market_line.setVisibility(View.VISIBLE);
        } else {
            tv_merchants.setVisibility(View.GONE);
            rv_merchants_list.setVisibility(View.GONE);
            v_market_line.setVisibility(View.GONE);
        }

    }

    private void initView(View parentView) {

        // 初始化window布局参数
        Window window = getDialog().getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        window.setBackgroundDrawableResource(R.color.transparent);
        window.setDimAmount(0.7f);
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = AndroidUtils.dip2px(mContext, 500f);
        wlp.windowAnimations = R.style.bottomSheet_animation;
        window.setAttributes(wlp);

        tv_active_name = parentView.findViewById(R.id.tv_active_name);
        tv_active_des = parentView.findViewById(R.id.tv_active_des);
        tv_conditions_name = parentView.findViewById(R.id.tv_conditions_name);
        tv_conditions_des = parentView.findViewById(R.id.tv_conditions_des);
        tv_merchants = parentView.findViewById(R.id.tv_merchants);
        vp_active_pictures_list = parentView.findViewById(R.id.vp_active_pictures_list);
        rv_merchants_list = parentView.findViewById(R.id.rv_merchants_list);
        v_market_line = parentView.findViewById(R.id.v_market_line);
    }


    @Override
    public void onClick(View v) {

    }

    public void show(FragmentManager manager){
        show(manager, TAG);
    }
}
