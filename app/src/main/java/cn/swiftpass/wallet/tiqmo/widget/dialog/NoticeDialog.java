package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class NoticeDialog extends BottomDialog {

    private Context mContext;
    private static NoticeDialog noticeDialog = null;
    private TextView tvNoticeTitle, tvNoticeContent;
    private ImageView ivNoticeClose;

    private String title, content;

    public void setTitle(String title) {
        this.title = title;
        if (tvNoticeTitle != null) {
            tvNoticeTitle.setText(title);
        }
    }

    public void setContent(String content) {
        this.content = content;
        if (tvNoticeContent != null) {
            tvNoticeContent.setText(content);
        }
    }

    public NoticeDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_notice, null);
        tvNoticeTitle = view.findViewById(R.id.tv_notice_title);
        tvNoticeContent = view.findViewById(R.id.tv_notice_content);
        ivNoticeClose = view.findViewById(R.id.iv_notice_close);
        ivNoticeClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (noticeDialog != null) {
                    noticeDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    public static NoticeDialog getInstance(Context context) {
        noticeDialog = new NoticeDialog(context);
        return noticeDialog;
    }

}
