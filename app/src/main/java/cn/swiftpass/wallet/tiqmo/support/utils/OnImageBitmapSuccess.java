package cn.swiftpass.wallet.tiqmo.support.utils;

import android.net.Uri;

public interface OnImageBitmapSuccess {
    void onImageBitmapCallBack(Uri uri);
}
