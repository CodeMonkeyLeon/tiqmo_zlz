package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillIOListAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillRefundListAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillRefundListPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.dialog.NoBillsFoundDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.PayBillErrorDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class PayBillRefundFragment  extends BaseFragment<PayBillContract.BillRefundPresenter> implements PayBillContract.BillRefundView{

    private static final String ERROR_BILL_OVERDUE = "080037";
    private static final String ERROR_NO_BILL = "080036";
    private static final String ERROR_NO_BILL_LOCAL = "080130";

    @BindView(R.id.tv_refund_bill)
    TextView tvRefundBill;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.rv_io_list)
    RecyclerView rvIOList;
    @BindView(R.id.ry_refund_list)
    MaxHeightRecyclerView rvRefundList;
    @BindView(R.id.tv_refund_title)
    TextView tvRefundTitle;

    private String billerId, sku, billerName;

    private String mChannelCode;

    private PayBillIOListAdapter payBillIOListAdapter;

    //退款的ioList  Moi才会有
    private List<PayBillIOEntity> refundIoList = new ArrayList<>();
    private HashMap<Integer, String> dataMap;

    private PayBillIOListEntity payBillIOListEntity;

    private PayBillOrderEntity payBillOrderEntity;

    private PayBillDetailEntity mPayBillDetailEntity;

    private PayBillRefundListAdapter payBillRefundListAdapter;
    private List<PayBillRefundEntity> refundList = new ArrayList<>();

    /**
     * Moi类型
     * @param payBillIOListEntity
     * @param billerId
     * @param billerName
     * @param channelCode
     * @return
     */
    public static PayBillRefundFragment getInstance(PayBillIOListEntity payBillIOListEntity,
                                                    String billerId,
                                                    String billerName,
                                                    String channelCode) {
        PayBillRefundFragment payBillRefundFragment = new PayBillRefundFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.paybill_billerIOListEntity, payBillIOListEntity);
        bundle.putString(Constants.paybill_billerId, billerId);
        bundle.putString(Constants.paybill_billerName, billerName);
        bundle.putString(Constants.CHANNEL_CODE, channelCode);
        payBillRefundFragment.setArguments(bundle);
        return payBillRefundFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_paybill_refund;
    }

    @Override
    protected void initView(View parentView) {
        if (getArguments() != null) {
            billerId = getArguments().getString(Constants.paybill_billerId);
            billerName = getArguments().getString(Constants.paybill_billerName);
            sku = getArguments().getString(Constants.paybill_billerSku);
            payBillIOListEntity = (PayBillIOListEntity) getArguments().getSerializable(Constants.paybill_billerIOListEntity);
            payBillOrderEntity = (PayBillOrderEntity) getArguments().getSerializable(Constants.paybill_billerOrderEntity);
            mChannelCode = getArguments().getString(Constants.CHANNEL_CODE);

            if (payBillOrderEntity != null) {
                billerId = payBillOrderEntity.billerId;
                billerName = payBillOrderEntity.billerName;
                sku = payBillOrderEntity.sku;
            }
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 24));
        rvIOList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvIOList.setLayoutManager(manager);
        payBillIOListAdapter = new PayBillIOListAdapter(refundIoList,true);
        payBillIOListAdapter.bindToRecyclerView(rvIOList);

        LinearLayoutManager refundListManager = new LinearLayoutManager(mContext);
        MyItemDecoration refundListItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 12));
        rvRefundList.addItemDecoration(refundListItemDecoration);
        refundListManager.setOrientation(RecyclerView.VERTICAL);
        rvRefundList.setLayoutManager(refundListManager);
        payBillRefundListAdapter = new PayBillRefundListAdapter(refundList);
        payBillRefundListAdapter.bindToRecyclerView(rvRefundList);

        payBillRefundListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                PayBillRefundEntity payBillRefundEntity = (PayBillRefundEntity) baseRecyclerAdapter.getDataList().get(position);
                if(payBillRefundEntity != null){
                    mPresenter.getPayBillRefundDetail(mChannelCode,payBillRefundEntity.id);
                }
            }
        });


        if (payBillIOListEntity != null) {
            setPayBillIOList(payBillIOListEntity);
        } else {
            if(Constants.paybill_local.equals(mChannelCode)){
                mPresenter.getNewPayBillIOList(billerId, billerId, mChannelCode);
            }else {
                mPresenter.getPayBillIOList(billerId, sku);
            }
        }
        mPresenter.getPayBillRefundList(billerId,mChannelCode);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }


    public void saveEditData(int position, String content) {
        dataMap.put(position, content);

        for (Integer k : dataMap.keySet()) {
            String value = dataMap.get(k);
            if (TextUtils.isEmpty(value)) {
                tvRefundBill.setEnabled(false);
                return;
            }
        }

        tvRefundBill.setEnabled(true);
    }

    @OnClick({R.id.tv_refund_bill})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_refund_bill:
                List<String> dataList = new ArrayList<>();
                if (dataMap.size() > 0) {
                    for (Integer k : dataMap.keySet()) {
                        String value = dataMap.get(k);
                        dataList.add(value);
                    }
                    mPresenter.getPayBillServiceDetail("", billerId, sku, mChannelCode, dataList,"R");
                }

                break;
            default:
                break;
        }
    }

    private void setPayBillIOList(PayBillIOListEntity payBillIOListEntity) {
        this.payBillIOListEntity = payBillIOListEntity;
        refundIoList.clear();
        if (payBillIOListEntity != null) {
            List<PayBillIOEntity> ioEntityList = payBillIOListEntity.refundIoList;
            int size = ioEntityList.size();
            dataMap = new HashMap<>();
            for(int i=0;i<ioEntityList.size();i++){
                dataMap.put(i, "");
            }
            refundIoList.addAll(ioEntityList);
            payBillIOListAdapter.setDataList(refundIoList);
        }
    }

    @Override
    public void getPayBillIOListSuccess(PayBillIOListEntity payBillIOListEntity) {
        setPayBillIOList(payBillIOListEntity);
    }

    @Override
    public void getPayBillDetailSuccess(PayBillDetailEntity payBillDetailEntity) {
        if (payBillDetailEntity != null) {
            this.mPayBillDetailEntity = payBillDetailEntity;
            List<String> invalidInputList = payBillDetailEntity.invalidInputList;
            List<PayBillIOEntity> ioEntityList = new ArrayList<>();
            ioEntityList.addAll(payBillIOListEntity.refundIoList);
            if(invalidInputList == null) invalidInputList = new ArrayList<>();
            int size = invalidInputList.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    int position = Integer.parseInt(invalidInputList.get(i));
                    for (int j = 0; j < ioEntityList.size(); j++) {
                        PayBillIOEntity payBillIOEntity = ioEntityList.get(j);
                        if (position == j + 1) {
                            payBillIOEntity.errorMsg = getString(R.string.sprint19_56);
                            ioEntityList.set(j, payBillIOEntity);
                        } else {
                            payBillIOEntity.errorMsg = "";
                            ioEntityList.set(j, payBillIOEntity);
                        }
                    }
                }
                payBillIOListEntity.refundIoList.clear();
                payBillIOListEntity.refundIoList.addAll(ioEntityList);
                payBillIOListAdapter.setDataList(payBillIOListEntity.refundIoList);
            } else {
                HashMap<String,Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.paybill_billerDetail, mPayBillDetailEntity);
                mHashMap.put(Constants.CHANNEL_CODE, mChannelCode);
                ActivitySkipUtil.startAnotherActivity(mActivity, PayBillRefundSummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                mPresenter.getLimit(Constants.LIMIT_TRANSFER);
            }
        }
    }

    private void showErrorDialog(int errorType) {
        PayBillErrorDialog payBillErrorDialog = new PayBillErrorDialog(mContext, errorType, billerName);
        payBillErrorDialog.setBottomButtonListener(new PayBillErrorDialog.BottomButtonListener() {
            @Override
            public void clickButton() {
                payBillErrorDialog.dismiss();
            }
        });
        payBillErrorDialog.showWithBottomAnim();
    }

    private void showNoBillsFoundDialog() {
        NoBillsFoundDialog noBillsFoundDialog = new NoBillsFoundDialog(mContext);
        noBillsFoundDialog.showWithBottomAnim();
    }

    @Override
    public void getPayBillDetailFail(String errorCode, String errorMsg) {
        if(ERROR_NO_BILL_LOCAL.equals(errorCode)){
            showNoBillsFoundDialog();
        }else if (ERROR_NO_BILL.equals(errorCode)) {
            //no bill found,查无账单
            showErrorDialog(1);
        } else if (ERROR_BILL_OVERDUE.equals(errorCode)) {
            //bill overdue,账单超期
            showErrorDialog(2);
        } else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void getPayBillRefundListSuccess(PayBillRefundListEntity payBillRefundListEntity) {
        if(payBillRefundListEntity != null){
            if(payBillRefundListEntity.refundInfoList.size()>0) {
                tvRefundTitle.setVisibility(View.VISIBLE);
            }else{
                tvRefundTitle.setVisibility(View.INVISIBLE);
            }
            refundList.clear();
            refundList.addAll(payBillRefundListEntity.refundInfoList);
            payBillRefundListAdapter.setDataList(refundList);
        }
    }

    @Override
    public void getPayBillRefundDetailSuccess(PayBillRefundEntity payBillRefundEntity) {
        if(payBillRefundEntity != null){
            HashMap<String, Object> mHashMaps = new HashMap<>(16);
            CardNotifyEntity cardNotifyEntity = new CardNotifyEntity();
            cardNotifyEntity.orderAmount = payBillRefundEntity.tradeAmount;
            cardNotifyEntity.paymentMethodDesc = payBillRefundEntity.paymentMethod;
            cardNotifyEntity.orderNo = payBillRefundEntity.orderNo;
            cardNotifyEntity.orderDesc = payBillRefundEntity.orderDesc;
            cardNotifyEntity.startTime = payBillRefundEntity.createTime;
            cardNotifyEntity.logo = payBillRefundEntity.logo;
            cardNotifyEntity.basicExtendProperties = payBillRefundEntity.basicExtendProperties;
            cardNotifyEntity.orderStatus = payBillRefundEntity.orderStatus;
            cardNotifyEntity.orderDirection = payBillRefundEntity.orderDirection;
            cardNotifyEntity.sceneType = payBillRefundEntity.sceneType;
            cardNotifyEntity.remark = payBillRefundEntity.remark;
            mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            ActivitySkipUtil.startAnotherActivity(mActivity, AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        PayBillDetailActivity.startPayBillDetailActivity(mActivity,
                mPayBillDetailEntity,
                transferLimitEntity,
                mChannelCode,true);
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }


    @Override
    public void getNewPayBillIOListSuccess(PayBillIOListEntity result) {
        if (result != null) {
            setPayBillIOList(updateData(payBillIOListEntity, result));
        }
    }


    private PayBillIOListEntity updateData(PayBillIOListEntity oldData, PayBillIOListEntity newData) {

        if (!TextUtils.isEmpty(oldData.paymentRulePrompt)) {
            newData.paymentRulePrompt = oldData.paymentRulePrompt;
        }

        if (!TextUtils.isEmpty(oldData.aliasName)) {
            newData.aliasName = oldData.aliasName;
        }

        if (!TextUtils.isEmpty(oldData.isValidBiller)) {
            newData.isValidBiller = oldData.isValidBiller;
        }

        newData.sceneType = oldData.sceneType;

        for (PayBillIOEntity oldE : oldData.refundIoList) {
            for (PayBillIOEntity newE : newData.refundIoList) {
                if (TextUtils.equals(oldE.name, newE.name)) {
                    newE.description = oldE.description;
                    newE.value = oldE.value;
                    newE.errorMsg = oldE.errorMsg;
                }
            }
        }

        return newData;

    }


    @Override
    public PayBillContract.BillRefundPresenter getPresenter() {
        return new PayBillRefundListPresenter();
    }
}
