package cn.swiftpass.wallet.tiqmo.module.register.presenter;

import cn.swiftpass.wallet.tiqmo.module.register.contract.RegisterUserDetailContract;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterFinishEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RegUserDetailPresenter implements RegisterUserDetailContract.Presenter {

    private RegisterUserDetailContract.View baseView;

    @Override
    public void loginAccount(String callingCode, String phone, String password, String verifyCode, String type, double longitude, double latitude) {
        if (baseView == null) {
            return;
        }
        AppClient.getInstance().loginByType(callingCode, phone, password, verifyCode, type, longitude, latitude, new LifecycleMVPResultCallback<UserInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(UserInfoEntity response) {
                baseView.loginAccountSuccess(response);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.loginAccountError(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void preCheckPd(String passwordType,String operatorType, String oldPassword) {
        AppClient.getInstance().preCheckPd(passwordType,operatorType, oldPassword, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                baseView.preCheckPdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.preCheckPdFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void finishRegister(String fullName, String callingCode, String phone, String password, String authType,
                               double longitude,double latitude,String noStr,String referralCode,
                               String shareLink) {
        AppClient.getInstance().registerByPhone(fullName, callingCode, phone, password, longitude, latitude,noStr,
                referralCode,shareLink,new LifecycleMVPResultCallback<RegisterFinishEntity>(baseView,true) {
            @Override
            protected void onSuccess(RegisterFinishEntity result) {
                baseView.finishRegSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.finishRegFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void forgetSetPwd(String callingCode, String phone, String password) {
        AppClient.getInstance().forgetPassword(callingCode, phone, password, new LifecycleMVPResultCallback<Void>(baseView,true) {
            @Override
            protected void onSuccess(Void result) {
                baseView.forgetSetPwdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.forgetSetPwdFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void changePwd(String pdType, String password, String oldPassword, String operatorType, String sceneType, String referralCode) {
        AppClient.getInstance().changePassword(pdType, password, oldPassword, operatorType, sceneType, referralCode, new LifecycleMVPResultCallback<UserInfoEntity>(baseView, true) {
            @Override
            protected void onSuccess(UserInfoEntity result) {
                baseView.changePwdSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                baseView.changePwdFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(RegisterUserDetailContract.View view) {
        this.baseView = view;
    }

    @Override
    public void detachView() {
        this.baseView = null;
    }
}
