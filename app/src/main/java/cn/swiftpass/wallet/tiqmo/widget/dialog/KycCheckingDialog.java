package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;

public class KycCheckingDialog extends BottomDialog{

    private Context mContext;

    private ContinueListener continueListener;

    public void setContinueListener(ContinueListener continueListener) {
        this.continueListener = continueListener;
    }

    public KycCheckingDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface ContinueListener {
        void clickContinue();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_kyc_checking, null);
        TextView tvAccept = view.findViewById(R.id.tv_accept);

        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (continueListener != null) {
                    continueListener.clickContinue();
                }
            }
        });

        setContentView(view);
        setCanceledOnTouchOutside(false);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
