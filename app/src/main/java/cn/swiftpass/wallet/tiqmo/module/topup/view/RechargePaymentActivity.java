package cn.swiftpass.wallet.tiqmo.module.topup.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ClipPagerTitleView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.UnScrollViewPager;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.ImrMainPagerAdapter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeDomeOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeInterOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class RechargePaymentActivity extends BaseCompatActivity {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.tv_add_payment)
    TextView tvAddPayment;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.indicator_recharge_main)
    MagicIndicator indicatorRechargeMain;
    @BindView(R.id.vp_recharge_main)
    UnScrollViewPager vpRechargeMain;

    private RechargeDomesticFragment rechargeDomesticFragment;
    private RechargeInterFragment rechargeInterFragment;
    private RechargeOrderListEntity rechargeOrderListEntity;

    List<RechargeDomeOrderEntity> domesticList = new ArrayList<>();
    List<RechargeInterOrderEntity> internationalList = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_recharge_payments;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, ivHeadCircle);
        tvTitle.setText(R.string.newhome_8);

        List<String> title = new ArrayList<>(Arrays.asList(this.getString(R.string.BillPay_4),
                this.getString(R.string.BillPay_5)));

        if (getIntent().getExtras() != null) {
            rechargeOrderListEntity = (RechargeOrderListEntity) getIntent().getExtras().getSerializable(Constants.rechargeOrderListEntity);
            domesticList.addAll(rechargeOrderListEntity.domesticList);
            internationalList.addAll(rechargeOrderListEntity.internationalList);
        }

        List<Fragment> fragments = new ArrayList<>();
        rechargeDomesticFragment = RechargeDomesticFragment.getInstance(rechargeOrderListEntity,false);
        rechargeInterFragment = RechargeInterFragment.getInstance(rechargeOrderListEntity);
        fragments.add(rechargeDomesticFragment);
        fragments.add(rechargeInterFragment);
        if (LocaleUtils.isRTL(mContext)) {
            Collections.reverse(title);
            Collections.reverse(fragments);
        }
        ImrMainPagerAdapter adapter = new ImrMainPagerAdapter(getSupportFragmentManager(), fragments);
        vpRechargeMain.setScrollEnable(false);// 禁止滑动
        vpRechargeMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        vpRechargeMain.setAdapter(adapter);
        if (LocaleUtils.isRTL(mContext)) {
            vpRechargeMain.setCurrentItem(title.size() - 1);
        }
        indicatorRechargeMain.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext, R.attr.bg_radius10_all_051446_white));
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return title == null ? 0 : title.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ClipPagerTitleView clipPagerTitleView = new ClipPagerTitleView(context);
                clipPagerTitleView.setText(title.get(index));
                clipPagerTitleView.setTextSize(UIUtil.dip2px(context, 14));
                clipPagerTitleView.setTextColor(getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_666b6c73)));
                clipPagerTitleView.setClipColor(Color.WHITE);
                clipPagerTitleView.setTextTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));
                clipPagerTitleView.setClipTypeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf"));

                clipPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ButtonUtils.isFastDoubleClick()) {
                            return;
                        }
                        vpRechargeMain.setCurrentItem(index);
                        if(LocaleUtils.isRTL(mContext)) {
                            if (index == 0) {
                                tvAddPayment.setVisibility(View.VISIBLE);
                            } else {
                                tvAddPayment.setVisibility(View.GONE);
                            }
                        }else{
                            if (index == 1) {
                                tvAddPayment.setVisibility(View.VISIBLE);
                            } else {
                                tvAddPayment.setVisibility(View.GONE);
                            }
                        }
                    }
                });
                return clipPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setLineHeight(UIUtil.dip2px(context, 44));
                indicator.setRoundRadius(UIUtil.dip2px(context, 7));
                indicator.setColors(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_0f66b5_1da1f1)));
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });
        indicatorRechargeMain.setNavigator(commonNavigator);
        commonNavigator.onPageSelected(LocaleUtils.isRTL(mContext) ? title.size() - 1 : 0);
        ViewPagerHelper.bind(indicatorRechargeMain, vpRechargeMain);

        try {
            //第二个有值就默认第一个
            if (internationalList.size() > 0) {
                vpRechargeMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 0 : 1);
                tvAddPayment.setVisibility(View.VISIBLE);
            } else {
                vpRechargeMain.setCurrentItem(LocaleUtils.isRTL(mContext) ? 1 : 0);
                tvAddPayment.setVisibility(View.GONE);
            }
        } catch (Throwable e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ADD_PAYMENT_SUCCESS == event.getEventType()) {
//            if (payBillDomesticFragment != null) {
//                payBillDomesticFragment.getPayBillOrderList("Y");
//            }
//            if (payBillInterFragment != null) {
//                payBillInterFragment.getPayBillOrderList("N");
//            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_add_payment})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_payment:
                HashMap<String, Object> mHashMap = new HashMap<>();
                mHashMap.put(Constants.rechargeOrderListEntity, rechargeOrderListEntity);
                ActivitySkipUtil.startAnotherActivity(this, RechargeCountryActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }
}
