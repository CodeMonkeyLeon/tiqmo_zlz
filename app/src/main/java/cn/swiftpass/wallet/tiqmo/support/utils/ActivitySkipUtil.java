package cn.swiftpass.wallet.tiqmo.support.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.view.MainActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;

public class ActivitySkipUtil {
    public enum ANIM_TYPE {
        NONE, RIGHT_IN, LEFT_IN, LEFT_OUT, RIGHT_OUT,BOTTOM_IN,BOTTOM_OUT,
    }

    public ActivitySkipUtil() {
        throw new UnsupportedOperationException("ActivitySkipUtil不能实例化");
    }

    /**
     * 功能描述:简单地Activity的跳转(不携带任何数据)
     *
     * @param context 发起跳转的Activity实例
     * @param cls     目标Activity实例
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void startAnotherActivity(Context context, Class<? extends Activity> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    /**
     * 功能描述:简单地Activity的跳转(不携带任何数据)
     *
     * @param activity       发起跳转的Activity实例
     */
    public static void startAnotherActivity(Activity activity, Class<? extends Activity> cls) {
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
    }

    /**
     * 登录后切换语言，跳转到mainActivity
     *
     * @param activity       发起跳转的Activity实例
     */
    public static void clearTaskToMainActivity(Activity activity) {
        if (AppClient.getInstance().getUserManager().getUserInfo() != null) {
            Intent intent = new Intent(activity, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.switch_locale_in, R.anim.switch_locale_out);
        }
    }

    /**
     * 功能描述:简单地Activity的跳转(不携带任何数据)
     *
     * @param activity       发起跳转的Activity实例
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void pickAnotherActivity(Activity activity, Class<? extends Activity> cls, ANIM_TYPE animType) {
        Intent intent = new Intent(activity, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    /**
     * 直接启动一个activity <功能详细描述>
     *
     * @param cla
     * @see [类、类#方法、类#成员]
     */
    public static void startActivityForResult(Activity activity, Class<? extends Activity> cla, int requestCode, ANIM_TYPE animType) {
        Intent intent = new Intent(activity, cla);
        activity.startActivityForResult(intent, requestCode);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    public static void startActivityForResult(Activity activity, Class<? extends Activity> cla, int requestCode) {
        startActivityForResult(activity, cla, requestCode, ANIM_TYPE.NONE);
    }


    public static void startAnotherActivity(Activity activity, Class<? extends Activity> cls, ANIM_TYPE animType) {
        startAnotherActivity(activity, cls);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    private static int getAnimationId(ANIM_TYPE animType) {
        int startAnimId = 0;
        switch (animType) {
            case LEFT_IN:
                startAnimId = startFromLeftInAnim();
                break;
            case RIGHT_IN:
                startAnimId = startFromRightInAnim();
                break;
            case LEFT_OUT:
                startAnimId = finishFromLeftOutAnim();
                break;
            case RIGHT_OUT:
                startAnimId = finishFromRightOutAnim();
                break;
            case BOTTOM_IN:
                startAnimId = finishFromBottomInAnim();
                break;
            case BOTTOM_OUT:
                startAnimId = finishFromBottomOutAnim();
                break;
            default:
                startAnimId = 0;
                break;
        }
        return startAnimId;
    }

    public static void finishActivityWithAnim(Activity activity, ANIM_TYPE animType) {
        int finishAnimId = getAnimationId(animType);
        if (finishAnimId != 0) {
            activity.overridePendingTransition(R.anim.none, finishAnimId);
        }
    }

    public static void finishWithAnim(Activity activity, ANIM_TYPE animType) {
        if(activity == null) {
            return;
        }
        activity.finish();
        int finishAnimId = getAnimationId(animType);
        if (finishAnimId != 0) {
            activity.overridePendingTransition(R.anim.none, finishAnimId);
        }
    }

    /**
     * 结束此activity时动画 从屏幕的右边移出
     *
     * @see [类、类#方法、类#成员]
     */
    private static int finishFromRightOutAnim() {
        return R.anim.from_right_out;
    }

    private static int finishFromBottomInAnim() {
        return R.anim.dialog_enter;
    }
    private static int finishFromBottomOutAnim() {
        return R.anim.dialog_exit;
    }

    /**
     * 结束此activity时动画 从屏幕的左边移出
     *
     * @see [类、类#方法、类#成员]
     */
    private static int finishFromLeftOutAnim() {
        return R.anim.from_left_out;
    }


    /**
     * @throws
     * @Title: startFromRightInAnim
     * @Description: 从屏幕的右边进入
     */
    public static int startFromRightInAnim() {
        return R.anim.from_right_in;
    }

    /**
     * @throws
     * @Title: startFromLeftInAnim
     * @Description: 从屏幕的左边进入
     */
    public static int startFromLeftInAnim() {
        return R.anim.from_left_in;
    }


    /**
     * 功能描述：带数据的Activity之间的跳转
     *
     * @param activity
     * @param cls
     * @param hashMap
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void startActivityForResult(Activity activity, Class<? extends Activity> cls, Map<String, ? extends Object> hashMap, int requestCode) {
        Intent intent = new Intent(activity, cls);
        Iterator<?> iterator = hashMap.entrySet().iterator();
        while (iterator.hasNext()) {
            @SuppressWarnings("unchecked") Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                intent.putExtra(key, (String) value);
            }
            if (value instanceof Boolean) {
                intent.putExtra(key, (boolean) value);
            }
            if (value instanceof Integer) {
                intent.putExtra(key, (int) value);
            }
            if (value instanceof Float) {
                intent.putExtra(key, (float) value);
            }
            if (value instanceof Double) {
                intent.putExtra(key, (double) value);
            }

            if (value instanceof Serializable) {
                intent.putExtra(key, (Serializable) value);
            }

            if (value instanceof Parcelable) {
                intent.putExtra(key, (Parcelable) value);
            }
            if (value instanceof ArrayList) {
                intent.putParcelableArrayListExtra(key, (ArrayList<? extends Parcelable>) value);
            }
        }
        activity.startActivityForResult(intent, requestCode);
        int startAnimId = getAnimationId(ANIM_TYPE.NONE);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    /**
     * 功能描述：带数据的Activity之间的跳转
     *
     * @param activity
     * @param cls
     * @param hashMap
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void pickActivityForResult(Activity activity, Class<? extends Activity> cls, HashMap<String, ? extends Object> hashMap, int requestCode) {
        Intent intent = new Intent(activity, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        Iterator<?> iterator = hashMap.entrySet().iterator();
        while (iterator.hasNext()) {
            @SuppressWarnings("unchecked") Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                intent.putExtra(key, (String) value);
            }
            if (value instanceof Boolean) {
                intent.putExtra(key, (boolean) value);
            }
            if (value instanceof Integer) {
                intent.putExtra(key, (int) value);
            }
            if (value instanceof Float) {
                intent.putExtra(key, (float) value);
            }
            if (value instanceof Double) {
                intent.putExtra(key, (double) value);
            }

            if (value instanceof Serializable) {
                intent.putExtra(key, (Serializable) value);
            }
            if (value instanceof Parcelable) {
                intent.putExtra(key, (Parcelable) value);
            }
            if (value instanceof ArrayList) {
                intent.putParcelableArrayListExtra(key, (ArrayList<? extends Parcelable>) value);
            }
        }
        activity.startActivityForResult(intent, requestCode);
        int startAnimId = getAnimationId(ANIM_TYPE.NONE);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }


    /**
     * 功能描述：带数据的Activity之间的跳转
     *
     * @param activity
     * @param cls
     * @param hashMap
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void startAnotherActivity(Activity activity, Class<? extends Activity> cls, Map<String, ? extends Object> hashMap, ANIM_TYPE animType) {
        Intent intent = new Intent(activity, cls);
        Iterator<?> iterator = hashMap.entrySet().iterator();
        while (iterator.hasNext()) {
            @SuppressWarnings("unchecked") Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                intent.putExtra(key, (String) value);
            }
            if (value instanceof Boolean) {
                intent.putExtra(key, (boolean) value);
            }
            if (value instanceof Integer) {
                intent.putExtra(key, (int) value);
            }
            if (value instanceof Float) {
                intent.putExtra(key, (float) value);
            }
            if (value instanceof Double) {
                intent.putExtra(key, (double) value);
            }

            if (value instanceof Serializable) {
                intent.putExtra(key, (Serializable) value);
            }
            if (value instanceof Parcelable) {
                intent.putExtra(key, (Parcelable) value);
            }
            if (value instanceof ArrayList) {
                intent.putParcelableArrayListExtra(key, (ArrayList<? extends Parcelable>) value);
            }

        }
        activity.startActivity(intent);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    /**
     * 功能描述：带数据的Activity之间的跳转
     *
     * @param activity
     * @param cls
     * @param hashMap
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void pickAnotherActivity(Activity activity, Class<? extends Activity> cls, Map<String, ? extends Object> hashMap, ANIM_TYPE animType) {
        Intent intent = new Intent(activity, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        Iterator<?> iterator = hashMap.entrySet().iterator();
        while (iterator.hasNext()) {
            @SuppressWarnings("unchecked") Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                intent.putExtra(key, (String) value);
            }
            if (value instanceof Boolean) {
                intent.putExtra(key, (boolean) value);
            }
            if (value instanceof Integer) {
                intent.putExtra(key, (int) value);
            }
            if (value instanceof Float) {
                intent.putExtra(key, (float) value);
            }
            if (value instanceof Double) {
                intent.putExtra(key, (double) value);
            }

            if (value instanceof Serializable) {
                intent.putExtra(key, (Serializable) value);
            }
            if (value instanceof Parcelable) {
                intent.putExtra(key, (Parcelable) value);
            }
            if (value instanceof ArrayList) {
                intent.putParcelableArrayListExtra(key, (ArrayList<? extends Parcelable>) value);
            }
        }
        activity.startActivity(intent);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    /**
     * 功能描述：带数据的Activity之间的跳转
     *
     * @param activity
     * @param cls
     * @param key
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void pickActivity(Activity activity, Class<? extends Activity> cls, String key, Serializable data, ANIM_TYPE animType) {
        Intent intent = new Intent(activity, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(key, data);
        activity.startActivity(intent);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    /**
     * 功能描述：带数据的Activity之间的跳转
     *
     * @param activity
     * @param cls
     * @param key
     * @Time 2016年4月25日
     * @Author lizy18
     */
    public static void startAnotherActivity(Activity activity, Class<? extends Activity> cls, String key, Serializable data, ANIM_TYPE animType) {
        Intent intent = new Intent(activity, cls);

        intent.putExtra(key, data);
        activity.startActivity(intent);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    public static void startAnotherActivity(Context activity, Class<? extends Activity> cls, String key, Serializable data) {
        Intent intent = new Intent(activity, cls);
        intent.putExtra(key, data);
        activity.startActivity(intent);
    }

    public static void startAnotherActivityForResult(Activity activity, Class<? extends Activity> cls, HashMap<String, ? extends Object> hashMap, ANIM_TYPE animType, int requestCode) {
        Intent intent = new Intent(activity, cls);
        Iterator<?> iterator = hashMap.entrySet().iterator();
        while (iterator.hasNext()) {
            @SuppressWarnings("unchecked") Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                intent.putExtra(key, (String) value);
            }
            if (value instanceof Boolean) {
                intent.putExtra(key, (boolean) value);
            }
            if (value instanceof Integer) {
                intent.putExtra(key, (int) value);
            }
            if (value instanceof Float) {
                intent.putExtra(key, (float) value);
            }
            if (value instanceof Double) {
                intent.putExtra(key, (double) value);
            }

            if (value instanceof Serializable) {
                intent.putExtra(key, (Serializable) value);
            }
            if (value instanceof Parcelable) {
                intent.putExtra(key, (Parcelable) value);
            }
            if (value instanceof ArrayList) {
                intent.putParcelableArrayListExtra(key, (ArrayList<? extends Parcelable>) value);
            }
        }
        activity.startActivityForResult(intent, requestCode);
        int startAnimId = getAnimationId(animType);
        if (startAnimId != 0) {
            activity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }
}
