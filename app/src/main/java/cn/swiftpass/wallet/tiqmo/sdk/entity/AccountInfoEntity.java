package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.text.TextUtils;

import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class AccountInfoEntity extends BaseEntity {

    public String authed;
    private String balance;
    public String frozenBalance;
    public String currencyCode;
    public String accountType;
    public String accountStatus;
    public String accountName;
    public String ibanNum;
    public String accountNo;

    public String getBalance() {
        try {
            if (!TextUtils.isEmpty(balance)) {
                String resultMoney = BigDecimalFormatUtils.div(balance,UserInfoManager.getInstance().getConvertUnit(),2);
                return AmountUtil.dataFormat(resultMoney);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return null;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
