package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.contract.CharityContract;
import cn.swiftpass.wallet.tiqmo.module.home.presenter.CharityOrderPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.WebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class CharityActivity extends BaseCompatActivity<CharityContract.CharityPresenter> implements CharityContract.CharityView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ll_donation)
    RadioGroup rgDonation;
    @BindView(R.id.et_amount)
    EditTextWithDel etAmount;
    @BindView(R.id.tv_money_currency)
    TextView tvMoneyCurrency;
    @BindView(R.id.tv_10)
    TextView tv10;
    @BindView(R.id.tv_50)
    TextView tv50;
    @BindView(R.id.tv_100)
    TextView tv100;
    @BindView(R.id.ll_select_money)
    LinearLayout llSelectMoney;
    @BindView(R.id.tv_notice)
    TextView tvNotice;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    private static final int AMOUNT_10 = 10;
    private static final int AMOUNT_50 = 50;
    private static final int AMOUNT_100 = 100;
    @BindView(R.id.rb_one)
    RadioButton rbOne;
    @BindView(R.id.rb_two)
    RadioButton rbTwo;
    @BindView(R.id.rb_three)
    RadioButton rbThree;
    @BindView(R.id.rb_four)
    RadioButton rbFour;
    private TransferLimitEntity transferLimitEntity;
    private double money;

    private RechargeOrderInfoEntity mRechargeOrderInfoEntity;

    private String domainDonation = "1";

    private double monthLimitMoney, valiableMoney;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_charity;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
//        windowColor();
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack,headCircle);
        tvTitle.setText(R.string.sprint17_21);
        etAmount.getTlEdit().setHint(getString(R.string.sprint17_25));
        if (getIntent() != null && getIntent().getExtras() != null) {
            transferLimitEntity = (TransferLimitEntity) getIntent().getExtras().getSerializable(Constants.TransferLimitEntity);
            tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
            tv10.setText(String.valueOf(AMOUNT_10));
            tv50.setText(String.valueOf(AMOUNT_50));
            tv100.setText(String.valueOf(AMOUNT_100));
            UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            tvNotice.setText(Spans.builder()
                    .text(mContext.getString(R.string.MobileValidation_SU_0002_2_D_6)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_6b6c73))).size(12)
                    .text(" " + mContext.getString(R.string.IMR_sendMoney_5)).color(mContext.getColor(R.color.color_fc4f00)).size(12).click(tvNotice, new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View widget) {
                            HashMap<String, Object> hashMap = new HashMap<>();
                            hashMap.put(Constants.terms_condition_type, "3");
                            ActivitySkipUtil.startAnotherActivity(CharityActivity.this, WebViewActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }

                        @Override
                        public void updateDrawState(@NonNull TextPaint ds) {
                            ds.setColor(mContext.getColor(R.color.color_fc4f00));
                            ds.setUnderlineText(false);
                        }
                    }).build());

            String balance = userInfoEntity.getBalance();
            String balanceNumber = userInfoEntity.getBalanceNumber();
            if (!TextUtils.isEmpty(balanceNumber)) {
                try {
                    balanceNumber = balanceNumber.replace(",", "");
                    valiableMoney = Double.parseDouble(balanceNumber);
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---" + e + "---");
                }
            }

            rgDonation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int viewId) {
                    switch (viewId){
                        case R.id.rb_one:
                            domainDonation = "1";
                            break;
                        case R.id.rb_two:
                            domainDonation = "2";
                            break;
                        case R.id.rb_three:
                            domainDonation = "3";
                            break;
                        case R.id.rb_four:
                            domainDonation = "4";
                            break;
                        default:
                            break;
                    }
                }
            });

            //两位小数过滤
            etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
            etAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            etAmount.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String amount = s.toString();
                    amount = amount.replace(",", "");
                    if (TextUtils.isEmpty(amount)) {
                        tvMoneyCurrency.setVisibility(View.GONE);
                        tvConfirm.setEnabled(false);
                    } else {
                        tvMoneyCurrency.setVisibility(View.VISIBLE);
                        try {
                            checkMoney(amount);
                        } catch (Exception e) {
                            tvConfirm.setEnabled(false);
                        }
                        if (money != AMOUNT_10) {
                            tv10.setSelected(false);
                        }
                        if (money != AMOUNT_50) {
                            tv50.setSelected(false);
                        }
                        if (money != AMOUNT_100) {
                            tv100.setSelected(false);
                        }
                    }
                }
            });

            etAmount.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    String amount = etAmount.getEditText().getText().toString().trim();
                    amount = amount.replace(",", "");
                    String fomatAmount = AmountUtil.dataFormat(amount);
                    if (!hasFocus) {
                        if (!TextUtils.isEmpty(amount)) {
                            etAmount.getEditText().setText(fomatAmount);
                            checkMoney(amount);
                        }
                    }
                }
            });

            tv10.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tv10.setSelected(true);
                    tv50.setSelected(false);
                    tv100.setSelected(false);
                    String formatAmount = AmountUtil.dataFormat(String.valueOf(AMOUNT_10));
                    etAmount.getEditText().setText(formatAmount);
                    checkMoney(String.valueOf(AMOUNT_10));
                }
            });

            tv50.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tv50.setSelected(true);
                    tv10.setSelected(false);
                    tv100.setSelected(false);
                    String formatAmount = AmountUtil.dataFormat(String.valueOf(AMOUNT_50));
                    etAmount.getEditText().setText(formatAmount);
                    checkMoney(String.valueOf(AMOUNT_50));
                }
            });

            tv100.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tv100.setSelected(true);
                    tv50.setSelected(false);
                    tv10.setSelected(false);

                    String formatAmount = AmountUtil.dataFormat(String.valueOf(AMOUNT_100));
                    etAmount.getEditText().setText(formatAmount);
                    checkMoney(String.valueOf(AMOUNT_100));
                }
            });
        }
    }

    private void checkContinueStatus() {
        String amount = etAmount.getEditText().getText().toString().trim();
        amount = amount.replace(",", "");
        if (!TextUtils.isEmpty(amount)) {
            money = Double.parseDouble(amount);
            if (money == 0) {
                tvConfirm.setEnabled(false);
            } else {
                tvConfirm.setEnabled(true);
            }
        }
    }

    private void checkMoney(String amount) {
        tvConfirm.setEnabled(false);
        if (TextUtils.isEmpty(amount)) {
            return;
        }
        money = Double.parseDouble(amount);
        if (money > valiableMoney) {
            etAmount.showErrorViewWithMsg(getString(R.string.sprint20_84));
            return;
        }
        etAmount.showErrorViewWithMsg("");
        checkContinueStatus();
    }

    @OnClick({R.id.tv_confirm,R.id.iv_back})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                String amount = etAmount.getEditText().getText().toString().trim();
                amount = amount.replace(",", "");
                mPresenter.preCharityOrder("Charity", AndroidUtils.getReqTransferMoney(amount),domainDonation);
                break;
            default:
                break;
        }
    }

    @Override
    public void preCharityOrderSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        this.mRechargeOrderInfoEntity = rechargeOrderInfoEntity;
        if (rechargeOrderInfoEntity != null) {
            String orderInfo = rechargeOrderInfoEntity.orderInfo;
            if (!TextUtils.isEmpty(orderInfo)) {
                mPresenter.checkOut("", orderInfo);
            }
        }
    }

    @Override
    public void preCharityOrderFail(String errorCode, String errorMsg) {
        if("080131".equals(errorCode)){
            etAmount.showErrorViewWithMsg(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void checkOutSuccess(CheckOutEntity checkOutEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.rechargeOrderInfoEntity, mRechargeOrderInfoEntity);
        ActivitySkipUtil.startAnotherActivity(this,
                CharitySummaryActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void checkOutFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public CharityContract.CharityPresenter getPresenter() {
        return new CharityOrderPresenter();
    }
}
