//package cn.swiftpass.wallet.tiqmo.sdk.net.api;
//
//import java.math.BigDecimal;
//import java.util.List;
//
//import cn.swiftpass.wallet.sdk.entity.BalanceEntity;
//import cn.swiftpass.wallet.sdk.entity.JsonResponse;
//import cn.swiftpass.wallet.sdk.entity.OrderEntity;
//import cn.swiftpass.wallet.sdk.entity.TopUpListEntity;
//import cn.swiftpass.wallet.sdk.net.RequestCall;
//import cn.swiftpass.wallet.sdk.net.annotation.Headers;
//import cn.swiftpass.wallet.sdk.net.annotation.Param;
//
///**
// * Created by 叶智星 on 2018年09月18日.
// * 每一个不曾起舞的日子，都是对生命的辜负。
// */
//public interface BalanceApi {
//
//    @Headers({"Service-Id:1032"})
//    RequestCall<JsonResponse<List<BalanceEntity>>> getBalance();
//
//
//    @Headers({"Service-Id:1063"})
//    RequestCall<JsonResponse<TopUpListEntity>> queryTopUpList();
//
//    @Headers({"Service-Id:1003"})
//    RequestCall<JsonResponse<OrderEntity>> createTopUpOrder(@Param("amount") BigDecimal amount,
//                                                            @Param("currencyCode") String currencyCode);
//
//
//    @Headers({"Service-Id:2098"})
//    RequestCall<JsonResponse<OrderEntity>> createWithdrawOrder(@Param("amount") BigDecimal amount,
//                                                               @Param("currencyCode") String currencyCode,
//                                                               @Param("cardId") String cardID);
//}
