package cn.swiftpass.wallet.tiqmo.module.login;

import android.content.Context;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;

public class PwdIconUtils {

    public static boolean setPwdHide(Context mContext, boolean isHide, CustomizeEditText et) {
        if (isHide) {
            // 获取光标位置
//            int index = et.getSelectionStart();
            //如果已经隐藏了密码  则显示
            isHide = false;
            et.setIcRight(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_hide_pwd), 14);
            et.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            // 重新设置光标位置
//            et.setSelection(index);
        } else {
            // 获取光标位置
//            int index = et.getSelectionStart();
            isHide = true;
            et.setIcRight(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_show_pwd), 16);
            et.setTransformationMethod(PasswordTransformationMethod.getInstance());
            // 重新设置光标位置
//            et.setSelection(index);
        }
        return isHide;
    }
}
