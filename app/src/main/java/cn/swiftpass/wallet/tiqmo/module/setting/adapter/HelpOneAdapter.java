package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpOneEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpTwoEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.QuestionListActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class HelpOneAdapter extends BaseRecyclerAdapter<HelpOneEntity> {

    private int currentPosition = 0;

    public HelpOneAdapter(@Nullable List<HelpOneEntity> data) {
        super(R.layout.item_how_to_help,data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, HelpOneEntity helpOneEntity, int position) {
        try {
            baseViewHolder.setText(R.id.tv_help_title, helpOneEntity.title);
            baseViewHolder.setText(R.id.tv_help_content, helpOneEntity.content);
            LinearLayout llHelpType = baseViewHolder.getView(R.id.ll_help_type);
            LinearLayout llHelpTitle = baseViewHolder.getView(R.id.ll_help_title);
            LinearLayout llHelpContent = baseViewHolder.getView(R.id.ll_help_content);
            RecyclerView ryQuestion = baseViewHolder.getView(R.id.ry_question);
            ImageView ivHelpArrow = baseViewHolder.getView(R.id.iv_help_arrow);
            LocaleUtils.viewRotationY(mContext, ivHelpArrow);
            LinearLayoutManager manager = new LinearLayoutManager(mContext);
            MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
            if (ryQuestion.getItemDecorationCount() > 0) {
                ryQuestion.removeItemDecorationAt(0);
            }
            ryQuestion.addItemDecoration(myItemDecoration);
            manager.setOrientation(RecyclerView.VERTICAL);
            ryQuestion.setLayoutManager(manager);
            HelpTwoAdapter helpTwoAdapter = new HelpTwoAdapter(helpOneEntity.children);
            helpTwoAdapter.bindToRecyclerView(ryQuestion);
            helpTwoAdapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                    HashMap<String, Object> mHashMap = new HashMap<>();
                    mHashMap.put("helpTwoEntity", helpOneEntity.children.get(position));
                    ActivitySkipUtil.startAnotherActivity((Activity) mContext, QuestionListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });
            if (position == currentPosition) {
                llHelpContent.setVisibility(View.VISIBLE);
                ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
            } else {
                llHelpContent.setVisibility(View.GONE);
                ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
            }
            llHelpTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (helpOneEntity.children.size() == 0) {
                        HashMap<String, Object> mHashMap = new HashMap<>();
                        mHashMap.put("helpTwoEntity",new HelpTwoEntity());
                        ActivitySkipUtil.startAnotherActivity((Activity) mContext, QuestionListActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        if (llHelpContent.getVisibility() == View.GONE) {
                            currentPosition = position;
                            llHelpContent.setVisibility(View.VISIBLE);
                            ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_down_arrow));
                        } else {
                            llHelpContent.setVisibility(View.GONE);
                            ivHelpArrow.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_right_arrow));
                            currentPosition = -1;
                        }
                        notifyDataSetChanged();
                    }
                }
            });
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
