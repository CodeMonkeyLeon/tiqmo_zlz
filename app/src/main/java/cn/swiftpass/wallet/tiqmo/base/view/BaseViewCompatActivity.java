package cn.swiftpass.wallet.tiqmo.base.view;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.ViewPumpAppCompatDelegate;
import androidx.core.app.ActivityCompat;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeChangeObserver;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeChangeObserverStack;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.widget.dialog.RootCheckDialog;
import dev.b3nedikt.restring.Restring;
import dev.b3nedikt.reword.Reword;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import me.jessyan.autosize.AutoSize;

public abstract class BaseViewCompatActivity extends AppCompatActivity implements ThemeChangeObserver {

    public Context mContext;

    private Unbinder mUnbinder;

    protected ConfigEntity configEntity;

    protected UserInfoEntity userInfoEntity;

    private AppCompatDelegate appCompatDelegate = null;

    @NonNull
    @Override
    public AppCompatDelegate getDelegate() {
        if (appCompatDelegate == null) {
            appCompatDelegate = new ViewPumpAppCompatDelegate(
                    super.getDelegate(),
                    this,
                    Restring::wrapContext
            );
        }
        return appCompatDelegate;
    }

    protected void changeLanguage(Locale locale) {
        Restring.setLocale(locale);

        // The layout containing the views you want to localize
        final View rootView = getWindow()
                .getDecorView()
                .findViewById(android.R.id.content);

        Reword.reword(rootView);
    }

    @LayoutRes
    protected abstract int getLayoutID();

    protected abstract void init(Bundle savedInstanceState);

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
    }

    protected boolean isFullScreen() {
        return false;
    }

    /**
     * 隐藏ActionBar和StatusBar
     */
    private void hideActionStatusBar() {
        //set no title bar 需要在setContentView之前调用
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //no status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //特殊情况下
        if (getSupportActionBar() != null) getSupportActionBar().hide();
        if (getActionBar() != null) getActionBar().hide();
    }

    /**
     * 隐藏 NavigationBar和StatusBar
     */
    protected void hideBottomStatusBar() {
        //隐藏虚拟按键，并且全屏
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }


    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        setupActivityBeforeCreate();
        super.onCreate(savedInstanceState);
        mContext = this;

        if (isFullScreen()) {
            hideActionStatusBar();
            hideBottomStatusBar();
        }

        int layoutID = getLayoutID();
        if (layoutID != 0) {
            setContentView(layoutID);
        }
        mUnbinder = ButterKnife.bind(this);
        onInit(savedInstanceState);

    }

    public ConfigEntity getConfigEntity() {
        configEntity = AppClient.getInstance().getUserManager().getConfig();
        return this.configEntity;
    }

    /**
     * register theme change observer
     * load current theme
     */
    private void setupActivityBeforeCreate() {
        ThemeChangeObserverStack.getInstance().registerObserver(this);
        ThemeUtils.loadingCurrentTheme(this, false);
        // set the status bar style according to the current theme.
        ThemeUtils.setTranslucentStatus(this);
    }

    protected void setDarkBar() {
        if (!ThemeUtils.isCurrentDark(mContext)) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        AutoSize.autoConvertDensityOfGlobal(this);
        return super.onCreateView(parent, name, context, attrs);
    }

    public UserInfoEntity getUserInfoEntity() {
        userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity == null) {
            userInfoEntity = new UserInfoEntity();
        }
        return userInfoEntity;
    }


    protected void onInit(Bundle savedInstanceState) {
        userInfoEntity = getUserInfoEntity();
        init(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(LocaleUtils.updateResources(newBase)));
        super.attachBaseContext(ViewPumpContextWrapper.wrap(LocaleUtils.updateResources(newBase)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else {
            return super.onOptionsItemSelected(item);
        }
        getRequestedOrientation();
        return true;
    }

    protected void showRootDialog() {
        RootCheckDialog rootCheckDialog = new RootCheckDialog(mContext);
        rootCheckDialog.setCloseListener(new RootCheckDialog.CloseListener() {
            @Override
            public void clickClose() {
                finish();
            }
        });
        rootCheckDialog.setCanceledOnTouchOutside(false);
        rootCheckDialog.setCancelable(false);
        rootCheckDialog.showWithBottomAnim();
    }

    /**
     * 检查是否有permission权限  返回true则表示有
     */
    public boolean isGranted(String permission) {
        if (mContext == null) {
            return false;
        }
        int checkSelfPermission = ActivityCompat.checkSelfPermission(mContext, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        ThemeChangeObserverStack.getInstance().unregisterObserver(this);
    }

    @Override
    public void notifyByThemeChanged() {
    }

}
