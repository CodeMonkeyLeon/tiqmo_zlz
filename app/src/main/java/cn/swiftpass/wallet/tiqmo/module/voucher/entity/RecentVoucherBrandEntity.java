package cn.swiftpass.wallet.tiqmo.module.voucher.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RecentVoucherBrandEntity extends BaseEntity {

    //代金券编码
    public String voucherCode;
    //代金券名称
    public String voucherName;
    //代金券金额 币种最小单位
    public String voucherAmount;
    public String voucherTypeIconUrl;
    //代金券 亮色版 Icon url
    public String voucherTypeLightIconUrl;
    //代金券 暗色版 Icon url
    public String voucherTypeDarkIconLogoUrl;
}
