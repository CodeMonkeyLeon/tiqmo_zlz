package cn.swiftpass.wallet.tiqmo.module.topup.adapter;

import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouTypeAndInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class TopUpAmountAdapter extends BaseRecyclerAdapter<VouTypeAndInfoEntity.VouInfo> {

    public TopUpAmountAdapter(@Nullable List<VouTypeAndInfoEntity.VouInfo> data) {
        super(R.layout.item_voucher_amount, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, VouTypeAndInfoEntity.VouInfo item, int position) {
        if (item == null) {
            return;
        }
        ImageView ivVoucherAmount = baseViewHolder.getView(R.id.iv_voucher_amount);

        GridLayoutManager.LayoutParams linearParams = (GridLayoutManager.LayoutParams) ivVoucherAmount.getLayoutParams();
        linearParams.height = ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 92,
                mContext.getResources().getDisplayMetrics()));
        ivVoucherAmount.setLayoutParams(linearParams);
        String logoUrl = ThemeUtils.isCurrentDark(mContext) ?item.voucherTypeDarkIconLogoUrl : item.voucherTypeLightIconUrl;

        if (!TextUtils.isEmpty(logoUrl)) {
            Glide.with(mContext).clear(ivVoucherAmount);
            Glide.with(mContext)
                    .load(logoUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .into(ivVoucherAmount);
        }
    }

    @Override
    public OnItemClickListener getOnItemClickListener() {
        return super.getOnItemClickListener();
    }
}
