package cn.swiftpass.wallet.tiqmo.support.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.io.Serializable;

public abstract class SpBaseUtil {
    protected Context mContext;
    private SharedPreferences sp;
    private Gson gson;

    protected abstract String getSpName();

    public SharedPreferences getSp() {
        if (sp == null) {
            sp = mContext.getSharedPreferences(getSpName(), Context.MODE_PRIVATE);
        }
        return sp;
    }

    public Gson getGson(){
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public SharedPreferences.Editor getEdit() {
        return getSp().edit();
    }

    public SpBaseUtil(Context context) {
        mContext = context;
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return getSp().getBoolean(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        getEdit().putBoolean(key, value).commit();
    }

    public String getString(String key, String defaultValue) {
        return getSp().getString(key, defaultValue);
    }

    public void putString(String key, String value) {
        getEdit().putString(key, value).commit();
    }

    public int getInt(String key, int defaultValue) {
        return getSp().getInt(key, defaultValue);
    }

    public void putInt(String key, int value) {
        getEdit().putInt(key, value).commit();
    }

    public void putFloat(String key, float value) {
        getEdit().putFloat(key, value).commit();
    }

    public float getFloat(String key, float defaultValue) {
        return getSp().getFloat(key, defaultValue);
    }

    public void putLong(String key, long value) {
        getEdit().putLong(key, value).commit();
    }

    public long getLong(String key, long defaultValue) {
        return getSp().getLong(key, defaultValue);
    }

    public void putSerializable(String key, Serializable value) {
        getEdit().putString(key, getGson().toJson(value)).commit();
    }

    public <T> T getSerializable(String key, Class<T> classOfT, String defaultValue) {
        return getGson().fromJson(getSp().getString(key, defaultValue), classOfT);
    }

}
