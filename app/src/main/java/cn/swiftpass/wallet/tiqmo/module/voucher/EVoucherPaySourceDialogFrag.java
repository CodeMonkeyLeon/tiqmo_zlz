package cn.swiftpass.wallet.tiqmo.module.voucher;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.setting.view.KycFailActivity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherOrderInfo;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;

public class EVoucherPaySourceDialogFrag extends BaseDialogFragmentN {

    private TextView tv_select_store_title;
    private LinearLayout ll_wallet_balance;
    private PaymentSourceListener paymentSourceListener;

    public void setPaymentSourceListener(PaymentSourceListener paymentSourceListener) {
        this.paymentSourceListener = paymentSourceListener;
    }

    public static EVoucherPaySourceDialogFrag newInstance(EVoucherOrderInfo eVoucherEntity) {
        EVoucherPaySourceDialogFrag fragment = new EVoucherPaySourceDialogFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("OrderInfo", eVoucherEntity);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_evoucher_payment_source;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        initView(view);
        initData();
    }

    private void initData() {
    }

    private void initView(View parentView) {
        tv_select_store_title = parentView.findViewById(R.id.tv_select_store_title);
        ll_wallet_balance = parentView.findViewById(R.id.ll_wallet_balance);
        ll_wallet_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkKyc();
            }
        });
    }

    private void checkKyc() {
        UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
        if (userInfoEntity.isKycChecking()) {
            ActivitySkipUtil.startAnotherActivity(getActivity(),
                    KycFailActivity.class, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        } else if (userInfoEntity.isKycLow()) {
            VoucherPayKycDiaFrg diaFrg = VoucherPayKycDiaFrg.newInstance();
            diaFrg.setUserInfoEntity(userInfoEntity);
            if (getFragmentManager() != null) {
                diaFrg.show(getFragmentManager(), "EVoucherKyc");
            }
        } else {
            if (paymentSourceListener != null) {
                paymentSourceListener.choosePayment();
            }
        }
    }

    public interface PaymentSourceListener {
        void choosePayment();
    }
}
