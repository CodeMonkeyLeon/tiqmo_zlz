package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import cn.swiftpass.wallet.tiqmo.module.home.contract.AnalysisContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SpendingPresenter implements AnalysisContract.SpendingPresenter {

    AnalysisContract.SpendingView mSpendingView;

    @Override
    public void getAnalysisDetail(String periodType, String spendingAnalysisQueryDate) {
        AppClient.getInstance().getTransferManager().getAnalysisDetail(periodType, spendingAnalysisQueryDate, new LifecycleMVPResultCallback<SpendingDetailEntity>(mSpendingView, true) {
            @Override
            protected void onSuccess(SpendingDetailEntity result) {
                mSpendingView.getAnalysisDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mSpendingView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(AnalysisContract.SpendingView spendingView) {
        this.mSpendingView = spendingView;
    }

    @Override
    public void detachView() {
        this.mSpendingView = null;
    }
}
