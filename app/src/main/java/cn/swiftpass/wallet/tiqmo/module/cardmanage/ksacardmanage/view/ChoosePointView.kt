package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils

class ChoosePointView : View {


    private lateinit var mContext: Context

    private var mSize = 0f

    private var mExternalR = 0f

    private var mInternalR = 0f

    private var mX = 0f

    private var mY = 0f

    private var mIsSelected = false


    private val mDefaultPaint by lazy {
        Paint().apply {
            color = mContext.getColor(R.color.color_default_card_type)
            isAntiAlias = true
            style = Paint.Style.STROKE
        }
    }


    private val mSelectedPaint by lazy {
        Paint().apply {
            val paintColor = mContext.getColor(
                ThemeSourceUtils.getSourceID(
                    mContext,
                    R.attr.bg_btn_next_page_normal
                )
            )

            color = paintColor
            isAntiAlias = true
            style = Paint.Style.FILL
        }
    }


    constructor(context: Context) : super(context) {
        mContext = context
    }


    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        mContext = context
    }


    constructor(context: Context, attributeSet: AttributeSet, def: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = measuredWidth
        mSize = width.toFloat()
        mExternalR = mSize / 2 - 4
        mInternalR = mExternalR * 3 / 5
        mDefaultPaint.strokeWidth = mSize / 20 - 2

        mX = mSize / 2
        mY = mSize / 2
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.run {
            if (mIsSelected) {
                mSelectedPaint.style = Paint.Style.STROKE
                drawCircle(mX, mY, mExternalR, mSelectedPaint)
                mSelectedPaint.style = Paint.Style.FILL
                drawCircle(mX, mY, mInternalR, mSelectedPaint)
            } else {
                drawCircle(mX, mY, mExternalR, mDefaultPaint)
            }
        }
    }


    fun setSelect(isSelect: Boolean) {
        mIsSelected = isSelect
        invalidate()
    }


}