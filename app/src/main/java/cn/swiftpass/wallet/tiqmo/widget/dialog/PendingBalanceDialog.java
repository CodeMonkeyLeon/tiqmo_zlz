package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BalanceEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;

public class PendingBalanceDialog extends BottomDialog {
    private Context mContext;

    private ConfirmListener confirmListener;

    private BalanceEntity balanceEntity;

    private String totalHoldAmount;

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public PendingBalanceDialog(Context context, BalanceEntity balanceEntity, String totalHoldAmount) {
        super(context);
        this.mContext = context;
        this.balanceEntity = balanceEntity;
        this.totalHoldAmount = totalHoldAmount;
        initViews(mContext,balanceEntity);
    }

    public interface ConfirmListener {
        void clickConfrim();
    }

    private void initViews(Context mContext,BalanceEntity balanceEntity) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_pending_balance, null);
        TextView tvOneContent = view.findViewById(R.id.tv_one_content);
        TextView tvTwoContent = view.findViewById(R.id.tv_two_content);
        TextView tvConfirm = view.findViewById(R.id.tv_confirm);
        TextView tvHoldBalance = view.findViewById(R.id.tv_hold_balance);
        LinearLayout llHoldBalance = view.findViewById(R.id.ll_hold_balance);
        LinearLayout llPendingBalance = view.findViewById(R.id.ll_pending_balance);
        try {

            if (balanceEntity != null && balanceEntity.balanceNotNull()) {
                llPendingBalance.setVisibility(View.VISIBLE);
                String totalMonthLimitAmt = balanceEntity.totalMonthLimitAmt;
                tvOneContent.setText(mContext.getString(R.string.sprint11_45).replace("20,000", totalMonthLimitAmt));
                tvTwoContent.setText(mContext.getString(R.string.sprint11_46) + " " + AndroidUtils.getTransferMoney(balanceEntity.balance) + " " + LocaleUtils.getCurrencyCode("") + " " + mContext.getString(R.string.sprint11_46_1));
            } else {
                llPendingBalance.setVisibility(View.GONE);
            }

            if (BigDecimalFormatUtils.compareBig(totalHoldAmount, "0")) {
                llHoldBalance.setVisibility(View.VISIBLE);
                tvHoldBalance.setText(mContext.getString(R.string.sprint20_102) + " " + AndroidUtils.getTransferMoney(totalHoldAmount) + " " + LocaleUtils.getCurrencyCode(""));
            } else {
                llHoldBalance.setVisibility(View.GONE);
            }
        }catch (Throwable e){
            e.printStackTrace();
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(confirmListener != null){
                    confirmListener.clickConfrim();
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
