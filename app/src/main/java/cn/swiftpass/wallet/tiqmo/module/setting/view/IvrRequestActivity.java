package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaPayResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.ResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetCardPwdOneActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.GetNewCardSuccessActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.KsaCardResultActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.IvrCallContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.IvrCallPresenter;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class IvrRequestActivity extends BaseCompatActivity<IvrCallContract.IvrCallPresenter> implements IvrCallContract.IvrCallView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_ivr_request)
    TextView tvIvrRequest;
    @BindView(R.id.tv_ivr_message)
    TextView tvIvrMessage;
    @BindView(R.id.tv_ivr_call)
    TextView tvIvrCall;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_ivr_status)
    TextView tvIvrStatus;
    @BindView(R.id.tv_ivr_step)
    TextView tvIvrStep;
    @BindView(R.id.ll_ivr_request)
    LinearLayout llIvrRequest;

    /**
     * 强制停止Ivr结果查询
     */
    private boolean isForceStopQuery;
    /**
     * IVR发起后 倒计时查询结果
     */
    private Timer queryVideoResultTimer;
    private int retainQueryTime = 60;
    private int retainQueryTimeDelay = 15;

    private Handler queryHandler;

    private String outActionId;
    private String ivrAmount = "";
    private int ivrRetryTime;
    private String categoryId;

    private String ivrResult;

    //交易相关
    private TransferEntity transferEntity;
    private String sceneType, orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode, transFees, vat;
    private KycContactEntity kycContactEntity;
    private String payerName;
    private CheckOutEntity checkOutEntity;
    private String paymentMethodNo;
    private boolean isFromSDK;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    boolean isFromEditBeneficiary = false;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;

    private String receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
            nickName, relationshipCode, imrCallingCode, phone,
            transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
            birthDate, sex, cityName, districtName,
            poBox, buildingNo, street, idNo, idExpiry,
            bankAccountType, ibanNo, bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
            cityId, beneCurrency, channelPayeeId, channelCode,branchId;

    /**
     * 付费开卡
     */
    private KsaCardSummaryEntity ksaCardSummaryEntity;
    /**
     * 免费开卡
     */
    private OpenCardReqEntity openCardReqEntity;
    private KsaCardEntity ksaCardEntity;
    private KsaLimitChannelEntity.CardLimitConfigEntity mCardLimitConfigEntity;

    private String proxyCardNo, blockCardReason,blockCardReasonStatus;
    //第一次流程OTP验证title
    private String firstOtpTitle;
    private RiskControlEntity riskControlEntity;
    private BottomDialog bottomDialog;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ivr_request_call;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.sprint11_103);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);

        if(getIntent() != null && getIntent().getExtras() != null){
            riskControlEntity = (RiskControlEntity) getIntent().getExtras().getSerializable(Constants.OTP_riskControlEntity);
            if(riskControlEntity != null){
                outActionId = riskControlEntity.outActionId;
                ivrRetryTime = riskControlEntity.ivrRetryTime;
            }
            firstOtpTitle = getIntent().getExtras().getString(Constants.DATA_FIRST_TITLE);
            mCardLimitConfigEntity = (KsaLimitChannelEntity.CardLimitConfigEntity) getIntent().getExtras().getSerializable(Constants.ksaCardLimitConfigEntity);
            ksaCardSummaryEntity = (KsaCardSummaryEntity) getIntent().getExtras().getSerializable(Constants.ksaCardSummaryEntity);
            openCardReqEntity = (OpenCardReqEntity) getIntent().getExtras().getSerializable(Constants.openCardReqEntity);
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            proxyCardNo = getIntent().getExtras().getString(Constants.proxyCardNo);
            blockCardReason = getIntent().getExtras().getString(Constants.blockCardReason);
            blockCardReasonStatus = getIntent().getExtras().getString(Constants.blockCardReasonStatus);
            userInfoEntity = (UserInfoEntity) getIntent().getExtras().getSerializable(Constants.DATA_USER);
            if (userInfoEntity == null) {
                userInfoEntity = getUserInfoEntity();
            }
            transferEntity = (TransferEntity) getIntent().getExtras().getSerializable(Constants.TRANSFER_ENTITY);
            kycContactEntity = (KycContactEntity) getIntent().getExtras().getSerializable(Constants.CONTACT_ENTITY);
            checkOutEntity = (CheckOutEntity) getIntent().getExtras().getSerializable(Constants.CHECK_OUT_ENTITY);
            sceneType = getIntent().getExtras().getString(Constants.sceneType);
            isFromSDK = getIntent().getExtras().getBoolean(Constants.IS_FROM_SDK);
            if (kycContactEntity != null) {
                payerName = kycContactEntity.getContactsName();
            }
            if (transferEntity != null) {
                orderNo = transferEntity.orderNo;
                payerName = transferEntity.payerName;
                payMethod = transferEntity.payMethod;
                transAmount = transferEntity.orderAmount;
                ivrAmount = transAmount;
                exchangeRate = transferEntity.exchangeRate;
                transCurrencyCode = transferEntity.orderCurrencyCode;
                transFees = transferEntity.transFees;
                vat = transferEntity.vat;
            } else if (checkOutEntity != null) {
                orderNo = checkOutEntity.orderNo;
                ivrAmount = checkOutEntity.payerAmount;
                if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
                    paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
                }
            }
            isFromEditBeneficiary = getIntent().getExtras().getBoolean(Constants.IS_FROM_EDIT_BENEFICIARY, false);
            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            if (imrBeneficiaryEntity != null) {
                receiptMethod = imrBeneficiaryEntity.receiptMethod;
                payeeFullName = imrBeneficiaryEntity.payeeFullName;
                nickName = imrBeneficiaryEntity.nickName;
                relationshipCode = imrBeneficiaryEntity.relationshipCode;
                imrCallingCode = imrBeneficiaryEntity.callingCode;
                phone = imrBeneficiaryEntity.phone;
                transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;
                payeeInfoCountryCode = imrBeneficiaryEntity.payeeInfoCountryCode;
                birthPlace = imrBeneficiaryEntity.birthPlace;
                birthDate = imrBeneficiaryEntity.birthDate;
                sex = imrBeneficiaryEntity.sex;
                cityName = imrBeneficiaryEntity.cityName;
                districtName = imrBeneficiaryEntity.districtName;
                poBox = imrBeneficiaryEntity.poBox;
                buildingNo = imrBeneficiaryEntity.buildingNo;
                street = imrBeneficiaryEntity.street;
                idNo = imrBeneficiaryEntity.idNo;
                idExpiry = imrBeneficiaryEntity.idExpiry;
                bankAccountType = imrBeneficiaryEntity.bankAccountType;
                ibanNo = imrBeneficiaryEntity.ibanNo;
                bankAccountNo = imrBeneficiaryEntity.bankAccountNo;
                receiptOrgCode = imrBeneficiaryEntity.receiptOrgCode;
                receiptOrgBranchCode = imrBeneficiaryEntity.receiptOrgBranchCode;
                saveFlag = imrBeneficiaryEntity.saveFlag;
                beneCurrency = imrBeneficiaryEntity.beneCurrency;
                cityId = imrBeneficiaryEntity.cityId;
                receiptOrgName = imrBeneficiaryEntity.receiptOrgName;
                receiptOrgBranchName = imrBeneficiaryEntity.receiptOrgBranchName;
                channelPayeeId = imrBeneficiaryEntity.channelPayeeId;
                channelCode = imrBeneficiaryEntity.channelCode;
                branchId = imrBeneficiaryEntity.branchId;
            }
        }

        mPresenter.sendIvrCall(outActionId,ivrAmount);
        queryHandler = new Handler();
    }

    public void setResetTime(int time) {
            tvIvrCall.setClickable(false);
            tvIvrCall.setEnabled(false);
            AdvancedCountdownTimer.getInstance().countDownEvent(time, new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (isFinishing() || tvIvrCall == null) {
                        return;
                    }
                    tvIvrMessage.setText(Spans.builder().text(mContext.getString(R.string.sprint11_83))
                            .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                            .size(12)
                            .text(" "+millisUntilFinished+" "+mContext.getString(R.string.sprint11_83_1))
                                    .color(mContext.getColor(R.color.color_fc4f00))
                                    .size(12).build());

                    tvIvrCall.setClickable(false);
                    tvIvrCall.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    if (isFinishing() || tvIvrCall == null) {
                        return;
                    }
                    tvIvrMessage.setText(getString(R.string.sprint11_84));
                    tvIvrCall.setClickable(true);
                    tvIvrCall.setEnabled(true);
                }
            });
    }

    @OnClick({R.id.iv_back, R.id.tv_ivr_call})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                //如果是拒绝状态下  点击返回键直接回到主页面
                if("2".equals(ivrResult)){
                    ProjectApp.removeAllTaskExcludeMainStack();
                }
                finish();
                break;
            case R.id.tv_ivr_call:
                mPresenter.sendIvrCall(outActionId,ivrAmount);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 轮训查询Ivr结果
     */
    private Runnable queryVideoResultRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isForceStopQuery) {
                mPresenter.getIvrResult(outActionId);
            }
        }
    };

    @Override
    public void sendIvrCallSuccess(ResultEntity resultEntity) {
        setResetTime(ivrRetryTime);
        if(resultEntity != null){
            String result = resultEntity.result;
            if("1".equals(result)){
                isForceStopQuery = false;
                //开始倒计时查询结果
                queryVideoResultTimer = new Timer();
                TimerTask queryVideoResultTimerTask = new TimerTask() {
                    @Override
                    public void run() {
                        queryHandler.post(queryVideoResultRunnable);
                    }
                };
                queryVideoResultTimer.schedule(queryVideoResultTimerTask, 0, 3000);
            }
        }
    }

    @Override
    public void getIvrResultSuccess(ResultEntity resultEntity) {
        if(resultEntity != null){
            ivrResult = resultEntity.result;
            //1:批准 2:拒绝，空则代表还未选择
            if("1".equals(ivrResult)){
                removeIvrCall();
                checkTransfer();
            }else if("2".equals(ivrResult)){
                AndroidUtils.setDrawableStart(mContext,tvIvrStatus,R.drawable.shape_oval_fd1d2d);
                removeIvrCall();
                llIvrRequest.setVisibility(View.GONE);
                tvIvrRequest.setVisibility(View.GONE);
                tvIvrStatus.setText(getString(R.string.sprint11_87));
                tvIvrStep.setText(getString(R.string.sprint11_86));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeIvrCall();
    }

    @Override
    public void getIvrResultFail(String errorCode, String errorMsg) {
        removeIvrCall();
        showTipDialog(errorMsg);
    }

    /**
     * 交易验证
     */
    private void checkTransfer() {
        if (Constants.TYPE_PAY_CARD.equals(sceneType)) {
            mPresenter.getKsaCardPayResult(ksaCardSummaryEntity.vat, ksaCardSummaryEntity.totalAmount, ksaCardSummaryEntity.openCardReqEntity);
        }else if (Constants.TYPE_CHARITY.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        }else if (Constants.TYPE_HyperPay_ADD_MONEY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_HyperPay_ADD_MONEY));
            finish();
        }else if (Constants.TYPE_CHANGE_LOGIN_PD.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_CHANGE_LOGIN_PD));
            finish();
        }else if (Constants.TYPE_WITHDRAW_ADD_BENEFICIARY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_WITHDRAW_ADD_BENEFICIARY));
            finish();
        }else if (Constants.TYPE_SEND_BENEFICIARY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_SEND_BENEFICIARY));
            finish();
        }else if (Constants.TYPE_ADD_SEND_BENE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ADD_SEND_BENEFICIARY));
            finish();
        } else if (Constants.TYPE_KSA_CARD_SET_LIMIT.equals(sceneType)) {
            String txnType = mCardLimitConfigEntity.txnType;
            String limitMoney = mCardLimitConfigEntity.isDaily ? mCardLimitConfigEntity.dailyLimitValue : mCardLimitConfigEntity.monthlyLimitValue;
            mPresenter.setKsaCardLimit(txnType,  mCardLimitConfigEntity.isDaily ? AndroidUtils.getReqTransferMoney(limitMoney) : "",
                    mCardLimitConfigEntity.isDaily ? "" : AndroidUtils.getReqTransferMoney(limitMoney), mCardLimitConfigEntity.proxyCardNo, mCardLimitConfigEntity.up);
        } else if (Constants.TYPE_KSA_CARD_FREEZE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_FREEZE));
            finish();
        } else if (Constants.TYPE_KSA_CARD_ACTIVATE.equals(sceneType)) {
            mPresenter.activateCard(ksaCardEntity.proxyCardNo, ksaCardEntity.cardNumber, ksaCardEntity.expiryTime);
        } else if (Constants.TYPE_KSA_CARD_DETAIL.equals(sceneType)) {
            mPresenter.getKsaCardDetails(ksaCardEntity.proxyCardNo);
        } else if (Constants.TYPE_KSA_CARD_SET_CHANNEL.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_SET_CHANNEL));
            finish();
        } else if (Constants.TYPE_KSA_CARD_UNFREEZE.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_FREEZE));
            finish();
        } else if (Constants.TYPE_KSA_CARD_SET_PIN.equals(sceneType)) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            mHashMap.put(Constants.proxyCardNo, proxyCardNo);
            ActivitySkipUtil.startAnotherActivity(IvrRequestActivity.this, GetCardPwdOneActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (Constants.TYPE_KSA_CARD_BLOCK.equals(sceneType)) {
            mPresenter.setKsaCardStatus(Constants.TYPE_CARD_BLOCK, proxyCardNo, blockCardReason,blockCardReasonStatus);
        } else if (Constants.TYPE_PAY_CARD_NO_FEE.equals(sceneType)) {
            mPresenter.getKsaCardPayResult("", "", openCardReqEntity);
        } else if (Constants.TYPE_IMR_PAY.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_INTER_TOP_UP.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_PAY_BILL.equals(sceneType)) {
            mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
        } else if (Constants.TYPE_SPLIT.equals(sceneType)) {
            if (transferEntity != null) {
                mPresenter.splitBill(transferEntity.orderNo, AndroidUtils.getReqTransferMoney(transferEntity.orderAmount),
                        transferEntity.orderCurrencyCode, transferEntity.remark,
                        transferEntity.receiverInfoList);
            }
        }else if (Constants.TYPE_IMR_ADD_BENEFICIARY.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_ADD_BENEFICIARY));
            finish();
        } else if (Constants.TYPE_TRANSFER_RT.equals(sceneType)) {
            if (kycContactEntity != null) {
                mPresenter.transferToContact(kycContactEntity.getUserId(), SpUtils.getInstance().getCallingCode(), kycContactEntity.getRequestPhoneNumber(), userInfoEntity.userId,
                        userInfoEntity.getPhone(), AndroidUtils.getReqTransferMoney(kycContactEntity.payMoney), UserInfoManager.getInstance().getCurrencyCode(),
                        kycContactEntity.remark, sceneType, "", "1", "", "", "", payerName);
            }
        } else {
            if (Constants.TYPE_TRANSFER_RC.equals(sceneType)) {
                mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            }else if (Constants.TYPE_RECEIVE_SPLIT_BILL.equals(sceneType)){
                mPresenter.transferSurePay(Constants.TYPE_TRANSFER_RT, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            } else if (Constants.TYPE_SCAN_MERCHANT.equals(sceneType)) {
                mPresenter.confirmPay(orderNo, paymentMethodNo, "P");
            } else {
                mPresenter.transferSurePay(sceneType, orderNo, payMethod, transAmount,
                        exchangeRate, transCurrencyCode, transFees, vat);
            }
        }
    }

    private void removeIvrCall(){
        if (queryVideoResultTimer != null) {
            queryVideoResultTimer.cancel();
        }
        if (queryHandler != null && queryVideoResultRunnable != null) {
            queryHandler.removeCallbacks(queryVideoResultRunnable);
        }
        isForceStopQuery = true;
    }

    @Override
    public void operateIvrCallSuccess(Void result) {

    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        if ("030194".equals(errorCode)) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.cardResultType, KsaCardResultActivity.cardResultType_Open_Card_fail);
            mHashMap.put(Constants.cardResultErrorMsg, errorMsg);
            ActivitySkipUtil.startAnotherActivity(this, KsaCardResultActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public IvrCallContract.IvrCallPresenter getPresenter() {
        return new IvrCallPresenter();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,sceneType,transferEntity,true);
    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        transferToContactSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void transferToContactFail(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        }else {
            showTipDialog(errMsg);
        }
    }

    @Override
    public void confirmPaySuccess(TransferEntity transferEntity) {
        confirmPaySuccess(this,sceneType,checkOutEntity,isFromSDK,transferEntity);
    }

    @Override
    public void confirmPayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {
        splitBillSuccess(this,sceneType,transferEntity);
    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity) {
        if(imrAddBeneResultEntity != null){
            imrBeneficiaryEntity.payeeInfoId = imrAddBeneResultEntity.payeeInfoId;
        }
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_ADD_BENE_SUCCESS, imrBeneficiaryEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrAddBeneficiaryFail(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void imrEditBeneficiarySuccess(ResponseEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_EDIT_BENE_SUCCESS, beneficiaryInfo,riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrEditBeneficiaryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getKsaCardPayResultSuccess(KsaPayResultEntity ksaPayResultEntity) {
        if (ksaPayResultEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put(Constants.ksa_cardType, ksaPayResultEntity.cardType);
            if (ksaCardSummaryEntity != null) {
                mHashMap.put(Constants.ksa_deliveryTime, ksaCardSummaryEntity.deliveryTime);
            }
            ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void getKsaCardPayResultFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void setKsaCardStatusSuccess(Void result) {
        if (Constants.TYPE_KSA_CARD_BLOCK.equals(sceneType)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_CARD_BLOCK));
        }
        ProjectApp.removeAllTaskExcludeMainStack();
        finish();
    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_SET_LIMIT_SUCCESS));
        finish();
    }

    @Override
    public void getKsaCardDetailsSuccess(CardDetailEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_KSA_SHOW_DETAIL, result));
        finish();
    }

    @Override
    public void activateCardSuccess(Void result) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.ksaCardEntity, ksaCardEntity);
        mHashMap.put(Constants.cardSuccessType, 3);
        ActivitySkipUtil.startAnotherActivity(this, GetNewCardSuccessActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }
}
