package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class UserChat extends BaseEntity {

    private String id;
    private String name;

    public UserChat() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return "UserSecureChat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
