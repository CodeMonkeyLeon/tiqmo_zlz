package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by 叶智星 on 2018年09月17日.
 * 每一个不曾起舞的日子，都是对生命的辜负。
 */
public class JsonRequest {
    private String partnerNo;
    private String sessionId;
    private String userId;
    private String deviceId;
    private String deviceModel;
    private String osType;
    private String version;
    private String lang;
    private String themeCode;
    private String deviceToken;
    private Object serviceBody;

    public String getDeviceModel() {
        return this.deviceModel;
    }

    public void setDeviceModel(final String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getPartnerNo() {
        return partnerNo;
    }

    public void setPartnerNo(String partnerNo) {
        this.partnerNo = partnerNo;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getThemeCode() {
        return this.themeCode;
    }

    public void setThemeCode(final String themeCode) {
        this.themeCode = themeCode;
    }

    public Object getServiceBody() {
        return serviceBody;
    }

    public void setServiceBody(Object serviceBody) {
        this.serviceBody = serviceBody;
    }
}
