package cn.swiftpass.wallet.tiqmo.sdk.net;

/**
 * Created by YZX on 2019年01月04日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class ResponseException extends Throwable {
    private String mErrorCode;
    private String mErrorMessage;

    public ResponseException(String errorCode, String errorMessage) {
        super(errorMessage);
        mErrorCode = errorCode;
        mErrorMessage = errorMessage;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
}
