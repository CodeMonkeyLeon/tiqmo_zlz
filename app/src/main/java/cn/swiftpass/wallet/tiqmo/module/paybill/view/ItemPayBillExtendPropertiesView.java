package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import cn.swiftpass.wallet.tiqmo.R;

public class ItemPayBillExtendPropertiesView extends LinearLayout {


    private TextView mTvKey;
    private TextView mTvValue;


    public ItemPayBillExtendPropertiesView(Context context) {
        super(context);
        init(context);
    }

    public ItemPayBillExtendPropertiesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ItemPayBillExtendPropertiesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context) {
        //加载布局
        View rootView = LayoutInflater.from(context).inflate(R.layout.item_pay_bill_extend_properties, this);
        //绑定控件
        mTvKey = rootView.findViewById(R.id.id_tv_key);
        mTvValue = rootView.findViewById(R.id.id_tv_value);
    }

    public void setKeyValue(String key, String value) {
        if (mTvKey != null) {
            mTvKey.setText(key);
        }

        if (mTvValue != null) {
            mTvValue.setText(value);
        }
    }

}
