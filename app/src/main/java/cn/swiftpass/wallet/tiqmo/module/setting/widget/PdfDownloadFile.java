package cn.swiftpass.wallet.tiqmo.module.setting.widget;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 下载Pdf展示
 */
public class PdfDownloadFile implements DownloadFile {

    private static final int KILOBYTE = 1024;

    private static final int BUFFER_LEN = 1 * KILOBYTE;
    private static final int NOTIFY_PERIOD = 150 * KILOBYTE;

    Context context;
    Handler uiThread;
    Listener listener = new NullListener();
    private OkHttpClient okHttpClient;

    public PdfDownloadFile(Context context, Handler uiThread, Listener listener) {
        this.context = context;
        this.uiThread = uiThread;
        this.listener = listener;

        okHttpClient = getBuilder(true)
                .connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .build();
    }

    /**
     * 添加自定义拦截器  拦截器处理request顺序按依赖关系排序
     *
     * @return
     */
    public OkHttpClient.Builder getBuilder(boolean isAddInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        try {
            if (getInputStream() == null) {
                if (BuildConfig.DOWNLOAD_CER_SERVER) {
//                    builder = setCertificates(builder, ProjectApp.getContext().getAssets().open("tiqmo_sit.cer"));
                    builder = ApiHelper.setCertificates(builder, ProjectApp.getContext().getAssets().open("tiqmo_k8s.cer"));
                }
            } else {
                builder = ApiHelper.setCertificates(builder, getInputStream());
            }
        } catch (Exception e) {
        }
        return builder;
    }

    public InputStream getInputStream() {
        String filePath = ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File file = new File(filePath);
        File[] filenameList = file.listFiles();
        InputStream inputStream = null;
        for (int i = 0; i < filenameList.length; i++) {
            String filename = filenameList[i].getName();
            if (!filename.endsWith(".crt") && !filename.endsWith(".cer")) {
                continue;
            }
            try {
                inputStream = new FileInputStream(filenameList[i]);
            } catch (FileNotFoundException e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        return inputStream;
    }

    @Override
    public void download(final String url, final String destinationPath) {

        Request request = new Request.Builder().url(url).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 下载失败监听回调
                notifyFailureOnUiThread(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                // 储存下载文件的目录
                File file = new File(destinationPath);
                try {
                    is = response.body().byteStream();
                    int total = (int) response.body().contentLength();
                    fos = new FileOutputStream(file);
                    int sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;
                        int progress = (int) (sum * 1.0f / total * 100);
                        // 下载中更新进度条
                        notifyProgressOnUiThread(sum, total);
                    }
                    fos.flush();
                    // 下载完成
                    notifySuccessOnUiThread(url, destinationPath);
                } catch (Exception e) {
                    notifyFailureOnUiThread(e);
                } finally {
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                        LogUtils.d("errorMsg", "---"+e+"---");
                    }
                }
            }
        });
    }

    protected void notifySuccessOnUiThread(final String url, final String destinationPath) {
        if (uiThread == null) {
            return;
        }

        uiThread.post(new Runnable() {
            @Override
            public void run() {
                listener.onSuccess(url, destinationPath);
            }
        });
    }

    protected void notifyFailureOnUiThread(final Exception e) {
        if (uiThread == null) {
            return;
        }

        uiThread.post(new Runnable() {
            @Override
            public void run() {
                listener.onFailure(e);
            }
        });
    }

    private void notifyProgressOnUiThread(final int downloadedSize, final int totalSize) {
        if (uiThread == null) {
            return;
        }

        uiThread.post(new Runnable() {
            @Override
            public void run() {
                listener.onProgressUpdate(downloadedSize, totalSize);
            }
        });
    }

    protected class NullListener implements Listener {
        public void onSuccess(String url, String destinationPath) {
            /* Empty */
        }

        public void onFailure(Exception e) {
            /* Empty */
        }

        public void onProgressUpdate(int progress, int total) {
            /* Empty */
        }
    }
}
