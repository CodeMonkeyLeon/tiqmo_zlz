package cn.swiftpass.wallet.tiqmo.module.chat.adapter;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.chat.entity.MessageEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.TimeUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseItemDataBinder;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class LeftFileImageBinder extends BaseItemDataBinder<MessageEntity> {
    private String searchTerm = "";
    @Override
    public int getViewType() {
        return MessageAdapter.LeftFileImage;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_chat_left_file;
    }

    @Override
    public void bindData(BaseViewHolder baseViewHolder, MessageEntity messageEntity, int position) {
        baseViewHolder.setText(R.id.tv_left_time, TimeUtils.getChatTime(messageEntity.getTimestamp()));
        baseViewHolder.setText(R.id.tv_left_name, messageEntity.getUser().getName());
        baseViewHolder.setText(R.id.tv_left_message, messageEntity.getSimpleMessageToDisplay(searchTerm));
        if(messageEntity.isGroupChat()){
            baseViewHolder.setGone(R.id.tv_left_name, false);
        }else{
            baseViewHolder.setGone(R.id.tv_left_name, true);
        }
    }
}
