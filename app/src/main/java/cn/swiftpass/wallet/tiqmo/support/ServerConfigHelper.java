package cn.swiftpass.wallet.tiqmo.support;

import android.content.Context;
import android.content.SharedPreferences;

import cn.swiftpass.wallet.tiqmo.BuildConfig;

public class ServerConfigHelper {

    static final String KEY_URL = "url";
    static final String KEY_PART_NO = "part_no";
    static final String KEY_USER_AGENT = "user_agent";
    static final String KEY_PUBLIC_SERVER = "public_server";
    static final String KEY_IS_ENCODE = "is_encode";

    static SharedPreferences mSharePreference;

    public static Model getConfig(Context context) {
        if (mSharePreference == null) {
            mSharePreference = context.getSharedPreferences("server_config", Context.MODE_PRIVATE);
        }
        Model model = new Model();
        model.mUrl = mSharePreference.getString(KEY_URL, BuildConfig.ServerUrl);
        model.mPartNo = mSharePreference.getString(KEY_PART_NO, BuildConfig.partnerNo);
        model.mUserAgent = mSharePreference.getString(KEY_USER_AGENT, BuildConfig.UserAgent);
        model.mPublicKey = mSharePreference.getString(KEY_PUBLIC_SERVER, BuildConfig.serverPublicKey);
        model.mIsEncodeData = mSharePreference.getBoolean(KEY_IS_ENCODE, BuildConfig.isEnableEncryptMode);
        return model;
    }

    public static String getUerAgent(){
        return mSharePreference.getString(KEY_USER_AGENT, BuildConfig.UserAgent);
    }

    public static void saveConfig(Model model) {
        mSharePreference.edit().putString(KEY_URL, model.mUrl)
                .putString(KEY_PART_NO, model.mPartNo)
                .putString(KEY_USER_AGENT, model.mUserAgent)
                .putString(KEY_PUBLIC_SERVER,model.mPublicKey)
                .putBoolean(KEY_IS_ENCODE, model.mIsEncodeData)
                .commit();
    }


    public static class Model {
        String mUrl;
        String mPartNo;
        String mPublicKey;
        String mUserAgent;
        boolean mIsEncodeData;

        public String getUrl() {
            return mUrl;
        }

        public void setUrl(String url) {
            mUrl = url;
        }

        public String getPartNo() {
            return mPartNo;
        }

        public void setPartNo(String partNo) {
            mPartNo = partNo;
        }

        public String getPublicKey() {
            return mPublicKey;
        }

        public void setPublicKey(String publicKey) {
            mPublicKey = publicKey;
        }

        public boolean isEncodeData() {
            return mIsEncodeData;
        }

        public void setEncodeData(boolean encodeData) {
            mIsEncodeData = encodeData;
        }

        public String getUserAgent() {
            return mUserAgent;
        }

        public void setUserAgent(String userAgent) {
            mUserAgent = userAgent;
        }
    }
}
