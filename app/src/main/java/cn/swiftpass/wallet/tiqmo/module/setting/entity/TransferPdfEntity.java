package cn.swiftpass.wallet.tiqmo.module.setting.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransferPdfEntity extends BaseEntity {
    //生成的发票pdf存于服务器地址
    public String url;
    //第三方返回已受理发票开具凭据id
    public String requestId;
    /**
     * 1是 invoice发票  2是receipt收据
     */
    public int pdfType = 1;
}
