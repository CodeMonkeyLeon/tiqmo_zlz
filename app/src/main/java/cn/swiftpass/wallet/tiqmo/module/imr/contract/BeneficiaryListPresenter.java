package cn.swiftpass.wallet.tiqmo.module.imr.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;

public class BeneficiaryListPresenter implements BeneficiaryListContract.Presenter {

    private BeneficiaryListContract.View mBaseView;

    @Override
    public void getBeneficiaryList(boolean isInit) {
        AppClient.getInstance().getImrBeneficiaryList(new LifecycleMVPResultCallback<ImrBeneficiaryListEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(ImrBeneficiaryListEntity result) {
                if (result == null || result.imrPayeeInfos == null
                        || result.imrPayeeInfos.size() <= 0) {
                    if (isInit) {
                        mBaseView.showSendMoneyFragment();
                    }
                    mBaseView.showNoBeneficiary();
                    return;
                }
                String channelCode = result.channelCode;
                SpUtils.getInstance().setChannelCode(channelCode);
                for (int i = 0; i <result.imrPayeeInfos.size(); i++) {
                    result.imrPayeeInfos.get(i).isShowSubView = i == 0;
                    for (int j = 0; j < result.imrPayeeInfos.get(i).imrPayeeDetailInfos.size(); j++) {
                        result.imrPayeeInfos.get(i).imrPayeeDetailInfos.get(j).setShowRemoveIcon(false);
                        result.imrPayeeInfos.get(i).imrPayeeDetailInfos.get(j).isShowBeneficiaryOP = false;
                    }
                }
                mBaseView.showBeneficiaryList(result.imrPayeeInfos);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) return;
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });

    }

    @Override
    public void removeBeneficiary(String payeeInfoId) {
        AppClient.getInstance().removeImrBeneficiary(payeeInfoId, new LifecycleMVPResultCallback<ResponseEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(ResponseEntity result) {
                mBaseView.showRemoveBeneficiarySuccess();
                getBeneficiaryList(false);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) return;
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getImrBeneficiaryDetail(ImrBeneficiaryListEntity.ImrBeneficiaryBean beneficiaryBean) {
        AppClient.getInstance().getImrBeneficiaryDetail(beneficiaryBean.payeeInfoId, new LifecycleMVPResultCallback<ImrBeneficiaryDetails>(mBaseView, true) {
            @Override
            protected void onSuccess(ImrBeneficiaryDetails result) {
                result.imrPayeeDetailInfo.countrySupportedCurrencyName = beneficiaryBean.countrySupportedCurrencyName;
                mBaseView.startEditView(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mBaseView == null) return;
                mBaseView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrExchangeRate(String payeeInfoId,String sourceAmount,
                                String destinationAmount,String channelCode) {
        AppClient.getInstance().imrExchangeRate(payeeInfoId,sourceAmount,destinationAmount,channelCode, new LifecycleMVPResultCallback<ImrExchangeRateEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(ImrExchangeRateEntity result) {
                mBaseView.imrExchangeRateSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.imrExchangeRateFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void activateBeneficiary(String payeeInfoId) {
        AppClient.getInstance().activateIvrBeneficary(payeeInfoId, new LifecycleMVPResultCallback<Void>(mBaseView,true) {
            @Override
            protected void onSuccess(Void result) {
                mBaseView.activateBeneficiarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mBaseView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                mBaseView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(BeneficiaryListContract.View view) {
        mBaseView = view;
    }

    @Override
    public void detachView() {
        mBaseView = null;
    }

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(mBaseView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                mBaseView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBaseView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }
}
