package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChooseProductListEntity extends BaseEntity {
    //展示的标题，已经做过国际化
    public String title;
    //	类型 0不展开 1：展开
    public String value;

    public List<ChooseProductEntity> productList = new ArrayList<>();
}
