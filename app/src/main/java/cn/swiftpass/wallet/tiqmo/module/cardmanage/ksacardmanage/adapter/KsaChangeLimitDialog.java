package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.OnOnlySingleClickListener;

public class KsaChangeLimitDialog extends BottomDialog {
    private Context mContext;

    private String currency;

    double dailyLimitMin = 0;
    double dailyLimitMax = 0;
    double monthlyLimitMin = 0;
    double monthlyLimitMax = 0;

    private KsaLimitChannelEntity.CardLimitConfigEntity cardLimitConfigEntity;

    private ChangeLimitListener mChangeLimitListener;

    public KsaChangeLimitDialog(Context context, KsaLimitChannelEntity.CardLimitConfigEntity cardLimitConfigEntity) {
        super(context);
        this.mContext = context;
        this.cardLimitConfigEntity = cardLimitConfigEntity;
        initViews(context);
    }

    public interface ChangeLimitListener {
        void changeLimit(KsaLimitChannelEntity.CardLimitConfigEntity cardLimitConfigEntity);
    }

    public void setChangeLimitListener(ChangeLimitListener changeLimitListener) {
        this.mChangeLimitListener = changeLimitListener;
    }

    private void initViews(Context context) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_change_ksa_card_limit, null);
        TextView tvChangeTitle = mView.findViewById(R.id.tv_change_title);
        TextView tvLimitCurrency = mView.findViewById(R.id.tv_limit_currency);
        TextView tvLimitLabel = mView.findViewById(R.id.tv_limit_label);
        EditTextWithDel etLimitAmount = mView.findViewById(R.id.et_limit_amount);
        TextView tvLimitChange = mView.findViewById(R.id.tv_limit_change);
        TextView tvErrorContent = mView.findViewById(R.id.tv_error_content);
        currency = LocaleUtils.getCurrencyCode("") + mContext.getString(R.string.Card_limits_7);

        String txnType = cardLimitConfigEntity.txnType;
        String monthlyLimitRangeNote = cardLimitConfigEntity.monthlyLimitRangeNote;
        String dailyLimitRangeNote = cardLimitConfigEntity.dailyLimitRangeNote;

        try {
            dailyLimitMin = AndroidUtils.getTransferMoneyNumber(cardLimitConfigEntity.dailyLimitMin);
            dailyLimitMax = AndroidUtils.getTransferMoneyNumber(cardLimitConfigEntity.dailyLimitMax);
            monthlyLimitMin = AndroidUtils.getTransferMoneyNumber(cardLimitConfigEntity.monthlyLimitMin);
            monthlyLimitMax = AndroidUtils.getTransferMoneyNumber(cardLimitConfigEntity.monthlyLimitMax);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }

        if (cardLimitConfigEntity.isDaily) {
            tvLimitLabel.setText(dailyLimitRangeNote);
            tvChangeTitle.setText(mContext.getString(R.string.DigitalCard_63));
        } else{
            currency = LocaleUtils.getCurrencyCode("") + mContext.getString(R.string.Card_limits_8);
            tvChangeTitle.setText(mContext.getString(R.string.DigitalCard_67));
            tvLimitLabel.setText(monthlyLimitRangeNote);
        }

        cardLimitConfigEntity.currency = currency;
        tvLimitCurrency.setText(currency);

        etLimitAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});
        etLimitAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        tvLimitChange.setOnClickListener(new OnOnlySingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mChangeLimitListener != null) {
                    String money = etLimitAmount.getText().toString().trim();
                    money = money.replace(",", "");
                    if (cardLimitConfigEntity.isDaily) {
                        cardLimitConfigEntity.dailyLimitValue = money;
                    } else {
                        cardLimitConfigEntity.monthlyLimitValue = money;
                    }
                    mChangeLimitListener.changeLimit(cardLimitConfigEntity);
                }
            }
        });

        etLimitAmount.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String amount = etLimitAmount.getText().toString().trim();
                if(!TextUtils.isEmpty(amount)) {
                    amount = amount.replace(",", "");
                    String fomatAmount = AmountUtil.dataFormat(amount);
                    if (!hasFocus) {
                        etLimitAmount.getEditText().setText(fomatAmount);
                        double money = Double.parseDouble(amount);
                        if (cardLimitConfigEntity.isDaily) {
                            if (money > dailyLimitMax || money < dailyLimitMin) {
                                tvLimitChange.setEnabled(false);
                                tvErrorContent.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (money > monthlyLimitMax || money < monthlyLimitMin) {
                                tvLimitChange.setEnabled(false);
                                tvErrorContent.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }
        });

        etLimitAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                try {
                    tvLimitChange.setEnabled(false);
                    if (TextUtils.isEmpty(amount)) {
                        return;
                    }
                    double money = Double.parseDouble(amount);
                    if (cardLimitConfigEntity.isDaily) {
                        if (money > dailyLimitMax || money < dailyLimitMin) {
                            tvLimitChange.setEnabled(false);
                        } else {
                            tvLimitChange.setEnabled(true);
                            tvErrorContent.setVisibility(View.GONE);
                        }
                    } else {
                        if (money > monthlyLimitMax || money < monthlyLimitMin) {
                            tvLimitChange.setEnabled(false);
                        } else {
                            tvLimitChange.setEnabled(true);
                            tvErrorContent.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    tvLimitChange.setEnabled(false);
                }
            }
        });

        setContentView(mView);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
