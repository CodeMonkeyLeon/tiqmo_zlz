package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class GetNewCardOneActivity extends BaseCompatActivity {

    public static final int type_my_tiqmo_card = 1;
    public static final int type_get_new_tiqmo_card = 3;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.iv_introduce_image)
    ImageView ivIntroduceImage;
    @BindView(R.id.tv_content_one)
    TextView tvContentOne;
    @BindView(R.id.tv_content_two)
    TextView tvContentTwo;
    @BindView(R.id.tv_content_three)
    TextView tvContentThree;
    @BindView(R.id.tv_get_card_now)
    TextView tvGetCardNow;

    private int seeCardType = 1;

    private double banlance;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_new_card_one;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        LocaleUtils.viewRotationY(mContext, ivBack, headCircle);
        ivBack.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_card_close));
        if (getIntent().getExtras() != null) {
            seeCardType = getIntent().getExtras().getInt(Constants.seeCardType);
            try {
                banlance = Double.parseDouble(getUserInfoEntity().getBalanceNumber());
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---" + e + "---");
            }
        }

        tvGetCardNow.setText(getString(R.string.DigitalCard_10));
        tvContentOne.setText(Spans.builder().text(getString(R.string.DigitalCard_2))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_3a3b44)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Regular.ttf" : "fonts/SFProDisplay-Regular.ttf"))
                .size(14)
                .text(" " + getString(R.string.DigitalCard_3))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf"))
                .size(14)
                .build());
        tvContentTwo.setText(Spans.builder().text(getString(R.string.DigitalCard_4))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf"))
                .size(14)
                .text(" " + getString(R.string.DigitalCard_5))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_3a3b44)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Regular.ttf" : "fonts/SFProDisplay-Regular.ttf"))
                .size(14)
                .build());
        tvContentThree.setText(Spans.builder().text(getString(R.string.DigitalCard_6))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_3a3b44)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Regular.ttf" : "fonts/SFProDisplay-Regular.ttf"))
                .size(14)
                .text(" " + getString(R.string.DigitalCard_7))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_090a15)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Bold.ttf" : "fonts/SFProDisplay-Bold.ttf"))
                .size(14)
                .text(" " + getString(R.string.DigitalCard_8))
                .color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_3a3b44)))
                .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext) ? "fonts/Alilato-Regular.ttf" : "fonts/SFProDisplay-Regular.ttf"))
                .size(14)
                .build());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            UserInfoEntity userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            try {
                banlance = Double.parseDouble(getUserInfoEntity().getBalanceNumber());
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---" + e + "---");
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_get_card_now})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_get_card_now:
                ActivitySkipUtil.startAnotherActivity(this, CardIntroductionActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }
}
