package cn.swiftpass.wallet.tiqmo.support.utils.input;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

/**
 * 输入相关的工具
 */
public class InputUtils {
    private static final Pattern mPattern = Pattern.compile("^0+[1-9]*(\\.[0-9]+)?");
    public static void addInputFilter(EditText editText, InputFilter inputFilter) {
        InputFilter[] filters = editText.getFilters();

        Class inputFilterClass = inputFilter.getClass();
        InputFilter intInputFilter = null;
        for (InputFilter filter : filters) {
            if (filter.getClass().equals(inputFilterClass)) {
                intInputFilter = filter;
            }
        }
        if (intInputFilter == null) {
            List<InputFilter> filterList = new ArrayList<>(Arrays.asList(filters));
            filterList.add(inputFilter);
            editText.setFilters(filterList.toArray(new InputFilter[0]));
        }
    }

    public static void setInputFilter(EditText editText, InputFilter... inputFilters) {
        editText.setFilters(inputFilters);
    }

    public static void hideKeyboard(final int maxLength, final EditText editText) {
        addInputFilter(editText, new InputFilter.LengthFilter(maxLength));
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= maxLength) {
                    AndroidUtils.hideKeyboard(editText);
                }
            }
        });
    }

    /**
     * 限制数为数字且格式正确，不能以0开头
     */
    public static void formatNum(final EditText editText) {
        addInputFilter(editText, new IntInputFilter());

        editText.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = s.toString().trim();
                if (TextUtils.isEmpty(temp)) {
                    return;
                }

                if (temp.length() > 1 && mPattern.matcher(temp).matches()) {
                    BigDecimal inputNum = new BigDecimal(temp);
                    if (inputNum.equals(BigDecimal.ZERO)) {
                        editText.setText("0");
                    } else {
                        editText.setText(inputNum.toPlainString());
                    }
                    editText.setSelection(editText.getText().length());
                }
            }
        });
    }

    public static void limitNum(final int min, final int max, final EditText editText) {
        limitNum(new BigDecimal(min), new BigDecimal(max), editText);
    }

    public static void limitNum(final BigDecimal minBigDecimal, final BigDecimal maxBigDecimal, final EditText editText) {
        addInputFilter(editText, new IntInputFilter());

        formatNum(editText);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = s.toString().trim();
                if (TextUtils.isEmpty(temp)) {
                    return;
                }

                BigDecimal inputNum = new BigDecimal(temp);
                if (inputNum.compareTo(maxBigDecimal) > 0) {
                    editText.setText(maxBigDecimal.toPlainString());
                    editText.setSelection(editText.getText().length());
                } else if (inputNum.compareTo(minBigDecimal) < 0) {
                    editText.setText(minBigDecimal.toPlainString());
                    editText.setSelection(editText.getText().length());
                }
            }
        });
    }
}
