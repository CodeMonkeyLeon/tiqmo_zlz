package cn.swiftpass.wallet.tiqmo.support.app2app;

import android.net.Uri;
import android.text.TextUtils;

public class PaySDKUtils {
    public static boolean checkUriIsFromPaySDK(Uri uri) {
        return uri != null && !TextUtils.isEmpty(uri.getScheme()) && uri.getScheme().equals("swiftpass")
                && !TextUtils.isEmpty(uri.getHost()) && uri.getHost().equals("cn.wallet.tiqmo")
                && !TextUtils.isEmpty(uri.getPath()) && uri.getPath().equals("/login")
                && isFromPaySDK(uri.getQueryParameter("origin"));
    }

    public static boolean isFromPaySDK(String origin) {
        return !TextUtils.isEmpty(origin) && origin.equals("PaySDK");
    }
}
