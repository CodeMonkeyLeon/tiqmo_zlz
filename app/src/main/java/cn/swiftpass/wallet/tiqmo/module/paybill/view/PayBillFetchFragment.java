package cn.swiftpass.wallet.tiqmo.module.paybill.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseFragment;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.adapter.PayBillIOListAdapter;
import cn.swiftpass.wallet.tiqmo.module.paybill.contract.PayBillContract;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.presenter.PayBillIOListPresenter;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.NoBillsFoundDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.PayBillErrorDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class PayBillFetchFragment extends BaseFragment<PayBillContract.BillIOListPresenter> implements PayBillContract.BillIOListView {

    private static final String ERROR_BILL_OVERDUE = "080037";
    private static final String ERROR_NO_BILL = "080036";
    private static final String ERROR_NO_BILL_LOCAL = "080130";
    
    @BindView(R.id.tv_fetch_bill)
    TextView tvFetchBill;
    @BindView(R.id.tv_payment_rule)
    TextView tvPaymentRule;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    //    @BindView(R.id.sc_content)
//    NestedScrollView scContent;
    @BindView(R.id.rv_io_list)
    RecyclerView rvIOList;

    private String billerId, sku, billerName;

    private String mChannelCode;

    private PayBillIOListAdapter payBillIOListAdapter;

    private List<PayBillIOEntity> ioList = new ArrayList<>();
    //退款的ioList  Moi才会有
    private List<PayBillIOEntity> refundIoList = new ArrayList<>();
    private HashMap<Integer, String> dataMap;

    private PayBillIOListEntity payBillIOListEntity;

    private PayBillOrderEntity payBillOrderEntity;

    private PayBillDetailEntity mPayBillDetailEntity;

    private int sceneType = -1;

    private boolean isMoi,isRefund;

    /**
     * 历史记录跳转
     * @param billerId
     * @param payBillIOListEntity
     * @param payBillOrderEntity
     * @param channelCode
     * @return
     */
    public static PayBillFetchFragment getInstance(String billerId,
                                                   PayBillIOListEntity payBillIOListEntity,
                                                   PayBillOrderEntity payBillOrderEntity,
                                                   String channelCode) {
        PayBillFetchFragment payBillFetchFragment = new PayBillFetchFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.paybill_billerId, billerId);
        bundle.putSerializable(Constants.paybill_billerIOListEntity, payBillIOListEntity);
        bundle.putSerializable(Constants.paybill_billerOrderEntity, payBillOrderEntity);
        bundle.putString(Constants.CHANNEL_CODE, channelCode);
        payBillFetchFragment.setArguments(bundle);
        return payBillFetchFragment;
    }

    /**
     * Moi类型
     * @param payBillIOListEntity
     * @param billerId
     * @param billerName
     * @param channelCode
     * @return
     */
    public static PayBillFetchFragment getInstance(PayBillIOListEntity payBillIOListEntity,
                                                   String billerId,
                                                   String billerName,
                                                   String channelCode,
                                                   boolean isMoi,boolean isRefund) {
        PayBillFetchFragment payBillFetchFragment = new PayBillFetchFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.paybill_billerIOListEntity, payBillIOListEntity);
        bundle.putString(Constants.paybill_billerId, billerId);
        bundle.putString(Constants.paybill_billerName, billerName);
        bundle.putString(Constants.CHANNEL_CODE, channelCode);
        bundle.putBoolean("isMoi", isMoi);
        bundle.putBoolean("isRefund", isRefund);
        payBillFetchFragment.setArguments(bundle);
        return payBillFetchFragment;
    }

    /**
     * bill类型
     * @param billerId
     * @param billerName
     * @param sku
     * @param channelCode
     * @return
     */
    public static PayBillFetchFragment getInstance(String billerId,
                                                   String billerName,
                                                   String sku,
                                                   String channelCode) {
        PayBillFetchFragment payBillFetchFragment = new PayBillFetchFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.paybill_billerId, billerId);
        bundle.putString(Constants.paybill_billerName, billerName);
        bundle.putString(Constants.paybill_billerSku, sku);
        bundle.putString(Constants.CHANNEL_CODE, channelCode);
        payBillFetchFragment.setArguments(bundle);
        return payBillFetchFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_paybill_fetch_bill;
    }

    @Override
    protected void initView(View parentView) {
        if (getArguments() != null) {
            billerId = getArguments().getString(Constants.paybill_billerId);
            billerName = getArguments().getString(Constants.paybill_billerName);
            sku = getArguments().getString(Constants.paybill_billerSku);
            payBillIOListEntity = (PayBillIOListEntity) getArguments().getSerializable(Constants.paybill_billerIOListEntity);
            payBillOrderEntity = (PayBillOrderEntity) getArguments().getSerializable(Constants.paybill_billerOrderEntity);
            mChannelCode = getArguments().getString(Constants.CHANNEL_CODE);
            isMoi = getArguments().getBoolean("isMoi");
            isRefund = getArguments().getBoolean("isRefund");

            if (payBillOrderEntity != null) {
                billerId = payBillOrderEntity.billerId;
                billerName = payBillOrderEntity.billerName;
                sku = payBillOrderEntity.sku;
                sceneType = payBillOrderEntity.sceneType;
                isMoi = payBillOrderEntity.isMoiBill();
                if (sceneType == 1 || sceneType == 2 || Constants.paybill_local.equals(mChannelCode)) {
                    tvFetchBill.setText(getString(R.string.BillPay_19_1));
                } else {
                    tvFetchBill.setText(getString(R.string.BillPay_19));
                }
            }
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 24));
        rvIOList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        rvIOList.setLayoutManager(manager);
        payBillIOListAdapter = new PayBillIOListAdapter(ioList,isMoi);
        payBillIOListAdapter.bindToRecyclerView(rvIOList);

        if (payBillIOListEntity != null) {
            setPayBillIOList(payBillIOListEntity);
        } else {
            if(Constants.paybill_local.equals(mChannelCode)){
                mPresenter.getNewPayBillIOList(billerId, billerId, mChannelCode);
            }else {
                mPresenter.getPayBillIOList(billerId, sku);
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void restart() {

    }


    public void saveEditData(int position, String content) {
        dataMap.put(position, content);

        for (Integer k : dataMap.keySet()) {
            String value = dataMap.get(k);
            if (TextUtils.isEmpty(value)) {
                tvFetchBill.setEnabled(false);
                return;
            }
        }

        tvFetchBill.setEnabled(true);
    }

    @OnClick({R.id.tv_fetch_bill})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_fetch_bill:
                List<String> dataList = new ArrayList<>();
                String aliasName = "";
                if (dataMap.size() > 0) {
                    for (Integer k : dataMap.keySet()) {
                        String value = dataMap.get(k);
                        PayBillIOEntity payBillIOEntity = payBillIOListEntity.ioList.get(k);
                        payBillIOEntity.value = value;
                        if (k == dataMap.size() - 1) {
                            aliasName = value;
                        } else {
                            dataList.add(value);
                        }
                    }
                    mPresenter.getPayBillServiceDetail(aliasName, billerId, sku, mChannelCode, dataList,isRefund?"R":"P");
                }

                break;
            default:
                break;
        }
    }

    private void setPayBillIOList(PayBillIOListEntity payBillIOListEntity) {
        this.payBillIOListEntity = payBillIOListEntity;
        ioList.clear();
        refundIoList.clear();
        if (payBillIOListEntity != null) {
            sceneType = payBillIOListEntity.sceneType;
            if (sceneType == 1 || sceneType == 2 || Constants.paybill_local.equals(mChannelCode)) {
                tvFetchBill.setText(getString(R.string.BillPay_19_1));
            } else {
                tvFetchBill.setText(getString(R.string.BillPay_19));
            }

            if(isMoi){
                if(isRefund){
                    tvFetchBill.setText(getString(R.string.sprint17_45));
                }else{
                    tvFetchBill.setText(getString(R.string.sprint17_44));
                }
            }
            String paymentRulePrompt = payBillIOListEntity.paymentRulePrompt;
            tvPaymentRule.setText(paymentRulePrompt);
            List<PayBillIOEntity> ioEntityList = payBillIOListEntity.ioList;
            if(isRefund){
                ioEntityList = payBillIOListEntity.refundIoList;
            }
            int size = ioEntityList.size();
            PayBillIOEntity payBillIOEntity = new PayBillIOEntity();
            payBillIOEntity.ioId = size + 1;
            payBillIOEntity.name = getString(R.string.BillPay_17);
            payBillIOEntity.description = getString(R.string.BillPay_18);
            payBillIOEntity.value = payBillIOListEntity.aliasName;
            ioEntityList.add(payBillIOEntity);

            dataMap = new HashMap<>();
            for(int i=0;i<ioEntityList.size();i++){
                dataMap.put(i, "");
            }
            if(isRefund){
                refundIoList.addAll(ioEntityList);
                payBillIOListAdapter.setDataList(refundIoList);
            }else {
                ioList.addAll(ioEntityList);
                payBillIOListAdapter.setDataList(ioList);
            }
        }
    }

    @Override
    public void getPayBillIOListSuccess(PayBillIOListEntity payBillIOListEntity) {
        setPayBillIOList(payBillIOListEntity);
    }

    @Override
    public void getPayBillDetailSuccess(PayBillDetailEntity payBillDetailEntity) {
        if (payBillDetailEntity != null) {
            this.mPayBillDetailEntity = payBillDetailEntity;
            String billReferenceName = mPayBillDetailEntity.billReferenceName;
            String billReferenceValue = mPayBillDetailEntity.billReferenceValue;
            if(TextUtils.isEmpty(billReferenceName)){
                if(payBillIOListEntity.ioList.size()>0) {
                    mPayBillDetailEntity.billReferenceName = payBillIOListEntity.ioList.get(0).name;
                }
            }
            if(TextUtils.isEmpty(billReferenceValue)){
                if(payBillIOListEntity.ioList.size()>0) {
                    mPayBillDetailEntity.billReferenceValue = payBillIOListEntity.ioList.get(0).value;
                }
            }

            List<String> invalidInputList = payBillDetailEntity.invalidInputList;
            List<PayBillIOEntity> ioEntityList = new ArrayList<>();
            ioEntityList.addAll(payBillIOListEntity.ioList);
            if(invalidInputList == null) invalidInputList = new ArrayList<>();
            int size = invalidInputList.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    int position = Integer.parseInt(invalidInputList.get(i));
                    for (int j = 0; j < ioEntityList.size(); j++) {
                        PayBillIOEntity payBillIOEntity = ioEntityList.get(j);
                        if (position == j + 1) {
                            payBillIOEntity.errorMsg = getString(R.string.BillPay_39) + " " + payBillIOEntity.name;
                            ioEntityList.set(j, payBillIOEntity);
                        } else {
                            payBillIOEntity.errorMsg = "";
                            ioEntityList.set(j, payBillIOEntity);
                        }
                    }
                }
                payBillIOListEntity.ioList.clear();
                payBillIOListEntity.ioList.addAll(ioEntityList);
                payBillIOListAdapter.setDataList(payBillIOListEntity.ioList);
            } else {
                mPresenter.getLimit(Constants.LIMIT_TRANSFER);
            }
        }
    }

    private void showErrorDialog(int errorType) {
        PayBillErrorDialog payBillErrorDialog = new PayBillErrorDialog(mContext, errorType, billerName);
        payBillErrorDialog.setBottomButtonListener(new PayBillErrorDialog.BottomButtonListener() {
            @Override
            public void clickButton() {
                payBillErrorDialog.dismiss();
            }
        });
        payBillErrorDialog.showWithBottomAnim();
    }

    private void showNoBillsFoundDialog() {
        NoBillsFoundDialog noBillsFoundDialog = new NoBillsFoundDialog(mContext);
        noBillsFoundDialog.showWithBottomAnim();
    }

    @Override
    public void getPayBillDetailFail(String errorCode, String errorMsg) {
        if(ERROR_NO_BILL_LOCAL.equals(errorCode)){
            showNoBillsFoundDialog();
        }else if (ERROR_NO_BILL.equals(errorCode)) {
            //no bill found,查无账单
            showErrorDialog(1);
        } else if (ERROR_BILL_OVERDUE.equals(errorCode)) {
            //bill overdue,账单超期
            showErrorDialog(2);
        } else {
            showTipDialog(errorMsg);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        PayBillDetailActivity.startPayBillDetailActivity(mActivity,
                mPayBillDetailEntity,
                transferLimitEntity,
                mChannelCode,isMoi);
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }


    @Override
    public void getNewPayBillIOListSuccess(PayBillIOListEntity result) {
        if (result != null) {
            setPayBillIOList(updateData(payBillIOListEntity, result));
        }
    }


    private PayBillIOListEntity updateData(PayBillIOListEntity oldData, PayBillIOListEntity newData) {

        if (!TextUtils.isEmpty(oldData.paymentRulePrompt)) {
            newData.paymentRulePrompt = oldData.paymentRulePrompt;
        }

        if (!TextUtils.isEmpty(oldData.aliasName)) {
            newData.aliasName = oldData.aliasName;
        }

        if (!TextUtils.isEmpty(oldData.isValidBiller)) {
            newData.isValidBiller = oldData.isValidBiller;
        }

        newData.sceneType = oldData.sceneType;

        for (PayBillIOEntity oldE : oldData.ioList) {
            for (PayBillIOEntity newE : newData.ioList) {
                if (TextUtils.equals(oldE.name, newE.name)) {
                    newE.description = oldE.description;
                    newE.value = oldE.value;
                    newE.errorMsg = oldE.errorMsg;
                }
            }
        }

        return newData;

    }


    @Override
    public PayBillContract.BillIOListPresenter getPresenter() {
        return new PayBillIOListPresenter();
    }
}
