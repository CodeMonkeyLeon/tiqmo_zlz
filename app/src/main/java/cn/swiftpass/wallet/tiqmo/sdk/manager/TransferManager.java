package cn.swiftpass.wallet.tiqmo.sdk.manager;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.IbanBaseActivityEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HomeAnalysisEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetListEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.TransferHistoryDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.BillServiceSearchResultEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillRefundListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillSkuListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillTypeListNewEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeCountryListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VoucherBrandListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.ApiHelper;
import cn.swiftpass.wallet.tiqmo.sdk.net.ResponseCallbackWrapper;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.AuthApi;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.PaymentApi;
import cn.swiftpass.wallet.tiqmo.sdk.net.api.UserApi;
import cn.swiftpass.wallet.tiqmo.sdk.util.CallbackUtil;

public class TransferManager {

    private AppClient mAppClient;
    private UserApi mUserApi;
    private AuthApi mAuthApi;
    private PaymentApi mPaymentApi;

    public TransferManager(AppClient client) {
        mAppClient = client;
        mUserApi = ApiHelper.getApi(UserApi.class);
        mAuthApi = ApiHelper.getApi(AuthApi.class);
        mPaymentApi = ApiHelper.getApi(PaymentApi.class);
    }

    //获取交易详情
    public void getTransferDetail(String orderNo, String orderType,String queryType, final ResultCallback<TransferHistoryDetailEntity> callback) {
        mAuthApi.getHistoryDetail(orderNo, orderType,queryType).enqueue(new ResponseCallbackWrapper<>(new ResultCallback<TransferHistoryDetailEntity>() {
            @Override
            public void onResult(TransferHistoryDetailEntity response) {
                CallbackUtil.callResult(response, callback);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                CallbackUtil.callFailure(errorCode, errorMsg, callback);
            }
        }));
    }

    //账单支付-国家列表(3220)
    public void getPayBillCountry(LifecycleMVPResultCallback<PayBillCountryListEntity> callback) {
        mPaymentApi.getPayBillCountry().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //3221
    public void getPayBillCategory(String countryCode, String channelCode, LifecycleMVPResultCallback<PayBillTypeListNewEntity> callback) {
        mPaymentApi.getPayBillCategory(countryCode, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //账单支付-账单类别(3221)
    public void getPayBillTypeList(String countryCode, String channelCode, LifecycleMVPResultCallback<PayBillTypeListEntity> callback) {
        mPaymentApi.getPayBillTypeList(countryCode, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //3222
    public void searchBillSearch(String searchBillerNameOrCodeOfLocalService, String countryCode, String channelCode, LifecycleMVPResultCallback<BillServiceSearchResultEntity> callback) {
        mPaymentApi.searchBillService(searchBillerNameOrCodeOfLocalService, countryCode, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //账单支付-账单类别(3222)
    public void getPayBillerList(String countryCode, String billerType, String billerDescription, String channelCode, LifecycleMVPResultCallback<PayBillerListEntity> callback) {
        mPaymentApi.getPayBillerList(countryCode, billerType, billerDescription, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //账单支付-sku列表(3223)
    public void getPayBillSkuList(String billerId, String channelCode, LifecycleMVPResultCallback<PayBillSkuListEntity> callback) {
        mPaymentApi.getPayBillSkuList(billerId, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //3224
    public void getPayBillServiceIOList(String billerId, String sku, String channelCode, LifecycleMVPResultCallback<PayBillIOListEntity> callback) {
        mPaymentApi.getPayBillServiceIOList(billerId, sku, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //账单支付-io列表(3224)
    public void getPayBillIOList(String billerId, String sku, LifecycleMVPResultCallback<PayBillIOListEntity> callback) {
        mPaymentApi.getPayBillIOList(billerId, sku).enqueue(new ResponseCallbackWrapper<>(callback));
    }

//    //账单支付-账单详情(3225)
//    public void getPayBillDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data, LifecycleMVPResultCallback<PayBillDetailEntity> callback) {
//        mPaymentApi.getPayBillServicreDetail(aliasName, billerId, sku, channelCode, data).enqueue(new ResponseCallbackWrapper<>(callback));
//    }


    //账单支付-账单详情(3225)
    public void getPayBillServiceDetail(String aliasName, String billerId, String sku, String channelCode, List<String> data,String paymentType, LifecycleMVPResultCallback<PayBillDetailEntity> callback) {
        mPaymentApi.getPayBillServiceDetail(aliasName, billerId, sku, channelCode, data,paymentType).enqueue(new ResponseCallbackWrapper<>(callback));
    }


    //账单支付-账单预下单(3226)
    public void fetchBill(String payAmount, String billAmount, String billProcessingFeePayAmount, String billProcessingFeeAmount, String channelCode, LifecycleMVPResultCallback<PayBillOrderInfoEntity> callback) {
        mPaymentApi.fetchBill(payAmount, billAmount, billProcessingFeePayAmount, billProcessingFeeAmount, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账单支付-账单预下单(3227)
    public void getBillOrderHistory(List<String> countryCodeList, String domesticFlag, LifecycleMVPResultCallback<PayBillOrderListEntity> callback) {
        mPaymentApi.getBillOrderHistory(countryCodeList, domesticFlag).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账单支付-账单预下单(3227)
    public void getHomeBillOrderList(LifecycleMVPResultCallback<PayBillOrderListEntity> callback) {
        mPaymentApi.getHomeBillOrderList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账单支付-偏好支付(3228)
    public void getPayBillOrderIOList(String billerId, String sku, String aliasName, String channelCode, LifecycleMVPResultCallback<PayBillIOListEntity> callback) {
        mPaymentApi.getPayBillOrderIOList(billerId, sku, aliasName, channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //账单支付-删除账单(3229)
    public void deletePayBillOrder(String billerId, String aliasName, LifecycleMVPResultCallback<Void> callback) {
        mPaymentApi.deletePayBillOrder(billerId, aliasName).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询 品牌信息和该品牌下券的总数（3230）
    public void getVoucherBrandList(LifecycleMVPResultCallback<VoucherBrandListEntity> callback) {
        mPaymentApi.getVoucherBrandList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询品牌列表和最近交易列表（3231）
    public void getRecentVoucherBrandList(LifecycleMVPResultCallback<VoucherBrandListEntity> callback) {
        mPaymentApi.getRecentVoucherBrandList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //首页预算统计接口(3120)
    public void getHomeAnalysis(LifecycleMVPResultCallback<HomeAnalysisEntity> callback) {
        mPaymentApi.getHomeAnalysis().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //消费支出分析汇总信息接口(3119)
    public void getAnalysisDetail(String periodType, String spendingAnalysisQueryDate, LifecycleMVPResultCallback<SpendingDetailEntity> callback) {
        mPaymentApi.getAnalysisDetail(periodType, spendingAnalysisQueryDate).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询预算信息列表（3234）
    public void getSpendingBudgetList(LifecycleMVPResultCallback<SpendingBudgetListEntity> callback) {
        mPaymentApi.getSpendingBudgetList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //保存和更新用户预算信息（3233）
    public void saveUserSpending(List<SpendingBudgetEntity> userSpendingBudgetSaveOrUpdateInfoList, List<String> deleteBudgetIdList, LifecycleMVPResultCallback<Void> callback) {
        mPaymentApi.saveUserSpending(userSpendingBudgetSaveOrUpdateInfoList, deleteBudgetIdList).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //国际话费充值-预下单（3235）
    public void rechargePreOrder(String productId, String operatorId, String country, String mobileNumber,
                                 String amount, String targetAmount, String shortName, String operatorName, LifecycleMVPResultCallback<RechargeOrderInfoEntity> callback) {
        mPaymentApi.rechargePreOrder(productId, operatorId, country, mobileNumber,
                amount, targetAmount, shortName, operatorName).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //获取国家列表 (3236)
    public void getRechargeCountryList(LifecycleMVPResultCallback<RechargeCountryListEntity> callback) {
        mPaymentApi.getRechargeCountryList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //通过手机号码获取运营商列表(3237)
    public void getRechargeOperatorList(String phoneNubmer, LifecycleMVPResultCallback<RechargeOperatorListEntity> callback) {
        mPaymentApi.getRechargeOperatorList(phoneNubmer).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //通过手机号码获取运营商列表(3238)
    public void getRechargeProductList(String operatorId, LifecycleMVPResultCallback<RechargeProductListEntity> callback) {
        mPaymentApi.getRechargeProductList(operatorId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //通过手机号码获取运营商列表(3239)
    public void getRechargeOrderList(LifecycleMVPResultCallback<RechargeOrderListEntity> callback) {
        mPaymentApi.getRechargeOrderList().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //通过手机号码获取运营商列表(3240)
    public void saveRechargeOrder(String countryCode, String userShortName, String operatorId,
                                  String operatorName, String callingCode, String phone, LifecycleMVPResultCallback<Void> callback) {
        mPaymentApi.saveRechargeOrder(countryCode, userShortName, operatorId,
                operatorName, callingCode, phone).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询话费充值偏好详情(3241)
    public void getRechargeOrderDetail(String recordId, LifecycleMVPResultCallback<RechargeOrderDetailEntity> callback) {
        mPaymentApi.getRechargeOrderDetail(recordId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //删除偏好信息(3242)
    public void deleteRechargeOrder(String recordId, LifecycleMVPResultCallback<Void> callback) {
        mPaymentApi.deleteRechargeOrder(recordId).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //慈善支付-账单预下单(3271)
    public void preCharityOrder(String channelCode,String payAmount,String domainDonation,LifecycleMVPResultCallback<RechargeOrderInfoEntity> callback) {
        mPaymentApi.preCharityOrder(channelCode,payAmount,domainDonation).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发起退款(3274)
    public void payBillRefundOrder(String channelCode,LifecycleMVPResultCallback<PayBillRefundEntity> callback) {
        mPaymentApi.payBillRefundOrder(channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //发起退款申请列表(3275)
    public void getPayBillRefundList(String billerId,String channelCode,LifecycleMVPResultCallback<PayBillRefundListEntity> callback) {
        mPaymentApi.getPayBillRefundList(billerId,channelCode).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //退款申请记录详情(3276)
    public void getPayBillRefundDetail(String channelCode,String id,LifecycleMVPResultCallback<PayBillRefundEntity> callback) {
        mPaymentApi.getPayBillRefundDetail(channelCode,id).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //查询IBAN充值订单活动规则(3277)
    public void getIbanActivityDetail(LifecycleMVPResultCallback<IbanBaseActivityEntity> callback) {
        mPaymentApi.getIbanActivityDetail().enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //绑卡充值预下单(3257)
    public void bindCardPreOrder(String custName, String cardNo, String expire, String cvv, String type, String orderAmount,
                                 String orderCurrencyCode,String creditType, LifecycleMVPResultCallback<TransferPayEntity> callback) {
        mPaymentApi.bindCardPreOrder(custName, cardNo, expire, cvv, type, orderAmount,
                orderCurrencyCode, creditType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //协议支付预下单(3258)
    public void cardAddMoneyPreOrder(String type, String orderAmount, String orderCurrencyCode,
                                     String protocolNo, ResultCallback<TransferPayEntity> callback) {
        mPaymentApi.cardAddMoneyPreOrder(type, orderAmount, orderCurrencyCode,protocolNo).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //绑卡充值确认(3259)
    public void bindCardConfirmPay(String orderNo, String cvv, String type,
                                   String shopperResultUrl,
                                   String creditType, ResultCallback<CardBind3DSEntity> callback) {
        mPaymentApi.bindCardConfirmPay(orderNo, cvv, type, shopperResultUrl, creditType).enqueue(new ResponseCallbackWrapper<>(callback));
    }

    //卡充值确认(3260)
    public void cardAddMoneyConfirmPay(String orderNo, String cvv, String type,
                                       String shopperResultUrl,
                                       String creditType, ResultCallback<CardBind3DSEntity> callback) {
        mPaymentApi.cardAddMoneyConfirmPay(orderNo, cvv, type, shopperResultUrl, creditType).enqueue(new ResponseCallbackWrapper<>(callback));
    }
}
