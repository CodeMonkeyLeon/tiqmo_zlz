package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AreaEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class AreaCodeAdapter extends BaseRecyclerAdapter<AreaEntity> {

    public AreaCodeAdapter(@Nullable List<AreaEntity> data) {
        super(R.layout.item_select_area, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, AreaEntity areaEntity, int position) {
        baseViewHolder.setGone(R.id.iv_arrow, position == 1 ? true : false);
        baseViewHolder.setText(R.id.tv_select_code, areaEntity.getShowCallingCode());
        baseViewHolder.setText(R.id.tv_select_content, areaEntity.countryName);
        baseViewHolder.setImageResource(R.id.iv_icon, areaEntity.countryImgId);
    }
}
