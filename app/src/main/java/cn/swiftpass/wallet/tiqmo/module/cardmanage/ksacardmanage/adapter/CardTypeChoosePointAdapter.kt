package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter

import androidx.constraintlayout.widget.ConstraintLayout
import cn.swiftpass.wallet.tiqmo.R
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.CardTypePointEntity
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view.ChoosePointView
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder

class CardTypeChoosePointAdapter(mList: List<CardTypePointEntity>) :
    BaseRecyclerAdapter<CardTypePointEntity>(
        R.layout.item_card_type_c,
        mList
    ) {


    override fun bindData(
        baseViewHolder: BaseViewHolder,
        item: CardTypePointEntity?,
        position: Int
    ) {
        item?.run {
            baseViewHolder.getView<ChoosePointView>(R.id.id_choose_point_view).setSelect(isSelect)

            if (isSelect) {
                baseViewHolder.getView<ConstraintLayout>(R.id.id_cl_card_type_point).background =
                    mContext.getDrawable(R.drawable.bg_card_type_select)
            } else {
                baseViewHolder.getView<ConstraintLayout>(R.id.id_cl_card_type_point).background =
                    mContext.getDrawable(R.drawable.bg_card_type_default)
            }


            if (isStandardCard) {
                baseViewHolder.setImageResource(R.id.id_img_card, R.drawable.standard_card)
            } else {
                baseViewHolder.setImageResource(R.id.id_img_card, R.drawable.platinum_card)
            }

            baseViewHolder.setText(R.id.id_tv_card_type, textId)
            baseViewHolder.setText(R.id.id_tv_price, priceText)
        }
    }

}




