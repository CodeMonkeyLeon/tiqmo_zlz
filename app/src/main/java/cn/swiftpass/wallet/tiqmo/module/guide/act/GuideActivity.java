package cn.swiftpass.wallet.tiqmo.module.guide.act;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.guide.OnNextClickListener;
import cn.swiftpass.wallet.tiqmo.module.guide.entity.GuidePageData;
import cn.swiftpass.wallet.tiqmo.module.guide.view.GuidePageView;
import cn.swiftpass.wallet.tiqmo.module.guide.view.GuidePointView;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;

/**
 * @author lizheng.zhao
 * @date 2023/06/09
 * @description 引导页
 */
public class GuideActivity extends BaseCompatActivity implements View.OnClickListener {


    @Override
    protected int getLayoutID() {
        return R.layout.act_guide;
    }


    @BindView(R.id.id_view_pager)
    ViewPager mViewPager;

    @BindView(R.id.id_guide_point)
    GuidePointView mGuidePointView;

    @BindView(R.id.id_btn_skip)
    MyTextView mBtnSkip;


    private List<GuidePageData> mPageDataList = new ArrayList<>();

    private List<GuidePageView> mPageViewList = new ArrayList<>();


    public static void startGuideActivity(Activity fromActivity) {
        ActivitySkipUtil.startAnotherActivity(
                fromActivity,
                GuideActivity.class,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN
        );
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        mBtnSkip.setOnClickListener(this);
        initPageData();
        initPageView();
        initViewPager();
    }


    private void initViewPager() {
        mViewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return mPageViewList.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }


            @NonNull
            @Override
            public Object instantiateItem(@NonNull final ViewGroup container, final int position) {
                container.addView(mPageViewList.get(position));
                return mPageViewList.get(position);
            }

            @Override
            public void destroyItem(@NonNull final ViewGroup container, final int position, @NonNull final Object object) {
                container.removeView(mPageViewList.get(position));
            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mGuidePointView.setSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void initPageView() {
        for (int i = 0; i < mPageDataList.size(); i++) {
            GuidePageView gp = new GuidePageView(this);
            gp.setIndex(i);
            gp.setImage(mPageDataList.get(i).getImgResId());
            gp.setTitle(mPageDataList.get(i).getTitle());
            gp.setContent(mPageDataList.get(i).getContent());
            gp.setNext(mPageDataList.get(i).getNext());
            gp.setOnNextClickListener(new OnNextClickListener() {
                @Override
                public void onNextClick(int index) {
                    if (index >= 3) {
                        finish();
                    } else {
                        mViewPager.setCurrentItem(index + 1);
                    }
                }
            });
            mPageViewList.add(gp);
        }

    }

    private void initPageData() {
        mPageDataList.add(
                new GuidePageData(
                        ThemeSourceUtils.getSourceID(mContext, R.attr.guide_page_1),
                        getString(R.string.sprint21_1),
                        getString(R.string.sprint21_2),
                        getString(R.string.sprint21_22)
                )
        );

        mPageDataList.add(
                new GuidePageData(
                        ThemeSourceUtils.getSourceID(mContext, R.attr.guide_page_2),
                        getString(R.string.sprint21_3),
                        getString(R.string.sprint21_4),
                        getString(R.string.sprint21_22)
                )
        );


        mPageDataList.add(
                new GuidePageData(
                        ThemeSourceUtils.getSourceID(mContext, R.attr.guide_page_3),
                        getString(R.string.sprint21_5),
                        getString(R.string.sprint21_6),
                        getString(R.string.sprint21_22)
                )
        );

        mPageDataList.add(
                new GuidePageData(
                        ThemeSourceUtils.getSourceID(mContext, R.attr.guide_page_4),
                        getString(R.string.sprint21_7),
                        getString(R.string.sprint21_8),
                        getString(R.string.sprint21_23)
                )
        );

    }


    @Override
    public void onClick(View v) {
        if (v != null) {
            switch (v.getId()) {
                case R.id.id_btn_skip:
                    //点击跳过
                    finish();
                    break;

                default:
                    break;
            }
        }
    }


    /**
     * 拦截返回事件
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {


        }
        return true;
    }

}
