package cn.swiftpass.wallet.tiqmo.module.setting.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.AccountDetailEntity;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class AccountPayDetailAdapter extends BaseRecyclerAdapter<AccountDetailEntity> {

    public AccountPayDetailAdapter(@Nullable List<AccountDetailEntity> data) {
        super(R.layout.item_pay_detail, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, AccountDetailEntity accountDetailEntity, int position) {
        String date = getSectionForPosition(position);
        if (TextUtils.isEmpty(date)) {
            return;
        }

        if (position == mDataList.size() - 1) {
            holder.setGone(R.id.view, false);
        } else {
            holder.setGone(R.id.view, true);
        }

        holder.setGone(R.id.ll_date, false);
        if (position == getPositionForSection(date)) {
            holder.setGone(R.id.ll_date, false);
            holder.setText(R.id.tv_date, accountDetailEntity.date);
        } else {
            holder.setGone(R.id.ll_date, true);
        }
    }

    /**
     * 根据当前位置获取时间值
     */
    public String getSectionForPosition(int position) {
        AccountDetailEntity accountDetailEntity = getItem(position);
        if(accountDetailEntity != null) {
            return accountDetailEntity.date;
        }
        return "";
    }

    public int getPositionForSection(String date) {
        for (int i = 0; i < getItemCount(); i++) {
            String dateStr = mDataList.get(i).date;
            if (!TextUtils.isEmpty(dateStr) && dateStr.equals(date)) {
                return i;
            }
        }
        return -1;
    }
}
