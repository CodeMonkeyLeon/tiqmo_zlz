package cn.swiftpass.wallet.tiqmo.module.transfer.entity;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class TransactionListInfos extends BaseEntity {
    private String orderDesc;
    private String tradeTime;
    private String amount;
    private String orderCurrency;
    private String orderStatus;
    private List<SplitOderDetail> splitOderDetail;
    private String dateDesc;

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<SplitOderDetail> getSplitOderDetail() {
        return splitOderDetail;
    }

    public void setSplitOderDetail(List<SplitOderDetail> splitOderDetail) {
        this.splitOderDetail = splitOderDetail;
    }

    public String getDateDesc() {
        return dateDesc;
    }

    public void setDateDesc(String dateDesc) {
        this.dateDesc = dateDesc;
    }


    public static class SplitOderDetail extends BaseEntity{

        private String orderDesc;
        private String tradeTime;
        private String amount;
        private String orderCurrency;
        private String orderStatus;

        public String getOrderDesc() {
            return orderDesc;
        }

        public void setOrderDesc(String orderDesc) {
            this.orderDesc = orderDesc;
        }

        public String getTradeTime() {
            return tradeTime;
        }

        public void setTradeTime(String tradeTime) {
            this.tradeTime = tradeTime;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getOrderCurrency() {
            return orderCurrency;
        }

        public void setOrderCurrency(String orderCurrency) {
            this.orderCurrency = orderCurrency;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }
    }
}
