package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ImrSetupPresenter implements ImrSetupContract.IImrSetupPresenter {

    private ImrSetupContract.ImrSetupView mImrSetupView;
    private BusinessParamEntity data;

    interface OnResponseListener {
        void onResponseSuccess();
    }

    public void getData(OnResponseListener listener) {
        AppClient.getInstance().getBusinessParamList(new LifecycleMVPResultCallback<BusinessParamEntity>(mImrSetupView, true) {
            @Override
            protected void onSuccess(BusinessParamEntity result) {
                data = result;
                if (listener != null) {
                    listener.onResponseSuccess();
                }
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mImrSetupView != null) {
                    mImrSetupView.showErrorMsg(errorCode, errorMsg);
                }
            }
        });
    }

    @Override
    public void getSourceOfFund() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    mImrSetupView.showSourceOfFund(data.sourceOfFund);
                }
            });
        } else {
            mImrSetupView.showSourceOfFund(data.sourceOfFund);
        }

    }

    @Override
    public void getProfession() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    mImrSetupView.showProfession(data.profession);
                }
            });
        } else {
            mImrSetupView.showProfession(data.profession);
        }
    }

    @Override
    public void getCompanyType() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    mImrSetupView.showCompanyType(data.companyType);
                }
            });
        } else {
            mImrSetupView.showCompanyType(data.companyType);
        }
    }

    @Override
    public void getSalaryRange() {
        if (data == null) {
            getData(new OnResponseListener() {
                @Override
                public void onResponseSuccess() {
                    mImrSetupView.showSalaryRange(data.annualIncome);
                }
            });
        } else {
            mImrSetupView.showSalaryRange(data.annualIncome);
        }
    }

    @Override
    public void getUserInfo() {
        AppClient.getInstance().getUserManager().getUserInfo(new LifecycleMVPResultCallback<UserInfoEntity>(mImrSetupView, true) {
            @Override
            protected void onSuccess(UserInfoEntity result) {
                mImrSetupView.getUserInfoSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
            }
        });
    }

    @Override
    public void updateUserInfo(UserInfoEntity userInfo) {
        AppClient.getInstance().getUserManager().updateUserInfo(userInfo,
                new LifecycleMVPResultCallback<UserInfoEntity>(mImrSetupView, true) {
                    @Override
                    protected void onSuccess(UserInfoEntity result) {
                        AppClient.getInstance().getUserManager().bindUserInfo(result, false);
                        mImrSetupView.updateUserInfoSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mImrSetupView.showErrorMsg(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void attachView(ImrSetupContract.ImrSetupView imrSetupView) {
        this.mImrSetupView = imrSetupView;
    }

    @Override
    public void detachView() {
        this.mImrSetupView = null;
    }
}
