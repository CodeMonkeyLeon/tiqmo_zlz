package cn.swiftpass.wallet.tiqmo.module.register.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseWebViewActivity;
import cn.swiftpass.wallet.tiqmo.module.login.view.LoginFastNewActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AccountPayDetailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AddCardFailActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.AddCardSuccessActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivityLifeManager;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class WebViewActivity extends BaseWebViewActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.webView)
    WebView webView;

    String url_3ds;
    String orderNo;
    boolean isCardDetail;
    private String type;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_agree;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (getIntent() != null && getIntent().getExtras() != null) {
            url_3ds = getIntent().getExtras().getString(Constants.BIND_CARD_3DS);
            orderNo = getIntent().getExtras().getString(Constants.Hyperpay_orderNo);
            isCardDetail = getIntent().getExtras().getBoolean("isCardDetail");
            type = getIntent().getExtras().getString(Constants.terms_condition_type);
        }

        if(!TextUtils.isEmpty(url_3ds)){
//            tvTitle.setText(R.string.add_new_card_1);
            setWebViewClient(webView);
            initWebView(webView, url_3ds, isCardDetail);
        }else{
            tvTitle.setText(R.string.Profile_CLP_0010_L_15);

            ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
            if(configEntity != null) {
                setWebViewClient(webView);
                if (configEntity.agreementURLs != null && configEntity.agreementURLs.size() > 0) {
                    initWebView(webView, configEntity.getAgreeUrl(type), false);
                }
            }
        }
        LocaleUtils.viewRotationY(this, ivBack, headCircle);
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        finish();
    }

    @Override
    protected void request3ds(String url,int operateType) {
        if(operateType == 2){
            AppClient.getInstance().request3ds("",orderNo, new ResultCallback<CardNotifyEntity>() {
                @Override
                public void onResult(CardNotifyEntity result) {
                    if (result == null) {
                        return;
                    }
                    if (!TextUtils.isEmpty(result.bizCode) && "010167".equals(result.bizCode)) {
                        return;
                    }
                    if (!TextUtils.isEmpty(result.operateType) && "recharge".equals(result.operateType)) {
                        addMoneySuccess(result, WebViewActivity.this);
                    }
                }

                @Override
                public void onFailure(String errorCode, String errorMsg) {
                    LogUtils.i(TAG, "shouldOverrideUrlLoading: errorCode: " + errorCode + errorMsg);
                }
            });
        }else {
            String sid = "";
            String[] temp = url.split("/");
            for (int i = 0; i < temp.length; i++) {
                if (!TextUtils.isEmpty(temp[i]) && temp[i].startsWith("sid_")) {
                    sid = temp[i];
                }
            }
            LogUtils.i(TAG, "shouldOverrideUrlLoading: sid: " + sid);
            if (TextUtils.isEmpty(sid)) {
                return;
            }
            AppClient.getInstance().request3ds(sid, new ResultCallback<CardNotifyEntity>() {
                @Override
                public void onResult(CardNotifyEntity result) {
                    if (result == null) {
                        return;
                    }
                    if (!TextUtils.isEmpty(result.bizCode) && "010167".equals(result.bizCode)) {
                        return;
                    }
                    if (!TextUtils.isEmpty(result.operateType) && "recharge".equals(result.operateType)) {
                        addMoneySuccess(result, WebViewActivity.this);
                    } else if (!TextUtils.isEmpty(result.operateType) && "bind_card".equals(result.operateType)) {
                        addCardSuccess(result, WebViewActivity.this);
                    }
                }

                @Override
                public void onFailure(String errorCode, String errorMsg) {
                    LogUtils.i(TAG, "shouldOverrideUrlLoading: errorCode: " + errorCode + errorMsg);
                }
            });
        }
    }



    private static void addCardSuccess(CardNotifyEntity cardNotifyEntity, Context context) {
        //绑卡成功
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
        ProjectApp.addCardSuccess = true;
        ProjectApp.removeKycTask();
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            if ("000000".equals(cardNotifyEntity.bizCode)) {
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AddCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            } else {
                ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AddCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            }
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.ADD_CARD_ENTITY, cardNotifyEntity);
            context.startActivity(it);
        }
    }

    private static void addMoneySuccess(CardNotifyEntity cardNotifyEntity, Context context) {
        //充值成功
        cardNotifyEntity.respCode = cardNotifyEntity.bizCode;
        if ("000000".equals(cardNotifyEntity.bizCode)) {
        } else {
            cardNotifyEntity.respMsg = cardNotifyEntity.bizMsg;
        }
        cardNotifyEntity.subject = Constants.SUBJECT_ADD_MONEY;
        cardNotifyEntity.paymentMethodDesc = cardNotifyEntity.payMethodDesc;
        HashMap<String, Object> mHashMaps = new HashMap<>(16);
        mHashMaps.put(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
        ProjectApp.removeHyperpayTask();
        ProjectApp.removeAddMoneyTask();
        if (ActivityLifeManager.getInstance().getCurrentActivity() != null) {
            ActivitySkipUtil.startAnotherActivity(ActivityLifeManager.getInstance().getCurrentActivity(), AccountPayDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
            ProjectApp.addMoneySuccessOnMain = false;
        } else {
            Intent it = new Intent(context, LoginFastNewActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Constants.ADD_MONEY_ENTITY, cardNotifyEntity);
            context.startActivity(it);
        }
    }
}
