package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.BeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TimeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AddBeneficiaryPresenter implements TransferContract.AddBeneficiaryPresenter {

    TransferContract.AddBeneficiaryView addBeneficiaryView;


    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(addBeneficiaryView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                addBeneficiaryView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addBeneficiaryView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void addBeneficiary(String beneficiaryName, String beneficiaryIbanNo, String chooseRelationship, String saveFlag) {
        AppClient.getInstance().addBeneficiary(beneficiaryName, beneficiaryIbanNo, chooseRelationship, saveFlag, new LifecycleMVPResultCallback<BeneficiaryEntity>(addBeneficiaryView, true) {
            @Override
            protected void onSuccess(BeneficiaryEntity result) {
                addBeneficiaryView.AddBeneficiarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addBeneficiaryView.AddBeneficiaryFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getTime(String ibanNo) {
        AppClient.getInstance().getTransferTime(ibanNo, new LifecycleMVPResultCallback<TimeEntity>(addBeneficiaryView, false) {
            @Override
            protected void onSuccess(TimeEntity result) {
                addBeneficiaryView.getTimeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addBeneficiaryView.getTimeFailed(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(addBeneficiaryView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                addBeneficiaryView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addBeneficiaryView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(TransferContract.AddBeneficiaryView addBeneficiaryView) {
        this.addBeneficiaryView = addBeneficiaryView;
    }

    @Override
    public void detachView() {
        this.addBeneficiaryView = null;
    }
}
