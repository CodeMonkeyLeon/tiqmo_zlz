package cn.swiftpass.wallet.tiqmo.module.guide.entity

import java.io.Serializable

data class GuidePageData(
    val imgResId: Int,
    val title: String?,
    val content: String?,
    val next: String?
) : Serializable
