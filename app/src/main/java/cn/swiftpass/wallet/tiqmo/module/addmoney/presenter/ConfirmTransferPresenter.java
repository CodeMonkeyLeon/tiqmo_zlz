package cn.swiftpass.wallet.tiqmo.module.addmoney.presenter;

import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CardNotifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;

public class ConfirmTransferPresenter implements AddMoneyContract.ConfirmTransferPresenter {

    AddMoneyContract.ConfirmTransferView mConfirmTransferView;

    @Override
    public void bindCardConfirmPay(String orderNo, String cvv, String type, String shopperResultUrl, String creditType) {
        AppClient.getInstance().getTransferManager().bindCardConfirmPay(orderNo, cvv, type, shopperResultUrl, creditType, new LifecycleMVPResultCallback<CardBind3DSEntity>(mConfirmTransferView,true) {
            @Override
            protected void onSuccess(CardBind3DSEntity result) {
                mConfirmTransferView.bindCardConfirmPayPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mConfirmTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void cardAddMoneyConfirmPay(String orderNo, String cvv, String type, String shopperResultUrl, String creditType) {
        AppClient.getInstance().getTransferManager().cardAddMoneyConfirmPay(orderNo, cvv, type, shopperResultUrl, creditType, new LifecycleMVPResultCallback<CardBind3DSEntity>(mConfirmTransferView,true) {
            @Override
            protected void onSuccess(CardBind3DSEntity result) {
                mConfirmTransferView.cardAddMoneyConfirmPaySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mConfirmTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void request3ds(String sessionId, String orderNo) {
        AppClient.getInstance().request3ds(sessionId, orderNo, new ResultCallback<CardNotifyEntity>() {
            @Override
            public void onResult(CardNotifyEntity response) {
                mConfirmTransferView.request3dsSuccess(response);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                mConfirmTransferView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddMoneyContract.ConfirmTransferView confirmTransferView) {
        this.mConfirmTransferView = confirmTransferView;
    }

    @Override
    public void detachView() {
        this.mConfirmTransferView = null;
    }
}
