package cn.swiftpass.wallet.tiqmo.module.addmoney.view;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.adapter.SelectCardAdapter;
import cn.swiftpass.wallet.tiqmo.module.addmoney.contract.AddMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferPayEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.presenter.CardListPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.unBindCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.AllCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.dialog.CardDeleteDialog;
import cn.swiftpass.wallet.tiqmo.widget.dialog.EnterCardCvvDialog;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class SelectCardListActivity extends BaseCompatActivity<AddMoneyContract.CardListPresenter> implements AddMoneyContract.CardListView {
    @BindView(R.id.tv_select_title)
    TextView tvSelectTitle;
    @BindView(R.id.tv_add_new)
    TextView tvAddNew;
    @BindView(R.id.ry_card_list)
    RecyclerView ryCardList;
    @BindView(R.id.ll_no_card)
    LinearLayout llNoCard;

    SelectCardAdapter selectCardAdapter;
    private List<CardEntity> cardList = new ArrayList<>();

    private String cardType = "";
    private AllCardEntity allCardEntity;
    private String addMoney;
    private CardEntity mCardEntity;

    private EnterCardCvvDialog enterCardCvvDialog;

    private CardDeleteDialog cardDeleteDialog;

    private TransFeeEntity mTransFeeEntity;

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_select_card;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        windowColor();

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 10));
        ryCardList.addItemDecoration(myItemDecoration);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryCardList.setLayoutManager(manager);
        selectCardAdapter = new SelectCardAdapter(cardList);
        selectCardAdapter.bindToRecyclerView(ryCardList);

        if(getIntent() != null && getIntent().getExtras() != null){
            addMoney = getIntent().getExtras().getString(Constants.ADD_MONEY);
            cardType = getIntent().getExtras().getString(Constants.transferCardType);
            allCardEntity = (AllCardEntity) getIntent().getExtras().getSerializable(Constants.allCardEntity);
            setCardList(allCardEntity);
        }

        selectCardAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()){
                    case R.id.iv_card_delete:
                        CardEntity cardEntity = cardList.get(position);
                        if(cardEntity != null) {
                            showDeleteDialog(cardEntity);
                        }
                        break;
                    default:break;
                }
            }
        });

        selectCardAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                CardEntity cardEntity = cardList.get(position);
                if(cardEntity != null){
                    mCardEntity = cardEntity;
                    if("1".equals(cardEntity.expireFlag))return;
                    String cardType = mCardEntity.getCardOrganization();
                    if("MADA".equals(cardType.toUpperCase(Locale.ENGLISH))) {
                        showCvvDialog(cardEntity);
                    }else{
                        mPresenter.cardAddMoneyPreOrder(Constants.PAY_TYPE_CARD, AndroidUtils.getReqTransferMoney(addMoney),
                                UserInfoManager.getInstance().getCurrencyCode(), mCardEntity.getProtocolNo());
                    }
                }
            }
        });
    }

    private void showCvvDialog(CardEntity cardEntity) {
        enterCardCvvDialog = new EnterCardCvvDialog(mContext,cardEntity);
        enterCardCvvDialog.setContinueListener(new EnterCardCvvDialog.ContinueListener() {
            @Override
            public void clickContinue(String cvv) {
                mCardEntity.setCvv(cvv);
                mPresenter.cardAddMoneyPreOrder(Constants.PAY_TYPE_CARD, AndroidUtils.getReqTransferMoney(addMoney),
                        UserInfoManager.getInstance().getCurrencyCode(), mCardEntity.getProtocolNo());
            }
        });
        enterCardCvvDialog.showWithBottomAnim();
    }

    private void setCardList(AllCardEntity allCardEntity) {
        if(allCardEntity != null){
            List<CardEntity> madaCardInfoList = allCardEntity.madaCardInfoList;
            List<CardEntity> creditCardInfoList = allCardEntity.creditCardInfoList;
            cardList.clear();
            cardList.addAll(allCardEntity.bindCardInfos);
            if(cardList.size() == 0){
                llNoCard.setVisibility(View.VISIBLE);
                ryCardList.setVisibility(View.GONE);
            }else{
                llNoCard.setVisibility(View.GONE);
                ryCardList.setVisibility(View.VISIBLE);

                selectCardAdapter.setDataList(cardList);
            }
        }
    }

    private void showDeleteDialog(CardEntity cardEntity){
        cardDeleteDialog = new CardDeleteDialog(mContext,cardEntity);
        cardDeleteDialog.setConfirmListener(new CardDeleteDialog.ConfirmListener() {
            @Override
            public void clickConfrim(CardEntity cardEntity) {
                mPresenter.unbindCard(cardEntity.getProtocolNo());
            }
        });
        cardDeleteDialog.showWithBottomAnim();
    }

    //重写finish方法
    @Override
    public void finish() {
        super.finish();
        //finish时调用退出动画
        overridePendingTransition(R.anim.none, R.anim.dialog_exit);
    }

    @OnClick(R.id.tv_add_new)
    public void onClick() {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        mHashMap.put(Constants.ADD_MONEY, addMoney);
        mHashMap.put(Constants.transferCardType, cardType);
        ActivitySkipUtil.startAnotherActivity(this, AddNewCardActivity.class,mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getCardListSuccess(AllCardEntity allCardEntity) {
        setCardList(allCardEntity);
    }

    @Override
    public void unbindCardSuccess(unBindCardEntity unBindCardEntity) {
        if(cardDeleteDialog != null){
            cardDeleteDialog.dismiss();
        }

        mPresenter.getCardList(cardType);
    }

    @Override
    public void bindNewCardSuccess(CardBind3DSEntity result) {

    }

    @Override
    public void cardAddMoneyPreOrderSuccess(TransferPayEntity transferPayEntity) {
        if(enterCardCvvDialog != null){
            enterCardCvvDialog.dismiss();
        }
        if(transferPayEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            mHashMap.put(Constants.transferPayEntity, transferPayEntity);
            mHashMap.put(Constants.cardEntity, mCardEntity);
            ActivitySkipUtil.startAnotherActivity(this, ConfirmAddMoneyActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN);
        }
    }


    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddMoneyContract.CardListPresenter getPresenter() {
        return new CardListPresenter();
    }
}
