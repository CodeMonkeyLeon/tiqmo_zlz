package cn.swiftpass.wallet.tiqmo.module.transfer.adapter;

import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class TransferRelationAdapter extends BaseRecyclerAdapter<KycContactEntity> {

    private int currentPosition = -1;

    public void setPosition(int position) {
        this.currentPosition = position;
        notifyDataSetChanged();
    }

    public TransferRelationAdapter(@Nullable List<KycContactEntity> data) {
        super(R.layout.item_relation, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, KycContactEntity kycContactEntity, int position) {
        String name = kycContactEntity.getContactsName();
        baseViewHolder.setText(R.id.tv_user_name, name);
        baseViewHolder.setTextColor(R.id.tv_user_name,
                mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_9c9da1)));
        CheckBox cbAvatar = baseViewHolder.getView(R.id.cb_avatar);
        if (currentPosition == position) {
            baseViewHolder.setGone(R.id.iv_checked, false);
            cbAvatar.setAlpha(1f);
            baseViewHolder.setTextColor(R.id.tv_user_name, mContext.getColor(R.color.color_1da1f1));
        } else {
//            if (currentPosition != -1) {
                cbAvatar.setAlpha(0.3f);
                baseViewHolder.setGone(R.id.iv_checked, true);
                baseViewHolder.setTextColor(R.id.tv_user_name,
                        mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_70white_9c9da1)));
//            }
        }
        int headResId = kycContactEntity.headResId;
        cbAvatar.setButtonDrawable(headResId);

        baseViewHolder.addOnClickListener(R.id.con_choose);
    }

    public void changeTheme() {
        notifyDataSetChanged();
    }
}
