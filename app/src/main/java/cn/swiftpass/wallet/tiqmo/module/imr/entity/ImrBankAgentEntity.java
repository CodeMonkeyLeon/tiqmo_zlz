package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrBankAgentEntity extends BaseEntity {
    //银行/代理代码
    public String bankAgentCode;
    //银行/代理描述
    public String bankAgentDesc;
    //银行/代理分支明细
    public String bankAgentDetail;
}
