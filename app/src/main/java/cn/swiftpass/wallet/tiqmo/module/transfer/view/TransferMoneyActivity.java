package cn.swiftpass.wallet.tiqmo.module.transfer.view;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransFeeEntity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ContactInviteEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.BusinessParamEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.TransferContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.RequestTransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.presenter.TransferToContactPresenter;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AmountUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.MoneyInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.support.uxcam.UxcamHelper;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class TransferMoneyActivity extends BaseCompatActivity<TransferContract.TransferToContactPresenter> implements TransferContract.TransferToContactView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_top)
    TextView tvTop;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_add_money)
    TextView tvAddMoney;
    @BindView(R.id.ll_money)
    ConstraintLayout llMoney;
    @BindView(R.id.iv_from_avatar)
    RoundedImageView ivFromAvatar;
    @BindView(R.id.iv_to_avatar)
    RoundedImageView ivToAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.et_amount)
    EditTextWithDel etAmount;
    @BindView(R.id.et_note)
    CustomizeEditText etNote;
    @BindView(R.id.tv_transfer)
    TextView tvTransfer;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_send)
    LottieAnimationView ivSend;
    @BindView(R.id.iv_head_circle)
    ImageView headCircle;
    @BindView(R.id.et_purpose)
    EditTextWithDel etPurpose;
    @BindView(R.id.con_purpose)
    ConstraintLayout conPurpose;
    @BindView(R.id.iv_date_to_delete)
    ImageView dateToDelete;
    @BindView(R.id.tv_money_currency)
    TextView tvMoneyCurrency;

    private double valiableMoney, money;
    private KycContactEntity kycContactEntity;

    private double monthLimitMoney;

    private String transferType;
    private String payerName;

    private TransferEntity mTransferEntity;

    private String orderNo, payMethod, transAmount, exchangeRate, transCurrencyCode, transFees, vat;
    private String transferPurpose;
    private String transferPurposeDesc;

    private RiskControlEntity riskControlEntity;

    private static TransferMoneyActivity transferMoneyActivity;
    private BottomDialog bottomDialog;

    private String chatId;

    public static TransferMoneyActivity getTransferMoneyActivity() {
        return transferMoneyActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        transferMoneyActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_transfer_money;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        transferMoneyActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, headCircle, ivSend);
        UxcamHelper.getInstance().hideScreen(true);
//        etAmount.setRightText(LocaleUtils.getCurrencyCode(""));

        tvMoneyCurrency.setText(LocaleUtils.getCurrencyCode(""));
        tvTitle.setText(R.string.wtw_1);
        etAmount.getTlEdit().setHint(getString(R.string.wtw_22));
        etPurpose.getTlEdit().setHint(getString(R.string.IMR_new_10));
        etPurpose.setFocusableFalse();
        if (getIntent() != null && getIntent().getExtras() != null) {
            kycContactEntity = (KycContactEntity) getIntent().getExtras().get(Constants.CONTACT_ENTITY);
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            transferType = getIntent().getExtras().getString(Constants.sceneType);
            chatId = getIntent().getExtras().getString(Constants.chatId);
            if (userInfoEntity != null) {
                checkUser(userInfoEntity);
            }
            if (kycContactEntity != null) {
                tvPhone.setText(kycContactEntity.getPhone());
                String name = kycContactEntity.getContactsName();
                payerName = kycContactEntity.getContactsName();

//                tvName.setText(AndroidUtils.getContactName(name));
                if (!TextUtils.isEmpty(name)) {
                    tvName.setVisibility(View.VISIBLE);
                } else {
                    tvName.setVisibility(View.GONE);
                }
                tvName.setText(name);

                String avatarUrl = kycContactEntity.getHeadIcon();
                String gender = kycContactEntity.getSex();
                if (!TextUtils.isEmpty(avatarUrl)) {
//                    Glide.with(this).clear(ivToAvatar);
                    Glide.with(this)
                            .load(avatarUrl)
                            .centerCrop().autoClone()
                            .dontAnimate()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                            .into(ivToAvatar);
                } else {
                    Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivToAvatar);
                }
            }
        }

        if (ThemeUtils.isCurrentDark(mContext)) {
            ivSend.setAnimation("lottie/arrow_right_transfer.json");
        } else {
            ivSend.setAnimation("lottie/l_request_arrow.json");
        }

        etPurpose.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                AppClient.getInstance().getTransferPurpose("PURPOSE_OF_TRANSFER",new ResultCallback<BusinessParamEntity>(){

                    @Override
                    public void onResult(BusinessParamEntity businessParamEntity) {
                        showProgress(false);

                        if (businessParamEntity != null) {
                            List<SelectInfoEntity> selectInfoEntityList = businessParamEntity.businessParamList;
                            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_new_10), false, true);
                            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    SelectInfoEntity  selectInfoEntity = selectInfoEntityList.get(position);
                                    if (selectInfoEntity != null) {
                                        transferPurpose = selectInfoEntity.businessParamKey;
                                        transferPurposeDesc = selectInfoEntity.businessParamValue;
                                        etPurpose.getEditText().setText(transferPurposeDesc);
                                    }
                                    checkContinueStatus();
                                }
                            });
                            dialog.showNow(getSupportFragmentManager(), "getNationalitySuccess");
                        }
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                    }
                });
            }
        });

        //两位小数过滤
        etAmount.getEditText().setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(10)});

        etAmount.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = s.toString();
                amount = amount.replace(",", "");
                try {
                    if (TextUtils.isEmpty(amount)) {
                        tvMoneyCurrency.setVisibility(View.GONE);
                        dateToDelete.setVisibility(View.GONE);
                    }else{
                        tvMoneyCurrency.setVisibility(View.VISIBLE);
                        dateToDelete.setVisibility(View.VISIBLE);
                    }
                    checkMoney(amount);
                } catch (Exception e) {
                    tvTransfer.setEnabled(false);
                }
            }
        });

        etAmount.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                String amount = etAmount.getEditText().getText().toString().trim();
                amount = amount.replace(",", "");
                String fomatAmount = AmountUtil.dataFormat(amount);
                if (!hasFocus) {
                    if (TextUtils.isEmpty(amount)) {
                        tvAddMoney.setEnabled(false);
                    } else {
                        etAmount.getEditText().setText(fomatAmount);
                        checkMoney(amount);
                    }
                }
            }
        });

        mPresenter.getLimit(Constants.LIMIT_TRANSFER);
    }

    private void checkContinueStatus() {
        String amount = etAmount.getEditText().getText().toString().trim();
        amount = amount.replace(",", "");
        if (!TextUtils.isEmpty(amount)) {
            money = Double.parseDouble(amount);
            if (money == 0 || money > valiableMoney || money > monthLimitMoney || TextUtils.isEmpty(transferPurpose)) {
                tvTransfer.setEnabled(false);
            } else {
                tvTransfer.setEnabled(true);
            }
        }
    }

    private void checkMoney(String amount) {
        tvTransfer.setEnabled(false);
        etAmount.showErrorViewWithMsg("");
        if (TextUtils.isEmpty(amount)) {
            return;
        }
        money = Double.parseDouble(amount);
        if (money > monthLimitMoney) {
            tvAddMoney.setEnabled(false);
            etAmount.showErrorViewWithMsg(getString(R.string.wtw_25_3));
        } else if (money <= valiableMoney) {
            if (money > 0 || Constants.TYPE_TRANSFER_ST.equals(transferType)) {
                tvTransfer.setEnabled(true);
            }
            tvAddMoney.setEnabled(false);
            etAmount.showErrorViewWithMsg("");
        } else {
            tvAddMoney.setEnabled(true);
            etAmount.showErrorViewWithMsg(getString(R.string.wtw_25));
        }

        if (valiableMoney == 0) {
            tvAddMoney.setEnabled(true);
        }

        checkContinueStatus();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.UPDATE_BALANCE == event.getEventType()) {
            userInfoEntity = AppClient.getInstance().getUserManager().getUserInfo();
            if (userInfoEntity != null) {
                setBanlance(userInfoEntity);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUser(getUserInfoEntity());
        if (etAmount != null) {
            String amount = etAmount.getEditText().getText().toString().trim();
            if (!TextUtils.isEmpty(amount)) {
                amount = amount.replace(",", "");
                checkMoney(amount);
            }
        }
    }

    private void setBanlance(UserInfoEntity userInfoEntity) {
        String balance = userInfoEntity.getBalance();
        String balanceNumber = userInfoEntity.getBalanceNumber();
        if (!TextUtils.isEmpty(balanceNumber)) {
            try {
                balanceNumber = balanceNumber.replace(",", "");
                valiableMoney = Double.parseDouble(balanceNumber);
            } catch (Exception e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        if (tvAmount != null) {
            tvAmount.setText(balance);
        }
    }

    private void checkUser(UserInfoEntity userInfoEntity) {
        if (userInfoEntity != null) {
            String avatar = userInfoEntity.avatar;
            String gender = userInfoEntity.gender;
            setBanlance(userInfoEntity);
            tvCurrency.setText(LocaleUtils.getCurrencyCode(UserInfoManager.getInstance().getCurrencyCode()));
            if (!TextUtils.isEmpty(avatar)) {
                Glide.with(this).clear(ivFromAvatar);
                Glide.with(this)
                        .load(avatar)
                        .dontAnimate()
                        .format(DecodeFormat.PREFER_RGB_565)
                        .placeholder(ThemeSourceUtils.getDefAvatar(mContext, gender))
                        .into(ivFromAvatar);
            } else {
                Glide.with(this).load(ThemeSourceUtils.getDefAvatar(mContext, gender)).into(ivFromAvatar);
            }
        }
    }

    @OnClick({R.id.tv_add_money, R.id.tv_transfer, R.id.iv_back,R.id.iv_date_to_delete})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_money:
                AndroidUtils.jumpToAddMoneyActivity(this);
                break;
            case R.id.tv_transfer:
                String payMoney = etAmount.getText().toString().trim();
                payMoney = payMoney.replace(",", "");
                String remark = etNote.getText().toString().trim();
                //如果payerName为空  则取电话号码
                if (TextUtils.isEmpty(payerName)) {
                    payerName = kycContactEntity.getPhone();
                }
                if(!TextUtils.isEmpty(chatId)) {
                    RequestTransferEntity requestTransferEntity = new RequestTransferEntity();
                    requestTransferEntity.chatId = chatId;
                    requestTransferEntity.payerUserId = userInfoEntity.userId;
                    requestTransferEntity.callingCode = SpUtils.getInstance().getCallingCode();
                    requestTransferEntity.payerNum = userInfoEntity.getPhone();
                    requestTransferEntity.payeeUserId = kycContactEntity.getUserId();
                    requestTransferEntity.payeeNum = kycContactEntity.getRequestPhoneNumber();
                    requestTransferEntity.payeeAmount = AndroidUtils.getReqTransferMoney(payMoney);
                    requestTransferEntity.payeeCurrencyCode = UserInfoManager.getInstance().getCurrencyCode();
                    requestTransferEntity.remark = remark;
                    requestTransferEntity.sceneType = transferType;
                    requestTransferEntity.transTimeType = "";
                    requestTransferEntity.payeeNumberType = "1";
                    requestTransferEntity.transferPurpose = transferPurpose;
                    requestTransferEntity.transFees = "";
                    requestTransferEntity.vat = "";
                    requestTransferEntity.payerName = payerName;
                    mPresenter.chatTransferToContact(requestTransferEntity);
                }else {
                    mPresenter.transferToContact(userInfoEntity.userId, SpUtils.getInstance().getCallingCode(), userInfoEntity.getPhone(), kycContactEntity.getUserId(),
                            kycContactEntity.getRequestPhoneNumber(), AndroidUtils.getReqTransferMoney(payMoney), UserInfoManager.getInstance().getCurrencyCode(),
                            remark, transferType, "", "1", transferPurpose, "", "", payerName);
                }
                break;
            case R.id.iv_date_to_delete:
                etAmount.getEditText().setText("");
                break;
            default:
                break;
        }
    }

    @Override
    public void transferToContactSuccess(TransferEntity transferEntity) {
        if (transferEntity == null) return;
        this.mTransferEntity = transferEntity;
        riskControlEntity = transferEntity.riskControlInfo;
        if (riskControlEntity != null) {
            if (riskControlEntity.isOtp()) {
                HashMap<String, Object> mHashMaps = new HashMap<>(16);
                setTransferMap(mHashMaps);
                ActivitySkipUtil.startAnotherActivity(TransferMoneyActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                jumpToPwd(false);
            }else if(riskControlEntity.isNeedIvr()){
                jumpToPwd(true);
            }else{
                requestTransfer();
            }
        } else {
            requestTransfer();
//            jumpToPwd();
        }
    }

    @Override
    public void contactInviteSuccess(ContactInviteEntity contactInviteEntity) {

    }

    @Override
    public void contactInviteFail(String errCode, String errMsg) {

    }

    private void requestTransfer(){
        if (mTransferEntity != null) {
            orderNo = mTransferEntity.orderNo;
            payerName = mTransferEntity.payerName;
            payMethod = mTransferEntity.payMethod;
            transAmount = mTransferEntity.orderAmount;
            exchangeRate = mTransferEntity.exchangeRate;
            transCurrencyCode = mTransferEntity.orderCurrencyCode;
            transFees = mTransferEntity.transFees;
            vat = mTransferEntity.vat;
        }
        mPresenter.transferSurePay(transferType, orderNo, payMethod, transAmount,
                exchangeRate, transCurrencyCode, transFees, vat);
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.TRANSFER_ENTITY, mTransferEntity);
        mHashMap.put(Constants.sceneType, transferType);
        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_TRANSFER_CONTACT);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtw_1));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(TransferMoneyActivity.this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(TransferMoneyActivity.this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void transferToContactFail(String errCode, String errMsg) {
        if ("030204".equals(errCode) || "030207".equals(errCode)) {
            showExcessBeneficiaryDialog(errMsg);
        }else {
            etAmount.showErrorViewWithMsg(errMsg);
        }
    }
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void transferSurePaySuccess(TransferEntity transferEntity) {
        transferSurePaySuccess(this,transferType,transferEntity,true);
    }

    @Override
    public void transferSurePayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getTransferFeeSuccess(TransFeeEntity transFeeEntity) {

    }

    @Override
    public void getTransferFeeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        String monthLimitAmt = transferLimitEntity.monthLimitAmt;
        try {
            monthLimitMoney = AndroidUtils.getTransferMoneyNumber(monthLimitAmt);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity riskControlEntity) {

    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {

    }

    @Override
    public void splitBillSuccess(TransferEntity transferEntity) {

    }

    @Override
    public void splitBillFail(String errorCode, String errorMsg) {

    }

    @Override
    public void sendNotifySuccess(PayBillOrderInfoEntity result) {

    }

    @Override
    public void sendNotifyFail(String errorCode, String errorMsg) {

    }

    @Override
    public TransferContract.TransferToContactPresenter getPresenter() {
        return new TransferToContactPresenter();
    }
}
