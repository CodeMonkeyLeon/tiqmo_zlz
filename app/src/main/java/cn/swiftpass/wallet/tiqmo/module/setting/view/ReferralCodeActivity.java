package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zrq.spanbuilder.Spans;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;

public class ReferralCodeActivity extends BaseCompatActivity implements BaseView {

    @BindView(R.id.ll_save_content)
    LinearLayout llSaveContent;
    @BindView(R.id.tv_referral_code)
    TextView tvReferralCode;
    @BindView(R.id.tv_referral_code_des)
    TextView tvReferralCodeDes;
    @BindView(R.id.tv_download_two)
    TextView tvDownload;
    @BindView(R.id.tv_share_two)
    TextView tvShare;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_copy_referral_code)
    ImageView ivReferralCodeCopy;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;

    private String shareDesc;

    @Override
    protected int getLayoutID() {
        return R.layout.referral_code_activity;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        getReferralCode();
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivHeadCircle, ivBack);
        tvTitle.setText(R.string.ref_code_01);
    }

    private void getReferralCode() {
        showProgress(true);
        AppClient.getInstance().requestRefCode(new LifecycleMVPResultCallback<ReferralCodeEntity>(this) {
            @Override
            protected void onSuccess(ReferralCodeEntity result) {
                showProgress(false);
                refreshView(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                showTipDialog(errorMsg);
                showProgress(false);
            }
        });
    }

    private void refreshView(ReferralCodeEntity result) {
        if (result == null || (result.referralCodeDesc.length == 0 &&
                result.amountAndCurrencyInfo.length == 0)) {
            return;
        }
        shareDesc = result.shareDesc;
        String[] desc = checkArray(result.referralCodeDesc),
                amount = checkArray(result.amountAndCurrencyInfo);
        tvReferralCodeDes.setText(Spans.builder()
                .text(desc[0]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_373942))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"))
                .text(amount[0]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_373942))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"))
                .text(desc[1]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_373942))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"))
                .text(amount[1]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_373942))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf"))
                .text(desc[2]).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_a3a3a3_373942))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Regular.ttf":"fonts/SFProDisplay-Regular.ttf"))
                .build());
        tvReferralCode.setText(isEmpty(result.referralCode));
    }

    private String[] checkArray(String[] useReferralCodeDesc) {
        String[] temp;
        if (useReferralCodeDesc.length == 0) {
            temp = new String[]{" ", " ", " "};
        } else {
            temp = new String[3];
            for (int i = 0; i < 3; i++) {
                if (i < useReferralCodeDesc.length) {
                    temp[i] = isEmpty(useReferralCodeDesc[i]);
                } else {
                    temp[i] = " ";
                }
            }
        }
        return temp;
    }

    public String isEmpty(String s){
        return TextUtils.isEmpty(s) ? " " : s;
    }

    public boolean firstRejectPermission = true;

    @OnClick({R.id.tv_download_two, R.id.tv_share_two, R.id.iv_copy_referral_code, R.id.iv_back})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_download_two:
                saveImage(llSaveContent);
                break;
            case R.id.tv_share_two:
//                shareDesc = "https://cn.wallet.tiqmo/splash";
                downShareImage(llSaveContent, shareDesc);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_copy_referral_code:
                String code = tvReferralCode.getText().toString().trim();
                AndroidUtils.setClipboardText(mContext, code, false);
                showTipDialog(getString(R.string.ref_code_07));
                break;
            default:
                break;
        }
    }


    @Override
    public Object getPresenter() {
        return null;
    }

    @Override
    public boolean isAttachedToPresenter() {
        return true;
    }
}
