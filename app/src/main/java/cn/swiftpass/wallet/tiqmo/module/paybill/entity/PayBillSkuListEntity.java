package cn.swiftpass.wallet.tiqmo.module.paybill.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class PayBillSkuListEntity extends BaseEntity {

    public String billerId;
    //sku列表，sceneType=2或4时可用
    public List<PayBillSkuEntity> skuList = new ArrayList<>();
    //1拉取账单-单sku 2拉取账单-多sku，sku页面 3非拉取账单-单sku 4非拉取账单-多sku，plan页面
    public int sceneType;
    //sceneType=1或3时可用
    public String sku;
}
