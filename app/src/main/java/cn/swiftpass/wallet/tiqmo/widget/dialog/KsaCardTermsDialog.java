package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.ConfigEntity;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class KsaCardTermsDialog extends BottomDialog{

    private Context mContext;

    private ApproveListener approveListener;

    public void setApproveListener(ApproveListener a) {
        this.approveListener = a;
    }

    public KsaCardTermsDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }


    public interface ApproveListener {
        void clickApprove();
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_ksa_card_terms, null);
        TextView tvApprove = view.findViewById(R.id.tv_approve);
        WebView webView = view.findViewById(R.id.webView);
        webView.setBackgroundColor(0);
//        webView.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.bg_trust_device));
        webView.setBackgroundResource(R.drawable.bg_shape_white_all);
        WebSettings webseting = webView.getSettings();
        webseting.setDomStorageEnabled(true);

        webseting.setAllowFileAccess(true);
        webseting.setJavaScriptEnabled(true);

        webseting.setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.setWebViewClient(new WebViewClient());

        if (!ThemeUtils.isCurrentDark(mContext)) {
            ResourceHelper helper = ResourceHelper.getInstance(mContext);
            helper.setBackgroundResourceByAttr(tvApprove,R.attr.bg_btn_next_page_card);
        }

//        webView.setWebViewClient(new WebViewClient(){
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                if(ThemeUtils.isCurrentDark(mContext)) {
////                    webView.loadUrl("javascript:function getSub(){" +
////                            "document.getElementsByTagName('body')[0].style.webkitTextFillColor='#ffffff'" +
////                            "};getSub();");
//                    view.loadUrl("javascript:function modifyTextColor(){" +
//
//                            "document.getElementsByTagName('body')[0].style.webkitTextFillColor='#ffffff'" +
//
//                            "};modifyTextColor();");
//                }
//            }
//        });

        ConfigEntity configEntity = AppClient.getInstance().getUserManager().getConfig();
        if(configEntity != null) {
            if (configEntity.agreementURLs != null && configEntity.agreementURLs.size() > 0) {
                webView.loadUrl(configEntity.getAgreeUrl("2"));
            }
        }

        tvApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (approveListener != null) {
                    approveListener.clickApprove();
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            int screenHeight = AndroidUtils.getScreenHeight(mContext);
            int maxHeight = screenHeight*3/4;
            layoutParams.height = maxHeight;
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
