package cn.swiftpass.wallet.tiqmo.sdk.entity;

/**
 * Created by YZX on 2018年12月04日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class FIOEntity {
    private String fioId;

    public String getFioId() {
        return fioId;
    }

    public void setFioId(String fioId) {
        this.fioId = fioId;
    }
}
