package cn.swiftpass.wallet.tiqmo.module.register.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class NafathResultEntity extends BaseEntity {
    /**
     * 	认证url
     */
    public String authUrl;
    /**
     * 	S认证成功 P 未认证 F认证失败
     */
    public String authStatus;
    /**
     * 	1 未成年 2 id 过期 3id不匹配
     */
    public String failType;
    /**
     * S认证成功返回first name
     */
    public String firstName = "";

    public String birthday = "";

    public boolean isAuthSuccess(){
        return "S".equals(authStatus);
    }

    public boolean isAuthFail(){
        return "F".equals(authStatus);
    }
}
