package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by YZX on 2019年09月20日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class H5OrderEntity implements Parcelable {
    private String mchId;
    private String preId;
    private String nonceStr;
    private String paySign;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getPreId() {
        return preId;
    }

    public void setPreId(String preId) {
        this.preId = preId;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getPaySign() {
        return paySign;
    }

    public void setPaySign(String paySign) {
        this.paySign = paySign;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mchId);
        dest.writeString(this.preId);
        dest.writeString(this.nonceStr);
        dest.writeString(this.paySign);
    }

    public H5OrderEntity() {
    }

    protected H5OrderEntity(Parcel in) {
        this.mchId = in.readString();
        this.preId = in.readString();
        this.nonceStr = in.readString();
        this.paySign = in.readString();
    }

    public static final Creator<H5OrderEntity> CREATOR = new Creator<H5OrderEntity>() {
        @Override
        public H5OrderEntity createFromParcel(Parcel source) {
            return new H5OrderEntity(source);
        }

        @Override
        public H5OrderEntity[] newArray(int size) {
            return new H5OrderEntity[size];
        }
    };
}
