package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter
import cn.swiftpass.wallet.tiqmo.base.view.BaseView
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardSummaryEntity

class ChooseCardTypeContract {


    interface View : BaseView<Presenter> {


        fun getKsaCardSummarySuccess(result: KsaCardSummaryEntity?)


        fun getKsaCardSummaryFailed(
            errorCode: String?,
            errorMsg: String?
        )
    }


    interface Presenter : BasePresenter<View> {
        fun getKsaCardSummary(
            cardType: String?,
            cardProductType: String?
        )
    }


}