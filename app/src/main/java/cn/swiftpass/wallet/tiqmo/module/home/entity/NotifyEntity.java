package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class NotifyEntity extends BaseEntity {

    public String msgSubject;
    public String msgResult;
    public String orderNo;
    public String receiverPhone;

}
