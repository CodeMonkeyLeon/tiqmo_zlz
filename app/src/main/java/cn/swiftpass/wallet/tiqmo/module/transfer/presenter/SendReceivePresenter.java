package cn.swiftpass.wallet.tiqmo.module.transfer.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.contract.SendReceiveContract;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.KycContactEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveTransferDetail;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SendReceivePresenter implements SendReceiveContract.SendReceivePresenter {

    private SendReceiveContract.SendReceiveView mView;

    @Override
    public void attachView(SendReceiveContract.SendReceiveView sendReceiveView) {
        mView = sendReceiveView;
    }

    @Override
    public void detachView() {
        mView = null;
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(mView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                if (mView == null) {
                    return;
                }
                mView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                if (mView == null) {
                    return;
                }
                mView.showError(errorCode, errorMsg);
            }
        });
    }


    @Override
    public void getTransferPeopleDetail(String groupId, String isGroup) {
        AppClient.getInstance().getTransferDetail(groupId, isGroup, new LifecycleMVPResultCallback<SendReceiveTransferDetail>(mView,false) {
            @Override
            protected void onSuccess(SendReceiveTransferDetail result) {
                mView.getTransferPeopleDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.showError(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void SendReceiveDetail(String groupId, String tradeType, String isGroup, int pageNum, int pageSize) {
        AppClient.getInstance().SendReceiveDetail(groupId, tradeType, isGroup, pageNum,pageSize, new LifecycleMVPResultCallback<SendReceiveDetailListEntity>(mView, true) {
            @Override
            protected void onSuccess(SendReceiveDetailListEntity result) {
                mView.SendReceiveDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.SendReceiveDetailFail(errorCode, errorMsg);
            }
        });
    }
}
