package cn.swiftpass.wallet.tiqmo.module.imr.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseDialogFragmentN;
import cn.swiftpass.wallet.tiqmo.module.imr.adapter.SelectInfoAdapter;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.SideBar;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;

public class ImrInfoSelectDialog extends BaseDialogFragmentN {

    private static ImrInfoSelectDialog imrInfoSelectDialog;
    TextView mainTitle;
    SideBar sideBar;
    EditTextWithDel etSearch;
    RecyclerView ryInfoList;
    LinearLayout llContent;
    LinearLayout llNoResult;
    List<SelectInfoEntity> selectInfoList = new ArrayList<>();
    List<SelectInfoEntity> filterInfoList = new ArrayList<>();
    String title;
    //1 Province 2 City
    private String message;
    private OnItemClickListener onItemClickListener;

    private boolean sideBarVisible, searchVisible;

    private String searchString;

    SelectInfoAdapter adapter;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private ImrInfoSelectDialog() {
    }

    public static ImrInfoSelectDialog getInstance(List<SelectInfoEntity> data, String title) {
        if (imrInfoSelectDialog == null) {
            imrInfoSelectDialog = new ImrInfoSelectDialog();
        }
        imrInfoSelectDialog.selectInfoList = data;
        imrInfoSelectDialog.title = title;
        return imrInfoSelectDialog;
    }

    public static ImrInfoSelectDialog getInstance(List<SelectInfoEntity> data, String title, boolean sideBarVisible, boolean searchVisible) {
        if (imrInfoSelectDialog == null) {
            imrInfoSelectDialog = new ImrInfoSelectDialog();
        }
        imrInfoSelectDialog.selectInfoList = data;
        imrInfoSelectDialog.title = title;
        imrInfoSelectDialog.sideBarVisible = sideBarVisible;
        imrInfoSelectDialog.searchVisible = searchVisible;
        return imrInfoSelectDialog;
    }

    public static ImrInfoSelectDialog getInstance(List<SelectInfoEntity> data, String title, boolean sideBarVisible, boolean searchVisible, String message) {
        if (imrInfoSelectDialog == null) {
            imrInfoSelectDialog = new ImrInfoSelectDialog();
        }
        imrInfoSelectDialog.selectInfoList = data;
        imrInfoSelectDialog.title = title;
        imrInfoSelectDialog.message = message;
        imrInfoSelectDialog.sideBarVisible = sideBarVisible;
        imrInfoSelectDialog.searchVisible = searchVisible;
        return imrInfoSelectDialog;
    }


    public void setData(ArrayList<SelectInfoEntity> data, String title) {

    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_imr_info_select;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        mainTitle = view.findViewById(R.id.tv_select_main_title);
        ryInfoList = view.findViewById(R.id.rv_info_list);
        sideBar = view.findViewById(R.id.id_sideBar);
        etSearch = view.findViewById(R.id.et_search);
        llContent = view.findViewById(R.id.ll_content);
        llNoResult = view.findViewById(R.id.ll_no_result);
        sideBar.setVisibility(sideBarVisible ? View.VISIBLE : View.GONE);
        etSearch.setVisibility(searchVisible ? View.VISIBLE : View.GONE);
        etSearch.getTlEdit().setHint(mContext.getString(R.string.wtw_15));
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(RecyclerView.VERTICAL);
        ryInfoList.setLayoutManager(manager);

        //如果需要显示A-Z索引  则排序
        if (sideBarVisible) {
            int size = selectInfoList.size();
            for (int i = 0; i < size; i++) {
                SelectInfoEntity selectInfoEntity = selectInfoList.get(i);
                if (!TextUtils.isEmpty(selectInfoEntity.businessParamValue)) {
                    selectInfoEntity.setSortLetterName(selectInfoEntity.businessParamValue);
                }
            }
//            AndroidUtils.comparatorAZ(selectInfoList);
        }

        if (searchVisible) {
            ViewGroup.LayoutParams lp = llContent.getLayoutParams();
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = AndroidUtils.dip2px(mContext, 450);
            llContent.setLayoutParams(lp);
        }

        if (ryInfoList.getAdapter() == null) {
            adapter = new SelectInfoAdapter(selectInfoList);
            adapter.setSearchVisible(searchVisible);
            ryInfoList.setAdapter(adapter);
        } else {
            adapter = (SelectInfoAdapter) ryInfoList.getAdapter();
            adapter.setSearchVisible(searchVisible);
            adapter.setDataList(selectInfoList);
        }
        adapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                int index = position;
                if (onItemClickListener != null) {
                    if (filterInfoList != null && filterInfoList.size() > 0) {
                        for (int i = 0; i < selectInfoList.size(); i++) {
                            if (filterInfoList.get(position).businessParamValue.equals(selectInfoList.get(i).businessParamValue)) {
                                index = i;
                                break;
                            }
                        }
                    }
                    onItemClickListener.onItemClick(index);
                }
                imrInfoSelectDialog.dismiss();
            }
        });

        sideBar.setHideSpecial(true);
        //设置右侧SideBar触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (manager != null) {
                        manager.scrollToPositionWithOffset(position, 0);
                    }
                }
            }
        });

        etSearch.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etSearch.hideErrorView();
                searchString = s.toString();
                if (selectInfoList != null && selectInfoList.size() > 0) {
                    searchFilterWithAllList(searchString);
                } else {
//                    isSearchEmpty = true;
                }
            }
        });

        mainTitle.setText(title);
    }

    private void searchFilterWithAllList(String filterStr) {
        try {
            llNoResult.setVisibility(View.GONE);
            filterInfoList.clear();
            //去除空格的匹配规则
//            filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
            filterStr = filterStr.trim();
            if (TextUtils.isEmpty(filterStr)) {
                sideBar.setVisibility(View.VISIBLE);
                filterInfoList.clear();
                adapter.setDataList(selectInfoList);
                return;
            } else {
                sideBar.setVisibility(View.GONE);
            }
            if (selectInfoList != null && selectInfoList.size() > 0) {
                int size = selectInfoList.size();
                for (int i = 0; i < size; i++) {
                    SelectInfoEntity selectInfoEntity = selectInfoList.get(i);
                    String name = selectInfoEntity.businessParamValue;
//                    String pinyinName = Pinyin.toPinyin(name, "");
                    if (name.toLowerCase(Locale.ENGLISH).startsWith(filterStr.toLowerCase(Locale.ENGLISH)) || name.toLowerCase(Locale.ENGLISH).contains(filterStr.toLowerCase(Locale.ENGLISH))) {
                        filterInfoList.add(selectInfoEntity);
                    }
                }
                if (filterInfoList.isEmpty()) {
                    etSearch.showErrorViewWithMsg(message);
                }
                adapter.setDataList(filterInfoList);
                if (filterInfoList.size() == 0) {
                    llNoResult.setVisibility(View.VISIBLE);
                }else {
                    llNoResult.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---" + e + "---");
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        onItemClickListener = null;
        imrInfoSelectDialog = null;
        etSearch.getEditText().setText("");
        AndroidUtils.hideKeyboard(etSearch.getEditText());
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
