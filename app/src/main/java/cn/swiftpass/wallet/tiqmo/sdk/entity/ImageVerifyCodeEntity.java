package cn.swiftpass.wallet.tiqmo.sdk.entity;

public class ImageVerifyCodeEntity {
    private String base64;
    private int codeLength = 4;

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public int getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(int codeLength) {
        this.codeLength = codeLength;
    }
}
