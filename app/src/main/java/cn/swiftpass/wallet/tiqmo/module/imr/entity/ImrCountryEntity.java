package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrCountryEntity extends BaseEntity {
    //国家类型
    public String countryType;
    //国家代码
    public String countryCode;
    //国家名称
    public String countryName;
    //币种代码
    public String currencyCode;
    //区号
    public String callingCode;
    //银行代码
//    public String bankCode;
    //前端页面展示地址&ID的类型，* 0：都不展示（跳过此页面），1：只展示Address页面，2：只展示ID页面，3：展示Address&Id页面
    public String bankAddrIdType;
    //代理代码
//    public String agentCode;
    //前端页面展示地址&ID的类型，* 0：都不展示（跳过此页面），1：只展示Address页面，2：只展示ID页面，3：展示Address&Id页面
    public String agentAddrIdType;
    //Iban前缀,没有匹配到时返回空
    public String ibanPreCode;
    //Iban长度,没有匹配到时返回空
    public String ibanLength;
    //国家Logo
    public String countryLogo;
    //	是否支持IBAN，Y：支持，N：不支持
    public String ibanSupport;
    //是否支持ACCOUNT，Y：支持，N：不支持
    public String accountSupport;

    //银行模式
    public String bankMode;
    //银行模式支持币种
    public List<String> bankSupportCurrencyCodes = new ArrayList<>();
    //机构模式
    public String agentMode;
    //机构模式支持币种
    public List<String> agentSupportCurrencyCodes = new ArrayList<>();
    /**
     *\"paymentMethodList\":[{\"paymentMode\":\"BT\",\"paymentMethodLogo\":\"\",\"paymentMethodName\":\"Bank Account\",
     * \"defaultPaymentOrgCode\":\"TZ51\",\"supportCurrencyList\":[\"TZS\"],\"channelCode\":\"TransFast\"}
     */

    public List<ImrPaymentEntity> paymentMethodList = new ArrayList<>();
}
