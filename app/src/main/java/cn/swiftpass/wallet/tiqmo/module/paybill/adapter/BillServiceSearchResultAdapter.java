package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillerEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.interfaces.OnBillServiceSearchResultItemClickListener;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class BillServiceSearchResultAdapter extends BaseRecyclerAdapter<PayBillerEntity> {


    private OnBillServiceSearchResultItemClickListener mOnItemClickListener;

    public void setOnSearchResultItemClickListener(OnBillServiceSearchResultItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    public BillServiceSearchResultAdapter(@Nullable List<PayBillerEntity> data) {
        super(R.layout.item_paybill_select_biller, data);
    }


    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillerEntity payBillerEntity, int position) {
        try {
            baseViewHolder.setText(R.id.tv_biller_name, payBillerEntity.billerName);
            baseViewHolder.setText(R.id.tv_biller_desc, payBillerEntity.billerDesc);
            RoundedImageView ivBiller = baseViewHolder.getView(R.id.iv_biller);
            Glide.with(mContext)
                    .load(payBillerEntity.imgUrl)
                    .dontAnimate()
                    .format(DecodeFormat.PREFER_RGB_565)
                    .placeholder(ThemeSourceUtils.getSourceID(mContext, R.attr.utilities))
                    .into(ivBiller);


            View itemView = baseViewHolder.getView(R.id.id_cl_item);
            if (itemView != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onBillServiceSearchResultItemClick(position, payBillerEntity);
                        }
                    }
                });
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }
}
