package cn.swiftpass.wallet.tiqmo.sdk.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

/**
 * Created by 叶智星 on 2018年09月27日.
 * 每一个不曾起舞的日子，都是对生命的辜负。
 */
public class OrderResultEntity implements Parcelable {
    private String payResult;
    private String orderNo;
    private String transTime;
    private String payeeName;
    private BigDecimal orderAmount;
    private String orderCurrencyCode;
    private BigDecimal payerAmount;
    private String payerCurrencyCode;
    private BigDecimal exchangeRate;
    private String payMethod;
    private String flowUseStatus;
    private String orderStatus;
    private String payMethodDesc;
    private String payeePortraitUrl;
    private String customerImgUrl;
    private String remark;
    private String payeeRemark;
    private String phoneNumber;
    private String orderDesc;
    private CurrencyEntity payerCurrencyInfo;
    private CurrencyEntity orderCurrencyInfo;

    public String getPayResult() {
        return payResult;
    }

    public void setPayResult(String payResult) {
        this.payResult = payResult;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderCurrencyCode() {
        return orderCurrencyCode;
    }

    public void setOrderCurrencyCode(String orderCurrencyCode) {
        this.orderCurrencyCode = orderCurrencyCode;
    }

    public BigDecimal getPayerAmount() {
        return payerAmount;
    }

    public void setPayerAmount(BigDecimal payerAmount) {
        this.payerAmount = payerAmount;
    }

    public String getPayerCurrencyCode() {
        return payerCurrencyCode;
    }

    public void setPayerCurrencyCode(String payerCurrencyCode) {
        this.payerCurrencyCode = payerCurrencyCode;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal rate) {
        this.exchangeRate = rate;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public String getFlowUseStatus() {
        return flowUseStatus;
    }

    public void setFlowUseStatus(String flowUseStatus) {
        this.flowUseStatus = flowUseStatus;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPayMethodDesc() {
        return payMethodDesc;
    }

    public void setPayMethodDesc(String payMethodDesc) {
        this.payMethodDesc = payMethodDesc;
    }

    public String getPayeePortraitUrl() {
        return payeePortraitUrl;
    }

    public void setPayeePortraitUrl(String payeePortraitUrl) {
        this.payeePortraitUrl = payeePortraitUrl;
    }

    public String getCustomerImgUrl() {
        return customerImgUrl;
    }

    public void setCustomerImgUrl(String customerImgUrl) {
        this.customerImgUrl = customerImgUrl;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phone) {
        this.phoneNumber = phone;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public CurrencyEntity getPayerCurrencyInfo() {
        return payerCurrencyInfo;
    }

    public void setPayerCurrencyInfo(CurrencyEntity payerCurrencyInfo) {
        this.payerCurrencyInfo = payerCurrencyInfo;
    }

    public CurrencyEntity getOrderCurrencyInfo() {
        return orderCurrencyInfo;
    }

    public void setOrderCurrencyInfo(CurrencyEntity orderCurrencyInfo) {
        this.orderCurrencyInfo = orderCurrencyInfo;
    }

    public String getPayeeRemark() {
        return payeeRemark;
    }

    public void setPayeeRemark(String payeeRemark) {
        this.payeeRemark = payeeRemark;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.payResult);
        dest.writeString(this.orderNo);
        dest.writeString(this.transTime);
        dest.writeString(this.payeeName);
        dest.writeSerializable(this.orderAmount);
        dest.writeString(this.orderCurrencyCode);
        dest.writeSerializable(this.payerAmount);
        dest.writeString(this.payerCurrencyCode);
        dest.writeSerializable(this.exchangeRate);
        dest.writeString(this.payMethod);
        dest.writeString(this.flowUseStatus);
        dest.writeString(this.orderStatus);
        dest.writeString(this.payMethodDesc);
        dest.writeString(this.payeePortraitUrl);
        dest.writeString(this.customerImgUrl);
        dest.writeString(this.remark);
        dest.writeString(this.payeeRemark);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.orderDesc);
    }

    public OrderResultEntity() {
    }

    protected OrderResultEntity(Parcel in) {
        this.payResult = in.readString();
        this.orderNo = in.readString();
        this.transTime = in.readString();
        this.payeeName = in.readString();
        this.orderAmount = (BigDecimal) in.readSerializable();
        this.orderCurrencyCode = in.readString();
        this.payerAmount = (BigDecimal) in.readSerializable();
        this.payerCurrencyCode = in.readString();
        this.exchangeRate = (BigDecimal) in.readSerializable();
        this.payMethod = in.readString();
        this.flowUseStatus = in.readString();
        this.orderStatus = in.readString();
        this.payMethodDesc = in.readString();
        this.payeePortraitUrl = in.readString();
        this.customerImgUrl = in.readString();
        this.remark = in.readString();
        this.payeeRemark = in.readString();
        this.phoneNumber = in.readString();
        this.orderDesc = in.readString();
        this.payerCurrencyInfo = in.readParcelable(CurrencyEntity.class.getClassLoader());
        this.orderCurrencyInfo = in.readParcelable(CurrencyEntity.class.getClassLoader());
    }

    public static final Creator<OrderResultEntity> CREATOR = new Creator<OrderResultEntity>() {
        @Override
        public OrderResultEntity createFromParcel(Parcel source) {
            return new OrderResultEntity(source);
        }

        @Override
        public OrderResultEntity[] newArray(int size) {
            return new OrderResultEntity[size];
        }
    };
}
