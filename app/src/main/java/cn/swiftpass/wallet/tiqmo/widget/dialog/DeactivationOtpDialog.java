package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.RegisterOtpEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.support.SystemInitManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SMSReceiver;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.VerifyPwdCodeView;

public class DeactivationOtpDialog extends BottomDialog implements SMSReceiver.OnReceivedMessageListener {

    private Context mContext;
    private static DeactivationOtpDialog deactivationOtpDialog = null;

    private String phone;

    TextView tvOtpSend;
    TextView tvErrorOtp;
    TextView tvSendMsg;
    TextView tvShowTimeExpire;
    TextView tvOtpTime;
    LinearLayout llOtpTime;
    LinearLayout llOtpPhone;
    VerifyPwdCodeView veriftOtp;
    boolean mReceiverTag = false;   //广播接受者标识
    private SMSReceiver receiver = new SMSReceiver();

    public DeactivationOtpDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    public static DeactivationOtpDialog getInstance(Context context) {
        deactivationOtpDialog = new DeactivationOtpDialog(context);
        return deactivationOtpDialog;
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_deactivation_otp, null);

        tvOtpSend = view.findViewById(R.id.tv_otp_send);
        llOtpPhone = view.findViewById(R.id.ll_otp_phone);
        veriftOtp = view.findViewById(R.id.verift_otp);
        tvErrorOtp = view.findViewById(R.id.tv_error_otp);
        tvOtpTime = view.findViewById(R.id.tv_otp_time);
        llOtpTime = view.findViewById(R.id.ll_otp_time);
        tvSendMsg = view.findViewById(R.id.tv_send_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvShowTimeExpire = view.findViewById(R.id.tv_show_time_expire);

        //动态注册广播接收者且设置为最大优先级
        registerReceiver();
        receiver.setOnReceivedMessageListener(this);

        UserInfoEntity user = AppClient.getInstance().getUserManager().getUserInfo();
        if (user != null) {
            phone = user.phone;
        }

        tvOtpSend.setText(AndroidUtils.addStrToPhone(SpUtils.getInstance().getCallingCode() + " " + AndroidUtils.addStarPhone(phone)));

        veriftOtp.setOnCodeFinishListener(new VerifyPwdCodeView.OnCodeFinishListener() {
            @Override
            public void onTextChange(View view, String content) {

            }

            @Override
            public void onComplete(View view, String content) {
                unregisterReceiver();
                showProgress(true);
                AppClient.getInstance().checkVerifyCodeOfType(SpUtils.getInstance().getCallingCode(), phone, content, Constants.DEACTIVATION_OTP_TYPE,"", new ResultCallback<Void>() {
                    @Override
                    public void onResult(Void result) {
                        showProgress(false);
                        tvErrorOtp.setVisibility(View.GONE);
                        AppClient.getInstance().getUserManager().stopAccount(new ResultCallback<Void>() {
                            @Override
                            public void onResult(Void result) {
                                AppClient.getInstance().logout(new ResultCallback<Void>() {
                                    @Override
                                    public void onResult(Void result) {

                                    }

                                    @Override
                                    public void onFailure(String code, String error) {

                                    }
                                });
                                ProjectApp.clearUserInfo();
                                RegisterActivity.startActivityOfNewTaskType(mContext);
                                ProjectApp.removeAllTaskExcludeLoginStack();
                            }

                            @Override
                            public void onFailure(String errorCode, String errorMsg) {

                            }
                        });
                    }

                    @Override
                    public void onFailure(String errorCode, String errorMsg) {
                        showProgress(false);
                        tvErrorOtp.setVisibility(View.VISIBLE);
                        if (!TextUtils.isEmpty(errorMsg)) {
                            tvErrorOtp.setText(errorMsg);
                        }
                        //出错则显示红色下划线
                        clearOtpInput();
                    }
                });
            }
        });

        tvSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvErrorOtp.setVisibility(View.GONE);
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                registerReceiver();
                sendOtp(phone);
            }
        });

        sendOtp(phone);

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (deactivationOtpDialog != null) {
                    deactivationOtpDialog = null;
                }
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    private void sendOtp(String phone) {
        AppClient.getInstance().obtainVerifyCodeOfType(SpUtils.getInstance().getCallingCode(), phone, Constants.DEACTIVATION_OTP_TYPE,new ArrayList<>(), new ResultCallback<RegisterOtpEntity>() {
            @Override
            public void onResult(RegisterOtpEntity response) {
                try {
                    long expiredTime = response.expiredTime;
                    Boolean isExpired = response.verifyCodeExpired;
                    if (isExpired != null && !isExpired) {
                        tvSendMsg.setEnabled(false);
                    } else {
                        tvSendMsg.setEnabled(true);
                        tvSendMsg.setFocusable(true);
                        tvSendMsg.setFocusableInTouchMode(true);
                    }
                    setResetTime(true, phone, (int) expiredTime);
                    clearOtpInput();
                } catch (Exception e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                onOtpActionFailed(errorCode, errorMsg, true);
            }
        });
    }

    private void onOtpActionFailed(final String errorCode, String errorMsg, final boolean isSend) {
        tvErrorOtp.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(errorMsg)) {
            tvErrorOtp.setText(errorMsg);
        }
        veriftOtp.setEmpty();
        tvSendMsg.setClickable(true);
        tvSendMsg.setEnabled(true);
        tvSendMsg.setFocusable(true);
        tvSendMsg.setFocusableInTouchMode(true);

    }

    private void clearOtpInput() {
        veriftOtp.setEmpty();
//        tvErrorOtp.setVisibility(View.GONE);
    }

    public void setResetTime(boolean isReset, String phoneNo, int time) {
        if (isReset) {
            tvSendMsg.setClickable(false);
            tvSendMsg.setEnabled(false);
            AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, time, new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg == null) {
                        return;
                    }
                    if (millisUntilFinished == 0) {
                        tvShowTimeExpire.setText(mContext.getString(R.string.common_21));
                        tvOtpTime.setVisibility(View.GONE);
                    } else if (millisUntilFinished < 60 && millisUntilFinished > 0) {
                        tvShowTimeExpire.setText(mContext.getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        if (LocaleUtils.isRTL(mContext)) {
                            tvOtpTime.setText("s " + millisUntilFinished);
                        } else {
                            tvOtpTime.setText(millisUntilFinished + " s");
                        }
                    } else {
                        tvShowTimeExpire.setText(mContext.getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        int second = millisUntilFinished % 60;
                        if (second < 10) {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":0" + second);
                        } else {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":" + second);
                        }
                    }
                    tvSendMsg.setClickable(false);
                    tvSendMsg.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg == null) {
                        return;
                    }
                    tvShowTimeExpire.setText(mContext.getString(R.string.common_21));
                    tvOtpTime.setVisibility(View.GONE);
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setFocusable(true);
                    tvSendMsg.setFocusableInTouchMode(true);
                }
            });
        } else {
            AdvancedCountdownTimer.getInstance().startCountDown(phoneNo, SystemInitManager.getInstance().getSystemInitEntity().getOtpIntervalTime(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg == null) {
                        return;
                    }
                    if (millisUntilFinished == 0) {
                        tvShowTimeExpire.setText(mContext.getString(R.string.common_21));
                        tvOtpTime.setVisibility(View.GONE);
                    } else if (millisUntilFinished < 60 && millisUntilFinished > 0) {
                        tvShowTimeExpire.setText(mContext.getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        tvOtpTime.setText(millisUntilFinished + "s");
                    } else {
                        tvShowTimeExpire.setText(mContext.getString(R.string.OtpVerification_SU_0003_1_D_4));
                        tvOtpTime.setVisibility(View.VISIBLE);
                        int second = millisUntilFinished % 60;
                        if (second < 10) {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":0" + second);
                        } else {
                            tvOtpTime.setText(millisUntilFinished / 60 + ":" + second);
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg == null) {
                        return;
                    }
                    tvShowTimeExpire.setText(mContext.getString(R.string.common_21));
                    tvOtpTime.setVisibility(View.GONE);
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setFocusable(true);
                    tvSendMsg.setFocusableInTouchMode(true);
                }
            });
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
       unregisterReceiver();
    }

    private void registerReceiver() {
        if (!mReceiverTag){     //在注册广播接受者的时候 判断是否已被注册,避免重复多次注册广播
            //动态注册广播接收者且设置为最大优先级
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.provider.Telephony.SMS_RECEIVED" );
            filter.setPriority(Integer.MAX_VALUE);//设置动态优先级为最大，1000应该不是最大
            getContext().registerReceiver(receiver,filter);
        }
    }
    //注销广播
    private void unregisterReceiver() {
        if (mReceiverTag) {   //判断广播是否注册
            mReceiverTag = false;   //Tag值 赋值为false 表示该广播已被注销
            getContext().unregisterReceiver(receiver);   //注销广播
            mReceiverTag = false;
        }

    }



    @Override
    public void onReceived(String message) {
        veriftOtp.setCode(message);
    }
}

