package cn.swiftpass.wallet.tiqmo.support.utils;

import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.FragmentActivity;

import java.util.concurrent.Executor;

import javax.crypto.Cipher;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.PreVerifyEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.manager.BiometricManager;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

/**
 * 生物认证单例类
 */
public class BiometricInstance {

    private BiometricInstance() {
    }

    private static class BiometricHolder {
        public static BiometricInstance biometricInstance = new BiometricInstance();
    }

    public static BiometricInstance getInstance() {
        return BiometricInstance.BiometricHolder.biometricInstance;
    }

    private Handler handler = new Handler();

    private Executor executor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };

    public void showBiometricPrompt(FragmentActivity fragmentActivity, boolean isLogin, BiometricListener biometricListener, boolean isOpen) {
        BiometricPrompt.PromptInfo promptInfo =
                new BiometricPrompt.PromptInfo.Builder()
                        .setTitle(fragmentActivity.getString(isLogin ? R.string.common_40 : R.string.common_37)) //设置大标题
                        .setSubtitle(fragmentActivity.getString(isLogin ? R.string.common_41 : R.string.common_38)) // 设置标题下的提示
                        .setNegativeButtonText(fragmentActivity.getString(R.string.common_6)) //设置取消按钮
                        .build();
        //需要提供的参数callback
        BiometricPrompt biometricPrompt = new BiometricPrompt(fragmentActivity,
                executor, new BiometricPrompt.AuthenticationCallback() {
            //各种异常的回调
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(fragmentActivity,
                        fragmentActivity.getString(R.string.common_42) + ": " + errString, Toast.LENGTH_SHORT)
                        .show();
                biometricListener.onBiometricError();
            }

            //认证成功的回调
            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                BiometricPrompt.CryptoObject authenticatedCryptoObject =
                        result.getCryptoObject();
                Toast.makeText(fragmentActivity, fragmentActivity.getString(R.string.common_43),
                        Toast.LENGTH_SHORT)
                        .show();
                preVerifyFio(biometricListener, isLogin, isOpen);
            }

            //认证失败的回调
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(fragmentActivity, fragmentActivity.getString(R.string.common_44),
                        Toast.LENGTH_SHORT)
                        .show();
                biometricListener.onBiometricError();
            }
        });
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            FingerPrintHelper mFingerPrintHelper = FingerPrintHelper.newInstance(fragmentActivity);
            if (mFingerPrintHelper == null) {
                return;
            }
            String mSecretKeyName = AppClient.getInstance().getUserID() + (isLogin ? BiometricManager.FINGERPRINT_SECRET_KEY_LOGIN : BiometricManager.FINGERPRINT_SECRET_KEY_PAYMENT);
            Cipher cipher = mFingerPrintHelper.getCipherIfFingerPrintNoChange(mSecretKeyName);
            if (cipher == null) {
                cipher = mFingerPrintHelper.generateNewSecretKey(mSecretKeyName);
                if (cipher == null) {
                    LogUtil.e("cipher == null");
                    return;
                }
            }
            BiometricPrompt.CryptoObject crypto = new BiometricPrompt.CryptoObject(cipher);
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo, crypto);
        } else {
            // 显示认证对话框
            biometricPrompt.authenticate(promptInfo);
        }
    }

    private void preVerifyFio(BiometricListener biometricListener, boolean isLogin, boolean isOpen) {
        AppClient.getInstance().getBiometricManager().preVerify(isLogin ? Constants.FIO_TYPE_LOGIN : Constants.FIO_TYPE_LOGIN, new ResultCallback<PreVerifyEntity>() {
            @Override
            public void onResult(PreVerifyEntity result) {
                verifyFio(result.getPreAuthId(), biometricListener, isLogin, isOpen);
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                if (biometricListener != null) {
                    biometricListener.onBiometricFail(errorMsg);
                }
            }
        });
    }

    private void verifyFio(String preAuthID, BiometricListener biometricListener, boolean isLogin, boolean isOpen) {
        AppClient.getInstance().getBiometricManager().verify(preAuthID, new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {
                if (isLogin) {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(isOpen);
                } else {
                    AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(isOpen);
                }
                if (biometricListener != null) {
                    biometricListener.onBiometricSuccess();
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                if (biometricListener != null) {
                    biometricListener.onBiometricFail(errorMsg);
                }
            }
        });
    }

    /**
     * 当没有打开生物认证时  需要调该注册方法
     */
    public void registerBiometric(FragmentActivity fragmentActivity, boolean isLogin, boolean isOpen, BiometricListener biometricListener) {
        AppClient.getInstance().getBiometricManager().registerFingerprintPayment(new ResultCallback<Void>() {
            @Override
            public void onResult(Void result) {
                showBiometricPrompt(fragmentActivity, isLogin, biometricListener, isOpen);
            }

            @Override
            public void onFailure(String code, String error) {
                biometricListener.onBiometricFail(error);
            }
        });
    }

    /**
     * 当已经打开过APP的生物认证  点击face控件时  调用该方法
     */
    public void clickWithFio(FragmentActivity fragmentActivity, boolean isLogin, boolean isOpen, BiometricListener biometricListener) {
        FingerPrintHelper helper = FingerPrintHelper.newInstance(fragmentActivity);
        String keyName = AppClient.getInstance().getUserID() + (isLogin ? BiometricManager.FINGERPRINT_SECRET_KEY_LOGIN : BiometricManager.FINGERPRINT_SECRET_KEY_PAYMENT);
        //判断指纹是否发生变化等等
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            if (helper != null && helper.getCipherIfFingerPrintNoChange(keyName) != null) {
                showBiometricPrompt(fragmentActivity, isLogin, biometricListener, isOpen);
                return;
            }
        } else {
            showBiometricPrompt(fragmentActivity, isLogin, biometricListener, isOpen);
            return;
        }
        //发生改变就清除状态
        if (isLogin) {
            if (AppClient.getInstance().getBiometricManager().isEnableFingerprintLogin()) {
                AppClient.getInstance().getBiometricManager().setEnableFingerprintLogin(false);
            }
        } else {
            if (AppClient.getInstance().getBiometricManager().isEnableFingerprintPay()) {
                AppClient.getInstance().getBiometricManager().setEnableFingerprintPay(false);
            }
        }
    }

    public interface BiometricListener {
        //接口报错
        void onBiometricFail(String errMsg);

        //认证成功
        void onBiometricSuccess();

        //认证报错
        void onBiometricError();
    }
}
