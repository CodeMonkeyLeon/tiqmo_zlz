package cn.swiftpass.wallet.tiqmo.module.home.entity;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

/**
 * 自然月预算信息(注意：如果这个对象没有返回或者为null,APP就应该按照需求展示”Set up budget +”)
 */
public class BudgetHomeEntity extends BaseEntity {
    //当前自然月剩余预算金额
    public String remainBalanceMoney;
    //当前自然月总预算金额
    public String totalBudgetMoney;
    //剩余自然月天数
    public String remainDays;
    //本月支持/本月预算( 当本月支出大于预算展示0%)
    public String spendingDiviBudgetPercent;
}
