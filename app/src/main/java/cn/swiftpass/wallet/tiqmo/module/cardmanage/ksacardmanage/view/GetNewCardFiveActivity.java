package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.MapResultEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.OpenCardReqEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.AddCardAddressPresenter;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.CityEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.StateListEntity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;

public class GetNewCardFiveActivity extends BaseCompatActivity<CardKsaContract.AddCardAddressPresenter> implements CardKsaContract.AddCardAddressView,OnMapReadyCallback {

    public static final int request_code = 101;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_introduce_image)
    ImageView ivIntroduceImage;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.iv_check_address)
    ImageView ivCheckAddress;
    @BindView(R.id.ll_address)
    LinearLayout llAddress;
    @BindView(R.id.iv_add_address)
    ImageView ivAddAddress;
    @BindView(R.id.tv_add_address)
    TextView tvAddAddress;
    @BindView(R.id.iv_check_add_address)
    ImageView ivCheckAddAddress;
    @BindView(R.id.ll_add_address)
    LinearLayout llAddAddress;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.con_map)
    ConstraintLayout conMap;

    private String address,province,city;

    private OpenCardReqEntity openCardReqEntity,editCardReqEntity;

    private boolean hasEdit;

    private GoogleMap mMap;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_new_card_five;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        try {
            tvTitle.setText(R.string.DigitalCard_0);
            setDarkBar();
            fixedSoftKeyboardSliding();
            LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
            if (getUserInfoEntity() != null) {
                address = getUserInfoEntity().address;
                city = getUserInfoEntity().cities;
                province = getUserInfoEntity().states;
                tvAddress.setText(address + " , " + city + " , " + province);
            }
            int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
            if (status == ConnectionResult.SUCCESS) {
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
                searchMap();
            } else {
                conMap.setVisibility(View.GONE);
            }

            llAddAddress.setAlpha(0.5f);
        }catch (Throwable e){
            e.printStackTrace();
        }
    }

    private void searchMap(){
        //Success! Do what you want
        String mapAddress = "";
        StringBuffer stringBuffer = new StringBuffer(mapAddress);
        if(!TextUtils.isEmpty(province)){
            stringBuffer.append(province);
            if(!TextUtils.isEmpty(city)){
                stringBuffer.append(",");
                stringBuffer.append(city);
            }

            if(!TextUtils.isEmpty(address)){
                stringBuffer.append(",");
                stringBuffer.append(address);
            }
        }else{
            if(!TextUtils.isEmpty(address)){
                stringBuffer.append(address);
            }
        }
        mapAddress = stringBuffer.toString();
        mPresenter.mapSearch(mapAddress, BuildConfig.googleMapKey);
    }

    @Override
    public void mapSearchSuccess(MapResultEntity mapResultEntity) {
        try {
            if(mapResultEntity.results != null && mapResultEntity.results.size()>0) {
                String lat = mapResultEntity.results.get(0).geometry.location.lat;
                String lng = mapResultEntity.results.get(0).geometry.location.lng;
                Bitmap unselectedMarker = setMarker(R.drawable.icon_map);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(mMap != null) {
                            LatLng sydney = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(unselectedMarker))
                                    .position(sydney));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15.0f));
                        }
                    }
                });
            }
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    public void showErrorMsg(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    public Bitmap setMarker(int resId) {
        View markerView = LayoutInflater.from(this).inflate(R.layout.view_map_marker_normal, null, false);
        ImageView ivBranchMarker = markerView.findViewById(R.id.iv_marker);
        ivBranchMarker.setImageResource(resId);
        Bitmap bmp = ((BitmapDrawable) ivBranchMarker.getDrawable()).getBitmap();
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        float scaleWidth = 1.0f;
        float scaleHeight = 1.0f;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        bmp = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, true);
        return bmp;
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        try {
            mMap = googleMap;
            mMap.getUiSettings().setZoomGesturesEnabled(false);
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            // Add a marker in Sydney and move the camera
            Bitmap unselectedMarker = setMarker(R.drawable.icon_map);
            LatLng sydney = new LatLng(ProjectApp.getLatitude(), ProjectApp.getLongitude());
            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(unselectedMarker))
                    .position(sydney));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15f));
        }catch (Throwable e){
            LogUtils.d("errorMsg", "---"+e+"---");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            openCardReqEntity = UserInfoManager.getInstance().getOpenCardReqEntity();
            ivIntroduceImage.setImageResource(AndroidUtils.getCardFaceDrawable(openCardReqEntity.cardFaceId, false));
        }catch (Throwable e){
            e.getStackTrace();
        }
    }

    @OnClick({R.id.iv_back, R.id.ll_address, R.id.ll_add_address, R.id.tv_confirm,R.id.iv_add_address})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_address:
                if(getUserInfoEntity() != null) {
                    address = getUserInfoEntity().address;
                    city = getUserInfoEntity().cities;
                    province = getUserInfoEntity().states;
                }
                ivCheckAddress.setVisibility(View.VISIBLE);
                ivCheckAddAddress.setVisibility(View.GONE);
                llAddress.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_1c47d1_efeeee));
                llAddAddress.setBackground(null);
                llAddress.setAlpha(1.0f);
                llAddAddress.setAlpha(0.5f);
                break;
            case R.id.iv_add_address:

                ActivitySkipUtil.startAnotherActivityForResult(this, AddCardAddressActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN, request_code);
                break;
            case R.id.ll_add_address:
                if(hasEdit){
                    address = editCardReqEntity.address;
                    city = editCardReqEntity.city;
                    province = editCardReqEntity.province;
                    ivCheckAddress.setVisibility(View.GONE);
                    ivCheckAddAddress.setVisibility(View.VISIBLE);
                    llAddAddress.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_1c47d1_efeeee));
                    llAddress.setBackground(null);
                    llAddAddress.setAlpha(1f);
                    llAddress.setAlpha(0.5f);
                }else {
                    ActivitySkipUtil.startAnotherActivityForResult(this, AddCardAddressActivity.class, hashMap, ActivitySkipUtil.ANIM_TYPE.BOTTOM_IN, request_code);
                }
                break;
            case R.id.tv_confirm:
                openCardReqEntity.address = address;
                openCardReqEntity.city = city;
                openCardReqEntity.province = province;
                UserInfoManager.getInstance().setOpenCardReqEntity(openCardReqEntity);
                ActivitySkipUtil.startAnotherActivity(this, GetCardPwdOneActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == request_code) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    editCardReqEntity = (OpenCardReqEntity) data.getSerializableExtra("openCardReqEntity");
                    address = editCardReqEntity.address;
                    city = editCardReqEntity.city;
                    province = editCardReqEntity.province;
                    ivCheckAddress.setVisibility(View.GONE);
                    ivCheckAddAddress.setVisibility(View.VISIBLE);
                    llAddAddress.setAlpha(1f);
                    llAddress.setAlpha(0.5f);
                    tvAddAddress.setText(address+" , "+city+" , "+province);
                    ivAddAddress.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_digital_address_edit));
                    llAddAddress.setBackgroundResource(ThemeSourceUtils.getSourceID(mContext,R.attr.shape_1c47d1_efeeee));
//                    llAddAddress.setBackgroundResource(R.drawable.shape_efeeee_white_1);
                    llAddress.setBackground(null);
                    hasEdit = true;
                }
            }
        }

    }

    @Override
    public CardKsaContract.AddCardAddressPresenter getPresenter() {
        return new AddCardAddressPresenter();
    }

    @Override
    public void showCityListDia(CityEntity result) {

    }

    @Override
    public void getStateListSuccess(StateListEntity result) {

    }
}
