package cn.swiftpass.wallet.tiqmo.module.home.view;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;

public class ProfileActivity extends BaseCompatActivity {

    ProfileFragment profileFragment;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_profile;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        profileFragment = new ProfileFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fl_profile, profileFragment);
        ft.commit();
    }

    @Override
    public void notifyByThemeChanged() {
        super.notifyByThemeChanged();
        if (profileFragment != null) {
            profileFragment.noticeThemeChange();
        }
    }
}
