package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.zrq.spanbuilder.Spans;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.ImrSendMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.ImrSendMoneySummaryPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.appsflyer.AppsFlyerEntity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class ImrSendMoneySummaryActivity extends BaseCompatActivity<ImrSendMoneyContract.SummaryPresenter> implements ImrSendMoneyContract.SummaryView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_to_avatar)
    ImageView ivToAvatar;
    @BindView(R.id.tv_bank_name_title)
    TextView tvBankNameTitle;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_bank_branch_title)
    TextView tvBankBranchTitle;
    @BindView(R.id.tv_bank_branch)
    TextView tvBankBranch;
    @BindView(R.id.tv_receiver_gets)
    TextView tvReceiverGets;
    @BindView(R.id.ll_recevier_gets)
    LinearLayout llRecevierGets;
    @BindView(R.id.tv_transfer_method)
    TextView tvTransferMethod;
    @BindView(R.id.tv_purpose)
    TextView tvPurpose;
    @BindView(R.id.tv_transfer_amount)
    TextView tvTransferAmount;
    @BindView(R.id.tv_wrong_fees)
    TextView tvWrongFees;
    @BindView(R.id.tv_fees)
    TextView tvFees;
    @BindView(R.id.tv_wrong_vat_money)
    TextView tvWrongVatMoney;
    @BindView(R.id.tv_vat_money)
    TextView tvVatMoney;
    @BindView(R.id.tv_discount_title)
    TextView tvDiscountTitle;
    @BindView(R.id.tv_discount_money)
    TextView tvDiscountMoney;
    @BindView(R.id.con_discount)
    ConstraintLayout conDiscount;
    @BindView(R.id.tv_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_bank_number)
    LinearLayout llBankNumber;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.tv_imr_pay)
    TextView tvImrPay;
    @BindView(R.id.tv_discount_content)
    TextView tv_discount_content;
    @BindView(R.id.ll_transfer_fee)
    ConstraintLayout llTransferFee;
    @BindView(R.id.ll_transfer_vat)
    ConstraintLayout llTransferVat;

    private String orderNo;
    private CheckOutEntity checkOutEntity;

    public String orderAmount;
    public String receiptFullName;
    public String receiptSex;
    public String destinationAmount;
    public String destinationCurrency;
    public String purpose;
    public String receiptMethod;
    public String receiptOrgName;
    public String receiptAccountNo;
    public String vatMoney;
    public String discountAmount;
    private String orderCurrencyCode, transferFees, receiptMethodDesc, couponType, payerAmount, wrongTransferFee, wrongVat;

    private ImrOrderInfoEntity mImrOrderInfoEntity;

    private String paymentMethodNo;
    private String orderType = "P";

    private RiskControlEntity riskControlEntity;

    private static ImrSendMoneySummaryActivity imrSendMoneySummaryActivity;

    public static ImrSendMoneySummaryActivity getImrSendMoneySummaryActivity() {
        return ImrSendMoneySummaryActivity.imrSendMoneySummaryActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imrSendMoneySummaryActivity = null;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_imr_send_money_summary;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        imrSendMoneySummaryActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvTitle.setText(R.string.IMR_sendMoneyS_1);
        if (getIntent() != null && getIntent().getExtras() != null) {
            checkOutEntity = (CheckOutEntity) getIntent().getExtras().getSerializable(Constants.CHECK_OUT_ENTITY);
            mImrOrderInfoEntity = (ImrOrderInfoEntity) getIntent().getExtras().getSerializable(Constants.mImrOrderInfoEntity);
            if (checkOutEntity != null) {
                orderAmount = checkOutEntity.orderAmount;
                orderNo = checkOutEntity.orderNo;
                receiptFullName = checkOutEntity.receiptFullName;
                receiptSex = checkOutEntity.receiptSex;
                destinationAmount = checkOutEntity.destinationAmount;
                destinationCurrency = checkOutEntity.destinationCurrency;
                purpose = checkOutEntity.purpose;
                receiptMethod = checkOutEntity.receiptMethod;
                receiptMethodDesc = checkOutEntity.receiptMethodDesc;
                receiptOrgName = checkOutEntity.receiptOrgName;
                receiptAccountNo = checkOutEntity.receiptAccountNo;
                vatMoney = checkOutEntity.discountVat;
                wrongVat = checkOutEntity.vat;
                discountAmount = checkOutEntity.couponAmount;
                orderCurrencyCode = checkOutEntity.orderCurrencyCode;
                transferFees = checkOutEntity.discountTransFees;
                wrongTransferFee = checkOutEntity.transFees;
                couponType = checkOutEntity.couponType;
                payerAmount = checkOutEntity.payerAmount;

                if (Constants.CASH_BACK.equals(couponType)) {
                    tvDiscountTitle.setText(R.string.trx_detail_6);
                    tv_discount_content.setVisibility(View.VISIBLE);
                } else if (Constants.CASH_REDUCE.equals(couponType)) {
                    tvDiscountTitle.setText(R.string.trx_detail_4);
                }

                tvTransferMethod.setText(receiptMethodDesc);

                tvBankName.setText(receiptOrgName);
                tvBankBranch.setText(receiptAccountNo);
                tvWrongFees.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                tvWrongVatMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

                tvReceiverGets.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(destinationAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                        .text(destinationCurrency).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                        .build());

                tvName.setText(receiptFullName);
                if (Constants.imr_BT.equals(receiptMethod)) {
                    tvBankNameTitle.setText(getString(R.string.IMR_84));
                    tvBankBranchTitle.setText(getString(R.string.IMR_92));
                } else {
                    tvBankNameTitle.setText(getString(R.string.IMR_84_1));
                    tvBankBranchTitle.setText(getString(R.string.IMR_85_1));
                }

                if ("W2W".equals(receiptMethod)) {
                    llBankNumber.setVisibility(View.GONE);
                    tvBankNameTitle.setText(getString(R.string.IMR_new_14));
                }

                ivToAvatar.setImageResource(ThemeSourceUtils.getDefAvatar(mContext, receiptSex));
                tvPurpose.setText(purpose);
                if (!TextUtils.isEmpty(orderAmount)) {
                    tvTransferAmount.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(orderAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                            .build());
                }

                if (!TextUtils.isEmpty(payerAmount)) {
                    tvImrPay.setText(getString(R.string.scan_pay_7) + " " + AndroidUtils.getTransferMoney(payerAmount) + " " + LocaleUtils.getCurrencyCode(orderCurrencyCode));
                }

                if (!TextUtils.isEmpty(wrongVat)) {
                    ////如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                    llTransferVat.setVisibility(View.VISIBLE);
                    tvWrongVatMoney.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(wrongVat) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());

                    if (!TextUtils.isEmpty(vatMoney)) {
                        tvVatMoney.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(vatMoney) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }else {
                    //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                    if (!TextUtils.isEmpty(vatMoney) && BigDecimalFormatUtils.compareBig(vatMoney, "0")) {
                        tvWrongVatMoney.setVisibility(View.GONE);
                        tvVatMoney.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(vatMoney) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    } else {
                        llTransferVat.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(wrongTransferFee)) {
                    //如果原金额和优惠金额不相等，就返回原金额，不管金额是否为0，都展示
                    llTransferFee.setVisibility(View.VISIBLE);
                    tvWrongFees.setText(Spans.builder()
                            .text(AndroidUtils.getTransferMoney(wrongTransferFee) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_50white_95979d))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                            .build());

                    if(!TextUtils.isEmpty(transferFees)) {
                        tvFees.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(transferFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }else{
                    //如果原金额和优惠金额相等，就不返回原金额，此时优惠金额为0不展示，如果不为0，就不展示划线的样式，只展示1行
                    if (!TextUtils.isEmpty(transferFees) && BigDecimalFormatUtils.compareBig(transferFees,"0")) {
                        tvWrongFees.setVisibility(View.GONE);
                        tvFees.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(transferFees) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }else{
                        llTransferFee.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(discountAmount)) {
                    if (!BigDecimalFormatUtils.compareBig(discountAmount,"0")) {
                        conDiscount.setVisibility(View.GONE);
                    } else {
                        tvDiscountMoney.setText(Spans.builder()
                                .text(AndroidUtils.getTransferMoney(discountAmount) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Bold.ttf":"fonts/SFProDisplay-Bold.ttf")).size(14)
                                .text(LocaleUtils.getCurrencyCode(orderCurrencyCode)).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_80white_3a3b44))).size(10)
                                .build());
                    }
                }
            }
        }
    }


    @Override
    public void imrCheckSuccess(Void result) {
        requestImrSend();
    }

    private void requestImrSend() {
        if (mImrOrderInfoEntity != null) {
            riskControlEntity = mImrOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    if (checkOutEntity != null) {
                        setTransferMap(mHashMaps);
                    }
                    ActivitySkipUtil.startAnotherActivity(ImrSendMoneySummaryActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)) {
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                } else {
                    requestTransfer();
                }
            } else {
                requestTransfer();
//                jumpToPwd();
            }
        }
    }

    private void requestTransfer() {
        if (checkOutEntity != null) {
            orderNo = checkOutEntity.orderNo;
            if (checkOutEntity.paymentMethodList != null && checkOutEntity.paymentMethodList.size() > 0) {
                paymentMethodNo = checkOutEntity.paymentMethodList.get(0).paymentMethodNo;
            }
            orderType = "P";
            mPresenter.confirmPay(checkOutEntity.orderNo, paymentMethodNo, "P");
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap) {
        mHashMap.put(Constants.CHECK_OUT_ENTITY, checkOutEntity);
        mHashMap.put(Constants.sceneType, Constants.TYPE_IMR_PAY);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_IMR);
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_20));
    }

    public void jumpToPwd(boolean isNeedIvr) {
        if (checkOutEntity != null) {
            HashMap<String, Object> mHashMap = new HashMap<>(16);
            setTransferMap(mHashMap);
            if(isNeedIvr){
                ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }else {
                ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public void confirmPaySuccess(TransferEntity transferEntity) {
        confirmPaySuccess(this,Constants.TYPE_IMR_PAY,checkOutEntity,false,transferEntity);
    }

    @Override
    public void confirmPayFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void imrCheckFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public ImrSendMoneyContract.SummaryPresenter getPresenter() {
        return new ImrSendMoneySummaryPresenter();
    }

    @OnClick({R.id.iv_back, R.id.tv_imr_pay})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_imr_pay:
                AppsFlyerEntity appsFlyerEntity = new AppsFlyerEntity();
                appsFlyerEntity.desitinationCode = checkOutEntity.destinationCountry;
                appsFlyerEntity.purpose = purpose;
                UserInfoManager.getInstance().setAppsFlyerEntity(appsFlyerEntity);
                if (isChannelTF()) {
                    requestImrSend();
                } else {
                    mPresenter.imrCheck(orderNo);
                }
                break;
            default:
                break;
        }
    }
}
