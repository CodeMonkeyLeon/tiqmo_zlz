package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChatSendMoneyEntity extends BaseEntity {
    /**
     * {
     *    "text":"send chat text",
     *    "transferMoney":{
     *        "sceneType":"8",// 7 send 8 Request Transfer 18 Split Bill
     *        "remindStatus":"1", // 1 表示已经发送过提醒了 0 或者空 表示 没有提醒过
     *        "payStatus":"1",// 1 表示已经付过钱了 0 还没有付钱
     *        "declineStatus":"1",// 1 表示已经拒绝了 0 还没有拒绝
     *        "orderNo":"model.orderNo", // 订单号，detail 使用
     *        "orderType":"model.orderType",  // 订单类型，detail 使用
     *        "orderAmount":"model.orderAmount",  // chat 显示使用
     *        "orderCurrencyCode":"model.orderCurrencyCode",  // chat 显示使用
     *        "payerUserId":"payerUserId",  // 付款人 UserId
     *        "payerName":"payerUserName", // 付款人 firstName
     *        "payeeUserId":"model.payeeNum" // 收款人 UserId
     *    }
     * }
     */
    public String sceneType;
    public String remindStatus;
    public String payStatus;
    public String declineStatus;
    public String orderNo;
    public String orderType;
    public String orderAmount = "0.00";
    public String orderCurrencyCode;
    public String payerUserId;
    public String payerName;
    public String payeeUserId;

    public static ChatSendMoneyEntity getChatSendEntity(TransferEntity transferEntity){
        ChatSendMoneyEntity chatSendMoneyEntity = new ChatSendMoneyEntity();
        chatSendMoneyEntity.sceneType = String.valueOf(transferEntity.sceneType);
        chatSendMoneyEntity.remindStatus = transferEntity.remindStatus;
        chatSendMoneyEntity.payStatus = transferEntity.payStatus;
        chatSendMoneyEntity.declineStatus = transferEntity.declineStatus;
        chatSendMoneyEntity.orderNo = transferEntity.orderNo;
        chatSendMoneyEntity.orderType = transferEntity.orderType;
        chatSendMoneyEntity.orderAmount = transferEntity.orderAmount;
        chatSendMoneyEntity.orderCurrencyCode = transferEntity.orderCurrencyCode;
        if(transferEntity.recentTransferPersons != null) {
            chatSendMoneyEntity.payerUserId = transferEntity.recentTransferPersons.payerUserId;
            chatSendMoneyEntity.payerName = transferEntity.recentTransferPersons.payerUserName;
            chatSendMoneyEntity.payeeUserId = transferEntity.recentTransferPersons.payeeUserId;
        }
        return chatSendMoneyEntity;
    }
}
