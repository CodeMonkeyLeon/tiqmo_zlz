package cn.swiftpass.wallet.tiqmo.module.imr.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPayerDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.presenter.AddBeneFivePresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.CustomizeEditText;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.dialog.BottomDialog;

public class AddBeneficiarySixActivity extends BaseCompatActivity<AddBeneficiaryContract.AddFivePresenter> implements AddBeneficiaryContract.AddFiveView {
    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.et_bank_name)
    EditTextWithDel etBankName;
    @BindView(R.id.et_bank_branch)
    EditTextWithDel etBankBranch;
    @BindView(R.id.et_iban)
    CustomizeEditText etIban;
    @BindView(R.id.et_account_number)
    EditTextWithDel etAccountNumber;
    @BindView(R.id.tv_save)
    TextView tvSave;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    @BindView(R.id.et_bene_currency)
    EditTextWithDel etBeneCurrency;
    @BindView(R.id.ll_branch_id)
    LinearLayout llBranchId;
    @BindView(R.id.et_branch_id)
    EditTextWithDel etBranchId;

    private ImrBankAgentEntity imrNameEntity, imrBranchEntity;

    private ImrBeneficiaryEntity imrBeneficiaryEntity;
    private boolean isEditBeneficiary = false;
    private ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo;// edit data

    private int ibanLengthLimit;

    private String receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
            nickName, relationshipCode, callingCode, phone,
            transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
            birthDate, sex, cityName, districtName,
            poBox, buildingNo, street, idNo, idExpiry,
            bankAccountType, ibanNo, bankAccountNo, ibanPreCode, ibanLength, saveFlag, ibanSupport, accountSupport,
            receiptOrgName, receiptOrgBranchName,
            cityId, beneCurrency,channelPayeeId,channelCode,branchId;

    private boolean showSaveList;

    private String payeeInfoId;
    private ImrExchangeRateEntity imrExchangeRateEntity;

    //银行模式支持币种
    public List<String> bankSupportCurrencyCodes = new ArrayList<>();

    private RiskControlEntity riskControlEntity;

    private static AddBeneficiarySixActivity addBeneficiarySixActivity;
    private BottomDialog bottomDialog;

    public static AddBeneficiarySixActivity getAddBeneficiarySixActivity() {
        return addBeneficiarySixActivity;
    }

    //处理失焦
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (AndroidUtils.isShouldHideInput(v, ev)) {//点击editText控件外部
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    assert v != null;
                    AndroidUtils.hideKeyboard(v);//软键盘工具类
                    if (etIban != null) {
                        etIban.clearFocus();
                    }
                    if (etAccountNumber.getEditText() != null) {
                        etAccountNumber.getEditText().clearFocus();
                    }
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_add_beneficiary_six;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        addBeneficiarySixActivity = this;
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvTitle.setText(getString(R.string.IMR_20));
        etBankName.getTlEdit().setHint(getString(R.string.IMR_84));
        etBankBranch.getTlEdit().setHint(getString(R.string.IMR_85));
        etBranchId.getTlEdit().setHint(getString(R.string.sprint11_104));
        etAccountNumber.getTlEdit().setHint(getString(R.string.IMR_24));
        etBeneCurrency.getTlEdit().setHint(getString(R.string.IMR_new_3));
        ivKycStep.setVisibility(View.VISIBLE);
        ivKycStep.setImageResource(R.drawable.kyc_step_3);
//        ivKycStep.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_imr_add_step_5));
        etBankName.setFocusableFalse();
        etBankBranch.setFocusableFalse();
        etBeneCurrency.setFocusableFalse();
        if(isChannelTF()){
            etBeneCurrency.setVisibility(View.VISIBLE);
        }
        isEditBeneficiary = getIntent().getBooleanExtra(Constants.IS_FROM_EDIT_BENEFICIARY, false);
        if (isEditBeneficiary) {
            beneficiaryInfo = (ImrBeneficiaryDetails.ImrBeneficiaryDetail) getIntent().getSerializableExtra("BeneficiaryInfo");
            receiptOrgCode = beneficiaryInfo.receiptOrgCode;
            receiptOrgBranchCode = beneficiaryInfo.receiptOrgBranchCode;
            ibanNo = beneficiaryInfo.ibanNo;
            receiptMethod = beneficiaryInfo.receiptMethod;
            bankAccountNo = beneficiaryInfo.bankAccountNo;
            ibanPreCode = beneficiaryInfo.ibanPreCode;
            ibanLength = beneficiaryInfo.ibanLength;
            bankAccountType = beneficiaryInfo.bankAccountType;
            ibanSupport = beneficiaryInfo.ibanSupport;
            accountSupport = beneficiaryInfo.accountSupport;
            transferDestinationCountryCode = beneficiaryInfo.transferDestinationCountryCode;
            beneCurrency = beneficiaryInfo.currencyCode;
            bankSupportCurrencyCodes = beneficiaryInfo.imrPaymentEntity.supportCurrencyList;
            receiptOrgName = beneficiaryInfo.receiptOrgName;
            channelCode = beneficiaryInfo.channelCode;
            channelPayeeId = beneficiaryInfo.channelPayeeId;
            branchId = beneficiaryInfo.branchId;
            receiptOrgBranchName = beneficiaryInfo.receiptOrgBranchName;
            if (!TextUtils.isEmpty(beneficiaryInfo.bankAccountNo)) {
                etAccountNumber.getEditText().setText(beneficiaryInfo.bankAccountNo);
            }
            if (!TextUtils.isEmpty(receiptOrgName)) {
                etBankName.getEditText().setText(receiptOrgName);
            }
            if (!TextUtils.isEmpty(receiptOrgBranchName)) {
                etBankBranch.getEditText().setText(receiptOrgBranchName);
            }
            if("OTHER (NOT LISTED)".equals(receiptOrgBranchCode)){
                llBranchId.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(branchId)) {
                etBranchId.getEditText().setText(branchId);
            }
            if (!TextUtils.isEmpty(ibanPreCode)) {
                etIban.setLeftText(ibanPreCode);
                etIban.setClickListener(new CustomizeEditText.OnViewClickListener() {
                    @Override
                    public void onRightViewClick() {

                    }
                });
            }

            etBeneCurrency.setContentText(beneCurrency);

            setIbanLimit();

            if (!TextUtils.isEmpty(ibanNo)) {
                String showIbanNo = ibanNo;
                if (!TextUtils.isEmpty(ibanPreCode)) {
                    showIbanNo = ibanNo.replace(ibanPreCode, "");
                }
                etIban.setText(showIbanNo);
                try {
                    etIban.setSelection(showIbanNo.length() - 1);
                } catch (Throwable e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                }
            }

            if ("Y".equals(ibanSupport) && !"Y".equals(accountSupport)) {
                etIban.setVisibility(View.GONE);
                etAccountNumber.setVisibility(View.GONE);
            } else if ("Y".equals(accountSupport) && !"Y".equals(ibanSupport)) {
                etIban.setVisibility(View.GONE);
                etAccountNumber.setVisibility(View.VISIBLE);
            }
            tvSave.setEnabled(!TextUtils.isEmpty(receiptOrgCode) && !TextUtils.isEmpty(receiptOrgBranchCode));
        } else {
            imrBeneficiaryEntity = AppClient.getInstance().getUserManager().getImrBeneficiaryEntity();
            if (imrBeneficiaryEntity != null) {
                receiptMethod = imrBeneficiaryEntity.receiptMethod;
                payeeFullName = imrBeneficiaryEntity.payeeFullName;
                nickName = imrBeneficiaryEntity.nickName;
                relationshipCode = imrBeneficiaryEntity.relationshipCode;
                callingCode = imrBeneficiaryEntity.callingCode;
                phone = imrBeneficiaryEntity.phone;
                transferDestinationCountryCode = imrBeneficiaryEntity.transferDestinationCountryCode;
                payeeInfoCountryCode = imrBeneficiaryEntity.payeeInfoCountryCode;
                birthPlace = imrBeneficiaryEntity.birthPlace;
                birthDate = imrBeneficiaryEntity.birthDate;
                sex = imrBeneficiaryEntity.sex;
                cityName = imrBeneficiaryEntity.cityName;
                districtName = imrBeneficiaryEntity.districtName;
                poBox = imrBeneficiaryEntity.poBox;
                buildingNo = imrBeneficiaryEntity.buildingNo;
                street = imrBeneficiaryEntity.street;
                idNo = imrBeneficiaryEntity.idNo;
                idExpiry = imrBeneficiaryEntity.idExpiry;
                bankAccountType = imrBeneficiaryEntity.bankAccountType;
                ibanNo = imrBeneficiaryEntity.ibanNo;
                bankAccountNo = imrBeneficiaryEntity.bankAccountNo;
                ibanPreCode = imrBeneficiaryEntity.ibanPreCode;
                ibanLength = imrBeneficiaryEntity.ibanLength;
                showSaveList = imrBeneficiaryEntity.showSaveList;
                saveFlag = imrBeneficiaryEntity.saveFlag;
                ibanSupport = imrBeneficiaryEntity.ibanSupport;
                accountSupport = imrBeneficiaryEntity.accountSupport;
                bankSupportCurrencyCodes = imrBeneficiaryEntity.bankSupportCurrencyCodes;
                channelCode = imrBeneficiaryEntity.channelCode;
                channelPayeeId = imrBeneficiaryEntity.channelPayeeId;
            }

            etIban.setLeftText(ibanPreCode);
            if ("Y".equals(ibanSupport) && !"Y".equals(accountSupport)) {
                etIban.setVisibility(View.GONE);
                etAccountNumber.setVisibility(View.GONE);
            } else if ("Y".equals(accountSupport) && !"Y".equals(ibanSupport)) {
                etIban.setVisibility(View.GONE);
                etAccountNumber.setVisibility(View.VISIBLE);
            }
        }

        if(TextUtils.isEmpty(beneCurrency)) {
            if (bankSupportCurrencyCodes.size() == 1) {
                beneCurrency = bankSupportCurrencyCodes.get(0);
                etBeneCurrency.setContentText(beneCurrency);
            } else if (bankSupportCurrencyCodes.size() > 1) {
                beneCurrency = "";
                etBeneCurrency.setContentText("");
            }
        }

        setIbanLimit();

        etAccountNumber.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(24),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_NUMBER)});
        etBranchId.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(60),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPACE)});

        etBankName.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.getBankAgentName(transferDestinationCountryCode, Constants.imr_BT,channelCode);
            }
        });

        etBankBranch.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(receiptOrgCode)) {
                    mPresenter.getBankAgentBranch(transferDestinationCountryCode, Constants.imr_BT, receiptOrgCode);
                }
            }
        });

        etAccountNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkContinueStatus();
            }
        });

        etBranchId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                branchId = s.toString();
                checkContinueStatus();
            }
        });

        etIban.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkContinueStatus();
            }
        });

        etIban.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                String ibanNo = etIban.getText().toString().trim();
                if (!TextUtils.isEmpty(ibanPreCode)) {
                    ibanNo = ibanPreCode + ibanNo;
                }
                if (!hasFocus) {
                    if (ibanLengthLimit > 0 && ibanLengthLimit < 64) {
                        if (!TextUtils.isEmpty(ibanNo) && ibanNo.length() < ibanLengthLimit) {
                            etIban.setError(getString(R.string.IMR_notice_9));
                        }
                    }
                }
            }
        });

        etBeneCurrency.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
                int size = bankSupportCurrencyCodes.size();
                for (int i = 0; i < size; i++) {
                    String currency = bankSupportCurrencyCodes.get(i);
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = currency;
                    selectInfoEntityList.add(selectInfoEntity);
                }
                ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_new_3), false, false);
                dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        beneCurrency = bankSupportCurrencyCodes.get(position);
                        if (!TextUtils.isEmpty(beneCurrency)) {
                            etBeneCurrency.getEditText().setText(beneCurrency);
                            checkContinueStatus();
                        }
                    }
                });
                dialog.showNow(getSupportFragmentManager(), "getBeneCurrencySuccess");
            }
        });
    }

    private void setIbanLimit() {
        try {
            ibanLengthLimit = Integer.parseInt(ibanLength);
            if (!TextUtils.isEmpty(ibanPreCode)) {
                ibanLengthLimit = ibanLengthLimit - ibanPreCode.length();
            }

            ibanLengthLimit = ibanLengthLimit > 0 ? ibanLengthLimit : 64;
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
            ibanLengthLimit = 64;
        }
        etIban.setFilters(new InputFilter[]{new InputFilter.LengthFilter(ibanLengthLimit),
                new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_NUMBER)});
        etIban.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        addBeneficiarySixActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_ADD_SEND_BENEFICIARY == event.getEventType()) {
            requestAddBene();
        }
    }

    private void requestAddBene(){
        mPresenter.imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, "", bankAccountNo, saveFlag, receiptOrgName, receiptOrgBranchName,
                cityId, beneCurrency,channelPayeeId,channelCode,branchId);
    }

    @OnClick({R.id.iv_back, R.id.tv_save})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_save:
                bankAccountNo = etAccountNumber.getEditText().getText().toString().trim();
                ibanNo = ibanPreCode + etIban.getText().toString().trim();
                if ("N".equals(accountSupport)) {
                    bankAccountNo = "";
                } else {
                    bankAccountType = "SWIFT";
                }

                if (showSaveList) {
                    List<String> additionalData = new ArrayList<>();
                    mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                } else {
                    if (isEditBeneficiary) {
                        beneficiaryInfo.receiptOrgBranchCode = receiptOrgBranchCode;
                        beneficiaryInfo.receiptOrgCode = receiptOrgCode;
                        beneficiaryInfo.ibanNo = "";
                        beneficiaryInfo.bankAccountNo = bankAccountNo;
                        beneficiaryInfo.bankAccountType = bankAccountType;
                        beneficiaryInfo.currencyCode = beneCurrency;
                        beneficiaryInfo.receiptOrgName = receiptOrgName;
                        beneficiaryInfo.receiptOrgBranchName = receiptOrgBranchName;
                        beneficiaryInfo.branchId = branchId;
                    } else {
                        imrBeneficiaryEntity.receiptOrgBranchCode = receiptOrgBranchCode;
                        imrBeneficiaryEntity.receiptOrgCode = receiptOrgCode;
                        imrBeneficiaryEntity.ibanNo = "";
                        imrBeneficiaryEntity.bankAccountNo = bankAccountNo;
                        imrBeneficiaryEntity.bankAccountType = bankAccountType;
                        imrBeneficiaryEntity.beneCurrency = beneCurrency;
                        imrBeneficiaryEntity.receiptOrgName = receiptOrgName;
                        imrBeneficiaryEntity.receiptOrgBranchName = receiptOrgBranchName;
                        imrBeneficiaryEntity.branchId = branchId;
                        AppClient.getInstance().getUserManager().setImrBeneficiaryEntity(imrBeneficiaryEntity);
                    }
                    List<String> additionalData = new ArrayList<>();
                    mPresenter.getOtpType("ActionType", "IMRP", additionalData);
                }
                break;
            default:
                break;
        }
    }

    private void checkContinueStatus() {
        if (TextUtils.isEmpty(receiptOrgCode) || TextUtils.isEmpty(receiptOrgBranchCode)) {
            tvSave.setEnabled(false);
            return;
        }
        if(isChannelTF()){
            if(TextUtils.isEmpty(beneCurrency)){
                tvSave.setEnabled(false);
                return;
            }
        }
        String bankAccountNo = etAccountNumber.getEditText().getText().toString().trim();
        if (TextUtils.isEmpty(bankAccountNo)) {
            tvSave.setEnabled(false);
            return;
        }
        if("OTHER (NOT LISTED)".equals(receiptOrgBranchCode)){
            if (TextUtils.isEmpty(branchId)) {
                tvSave.setEnabled(false);
                return;
            }
        }
//        String ibanNo = etIban.getText().toString().trim();

//        if ("Y".equals(ibanSupport) && !"Y".equals(accountSupport)) {
//            if (TextUtils.isEmpty(ibanNo)) {
//                tvSave.setEnabled(false);
//                return;
//            }
//        } else if ("Y".equals(accountSupport) && !"Y".equals(ibanSupport)) {
//            if (TextUtils.isEmpty(bankAccountNo)) {
//                tvSave.setEnabled(false);
//                return;
//            }
//        } else {
//            if (TextUtils.isEmpty(ibanNo) || TextUtils.isEmpty(bankAccountNo)) {
//                tvSave.setEnabled(false);
//                return;
//            }
//        }
//
//        if (ibanLengthLimit > 0 && ibanLengthLimit < 64) {
//            if (!TextUtils.isEmpty(ibanNo) && ibanNo.length() < ibanLengthLimit) {
//                tvSave.setEnabled(false);
//                return;
//            }
//        }

        tvSave.setEnabled(true);

    }

    @Override
    public void getImrCityListSuccess(ImrCityListEntity data) {

    }

    @Override
    public void getImrCityListFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getBankAgentNameSuccess(ImrBankAgentListEntity imrBankAgentListEntity) {
        if (imrBankAgentListEntity != null) {
            List<ImrBankAgentEntity> bankAgentList = imrBankAgentListEntity.bankAgentList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = bankAgentList.size();
            for (int i = 0; i < size; i++) {
                ImrBankAgentEntity imrBankAgentEntity = bankAgentList.get(i);
                if (imrBankAgentEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = imrBankAgentEntity.bankAgentDesc;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_84), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    imrNameEntity = bankAgentList.get(position);
                    if (imrNameEntity != null) {
//                        if (!TextUtils.isEmpty(imrNameEntity.bankAgentCode) && !imrNameEntity.bankAgentCode.equals(receiptOrgCode)) {
//                        }
                        receiptOrgCode = imrNameEntity.bankAgentCode;
                        receiptOrgBranchCode = "";
                        etBankBranch.getEditText().setText("");
                        receiptOrgName = imrNameEntity.bankAgentDesc;
                        etBankName.getEditText().setText(receiptOrgName);
                        checkContinueStatus();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getBankAgentNameSuccess");
        }
    }

    @Override
    public void getBankAgentNameFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getBankAgentBranchSuccess(ImrBankAgentListEntity imrBankAgentListEntity) {
        if (imrBankAgentListEntity != null) {
            List<ImrBankAgentEntity> bankAgentList = imrBankAgentListEntity.bankAgentList;
            ArrayList<SelectInfoEntity> selectInfoEntityList = new ArrayList<>();
            int size = bankAgentList.size();
            for (int i = 0; i < size; i++) {
                ImrBankAgentEntity imrBankAgentEntity = bankAgentList.get(i);
                if (imrBankAgentEntity != null) {
                    SelectInfoEntity selectInfoEntity = new SelectInfoEntity();
                    selectInfoEntity.businessParamValue = imrBankAgentEntity.bankAgentDesc;
                    selectInfoEntityList.add(selectInfoEntity);
                }
            }
            ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, getString(R.string.IMR_85), true, true);
            dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    imrBranchEntity = bankAgentList.get(position);
                    if (imrBranchEntity != null) {
                        receiptOrgBranchCode = imrBranchEntity.bankAgentCode;
                        receiptOrgBranchName = imrBranchEntity.bankAgentDesc;
                        if("OTHER (NOT LISTED)".equals(receiptOrgBranchCode)){
                            llBranchId.setVisibility(View.VISIBLE);
                        }else{
                            llBranchId.setVisibility(View.GONE);
                            branchId = "";
                        }
                        etBankBranch.getEditText().setText(receiptOrgBranchName);
                        checkContinueStatus();
                    }
                }
            });
            dialog.showNow(getSupportFragmentManager(), "getBankAgentBranchSuccess");
        }
    }

    @Override
    public void getBankAgentBranchFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void imrAddBeneficiarySuccess(ImrAddBeneResultEntity imrAddBeneResultEntity) {
        if (imrAddBeneResultEntity != null) {
            if(showSaveList){
            payeeInfoId = imrAddBeneResultEntity.payeeInfoId;
            imrBeneficiaryEntity.payeeInfoId = payeeInfoId;
            mPresenter.activateBeneficiary(payeeInfoId);
            }else{
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_ADD_BENE_SUCCESS, imrBeneficiaryEntity,riskControlEntity));
                ProjectApp.removeImrAddBeneTask();
                finish();
            }
        }
    }

    @Override
    public void imrAddBeneficiaryFail(String errorCode, String errorMsg) {
        if ("030203".equals(errorCode)) {
            showExcessBeneficiaryDialog(errorMsg);
        }else {
            showTipDialog(errorMsg);
        }
    }

    //添加受益人次数
    public void showExcessBeneficiaryDialog(String errorMsg) {
        bottomDialog = new BottomDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.execss_beneficiary_dialog, null);
        TextView tvDeactivate = view.findViewById(R.id.tv_deactivate);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        LinearLayout llContent = view.findViewById(R.id.ll_content);
        tvMsg.setText(errorMsg);
        tvDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setAlpha(mContext, 1f);
            }
        });

        bottomDialog.setContentView(view);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void imrExchangeRateSuccess(ImrExchangeRateEntity imrExchangeRateEntity) {
        if (imrExchangeRateEntity != null) {
            this.imrExchangeRateEntity = imrExchangeRateEntity;
            mPresenter.getLimit(Constants.LIMIT_TRANSFER);
        }
    }

    @Override
    public void imrExchangeRateFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void getLimitSuccess(TransferLimitEntity transferLimitEntity) {
        HashMap<String, Object> mHashMap = new HashMap<>();
        mHashMap.put(Constants.TransferLimitEntity, transferLimitEntity);
        mHashMap.put(Constants.imrExchangeRateEntity, imrExchangeRateEntity);
        mHashMap.put(Constants.ImrBeneficiaryEntity, imrBeneficiaryEntity);
        ActivitySkipUtil.startAnotherActivity(AddBeneficiarySixActivity.this, ImrSendMoneyActivity.class, mHashMap,
                ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        if ("0".equals(saveFlag)) {
            EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_SEND_ADD_BENE_SUCCESS, imrBeneficiaryEntity));
            ProjectApp.removeImrAddBeneTask();
        }
    }

    @Override
    public void getLimitFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public void getImrPayerDetailSuccess(ImrPayerDetailEntity imrPayerDetailEntity) {

    }

    @Override
    public void getImrPayerDetailFail(String errCode, String errMsg) {

    }

    @Override
    public void imrEditBeneficiarySuccess(ResponseEntity result) {
        EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_IMR_EDIT_BENE_SUCCESS, beneficiaryInfo,riskControlEntity));
        ProjectApp.removeImrAddBeneTask();
        finish();
    }

    @Override
    public void imrEditBeneficiaryFail(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public void activateBeneficiarySuccess(Void result) {
        if(imrBeneficiaryEntity != null) {
            mPresenter.imrExchangeRate(imrBeneficiaryEntity.payeeInfoId, "", "", channelCode);
        }
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public AddBeneficiaryContract.AddFivePresenter getPresenter() {
        return new AddBeneFivePresenter();
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr() && showSaveList){
                    jumpToPwd(true);
                }else{
                    requestAddBene();
                }
            } else {
                requestAddBene();
            }
        } else {
            requestAddBene();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        if(showSaveList) {
            mHashMap.put(Constants.sceneType, Constants.TYPE_ADD_SEND_BENE);
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_ADD_SEND_BENE);
//            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.wtw_1));
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_new_15));
        }else{
            mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_ADD_IMR);
            mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_new_15));
            mHashMap.put(Constants.sceneType, Constants.TYPE_IMR_ADD_BENEFICIARY);
            if(isEditBeneficiary){
                mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.IMR_96));
                mHashMap.put("BeneficiaryInfo", beneficiaryInfo);
                mHashMap.put(Constants.IS_FROM_EDIT_BENEFICIARY, true);
            }
        }
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }


    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }
}
