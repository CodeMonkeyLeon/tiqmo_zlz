package cn.swiftpass.wallet.tiqmo.module.setting.animation.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;

public class StackCardAdapter extends BaseAdapter {

    HashMap<String, Object> mHashMaps = new HashMap<>();
    Context mContext;

    public void setmHashMaps(HashMap<String, Object> mHashMaps) {
        this.mHashMaps = mHashMaps;
    }

    private List<CardEntity> newCardList = new ArrayList<>();

    public List<CardEntity> getResIds() {
        return newCardList;
    }

    public void setResIds(List<CardEntity> resIds) {
        this.newCardList = resIds;
    }

    public StackCardAdapter(Context mContext, List<CardEntity> resIds) {
        this.mContext = mContext;
        this.newCardList = resIds;
    }

    @Override
    public int getCount() {
        return newCardList.size();
    }

    @Override
    public CardEntity getItem(int position) {
        return newCardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.item_card_list, parent, false);
            holder = new ViewHolder();
            holder.tv_card_num = convertView.findViewById(R.id.tv_card_num);
            holder.iv_visa = convertView.findViewById(R.id.iv_visa);
            holder.iv_mastercard = convertView.findViewById(R.id.iv_mastercard);
            holder.iv_front_card_image = convertView.findViewById(R.id.iv_front_card_image);
            holder.tv_card_holder_name = convertView.findViewById(R.id.tv_card_holder_name);
            holder.tv_valid_thru = convertView.findViewById(R.id.tv_valid_thru);
//            holder.cardNumber = convertView.findViewById(R.id.card_number);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        CardEntity cardEntity = newCardList.get(position);
//        switch (position){
//            case 0:
//                cardEntity.resId = R.drawable.bg1;
//                break;
//            case 1:
//                cardEntity.resId = R.drawable.bg2;
//                break;
//            case 2:
//                cardEntity.resId = R.drawable.bg3;
//                break;
//            case 3:
//                cardEntity.resId = R.drawable.bg4;
//                break;
//            default:break;
//        }
//        convertView.setBackgroundResource(cardEntity.resId);
//        holder.cardNumber.setText(cardEntity.getCardNo());
        //本地有图先取本地的图
        int resId = (int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6));
        AndroidUtils.setCardLogoVisible(cardEntity.getCardNo(), holder.iv_visa, holder.iv_mastercard);
        if (resId != 0 && resId != -1) {
            holder.iv_front_card_image.setImageResource((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)));
        } else {
            if (cardEntity.getCardFace() != null && !TextUtils.isEmpty(cardEntity.getCardFace())) {
                String card_url = (String) mHashMaps.get(cardEntity.getCardFace());
                Glide.with(mContext)
                        .load(card_url)
                        .placeholder((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)))
                        .error((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)))
                        .into(holder.iv_front_card_image);
            } else {
                holder.iv_front_card_image.setBackgroundResource((int) mHashMaps.get(cardEntity.getCardNo().substring(0, 6)));
            }
        }

        holder.tv_card_num.setText(AndroidUtils.convertBankCardNoWithBlank(cardEntity.getCardNo()));
        holder.tv_card_holder_name.setText(cardEntity.getCustName());

        holder.tv_valid_thru.setText(AndroidUtils.converExpireDate(cardEntity.getExpire()));
        return convertView;
    }

    static class ViewHolder {
        ImageView iv_front_card_image;
        ImageView iv_visa;
        ImageView iv_mastercard;
        TextView tv_card_num;
        TextView tv_card_holder_name;
        TextView tv_valid_thru;
//        TextView cardNumber;
    }
}
