package cn.swiftpass.wallet.tiqmo.module.imr.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ImrCountryListEntity extends BaseEntity {
    public String channelCode;
    public List<ImrCountryEntity> imrCountrysList = new ArrayList<>();
}
