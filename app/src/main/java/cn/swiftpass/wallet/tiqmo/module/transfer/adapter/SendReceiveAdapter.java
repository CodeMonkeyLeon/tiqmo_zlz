package cn.swiftpass.wallet.tiqmo.module.transfer.adapter;

import android.app.Activity;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransactionListInfos;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ResourceHelper;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class SendReceiveAdapter extends BaseRecyclerAdapter<TransactionListInfos> {

    private Activity mActivity;
    private SplitOderDetailAdapter detailAdapter;
    public SendReceiveAdapter(@Nullable List<TransactionListInfos> data, Activity activity) {
        super(R.layout.item_send_receive, data);
        mActivity = activity;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, TransactionListInfos item, int position) {
        String date = getSectionForPosition(position);
        if (TextUtils.isEmpty(date)) {
            return;
        }
        RoundedImageView ivAvatar = holder.getView(R.id.iv_avatar);
        RecyclerView rlInfo = holder.getView(R.id.send_receive_info);

        switch (item.getOrderStatus()){
            case "RECEIVED":
                ivAvatar.setBackgroundResource(R.drawable.l_send_receive_added);
                holder.setTextColor(R.id.tv_money,mContext.getColor(R.color.green2));
                holder.setTextColor(R.id.tv_currencyType,mContext.getColor(R.color.green2));
                break;
            case "SEND":
                ivAvatar.setBackgroundResource(R.drawable.l_send_receive_deducted);
                holder.setTextColor(R.id.tv_money,mContext.getColor(R.color.red2));
                holder.setTextColor(R.id.tv_currencyType,mContext.getColor(R.color.red2));
                break;
            case "SUCCESS":
                ivAvatar.setBackgroundResource(R.drawable.l_split_success);
                break;
            case "PROCESS":
                ivAvatar.setBackgroundResource(R.drawable.l_split_process);
                break;
            case "REFUSED":
                ivAvatar.setBackgroundResource(R.drawable.l_split_reject);
                break;

        }

        holder.setText(R.id.tv_name,item.getOrderDesc());
        holder.setText(R.id.tv_time, item.getTradeTime());
        holder.setText(R.id.tv_money,AndroidUtils.getTransferMoney(item.getAmount()));
        holder.setText(R.id.tv_currencyType,LocaleUtils.getCurrencyCode(item.getOrderCurrency()));

        holder.setGone(R.id.ll_date, false);
        if (position == getPositionForSection(date)) {
            holder.setGone(R.id.ll_date, false);
            holder.setText(R.id.tv_date, item.getDateDesc());
            holder.setTextColor(R.id.tv_date, mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.history_filter_list_title_text_color)));
            holder.setBackgroundRes(R.id.tv_history_line, ThemeSourceUtils.getSourceID(mContext, R.attr.color_30gray_979797));
        } else {
            holder.setGone(R.id.ll_date, true);
        }

        if (item.getSplitOderDetail() != null) {
            LinearLayoutManager manager = new LinearLayoutManager(mContext);
            rlInfo.setLayoutManager(manager);
            detailAdapter = new SplitOderDetailAdapter(item.getSplitOderDetail());
            rlInfo.setAdapter(detailAdapter);
        }

    }


    /**
     * 根据当前位置获取时间值
     */
    public String getSectionForPosition(int position) {
        TransactionListInfos infos = getItem(position);
        if (infos != null) {
            return infos.getDateDesc();
        }
        return "";
    }

    public int getPositionForSection(String date) {
        for (int i = 0; i < getItemCount(); i++) {
            String dateStr = mDataList.get(i).getDateDesc();
            if (!TextUtils.isEmpty(dateStr) && dateStr.equals(date)) {
                return i;
            }
        }
        return -1;
    }

}
