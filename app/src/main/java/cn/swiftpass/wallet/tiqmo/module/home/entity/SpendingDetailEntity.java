package cn.swiftpass.wallet.tiqmo.module.home.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class SpendingDetailEntity extends BaseEntity {

    //环形图表信息
    public AnalysisGraphicEntity graphicInfo;

    //支出预算数据信息
    public SpendingCategoryBudgetEntity spendingCategoryBudgetInfo;

    //支出预算数据信息
    public List<SpendingCategoryListEntity> spendingCategoryList = new ArrayList<>();

    public SpendingActivityDataEntity tiqmoInnerActivityData;
}
