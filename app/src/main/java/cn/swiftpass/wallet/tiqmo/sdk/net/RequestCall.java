package cn.swiftpass.wallet.tiqmo.sdk.net;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.sdk.entity.JsonRequest;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResponseCallback;
import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.HttpParams;
import cn.swiftpass.wallet.tiqmo.sdk.util.ResourcesHelper;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.SpUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;


//请求Call，主要的职责是将HttpParams封装成OKHttp的Call并发起访问
public class RequestCall<T> {

    public static final String SSL_KEY_ALIAS = "Tiqmo";

    private HttpParams mHttpParams;
    private Type mGenericType;//返回值的泛型类型

    private static final Handler HANDLER = new Handler(Looper.getMainLooper());

    public RequestCall(HttpParams httpParams, Type genericType) {
        mHttpParams = httpParams;
        mGenericType = genericType;
    }

    //封装了一个enqueue方法用于发起请求
    public void enqueue(final ResponseCallback<T> callback) {
        if (buildCall(mHttpParams) != null) {
            enqueue(buildCall(mHttpParams), mGenericType, callback);
        }
    }

    public void ocrEnqueue(String url, final ResultCallback<T> callback) {
        ocrEnqueue(ocrBuildCall(url, mHttpParams), mGenericType, callback);
    }
    public void mapEnqueue(String url, final ResultCallback<T> callback) {
        ocrEnqueue(mapBuildCall(url, mHttpParams.params), mGenericType, callback);
    }

    public static <T> void ocrEnqueue(Call call, final Type genericType, final ResultCallback<T> callback) {
        //这里的call是okHttp的call
        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (callback == null) {
                    return;
                }
                //判断Body是否有值
                ResponseBody body = response.body();
                if (!response.isSuccessful() || body == null) {
                    callback.onFailure("HTTP_" + response.code(), ResourcesHelper.getString(R.string.common_3));
                    return;
                }
                String jsonBody;
                try {
                    //转成string
                    jsonBody = body.string();
                } catch (IOException e) {
                    callback.onFailure("HTTP_" + response.code(), ResourcesHelper.getString(R.string.common_3));
                    return;
                }
                //判空
                if (TextUtils.isEmpty(jsonBody)) {
                    callback.onFailure("HTTP_" + response.code(), "Response body is empty");
                    return;
                }
                //判断泛型类型是不是Void，如果是void，就不需要将json转成实体类
                if (genericType != Void.class) {
                    try {
                        //json转成实体类并回调
                        final T jsonResponse = ApiHelper.GSON.fromJson(jsonBody, genericType);
                        if (jsonResponse == null) {
                            callback.onFailure("HTTP_" + response.code(), "json format fail,Response body is unknown");
                        } else {
                            callback.onResult(jsonResponse);
                        }
                    } catch (JsonSyntaxException e) {
                        callback.onFailure(e.getMessage(), e.getMessage());
                    }
                } else {
                    callback.onResult(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull final IOException e) {
                callback.onFailure(e.getMessage(), e.getMessage());
            }

        });
    }

    public static <T> void enqueue(Call call, final Type genericType, final ResponseCallback<T> callback) {
        //这里的call是okHttp的call
        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (callback == null) {
                    return;
                }
                //判断Body是否有值
                ResponseBody body = response.body();
                if (!response.isSuccessful() || body == null) {
                    callFailure(callback, new ResponseException("HTTP_" + response.code(), ResourcesHelper.getString(R.string.common_3)));
                    return;
                }
                String jsonBody;
                try {
                    //转成string
                    jsonBody = body.string();
                } catch (IOException e) {
                    callFailure(callback, e);
                    return;
                }
                //判空
                if (TextUtils.isEmpty(jsonBody)) {
                    callFailure(callback, new Exception("Response body is empty"));
                    return;
                }
                //判断泛型类型是不是Void，如果是void，就不需要将json转成实体类
                if (genericType != Void.class) {
                    try {
                        //json转成实体类并回调
                        final T jsonResponse = ApiHelper.GSON.fromJson(jsonBody, genericType);
                        if (jsonResponse == null) {
                            callFailure(callback, new Exception("json format fail,Response body is unknown"));
                        } else {
                            callSuccessful(callback, jsonResponse);
                        }
                    } catch (JsonSyntaxException e) {
                        callFailure(callback, e);
                    }
                } else {
                    callSuccessful(callback, null);
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull final IOException e) {
                callFailure(callback, e);
            }

        });
    }

    public static OkHttpClient.Builder setBuilder(OkHttpClient.Builder builder, Certificate[] mCertificates) {
        try {
            LogUtils.i("CertsDownloadManager", "cers ssl");
            SSLContext sslContext = SSLContext.getInstance("SSL");
            final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(readKeyStore(mCertificates));
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            builder.sslSocketFactory(sslContext.getSocketFactory());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession sslSession) {
                    if(BuildConfig.ServerUrl.contains(hostname)) {
                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return builder;
    }

    public static KeyStore readKeyStore(Certificate[] mCertificates) {
        KeyStore keyStore = null;
        CertificateFactory certificateFactory = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }

        if (null == keyStore || null == certificateFactory) {
            return null;
        }
        try {
            int index = 0;
            for (int i = 0; i < mCertificates.length; i++) {
                Certificate cert = mCertificates[i];
                String certificateAlias = Integer.toString(index++);
                keyStore.setCertificateEntry(certificateAlias, cert);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }

        return keyStore;
    }

    //把HttpParams构建成一个OkHttp的call
    private static Call buildCall(HttpParams httpParams) {
//        if(BuildConfig.DOWNLOAD_CER_SERVER) {
//            if (ApiHelper.getInstance().getInputStream() != null) {
//                OkHttpClient.Builder builder = ApiHelper.setCertificates(ApiHelper.getInstance().getOkHttpClient().newBuilder(), ApiHelper.getInstance().getInputStream());
//                ApiHelper.getInstance().setmOkHttpClient(builder.build());
//            } else {
//                CertsDownloadManager.getInstance().initCertificate(ProjectApp.getContext());
//                return null;
//            }
//        }
        Map<String, List<String>> uploadPart = httpParams.uploadPart;
        Request.Builder builder = new Request.Builder();
        for (String header : httpParams.headers) {
            String[] keyAndValue = header.split(":");
            if (keyAndValue.length == 2) {
                builder.addHeader(keyAndValue[0], keyAndValue[1]);
            }
        }

        //填充公共参数部分
        JsonRequest jsonRequest = new JsonRequest();
        jsonRequest.setPartnerNo(AppClient.getPartnerNo());
        jsonRequest.setVersion(AppClient.getVersion());
        jsonRequest.setOsType(AppClient.getOSType());
        jsonRequest.setLang(AppClient.getLanguage());
        jsonRequest.setThemeCode(ThemeUtils.getThemeCode(ProjectApp.getContext()));
//        if (UserInfoManager.getInstance().isLogin()) {
//            jsonRequest.setSessionId(AppClient.getInstance().getLastSessionID());
//            jsonRequest.setUserId(AppClient.getInstance().getLastUserID());
//        } else {
        jsonRequest.setSessionId(AppClient.getInstance().getSessionID());
        jsonRequest.setUserId(AppClient.getInstance().getUserID());
//        }
        if (!TextUtils.isEmpty(SpUtils.getInstance().getDeviceToken())) {
            jsonRequest.setDeviceToken(SpUtils.getInstance().getDeviceToken());
        } else if (!TextUtils.isEmpty(UserInfoManager.getInstance().getmGcmDeviceToken())) {
            jsonRequest.setDeviceToken(UserInfoManager.getInstance().getmGcmDeviceToken());
        } else {
            jsonRequest.setDeviceToken(FirebaseInstanceId.getInstance().getToken());
        }
        jsonRequest.setDeviceId(AppClient.getInstance().getDeviceID());
        jsonRequest.setDeviceModel(AppClient.getInstance().getDeviceModel());
        jsonRequest.setServiceBody(httpParams.params);

        LogUtils.d("public_key", AppConfig.SERVER_PUBLIC_KEY);
        //判断是否需要上传文件
        if (uploadPart.size() == 0) {
            //如果不需要上传就直接生成RequestBody
            builder.post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), ApiHelper.GSON.toJson(jsonRequest)));
            builder.url(AppClient.getServerUrl());
        } else {
            //否真需要用到MultipartBody来生成Body
            MultipartBody.Builder multipartBody = new MultipartBody.Builder();
            for (Map.Entry<String, List<String>> entry : uploadPart.entrySet()) {
                for (String path : entry.getValue()) {
                    File file = new File(path);
                    if (file.exists()) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                        multipartBody.addFormDataPart(entry.getKey(), file.getName(), requestFile);
                    }

                }
            }
            multipartBody.addFormDataPart(AppConfig.MULTIPART_CONTENT_NAME, ApiHelper.GSON.toJson(jsonRequest));
            multipartBody.setType(MultipartBody.FORM);
            builder.post(multipartBody.build());
            builder.url(AppClient.getServerUploadUrl());
        }
        return ApiHelper.getInstance().getOkHttpClient().newCall(builder.build());
    }

    //把HttpParams构建成一个OkHttp的call
    private static Call mapBuildCall(String url, Map<String, Object> headMap) {
        Request.Builder builder = new Request.Builder();

        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
        String urlNew = url;
        urlNew+=getBodyParams(headMap);
        Request request = builder
                .url(urlNew)
                .get()
                .build();
        return client.newCall(request);
    }


    //添加参数
    private static String getBodyParams(Map<String, Object> bodyParams) {
        //1.添加请求参数
        //遍历map中所有参数到builder
        if (bodyParams != null && bodyParams.size() > 0) {
            StringBuffer stringBuffer = new StringBuffer("?");
            for (String key : bodyParams.keySet()) {
                if (bodyParams.get(key) != null) {//如果参数不是null，就拼接起来
                    stringBuffer.append("&");
                    stringBuffer.append(key);
                    stringBuffer.append("=");
                    stringBuffer.append(bodyParams.get(key));
                }
            }

            return stringBuffer.toString();
        } else {
            return "";
        }
    }

    //把HttpParams构建成一个OkHttp的call
    private static Call ocrBuildCall(String url, HttpParams httpParams) {
        Map<String, List<String>> uploadPart = httpParams.uploadPart;
        Request.Builder builder = new Request.Builder();

        OkHttpClient client = new OkHttpClient();
        String content = ApiHelper.GSON.toJson(httpParams.params);
        MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
        RequestBody body = RequestBody.create(mediaType, content);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("accept-language", "en-US,en;q=0.8")
                .addHeader("accept", "*/*")
                .build();

//        //如果不需要上传就直接生成RequestBody
//        builder.post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), ApiHelper.GSON.toJson(ocrJsonRequest)));
//        builder.url(url);
//
//        for (String header : httpParams.headers) {
//            String[] keyAndValue = header.split(":");
//            if (keyAndValue.length == 2) {
//                builder.addHeader(keyAndValue[0], keyAndValue[1]);
//            }
//        }
        return client.newCall(request);
    }

    //回调都是在主线程
    private static void callFailure(final ResponseCallback callback, final Throwable e) {
        if (callback != null) {
            HANDLER.post(new Runnable() {
                @Override
                public void run() {
                    callback.onFailure(e);
                }
            });
        }
    }

    //回调都是在主线程
    private static <T> void callSuccessful(final ResponseCallback<T> callback, final T response) {
        if (callback != null) {
            HANDLER.post(new Runnable() {
                @Override
                public void run() {
                    callback.onResponse(response);
                }
            });
        }
    }
}