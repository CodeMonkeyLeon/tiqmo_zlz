package cn.swiftpass.wallet.tiqmo.sdk.net;


import android.os.Environment;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.crypto.SecretKey;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.wallet.tiqmo.BuildConfig;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.AppConfig;
import cn.swiftpass.wallet.tiqmo.sdk.entity.DecryptedResponse;
import cn.swiftpass.wallet.tiqmo.sdk.entity.EncryptedRequest;
import cn.swiftpass.wallet.tiqmo.sdk.net.annotation.AnnotationParser;
import cn.swiftpass.wallet.tiqmo.sdk.util.AESUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.Base64Util;
import cn.swiftpass.wallet.tiqmo.sdk.util.ECCUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.ECDHUtil;
import cn.swiftpass.wallet.tiqmo.sdk.util.LogUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.LogUtils;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

//这个类非常重要，包含了接口调用中各种加解密的操作
@SuppressWarnings("unchecked")
public class ApiHelper {
    public static final String SSL_KEY_ALIAS = "Tiqmo";
    private static final String ECC_ELLIPTIC_CURVE = "prime256v1";

    public static final Gson GSON;
    //    public static final OkHttpClient OK_HTTP_CLIENT;
    public static final OkHttpClient OCR_OK_HTTP_CLIENT;
    private static String CLIENT_PUBLIC_KEY;
    private static SecretKey AES_KEY;
    //    private static final Signature CLIENT_SIGNATURE;
//    private static final Signature SERVER_SIGNATURE;
    private static PrivateKey clientPrivateKey;
    private static PublicKey serverPublicKey;

    //全后端目前使用ECDH加解密，这里首先对全部密钥做初始化
    //根据不同的系统版本，androidKeyStore不一定支持prime256v1，不支持的时候就要用到BC
    public static void createKey() {
        //生成KeyPair
        KeyPair ecKeyPair = ECCUtil.generateECCKeyPair(ECC_ELLIPTIC_CURVE);
        if (ecKeyPair == null) {
            ecKeyPair = ECCUtil.generateECCKeyPair(ECC_ELLIPTIC_CURVE, "BC");
        }
        //获取公私钥
        PublicKey clientPublicKey = ecKeyPair.getPublic();
        clientPrivateKey = ecKeyPair.getPrivate();

        //初始化服务器的公钥
        serverPublicKey = ECCUtil.loadECPublicKey(Base64Util.decode(AppConfig.SERVER_PUBLIC_KEY));
        if (serverPublicKey == null) {
            serverPublicKey = ECCUtil.loadECPublicKey(Base64Util.decode(AppConfig.SERVER_PUBLIC_KEY), "BC");
        }

        //进行ECDH合成，合出一把AES KEY
        SecretKey aesKey = AESUtil.loadKey(ECDHUtil.ecdh(clientPrivateKey, serverPublicKey));
        if (aesKey == null) {
            aesKey = AESUtil.loadKey(ECDHUtil.ecdh(clientPrivateKey, serverPublicKey, "BC"));
        }
        AES_KEY = aesKey;

//        //签名初始化
//        Signature clientSignature = ECCUtil.loadSignature(clientPrivateKey);
//        if (clientSignature == null) {
//            clientSignature = ECCUtil.loadSignature(clientPrivateKey, "BC");
//        }
//        CLIENT_SIGNATURE = clientSignature;

        //签名初始化
//        Signature serverSignature = ECCUtil.loadSignature(serverPublicKey);
//        if (serverSignature == null) {
//            serverSignature = ECCUtil.loadSignature(serverPublicKey, "BC");
//        }
//        SERVER_SIGNATURE = serverSignature;

        CLIENT_PUBLIC_KEY = Base64Util.encodeToString(clientPublicKey.getEncoded());
    }

    private static ApiHelper apiHelperInstance;

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    public void setmOkHttpClient(final OkHttpClient mOkHttpClient) {
        this.mOkHttpClient = mOkHttpClient;
    }

    private OkHttpClient mOkHttpClient;

    /**
     * 创建 单例模式（OkHttp官方建议如此操作）
     */
    public static ApiHelper getInstance() {
        if (apiHelperInstance == null) {
            apiHelperInstance = new ApiHelper();
        }
        return apiHelperInstance;
    }


    private ApiHelper() {
        try {
            mOkHttpClient = getBuilder(true)
                    .connectTimeout(90, TimeUnit.SECONDS)
                    .readTimeout(90, TimeUnit.SECONDS)
                    .writeTimeout(90, TimeUnit.SECONDS)
                    .build();
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");

        }

    }

    /**
     * 添加自定义拦截器  拦截器处理request顺序按依赖关系排序
     *
     * @return
     */
    public OkHttpClient.Builder getBuilder(boolean isAddInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new EncryptDecodeInterceptor());

        try {
            if (getInputStream() == null) {
                if (BuildConfig.DOWNLOAD_CER_SERVER) {
//                    builder = setCertificates(builder, ProjectApp.getContext().getAssets().open("tiqmo_sit.cer"));
                    builder = setCertificates(builder, ProjectApp.getContext().getAssets().open("tiqmo_k8s.cer"));
                }
            } else {
                builder = setCertificates(builder, getInputStream());
            }
        } catch (Exception e) {
        }
        return builder;
    }

    public InputStream getInputStream() {
        String filePath = ProjectApp.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File file = new File(filePath);
        File[] filenameList = file.listFiles();
        InputStream inputStream = null;
        for (int i = 0; i < filenameList.length; i++) {
            String filename = filenameList[i].getName();
            if (!filename.endsWith(".crt") && !filename.endsWith(".cer")) {
                continue;
            }
            try {
                inputStream = new FileInputStream(filenameList[i]);
            } catch (FileNotFoundException e) {
                LogUtils.d("errorMsg", "---"+e+"---");
            }
        }
        return inputStream;
    }

    //这里对GSON进行初始化，全局就用到GSON一个单例
    static {
        GSON = new GsonBuilder()
                .serializeNulls()
                .enableComplexMapKeySerialization()
                .disableHtmlEscaping()
                .registerTypeAdapter(String.class, new TypeAdapter<String>() {
                    @Override
                    public String read(JsonReader reader) throws IOException {
                        if (reader.peek() == JsonToken.NULL) {
                            reader.nextNull();
                            return "";
                        }
                        return reader.nextString();
                    }

                    @Override
                    public void write(JsonWriter writer, String value) throws IOException {
                        if (value == null) {
                            writer.value("");
                            return;
                        }
                        writer.value(value);
                    }
                })
                .create();
    }

    public static OkHttpClient.Builder setCertificates(OkHttpClient.Builder client, InputStream... certificates) {
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            int index = 0;
            for (InputStream certificate : certificates) {
                String certificateAlias = Integer.toString(index++);
                keyStore.setCertificateEntry(certificateAlias, certificateFactory.generateCertificate(certificate));
                try {
                    if (certificate != null) certificate.close();
                } catch (IOException e) {
                    LogUtils.d("errorMsg", "---"+e+"---");

                }
            }
            SSLContext sslContext = SSLContext.getInstance("SSL");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            final X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            client.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return client;
    }

    public static OkHttpClient.Builder setBuilder(OkHttpClient.Builder builder, Certificate[] mCertificates) {
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(readKeyStore(mCertificates));
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            builder.sslSocketFactory(sslContext.getSocketFactory());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession sslSession) {
                    if(BuildConfig.ServerUrl.contains(hostname)) {
                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return builder;
    }

    public static KeyStore readKeyStore(Certificate[] mCertificates) {
        KeyStore keyStore = null;
        CertificateFactory certificateFactory = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }

        if (null == keyStore || null == certificateFactory) {
            return null;
        }
        try {
            int index = 0;
            for (int i = 0; i < mCertificates.length; i++) {
                Certificate cert = mCertificates[i];
                String certificateAlias = Integer.toString(index++);
                keyStore.setCertificateEntry(certificateAlias, cert);
            }
        } catch (Exception e) {
            LogUtils.d("errorMsg", "---"+e+"---");
        }
        return keyStore;
    }

    //这里对OCR_OKHTTP进行初始化
    static {
        OCR_OK_HTTP_CLIENT = new OkHttpClient();
    }

    public static <T> T getApi(Class<T> c) {
        return (T) Proxy.newProxyInstance(c.getClassLoader(), new Class[]{c}, new ApiProxyHandler());
    }

    //动态代理，主要是对注解进行解密
    private static class ApiProxyHandler implements InvocationHandler {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) {
            Class<?> returnType = method.getReturnType();
            //判断返回值类型
            if (returnType == RequestCall.class) {
                //获取返回值的泛型类型
                Type genericReturnType = method.getGenericReturnType();
                if (genericReturnType instanceof ParameterizedType) {
                    Type type = ((ParameterizedType) genericReturnType).getActualTypeArguments()[0];
                    if (type instanceof WildcardType) {
                        //如果是通配符就报错
                        throw new RuntimeException("The \"" + method.getName() + "\" method must explicitly declare the generic parameters of the returned value");
                    }
                    return new RequestCall<>(AnnotationParser.parse(method, args), type);
                } else {
                    throw new RuntimeException("The return value of \"" + method.getName() + "\" must explicitly declare generic parameters");
                }
            } else {
                throw new RuntimeException("The return value type of the \"" + method.getName() + "\" method must be " + RequestCall.class);
            }
        }
    }

    //这个拦截器主要拦截请求和响应并对他们加解密
    private static class EncryptDecodeInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            //对请求进行加密
            Response response = chain.proceed(EncryptRequest(request));
            //对请求进行解密
            return decryptResponse(response, request.header("Service-Id"));
        }

        private Request EncryptRequest(Request request) {
            String content = null;
            String encryptedContent = null;

            Request.Builder newBuilder = request.newBuilder();

            RequestBody requestBody = request.body();
            //判断请求是普通的body还是带上传文件功能的MultipartBody
            //无论那种，本质都是为了找到参数部分的body并对其进行加密
            if (requestBody instanceof MultipartBody) {
                MultipartBody multipartBody = (MultipartBody) requestBody;
                MultipartBody.Builder bodyBuilder = new MultipartBody.Builder();
                for (MultipartBody.Part part : multipartBody.parts()) {
                    Headers headers = part.headers();
                    if (headers != null && headers.toString().contains(AppConfig.MULTIPART_CONTENT_NAME)) {
                        content = readContentFromRequestBody(part.body());
                        encryptedContent = encrypt(newBuilder, content, true);//加密body
                        bodyBuilder.addPart(MultipartBody.Part.createFormData(AppConfig.MULTIPART_CONTENT_NAME, encryptedContent));
                    } else {
                        bodyBuilder.addPart(part);
                    }
                }
                bodyBuilder.setType(multipartBody.type());
                newBuilder.method(request.method(), bodyBuilder.build());
            } else {
                content = readContentFromRequestBody(requestBody);
                encryptedContent = encrypt(newBuilder, content, false);//加密body
                newBuilder.method(request.method(), RequestBody.create(MediaType.parse("application/json; charset=utf-8"), encryptedContent));
            }

            //打印
            request = newBuilder.build();
            LogUtil.d(String.format(
                    Locale.getDefault(),
                    "\n正在发出请求：url = %s  ,  header = %s\n打印请求内容：%s\n完整加密内容：%s",
                    request.url(),
                    request.headers().toString().replace("\n", ","),
                    content,
                    encryptedContent
            ));
            return request;
        }

        //解密并校验response
        private Response decryptResponse(Response response, String requestID) throws IOException {
            String content = "";
            String decryptedContent = "";
            MediaType contentType = null;

            //先判断请求是否成功
            if (response.isSuccessful()) {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    contentType = responseBody.contentType();
                    content = responseBody.string();
                    if (!TextUtils.isEmpty(content)) {
                        decryptedContent = decrypt(response, content);//解密
                    }
                }
            }
            //打印
            String responseMsg = String.format(
                    Locale.getDefault(),
                    "\n正在处理响应：Service-Id = %s  ,  HTTP code = %s\n打印响应内容：%s\n解密响应内容：%s",
                    requestID,
                    response.code(),
                    content,
                    TextUtils.isEmpty(decryptedContent) ? "解密或验签失败" : decryptedContent
            );
            if (responseMsg.length() > 4000) {
                // 打印超过4000字符的日志信息
                for (int i = 0; i < responseMsg.length(); i += 4000) {
                    if (i + 4000 < responseMsg.length()) {
                        LogUtil.d("Service-Id " + i + "\n" + responseMsg.substring(i, i + 4000));
                    } else {
                        LogUtil.d("Service-Id " + i + "\n" + responseMsg.substring(i, responseMsg.length()));
                    }
                }
            } else {
                LogUtil.d(responseMsg);
            }
            return response.newBuilder()
                    .body(ResponseBody.create(contentType, decryptedContent == null ? "" : decryptedContent))
                    .build();
        }

        //请求加密
        private static String encrypt(Request.Builder builder, String content, boolean isMulitpart) {
            builder.addHeader("partnerNo", AppClient.getPartnerNo());
            //判断是否需要启动加解密模式
            if(!AppClient.isEnableEncryptMode()){
                if (isMulitpart) {
                    return content;
                } else {
                    return String.format("{\"content\":%s}",content);
                }
            }


            //先生成Body的签名
            String signature = Base64Util.encodeToString(ECCUtil.sign(clientPrivateKey, content.getBytes()));
            //对Body进行加密
            String encryptedContent = Base64Util.encodeToString(AESUtil.encrypt(content.getBytes(), AES_KEY, null));
            //将签名和客户端的公钥写入header
            builder.addHeader("Signature-Data", signature);
            builder.addHeader("Public-Key", CLIENT_PUBLIC_KEY);

            //如果是上传文件类型就直接返回，否真还要包一层json，这里真的很蠢，只要你看了body的格式你就知道，最外层强行有一个content，呵呵
            if (isMulitpart) {
                //不用包一层content
                return encryptedContent;
            } else {
                //需要包一层content
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("content", encryptedContent);
                return GSON.toJson(new EncryptedRequest(encryptedContent));
            }
        }

        //解密
        private static String decrypt(Response response, String content) {
            //判断是否需要启动加解密模式
            if(!AppClient.isEnableEncryptMode()){
                JSONObject object;
                try {
                    object = new JSONObject(content);
                    DecryptedResponse decryptedResponse = new DecryptedResponse();
                    decryptedResponse.setCode(object.getString("code"));
                    decryptedResponse.setMsg(object.getString("msg"));
                    if(ResponseCallbackWrapper.RESPONSE_CODE_SUCCESS.equals(decryptedResponse.getCode())&&!object.isNull("content")){
                        decryptedResponse.setContent(object.getJSONObject("content").toString());
                    }
                    return  GSON.toJson(decryptedResponse);
                } catch (JSONException e) {
                    LogUtils.d("errorMsg", "---"+e+"---");
                    return null;
                }
            }


            DecryptedResponse decryptedResponse;
            try {
                //先转成对象
                decryptedResponse = GSON.fromJson(content, DecryptedResponse.class);
            } catch (JsonSyntaxException e) {
                LogUtil.w(e.toString(), e);
                return null;
            }
            if (decryptedResponse == null) {
                return null;
            }

            String responseContent = decryptedResponse.getContent();
            if (!TextUtils.isEmpty(responseContent)) {
                //解密内容
                byte[] decryptContent = AESUtil.decrypt(Base64Util.decode(responseContent), AES_KEY, null);
                //获取签名
                String signature = response.header("Signature-Data");
                if (TextUtils.isEmpty(signature)) {
                    return null;
                }

                Signature serverSignature = ECCUtil.loadSignature(serverPublicKey);
                if (serverSignature == null) {
                    serverSignature = ECCUtil.loadSignature(serverPublicKey, "BC");
                }

                //用签名校验内容，判断是否被篡改
                byte[] signDate = Base64Util.decode(signature);
                if (!ECCUtil.verify(serverSignature, decryptContent, signDate)) {
                    return null;
                }
//                if (!ECCUtil.verify(SERVER_SIGNATURE, decryptContent, signDate)) {
//                    return null;
//                }
                decryptedResponse.setContent(new String(decryptContent));
            }
            return GSON.toJson(decryptedResponse);
        }

        //工具方法，从Body读取内容成String
        private static String readContentFromRequestBody(RequestBody body) {
            Buffer buffer = null;
            try {
                if (body != null && body.contentLength() > 0) {
                    buffer = new Buffer();
                    body.writeTo(buffer);
                    return buffer.readString(StandardCharsets.UTF_8);
                }
            } catch (IOException e) {
                LogUtil.d(e.toString(), e);
            } finally {
                if (buffer != null) {
                    buffer.close();
                }
            }
            return "";
        }
    }
}

