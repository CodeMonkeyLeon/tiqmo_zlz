package cn.swiftpass.wallet.tiqmo.module.home.contract;


import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.AllMarketInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.HomeAnalysisEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.InviteEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.MarketDetailsEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.ReferralCodeDescEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.SendReceiveEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.EVoucherEntity;
import cn.swiftpass.wallet.tiqmo.module.voucher.entity.VouSupCountryEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.HomeIconEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.UserInfoEntity;

public class HomeFragmentContract {

    public interface View extends BaseView<Presenter> {

        void getUserInfoSuccess(UserInfoEntity userInfoEntity);

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void showMarketList(AllMarketInfoEntity result);

        void showMarketDetailsDia(MarketDetailsEntity result);

        void showMarketDetailsForRefCodeDia(ReferralCodeDescEntity result);

        void showVoucherBrandList(EVoucherEntity result);

        void hideVoucherBrandList(String errCode, String errMsg);

        void showEVouCountryDia(VouSupCountryEntity result, String providerCode, String brandCode, String brandName);

        void showTopUpSelectDia(ArrayList<EVoucherEntity.StoreInfo> data);

        void showOrderRepeatDia(ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType);

        void checkOrderStatusSuccess(ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType);

        void showErrorMsg(String errCode, String errMsg);

        void showServicesList(List<HomeIconEntity> iconList);

        void getPayBillOrderListSuccess(PayBillOrderListEntity payBillOrderListEntity);

        void getHomeAnalysisSuccess(HomeAnalysisEntity homeAnalysisEntity);

        void getTopUpOrderListSuccess(RechargeOrderListEntity rechargeOrderListEntity);

        void getInviteDetail(InviteEntity inviteEntity);

        void getSendReceiveSuccess(List<SendReceiveEntity> sendReceiveList);

        void getAnalysisDetailSuccess(SpendingDetailEntity spendingDetailEntity);
    }


    public interface Presenter extends BasePresenter<View> {

        void getAnalysisDetail(String periodType, String spendingAnalysisQueryDate);
        void getHomeAnalysis();

        void getUserInfo(boolean isLoading);

        void autoLogin();

        void getConfig();

        void getLimit(String type);

        void getAllMarket();

        void getMarketDetails(String activityNo);

        void getRefCodeDescDetails();

        void requestVoucherBrand();

        void requestSupCountry(String providerCode, String brandCode, String brandName);

        void getTopUpList();

        void checkOrderStatus(ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType);

        void skipToVouTypeAndInfo(Activity activity, String providerCode, String brandCode, String brandName, String countryCode);

        void continueToSelectTickets(Activity activity, ArrayList<EVoucherEntity.StoreInfo> data, int position, String sceneType);

        void getServicesList();

        void getPayBillOrderList();

        void getTopUpOrderList();

        void getInviteDetail();

        void getSendReceive();
    }
}
