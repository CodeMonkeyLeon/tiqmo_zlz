package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import cn.swiftpass.wallet.tiqmo.module.imr.contract.ImrSendMoneyContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPurposeListEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.CheckOutEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class ImrSendMoneyPresenter implements ImrSendMoneyContract.Presenter {

    ImrSendMoneyContract.View mView;

    @Override
    public void imrCheckoutOrder(String orderAmount, String orderCurrency, String payeeId, String purpose, String receiptMethod, String includeFee,
                                 String purposeId, String purposeDesc,String channelCode,
                                 String transFer,
                                 String destinationAmount,
                                 String destinationCurrency,
                                 String payAmount,
                                 String exchangeRate) {
        AppClient.getInstance().imrCheckoutOrder(orderAmount, orderCurrency, payeeId, purpose,
                receiptMethod, includeFee,purposeId,purposeDesc,channelCode,
                transFer,
                destinationAmount,
                destinationCurrency,
                payAmount,exchangeRate,new LifecycleMVPResultCallback<ImrOrderInfoEntity>(mView, true) {
                    @Override
                    protected void onSuccess(ImrOrderInfoEntity result) {
                        mView.imrCheckoutOrderSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mView.imrCheckoutOrderFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void getImrPurposeList(String countryCode,String channelCode) {
        AppClient.getInstance().getImrPurposeList(countryCode,channelCode, new LifecycleMVPResultCallback<ImrPurposeListEntity>(mView,true) {
            @Override
            protected void onSuccess(ImrPurposeListEntity result) {
                mView.getImrPurposeListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.getImrPurposeListFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void imrExchangeRate(String payeeInfoId, String sourceAmount, String destinationAmount, String channelCode) {
        AppClient.getInstance().imrExchangeRate(payeeInfoId, sourceAmount,
                destinationAmount, channelCode, new LifecycleMVPResultCallback<ImrExchangeRateEntity>(mView, true) {
                    @Override
                    protected void onSuccess(ImrExchangeRateEntity result) {
                        mView.imrExchangeRateSuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        mView.imrExchangeRateFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void checkOut(String orderNo, String orderInfo) {
        AppClient.getInstance().checkOut(orderNo, orderInfo, new LifecycleMVPResultCallback<CheckOutEntity>(mView, true) {
            @Override
            protected void onSuccess(CheckOutEntity result) {
                mView.checkOutSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mView.checkOutFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(ImrSendMoneyContract.View view) {
        this.mView = view;
    }

    @Override
    public void detachView() {
        this.mView = null;
    }
}
