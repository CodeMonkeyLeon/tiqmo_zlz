package cn.swiftpass.wallet.tiqmo.module.home.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.home.contract.AnalysisContract;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class SpendingBudgetPresenter implements AnalysisContract.BudgetPresenter {

    AnalysisContract.BudgetView mBudgetView;

    @Override
    public void getSpendingBudgetList() {
        AppClient.getInstance().getTransferManager().getSpendingBudgetList(new LifecycleMVPResultCallback<SpendingBudgetListEntity>(mBudgetView, true) {
            @Override
            protected void onSuccess(SpendingBudgetListEntity result) {
                mBudgetView.getSpendingBudgetListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBudgetView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void saveUserSpending(List<SpendingBudgetEntity> userSpendingBudgetSaveOrUpdateInfoList, List<String> deleteBudgetIdList) {
        AppClient.getInstance().getTransferManager().saveUserSpending(userSpendingBudgetSaveOrUpdateInfoList, deleteBudgetIdList, new LifecycleMVPResultCallback<Void>(mBudgetView, true) {
            @Override
            protected void onSuccess(Void result) {
                mBudgetView.saveUserSpendingSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mBudgetView.showErrorMsg(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void attachView(AnalysisContract.BudgetView budgetView) {
        this.mBudgetView = budgetView;
    }

    @Override
    public void detachView() {
        this.mBudgetView = null;
    }
}
