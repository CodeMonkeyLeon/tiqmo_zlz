package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;

public class CardDeleteDialog extends BottomDialog {
    private Context mContext;

    private ConfirmListener confirmListener;

    private CardEntity cardEntity;

    public void setCardEntity(final CardEntity cardEntity) {
        this.cardEntity = cardEntity;
    }

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public CardDeleteDialog(Context context,CardEntity cardEntity) {
        super(context);
        this.mContext = context;
        this.cardEntity = cardEntity;
        initViews(mContext,cardEntity);
    }

    public interface ConfirmListener {
        void clickConfrim(CardEntity cardEntity);
    }

    private void initViews(Context mContext,CardEntity cardEntity) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_delete_card, null);
        TextView tvSelectTitle = view.findViewById(R.id.tv_select_title);
        TextView tvConfirm = view.findViewById(R.id.tv_confirm);
        TextView tvCancel = view.findViewById(R.id.tv_cancel);
        tvSelectTitle.setText(mContext.getString(R.string.sprint11_39).replace("XXX",cardEntity.getCardNo().substring(12)));
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(confirmListener != null){
                    confirmListener.clickConfrim(cardEntity);
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }
}
