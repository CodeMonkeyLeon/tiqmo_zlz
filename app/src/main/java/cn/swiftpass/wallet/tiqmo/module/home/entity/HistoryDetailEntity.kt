package cn.swiftpass.wallet.tiqmo.module.home.entity

import java.io.Serializable

data class HistoryDetailEntity(
    val orderNo: String?,
    val orderType: String?,
    val queryType: String?
) : Serializable
