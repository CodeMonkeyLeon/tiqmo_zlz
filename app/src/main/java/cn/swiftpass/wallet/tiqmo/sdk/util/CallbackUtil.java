package cn.swiftpass.wallet.tiqmo.sdk.util;

import android.os.Handler;
import android.os.Looper;

import cn.swiftpass.wallet.tiqmo.sdk.listener.ResultCallback;

public class CallbackUtil {
    private static Handler sUIHandler = new Handler(Looper.getMainLooper());

    public static <T> void callResult(final T result, final ResultCallback<T> callback) {
        if (callback != null) {
            if (Thread.currentThread().getId() != Looper.getMainLooper().getThread().getId()) {
                sUIHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onResult(result);
                    }
                });
            } else {
                callback.onResult(result);
            }
        }
    }

    public static void callFailure(final String code, final String error, final ResultCallback callback) {
        if (callback != null) {
            if (Thread.currentThread().getId() != Looper.getMainLooper().getThread().getId()) {
                sUIHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure(code, error);
                    }
                });
            } else {
                callback.onFailure(code, error);
            }
        }
    }
}
