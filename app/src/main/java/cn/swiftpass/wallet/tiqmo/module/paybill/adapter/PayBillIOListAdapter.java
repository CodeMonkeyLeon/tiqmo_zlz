package cn.swiftpass.wallet.tiqmo.module.paybill.adapter;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.imr.dialog.ImrInfoSelectDialog;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.SelectInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillIOEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.SelectOptionListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.view.PayBillFetchActivity;
import cn.swiftpass.wallet.tiqmo.module.paybill.view.PayBillMoiFetchActivity;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.NormalInputFilter;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseViewHolder;

public class PayBillIOListAdapter extends BaseRecyclerAdapter<PayBillIOEntity> {

    private boolean isMoi;
    private String selectKey;

    public PayBillIOListAdapter(@Nullable List<PayBillIOEntity> data,boolean isMoi) {
        super(R.layout.item_paybill_io_list, data);
        this.isMoi = isMoi;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, PayBillIOEntity payBillIOEntity, int position) {
        if (payBillIOEntity != null) {
            String name = payBillIOEntity.name;
            String errorMsg = payBillIOEntity.errorMsg;
            String dataType = payBillIOEntity.datatype;
            String value = payBillIOEntity.value;

            ConstraintLayout clSelect = baseViewHolder.getView(R.id.id_cl_select);
            EditTextWithDel etSelect = baseViewHolder.getView(R.id.id_et_select);
            EditTextWithDel etIOContent = baseViewHolder.getView(R.id.et_io_content);
            etSelect.getTlEdit().setHint(mContext.getString(R.string.IMR_new_10));
            TextView tvDesc = baseViewHolder.getView(R.id.tv_io_desc);
            if (etIOContent.getTlEdit() != null) {
                etIOContent.getTlEdit().setHintAnimationEnabled(false);
            }
            if (etSelect.getTlEdit() != null) {
                etSelect.getTlEdit().setHintAnimationEnabled(false);
            }
            etSelect.setFocusableFalse();

            if (TextUtils.equals(dataType, PayBillIOEntity.TYPE_SELECT)) {
                setSelectLayoutVisible(true, clSelect, etIOContent);
                etSelect.getTlEdit().setHint(name);
                if (!TextUtils.isEmpty(value)) {
                    saveEditData(position, value);
                    etSelect.setContentText(value);
                }
                etSelect.getEditText().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSelectDialog(name, payBillIOEntity.selectOptionList, etSelect);
                    }
                });
                etSelect.setTag(position);
                etSelect.getEditText().addTextChangedListener(new TextSwitcher(baseViewHolder, PayBillIOEntity.TYPE_SELECT,tvDesc,payBillIOEntity.description));
            } else {
                setSelectLayoutVisible(false, clSelect, etIOContent);
                if (position == getDataList().size() - 1) {
                    etIOContent.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(30), new NormalInputFilter(NormalInputFilter.CHARSEQUENCE__NUMBER_SPECIAL_SPACE)});
                }
                etIOContent.getTlEdit().setHint(name);
                etIOContent.setTag(position);
                if (!TextUtils.isEmpty(value)) {
                    saveEditData(position, value);
                    etIOContent.setContentText(value);
                }
                etIOContent.getEditText().addTextChangedListener(new TextSwitcher(baseViewHolder, "",tvDesc,payBillIOEntity.description));
            }

            if (!TextUtils.isEmpty(errorMsg)) {
                tvDesc.setTextColor(mContext.getColor(R.color.color_fd1d2d));
                tvDesc.setText(errorMsg);
            } else {
                tvDesc.setTextColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_40white_403a3b44)));
                baseViewHolder.setText(R.id.tv_io_desc, payBillIOEntity.description);
            }


        }
    }


    private void saveEditData(int position, String value) {
        if(isMoi){
            ((PayBillMoiFetchActivity) mContext).saveEditData(position, value);
        }else {
            ((PayBillFetchActivity) mContext).saveEditData(position, value);
        }
    }


    private List<SelectInfoEntity> transferData(List<SelectOptionListEntity> list) {
        List<SelectInfoEntity> resultList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (SelectOptionListEntity e : list) {
                SelectInfoEntity selectInfo = new SelectInfoEntity();
                selectInfo.businessParamKey = e.getKey();
                selectInfo.businessParamValue = e.getValue();
                resultList.add(selectInfo);
            }
        }
        return resultList;
    }


    private void showSelectDialog(String title, List<SelectOptionListEntity> list, EditTextWithDel etSelect) {
        List<SelectInfoEntity> selectInfoEntityList = transferData(list);
        ImrInfoSelectDialog dialog = ImrInfoSelectDialog.getInstance(selectInfoEntityList, title, false, false);
        dialog.setOnItemClickListener(new ImrInfoSelectDialog.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                SelectInfoEntity selectInfoEntity = selectInfoEntityList.get(position);
                if (selectInfoEntity != null) {
                    String key = selectInfoEntity.businessParamKey;
                    String value = selectInfoEntity.businessParamValue;
                    selectKey = key;
                    etSelect.getEditText().setText(value);
                }
            }
        });


        if(isMoi){
            FragmentManager fragmentManager = ((PayBillMoiFetchActivity) mContext).getSupportFragmentManager();
            dialog.showNow(fragmentManager, "PayBillMoiFetchActivity");
        }else {
            FragmentManager fragmentManager = ((PayBillFetchActivity) mContext).getSupportFragmentManager();
            dialog.showNow(fragmentManager, "PayBillFetchActivity");
        }
    }


    private void setSelectLayoutVisible(boolean isVisible, ConstraintLayout clSelect, EditTextWithDel etIOContent) {
        if (isVisible) {
            clSelect.setVisibility(View.VISIBLE);
            etIOContent.setVisibility(View.GONE);
        } else {
            clSelect.setVisibility(View.GONE);
            etIOContent.setVisibility(View.VISIBLE);
        }
    }


    class TextSwitcher implements TextWatcher {
        private BaseViewHolder mHolder;
        private String mType;
        private TextView mTvDesc;
        private String mDescription;

        public TextSwitcher(BaseViewHolder holder, String type,TextView tvDesc,String description) {
            mHolder = holder;
            mType = type;
            mTvDesc = tvDesc;
            mDescription = description;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mTvDesc.setTextColor(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_40white_403a3b44)));
            mTvDesc.setText(mDescription);
            String content = s.toString();
            int position = 0;
            if (TextUtils.equals(mType, PayBillIOEntity.TYPE_SELECT)) {
                position = (int) mHolder.getView(R.id.id_et_select).getTag();//取tag值
                content = selectKey;
            } else {
                position = (int) mHolder.getView(R.id.et_io_content).getTag();//取tag值
            }
            saveEditData(position, content);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
