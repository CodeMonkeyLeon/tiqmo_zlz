package cn.swiftpass.wallet.tiqmo.module.topup.presenter;

import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOperatorListEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeProductListEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class RechargeOperatorPresenter implements RechargeContract.RechargeOperatorPresenter{

    RechargeContract.RechargeOperatorView mRechargeOperatorView;

    @Override
    public void getRechargeOperator(String phoneNumber) {
        AppClient.getInstance().getTransferManager().getRechargeOperatorList(phoneNumber, new LifecycleMVPResultCallback<RechargeOperatorListEntity>(mRechargeOperatorView,true) {
            @Override
            protected void onSuccess(RechargeOperatorListEntity result) {
                mRechargeOperatorView.getRechargeOperatorSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeOperatorView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getRechargeProductList(String operatorId) {
        AppClient.getInstance().getTransferManager().getRechargeProductList(operatorId, new LifecycleMVPResultCallback<RechargeProductListEntity>(mRechargeOperatorView,true) {
            @Override
            protected void onSuccess(RechargeProductListEntity result) {
                mRechargeOperatorView.getRechargeProductListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                mRechargeOperatorView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(RechargeContract.RechargeOperatorView rechargeOperatorView) {
        this.mRechargeOperatorView = rechargeOperatorView;
    }

    @Override
    public void detachView() {
        this.mRechargeOperatorView = null;
    }
}
