package cn.swiftpass.wallet.tiqmo.module.setting.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.QuestionDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.contract.HelpContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpListEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.HelpThreeEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.presenter.HelpPresenter;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.widget.EditTextWithDel;

public class QuestionDetailActivity extends BaseCompatActivity<HelpContract.HelpListPresenter> implements HelpContract.HelpListView {

    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_question_arrow)
    ImageView ivQuestionArrow;
    @BindView(R.id.tv_question_content)
    TextView tvQuestionContent;
    @BindView(R.id.tv_question_title)
    TextView tvQuestionTitle;
    @BindView(R.id.cb_good)
    CheckBox cbGood;
    @BindView(R.id.cb_bad)
    CheckBox cbBad;
    @BindView(R.id.et_improve)
    EditTextWithDel etImprove;
    @BindView(R.id.tv_submit)
    TextView tvSubmit;
    @BindView(R.id.tv_submit_success)
    TextView tvSubmitSuccess;
    @BindView(R.id.ll_question_content)
    LinearLayout llQuestionContent;
    @BindView(R.id.ll_question)
    LinearLayout llQuestion;
    @BindView(R.id.ll_submit_content)
    LinearLayout llSubmitContent;
    @BindView(R.id.ll_submit_success)
    LinearLayout llSubmitSuccess;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;

    private HelpThreeEntity helpThreeEntity;
    //0点赞 1反对
    private String thumb;
    private String id,comment;

    private List<QuestionDetailEntity> questionDetailList = new ArrayList<>();

    @Override
    protected int getLayoutID() {
        return R.layout.activity_question_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        tvTitle.setText(R.string.Help_support_1);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        etImprove.getTlEdit().setHint(getString(R.string.DigitalCard_86));
        if(getIntent() != null && getIntent().getExtras() != null){
            helpThreeEntity = (HelpThreeEntity) getIntent().getExtras().getSerializable("helpThreeEntity");
            if(helpThreeEntity != null){
                tvQuestionTitle.setText(helpThreeEntity.title);
                tvQuestionContent.setText(helpThreeEntity.content);
                id = helpThreeEntity.id;
            }
        }

        cbBad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    thumb = "1";
                    cbGood.setChecked(false);
                }else{
                    thumb = "";
                }
                checkButtonStatus();
            }
        });

        cbGood.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    thumb = "0";
                    cbBad.setChecked(false);
                }else{
                    thumb = "";
                }
                checkButtonStatus();
            }
        });

        etImprove.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                comment = s.toString();
                checkButtonStatus();
            }
        });
    }

    private void checkButtonStatus(){
        tvSubmit.setVisibility(!TextUtils.isEmpty(comment)?View.VISIBLE:View.GONE);
        tvSubmit.setEnabled(!TextUtils.isEmpty(thumb));
    }

    @OnClick({R.id.iv_back, R.id.tv_submit})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_submit:
                comment = etImprove.getText().toString().trim();
                mPresenter.submitHelpQuestion(id,comment,thumb);
                break;
            default:break;
        }
    }

    @Override
    public void getHelpContentListSuccess(HelpListEntity helpListEntity) {

    }

    @Override
    public void submitHelpQuestionSuccess(Void result) {
        llSubmitSuccess.setVisibility(View.VISIBLE);
        llSubmitContent.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @Override
    public HelpContract.HelpListPresenter getPresenter() {
        return new HelpPresenter();
    }
}
