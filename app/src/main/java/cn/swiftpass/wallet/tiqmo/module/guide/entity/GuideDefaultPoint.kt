package cn.swiftpass.wallet.tiqmo.module.guide.entity

import java.io.Serializable

data class GuideDefaultPoint(
    val x: Float,
    val y: Float,
    val r: Float
) : Serializable {


    fun getRx() = x + r

    fun getRy() = y + r


}
