package cn.swiftpass.wallet.tiqmo.module.home.contract;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;
import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.NotifyEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.RequestCenterEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SendReceiveSplitDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SplitListEntity;
import cn.swiftpass.wallet.tiqmo.module.paybill.entity.PayBillOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;

public class SplitBillContract {

    public interface View extends BaseView<SplitBillContract.Presenter> {

        void getLimitSuccess(TransferLimitEntity transferLimitEntity);

        void getLimitFail(String errCode, String errMsg);
    }

    public interface Presenter extends BasePresenter<SplitBillContract.View> {
        void getLimit(String type);
    }

    public interface SplitPresenter extends BasePresenter<SplitView>{
        void getLimit(String type);
        void getSplit(String splitType);
        void ReceiveSplitBillDetail(String orderNo);
        void sendNotify(List<NotifyEntity> receivers);
        void getSplitDetail(String originalOrderNo,String orderRelevancy,String splitType);
    }

    public interface SplitView extends BaseView<SplitBillContract.SplitPresenter>{
        void ReceiveSplitBillDetailSuccess(RequestCenterEntity requestCenterEntity);
        void getSplitSuccess(SplitListEntity entity);
        void getLimitSuccess(TransferLimitEntity transferLimitEntity);
        void getSplitDetailSuccess(SendReceiveSplitDetailEntity detailEntity);
        void sendNotifyFail(String errorCode, String errorMsg);
        void sendNotifySuccess(PayBillOrderInfoEntity result);
        void showError(String errCode, String errMsg);
    }

    public interface SendReceiveDetailPresenter extends BasePresenter<SplitBillContract.SendReceiveDetailView>{
        void sendSplitsRemind(String originalOrderNo,String orderRelevancy);
        void sendNotify(List<NotifyEntity> receivers);
        void ReceiveSplitBillDetail(String orderNo);
        void getSplitDetail(String originalOrderNo,String orderRelevancy,String splitType);
        void transferSurePay(String sceneType, String orderNo, String payMethod, String transAmount,
                             String exchangeRate, String transCurrencyCode, String transFees, String vat);
    }

    public interface SendReceiveDetailView extends BaseView<SplitBillContract.SendReceiveDetailPresenter>{
        void transferSurePaySuccess(TransferEntity transferEntity);
        void ReceiveSplitBillDetailSuccess(RequestCenterEntity requestCenterEntity);
        void getSplitDetailSuccess(SendReceiveSplitDetailEntity detailEntity);
        void sendSplitsRemindSuccess();
        void showError(String errCode, String errMsg);
        void sendNotifyFail(String errorCode, String errorMsg);
        void sendNotifySuccess(PayBillOrderInfoEntity result);
    }
}
