package cn.swiftpass.wallet.tiqmo.module.chat.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.gc.gcchat.GcChatSDK;
import com.gc.gcchat.GcChatSession;
import com.gc.gcchat.callback.GcChatSimpleCallback;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.chat.dialog.ChatConfirmDialog;
import cn.swiftpass.wallet.tiqmo.module.chat.utils.MessageUtils;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.support.chat.ChatHelper;
import cn.swiftpass.wallet.tiqmo.support.chat.GcChatErrorUtils;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ButtonUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;
import cn.swiftpass.wallet.tiqmo.widget.MyTextView;

public class EditChatUserActivity extends BaseCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    MyTextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.iv_user_status)
    ImageView ivUserStatus;
    @BindView(R.id.tv_name)
    MyTextView tvName;
    @BindView(R.id.tv_create_date)
    MyTextView tvCreateDate;
    @BindView(R.id.tv_block_user)
    MyTextView tvBlockUser;
    @BindView(R.id.ll_block_member)
    LinearLayout llBlockMember;

    private GcChatSession gcChatSession;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_edit_chat_user;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        setDarkBar();
        LocaleUtils.viewRotationY(mContext, ivBack, ivHeadCircle);
        if (getIntent() != null && getIntent().getExtras() != null) {
            gcChatSession = (GcChatSession) getIntent().getSerializableExtra(Constants.gcChatSession);
            if (gcChatSession != null) {
                tvName.setText(MessageUtils.getChatSessionName(gcChatSession));
                String displayPic = MessageUtils.getChatSessionDisplayPic(gcChatSession);
                if (displayPic != null && !displayPic.isEmpty()) {
                    Glide.with(mContext).load(ChatHelper.getInstance().getChatUserAvatar(displayPic)).centerCrop().autoClone().into(ivAvatar);
                } else {
                    Glide.with(mContext).load(ThemeSourceUtils.getDefAvatar(mContext, "")).into(ivAvatar);
                }
                setUserStatus();
            }
        }
    }

    private void setUserStatus(){
        if(gcChatSession.isBlocked()){
            ivUserStatus.setVisibility(View.VISIBLE);
            ivAvatar.setAlpha(0.3f);
            tvBlockUser.setText(getString(R.string.sprint20_45));
            tvCreateDate.setText(getString(R.string.sprint11_123));
        }else{
            ivUserStatus.setVisibility(View.GONE);
            ivAvatar.setAlpha(1.0f);
            tvBlockUser.setText(getString(R.string.sprint20_40));
            tvCreateDate.setText("");
//            tvCreateDate.setText(getString(R.string.sprint20_55).replace("XXX", MessageUtils.getChatSessionDate(gcChatSession)));
        }
    }

    @OnClick({R.id.iv_back, R.id.ll_block_member})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_block_member:
                if(gcChatSession != null){
                    if(gcChatSession.isBlocked()){
                        unblockSession(gcChatSession);
                    }else{
                        showChatConfirmDialog(getString(R.string.sprint20_41).replace("XXX",MessageUtils.getChatSessionName(gcChatSession)), getString(R.string.common_6), getString(R.string.sprint20_53),
                                getString(R.string.sprint20_42), new ChatConfirmDialog.ConfirmListener() {
                                    @Override
                                    public void clickLeft() {
                                        dissmissChatConfirmDialog();
                                    }

                                    @Override
                                    public void clickRight() {
                                        dissmissChatConfirmDialog();
                                        blockSession(gcChatSession);
                                    }
                                });
                    }
                }
                break;
            default:break;
        }
    }

    private void blockSession(GcChatSession gcChatSession) {
        showProgressCancelable(true,true);
        GcChatSDK.blockSession(gcChatSession.getSessionId(), new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                showProgress(false);
                gcChatSession.setBlocked(true);
                setUserStatus();
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_BLOCK_CHAT_USER,gcChatSession));
//                showTipDialog("Blocked successfully!!");
            }

            @Override
            public void onFailure(int i) {
                showProgress(false);
                showTipDialog("Error in block session - " + GcChatErrorUtils.getErrorMessage(i));
            }
        });
    }

    private void unblockSession(GcChatSession gcChatSession) {
        showProgressCancelable(true,true);
        GcChatSDK.unblockSession(gcChatSession.getSessionId(), new GcChatSimpleCallback() {
            @Override
            public void onSuccessful() {
                showProgress(false);
                gcChatSession.setBlocked(false);
//                showTipDialog("Unblocked successfully!!");
                ChatRoomActivity.refreshOnResume = true;
                setUserStatus();
                EventBus.getDefault().post(new EventEntity(EventEntity.EVENT_BLOCK_CHAT_USER,gcChatSession));
            }

            @Override
            public void onFailure(int i) {
                showTipDialog("Error in unblock session - " + GcChatErrorUtils.getErrorMessage(i));
                showProgress(false);
            }
        });
    }
}
