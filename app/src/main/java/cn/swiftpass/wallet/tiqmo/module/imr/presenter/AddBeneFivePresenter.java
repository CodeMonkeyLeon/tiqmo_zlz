package cn.swiftpass.wallet.tiqmo.module.imr.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.addmoney.entity.TransferLimitEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.contract.AddBeneficiaryContract;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrAddBeneResultEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBankAgentListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrBeneficiaryDetails;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrCityListEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrExchangeRateEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ImrPayerDetailEntity;
import cn.swiftpass.wallet.tiqmo.module.imr.entity.ResponseEntity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

public class AddBeneFivePresenter implements AddBeneficiaryContract.AddFivePresenter {

    AddBeneficiaryContract.AddFiveView addFiveView;

    @Override
    public void getOtpType(String category, String categoryId, List<String> additionalData) {
        AppClient.getInstance().getOtpType(category, categoryId, additionalData, new LifecycleMVPResultCallback<RechargeOrderInfoEntity>(addFiveView,true) {
            @Override
            protected void onSuccess(RechargeOrderInfoEntity result) {
                addFiveView.getOtpTypeSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.getOtpTypeFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void activateBeneficiary(String payeeInfoId) {
        AppClient.getInstance().activateIvrBeneficary(payeeInfoId, new LifecycleMVPResultCallback<Void>(addFiveView,true) {
            @Override
            protected void onSuccess(Void result) {
                addFiveView.activateBeneficiarySuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.showErrorMsg(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getBankAgentName(String countryCode, String receiptMethod,String channelCode) {
        AppClient.getInstance().imrBankAgentNameList(countryCode, receiptMethod,channelCode, new LifecycleMVPResultCallback<ImrBankAgentListEntity>(addFiveView, true) {
            @Override
            protected void onSuccess(ImrBankAgentListEntity result) {
                addFiveView.getBankAgentNameSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.getBankAgentNameFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getImrCityList(String countryCode,String channelCode) {
        AppClient.getInstance().getImrCityList(countryCode,channelCode, new LifecycleMVPResultCallback<ImrCityListEntity>(addFiveView,true) {
            @Override
            protected void onSuccess(ImrCityListEntity result) {
                addFiveView.getImrCityListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.getImrCityListFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void getBankAgentBranch(String countryCode, String receiptMethod, String parentNodeCode) {
        AppClient.getInstance().imrBankAgentBranchList(countryCode, receiptMethod, parentNodeCode, new LifecycleMVPResultCallback<ImrBankAgentListEntity>(addFiveView, true) {
            @Override
            protected void onSuccess(ImrBankAgentListEntity result) {
                addFiveView.getBankAgentBranchSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.getBankAgentBranchFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void imrAddBeneficiary(String receiptMethod, String receiptOrgCode, String receiptOrgBranchCode, String payeeFullName, String nickName, String relationshipCode, String callingCode, String phone, String transferDestinationCountryCode, String payeeInfoCountryCode, String birthPlace, String birthDate, String sex, String cityName, String districtName, String poBox, String buildingNo, String street, String idNo, String idExpiry, String bankAccountType, String ibanNo, String bankAccountNo, String saveFlag,
                                  String receiptOrgName, String receiptOrgBranchName, String cityId, String currencyCode,String channelPayeeId,String channelCode,String branchId) {
        AppClient.getInstance().imrAddBeneficiary(receiptMethod, receiptOrgCode, receiptOrgBranchCode, payeeFullName,
                nickName, relationshipCode, callingCode, phone,
                transferDestinationCountryCode, payeeInfoCountryCode, birthPlace,
                birthDate, sex, cityName, districtName,
                poBox, buildingNo, street, idNo, idExpiry,
                bankAccountType, ibanNo, bankAccountNo, saveFlag,receiptOrgName,
                receiptOrgBranchName,cityId,currencyCode,channelPayeeId,channelCode,branchId, new LifecycleMVPResultCallback<ImrAddBeneResultEntity>(addFiveView, true) {
                    @Override
                    protected void onSuccess(ImrAddBeneResultEntity result) {
                        addFiveView.imrAddBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        addFiveView.imrAddBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void imrExchangeRate(String payeeInfoId,String sourceAmount,
                                String destinationAmount,String channelCode) {
        AppClient.getInstance().imrExchangeRate(payeeInfoId, sourceAmount,
                 destinationAmount, channelCode, new LifecycleMVPResultCallback<ImrExchangeRateEntity>(addFiveView, true) {
            @Override
            protected void onSuccess(ImrExchangeRateEntity result) {
                addFiveView.imrExchangeRateSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.imrExchangeRateFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void getLimit(String type) {
        AppClient.getInstance().getLimit(type, new LifecycleMVPResultCallback<TransferLimitEntity>(addFiveView, true) {
            @Override
            protected void onSuccess(TransferLimitEntity result) {
                addFiveView.getLimitSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.getLimitFail(errorCode, errorMsg);
            }
        });
    }

    @Override
    public void saveImrBeneficiaryInfo(ImrBeneficiaryDetails.ImrBeneficiaryDetail beneficiaryInfo) {
        AppClient.getInstance().saveImrBeneficiaryInfo(beneficiaryInfo.payeeInfoId,
                beneficiaryInfo.transferDestinationCountryCode, beneficiaryInfo.receiptMethod,
                beneficiaryInfo.receiptOrgCode, beneficiaryInfo.receiptOrgBranchCode,
                beneficiaryInfo.payeeFullName, beneficiaryInfo.nickName, beneficiaryInfo.relationshipCode,
                beneficiaryInfo.callingCode, beneficiaryInfo.phone, beneficiaryInfo.payeeInfoCountryCode,
                beneficiaryInfo.birthPlace, beneficiaryInfo.birthDate, beneficiaryInfo.sex,
                beneficiaryInfo.cityName, beneficiaryInfo.districtName, beneficiaryInfo.poBox,
                beneficiaryInfo.buildingNo, beneficiaryInfo.street, beneficiaryInfo.idNo,
                beneficiaryInfo.idExpiry, beneficiaryInfo.bankAccountType, beneficiaryInfo.ibanNo,
                beneficiaryInfo.bankAccountNo,beneficiaryInfo.receiptOrgName,beneficiaryInfo.receiptOrgBranchName,
                beneficiaryInfo.cityId,beneficiaryInfo.currencyCode,beneficiaryInfo.channelPayeeId,beneficiaryInfo.channelCode,beneficiaryInfo.branchId,
                new LifecycleMVPResultCallback<ResponseEntity>(addFiveView, true) {
                    @Override
                    protected void onSuccess(ResponseEntity result) {
                        addFiveView.imrEditBeneficiarySuccess(result);
                    }

                    @Override
                    protected void onFail(String errorCode, String errorMsg) {
                        addFiveView.imrEditBeneficiaryFail(errorCode, errorMsg);
                    }
                });
    }

    @Override
    public void getImrPayerDetail(String countryCode, String cityId, String payerCurrencyCode, String payeeCurrencyCode,String channelCode) {
        AppClient.getInstance().getImrPayerDetail(countryCode, cityId, payerCurrencyCode, payeeCurrencyCode,channelCode, new LifecycleMVPResultCallback<ImrPayerDetailEntity>(addFiveView,true) {
            @Override
            protected void onSuccess(ImrPayerDetailEntity result) {
                addFiveView.getImrPayerDetailSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                addFiveView.getImrPayerDetailFail(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(AddBeneficiaryContract.AddFiveView addFiveView) {
        this.addFiveView = addFiveView;
    }

    @Override
    public void detachView() {
        this.addFiveView = null;
    }
}
