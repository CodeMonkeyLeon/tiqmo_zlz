package cn.swiftpass.wallet.tiqmo.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.module.home.adapter.BudgetCategorychooseAdapter;
import cn.swiftpass.wallet.tiqmo.module.home.entity.SpendingBudgetEntity;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.widget.MaxHeightRecyclerView;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.tiqmo.widget.recyclerview.MyItemDecoration;

public class BudgetCategoryDialog extends BottomDialog {
    private Context mContext;
    private MaxHeightRecyclerView rvCategoryList;


    private LinearLayoutManager mLayoutManager;
    private BudgetCategorychooseAdapter budgetCategorychooseAdapter;

    private BudgetCategoryItemClickListener mBudgetCategoryItemClickListener;

    public void setBudgetCategoryItemClickListener(final BudgetCategoryItemClickListener budgetCategoryItemClickListener) {
        this.mBudgetCategoryItemClickListener = budgetCategoryItemClickListener;
    }

    public List<SpendingBudgetEntity> mDateList = new ArrayList<>();

    public void setDateList(final List<SpendingBudgetEntity> dateList) {
        this.mDateList = dateList;
        initRecyclerView(dateList);
    }

    private void initRecyclerView(List<SpendingBudgetEntity> dateList) {
        budgetCategorychooseAdapter = new BudgetCategorychooseAdapter(dateList);
        mLayoutManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        MyItemDecoration myItemDecoration = MyItemDecoration.createVertical(mContext.getColor(R.color.transparent), AndroidUtils.dip2px(mContext, 12));
//        rvCategoryList.addItemDecoration(myItemDecoration);
        rvCategoryList.setLayoutManager(mLayoutManager);
        budgetCategorychooseAdapter.bindToRecyclerView(rvCategoryList);
        budgetCategorychooseAdapter.setDataList(dateList);

        budgetCategorychooseAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                SpendingBudgetEntity spendingBudgetEntity = budgetCategorychooseAdapter.getDataList().get(position);
                if (mBudgetCategoryItemClickListener != null) {
                    if (spendingBudgetEntity != null) {
                        mBudgetCategoryItemClickListener.chooseBudgetCategory(spendingBudgetEntity);
                        dismiss();
                    }
                }
            }
        });
    }

    public BudgetCategoryDialog(Context context) {
        super(context);
        this.mContext = context;
        initViews(mContext);
    }

    private void initViews(Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_budget_category_list, null);
        rvCategoryList = view.findViewById(R.id.rv_category_list);

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        setContentView(view);
        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setAttributes(layoutParams);
        }
    }

    public interface BudgetCategoryItemClickListener {
        void chooseBudgetCategory(SpendingBudgetEntity spendingBudgetEntity);
    }
}
