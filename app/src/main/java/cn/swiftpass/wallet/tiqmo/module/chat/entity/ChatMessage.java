package cn.swiftpass.wallet.tiqmo.module.chat.entity;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.transfer.entity.TransferEntity;
import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class ChatMessage extends BaseEntity {

    @SerializedName("text")
    private String text;
    @SerializedName("attachments")
    private List<Attachment> attachments;

    public String getMessage() {
        return text == null?"":text;
    }

    public void setMessage(String text) {
        this.text = text;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    @NonNull
    public List<Attachment> getAttachments() {
        if(attachments == null){
            attachments = new ArrayList<>();
        }
        return attachments;
    }

    public static class Attachment implements Serializable {
        @SerializedName("fileId")
        public String fileId;

        @SerializedName("downloadLink")
        public String downloadLink;

        @SerializedName("fileType")
        public String fileType;

        @SerializedName("fileName")
        public String fileName;
    }

}
