package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.zrq.spanbuilder.Spans;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.adapter.KsaChangeLimitDialog;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.contract.CardKsaContract;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaCardEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity.KsaLimitChannelEntity;
import cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.presenter.KsaCardSetPresenter;
import cn.swiftpass.wallet.tiqmo.module.register.view.RegisterOtpActivity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.EventEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.RiskControlEntity;
import cn.swiftpass.wallet.tiqmo.module.setting.view.IvrRequestActivity;
import cn.swiftpass.wallet.tiqmo.module.topup.entity.RechargeOrderInfoEntity;
import cn.swiftpass.wallet.tiqmo.module.transfer.view.TransferPwdActivity;
import cn.swiftpass.wallet.tiqmo.support.UserInfoManager;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.tiqmo.support.utils.AndroidUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class KsaCardLimitActivity extends BaseCompatActivity<CardKsaContract.SetCardLimitPresener> implements CardKsaContract.SetCardLimitView {


    @BindView(R.id.tv_background)
    TextView tvBackground;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.tv_daily_money)
    TextView tvDailyMoney;
    @BindView(R.id.tv_daily_second_money)
    TextView tvDailySecondMoney;
    @BindView(R.id.progress_daily_limit)
    ProgressBar progressDailyLimit;
    @BindView(R.id.tv_daily_rate)
    TextView tvDailyRate;
    @BindView(R.id.tv_change_daily)
    TextView tvChangeDaily;
    @BindView(R.id.tv_line)
    TextView tvLine;
    @BindView(R.id.tv_monthly_money)
    TextView tvMonthlyMoney;
    @BindView(R.id.tv_monthly_second_money)
    TextView tvMonthlySecondMoney;
    @BindView(R.id.progress_monthly_limit)
    ProgressBar progressMonthlyLimit;
    @BindView(R.id.tv_monthly_rate)
    TextView tvMonthlyRate;
    @BindView(R.id.tv_change_monthly)
    TextView tvChangeMonthly;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.sc_content)
    NestedScrollView scContent;
    private KsaChangeLimitDialog cardChangeLimitDialog;

    private KsaLimitChannelEntity ksaLimitChannelEntity;

    KsaLimitChannelEntity.CardLimitConfigEntity mCardLimitConfigEntity;

//    private String txnType, txnTypeEnabled = "1";

    public List<KsaLimitChannelEntity.CardLimitConfigEntity> limitConfig = new ArrayList<>();

    public List<String> enabledChannels = new ArrayList<>();
    public List<String> disabledChannels = new ArrayList<>();
    private String proxyCardNo;

    private String lastDailyLimit,lastMonthlyLimit;

    //1代表上调限额 0代表减少限额
    private String up;
    private KsaCardEntity ksaCardEntity;

    private RiskControlEntity riskControlEntity;

    private static KsaCardLimitActivity ksaCardLimitActivity;

    public static KsaCardLimitActivity getKsaCardLimitActivity() {
        return ksaCardLimitActivity;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_ksa_card_limits;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ksaCardLimitActivity = this;
        tvTitle.setText(R.string.Card_limits_2);
        setDarkBar();
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        if (getIntent().getExtras() != null) {
            ksaCardEntity = (KsaCardEntity) getIntent().getExtras().getSerializable(Constants.ksaCardEntity);
            ksaLimitChannelEntity = (KsaLimitChannelEntity) getIntent().getExtras().getSerializable(Constants.ksaLimitChannelEntity);
            setCardLimit(ksaLimitChannelEntity);
        }
    }

    private void setCardLimit(KsaLimitChannelEntity ksaLimitChannelEntity){
        if (ksaLimitChannelEntity != null) {
            proxyCardNo = ksaLimitChannelEntity.proxyCardNo;
            limitConfig = ksaLimitChannelEntity.limitConfig;
            enabledChannels = ksaLimitChannelEntity.enabledChannels;
            disabledChannels = ksaLimitChannelEntity.disabledChannels;
            if (disabledChannels != null && disabledChannels.size() > 0) {
                int channelSize = disabledChannels.size();
                for (int i = 0; i < channelSize; i++) {
                    String channel = disabledChannels.get(i);
                }
            }
            int size = limitConfig.size();
            if(size>0){
                mCardLimitConfigEntity = limitConfig.get(0);
                //	月限额最小值	5000.0
                String monthlyLimitMin;
                //月限额最大值	10000.0
                String monthlyLimitMax = mCardLimitConfigEntity.monthlyLimitMax;
                //月限额范围国际化提示信息
                String monthlyLimitRangeNote;
                //当前月限额	5000.0
                String monthlyLimitValue = mCardLimitConfigEntity.monthlyLimitValue;
                //已使用日限额
                String dailyLimitCnt = mCardLimitConfigEntity.dailyLimitCnt;
                //已使用月限额
                String monthlyLimitCnt = mCardLimitConfigEntity.monthlyLimitCnt;
                //日限额最小值	100.0
                String dailyLimitMin = mCardLimitConfigEntity.dailyLimitMin;
                //	日限额最大值	1000.0
                String dailyLimitMax = mCardLimitConfigEntity.dailyLimitMax;
                //	日限额范围国际化提示信息
                String dailyLimitRangeNote;
                //交易类型	ATM/NFC/POS/ECOM
                String txnType;
                //当前日限额	200.0
                String dailyLimitValue = mCardLimitConfigEntity.dailyLimitValue;
                lastDailyLimit = AndroidUtils.getTransferStringMoney(mCardLimitConfigEntity.dailyLimitValue);
                lastMonthlyLimit = AndroidUtils.getTransferStringMoney(mCardLimitConfigEntity.monthlyLimitValue);
                tvDailyMoney.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(dailyLimitCnt) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf")).size(15)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf")).size(10)
                        .build());
                tvDailySecondMoney.setText(mContext.getString(R.string.analytics_21) + " " + AndroidUtils.getTransferMoney(dailyLimitValue) + " " + LocaleUtils.getCurrencyCode(""));

                int dailyProgress = Integer.parseInt(BigDecimalFormatUtils.div(BigDecimalFormatUtils.mul(dailyLimitCnt,UserInfoManager.getInstance().getConvertUnit(),0),dailyLimitValue,0));
                if (dailyProgress < 100) {
                    progressDailyLimit.setProgressDrawable(mContext.getDrawable(R.drawable.progressbar_budget_1cfcff));
                } else {
                    progressDailyLimit.setProgressDrawable(mContext.getDrawable(R.drawable.progressbar_budget_fd1d2d));
                }
                progressDailyLimit.setProgress(dailyProgress);
                tvDailyRate.setText(dailyProgress + "%");

                tvMonthlyMoney.setText(Spans.builder()
                        .text(AndroidUtils.getTransferMoney(monthlyLimitCnt) + " ").color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf")).size(15)
                        .text(LocaleUtils.getCurrencyCode("")).color(mContext.getColor(ThemeSourceUtils.getSourceID(mContext, R.attr.color_white_3a3b44)))
                        .typeface(Typeface.createFromAsset(mContext.getAssets(), LocaleUtils.isRTL(mContext)?"fonts/Alilato-Semi-Bold.ttf":"fonts/SFProDisplay-Semibold.ttf")).size(10)
                        .build());
                tvMonthlySecondMoney.setText(mContext.getString(R.string.analytics_21) + " " + AndroidUtils.getTransferMoney(monthlyLimitValue) + " " + LocaleUtils.getCurrencyCode(""));

                int monthlyProgress = Integer.parseInt(BigDecimalFormatUtils.div(BigDecimalFormatUtils.mul(monthlyLimitCnt,UserInfoManager.getInstance().getConvertUnit(),0),monthlyLimitValue,0));
                if (monthlyProgress < 100) {
                    progressMonthlyLimit.setProgressDrawable(mContext.getDrawable(R.drawable.progressbar_budget_1cfcff));
                } else {
                    progressMonthlyLimit.setProgressDrawable(mContext.getDrawable(R.drawable.progressbar_budget_fd1d2d));
                }
                progressMonthlyLimit.setProgress(monthlyProgress);
                tvMonthlyRate.setText(monthlyProgress + "%");
            }
        }
    }

    private void showChangeLimitDialog(KsaLimitChannelEntity.CardLimitConfigEntity cardLimitConfigEntity) {
        if (cardLimitConfigEntity == null) {
            return;
        }
        cardChangeLimitDialog = new KsaChangeLimitDialog(mContext, cardLimitConfigEntity);
        cardChangeLimitDialog.setChangeLimitListener(new KsaChangeLimitDialog.ChangeLimitListener() {
            @Override
            public void changeLimit(KsaLimitChannelEntity.CardLimitConfigEntity cardLimitConfigEntity) {
                mCardLimitConfigEntity = cardLimitConfigEntity;
                mCardLimitConfigEntity.proxyCardNo = proxyCardNo;
                String chooseDailyLimit = mCardLimitConfigEntity.dailyLimitValue;
                String chooseMonthlyLimit = mCardLimitConfigEntity.monthlyLimitValue;
                List<String> additionalData = new ArrayList<>();
                if(ksaCardEntity != null){
                    additionalData.add(ksaCardEntity.maskedCardNo);
                }
                if(mCardLimitConfigEntity.isDaily) {
                    if (BigDecimalFormatUtils.compareInt(lastDailyLimit, chooseDailyLimit) > 0) {
                        up = "0";
                        mPresenter.getOtpType("ActionType", "TCA06", additionalData);
                    } else if (BigDecimalFormatUtils.compareInt(lastDailyLimit, chooseDailyLimit) < 0) {
                        up = "1";
                        mPresenter.getOtpType("ActionType", "TCA07", additionalData);
                    }else{
                        cardChangeLimitDialog.dismiss();
                    }
                }else{
                    if (BigDecimalFormatUtils.compareInt(lastMonthlyLimit, chooseMonthlyLimit) > 0) {
                        up = "0";
                        mPresenter.getOtpType("ActionType", "TCA06", additionalData);
                    } else if (BigDecimalFormatUtils.compareInt(lastMonthlyLimit, chooseMonthlyLimit) < 0) {
                        up = "1";
                        mPresenter.getOtpType("ActionType", "TCA07", additionalData);
                    }else{
                        cardChangeLimitDialog.dismiss();
                    }
                }
            }
        });
        cardChangeLimitDialog.showWithBottomAnim();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventEntity event) {
        if (EventEntity.EVENT_KSA_SET_LIMIT_SUCCESS == event.getEventType()) {
            if(cardChangeLimitDialog != null){
                cardChangeLimitDialog.dismiss();
            }
            mPresenter.getKsaLimitChannel(ksaLimitChannelEntity.proxyCardNo);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ksaCardLimitActivity = null;
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    public void getKsaLimitChannelSuccess(KsaLimitChannelEntity ksaLimitChannelEntity) {
        ksaLimitChannelEntity.proxyCardNo = proxyCardNo;
        setCardLimit(ksaLimitChannelEntity);
    }

    @Override
    public void setKsaCardLimitSuccess(Void result) {
        if(cardChangeLimitDialog != null){
            cardChangeLimitDialog.dismiss();
        }
        mPresenter.getKsaLimitChannel(ksaLimitChannelEntity.proxyCardNo);
    }

    @Override
    public void showErrorMsg(String errorCode, String errorMsg) {
        showTipDialog(errorMsg);
    }

    @OnClick({R.id.iv_back, R.id.tv_change_daily, R.id.tv_change_monthly})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_change_daily:
                if(AndroidUtils.isCloseUse(this,Constants.cards_limit_change))return;
                mCardLimitConfigEntity.isDaily = true;
                showChangeLimitDialog(mCardLimitConfigEntity);
                break;
            case R.id.tv_change_monthly:
                if(AndroidUtils.isCloseUse(this,Constants.cards_limit_change))return;
                mCardLimitConfigEntity.isDaily = false;
                showChangeLimitDialog(mCardLimitConfigEntity);
                break;
            default:break;
        }
    }

    @Override
    public void getOtpTypeSuccess(RechargeOrderInfoEntity rechargeOrderInfoEntity) {
        if (rechargeOrderInfoEntity != null) {
            riskControlEntity = rechargeOrderInfoEntity.riskControlInfo;
            if (riskControlEntity != null) {
                if (riskControlEntity.isOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>(16);
                    setTransferMap(mHashMaps);
                    ActivitySkipUtil.startAnotherActivity(KsaCardLimitActivity.this, RegisterOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else if(riskControlEntity.isPayPwd(riskControlEntity.paySecretRequired)){
                    jumpToPwd(false);
                }else if(riskControlEntity.isNeedIvr()){
                    jumpToPwd(true);
                }else{
                    requestTransfer();
                }
            } else {
                requestTransfer();
            }
        } else {
            requestTransfer();
        }
    }

    public void jumpToPwd(boolean isNeedIvr) {
        HashMap<String, Object> mHashMap = new HashMap<>(16);
        setTransferMap(mHashMap);
        if(isNeedIvr){
            ActivitySkipUtil.startAnotherActivity(this, IvrRequestActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }else {
            ActivitySkipUtil.startAnotherActivity(this, TransferPwdActivity.class, mHashMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void setTransferMap(HashMap<String, Object> mHashMap){
        mHashMap.put(Constants.sceneType, Constants.TYPE_KSA_CARD_SET_LIMIT);
        mCardLimitConfigEntity.up = up;
        mHashMap.put(Constants.ksaCardLimitConfigEntity, mCardLimitConfigEntity);
        mHashMap.put(Constants.ksaCardEntity,ksaCardEntity);

        mHashMap.put(Constants.DATA_OTP_TYPE, Constants.OTP_FIRST_SET_KSA_CARD_LIMIT);
        mHashMap.put(Constants.DATA_FIRST_TITLE, getString(R.string.DigitalCard_0));
        mHashMap.put(Constants.OTP_riskControlEntity, riskControlEntity);
    }

    private void requestTransfer(){
        String txnType = mCardLimitConfigEntity.txnType;
        String limitMoney = mCardLimitConfigEntity.isDaily ? mCardLimitConfigEntity.dailyLimitValue : mCardLimitConfigEntity.monthlyLimitValue;
        mPresenter.setKsaCardLimit(txnType, mCardLimitConfigEntity.isDaily ? AndroidUtils.getReqTransferMoney(limitMoney) : "",
                mCardLimitConfigEntity.isDaily ? "" : AndroidUtils.getReqTransferMoney(limitMoney),ksaLimitChannelEntity.proxyCardNo,up);
    }

    @Override
    public void getOtpTypeFail(String errCode, String errMsg) {
        showTipDialog(errMsg);
    }

    @Override
    public CardKsaContract.SetCardLimitPresener getPresenter() {
        return new KsaCardSetPresenter();
    }
}
