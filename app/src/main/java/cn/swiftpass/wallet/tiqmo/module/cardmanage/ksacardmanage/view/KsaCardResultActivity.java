package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.tiqmo.ProjectApp;
import cn.swiftpass.wallet.tiqmo.R;
import cn.swiftpass.wallet.tiqmo.base.view.BaseCompatActivity;
import cn.swiftpass.wallet.tiqmo.support.locale.LocaleUtils;
import cn.swiftpass.wallet.tiqmo.support.theme.ThemeSourceUtils;
import cn.swiftpass.wallet.tiqmo.support.utils.constants.Constants;

public class KsaCardResultActivity extends BaseCompatActivity {

    public static final int cardResultType_Set_Pin = 1;
    //开卡失败
    public static final int cardResultType_Open_Card_fail = 2;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_head_circle)
    ImageView ivHeadCircle;
    @BindView(R.id.iv_kyc_step)
    ImageView ivKycStep;
    @BindView(R.id.common_head)
    ConstraintLayout commonHead;
    @BindView(R.id.iv_result)
    ImageView ivResult;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_content_desc)
    TextView tvContentDesc;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    private int cardResultType;
    private String cardResultErrorMsg;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_card_result_success;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
//        ivBack.setVisibility(View.GONE);
        LocaleUtils.viewRotationY(this, ivBack, ivHeadCircle);
        tvCancel.setVisibility(View.GONE);
        tvContentDesc.setVisibility(View.GONE);
        ivBack.setImageResource(ThemeSourceUtils.getSourceID(mContext, R.attr.icon_card_close));
        if (getIntent().getExtras() != null) {
            cardResultType = getIntent().getExtras().getInt(Constants.cardResultType);
            cardResultErrorMsg = getIntent().getExtras().getString(Constants.cardResultErrorMsg);
            switch (cardResultType){
                case 1:
                    //set pin成功
                    tvConfirm.setText(getString(R.string.DigitalCard_37));
                    tvContent.setText(getString(R.string.DigitalCard_72));
                    ivResult.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_set_pin_success));
                    break;
                case cardResultType_Open_Card_fail:
                    ivBack.setVisibility(View.GONE);
                    tvConfirm.setText(getString(R.string.DigitalCard_37));
                    tvContent.setText(getString(R.string.Card_block_error_1));
                    tvContentDesc.setVisibility(View.VISIBLE);
                    tvContentDesc.setText(cardResultErrorMsg);
                    ivResult.setImageResource(ThemeSourceUtils.getSourceID(mContext,R.attr.icon_digital_block));
                    break;
            }
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_confirm, R.id.tv_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
            case R.id.tv_confirm:
                ProjectApp.removeAllTaskExcludeMainStack();
                finish();
                break;
            case R.id.tv_cancel:
                finish();
                break;
            default:
                break;
        }
    }
}