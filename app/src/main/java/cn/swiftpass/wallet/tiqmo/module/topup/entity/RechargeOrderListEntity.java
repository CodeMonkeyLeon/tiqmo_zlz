package cn.swiftpass.wallet.tiqmo.module.topup.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class RechargeOrderListEntity extends BaseEntity {
    /**
     * "internationalList\":[{\"countryImgUrl\":\"http://wallet-saas-dev.wallyt.net/tiqmo/cms/file/download?imr/country/in.png&r=1647487054092\",
     * \"userShortName\":\"Dpp\",\"recordId\":3,\"countryCode\":\"IND\",\"countryName\":\"India\",\"operatorName\":\"Airtel India\"}],
     */

    //国内话费运营商列表
    public List<RechargeDomeOrderEntity> domesticList = new ArrayList<>();
    //偏好支付列表
    public List<RechargeInterOrderEntity> internationalList = new ArrayList<>();
    //偏好支付列表条数
    public long pastPaymentCount;

    public List<RechargeInterOrderEntity> getTestList() {
        List<RechargeInterOrderEntity> list = new ArrayList<>();
        RechargeInterOrderEntity payBillOrderEntity = new RechargeInterOrderEntity();
        payBillOrderEntity.countryImgUrl = "http://wallet-saas-dev.wallyt.net/tiqmo/cms/file/download?imr/country/pk.png";
        payBillOrderEntity.recordId = "586000015";
        payBillOrderEntity.operatorName = "mobile prepaid";
        payBillOrderEntity.userShortName = "Warid Prepaid";
        payBillOrderEntity.countryName = "Warid Prepaid";
        payBillOrderEntity.operatorImgUrl = "http://wallet-saas-dev.wallyt.net/tiqmo/cms/file/download?imr/country/pk.png";
        payBillOrderEntity.countryName = "Pakistan";
        payBillOrderEntity.countryCode = "PAK";
        list.add(payBillOrderEntity);
        list.add(payBillOrderEntity);
        list.add(payBillOrderEntity);
        list.add(payBillOrderEntity);
        return list;
    }
}
