package cn.swiftpass.wallet.tiqmo.module.cardmanage.ksacardmanage.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.tiqmo.support.entity.BaseEntity;

public class MapResultEntity extends BaseEntity {
    public List<ResultEntity> results = new ArrayList<>();

    public class ResultEntity{
        public String formatted_address;
        public LocationMapEntity geometry;
        public class LocationMapEntity{
            public LocationEntity location;
            public class LocationEntity{
                public String lat;
                public String lng;
            }
        }

    }
}
