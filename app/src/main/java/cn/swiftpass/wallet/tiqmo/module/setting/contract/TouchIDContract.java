package cn.swiftpass.wallet.tiqmo.module.setting.contract;

import cn.swiftpass.wallet.tiqmo.base.view.BasePresenter;
import cn.swiftpass.wallet.tiqmo.base.view.BaseView;

public class TouchIDContract {

    public interface View extends BaseView<Presenter> {
        void switchTouchIDPayment(boolean isOpen);

        void switchTouchIDLogin(boolean isOpen);
    }


    public interface Presenter extends BasePresenter<View> {
        String getUserID();

        //校验一下指纹信息有没有过期
        void checkTouchIDPaymentStateFromServer(String type);
        //设置启动指纹解锁
        void setEnableTouchIDLogin(boolean isEnable);
        //设置启动指纹支付
        void setEnableTouchIDPayment(boolean isEnable);

    }
}
