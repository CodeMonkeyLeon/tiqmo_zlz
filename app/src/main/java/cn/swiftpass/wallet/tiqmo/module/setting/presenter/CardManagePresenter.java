package cn.swiftpass.wallet.tiqmo.module.setting.presenter;

import java.util.List;

import cn.swiftpass.wallet.tiqmo.module.setting.contract.CardManageContract;
import cn.swiftpass.wallet.tiqmo.module.setting.entity.verifyCardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.AppClient;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardBind3DSEntity;
import cn.swiftpass.wallet.tiqmo.sdk.entity.CardEntity;
import cn.swiftpass.wallet.tiqmo.sdk.listener.LifecycleMVPResultCallback;

/**
 * Created by aijingya on 2020/7/10.
 *
 * @Package cn.swiftpass.wallet.tiqmo.module.setting.presenter
 * @Description:
 * @date 2020/7/10.15:16.
 */
public class CardManagePresenter implements CardManageContract.Presenter {

    private CardManageContract.View CardManagerView;

    @Override
    public void getBindCardList() {
        AppClient.getInstance().getCardManager().getAllBindCard(new LifecycleMVPResultCallback<List<CardEntity>>(CardManagerView,true) {
            @Override
            protected void onSuccess(List<CardEntity> result) {
                CardManagerView.getBindCardListSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                CardManagerView.getBindCardListFailed(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void bindNewCard(String cardHolderName,String cardNum, String expireDate,String CVV,String type) {
        AppClient.getInstance().getCardManager().bindCard(cardHolderName, cardNum, expireDate, CVV, type, new LifecycleMVPResultCallback<CardBind3DSEntity>(CardManagerView,true) {
            @Override
            protected void onSuccess(CardBind3DSEntity result) {
                CardManagerView.bindNewCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                CardManagerView.bindNewCardFailed(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void verifyCard(String cardNo) {
        AppClient.getInstance().getCardManager().verifyCard(cardNo, new LifecycleMVPResultCallback<verifyCardEntity>(CardManagerView,false) {
            @Override
            protected void onSuccess(verifyCardEntity result) {
                CardManagerView.verifyCardSuccess(result);
            }

            @Override
            protected void onFail(String errorCode, String errorMsg) {
                CardManagerView.verifyCardFailed(errorCode,errorMsg);
            }
        });
    }

    @Override
    public void attachView(CardManageContract.View view) {
        this.CardManagerView =view;
    }

    @Override
    public void detachView() {
        this.CardManagerView = null;
    }
}
